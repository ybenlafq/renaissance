      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM50   ERM50                                              00000020
      ***************************************************************** 00000030
       01   ERM50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCGROUPL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCGROUPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCGROUPF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCGROUPI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCENTI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DSIMULL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 DSIMULL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 DSIMULF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 DSIMULI   PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NSIMULL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 NSIMULL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NSIMULF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 NSIMULI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNCODICI  PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMGL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCFAMGL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMGF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCFAMGI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTEL     COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MQTEL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQTEF     PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQTEI     PIC X(4).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQSAL     COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MQSAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQSAF     PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQSAI     PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNSOCI    PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNMAGI    PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDEFFETI  PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCHEFPL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCHEFPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCHEFPF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCHEFPI   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLERL    COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCLERL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCLERF    PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCLERI    PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFONCF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCFONCI   PIC X(3).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBERRI  PIC X(78).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODTRAI  PIC X(4).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCICSI    PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNETNAMI  PIC X(8).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSCREENI  PIC X(4).                                       00000970
      ***************************************************************** 00000980
      * SDF: ERM50   ERM50                                              00000990
      ***************************************************************** 00001000
       01   ERM50O REDEFINES ERM50I.                                    00001010
           02 FILLER    PIC X(12).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MTIMJOUA  PIC X.                                          00001110
           02 MTIMJOUC  PIC X.                                          00001120
           02 MTIMJOUP  PIC X.                                          00001130
           02 MTIMJOUH  PIC X.                                          00001140
           02 MTIMJOUV  PIC X.                                          00001150
           02 MTIMJOUO  PIC X(5).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MZONCMDA  PIC X.                                          00001180
           02 MZONCMDC  PIC X.                                          00001190
           02 MZONCMDP  PIC X.                                          00001200
           02 MZONCMDH  PIC X.                                          00001210
           02 MZONCMDV  PIC X.                                          00001220
           02 MZONCMDO  PIC X(15).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MCGROUPA  PIC X.                                          00001250
           02 MCGROUPC  PIC X.                                          00001260
           02 MCGROUPP  PIC X.                                          00001270
           02 MCGROUPH  PIC X.                                          00001280
           02 MCGROUPV  PIC X.                                          00001290
           02 MCGROUPO  PIC X(5).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNSOCENTA      PIC X.                                     00001320
           02 MNSOCENTC PIC X.                                          00001330
           02 MNSOCENTP PIC X.                                          00001340
           02 MNSOCENTH PIC X.                                          00001350
           02 MNSOCENTV PIC X.                                          00001360
           02 MNSOCENTO      PIC X(3).                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNDEPOTA  PIC X.                                          00001390
           02 MNDEPOTC  PIC X.                                          00001400
           02 MNDEPOTP  PIC X.                                          00001410
           02 MNDEPOTH  PIC X.                                          00001420
           02 MNDEPOTV  PIC X.                                          00001430
           02 MNDEPOTO  PIC X(3).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 DSIMULA   PIC X.                                          00001460
           02 DSIMULC   PIC X.                                          00001470
           02 DSIMULP   PIC X.                                          00001480
           02 DSIMULH   PIC X.                                          00001490
           02 DSIMULV   PIC X.                                          00001500
           02 DSIMULO   PIC X(10).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 NSIMULA   PIC X.                                          00001530
           02 NSIMULC   PIC X.                                          00001540
           02 NSIMULP   PIC X.                                          00001550
           02 NSIMULH   PIC X.                                          00001560
           02 NSIMULV   PIC X.                                          00001570
           02 NSIMULO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNCODICA  PIC X.                                          00001600
           02 MNCODICC  PIC X.                                          00001610
           02 MNCODICP  PIC X.                                          00001620
           02 MNCODICH  PIC X.                                          00001630
           02 MNCODICV  PIC X.                                          00001640
           02 MNCODICO  PIC X(7).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCFAMGA   PIC X.                                          00001670
           02 MCFAMGC   PIC X.                                          00001680
           02 MCFAMGP   PIC X.                                          00001690
           02 MCFAMGH   PIC X.                                          00001700
           02 MCFAMGV   PIC X.                                          00001710
           02 MCFAMGO   PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MQTEA     PIC X.                                          00001740
           02 MQTEC     PIC X.                                          00001750
           02 MQTEP     PIC X.                                          00001760
           02 MQTEH     PIC X.                                          00001770
           02 MQTEV     PIC X.                                          00001780
           02 MQTEO     PIC X(4).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MQSAA     PIC X.                                          00001810
           02 MQSAC     PIC X.                                          00001820
           02 MQSAP     PIC X.                                          00001830
           02 MQSAH     PIC X.                                          00001840
           02 MQSAV     PIC X.                                          00001850
           02 MQSAO     PIC X(3).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNSOCA    PIC X.                                          00001880
           02 MNSOCC    PIC X.                                          00001890
           02 MNSOCP    PIC X.                                          00001900
           02 MNSOCH    PIC X.                                          00001910
           02 MNSOCV    PIC X.                                          00001920
           02 MNSOCO    PIC X(3).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MNMAGA    PIC X.                                          00001950
           02 MNMAGC    PIC X.                                          00001960
           02 MNMAGP    PIC X.                                          00001970
           02 MNMAGH    PIC X.                                          00001980
           02 MNMAGV    PIC X.                                          00001990
           02 MNMAGO    PIC X(3).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MDEFFETA  PIC X.                                          00002020
           02 MDEFFETC  PIC X.                                          00002030
           02 MDEFFETP  PIC X.                                          00002040
           02 MDEFFETH  PIC X.                                          00002050
           02 MDEFFETV  PIC X.                                          00002060
           02 MDEFFETO  PIC X(10).                                      00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCHEFPA   PIC X.                                          00002090
           02 MCHEFPC   PIC X.                                          00002100
           02 MCHEFPP   PIC X.                                          00002110
           02 MCHEFPH   PIC X.                                          00002120
           02 MCHEFPV   PIC X.                                          00002130
           02 MCHEFPO   PIC X(5).                                       00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MCLERA    PIC X.                                          00002160
           02 MCLERC    PIC X.                                          00002170
           02 MCLERP    PIC X.                                          00002180
           02 MCLERH    PIC X.                                          00002190
           02 MCLERV    PIC X.                                          00002200
           02 MCLERO    PIC X(5).                                       00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCFONCA   PIC X.                                          00002230
           02 MCFONCC   PIC X.                                          00002240
           02 MCFONCP   PIC X.                                          00002250
           02 MCFONCH   PIC X.                                          00002260
           02 MCFONCV   PIC X.                                          00002270
           02 MCFONCO   PIC X(3).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLIBERRA  PIC X.                                          00002300
           02 MLIBERRC  PIC X.                                          00002310
           02 MLIBERRP  PIC X.                                          00002320
           02 MLIBERRH  PIC X.                                          00002330
           02 MLIBERRV  PIC X.                                          00002340
           02 MLIBERRO  PIC X(78).                                      00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCODTRAA  PIC X.                                          00002370
           02 MCODTRAC  PIC X.                                          00002380
           02 MCODTRAP  PIC X.                                          00002390
           02 MCODTRAH  PIC X.                                          00002400
           02 MCODTRAV  PIC X.                                          00002410
           02 MCODTRAO  PIC X(4).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCICSA    PIC X.                                          00002440
           02 MCICSC    PIC X.                                          00002450
           02 MCICSP    PIC X.                                          00002460
           02 MCICSH    PIC X.                                          00002470
           02 MCICSV    PIC X.                                          00002480
           02 MCICSO    PIC X(5).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNETNAMA  PIC X.                                          00002510
           02 MNETNAMC  PIC X.                                          00002520
           02 MNETNAMP  PIC X.                                          00002530
           02 MNETNAMH  PIC X.                                          00002540
           02 MNETNAMV  PIC X.                                          00002550
           02 MNETNAMO  PIC X(8).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MSCREENA  PIC X.                                          00002580
           02 MSCREENC  PIC X.                                          00002590
           02 MSCREENP  PIC X.                                          00002600
           02 MSCREENH  PIC X.                                          00002610
           02 MSCREENV  PIC X.                                          00002620
           02 MSCREENO  PIC X(4).                                       00002630
                                                                                
