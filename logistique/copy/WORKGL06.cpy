      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * LONGUEUR  20                                                            
             03 COMM-GF47GL06-APPLI.                                            
                04 COMM-GF47GL06-NSEMAINE.                                      
                   05 COMM-GF47GL06-ANNEE.                                      
                      06 COMM-GF47GL06-SS     PIC X(2).                         
                      06 COMM-GF47GL06-AA     PIC X(2).                         
                   05 COMM-GF47GL06-NUMSEM    PIC X(2).                         
                04 COMM-GF47GL06-NSOCDEPOT    PIC X(3).                         
                04 COMM-GF47GL06-NDEPOT       PIC X(3).                         
             03 COMM-GF47GL06-CODLANG         PIC X(2).                         
             03 COMM-GF47GL06-CODPIC          PIC X(2).                         
             03 FILLER                        PIC X(4).                         
                                                                                
