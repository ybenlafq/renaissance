      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IIE515 AU 07/01/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,03,PD,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,07,BI,A,                          *        
      *                           33,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IIE515.                                                        
            05 NOMETAT-IIE515           PIC X(6) VALUE 'IIE515'.                
            05 RUPTURES-IIE515.                                                 
           10 IIE515-CLIEU              PIC X(03).                      007  003
           10 IIE515-SSLIEU             PIC X(03).                      010  003
           10 IIE515-LTRT               PIC X(05).                      013  005
           10 IIE515-WSEQFAM            PIC S9(05)      COMP-3.         018  003
           10 IIE515-CMARQ              PIC X(05).                      021  005
           10 IIE515-NCODIC             PIC X(07).                      026  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IIE515-SEQUENCE           PIC S9(04) COMP.                033  002
      *--                                                                       
           10 IIE515-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IIE515.                                                   
           10 IIE515-CFAM               PIC X(05).                      035  005
           10 IIE515-LREF               PIC X(20).                      040  020
           10 IIE515-NHSC               PIC X(07).                      060  007
           10 IIE515-NHSS               PIC X(07).                      067  007
           10 IIE515-ECART              PIC S9(05)      COMP-3.         074  003
            05 FILLER                      PIC X(436).                          
                                                                                
