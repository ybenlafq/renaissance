      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE TL1000                       
      ******************************************************************        
      *                                                                         
       CLEF-TL1000             SECTION.                                         
      *                                                                         
           MOVE 'RVTL1000          '       TO   TABLE-NAME.                     
           MOVE 'TL1000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-TL1000. EXIT.                                                   
                EJECT                                                           
                                                                                
