      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIN81   EIN81                                              00000020
      ***************************************************************** 00000030
       01   EIN81I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEINVL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MDATEINVL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEINVF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDATEINVI      PIC X(10).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCIETEL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MSOCIETEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSOCIETEF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MSOCIETEI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIEUI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPEI    PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSLIEUL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSSLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSSLIEUF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSSLIEUI  PIC X(3).                                       00000370
      * LIB STATUT1                                                     00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LSTAT1L   COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 LSTAT1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 LSTAT1F   PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 LSTAT1I   PIC X(3).                                       00000420
      * LIB STATUT2                                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LSTAT2L   COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 LSTAT2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 LSTAT2F   PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 LSTAT2I   PIC X(3).                                       00000470
      * LIG ARTICLE                                                     00000480
           02 LIGARTI OCCURS   12 TIMES .                               00000490
      * NCODIC                                                          00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNCODICI     PIC X(7).                                  00000540
      * STATUT                                                          00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MSTATUTI     PIC X.                                     00000590
      * STK INVENTAI                                                    00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKINL   COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MQSTOCKINL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQSTOCKINF   PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MQSTOCKINI   PIC X(6).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKREL   COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MQSTOCKREL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQSTOCKREF   PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MQSTOCKREI   PIC X(6).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENTL    COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MCOMMENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENTF    PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MCOMMENTI    PIC X(20).                                 00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKAPL   COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MQSTOCKAPL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQSTOCKAPF   PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MQSTOCKAPI   PIC X(6).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MZONCMDI  PIC X(15).                                      00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MLIBERRI  PIC X(58).                                      00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MCODTRAI  PIC X(4).                                       00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MCICSI    PIC X(5).                                       00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MNETNAMI  PIC X(8).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MSCREENI  PIC X(4).                                       00001000
      ***************************************************************** 00001010
      * SDF: EIN81   EIN81                                              00001020
      ***************************************************************** 00001030
       01   EIN81O REDEFINES EIN81I.                                    00001040
           02 FILLER    PIC X(12).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MDATJOUA  PIC X.                                          00001070
           02 MDATJOUC  PIC X.                                          00001080
           02 MDATJOUP  PIC X.                                          00001090
           02 MDATJOUH  PIC X.                                          00001100
           02 MDATJOUV  PIC X.                                          00001110
           02 MDATJOUO  PIC X(10).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MTIMJOUA  PIC X.                                          00001140
           02 MTIMJOUC  PIC X.                                          00001150
           02 MTIMJOUP  PIC X.                                          00001160
           02 MTIMJOUH  PIC X.                                          00001170
           02 MTIMJOUV  PIC X.                                          00001180
           02 MTIMJOUO  PIC X(5).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MPAGEA    PIC X.                                          00001210
           02 MPAGEC    PIC X.                                          00001220
           02 MPAGEP    PIC X.                                          00001230
           02 MPAGEH    PIC X.                                          00001240
           02 MPAGEV    PIC X.                                          00001250
           02 MPAGEO    PIC X(3).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDATEINVA      PIC X.                                     00001280
           02 MDATEINVC PIC X.                                          00001290
           02 MDATEINVP PIC X.                                          00001300
           02 MDATEINVH PIC X.                                          00001310
           02 MDATEINVV PIC X.                                          00001320
           02 MDATEINVO      PIC X(10).                                 00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MSOCIETEA      PIC X.                                     00001350
           02 MSOCIETEC PIC X.                                          00001360
           02 MSOCIETEP PIC X.                                          00001370
           02 MSOCIETEH PIC X.                                          00001380
           02 MSOCIETEV PIC X.                                          00001390
           02 MSOCIETEO      PIC X(3).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLIEUA    PIC X.                                          00001420
           02 MLIEUC    PIC X.                                          00001430
           02 MLIEUP    PIC X.                                          00001440
           02 MLIEUH    PIC X.                                          00001450
           02 MLIEUV    PIC X.                                          00001460
           02 MLIEUO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MTYPEA    PIC X.                                          00001490
           02 MTYPEC    PIC X.                                          00001500
           02 MTYPEP    PIC X.                                          00001510
           02 MTYPEH    PIC X.                                          00001520
           02 MTYPEV    PIC X.                                          00001530
           02 MTYPEO    PIC X(4).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MSSLIEUA  PIC X.                                          00001560
           02 MSSLIEUC  PIC X.                                          00001570
           02 MSSLIEUP  PIC X.                                          00001580
           02 MSSLIEUH  PIC X.                                          00001590
           02 MSSLIEUV  PIC X.                                          00001600
           02 MSSLIEUO  PIC X(3).                                       00001610
      * LIB STATUT1                                                     00001620
           02 FILLER    PIC X(2).                                       00001630
           02 LSTAT1A   PIC X.                                          00001640
           02 LSTAT1C   PIC X.                                          00001650
           02 LSTAT1P   PIC X.                                          00001660
           02 LSTAT1H   PIC X.                                          00001670
           02 LSTAT1V   PIC X.                                          00001680
           02 LSTAT1O   PIC X(3).                                       00001690
      * LIB STATUT2                                                     00001700
           02 FILLER    PIC X(2).                                       00001710
           02 LSTAT2A   PIC X.                                          00001720
           02 LSTAT2C   PIC X.                                          00001730
           02 LSTAT2P   PIC X.                                          00001740
           02 LSTAT2H   PIC X.                                          00001750
           02 LSTAT2V   PIC X.                                          00001760
           02 LSTAT2O   PIC X(3).                                       00001770
      * LIG ARTICLE                                                     00001780
           02 LIGARTO OCCURS   12 TIMES .                               00001790
      * NCODIC                                                          00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MNCODICA     PIC X.                                     00001820
             03 MNCODICC     PIC X.                                     00001830
             03 MNCODICP     PIC X.                                     00001840
             03 MNCODICH     PIC X.                                     00001850
             03 MNCODICV     PIC X.                                     00001860
             03 MNCODICO     PIC X(7).                                  00001870
      * STATUT                                                          00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MSTATUTA     PIC X.                                     00001900
             03 MSTATUTC     PIC X.                                     00001910
             03 MSTATUTP     PIC X.                                     00001920
             03 MSTATUTH     PIC X.                                     00001930
             03 MSTATUTV     PIC X.                                     00001940
             03 MSTATUTO     PIC X.                                     00001950
      * STK INVENTAI                                                    00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MQSTOCKINA   PIC X.                                     00001980
             03 MQSTOCKINC   PIC X.                                     00001990
             03 MQSTOCKINP   PIC X.                                     00002000
             03 MQSTOCKINH   PIC X.                                     00002010
             03 MQSTOCKINV   PIC X.                                     00002020
             03 MQSTOCKINO   PIC ------.                                00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MQSTOCKREA   PIC X.                                     00002050
             03 MQSTOCKREC   PIC X.                                     00002060
             03 MQSTOCKREP   PIC X.                                     00002070
             03 MQSTOCKREH   PIC X.                                     00002080
             03 MQSTOCKREV   PIC X.                                     00002090
             03 MQSTOCKREO   PIC ------.                                00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MCOMMENTA    PIC X.                                     00002120
             03 MCOMMENTC    PIC X.                                     00002130
             03 MCOMMENTP    PIC X.                                     00002140
             03 MCOMMENTH    PIC X.                                     00002150
             03 MCOMMENTV    PIC X.                                     00002160
             03 MCOMMENTO    PIC X(20).                                 00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MQSTOCKAPA   PIC X.                                     00002190
             03 MQSTOCKAPC   PIC X.                                     00002200
             03 MQSTOCKAPP   PIC X.                                     00002210
             03 MQSTOCKAPH   PIC X.                                     00002220
             03 MQSTOCKAPV   PIC X.                                     00002230
             03 MQSTOCKAPO   PIC ------.                                00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MZONCMDA  PIC X.                                          00002260
           02 MZONCMDC  PIC X.                                          00002270
           02 MZONCMDP  PIC X.                                          00002280
           02 MZONCMDH  PIC X.                                          00002290
           02 MZONCMDV  PIC X.                                          00002300
           02 MZONCMDO  PIC X(15).                                      00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLIBERRA  PIC X.                                          00002330
           02 MLIBERRC  PIC X.                                          00002340
           02 MLIBERRP  PIC X.                                          00002350
           02 MLIBERRH  PIC X.                                          00002360
           02 MLIBERRV  PIC X.                                          00002370
           02 MLIBERRO  PIC X(58).                                      00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MCODTRAA  PIC X.                                          00002400
           02 MCODTRAC  PIC X.                                          00002410
           02 MCODTRAP  PIC X.                                          00002420
           02 MCODTRAH  PIC X.                                          00002430
           02 MCODTRAV  PIC X.                                          00002440
           02 MCODTRAO  PIC X(4).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MCICSA    PIC X.                                          00002470
           02 MCICSC    PIC X.                                          00002480
           02 MCICSP    PIC X.                                          00002490
           02 MCICSH    PIC X.                                          00002500
           02 MCICSV    PIC X.                                          00002510
           02 MCICSO    PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MNETNAMA  PIC X.                                          00002540
           02 MNETNAMC  PIC X.                                          00002550
           02 MNETNAMP  PIC X.                                          00002560
           02 MNETNAMH  PIC X.                                          00002570
           02 MNETNAMV  PIC X.                                          00002580
           02 MNETNAMO  PIC X(8).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MSCREENA  PIC X.                                          00002610
           02 MSCREENC  PIC X.                                          00002620
           02 MSCREENP  PIC X.                                          00002630
           02 MSCREENH  PIC X.                                          00002640
           02 MSCREENV  PIC X.                                          00002650
           02 MSCREENO  PIC X(4).                                       00002660
                                                                                
