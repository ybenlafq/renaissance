      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM621 AU 17/03/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM621.                                                        
            05 NOMETAT-JRM621           PIC X(6) VALUE 'JRM621'.                
            05 RUPTURES-JRM621.                                                 
           10 JRM621-CHEFPROD           PIC X(05).                      007  005
           10 JRM621-CRMGROUP           PIC X(05).                      012  005
           10 JRM621-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 JRM621-LAGREGATED         PIC X(20).                      020  020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM621-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 JRM621-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM621.                                                   
           10 JRM621-DPM1               PIC X(04).                      042  004
           10 JRM621-DPM2               PIC X(04).                      046  004
           10 JRM621-DPM3               PIC X(04).                      050  004
           10 JRM621-DPM4               PIC X(04).                      054  004
           10 JRM621-DPM5               PIC X(04).                      058  004
           10 JRM621-DPM6               PIC X(04).                      062  004
           10 JRM621-DPM7               PIC X(04).                      066  004
           10 JRM621-DPM8               PIC X(04).                      070  004
           10 JRM621-DPRESTOCK1         PIC X(04).                      074  004
           10 JRM621-DPRESTOCK2         PIC X(04).                      078  004
           10 JRM621-DPV1               PIC X(04).                      082  004
           10 JRM621-DPV2               PIC X(04).                      086  004
           10 JRM621-DPV3               PIC X(04).                      090  004
           10 JRM621-DV1SD              PIC X(04).                      094  004
           10 JRM621-DV1SF              PIC X(04).                      098  004
           10 JRM621-DV2SD              PIC X(04).                      102  004
           10 JRM621-DV2SF              PIC X(04).                      106  004
           10 JRM621-DV3SD              PIC X(04).                      110  004
           10 JRM621-DV3SF              PIC X(04).                      114  004
           10 JRM621-DV4SD              PIC X(04).                      118  004
           10 JRM621-DV4SF              PIC X(04).                      122  004
           10 JRM621-LCHEFPROD          PIC X(20).                      126  020
           10 JRM621-LFAM               PIC X(20).                      146  020
           10 JRM621-QPV1               PIC X(06).                      166  006
           10 JRM621-QPV2               PIC X(06).                      172  006
           10 JRM621-QPV3               PIC X(06).                      178  006
           10 JRM621-WEDIT              PIC X(01).                      184  001
           10 JRM621-WPV1               PIC X(01).                      185  001
           10 JRM621-WPV2               PIC X(01).                      186  001
           10 JRM621-WPV3               PIC X(01).                      187  001
           10 JRM621-WWPV1              PIC X(01).                      188  001
           10 JRM621-WWPV2              PIC X(01).                      189  001
           10 JRM621-WWPV3              PIC X(01).                      190  001
           10 JRM621-QPM-S1             PIC S9(07)      COMP-3.         191  004
           10 JRM621-QPM-S2             PIC S9(07)      COMP-3.         195  004
           10 JRM621-QPM-S3             PIC S9(07)      COMP-3.         199  004
           10 JRM621-QPM-S4             PIC S9(07)      COMP-3.         203  004
           10 JRM621-QPM-S5             PIC S9(07)      COMP-3.         207  004
           10 JRM621-QPM-S6             PIC S9(07)      COMP-3.         211  004
           10 JRM621-QPM-S7             PIC S9(07)      COMP-3.         215  004
           10 JRM621-QPM-S8             PIC S9(07)      COMP-3.         219  004
           10 JRM621-QPVS-1             PIC S9(07)      COMP-3.         223  004
           10 JRM621-QPVS-2             PIC S9(07)      COMP-3.         227  004
           10 JRM621-QPVS-3             PIC S9(07)      COMP-3.         231  004
           10 JRM621-QPVS-4             PIC S9(07)      COMP-3.         235  004
           10 JRM621-QV1S               PIC S9(07)      COMP-3.         239  004
           10 JRM621-QV2S               PIC S9(07)      COMP-3.         243  004
           10 JRM621-QV3S               PIC S9(07)      COMP-3.         247  004
           10 JRM621-QV4S               PIC S9(07)      COMP-3.         251  004
            05 FILLER                      PIC X(258).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM621-LONG           PIC S9(4)   COMP  VALUE +254.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM621-LONG           PIC S9(4) COMP-5  VALUE +254.           
                                                                                
      *}                                                                        
