      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU70   EMU70                                              00000020
      ***************************************************************** 00000030
       01   EMU70I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * commande                                                        00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MZONCMDI  PIC X(15).                                      00000180
      * soc entrepot                                                    00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNSOCENTI      PIC X(3).                                  00000230
      * lieu entrepot                                                   00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPENTL      COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MNDEPENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPENTF      PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MNDEPENTI      PIC X(3).                                  00000280
      * volume minimum                                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVOLUMEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MVOLUMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MVOLUMEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MVOLUMEI  PIC X(6).                                       00000330
      * nb de pieces minimum                                            00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPMINL  COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MNBPMINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPMINF  PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MNBPMINI  PIC X(3).                                       00000380
      * code soc de la vente                                            00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVTL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNSOCVTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCVTF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNSOCVTI  PIC X(3).                                       00000430
      * code mag de la vente                                            00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MNMAGI    PIC X(3).                                       00000480
      * date debut periode                                              00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATDEBI  PIC X(10).                                      00000530
      * date fin periode                                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MDATFINI  PIC X(10).                                      00000580
      * code societe vente                                              00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVTEL      COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MNSOCVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCVTEF      PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNSOCVTEI      PIC X(3).                                  00000630
      * code magasin vente                                              00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGVTEL      COMP PIC S9(4).                            00000650
      *--                                                                       
           02 MNMAGVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGVTEF      PIC X.                                     00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNMAGVTEI      PIC X(3).                                  00000680
      * numero de vente                                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNVENTEI  PIC X(7).                                       00000730
      * numero de mutation                                              00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTL    COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MNMUTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMUTF    PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MNMUTI    PIC X(7).                                       00000780
      * ddeliv de la mut                                                00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDELIVL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MDDELIVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDELIVF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MDDELIVI  PIC X(10).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MLIBERRI  PIC X(78).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MCODTRAI  PIC X(4).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCICSI    PIC X(5).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MNETNAMI  PIC X(8).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MSCREENI  PIC X(4).                                       00001030
      ***************************************************************** 00001040
      * SDF: EMU70   EMU70                                              00001050
      ***************************************************************** 00001060
       01   EMU70O REDEFINES EMU70I.                                    00001070
           02 FILLER    PIC X(12).                                      00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MDATJOUA  PIC X.                                          00001100
           02 MDATJOUC  PIC X.                                          00001110
           02 MDATJOUP  PIC X.                                          00001120
           02 MDATJOUH  PIC X.                                          00001130
           02 MDATJOUV  PIC X.                                          00001140
           02 MDATJOUO  PIC X(10).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MTIMJOUA  PIC X.                                          00001170
           02 MTIMJOUC  PIC X.                                          00001180
           02 MTIMJOUP  PIC X.                                          00001190
           02 MTIMJOUH  PIC X.                                          00001200
           02 MTIMJOUV  PIC X.                                          00001210
           02 MTIMJOUO  PIC X(5).                                       00001220
      * commande                                                        00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MZONCMDA  PIC X.                                          00001250
           02 MZONCMDC  PIC X.                                          00001260
           02 MZONCMDP  PIC X.                                          00001270
           02 MZONCMDH  PIC X.                                          00001280
           02 MZONCMDV  PIC X.                                          00001290
           02 MZONCMDO  PIC X(15).                                      00001300
      * soc entrepot                                                    00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNSOCENTA      PIC X.                                     00001330
           02 MNSOCENTC PIC X.                                          00001340
           02 MNSOCENTP PIC X.                                          00001350
           02 MNSOCENTH PIC X.                                          00001360
           02 MNSOCENTV PIC X.                                          00001370
           02 MNSOCENTO      PIC X(3).                                  00001380
      * lieu entrepot                                                   00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNDEPENTA      PIC X.                                     00001410
           02 MNDEPENTC PIC X.                                          00001420
           02 MNDEPENTP PIC X.                                          00001430
           02 MNDEPENTH PIC X.                                          00001440
           02 MNDEPENTV PIC X.                                          00001450
           02 MNDEPENTO      PIC X(3).                                  00001460
      * volume minimum                                                  00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MVOLUMEA  PIC X.                                          00001490
           02 MVOLUMEC  PIC X.                                          00001500
           02 MVOLUMEP  PIC X.                                          00001510
           02 MVOLUMEH  PIC X.                                          00001520
           02 MVOLUMEV  PIC X.                                          00001530
           02 MVOLUMEO  PIC X(6).                                       00001540
      * nb de pieces minimum                                            00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNBPMINA  PIC X.                                          00001570
           02 MNBPMINC  PIC X.                                          00001580
           02 MNBPMINP  PIC X.                                          00001590
           02 MNBPMINH  PIC X.                                          00001600
           02 MNBPMINV  PIC X.                                          00001610
           02 MNBPMINO  PIC X(3).                                       00001620
      * code soc de la vente                                            00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNSOCVTA  PIC X.                                          00001650
           02 MNSOCVTC  PIC X.                                          00001660
           02 MNSOCVTP  PIC X.                                          00001670
           02 MNSOCVTH  PIC X.                                          00001680
           02 MNSOCVTV  PIC X.                                          00001690
           02 MNSOCVTO  PIC X(3).                                       00001700
      * code mag de la vente                                            00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNMAGA    PIC X.                                          00001730
           02 MNMAGC    PIC X.                                          00001740
           02 MNMAGP    PIC X.                                          00001750
           02 MNMAGH    PIC X.                                          00001760
           02 MNMAGV    PIC X.                                          00001770
           02 MNMAGO    PIC X(3).                                       00001780
      * date debut periode                                              00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MDATDEBA  PIC X.                                          00001810
           02 MDATDEBC  PIC X.                                          00001820
           02 MDATDEBP  PIC X.                                          00001830
           02 MDATDEBH  PIC X.                                          00001840
           02 MDATDEBV  PIC X.                                          00001850
           02 MDATDEBO  PIC X(10).                                      00001860
      * date fin periode                                                00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATFINA  PIC X.                                          00001890
           02 MDATFINC  PIC X.                                          00001900
           02 MDATFINP  PIC X.                                          00001910
           02 MDATFINH  PIC X.                                          00001920
           02 MDATFINV  PIC X.                                          00001930
           02 MDATFINO  PIC X(10).                                      00001940
      * code societe vente                                              00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNSOCVTEA      PIC X.                                     00001970
           02 MNSOCVTEC PIC X.                                          00001980
           02 MNSOCVTEP PIC X.                                          00001990
           02 MNSOCVTEH PIC X.                                          00002000
           02 MNSOCVTEV PIC X.                                          00002010
           02 MNSOCVTEO      PIC X(3).                                  00002020
      * code magasin vente                                              00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNMAGVTEA      PIC X.                                     00002050
           02 MNMAGVTEC PIC X.                                          00002060
           02 MNMAGVTEP PIC X.                                          00002070
           02 MNMAGVTEH PIC X.                                          00002080
           02 MNMAGVTEV PIC X.                                          00002090
           02 MNMAGVTEO      PIC X(3).                                  00002100
      * numero de vente                                                 00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNVENTEA  PIC X.                                          00002130
           02 MNVENTEC  PIC X.                                          00002140
           02 MNVENTEP  PIC X.                                          00002150
           02 MNVENTEH  PIC X.                                          00002160
           02 MNVENTEV  PIC X.                                          00002170
           02 MNVENTEO  PIC X(7).                                       00002180
      * numero de mutation                                              00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MNMUTA    PIC X.                                          00002210
           02 MNMUTC    PIC X.                                          00002220
           02 MNMUTP    PIC X.                                          00002230
           02 MNMUTH    PIC X.                                          00002240
           02 MNMUTV    PIC X.                                          00002250
           02 MNMUTO    PIC X(7).                                       00002260
      * ddeliv de la mut                                                00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MDDELIVA  PIC X.                                          00002290
           02 MDDELIVC  PIC X.                                          00002300
           02 MDDELIVP  PIC X.                                          00002310
           02 MDDELIVH  PIC X.                                          00002320
           02 MDDELIVV  PIC X.                                          00002330
           02 MDDELIVO  PIC X(10).                                      00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLIBERRA  PIC X.                                          00002360
           02 MLIBERRC  PIC X.                                          00002370
           02 MLIBERRP  PIC X.                                          00002380
           02 MLIBERRH  PIC X.                                          00002390
           02 MLIBERRV  PIC X.                                          00002400
           02 MLIBERRO  PIC X(78).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCODTRAA  PIC X.                                          00002430
           02 MCODTRAC  PIC X.                                          00002440
           02 MCODTRAP  PIC X.                                          00002450
           02 MCODTRAH  PIC X.                                          00002460
           02 MCODTRAV  PIC X.                                          00002470
           02 MCODTRAO  PIC X(4).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCICSA    PIC X.                                          00002500
           02 MCICSC    PIC X.                                          00002510
           02 MCICSP    PIC X.                                          00002520
           02 MCICSH    PIC X.                                          00002530
           02 MCICSV    PIC X.                                          00002540
           02 MCICSO    PIC X(5).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNETNAMA  PIC X.                                          00002570
           02 MNETNAMC  PIC X.                                          00002580
           02 MNETNAMP  PIC X.                                          00002590
           02 MNETNAMH  PIC X.                                          00002600
           02 MNETNAMV  PIC X.                                          00002610
           02 MNETNAMO  PIC X(8).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MSCREENA  PIC X.                                          00002640
           02 MSCREENC  PIC X.                                          00002650
           02 MSCREENP  PIC X.                                          00002660
           02 MSCREENH  PIC X.                                          00002670
           02 MSCREENV  PIC X.                                          00002680
           02 MSCREENO  PIC X(4).                                       00002690
                                                                                
