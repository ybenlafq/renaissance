      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00001000
      *  TRANSACTION GD00 : GESTION DE DESTOCKAGE                  *    00002000
      *                                                                 00002100
      * COMMAREA SPECIFIQUE PRG TGD56 (TGD00 -> MENU)    TR: GD00  *    00002200
      *          REMISE EN STOCK GLOBALE                           *    00002300
      *          GESTION DES EMPLACEMENTS : EDITION ETIQUETTES     *    00002400
      *                                     INDIVIDUALISEES        *    00002500
      *  MODIF SM LE 24101990 : SUPPRESSION ZONE LIBRE QUI NE      *    00002600
      *  SERT A RIEN ET DONT LA LONGUEUR NE SERAIT DE TOUTE FACON  *    00002700
      *  PLUS EGALE A 383 MAIS 283                                 *    00002710
      *                                                                 00002720
        03 COMM-GD56-APPLI REDEFINES COMM-GD00-FILLER.                  00002800
      *                                                                 00002900
      *------------------------------ ZONE DONNEES LIGNES ARTICLES      00003000
           04 COMM-GD56-TABART.                                         00003100
      *                                                                 00004000
                05 COMM-GD56-LIGART OCCURS 12.                          00005000
      *------------------------------ CODE ARTICLE                      00009000
                   06 COMM-GD56-NCODIC            PIC X(7).             00010000
      *------------------------------ CODE FAMILLE                      00020000
                   06 COMM-GD56-CFAM              PIC X(5).             00030000
      *------------------------------ CODE MARQUE                       00040000
                   06 COMM-GD56-CMARQ             PIC X(5).             00050000
      *------------------------------ LIBELLE REFERENCE FOURNISSEUR     00060000
                   06 COMM-GD56-LREFFOURN         PIC X(20).            00070000
      *------------------------------ LIBELLE EMBALLAGE                 00080000
                   06 COMM-GD56-LEMBALLAGE        PIC X(50).            00090000
      *------------------------------ CODE EMPLACEMENT DESTINATION      00150000
                   06 COMM-GD56-CEMP.                                   00160003
      *------------------------------                  DEST    1        00170000
      *                             AIRE (SOL) ALLEE (RACK) ZONE (VRAC) 00180000
      *------------------------------                                   00190000
                      08 COMM-GD56-CEMP1             PIC X(02).         00200003
      *------------------------------                  DEST    2        00210000
      *                         COTE (SOL) NIVEAU (RACK) SECTEUR (VRAC) 00220000
      *------------------------------                                   00230000
                      08 COMM-GD56-CEMP2             PIC X(02).         00240003
      *------------------------------                  DEST    3        00250000
      *             NPOSITION (SOL) POSITION (RACK) SOUS-SECTEUR (VRAC) 00260000
      *------------------------------                                   00270000
                      08 COMM-GD56-CEMP3             PIC X(03).         00280003
      *------------------------------ COTE DE PREHENSION ( SOL / VRAC)  00290000
      *                                                ( P = PROFONDEUR)00300000
      *                                                ( L = LARGEUR   )00310000
      *                                                ( H = HAUTEUR   )00320000
                   06 COMM-GD56-CCOTEHOLD         PIC X(01).            00330000
      *------------------------------ CONDITIONNEMENT                   00340000
                   06 COMM-GD56-QCONDT            PIC 9(05).            00350000
      *------------------------------ QUANTITE EN STOCK ECRAN           00360000
                   06 COMM-GD56-QSTOCK            PIC 9(05).            00370000
      *------------------------------ QUANTITE EN STOCK ANCIENNE        00371000
                   06 COMM-GD56-QSTOCKA           PIC 9(05).            00372000
      *------------------------------ QUANTITE EN STOCK NOUVELLE        00373000
                   06 COMM-GD56-QSTOCKN           PIC 9(05).            00374000
      *------------------------------ NUMERO PROFONDEUR DS EMPLACEMENT  00380000
                   06 COMM-GD56-CCRAN             PIC 9(03).            00390000
      *------------------------------ LARGEUR PRODUIT                   00400000
                   06 COMM-GD56-QLARGEUR          PIC 9(03).            00410000
      *------------------------------ PROFONDEUR PRODUIT                00410100
                   06 COMM-GD56-QPROFONDEUR       PIC 9(03).            00410200
      *------------------------------ HAUTEUR PRODUIT                   00410300
                   06 COMM-GD56-QHAUTEUR          PIC 9(03).            00410400
      *------------------------------ NOMBRE PRODUITS DS LARGEUR MAILLE 00411000
                   06 COMM-GD56-QNBRANMAIL        PIC 9(03).            00412000
      *------------------------------ NOMBRE PRODUITS DS PROFOND.MAILLE 00420000
                   06 COMM-GD56-QNBPRNMAIL        PIC 9(03).            00430000
      *------------------------------ NOMBRE NIVEAUX GERBAGES           00440000
                   06 COMM-GD56-QNBNIVGERB        PIC 9(03).            00450000
      *------------------------------ NOMBRE PRODUITS RACK              00451000
                   06 COMM-GD56-QNBPRACK          PIC 9(03).            00452000
      *------------------------------ CAPACITE                          00460000
                   06 COMM-GD56-QCAPACITE         PIC 9(05).            00470000
      *------------------------------ CODE SPECIFICITE                  00480000
                   06 COMM-GD56-CSPECIFSTK        PIC X(01).            00490000
      *------------------------------ CODE CONTENEUR                    00500000
                   06 COMM-GD56-CCONTENEUR        PIC X(01).            00510000
      *------------------------------ CODE ZONE ACCESSIBILITE           00511000
                   06 COMM-GD56-CZONEACCES        PIC X(01).            00512000
      *------------------------------ CLASSE DE ROTATION ABC            00511000
                   06 COMM-GD56-CLASSE            PIC X(01).            00512000
      *------------------------------ DATE D ATTRIBUTION                00520000
                   06 COMM-GD56-DATTR             PIC X(08).            00530000
      *------------------------------ EMPLACEMENT OK                    00531003
                   06 COMM-GD56-EMPLCT-OK         PIC X(01).            00532003
      *------------------------------ MESSAGE CAPACITES RACK            00533003
                   06 COMM-GD56-MESS-RACK         PIC X(60).            00534005
      *------------------------------ MESSAGE CAPACITES RACK            00535005
                   06 COMM-GD56-EDIT-OK           PIC X(01).            00536005
      *------------------------------ ZONE ATTRRIBUTS                   00540000
           04 COMM-GD56-ATTR   OCCURS 400         PIC X(01).            00550001
      ***************************************************************** 00580000
                                                                                
