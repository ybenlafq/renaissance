      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESR10   ESR10                                              00000020
      ***************************************************************** 00000030
       01   ESR10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSEPL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWSEPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWSEPF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWSEPI    PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEFL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWPAGEFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWPAGEFF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWPAGEFI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFNCODICL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MFNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MFNCODICF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MFNCODICI      PIC X(7).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MFLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MFLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MFLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MFLIEUI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSLIEUI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSNCODICL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MSNCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSNCODICF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSNCODICI      PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDJJDEBL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MSDJJDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDJJDEBF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSDJJDEBI      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDMMDEBL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MSDMMDEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDMMDEBF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSDMMDEBI      PIC X(2).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDAADEBL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MSDAADEBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDAADEBF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MSDAADEBI      PIC X(4).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDJJFINL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MSDJJFINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDJJFINF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSDJJFINI      PIC X(2).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDMMFINL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MSDMMFINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDMMFINF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSDMMFINI      PIC X(2).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDAAFINL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MSDAAFINL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDAAFINF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSDAAFINI      PIC X(4).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCLIEUGL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MSCLIEUGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSCLIEUGF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCLIEUGI      PIC X(5).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCTRAITL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MSCTRAITL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSCTRAITF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCTRAITI      PIC X(5).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCTYPHSL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MSCTYPHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSCTYPHSF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCTYPHSI      PIC X(5).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSLIBREL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MSLIBREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSLIBREF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MSLIBREI  PIC X(10).                                      00000810
           02 M108I OCCURS   10 TIMES .                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MACTL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MACTF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MACTI   PIC X.                                          00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLIEUI  PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNCODICI     PIC X(7).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJJDEBL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MDJJDEBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJJDEBF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MDJJDEBI     PIC X(2).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMMDEBL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MDMMDEBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDMMDEBF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDMMDEBI     PIC X(2).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDAADEBL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MDAADEBL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDAADEBF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDAADEBI     PIC X(4).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJJFINL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MDJJFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDJJFINF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDJJFINI     PIC X(2).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMMFINL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MDMMFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDMMFINF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDMMFINI     PIC X(2).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDAAFINL     COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MDAAFINL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDAAFINF     PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MDAAFINI     PIC X(4).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCLIEUGL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MCLIEUGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCLIEUGF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCLIEUGI     PIC X(5).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRAITL     COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MCTRAITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTRAITF     PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MCTRAITI     PIC X(5).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTYPHSL     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MCTYPHSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTYPHSF     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MCTYPHSI     PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBREL      COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MLIBREL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBREF      PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MLIBREI      PIC X(10).                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MZONCMDI  PIC X(15).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(58).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * SDF: ESR10   ESR10                                              00001600
      ***************************************************************** 00001610
       01   ESR10O REDEFINES ESR10I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MWPAGEA   PIC X.                                          00001790
           02 MWPAGEC   PIC X.                                          00001800
           02 MWPAGEP   PIC X.                                          00001810
           02 MWPAGEH   PIC X.                                          00001820
           02 MWPAGEV   PIC X.                                          00001830
           02 MWPAGEO   PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MWSEPA    PIC X.                                          00001860
           02 MWSEPC    PIC X.                                          00001870
           02 MWSEPP    PIC X.                                          00001880
           02 MWSEPH    PIC X.                                          00001890
           02 MWSEPV    PIC X.                                          00001900
           02 MWSEPO    PIC X.                                          00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MWPAGEFA  PIC X.                                          00001930
           02 MWPAGEFC  PIC X.                                          00001940
           02 MWPAGEFP  PIC X.                                          00001950
           02 MWPAGEFH  PIC X.                                          00001960
           02 MWPAGEFV  PIC X.                                          00001970
           02 MWPAGEFO  PIC X(3).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MFNCODICA      PIC X.                                     00002000
           02 MFNCODICC PIC X.                                          00002010
           02 MFNCODICP PIC X.                                          00002020
           02 MFNCODICH PIC X.                                          00002030
           02 MFNCODICV PIC X.                                          00002040
           02 MFNCODICO      PIC X(7).                                  00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MFLIEUA   PIC X.                                          00002070
           02 MFLIEUC   PIC X.                                          00002080
           02 MFLIEUP   PIC X.                                          00002090
           02 MFLIEUH   PIC X.                                          00002100
           02 MFLIEUV   PIC X.                                          00002110
           02 MFLIEUO   PIC X(3).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MSLIEUA   PIC X.                                          00002140
           02 MSLIEUC   PIC X.                                          00002150
           02 MSLIEUP   PIC X.                                          00002160
           02 MSLIEUH   PIC X.                                          00002170
           02 MSLIEUV   PIC X.                                          00002180
           02 MSLIEUO   PIC X(3).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MSNCODICA      PIC X.                                     00002210
           02 MSNCODICC PIC X.                                          00002220
           02 MSNCODICP PIC X.                                          00002230
           02 MSNCODICH PIC X.                                          00002240
           02 MSNCODICV PIC X.                                          00002250
           02 MSNCODICO      PIC X(7).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MSDJJDEBA      PIC X.                                     00002280
           02 MSDJJDEBC PIC X.                                          00002290
           02 MSDJJDEBP PIC X.                                          00002300
           02 MSDJJDEBH PIC X.                                          00002310
           02 MSDJJDEBV PIC X.                                          00002320
           02 MSDJJDEBO      PIC X(2).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MSDMMDEBA      PIC X.                                     00002350
           02 MSDMMDEBC PIC X.                                          00002360
           02 MSDMMDEBP PIC X.                                          00002370
           02 MSDMMDEBH PIC X.                                          00002380
           02 MSDMMDEBV PIC X.                                          00002390
           02 MSDMMDEBO      PIC X(2).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MSDAADEBA      PIC X.                                     00002420
           02 MSDAADEBC PIC X.                                          00002430
           02 MSDAADEBP PIC X.                                          00002440
           02 MSDAADEBH PIC X.                                          00002450
           02 MSDAADEBV PIC X.                                          00002460
           02 MSDAADEBO      PIC X(4).                                  00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MSDJJFINA      PIC X.                                     00002490
           02 MSDJJFINC PIC X.                                          00002500
           02 MSDJJFINP PIC X.                                          00002510
           02 MSDJJFINH PIC X.                                          00002520
           02 MSDJJFINV PIC X.                                          00002530
           02 MSDJJFINO      PIC X(2).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MSDMMFINA      PIC X.                                     00002560
           02 MSDMMFINC PIC X.                                          00002570
           02 MSDMMFINP PIC X.                                          00002580
           02 MSDMMFINH PIC X.                                          00002590
           02 MSDMMFINV PIC X.                                          00002600
           02 MSDMMFINO      PIC X(2).                                  00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MSDAAFINA      PIC X.                                     00002630
           02 MSDAAFINC PIC X.                                          00002640
           02 MSDAAFINP PIC X.                                          00002650
           02 MSDAAFINH PIC X.                                          00002660
           02 MSDAAFINV PIC X.                                          00002670
           02 MSDAAFINO      PIC X(4).                                  00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MSCLIEUGA      PIC X.                                     00002700
           02 MSCLIEUGC PIC X.                                          00002710
           02 MSCLIEUGP PIC X.                                          00002720
           02 MSCLIEUGH PIC X.                                          00002730
           02 MSCLIEUGV PIC X.                                          00002740
           02 MSCLIEUGO      PIC X(5).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MSCTRAITA      PIC X.                                     00002770
           02 MSCTRAITC PIC X.                                          00002780
           02 MSCTRAITP PIC X.                                          00002790
           02 MSCTRAITH PIC X.                                          00002800
           02 MSCTRAITV PIC X.                                          00002810
           02 MSCTRAITO      PIC X(5).                                  00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSCTYPHSA      PIC X.                                     00002840
           02 MSCTYPHSC PIC X.                                          00002850
           02 MSCTYPHSP PIC X.                                          00002860
           02 MSCTYPHSH PIC X.                                          00002870
           02 MSCTYPHSV PIC X.                                          00002880
           02 MSCTYPHSO      PIC X(5).                                  00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MSLIBREA  PIC X.                                          00002910
           02 MSLIBREC  PIC X.                                          00002920
           02 MSLIBREP  PIC X.                                          00002930
           02 MSLIBREH  PIC X.                                          00002940
           02 MSLIBREV  PIC X.                                          00002950
           02 MSLIBREO  PIC X(10).                                      00002960
           02 M108O OCCURS   10 TIMES .                                 00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MACTA   PIC X.                                          00002990
             03 MACTC   PIC X.                                          00003000
             03 MACTP   PIC X.                                          00003010
             03 MACTH   PIC X.                                          00003020
             03 MACTV   PIC X.                                          00003030
             03 MACTO   PIC X.                                          00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MLIEUA  PIC X.                                          00003060
             03 MLIEUC  PIC X.                                          00003070
             03 MLIEUP  PIC X.                                          00003080
             03 MLIEUH  PIC X.                                          00003090
             03 MLIEUV  PIC X.                                          00003100
             03 MLIEUO  PIC X(3).                                       00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MNCODICA     PIC X.                                     00003130
             03 MNCODICC     PIC X.                                     00003140
             03 MNCODICP     PIC X.                                     00003150
             03 MNCODICH     PIC X.                                     00003160
             03 MNCODICV     PIC X.                                     00003170
             03 MNCODICO     PIC X(7).                                  00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MDJJDEBA     PIC X.                                     00003200
             03 MDJJDEBC     PIC X.                                     00003210
             03 MDJJDEBP     PIC X.                                     00003220
             03 MDJJDEBH     PIC X.                                     00003230
             03 MDJJDEBV     PIC X.                                     00003240
             03 MDJJDEBO     PIC X(2).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MDMMDEBA     PIC X.                                     00003270
             03 MDMMDEBC     PIC X.                                     00003280
             03 MDMMDEBP     PIC X.                                     00003290
             03 MDMMDEBH     PIC X.                                     00003300
             03 MDMMDEBV     PIC X.                                     00003310
             03 MDMMDEBO     PIC X(2).                                  00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MDAADEBA     PIC X.                                     00003340
             03 MDAADEBC     PIC X.                                     00003350
             03 MDAADEBP     PIC X.                                     00003360
             03 MDAADEBH     PIC X.                                     00003370
             03 MDAADEBV     PIC X.                                     00003380
             03 MDAADEBO     PIC X(4).                                  00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MDJJFINA     PIC X.                                     00003410
             03 MDJJFINC     PIC X.                                     00003420
             03 MDJJFINP     PIC X.                                     00003430
             03 MDJJFINH     PIC X.                                     00003440
             03 MDJJFINV     PIC X.                                     00003450
             03 MDJJFINO     PIC X(2).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MDMMFINA     PIC X.                                     00003480
             03 MDMMFINC     PIC X.                                     00003490
             03 MDMMFINP     PIC X.                                     00003500
             03 MDMMFINH     PIC X.                                     00003510
             03 MDMMFINV     PIC X.                                     00003520
             03 MDMMFINO     PIC X(2).                                  00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MDAAFINA     PIC X.                                     00003550
             03 MDAAFINC     PIC X.                                     00003560
             03 MDAAFINP     PIC X.                                     00003570
             03 MDAAFINH     PIC X.                                     00003580
             03 MDAAFINV     PIC X.                                     00003590
             03 MDAAFINO     PIC X(4).                                  00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MCLIEUGA     PIC X.                                     00003620
             03 MCLIEUGC     PIC X.                                     00003630
             03 MCLIEUGP     PIC X.                                     00003640
             03 MCLIEUGH     PIC X.                                     00003650
             03 MCLIEUGV     PIC X.                                     00003660
             03 MCLIEUGO     PIC X(5).                                  00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MCTRAITA     PIC X.                                     00003690
             03 MCTRAITC     PIC X.                                     00003700
             03 MCTRAITP     PIC X.                                     00003710
             03 MCTRAITH     PIC X.                                     00003720
             03 MCTRAITV     PIC X.                                     00003730
             03 MCTRAITO     PIC X(5).                                  00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MCTYPHSA     PIC X.                                     00003760
             03 MCTYPHSC     PIC X.                                     00003770
             03 MCTYPHSP     PIC X.                                     00003780
             03 MCTYPHSH     PIC X.                                     00003790
             03 MCTYPHSV     PIC X.                                     00003800
             03 MCTYPHSO     PIC X(5).                                  00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MLIBREA      PIC X.                                     00003830
             03 MLIBREC PIC X.                                          00003840
             03 MLIBREP PIC X.                                          00003850
             03 MLIBREH PIC X.                                          00003860
             03 MLIBREV PIC X.                                          00003870
             03 MLIBREO      PIC X(10).                                 00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MZONCMDA  PIC X.                                          00003900
           02 MZONCMDC  PIC X.                                          00003910
           02 MZONCMDP  PIC X.                                          00003920
           02 MZONCMDH  PIC X.                                          00003930
           02 MZONCMDV  PIC X.                                          00003940
           02 MZONCMDO  PIC X(15).                                      00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLIBERRA  PIC X.                                          00003970
           02 MLIBERRC  PIC X.                                          00003980
           02 MLIBERRP  PIC X.                                          00003990
           02 MLIBERRH  PIC X.                                          00004000
           02 MLIBERRV  PIC X.                                          00004010
           02 MLIBERRO  PIC X(58).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCODTRAA  PIC X.                                          00004040
           02 MCODTRAC  PIC X.                                          00004050
           02 MCODTRAP  PIC X.                                          00004060
           02 MCODTRAH  PIC X.                                          00004070
           02 MCODTRAV  PIC X.                                          00004080
           02 MCODTRAO  PIC X(4).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCICSA    PIC X.                                          00004110
           02 MCICSC    PIC X.                                          00004120
           02 MCICSP    PIC X.                                          00004130
           02 MCICSH    PIC X.                                          00004140
           02 MCICSV    PIC X.                                          00004150
           02 MCICSO    PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MNETNAMA  PIC X.                                          00004180
           02 MNETNAMC  PIC X.                                          00004190
           02 MNETNAMP  PIC X.                                          00004200
           02 MNETNAMH  PIC X.                                          00004210
           02 MNETNAMV  PIC X.                                          00004220
           02 MNETNAMO  PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSCREENA  PIC X.                                          00004250
           02 MSCREENC  PIC X.                                          00004260
           02 MSCREENP  PIC X.                                          00004270
           02 MSCREENH  PIC X.                                          00004280
           02 MSCREENV  PIC X.                                          00004290
           02 MSCREENO  PIC X(4).                                       00004300
                                                                                
