      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEF26   EEF26                                              00000020
      ***************************************************************** 00000030
       01   EEF26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRAITL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTRAITF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCTRAITI  PIC X(5).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRAITL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLTRAITL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTRAITF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLTRAITI  PIC X(20).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUHSL      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 MNLIEUHSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNLIEUHSF      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNLIEUHSI      PIC X(3).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNHSL     COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNHSL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNHSF     PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNHSI     PIC X(7).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNCODICI  PIC X(7).                                       00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MCFAMI    PIC X(5).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCMARQI   PIC X(5).                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLREFI    PIC X(20).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTIERSL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MLTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTIERSF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MLTIERSI  PIC X(20).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTIERSF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCTIERSI  PIC X(5).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLNOMI    PIC X(20).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENVOIL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MNENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNENVOIF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNENVOII  PIC X(7).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDENVOIL  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MDENVOIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDENVOIF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MDENVOII  PIC X(10).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MD1RELL   COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MD1RELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MD1RELF   PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MD1RELI   PIC X(10).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MD2RELL   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MD2RELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MD2RELF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MD2RELI   PIC X(10).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MD3RELL   COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MD3RELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MD3RELF   PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MD3RELI   PIC X(10).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTAIREL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCTAIREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTAIREF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCTAIREI  PIC X(15).                                      00000830
      * MESSAGE ERREUR                                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MLIBERRI  PIC X(78).                                      00000880
      * CODE TRANSACTION                                                00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCODTRAI  PIC X(4).                                       00000930
      * CICS DE TRAVAIL                                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      * NETNAME                                                         00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      * CODE TERMINAL                                                   00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MSCREENI  PIC X(5).                                       00001080
      ***************************************************************** 00001090
      * SDF: EEF26   EEF26                                              00001100
      ***************************************************************** 00001110
       01   EEF26O REDEFINES EEF26I.                                    00001120
           02 FILLER    PIC X(12).                                      00001130
      * DATE DU JOUR                                                    00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MDATJOUA  PIC X.                                          00001160
           02 MDATJOUC  PIC X.                                          00001170
           02 MDATJOUP  PIC X.                                          00001180
           02 MDATJOUH  PIC X.                                          00001190
           02 MDATJOUV  PIC X.                                          00001200
           02 MDATJOUO  PIC X(10).                                      00001210
      * HEURE                                                           00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MTIMJOUA  PIC X.                                          00001240
           02 MTIMJOUC  PIC X.                                          00001250
           02 MTIMJOUP  PIC X.                                          00001260
           02 MTIMJOUH  PIC X.                                          00001270
           02 MTIMJOUV  PIC X.                                          00001280
           02 MTIMJOUO  PIC X(5).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCTRAITA  PIC X.                                          00001310
           02 MCTRAITC  PIC X.                                          00001320
           02 MCTRAITP  PIC X.                                          00001330
           02 MCTRAITH  PIC X.                                          00001340
           02 MCTRAITV  PIC X.                                          00001350
           02 MCTRAITO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLTRAITA  PIC X.                                          00001380
           02 MLTRAITC  PIC X.                                          00001390
           02 MLTRAITP  PIC X.                                          00001400
           02 MLTRAITH  PIC X.                                          00001410
           02 MLTRAITV  PIC X.                                          00001420
           02 MLTRAITO  PIC X(20).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNLIEUHSA      PIC X.                                     00001450
           02 MNLIEUHSC PIC X.                                          00001460
           02 MNLIEUHSP PIC X.                                          00001470
           02 MNLIEUHSH PIC X.                                          00001480
           02 MNLIEUHSV PIC X.                                          00001490
           02 MNLIEUHSO      PIC X(3).                                  00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNHSA     PIC X.                                          00001520
           02 MNHSC     PIC X.                                          00001530
           02 MNHSP     PIC X.                                          00001540
           02 MNHSH     PIC X.                                          00001550
           02 MNHSV     PIC X.                                          00001560
           02 MNHSO     PIC X(7).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNCODICA  PIC X.                                          00001590
           02 MNCODICC  PIC X.                                          00001600
           02 MNCODICP  PIC X.                                          00001610
           02 MNCODICH  PIC X.                                          00001620
           02 MNCODICV  PIC X.                                          00001630
           02 MNCODICO  PIC X(7).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCFAMA    PIC X.                                          00001660
           02 MCFAMC    PIC X.                                          00001670
           02 MCFAMP    PIC X.                                          00001680
           02 MCFAMH    PIC X.                                          00001690
           02 MCFAMV    PIC X.                                          00001700
           02 MCFAMO    PIC X(5).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCMARQA   PIC X.                                          00001730
           02 MCMARQC   PIC X.                                          00001740
           02 MCMARQP   PIC X.                                          00001750
           02 MCMARQH   PIC X.                                          00001760
           02 MCMARQV   PIC X.                                          00001770
           02 MCMARQO   PIC X(5).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MLREFA    PIC X.                                          00001800
           02 MLREFC    PIC X.                                          00001810
           02 MLREFP    PIC X.                                          00001820
           02 MLREFH    PIC X.                                          00001830
           02 MLREFV    PIC X.                                          00001840
           02 MLREFO    PIC X(20).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLTIERSA  PIC X.                                          00001870
           02 MLTIERSC  PIC X.                                          00001880
           02 MLTIERSP  PIC X.                                          00001890
           02 MLTIERSH  PIC X.                                          00001900
           02 MLTIERSV  PIC X.                                          00001910
           02 MLTIERSO  PIC X(20).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCTIERSA  PIC X.                                          00001940
           02 MCTIERSC  PIC X.                                          00001950
           02 MCTIERSP  PIC X.                                          00001960
           02 MCTIERSH  PIC X.                                          00001970
           02 MCTIERSV  PIC X.                                          00001980
           02 MCTIERSO  PIC X(5).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLNOMA    PIC X.                                          00002010
           02 MLNOMC    PIC X.                                          00002020
           02 MLNOMP    PIC X.                                          00002030
           02 MLNOMH    PIC X.                                          00002040
           02 MLNOMV    PIC X.                                          00002050
           02 MLNOMO    PIC X(20).                                      00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNENVOIA  PIC X.                                          00002080
           02 MNENVOIC  PIC X.                                          00002090
           02 MNENVOIP  PIC X.                                          00002100
           02 MNENVOIH  PIC X.                                          00002110
           02 MNENVOIV  PIC X.                                          00002120
           02 MNENVOIO  PIC X(7).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MDENVOIA  PIC X.                                          00002150
           02 MDENVOIC  PIC X.                                          00002160
           02 MDENVOIP  PIC X.                                          00002170
           02 MDENVOIH  PIC X.                                          00002180
           02 MDENVOIV  PIC X.                                          00002190
           02 MDENVOIO  PIC X(10).                                      00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MD1RELA   PIC X.                                          00002220
           02 MD1RELC   PIC X.                                          00002230
           02 MD1RELP   PIC X.                                          00002240
           02 MD1RELH   PIC X.                                          00002250
           02 MD1RELV   PIC X.                                          00002260
           02 MD1RELO   PIC X(10).                                      00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MD2RELA   PIC X.                                          00002290
           02 MD2RELC   PIC X.                                          00002300
           02 MD2RELP   PIC X.                                          00002310
           02 MD2RELH   PIC X.                                          00002320
           02 MD2RELV   PIC X.                                          00002330
           02 MD2RELO   PIC X(10).                                      00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MD3RELA   PIC X.                                          00002360
           02 MD3RELC   PIC X.                                          00002370
           02 MD3RELP   PIC X.                                          00002380
           02 MD3RELH   PIC X.                                          00002390
           02 MD3RELV   PIC X.                                          00002400
           02 MD3RELO   PIC X(10).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCTAIREA  PIC X.                                          00002430
           02 MCTAIREC  PIC X.                                          00002440
           02 MCTAIREP  PIC X.                                          00002450
           02 MCTAIREH  PIC X.                                          00002460
           02 MCTAIREV  PIC X.                                          00002470
           02 MCTAIREO  PIC X(15).                                      00002480
      * MESSAGE ERREUR                                                  00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MLIBERRA  PIC X.                                          00002510
           02 MLIBERRC  PIC X.                                          00002520
           02 MLIBERRP  PIC X.                                          00002530
           02 MLIBERRH  PIC X.                                          00002540
           02 MLIBERRV  PIC X.                                          00002550
           02 MLIBERRO  PIC X(78).                                      00002560
      * CODE TRANSACTION                                                00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MCODTRAA  PIC X.                                          00002590
           02 MCODTRAC  PIC X.                                          00002600
           02 MCODTRAP  PIC X.                                          00002610
           02 MCODTRAH  PIC X.                                          00002620
           02 MCODTRAV  PIC X.                                          00002630
           02 MCODTRAO  PIC X(4).                                       00002640
      * CICS DE TRAVAIL                                                 00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MCICSA    PIC X.                                          00002670
           02 MCICSC    PIC X.                                          00002680
           02 MCICSP    PIC X.                                          00002690
           02 MCICSH    PIC X.                                          00002700
           02 MCICSV    PIC X.                                          00002710
           02 MCICSO    PIC X(5).                                       00002720
      * NETNAME                                                         00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
      * CODE TERMINAL                                                   00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSCREENA  PIC X.                                          00002830
           02 MSCREENC  PIC X.                                          00002840
           02 MSCREENP  PIC X.                                          00002850
           02 MSCREENH  PIC X.                                          00002860
           02 MSCREENV  PIC X.                                          00002870
           02 MSCREENO  PIC X(5).                                       00002880
                                                                                
