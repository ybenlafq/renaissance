      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TGS76                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-GS76-LONG            PIC S9(3) COMP-3 VALUE 15.           00060002
       01  TS-GS76-DONNEES.                                             00070001
              10 TS-GS76-SEL       PIC X.                               00080001
              10 TS-GS76-NSOCO     PIC X(3).                            00090001
              10 TS-GS76-NLIEUO    PIC X(3).                            00100001
              10 TS-GS76-NSOCD     PIC X(3).                            00110001
              10 TS-GS76-NLIEUD    PIC X(3).                            00120001
              10 TS-GS76-MODIF     PIC X.                               00130001
              10 TS-GS76-EXIST     PIC X.                               00140002
                                                                                
