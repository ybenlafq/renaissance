      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU27   EMU27                                              00000020
      ***************************************************************** 00000030
       01   EMU27I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(8).                                       00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
           02 M5I OCCURS   11 TIMES .                                   00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTATIL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNMUTATIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNMUTATIF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNMUTATII    PIC X(7).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDEBSAIL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MDDEBSAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDDEBSAIF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDDEBSAII    PIC X(5).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINSAIL    COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MDFINSAIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINSAIF    PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MDFINSAII    PIC X(5).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDESTOCL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MDDESTOCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDDESTOCF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MDDESTOCI    PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCHARGTL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MDCHARGTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDCHARGTF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MDCHARGTI    PIC X(5).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMUTATIL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDMUTATIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDMUTATIF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDMUTATII    PIC X(5).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCENTL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNSOCENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCENTF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNSOCENTI    PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNDEPOTI     PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSELARTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCSELARTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCSELARTF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCSELARTI    PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDHEUREML    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDHEUREML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDHEUREMF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDHEUREMI    PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMINML      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDMINML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDMINMF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDMINMI      PIC X(2).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBCAML      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNBCAML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBCAMF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNBCAMI      PIC X(2).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUOTAM3L    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MQUOTAM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQUOTAM3F    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MQUOTAM3I    PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBAL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MNBAL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNBAF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNBAI   PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPROL   COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MPROL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPROF   PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MPROI   PIC X.                                          00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(12).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(61).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EMU27   EMU27                                              00001160
      ***************************************************************** 00001170
       01   EMU27O REDEFINES EMU27I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(8).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNSOCA    PIC X.                                          00001420
           02 MNSOCC    PIC X.                                          00001430
           02 MNSOCP    PIC X.                                          00001440
           02 MNSOCH    PIC X.                                          00001450
           02 MNSOCV    PIC X.                                          00001460
           02 MNSOCO    PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MNLIEUA   PIC X.                                          00001490
           02 MNLIEUC   PIC X.                                          00001500
           02 MNLIEUP   PIC X.                                          00001510
           02 MNLIEUH   PIC X.                                          00001520
           02 MNLIEUV   PIC X.                                          00001530
           02 MNLIEUO   PIC X(3).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MLLIEUA   PIC X.                                          00001560
           02 MLLIEUC   PIC X.                                          00001570
           02 MLLIEUP   PIC X.                                          00001580
           02 MLLIEUH   PIC X.                                          00001590
           02 MLLIEUV   PIC X.                                          00001600
           02 MLLIEUO   PIC X(20).                                      00001610
           02 M5O OCCURS   11 TIMES .                                   00001620
             03 FILLER       PIC X(2).                                  00001630
             03 MNMUTATIA    PIC X.                                     00001640
             03 MNMUTATIC    PIC X.                                     00001650
             03 MNMUTATIP    PIC X.                                     00001660
             03 MNMUTATIH    PIC X.                                     00001670
             03 MNMUTATIV    PIC X.                                     00001680
             03 MNMUTATIO    PIC X(7).                                  00001690
             03 FILLER       PIC X(2).                                  00001700
             03 MDDEBSAIA    PIC X.                                     00001710
             03 MDDEBSAIC    PIC X.                                     00001720
             03 MDDEBSAIP    PIC X.                                     00001730
             03 MDDEBSAIH    PIC X.                                     00001740
             03 MDDEBSAIV    PIC X.                                     00001750
             03 MDDEBSAIO    PIC X(5).                                  00001760
             03 FILLER       PIC X(2).                                  00001770
             03 MDFINSAIA    PIC X.                                     00001780
             03 MDFINSAIC    PIC X.                                     00001790
             03 MDFINSAIP    PIC X.                                     00001800
             03 MDFINSAIH    PIC X.                                     00001810
             03 MDFINSAIV    PIC X.                                     00001820
             03 MDFINSAIO    PIC X(5).                                  00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MDDESTOCA    PIC X.                                     00001850
             03 MDDESTOCC    PIC X.                                     00001860
             03 MDDESTOCP    PIC X.                                     00001870
             03 MDDESTOCH    PIC X.                                     00001880
             03 MDDESTOCV    PIC X.                                     00001890
             03 MDDESTOCO    PIC X(5).                                  00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MDCHARGTA    PIC X.                                     00001920
             03 MDCHARGTC    PIC X.                                     00001930
             03 MDCHARGTP    PIC X.                                     00001940
             03 MDCHARGTH    PIC X.                                     00001950
             03 MDCHARGTV    PIC X.                                     00001960
             03 MDCHARGTO    PIC X(5).                                  00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MDMUTATIA    PIC X.                                     00001990
             03 MDMUTATIC    PIC X.                                     00002000
             03 MDMUTATIP    PIC X.                                     00002010
             03 MDMUTATIH    PIC X.                                     00002020
             03 MDMUTATIV    PIC X.                                     00002030
             03 MDMUTATIO    PIC X(5).                                  00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MNSOCENTA    PIC X.                                     00002060
             03 MNSOCENTC    PIC X.                                     00002070
             03 MNSOCENTP    PIC X.                                     00002080
             03 MNSOCENTH    PIC X.                                     00002090
             03 MNSOCENTV    PIC X.                                     00002100
             03 MNSOCENTO    PIC X(3).                                  00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MNDEPOTA     PIC X.                                     00002130
             03 MNDEPOTC     PIC X.                                     00002140
             03 MNDEPOTP     PIC X.                                     00002150
             03 MNDEPOTH     PIC X.                                     00002160
             03 MNDEPOTV     PIC X.                                     00002170
             03 MNDEPOTO     PIC X(3).                                  00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MCSELARTA    PIC X.                                     00002200
             03 MCSELARTC    PIC X.                                     00002210
             03 MCSELARTP    PIC X.                                     00002220
             03 MCSELARTH    PIC X.                                     00002230
             03 MCSELARTV    PIC X.                                     00002240
             03 MCSELARTO    PIC X(5).                                  00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MDHEUREMA    PIC X.                                     00002270
             03 MDHEUREMC    PIC X.                                     00002280
             03 MDHEUREMP    PIC X.                                     00002290
             03 MDHEUREMH    PIC X.                                     00002300
             03 MDHEUREMV    PIC X.                                     00002310
             03 MDHEUREMO    PIC X(2).                                  00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MDMINMA      PIC X.                                     00002340
             03 MDMINMC PIC X.                                          00002350
             03 MDMINMP PIC X.                                          00002360
             03 MDMINMH PIC X.                                          00002370
             03 MDMINMV PIC X.                                          00002380
             03 MDMINMO      PIC X(2).                                  00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MNBCAMA      PIC X.                                     00002410
             03 MNBCAMC PIC X.                                          00002420
             03 MNBCAMP PIC X.                                          00002430
             03 MNBCAMH PIC X.                                          00002440
             03 MNBCAMV PIC X.                                          00002450
             03 MNBCAMO      PIC X(2).                                  00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MQUOTAM3A    PIC X.                                     00002480
             03 MQUOTAM3C    PIC X.                                     00002490
             03 MQUOTAM3P    PIC X.                                     00002500
             03 MQUOTAM3H    PIC X.                                     00002510
             03 MQUOTAM3V    PIC X.                                     00002520
             03 MQUOTAM3O    PIC X(5).                                  00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MNBAA   PIC X.                                          00002550
             03 MNBAC   PIC X.                                          00002560
             03 MNBAP   PIC X.                                          00002570
             03 MNBAH   PIC X.                                          00002580
             03 MNBAV   PIC X.                                          00002590
             03 MNBAO   PIC X(5).                                       00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MPROA   PIC X.                                          00002620
             03 MPROC   PIC X.                                          00002630
             03 MPROP   PIC X.                                          00002640
             03 MPROH   PIC X.                                          00002650
             03 MPROV   PIC X.                                          00002660
             03 MPROO   PIC X.                                          00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(12).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(61).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
