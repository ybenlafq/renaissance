      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU10   EMU10                                              00000020
      ***************************************************************** 00000030
       01   EMU10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCLIEL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCLIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCLIEF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCLIEI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMUTATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNMUTATF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMUTATI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTATL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDMUTATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMUTATF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDMUTATI  PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCENF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCENI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPOTI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELAL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCSELAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSELAF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCSELAI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODIMPL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCODIMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODIMPF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCODIMPI  PIC X(4).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJOURL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDJOURF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDJOURI   PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL8L  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBEL8L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL8F  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBEL8I  PIC X(55).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL9L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBEL9L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL9F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBEL9I  PIC X(55).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL10L      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLIBEL10L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBEL10F      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIBEL10I      PIC X(55).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL5L  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIBEL5L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL5F  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIBEL5I  PIC X(41).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL6L  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBEL6L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL6F  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBEL6I  PIC X(44).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL7L  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBEL7L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBEL7F  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBEL7I  PIC X(24).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBEL11L      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLIBEL11L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBEL11F      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLIBEL11I      PIC X(55).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(78).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCODTRAI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCICSI    PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(4).                                       00001010
      ***************************************************************** 00001020
      * SDF: EMU10   EMU10                                              00001030
      ***************************************************************** 00001040
       01   EMU10O REDEFINES EMU10I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MTIMJOUA  PIC X.                                          00001150
           02 MTIMJOUC  PIC X.                                          00001160
           02 MTIMJOUP  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUV  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MZONCMDA  PIC X.                                          00001220
           02 MZONCMDC  PIC X.                                          00001230
           02 MZONCMDP  PIC X.                                          00001240
           02 MZONCMDH  PIC X.                                          00001250
           02 MZONCMDV  PIC X.                                          00001260
           02 MZONCMDO  PIC X(15).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNSOCLIEA      PIC X.                                     00001290
           02 MNSOCLIEC PIC X.                                          00001300
           02 MNSOCLIEP PIC X.                                          00001310
           02 MNSOCLIEH PIC X.                                          00001320
           02 MNSOCLIEV PIC X.                                          00001330
           02 MNSOCLIEO      PIC X(3).                                  00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MNLIEUA   PIC X.                                          00001360
           02 MNLIEUC   PIC X.                                          00001370
           02 MNLIEUP   PIC X.                                          00001380
           02 MNLIEUH   PIC X.                                          00001390
           02 MNLIEUV   PIC X.                                          00001400
           02 MNLIEUO   PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNMUTATA  PIC X.                                          00001430
           02 MNMUTATC  PIC X.                                          00001440
           02 MNMUTATP  PIC X.                                          00001450
           02 MNMUTATH  PIC X.                                          00001460
           02 MNMUTATV  PIC X.                                          00001470
           02 MNMUTATO  PIC X(7).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDMUTATA  PIC X.                                          00001500
           02 MDMUTATC  PIC X.                                          00001510
           02 MDMUTATP  PIC X.                                          00001520
           02 MDMUTATH  PIC X.                                          00001530
           02 MDMUTATV  PIC X.                                          00001540
           02 MDMUTATO  PIC X(6).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNSOCENA  PIC X.                                          00001570
           02 MNSOCENC  PIC X.                                          00001580
           02 MNSOCENP  PIC X.                                          00001590
           02 MNSOCENH  PIC X.                                          00001600
           02 MNSOCENV  PIC X.                                          00001610
           02 MNSOCENO  PIC X(3).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNDEPOTA  PIC X.                                          00001640
           02 MNDEPOTC  PIC X.                                          00001650
           02 MNDEPOTP  PIC X.                                          00001660
           02 MNDEPOTH  PIC X.                                          00001670
           02 MNDEPOTV  PIC X.                                          00001680
           02 MNDEPOTO  PIC X(3).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCSELAA   PIC X.                                          00001710
           02 MCSELAC   PIC X.                                          00001720
           02 MCSELAP   PIC X.                                          00001730
           02 MCSELAH   PIC X.                                          00001740
           02 MCSELAV   PIC X.                                          00001750
           02 MCSELAO   PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCODIMPA  PIC X.                                          00001780
           02 MCODIMPC  PIC X.                                          00001790
           02 MCODIMPP  PIC X.                                          00001800
           02 MCODIMPH  PIC X.                                          00001810
           02 MCODIMPV  PIC X.                                          00001820
           02 MCODIMPO  PIC X(4).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDJOURA   PIC X.                                          00001850
           02 MDJOURC   PIC X.                                          00001860
           02 MDJOURP   PIC X.                                          00001870
           02 MDJOURH   PIC X.                                          00001880
           02 MDJOURV   PIC X.                                          00001890
           02 MDJOURO   PIC X(6).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLIBEL8A  PIC X.                                          00001920
           02 MLIBEL8C  PIC X.                                          00001930
           02 MLIBEL8P  PIC X.                                          00001940
           02 MLIBEL8H  PIC X.                                          00001950
           02 MLIBEL8V  PIC X.                                          00001960
           02 MLIBEL8O  PIC X(55).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBEL9A  PIC X.                                          00001990
           02 MLIBEL9C  PIC X.                                          00002000
           02 MLIBEL9P  PIC X.                                          00002010
           02 MLIBEL9H  PIC X.                                          00002020
           02 MLIBEL9V  PIC X.                                          00002030
           02 MLIBEL9O  PIC X(55).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLIBEL10A      PIC X.                                     00002060
           02 MLIBEL10C PIC X.                                          00002070
           02 MLIBEL10P PIC X.                                          00002080
           02 MLIBEL10H PIC X.                                          00002090
           02 MLIBEL10V PIC X.                                          00002100
           02 MLIBEL10O      PIC X(55).                                 00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLIBEL5A  PIC X.                                          00002130
           02 MLIBEL5C  PIC X.                                          00002140
           02 MLIBEL5P  PIC X.                                          00002150
           02 MLIBEL5H  PIC X.                                          00002160
           02 MLIBEL5V  PIC X.                                          00002170
           02 MLIBEL5O  PIC X(41).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLIBEL6A  PIC X.                                          00002200
           02 MLIBEL6C  PIC X.                                          00002210
           02 MLIBEL6P  PIC X.                                          00002220
           02 MLIBEL6H  PIC X.                                          00002230
           02 MLIBEL6V  PIC X.                                          00002240
           02 MLIBEL6O  PIC X(44).                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLIBEL7A  PIC X.                                          00002270
           02 MLIBEL7C  PIC X.                                          00002280
           02 MLIBEL7P  PIC X.                                          00002290
           02 MLIBEL7H  PIC X.                                          00002300
           02 MLIBEL7V  PIC X.                                          00002310
           02 MLIBEL7O  PIC X(24).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MLIBEL11A      PIC X.                                     00002340
           02 MLIBEL11C PIC X.                                          00002350
           02 MLIBEL11P PIC X.                                          00002360
           02 MLIBEL11H PIC X.                                          00002370
           02 MLIBEL11V PIC X.                                          00002380
           02 MLIBEL11O      PIC X(55).                                 00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLIBERRA  PIC X.                                          00002410
           02 MLIBERRC  PIC X.                                          00002420
           02 MLIBERRP  PIC X.                                          00002430
           02 MLIBERRH  PIC X.                                          00002440
           02 MLIBERRV  PIC X.                                          00002450
           02 MLIBERRO  PIC X(78).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MCODTRAA  PIC X.                                          00002480
           02 MCODTRAC  PIC X.                                          00002490
           02 MCODTRAP  PIC X.                                          00002500
           02 MCODTRAH  PIC X.                                          00002510
           02 MCODTRAV  PIC X.                                          00002520
           02 MCODTRAO  PIC X(4).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCICSA    PIC X.                                          00002550
           02 MCICSC    PIC X.                                          00002560
           02 MCICSP    PIC X.                                          00002570
           02 MCICSH    PIC X.                                          00002580
           02 MCICSV    PIC X.                                          00002590
           02 MCICSO    PIC X(5).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNETNAMA  PIC X.                                          00002620
           02 MNETNAMC  PIC X.                                          00002630
           02 MNETNAMP  PIC X.                                          00002640
           02 MNETNAMH  PIC X.                                          00002650
           02 MNETNAMV  PIC X.                                          00002660
           02 MNETNAMO  PIC X(8).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSCREENA  PIC X.                                          00002690
           02 MSCREENC  PIC X.                                          00002700
           02 MSCREENP  PIC X.                                          00002710
           02 MSCREENH  PIC X.                                          00002720
           02 MSCREENV  PIC X.                                          00002730
           02 MSCREENO  PIC X(4).                                       00002740
                                                                                
