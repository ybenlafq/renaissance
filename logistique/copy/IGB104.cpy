      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGB104 AU 10/06/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,24,BI,A,                          *        
      *                           37,05,BI,A,                          *        
      *                           42,03,BI,A,                          *        
      *                           45,03,BI,A,                          *        
      *                           48,03,BI,A,                          *        
      *                           51,07,BI,A,                          *        
      *                           58,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGB104.                                                        
            05 NOMETAT-IGB104           PIC X(6) VALUE 'IGB104'.                
            05 RUPTURES-IGB104.                                                 
           10 IGB104-NSOCENTR           PIC X(03).                      007  003
           10 IGB104-NDEPOT             PIC X(03).                      010  003
           10 IGB104-DDESTOCK           PIC X(24).                      013  024
           10 IGB104-CSELART            PIC X(05).                      037  005
           10 IGB104-NSOCGRP            PIC X(03).                      042  003
           10 IGB104-NSOCIETE           PIC X(03).                      045  003
           10 IGB104-NLIEU              PIC X(03).                      048  003
           10 IGB104-NMUTATION          PIC X(07).                      051  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGB104-SEQUENCE           PIC S9(04) COMP.                058  002
      *--                                                                       
           10 IGB104-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGB104.                                                   
           10 IGB104-LFILIALE           PIC X(13).                      060  013
           10 IGB104-LLIEU              PIC X(20).                      073  020
           10 IGB104-QDEMANDEE          PIC S9(07)      COMP-3.         093  004
           10 IGB104-QMONTANT           PIC S9(11)      COMP-3.         097  006
           10 IGB104-QVOLUME            PIC S9(04)V9(3) COMP-3.         103  004
            05 FILLER                      PIC X(406).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGB104-LONG           PIC S9(4)   COMP  VALUE +106.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGB104-LONG           PIC S9(4) COMP-5  VALUE +106.           
                                                                                
      *}                                                                        
