      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM03                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 61.              00060000
       01  TS-DONNEES.                                                  00070000
              10 TS-CFAM         PIC X(5).                              00080000
              10 TS-LFAM         PIC X(20).                             00090000
              10 TS-INDISP       PIC X(5).                              00100000
              10 TS-POND         PIC X(4).                              00110000
              10 TS-INDICE       PIC X.                                 00120000
              10 TS-SEUIL        PIC X(6).                              00121000
              10 TS-PVM          PIC X(4).                              00122000
              10 TS-QLSMAX       PIC X(5).                              00122100
              10 TS-QEXPMAX      PIC X(5).                              00122200
              10 TS-MODIF        PIC X(5).                              00122300
              10 TS-WEXCEP       PIC X(1).                              00122400
                                                                                
