      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JRM631 AU 16/12/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,20,BI,A,                          *        
      *                           40,04,PD,A,                          *        
      *                           44,07,BI,A,                          *        
      *                           51,01,BI,A,                          *        
      *                           52,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JRM631.                                                        
            05 NOMETAT-JRM631           PIC X(6) VALUE 'JRM631'.                
            05 RUPTURES-JRM631.                                                 
           10 JRM631-CHEFPROD           PIC X(05).                      007  005
           10 JRM631-CRMGROUP           PIC X(05).                      012  005
           10 JRM631-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 JRM631-LAGREGATED         PIC X(20).                      020  020
           10 JRM631-QV1STRI            PIC S9(07)      COMP-3.         040  004
           10 JRM631-ZNCODIC            PIC X(07).                      044  007
           10 JRM631-REMPLACE           PIC X(01).                      051  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JRM631-SEQUENCE           PIC S9(04) COMP.                052  002
      *--                                                                       
           10 JRM631-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JRM631.                                                   
           10 JRM631-CFAM               PIC X(05).                      054  005
           10 JRM631-CMARQ              PIC X(05).                      059  005
           10 JRM631-DPV1               PIC X(04).                      064  004
           10 JRM631-DPV2               PIC X(04).                      068  004
           10 JRM631-DPV3               PIC X(04).                      072  004
           10 JRM631-DV1SD              PIC X(04).                      076  004
           10 JRM631-DV1SF              PIC X(04).                      080  004
           10 JRM631-DV2SD              PIC X(04).                      084  004
           10 JRM631-DV2SF              PIC X(04).                      088  004
           10 JRM631-DV3SD              PIC X(04).                      092  004
           10 JRM631-DV3SF              PIC X(04).                      096  004
           10 JRM631-DV4SD              PIC X(04).                      100  004
           10 JRM631-DV4SF              PIC X(04).                      104  004
           10 JRM631-INDLOI             PIC X(01).                      108  001
           10 JRM631-INDLOIS            PIC X(01).                      109  001
           10 JRM631-LCHEFPROD          PIC X(20).                      110  020
           10 JRM631-LFAM               PIC X(20).                      130  020
           10 JRM631-LIBELLE1           PIC X(07).                      150  007
           10 JRM631-LIBELLE2           PIC X(05).                      157  005
           10 JRM631-LREFFOURN          PIC X(20).                      162  020
           10 JRM631-LSTATCOMP          PIC X(03).                      182  003
           10 JRM631-NCODIC1            PIC X(08).                      185  008
           10 JRM631-PLURIEL            PIC X(01).                      193  001
           10 JRM631-QSTOCKDEP          PIC X(05).                      194  005
           10 JRM631-REMP               PIC X(03).                      199  003
           10 JRM631-TRI                PIC X(01).                      202  001
           10 JRM631-WDEP2              PIC X(01).                      203  001
           10 JRM631-WDEP3              PIC X(01).                      204  001
           10 JRM631-WDEP4              PIC X(01).                      205  001
           10 JRM631-WDEP5              PIC X(01).                      206  001
           10 JRM631-WDEP6              PIC X(01).                      207  001
           10 JRM631-WDEP7              PIC X(01).                      208  001
           10 JRM631-WDEP8              PIC X(01).                      209  001
           10 JRM631-WEDIT              PIC X(01).                      210  001
           10 JRM631-WEDIT2             PIC X(01).                      211  001
           10 JRM631-WEDIT3             PIC X(01).                      212  001
           10 JRM631-WPV1               PIC X(01).                      213  001
           10 JRM631-WPV2               PIC X(01).                      214  001
           10 JRM631-WPV3               PIC X(01).                      215  001
           10 JRM631-WPV4               PIC X(01).                      216  001
           10 JRM631-WREMP              PIC X(01).                      217  001
           10 JRM631-WSAVANCE           PIC X(01).                      218  001
           10 JRM631-WSENSAPPRO         PIC X(01).                      219  001
           10 JRM631-WWPV1              PIC X(01).                      220  001
           10 JRM631-WWPV2              PIC X(01).                      221  001
           10 JRM631-WWPV3              PIC X(01).                      222  001
           10 JRM631-W8020              PIC X(01).                      223  001
           10 JRM631-ZDEPOT             PIC X(03).                      224  003
           10 JRM631-ZDEPOT2            PIC X(03).                      227  003
           10 JRM631-ZDEPOT3            PIC X(03).                      230  003
           10 JRM631-ZDEPOT4            PIC X(03).                      233  003
           10 JRM631-ZDEPOT5            PIC X(03).                      236  003
           10 JRM631-ZDEPOT6            PIC X(03).                      239  003
           10 JRM631-ZDEPOT7            PIC X(03).                      242  003
           10 JRM631-ZDEPOT8            PIC X(03).                      245  003
           10 JRM631-ZQPV1              PIC X(06).                      248  006
           10 JRM631-ZQPV2              PIC X(06).                      254  006
           10 JRM631-ZQPV3              PIC X(06).                      260  006
           10 JRM631-ZQSTOCKDEP2        PIC X(05).                      266  005
           10 JRM631-ZQSTOCKDEP3        PIC X(05).                      271  005
           10 JRM631-ZQSTOCKDEP4        PIC X(05).                      276  005
           10 JRM631-ZQSTOCKDEP5        PIC X(05).                      281  005
           10 JRM631-ZQSTOCKDEP6        PIC X(05).                      286  005
           10 JRM631-ZQSTOCKDEP7        PIC X(05).                      291  005
           10 JRM631-ZQSTOCKDEP8        PIC X(05).                      296  005
           10 JRM631-DISPO              PIC S9(03)      COMP-3.         301  002
           10 JRM631-PREFTTC            PIC S9(07)V9(2) COMP-3.         303  005
           10 JRM631-QCDE               PIC S9(05)      COMP-3.         308  003
           10 JRM631-QCDE2              PIC S9(05)      COMP-3.         311  003
           10 JRM631-QCDE3              PIC S9(05)      COMP-3.         314  003
           10 JRM631-QCDE4              PIC S9(05)      COMP-3.         317  003
           10 JRM631-QCDE5              PIC S9(05)      COMP-3.         320  003
           10 JRM631-QCDE6              PIC S9(05)      COMP-3.         323  003
           10 JRM631-QCDE7              PIC S9(05)      COMP-3.         326  003
           10 JRM631-QCDE8              PIC S9(05)      COMP-3.         329  003
           10 JRM631-QCOLD              PIC S9(03)      COMP-3.         332  002
           10 JRM631-QNBMAG             PIC S9(03)      COMP-3.         334  002
           10 JRM631-QRG                PIC S9(03)      COMP-3.         336  002
           10 JRM631-QSTOCKMAG          PIC S9(06)      COMP-3.         338  004
           10 JRM631-QSTOCKMAG2         PIC S9(06)      COMP-3.         342  004
           10 JRM631-QSTOCKMAG3         PIC S9(06)      COMP-3.         346  004
           10 JRM631-QSTOCKMAG4         PIC S9(06)      COMP-3.         350  004
           10 JRM631-QSTOCKMAG5         PIC S9(06)      COMP-3.         354  004
           10 JRM631-QSTOCKMAG6         PIC S9(06)      COMP-3.         358  004
           10 JRM631-QSTOCKMAG7         PIC S9(06)      COMP-3.         362  004
           10 JRM631-QSTOCKMAG8         PIC S9(06)      COMP-3.         366  004
           10 JRM631-QTOTMAG            PIC S9(03)      COMP-3.         370  002
           10 JRM631-QVS-1              PIC S9(07)      COMP-3.         372  004
           10 JRM631-QVS-2              PIC S9(07)      COMP-3.         376  004
           10 JRM631-QVS-3              PIC S9(07)      COMP-3.         380  004
           10 JRM631-QVS-4              PIC S9(07)      COMP-3.         384  004
           10 JRM631-QV1S               PIC S9(07)      COMP-3.         388  004
           10 JRM631-QV2S               PIC S9(07)      COMP-3.         392  004
           10 JRM631-QV3S               PIC S9(07)      COMP-3.         396  004
           10 JRM631-QV4S               PIC S9(07)      COMP-3.         400  004
           10 JRM631-STOCKAV            PIC S9(02)V9(2) COMP-3.         404  003
            05 FILLER                      PIC X(106).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JRM631-LONG           PIC S9(4)   COMP  VALUE +406.           
      *                                                                         
      *--                                                                       
        01  DSECT-JRM631-LONG           PIC S9(4) COMP-5  VALUE +406.           
                                                                                
      *}                                                                        
