      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVMU3000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMU3000                         
      **********************************************************                
       01  RVMU3000.                                                            
           02  MU30-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  MU30-NLIEU                                                       
               PIC X(0003).                                                     
           02  MU30-NROTATION                                                   
               PIC X(0001).                                                     
           02  MU30-WPOLITIQUE                                                  
               PIC X(0001).                                                     
           02  MU30-QARTRATIO                                                   
               PIC S9(4)V9(0001) COMP-3.                                        
           02  MU30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVMU3000                                  
      **********************************************************                
       01  RVMU3000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU30-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU30-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU30-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU30-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU30-NROTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU30-NROTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU30-WPOLITIQUE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU30-WPOLITIQUE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU30-QARTRATIO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MU30-QARTRATIO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MU30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  MU30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
