      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESU52   ESU52                                              00000020
      ***************************************************************** 00000030
       01   ESU52I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(32).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCRL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCRF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCRI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGRL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMAGRF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGRI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAMRL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAMRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCFAMRF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMRI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICRL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNCODICRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODICRF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICRI      PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETML      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDEFFETML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDEFFETMF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDEFFETMI      PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQRL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMARQRF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQRI  PIC X(5).                                       00000450
           02 LIGNEI OCCURS   14 TIMES .                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCMAGL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSOCMAGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCMAGF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSOCMAGI    PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNCODICI     PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MCFAMI  PIC X(5).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCMARQI      PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLREFI  PIC X(20).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MQTEI   PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEFFETL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDEFFETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDEFFETF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDEFFETI     PIC X(10).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDFINEFFL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDFINEFFL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDFINEFFF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDFINEFFI    PIC X(10).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(12).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(61).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: ESU52   ESU52                                              00001040
      ***************************************************************** 00001050
       01   ESU52O REDEFINES ESU52I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MTITREA   PIC X.                                          00001230
           02 MTITREC   PIC X.                                          00001240
           02 MTITREP   PIC X.                                          00001250
           02 MTITREH   PIC X.                                          00001260
           02 MTITREV   PIC X.                                          00001270
           02 MTITREO   PIC X(32).                                      00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNPAGEA   PIC X.                                          00001300
           02 MNPAGEC   PIC X.                                          00001310
           02 MNPAGEP   PIC X.                                          00001320
           02 MNPAGEH   PIC X.                                          00001330
           02 MNPAGEV   PIC X.                                          00001340
           02 MNPAGEO   PIC X(3).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNSOCRA   PIC X.                                          00001370
           02 MNSOCRC   PIC X.                                          00001380
           02 MNSOCRP   PIC X.                                          00001390
           02 MNSOCRH   PIC X.                                          00001400
           02 MNSOCRV   PIC X.                                          00001410
           02 MNSOCRO   PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MNMAGRA   PIC X.                                          00001440
           02 MNMAGRC   PIC X.                                          00001450
           02 MNMAGRP   PIC X.                                          00001460
           02 MNMAGRH   PIC X.                                          00001470
           02 MNMAGRV   PIC X.                                          00001480
           02 MNMAGRO   PIC X(3).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MCFAMRA   PIC X.                                          00001510
           02 MCFAMRC   PIC X.                                          00001520
           02 MCFAMRP   PIC X.                                          00001530
           02 MCFAMRH   PIC X.                                          00001540
           02 MCFAMRV   PIC X.                                          00001550
           02 MCFAMRO   PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNCODICRA      PIC X.                                     00001580
           02 MNCODICRC PIC X.                                          00001590
           02 MNCODICRP PIC X.                                          00001600
           02 MNCODICRH PIC X.                                          00001610
           02 MNCODICRV PIC X.                                          00001620
           02 MNCODICRO      PIC X(7).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDEFFETMA      PIC X.                                     00001650
           02 MDEFFETMC PIC X.                                          00001660
           02 MDEFFETMP PIC X.                                          00001670
           02 MDEFFETMH PIC X.                                          00001680
           02 MDEFFETMV PIC X.                                          00001690
           02 MDEFFETMO      PIC X(10).                                 00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MCMARQRA  PIC X.                                          00001720
           02 MCMARQRC  PIC X.                                          00001730
           02 MCMARQRP  PIC X.                                          00001740
           02 MCMARQRH  PIC X.                                          00001750
           02 MCMARQRV  PIC X.                                          00001760
           02 MCMARQRO  PIC X(5).                                       00001770
           02 LIGNEO OCCURS   14 TIMES .                                00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNSOCMAGA    PIC X.                                     00001800
             03 MNSOCMAGC    PIC X.                                     00001810
             03 MNSOCMAGP    PIC X.                                     00001820
             03 MNSOCMAGH    PIC X.                                     00001830
             03 MNSOCMAGV    PIC X.                                     00001840
             03 MNSOCMAGO    PIC X(6).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MNCODICA     PIC X.                                     00001870
             03 MNCODICC     PIC X.                                     00001880
             03 MNCODICP     PIC X.                                     00001890
             03 MNCODICH     PIC X.                                     00001900
             03 MNCODICV     PIC X.                                     00001910
             03 MNCODICO     PIC X(7).                                  00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MCFAMA  PIC X.                                          00001940
             03 MCFAMC  PIC X.                                          00001950
             03 MCFAMP  PIC X.                                          00001960
             03 MCFAMH  PIC X.                                          00001970
             03 MCFAMV  PIC X.                                          00001980
             03 MCFAMO  PIC X(5).                                       00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MCMARQA      PIC X.                                     00002010
             03 MCMARQC PIC X.                                          00002020
             03 MCMARQP PIC X.                                          00002030
             03 MCMARQH PIC X.                                          00002040
             03 MCMARQV PIC X.                                          00002050
             03 MCMARQO      PIC X(5).                                  00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MLREFA  PIC X.                                          00002080
             03 MLREFC  PIC X.                                          00002090
             03 MLREFP  PIC X.                                          00002100
             03 MLREFH  PIC X.                                          00002110
             03 MLREFV  PIC X.                                          00002120
             03 MLREFO  PIC X(20).                                      00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MQTEA   PIC X.                                          00002150
             03 MQTEC   PIC X.                                          00002160
             03 MQTEP   PIC X.                                          00002170
             03 MQTEH   PIC X.                                          00002180
             03 MQTEV   PIC X.                                          00002190
             03 MQTEO   PIC X(5).                                       00002200
             03 FILLER       PIC X(2).                                  00002210
             03 MDEFFETA     PIC X.                                     00002220
             03 MDEFFETC     PIC X.                                     00002230
             03 MDEFFETP     PIC X.                                     00002240
             03 MDEFFETH     PIC X.                                     00002250
             03 MDEFFETV     PIC X.                                     00002260
             03 MDEFFETO     PIC X(10).                                 00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MDFINEFFA    PIC X.                                     00002290
             03 MDFINEFFC    PIC X.                                     00002300
             03 MDFINEFFP    PIC X.                                     00002310
             03 MDFINEFFH    PIC X.                                     00002320
             03 MDFINEFFV    PIC X.                                     00002330
             03 MDFINEFFO    PIC X(10).                                 00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(12).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(61).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
