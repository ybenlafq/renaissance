      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TLV02                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LV02-LONG            PIC S9(3) COMP-3 VALUE 75.           00060000
       01  TS-LV02-DONNEES.                                             00070000
              10 TS-LV02-DJOUR     PIC X(08).                           00080000
              10 TS-LV02-NSOCDEPOT PIC X(03).                           00090000
              10 TS-LV02-NDEPOT    PIC X(03).                           00100000
              10 TS-LV02-NMUT      PIC X(07).                           00110000
              10 TS-LV02-NSOC      PIC X(03).                           00120000
              10 TS-LV02-NLIEU     PIC X(03).                           00130000
              10 TS-LV02-NSELAR    PIC X(05).                           00140000
              10 TS-LV02-NQP       PIC X(05).                           00150000
              10 TS-LV02-NQV       PIC X(06).                           00160000
              10 TS-LV02-NDDEST    PIC X(10).                           00170000
              10 TS-LV02-HCHRGT    PIC X(05).                           00180000
              10 TS-LV02-NCTYPE    PIC X(02).                           00190000
              10 TS-LV02-NCTRANS   PIC X(13).                           00191000
              10 TS-LV02-SELECT    PIC X.                               00192000
              10 TS-LV02-TRANSFERT PIC X.                               00193000
                                                                                
