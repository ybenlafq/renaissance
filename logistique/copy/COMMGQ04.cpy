      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TGQ04 (TGQ00 -> MENU)    TR: GQ00  *    00002236
      *          GESTION DES PARAMETRES GENERAUX LIVRAISON         *    00002332
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ============================ 3724  00009000
      *                                                                 00410100
      *  TRANSACTION GQ04 : GESTION DES PARAMETRES GEOGRAPHIQUES   *    00411036
      *                                                                 00412000
          02 COMM-GQ04-APPLI REDEFINES COMM-GQ00-APPLI.                 00420036
      *                                                                 00430036
      *------------------------------ CODE DEPARTEMENT                  00550036
             03 COMM-GQ04-CDEPART        PIC X(2).                      00560036
      *------------------------------ ZONE TGQ04                        01710035
             03 COMM-GQ04-LIBRE.                                        01720036
      *---------------------------------------------------------------- 02170035
                 10 COMM-GQ04-VALEURS.                                  02170037
                    15 COMM-GQ04-NUMERO-PAGE      PIC S9(3) COMP-3.     02170038
                    15 COMM-GQ04-NUMERO-ITEM      PIC S9(3) COMP-3.     02170039
                    15 COMM-GQ04-PROGRAMME-RETOUR PIC X(5).             02170040
                    15 COMM-GQ04-FIN-LECTURE      PIC X(1).             02170041
                    15 COMM-GQ04-CINSEE           PIC X(5).             02170042
                    15 COMM-GQ04-CINSEE-MAX       PIC X(5).             02170043
      *-------------------------------- CODE SOCIETE ASSOCIE DEPARTEMENT02170035
                 10 COMM-GQ04-NSOCDEP           PIC X(03).              02170060
      *-------------------------------- CODE LIBELLE SOCIETE            02170035
                 10 COMM-GQ04-LSOCDEP           PIC X(20).              02170060
      *-------------------------------- FLAG DE DEBUT DE TRAITEMENT     02170035
                 10 COMM-GQ04-DEBUT             PIC X.                  02170060
      *-------------------------------- DEBUT DE LA COMMUNE D'AFFICHAGE 02170035
                 10 COMM-GQ04-COMMUNE1          PIC X(32).              02170060
      *-------------------------------- VALEUR DE RTGQ12                02170035
                 10 COMM-GQ04-GRP-TABLE.                                02170060
                    15 COMM-GQ04-GRP-OCCURS   OCCURS 3.                    02170
                       20 COMM-GQ04-CGRP        PIC X(5).                  02170
      *-------------------------------- CODE CONTRAT/PRESTATION         02170035
                 10 COMM-GQ04-CODE              PIC X(1).                  02170
      *-------------------------------- CODE CADRE DE VENTE (PERIMETRE) 02170035
                 10 COMM-GQ04-CADRE             PIC X(5).                  02170
      *-------------------------------- ZONE LIBRE                      02170035
                 10 COMM-GQ04-FILLER            PIC X(3625).            02170060
                                                                                
