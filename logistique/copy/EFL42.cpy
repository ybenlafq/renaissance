      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EFL42   EFL42                                              00000020
      ***************************************************************** 00000030
       01   EFL42I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEYL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEYL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEYF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEYI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMMENTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCOMMENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMMENTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCOMMENTI      PIC X(23).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEFAMILL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCEFAMILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCEFAMILF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCEFAMILI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLEFAMILL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLEFAMILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLEFAMILF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLEFAMILI      PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPROAFFL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPROAFFF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCPROAFFI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPROAFFL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLPROAFFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPROAFFF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLPROAFFI      PIC X(20).                                 00000410
           02 MTABLEI OCCURS   15 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MCMARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCMARF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MCMARI  PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMARL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MLMARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMARF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MLMARI  PIC X(20).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC1L  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MSOC1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC1F  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MSOC1I  PIC X(3).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT1L  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MENT1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT1F  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MENT1I  PIC X(3).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC2L  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MSOC2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC2F  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MSOC2I  PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT2L  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MENT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT2F  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MENT2I  PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC3L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSOC3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC3F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSOC3I  PIC X(3).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT3L  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MENT3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT3F  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MENT3I  PIC X(3).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC4L  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MSOC4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC4F  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MSOC4I  PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT4L  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MENT4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT4F  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MENT4I  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOC5L  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MSOC5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSOC5F  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MSOC5I  PIC X(3).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MENT5L  COMP PIC S9(4).                                 00000870
      *--                                                                       
             03 MENT5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MENT5F  PIC X.                                          00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MENT5I  PIC X(3).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(58).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * SDF: EFL42   EFL42                                              00001160
      ***************************************************************** 00001170
       01   EFL42O REDEFINES EFL42I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEA    PIC X.                                          00001350
           02 MPAGEC    PIC X.                                          00001360
           02 MPAGEP    PIC X.                                          00001370
           02 MPAGEH    PIC X.                                          00001380
           02 MPAGEV    PIC X.                                          00001390
           02 MPAGEO    PIC X(3).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MPAGEYA   PIC X.                                          00001420
           02 MPAGEYC   PIC X.                                          00001430
           02 MPAGEYP   PIC X.                                          00001440
           02 MPAGEYH   PIC X.                                          00001450
           02 MPAGEYV   PIC X.                                          00001460
           02 MPAGEYO   PIC X(3).                                       00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCOMMENTA      PIC X.                                     00001490
           02 MCOMMENTC PIC X.                                          00001500
           02 MCOMMENTP PIC X.                                          00001510
           02 MCOMMENTH PIC X.                                          00001520
           02 MCOMMENTV PIC X.                                          00001530
           02 MCOMMENTO      PIC X(23).                                 00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCEFAMILA      PIC X.                                     00001560
           02 MCEFAMILC PIC X.                                          00001570
           02 MCEFAMILP PIC X.                                          00001580
           02 MCEFAMILH PIC X.                                          00001590
           02 MCEFAMILV PIC X.                                          00001600
           02 MCEFAMILO      PIC X(5).                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLEFAMILA      PIC X.                                     00001630
           02 MLEFAMILC PIC X.                                          00001640
           02 MLEFAMILP PIC X.                                          00001650
           02 MLEFAMILH PIC X.                                          00001660
           02 MLEFAMILV PIC X.                                          00001670
           02 MLEFAMILO      PIC X(20).                                 00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCPROAFFA      PIC X.                                     00001700
           02 MCPROAFFC PIC X.                                          00001710
           02 MCPROAFFP PIC X.                                          00001720
           02 MCPROAFFH PIC X.                                          00001730
           02 MCPROAFFV PIC X.                                          00001740
           02 MCPROAFFO      PIC X(5).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLPROAFFA      PIC X.                                     00001770
           02 MLPROAFFC PIC X.                                          00001780
           02 MLPROAFFP PIC X.                                          00001790
           02 MLPROAFFH PIC X.                                          00001800
           02 MLPROAFFV PIC X.                                          00001810
           02 MLPROAFFO      PIC X(20).                                 00001820
           02 MTABLEO OCCURS   15 TIMES .                               00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MCMARA  PIC X.                                          00001850
             03 MCMARC  PIC X.                                          00001860
             03 MCMARP  PIC X.                                          00001870
             03 MCMARH  PIC X.                                          00001880
             03 MCMARV  PIC X.                                          00001890
             03 MCMARO  PIC X(5).                                       00001900
             03 FILLER       PIC X(2).                                  00001910
             03 MLMARA  PIC X.                                          00001920
             03 MLMARC  PIC X.                                          00001930
             03 MLMARP  PIC X.                                          00001940
             03 MLMARH  PIC X.                                          00001950
             03 MLMARV  PIC X.                                          00001960
             03 MLMARO  PIC X(20).                                      00001970
             03 FILLER       PIC X(2).                                  00001980
             03 MSOC1A  PIC X.                                          00001990
             03 MSOC1C  PIC X.                                          00002000
             03 MSOC1P  PIC X.                                          00002010
             03 MSOC1H  PIC X.                                          00002020
             03 MSOC1V  PIC X.                                          00002030
             03 MSOC1O  PIC X(3).                                       00002040
             03 FILLER       PIC X(2).                                  00002050
             03 MENT1A  PIC X.                                          00002060
             03 MENT1C  PIC X.                                          00002070
             03 MENT1P  PIC X.                                          00002080
             03 MENT1H  PIC X.                                          00002090
             03 MENT1V  PIC X.                                          00002100
             03 MENT1O  PIC X(3).                                       00002110
             03 FILLER       PIC X(2).                                  00002120
             03 MSOC2A  PIC X.                                          00002130
             03 MSOC2C  PIC X.                                          00002140
             03 MSOC2P  PIC X.                                          00002150
             03 MSOC2H  PIC X.                                          00002160
             03 MSOC2V  PIC X.                                          00002170
             03 MSOC2O  PIC X(3).                                       00002180
             03 FILLER       PIC X(2).                                  00002190
             03 MENT2A  PIC X.                                          00002200
             03 MENT2C  PIC X.                                          00002210
             03 MENT2P  PIC X.                                          00002220
             03 MENT2H  PIC X.                                          00002230
             03 MENT2V  PIC X.                                          00002240
             03 MENT2O  PIC X(3).                                       00002250
             03 FILLER       PIC X(2).                                  00002260
             03 MSOC3A  PIC X.                                          00002270
             03 MSOC3C  PIC X.                                          00002280
             03 MSOC3P  PIC X.                                          00002290
             03 MSOC3H  PIC X.                                          00002300
             03 MSOC3V  PIC X.                                          00002310
             03 MSOC3O  PIC X(3).                                       00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MENT3A  PIC X.                                          00002340
             03 MENT3C  PIC X.                                          00002350
             03 MENT3P  PIC X.                                          00002360
             03 MENT3H  PIC X.                                          00002370
             03 MENT3V  PIC X.                                          00002380
             03 MENT3O  PIC X(3).                                       00002390
             03 FILLER       PIC X(2).                                  00002400
             03 MSOC4A  PIC X.                                          00002410
             03 MSOC4C  PIC X.                                          00002420
             03 MSOC4P  PIC X.                                          00002430
             03 MSOC4H  PIC X.                                          00002440
             03 MSOC4V  PIC X.                                          00002450
             03 MSOC4O  PIC X(3).                                       00002460
             03 FILLER       PIC X(2).                                  00002470
             03 MENT4A  PIC X.                                          00002480
             03 MENT4C  PIC X.                                          00002490
             03 MENT4P  PIC X.                                          00002500
             03 MENT4H  PIC X.                                          00002510
             03 MENT4V  PIC X.                                          00002520
             03 MENT4O  PIC X(3).                                       00002530
             03 FILLER       PIC X(2).                                  00002540
             03 MSOC5A  PIC X.                                          00002550
             03 MSOC5C  PIC X.                                          00002560
             03 MSOC5P  PIC X.                                          00002570
             03 MSOC5H  PIC X.                                          00002580
             03 MSOC5V  PIC X.                                          00002590
             03 MSOC5O  PIC X(3).                                       00002600
             03 FILLER       PIC X(2).                                  00002610
             03 MENT5A  PIC X.                                          00002620
             03 MENT5C  PIC X.                                          00002630
             03 MENT5P  PIC X.                                          00002640
             03 MENT5H  PIC X.                                          00002650
             03 MENT5V  PIC X.                                          00002660
             03 MENT5O  PIC X(3).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MZONCMDA  PIC X.                                          00002690
           02 MZONCMDC  PIC X.                                          00002700
           02 MZONCMDP  PIC X.                                          00002710
           02 MZONCMDH  PIC X.                                          00002720
           02 MZONCMDV  PIC X.                                          00002730
           02 MZONCMDO  PIC X(15).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIBERRA  PIC X.                                          00002760
           02 MLIBERRC  PIC X.                                          00002770
           02 MLIBERRP  PIC X.                                          00002780
           02 MLIBERRH  PIC X.                                          00002790
           02 MLIBERRV  PIC X.                                          00002800
           02 MLIBERRO  PIC X(58).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MCODTRAA  PIC X.                                          00002830
           02 MCODTRAC  PIC X.                                          00002840
           02 MCODTRAP  PIC X.                                          00002850
           02 MCODTRAH  PIC X.                                          00002860
           02 MCODTRAV  PIC X.                                          00002870
           02 MCODTRAO  PIC X(4).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MCICSA    PIC X.                                          00002900
           02 MCICSC    PIC X.                                          00002910
           02 MCICSP    PIC X.                                          00002920
           02 MCICSH    PIC X.                                          00002930
           02 MCICSV    PIC X.                                          00002940
           02 MCICSO    PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNETNAMA  PIC X.                                          00002970
           02 MNETNAMC  PIC X.                                          00002980
           02 MNETNAMP  PIC X.                                          00002990
           02 MNETNAMH  PIC X.                                          00003000
           02 MNETNAMV  PIC X.                                          00003010
           02 MNETNAMO  PIC X(8).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MSCREENA  PIC X.                                          00003040
           02 MSCREENC  PIC X.                                          00003050
           02 MSCREENP  PIC X.                                          00003060
           02 MSCREENH  PIC X.                                          00003070
           02 MSCREENV  PIC X.                                          00003080
           02 MSCREENO  PIC X(4).                                       00003090
                                                                                
