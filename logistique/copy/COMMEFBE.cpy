      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY ******************************************************         
      *    GEF/GHE : EDITION DU BON D'ENVOI FOURNISSEUR               *         
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-EFBE-LONG-COMMAREA PIC S9(4) COMP VALUE +500.                   
      *--                                                                       
       01  COMM-EFBE-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +500.                 
      *}                                                                        
       01    Z-COMMAREA-EFBE.                                                   
           05      COMM-EFBE-AIDA          PIC X(220).                          
           05      COMM-EFBE-NSOC          PIC X(003).                          
           05      COMM-EFBE-NLIEU         PIC X(003).                          
           05      COMM-EFBE-CTRAIT        PIC X(005).                          
           05      COMM-EFBE-LTRAIT        PIC X(020).                          
           05      COMM-EFBE-CTIERS        PIC X(005).                          
           05      COMM-EFBE-CTIERS-GEF    PIC X(005).                          
           05      COMM-EFBE-LTYPTRAN      PIC X(020).                          
           05      COMM-EFBE-NENVOI        PIC X(007).                          
           05      COMM-EFBE-DENVOI        PIC X(008).                          
           05      COMM-EFBE-WAVENANT      PIC X(001).                          
           05      COMM-EFBE-CIMP          PIC X(004).                          
           05      COMM-EFBE-NOM-TS        PIC X(008).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05      COMM-EFBE-NBITEM        PIC S9(04) COMP.                     
      *--                                                                       
           05      COMM-EFBE-NBITEM        PIC S9(04) COMP-5.                   
      *}                                                                        
           05      COMM-EFBE-CRETOUR       PIC X(001).                          
                88 COMM-EFBE-IMP-OK VALUE '0'.                                  
                88 COMM-EFBE-IMP-KO VALUE '1'.                                  
           05      COMM-EFBE-MESS-RETOUR   PIC X(080).                          
           05      COMM-EFBE-FILLER        PIC X(112).                          
      *                                         ----                            
      *                                          500                            
                                                                                
