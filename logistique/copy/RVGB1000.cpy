      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGB1000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB1000                         
      **********************************************************                
       01  RVGB1000.                                                            
           02  GB10-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB10-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB10-DANNEE                                                      
               PIC X(0004).                                                     
           02  GB10-DSEMAINE                                                    
               PIC X(0002).                                                     
           02  GB10-WTYPFERM1                                                   
               PIC X(0004).                                                     
           02  GB10-WTYPFERM2                                                   
               PIC X(0004).                                                     
           02  GB10-WTYPFERM3                                                   
               PIC X(0004).                                                     
           02  GB10-WTYPFERM4                                                   
               PIC X(0004).                                                     
           02  GB10-WTYPFERM5                                                   
               PIC X(0004).                                                     
           02  GB10-WTYPFERM6                                                   
               PIC X(0004).                                                     
           02  GB10-WTYPFERM7                                                   
               PIC X(0004).                                                     
           02  GB10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGB1000                                  
      **********************************************************                
       01  RVGB1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-DANNEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-DANNEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-DSEMAINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-DSEMAINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM4-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM4-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM5-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM5-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM6-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM6-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-WTYPFERM7-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB10-WTYPFERM7-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GB10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
