      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * DSA057 10/02/05 SUPPORT EVOLUTION E63                                   
      *                 VALEUR PAR DEFAUT                                       
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVDEFAU.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVDEFAU.                                                             
      *}                                                                        
           05  DEFAU-CTABLEG2    PIC X(15).                                     
           05  DEFAU-CTABLEG2-REDEF REDEFINES DEFAU-CTABLEG2.                   
               10  DEFAU-COPCO           PIC X(03).                             
               10  DEFAU-CCHAMP          PIC X(10).                             
           05  DEFAU-WTABLEG     PIC X(80).                                     
           05  DEFAU-WTABLEG-REDEF  REDEFINES DEFAU-WTABLEG.                    
               10  DEFAU-CODE            PIC X(20).                             
               10  DEFAU-WFLAG           PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVDEFAU-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVDEFAU-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DEFAU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DEFAU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DEFAU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DEFAU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
