      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      * NB  : RECOPIE DE TSTL27 EN AJOUTANT UNE ZONE POUR REAFFICHAGE * 00011001
      *       DU MONTANT                                              * 00012001
      *  TS : REPARTITION ENCAISSEMENT SUITE A TOPE                   * 00020001
      ***************************************************************** 00030000
       01  TS-TL27.                                                     00040000
           02 TS27-NOM.                                                 00050000
               03 TS27-NOM1       PIC X(04) VALUE 'TL27'.               00060000
               03 TS27-TERM       PIC X(04).                            00070000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
GCT   *     02 TS27-LONG          PIC S9(04) COMP VALUE +2812.          00080001
      *--                                                                       
            02 TS27-LONG          PIC S9(04) COMP-5 VALUE +2812.                
      *}                                                                        
GCT         02 RES-TS27-DONNEES   PIC X(2812).                          00090001
            02 TS27-DONNEES.                                            00100000
               03 TS27-NLIGNE     PIC 99.                               00110000
               03 TS27-LIGNE           OCCURS 10.                       00120000
                  04 TS27-NMAG             PIC X(03).                   00130000
                  04 TS27-NVENTE           PIC X(07).                   00140000
                  04 TS27-LCLIENT          PIC X(19).                   00150000
                  04 TS27-WREP             PIC X(01).                   00160000
                  04 TS27-CADRTOUR         PIC X(01).                   00170000
                  04 TS27-CINSEE           PIC X(05).                   00180000
                  04 TS27-PVERSE           PIC S9(06)V99.               00190000
                  04 TS27-PMONTOT          PIC S9(06)V99.               00200000
GCT               04 TS27-PTOT-AFF         PIC S9(06)V99.               00201001
                  04 TS27-QVENTE           PIC 9(02).                   00210000
                  04 TS27-REGLT-INITIAL.                                00220000
                     05 TS27-PMONT1X-I.                                 00230000
                        06 TS27-PMONT1-I   PIC S9(06)V99.               00240000
                     05 TS27-CMODP1-I      PIC X(03).                   00250000
                     05 TS27-DEV1-I        PIC 9.                       00260000
                     05 TS27-DMONT1-I      PIC S9(06)V99.               00260100
                     05 TS27-EMONT1-I      PIC S9V99.                   00260200
                     05 TS27-PMONT2-I      PIC S9(06)V99.               00260300
                     05 TS27-CMODP2-I      PIC X(03).                   00270000
                     05 TS27-DEV2-I        PIC 9.                       00270100
                     05 TS27-DMONT2-I      PIC S9(06)V99.               00270200
                     05 TS27-EMONT2-I      PIC S9V99.                   00270300
                     05 TS27-PMONT3-I      PIC S9(06)V99.               00280000
                     05 TS27-CMODP3-I      PIC X(03).                   00290000
                     05 TS27-DEV3-I        PIC 9.                       00290100
                     05 TS27-DMONT3-I      PIC S9(06)V99.               00290200
                     05 TS27-EMONT3-I      PIC S9V99.                   00290300
                  04 TS27-REGLT-INIT REDEFINES TS27-REGLT-INITIAL.      00300000
                     05 POS-TS27-REGLT-INIT OCCURS 3.                   00300010
                        10 TS27-PMONT-I    PIC S9(06)V99.               00300020
                        10 TS27-CMODP-I    PIC X(03).                   00300030
                        10 TS27-DEV-I      PIC 9.                       00300040
                        10 TS27-DMONT-I    PIC S9(06)V99.               00300050
                        10 TS27-EMONT-I    PIC S9V99.                   00300060
                  04 TS27-REGLT-FINAL.                                  00300100
                     05 TS27-PMONT1X-F.                                 00310000
                        06 TS27-PMONT1-F   PIC S9(06)V99.               00320000
                     05 TS27-CMODP1-F      PIC X(03).                   00330000
                     05 TS27-DEV1-F        PIC 9.                       00330100
                     05 TS27-DMONT1-F      PIC S9(06)V99.               00330200
                     05 TS27-EMONT1-F      PIC S9V99.                   00330300
                     05 TS27-PMONT2-F      PIC S9(06)V99.               00340000
                     05 TS27-CMODP2-F      PIC X(03).                   00350000
                     05 TS27-DEV2-F        PIC 9.                       00350100
                     05 TS27-DMONT2-F      PIC S9(06)V99.               00350200
                     05 TS27-EMONT2-F      PIC S9V99.                   00350300
                     05 TS27-PMONT3-F      PIC S9(06)V99.               00360000
                     05 TS27-CMODP3-F      PIC X(03).                   00370000
                     05 TS27-DEV3-F        PIC 9.                       00370100
                     05 TS27-DMONT3-F      PIC S9(06)V99.               00370200
                     05 TS27-EMONT3-F      PIC S9V99.                   00370300
                  04 TS27-REGLT-FINAL REDEFINES TS27-REGLT-FINAL.       00370400
                     05 POS-TS27-REGLT-FINAL OCCURS 3.                  00370500
                        10 TS27-PMONT-F    PIC S9(06)V99.               00370600
                        10 TS27-CMODP-F    PIC X(03).                   00370700
                        10 TS27-DEV-F      PIC 9.                       00370800
                        10 TS27-DMONT-F    PIC S9(06)V99.               00370900
                        10 TS27-EMONT-F    PIC S9V99.                   00371000
      *                                    MAJ EFFECTUEE, 'A' AJOUT     00380000
      *                                                   'S' SUPPRESS. 00390000
      *                                                   'M' MODIFICA. 00400000
                  04 TS27-WMAJ             PIC X.                       00410000
      *                                    HS TRAITE                    00420000
                  04 TS27-WHS              PIC X.                       00430000
      *                                    RETOUR SUR LA VENTE O/N      00440000
                  04 TS27-WRETOUR          PIC X.                       00450000
      *                                    MOTIF DE RETOUR SI RETOUR    00460000
      *                                    GLOBAL                       00470000
                  04 TS27-CRETOUR          PIC X(5).                    00480000
      *                                    SI VENTE EXPORT A 'O'        00490001
                  04 TS27-WEXPORT          PIC X.                       00500001
                                                                                
                                                                        00510000
