      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MGNMD PROGRAMMES D INIT MGI=>NMD       *        
      *----------------------------------------------------------------*        
       01  RVMGNMD.                                                             
           05  MGNMD-CTABLEG2    PIC X(15).                                     
           05  MGNMD-CTABLEG2-REDEF REDEFINES MGNMD-CTABLEG2.                   
               10  MGNMD-NSOCMG          PIC X(03).                             
               10  MGNMD-NSOCMG-N       REDEFINES MGNMD-NSOCMG                  
                                         PIC 9(03).                             
               10  MGNMD-DATE            PIC X(06).                             
               10  MGNMD-CPROG           PIC X(06).                             
           05  MGNMD-WTABLEG     PIC X(80).                                     
           05  MGNMD-WTABLEG-REDEF  REDEFINES MGNMD-WTABLEG.                    
               10  MGNMD-SOCMAG          PIC X(06).                             
               10  MGNMD-LCOMMENT        PIC X(30).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMGNMD-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MGNMD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MGNMD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MGNMD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MGNMD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
