      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRM5800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM5800                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRM5800.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRM5800.                                                            
      *}                                                                        
           02  RM58-CGROUP                                                      
               PIC X(0005).                                                     
           02  RM58-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM58-NCODICLIE                                                   
               PIC X(0007).                                                     
           02  RM58-WACTIF                                                      
               PIC X(0001).                                                     
           02  RM58-DREMPLACEMENT                                               
               PIC X(0008).                                                     
           02  RM58-FILIALE                                                     
               PIC X(0003).                                                     
           02  RM58-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM5800                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRM5800-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRM5800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-CGROUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-CGROUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-NCODICLIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-NCODICLIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-WACTIF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-DREMPLACEMENT-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-DREMPLACEMENT-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-FILIALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-FILIALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM58-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM58-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
