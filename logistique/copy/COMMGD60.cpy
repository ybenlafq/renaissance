      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00010016
      * COMMAREA SPECIFIQUE PRG TGD60 (TGD00 -> MENU)    TR: GD00  *    00020016
      *               GESTION DES EMPLACEMENTS DE STOCKS           *    00030016
      *                                                                 00040016
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3566  00050016
      *                                                                 00060016
      *        TRANSACTION GD60 : CREATION RECEPTION FOURNISSEUR   *    00070016
      *                                                                 00080016
          02 COMM-GD60-APPLI REDEFINES COMM-GD00-APPLI.                 00090016
      *------------------------------ ZONE DONNEES TGD60                00100016
             03 COMM-GD60-DONNEES-TGD60.                                00110016
      *------------------------------                                   00120016
                 04  COMM-GD60-FILLER          PIC X(06).               00130016
                 04  COMM-GD60-NPAGE           PIC 9(03).               00170016
                 04  COMM-GD60-NPAGMAX         PIC 9(03).               00190016
                 04  COMM-GD60-PAGSUI          PIC X(01).               00190016
                 04  COMM-GD60-SWAP-ATTR       PIC X(01)  OCCURS 300.   00190016
      *                                                                 00640016
      *------------------------------ ZONE LIBRE                        00650018
      *                                                                 00660016
             03 COMM-GD60-LIBRE             PIC X(2887).                00670030
      *                                                                 00680016
                                                                                
