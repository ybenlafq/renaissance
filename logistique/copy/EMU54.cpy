      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Param�trage fili�re dacem                                       00000020
      ***************************************************************** 00000030
       01   EMU54I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MCPTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCPTF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MCPTI     PIC X(79).                                      00000170
           02 MHDARTYD OCCURS   16 TIMES .                              00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDARTYL     COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MHDARTYL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHDARTYF     PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MHDARTYI     PIC X(4).                                  00000220
           02 MSELARTD OCCURS   16 TIMES .                              00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000240
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00000250
             03 FILLER  PIC X(4).                                       00000260
             03 MSELARTI     PIC X(5).                                  00000270
           02 MSELARDD OCCURS   16 TIMES .                              00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARDL     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MSELARDL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARDF     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MSELARDI     PIC X(5).                                  00000320
           02 MTYPED OCCURS   16 TIMES .                                00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000340
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000350
             03 FILLER  PIC X(4).                                       00000360
             03 MTYPEI  PIC X(5).                                       00000370
           02 MHDACEMD OCCURS   16 TIMES .                              00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDACEML     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MHDACEML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHDACEMF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MHDACEMI     PIC X(4).                                  00000420
           02 MEC306D OCCURS   16 TIMES .                               00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MEC306L      COMP PIC S9(4).                            00000440
      *--                                                                       
             03 MEC306L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MEC306F      PIC X.                                     00000450
             03 FILLER  PIC X(4).                                       00000460
             03 MEC306I      PIC X.                                     00000470
           02 MINDICED OCCURS   16 TIMES .                              00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MINDICEL     COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MINDICEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MINDICEF     PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MINDICEI     PIC X.                                     00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MLIBERRI  PIC X(79).                                      00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCODTRAI  PIC X(4).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MZONCMDI  PIC X(15).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCICSI    PIC X(5).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MNETNAMI  PIC X(8).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MSCREENI  PIC X(4).                                       00000760
      ***************************************************************** 00000770
      * Param�trage fili�re dacem                                       00000780
      ***************************************************************** 00000790
       01   EMU54O REDEFINES EMU54I.                                    00000800
           02 FILLER    PIC X(12).                                      00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MDATJOUA  PIC X.                                          00000830
           02 MDATJOUC  PIC X.                                          00000840
           02 MDATJOUP  PIC X.                                          00000850
           02 MDATJOUH  PIC X.                                          00000860
           02 MDATJOUV  PIC X.                                          00000870
           02 MDATJOUO  PIC X(10).                                      00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MTIMJOUA  PIC X.                                          00000900
           02 MTIMJOUC  PIC X.                                          00000910
           02 MTIMJOUP  PIC X.                                          00000920
           02 MTIMJOUH  PIC X.                                          00000930
           02 MTIMJOUV  PIC X.                                          00000940
           02 MTIMJOUO  PIC X(5).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MCPTA     PIC X.                                          00000970
           02 MCPTC     PIC X.                                          00000980
           02 MCPTP     PIC X.                                          00000990
           02 MCPTH     PIC X.                                          00001000
           02 MCPTV     PIC X.                                          00001010
           02 MCPTO     PIC X(79).                                      00001020
           02 DFHMS1 OCCURS   16 TIMES .                                00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MHDARTYA     PIC X.                                     00001050
             03 MHDARTYC     PIC X.                                     00001060
             03 MHDARTYP     PIC X.                                     00001070
             03 MHDARTYH     PIC X.                                     00001080
             03 MHDARTYV     PIC X.                                     00001090
             03 MHDARTYO     PIC X(4).                                  00001100
           02 DFHMS2 OCCURS   16 TIMES .                                00001110
             03 FILLER       PIC X(2).                                  00001120
             03 MSELARTA     PIC X.                                     00001130
             03 MSELARTC     PIC X.                                     00001140
             03 MSELARTP     PIC X.                                     00001150
             03 MSELARTH     PIC X.                                     00001160
             03 MSELARTV     PIC X.                                     00001170
             03 MSELARTO     PIC X(5).                                  00001180
           02 DFHMS3 OCCURS   16 TIMES .                                00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MSELARDA     PIC X.                                     00001210
             03 MSELARDC     PIC X.                                     00001220
             03 MSELARDP     PIC X.                                     00001230
             03 MSELARDH     PIC X.                                     00001240
             03 MSELARDV     PIC X.                                     00001250
             03 MSELARDO     PIC X(5).                                  00001260
           02 DFHMS4 OCCURS   16 TIMES .                                00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MTYPEA  PIC X.                                          00001290
             03 MTYPEC  PIC X.                                          00001300
             03 MTYPEP  PIC X.                                          00001310
             03 MTYPEH  PIC X.                                          00001320
             03 MTYPEV  PIC X.                                          00001330
             03 MTYPEO  PIC X(5).                                       00001340
           02 DFHMS5 OCCURS   16 TIMES .                                00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MHDACEMA     PIC X.                                     00001370
             03 MHDACEMC     PIC X.                                     00001380
             03 MHDACEMP     PIC X.                                     00001390
             03 MHDACEMH     PIC X.                                     00001400
             03 MHDACEMV     PIC X.                                     00001410
             03 MHDACEMO     PIC X(4).                                  00001420
           02 DFHMS6 OCCURS   16 TIMES .                                00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MEC306A      PIC X.                                     00001450
             03 MEC306C PIC X.                                          00001460
             03 MEC306P PIC X.                                          00001470
             03 MEC306H PIC X.                                          00001480
             03 MEC306V PIC X.                                          00001490
             03 MEC306O      PIC X.                                     00001500
           02 DFHMS7 OCCURS   16 TIMES .                                00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MINDICEA     PIC X.                                     00001530
             03 MINDICEC     PIC X.                                     00001540
             03 MINDICEP     PIC X.                                     00001550
             03 MINDICEH     PIC X.                                     00001560
             03 MINDICEV     PIC X.                                     00001570
             03 MINDICEO     PIC X.                                     00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MLIBERRA  PIC X.                                          00001600
           02 MLIBERRC  PIC X.                                          00001610
           02 MLIBERRP  PIC X.                                          00001620
           02 MLIBERRH  PIC X.                                          00001630
           02 MLIBERRV  PIC X.                                          00001640
           02 MLIBERRO  PIC X(79).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCODTRAA  PIC X.                                          00001670
           02 MCODTRAC  PIC X.                                          00001680
           02 MCODTRAP  PIC X.                                          00001690
           02 MCODTRAH  PIC X.                                          00001700
           02 MCODTRAV  PIC X.                                          00001710
           02 MCODTRAO  PIC X(4).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MZONCMDA  PIC X.                                          00001740
           02 MZONCMDC  PIC X.                                          00001750
           02 MZONCMDP  PIC X.                                          00001760
           02 MZONCMDH  PIC X.                                          00001770
           02 MZONCMDV  PIC X.                                          00001780
           02 MZONCMDO  PIC X(15).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCICSA    PIC X.                                          00001810
           02 MCICSC    PIC X.                                          00001820
           02 MCICSP    PIC X.                                          00001830
           02 MCICSH    PIC X.                                          00001840
           02 MCICSV    PIC X.                                          00001850
           02 MCICSO    PIC X(5).                                       00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNETNAMA  PIC X.                                          00001880
           02 MNETNAMC  PIC X.                                          00001890
           02 MNETNAMP  PIC X.                                          00001900
           02 MNETNAMH  PIC X.                                          00001910
           02 MNETNAMV  PIC X.                                          00001920
           02 MNETNAMO  PIC X(8).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MSCREENA  PIC X.                                          00001950
           02 MSCREENC  PIC X.                                          00001960
           02 MSCREENP  PIC X.                                          00001970
           02 MSCREENH  PIC X.                                          00001980
           02 MSCREENV  PIC X.                                          00001990
           02 MSCREENO  PIC X(4).                                       00002000
                                                                                
