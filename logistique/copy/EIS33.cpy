      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EIE32   EIE32                                              00000020
      ***************************************************************** 00000030
       01   EIS33I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE JOUR TRAITEMENT                                            00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE TRAITEMENT                                                00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO SOCIETE                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNSOCI    PIC X(3).                                       00000200
      * NUMERO DE LIEU                                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOTL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEPOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEPOTF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEPOTI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MZONEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MZONEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MZONEI    PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSECTL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MSECTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSECTF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MSECTI    PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBELF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBELI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOC2L   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNSOC2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOC2F   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSOC2I   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEPOT2L  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDEPOT2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEPOT2F  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDEPOT2I  PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSSLIEUL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSSLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSSLIEUF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSSLIEUI  PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUTRTL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLIEUTRTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUTRTF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIEUTRTI      PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNCODICI  PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFCOL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLREFCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREFCOF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLREFCOI  PIC X(20).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCFAMI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLFAMI    PIC X(20).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCMARQI   PIC X(5).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLMARQI   PIC X(20).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MLREFFOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREFFOF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLREFFOI  PIC X(50).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTAL1L  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MTOTAL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTAL1F  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MTOTAL1I  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTAL2L  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MTOTAL2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTAL2F  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MTOTAL2I  PIC X(5).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTCKEML  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSTCKEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTCKEMF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSTCKEMI  PIC X(5).                                       00000930
      * ZONE MESSAGE                                                    00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(78).                                      00000980
      * CODE TRANSACTION                                                00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCODTRAI  PIC X(4).                                       00001030
      * NOM DU CICS                                                     00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MCICSI    PIC X(5).                                       00001080
      * NOM LIGNE VTAM                                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MNETNAMI  PIC X(8).                                       00001130
      * CODE TERMINAL                                                   00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EIE32   EIE32                                              00001200
      ***************************************************************** 00001210
       01   EIS33O REDEFINES EIS33I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
      * DATE JOUR TRAITEMENT                                            00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MDATJOUA  PIC X.                                          00001260
           02 MDATJOUC  PIC X.                                          00001270
           02 MDATJOUP  PIC X.                                          00001280
           02 MDATJOUH  PIC X.                                          00001290
           02 MDATJOUV  PIC X.                                          00001300
           02 MDATJOUO  PIC X(10).                                      00001310
      * HEURE TRAITEMENT                                                00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTIMJOUA  PIC X.                                          00001340
           02 MTIMJOUC  PIC X.                                          00001350
           02 MTIMJOUP  PIC X.                                          00001360
           02 MTIMJOUH  PIC X.                                          00001370
           02 MTIMJOUV  PIC X.                                          00001380
           02 MTIMJOUO  PIC X(5).                                       00001390
      * NUMERO SOCIETE                                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNSOCA    PIC X.                                          00001420
           02 MNSOCC    PIC X.                                          00001430
           02 MNSOCP    PIC X.                                          00001440
           02 MNSOCH    PIC X.                                          00001450
           02 MNSOCV    PIC X.                                          00001460
           02 MNSOCO    PIC X(3).                                       00001470
      * NUMERO DE LIEU                                                  00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDEPOTA   PIC X.                                          00001500
           02 MDEPOTC   PIC X.                                          00001510
           02 MDEPOTP   PIC X.                                          00001520
           02 MDEPOTH   PIC X.                                          00001530
           02 MDEPOTV   PIC X.                                          00001540
           02 MDEPOTO   PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MZONEA    PIC X.                                          00001570
           02 MZONEC    PIC X.                                          00001580
           02 MZONEP    PIC X.                                          00001590
           02 MZONEH    PIC X.                                          00001600
           02 MZONEV    PIC X.                                          00001610
           02 MZONEO    PIC X(2).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MSECTA    PIC X.                                          00001640
           02 MSECTC    PIC X.                                          00001650
           02 MSECTP    PIC X.                                          00001660
           02 MSECTH    PIC X.                                          00001670
           02 MSECTV    PIC X.                                          00001680
           02 MSECTO    PIC X(2).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLIBELA   PIC X.                                          00001710
           02 MLIBELC   PIC X.                                          00001720
           02 MLIBELP   PIC X.                                          00001730
           02 MLIBELH   PIC X.                                          00001740
           02 MLIBELV   PIC X.                                          00001750
           02 MLIBELO   PIC X(20).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNSOC2A   PIC X.                                          00001780
           02 MNSOC2C   PIC X.                                          00001790
           02 MNSOC2P   PIC X.                                          00001800
           02 MNSOC2H   PIC X.                                          00001810
           02 MNSOC2V   PIC X.                                          00001820
           02 MNSOC2O   PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDEPOT2A  PIC X.                                          00001850
           02 MDEPOT2C  PIC X.                                          00001860
           02 MDEPOT2P  PIC X.                                          00001870
           02 MDEPOT2H  PIC X.                                          00001880
           02 MDEPOT2V  PIC X.                                          00001890
           02 MDEPOT2O  PIC X(3).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MSSLIEUA  PIC X.                                          00001920
           02 MSSLIEUC  PIC X.                                          00001930
           02 MSSLIEUP  PIC X.                                          00001940
           02 MSSLIEUH  PIC X.                                          00001950
           02 MSSLIEUV  PIC X.                                          00001960
           02 MSSLIEUO  PIC X.                                          00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIEUTRTA      PIC X.                                     00001990
           02 MLIEUTRTC PIC X.                                          00002000
           02 MLIEUTRTP PIC X.                                          00002010
           02 MLIEUTRTH PIC X.                                          00002020
           02 MLIEUTRTV PIC X.                                          00002030
           02 MLIEUTRTO      PIC X(5).                                  00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNCODICA  PIC X.                                          00002060
           02 MNCODICC  PIC X.                                          00002070
           02 MNCODICP  PIC X.                                          00002080
           02 MNCODICH  PIC X.                                          00002090
           02 MNCODICV  PIC X.                                          00002100
           02 MNCODICO  PIC X(7).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MLREFCOA  PIC X.                                          00002130
           02 MLREFCOC  PIC X.                                          00002140
           02 MLREFCOP  PIC X.                                          00002150
           02 MLREFCOH  PIC X.                                          00002160
           02 MLREFCOV  PIC X.                                          00002170
           02 MLREFCOO  PIC X(20).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCFAMA    PIC X.                                          00002200
           02 MCFAMC    PIC X.                                          00002210
           02 MCFAMP    PIC X.                                          00002220
           02 MCFAMH    PIC X.                                          00002230
           02 MCFAMV    PIC X.                                          00002240
           02 MCFAMO    PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLFAMA    PIC X.                                          00002270
           02 MLFAMC    PIC X.                                          00002280
           02 MLFAMP    PIC X.                                          00002290
           02 MLFAMH    PIC X.                                          00002300
           02 MLFAMV    PIC X.                                          00002310
           02 MLFAMO    PIC X(20).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MCMARQA   PIC X.                                          00002340
           02 MCMARQC   PIC X.                                          00002350
           02 MCMARQP   PIC X.                                          00002360
           02 MCMARQH   PIC X.                                          00002370
           02 MCMARQV   PIC X.                                          00002380
           02 MCMARQO   PIC X(5).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLMARQA   PIC X.                                          00002410
           02 MLMARQC   PIC X.                                          00002420
           02 MLMARQP   PIC X.                                          00002430
           02 MLMARQH   PIC X.                                          00002440
           02 MLMARQV   PIC X.                                          00002450
           02 MLMARQO   PIC X(20).                                      00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MLREFFOA  PIC X.                                          00002480
           02 MLREFFOC  PIC X.                                          00002490
           02 MLREFFOP  PIC X.                                          00002500
           02 MLREFFOH  PIC X.                                          00002510
           02 MLREFFOV  PIC X.                                          00002520
           02 MLREFFOO  PIC X(50).                                      00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MTOTAL1A  PIC X.                                          00002550
           02 MTOTAL1C  PIC X.                                          00002560
           02 MTOTAL1P  PIC X.                                          00002570
           02 MTOTAL1H  PIC X.                                          00002580
           02 MTOTAL1V  PIC X.                                          00002590
           02 MTOTAL1O  PIC X(5).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MTOTAL2A  PIC X.                                          00002620
           02 MTOTAL2C  PIC X.                                          00002630
           02 MTOTAL2P  PIC X.                                          00002640
           02 MTOTAL2H  PIC X.                                          00002650
           02 MTOTAL2V  PIC X.                                          00002660
           02 MTOTAL2O  PIC X(5).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSTCKEMA  PIC X.                                          00002690
           02 MSTCKEMC  PIC X.                                          00002700
           02 MSTCKEMP  PIC X.                                          00002710
           02 MSTCKEMH  PIC X.                                          00002720
           02 MSTCKEMV  PIC X.                                          00002730
           02 MSTCKEMO  PIC X(5).                                       00002740
      * ZONE MESSAGE                                                    00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MLIBERRA  PIC X.                                          00002770
           02 MLIBERRC  PIC X.                                          00002780
           02 MLIBERRP  PIC X.                                          00002790
           02 MLIBERRH  PIC X.                                          00002800
           02 MLIBERRV  PIC X.                                          00002810
           02 MLIBERRO  PIC X(78).                                      00002820
      * CODE TRANSACTION                                                00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MCODTRAA  PIC X.                                          00002850
           02 MCODTRAC  PIC X.                                          00002860
           02 MCODTRAP  PIC X.                                          00002870
           02 MCODTRAH  PIC X.                                          00002880
           02 MCODTRAV  PIC X.                                          00002890
           02 MCODTRAO  PIC X(4).                                       00002900
      * NOM DU CICS                                                     00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MCICSA    PIC X.                                          00002930
           02 MCICSC    PIC X.                                          00002940
           02 MCICSP    PIC X.                                          00002950
           02 MCICSH    PIC X.                                          00002960
           02 MCICSV    PIC X.                                          00002970
           02 MCICSO    PIC X(5).                                       00002980
      * NOM LIGNE VTAM                                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNETNAMA  PIC X.                                          00003010
           02 MNETNAMC  PIC X.                                          00003020
           02 MNETNAMP  PIC X.                                          00003030
           02 MNETNAMH  PIC X.                                          00003040
           02 MNETNAMV  PIC X.                                          00003050
           02 MNETNAMO  PIC X(8).                                       00003060
      * CODE TERMINAL                                                   00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MSCREENA  PIC X.                                          00003090
           02 MSCREENC  PIC X.                                          00003100
           02 MSCREENP  PIC X.                                          00003110
           02 MSCREENH  PIC X.                                          00003120
           02 MSCREENV  PIC X.                                          00003130
           02 MSCREENO  PIC X(4).                                       00003140
                                                                                
