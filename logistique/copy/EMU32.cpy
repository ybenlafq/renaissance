      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU32   EMU32                                              00000020
      ***************************************************************** 00000030
       01   EMU32I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTSOCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNENTSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNENTSOCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNENTSOCI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTLIEUL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNENTLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNENTLIEUF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNENTLIEUI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELARTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCSELARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSELARTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCSELARTI      PIC X(5).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGSOCL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNMAGSOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGSOCF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNMAGSOCI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGLIEUL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNMAGLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNMAGLIEUF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMAGLIEUI     PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGLIBL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNMAGLIBL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGLIBF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNMAGLIBI      PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEE1L      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDANNEE1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNEE1F      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDANNEE1I      PIC X(4).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEMAINE1L    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDSEMAINE1L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSEMAINE1F    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDSEMAINE1I    PIC X(2).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEE2L      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDANNEE2L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNEE2F      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDANNEE2I      PIC X(4).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEMAINE2L    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MDSEMAINE2L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSEMAINE2F    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDSEMAINE2I    PIC X(2).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEE3L      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDANNEE3L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNEE3F      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDANNEE3I      PIC X(4).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEMAINE3L    COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MDSEMAINE3L COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDSEMAINE3F    PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDSEMAINE3I    PIC X(2).                                  00000650
           02 MJJMMI OCCURS   2 TIMES .                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMLUN1L    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MDJMLUN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMLUN1F    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDJMLUN1I    PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMAR1L    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MDJMMAR1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMAR1F    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDJMMAR1I    PIC X(2).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMER1L    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MDJMMER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMER1F    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDJMMER1I    PIC X(2).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMJEU1L    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MDJMJEU1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMJEU1F    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MDJMJEU1I    PIC X(2).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMVEN1L    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MDJMVEN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMVEN1F    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MDJMVEN1I    PIC X(2).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMSAM1L    COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MDJMSAM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMSAM1F    PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MDJMSAM1I    PIC X(2).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMDIM1L    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MDJMDIM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMDIM1F    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MDJMDIM1I    PIC X(2).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMLUN2L    COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MDJMLUN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMLUN2F    PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MDJMLUN2I    PIC X(2).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMAR2L    COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MDJMMAR2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMAR2F    PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDJMMAR2I    PIC X(2).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMER2L    COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MDJMMER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMER2F    PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MDJMMER2I    PIC X(2).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMJEU2L    COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MDJMJEU2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMJEU2F    PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDJMJEU2I    PIC X(2).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMVEN2L    COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MDJMVEN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMVEN2F    PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDJMVEN2I    PIC X(2).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMSAM2L    COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MDJMSAM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMSAM2F    PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MDJMSAM2I    PIC X(2).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMDIM2L    COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MDJMDIM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMDIM2F    PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MDJMDIM2I    PIC X(2).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMLUN3L    COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MDJMLUN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMLUN3F    PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MDJMLUN3I    PIC X(2).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMAR3L    COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MDJMMAR3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMAR3F    PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MDJMMAR3I    PIC X(2).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMMER3L    COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MDJMMER3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMMER3F    PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MDJMMER3I    PIC X(2).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMJEU3L    COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MDJMJEU3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMJEU3F    PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MDJMJEU3I    PIC X(2).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMVEN3L    COMP PIC S9(4).                            00001390
      *--                                                                       
             03 MDJMVEN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMVEN3F    PIC X.                                     00001400
             03 FILLER  PIC X(4).                                       00001410
             03 MDJMVEN3I    PIC X(2).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMSAM3L    COMP PIC S9(4).                            00001430
      *--                                                                       
             03 MDJMSAM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMSAM3F    PIC X.                                     00001440
             03 FILLER  PIC X(4).                                       00001450
             03 MDJMSAM3I    PIC X(2).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDJMDIM3L    COMP PIC S9(4).                            00001470
      *--                                                                       
             03 MDJMDIM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDJMDIM3F    PIC X.                                     00001480
             03 FILLER  PIC X(4).                                       00001490
             03 MDJMDIM3I    PIC X(2).                                  00001500
           02 MCODESI OCCURS   8 TIMES .                                00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODLUN1L    COMP PIC S9(4).                            00001520
      *--                                                                       
             03 MCODLUN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODLUN1F    PIC X.                                     00001530
             03 FILLER  PIC X(4).                                       00001540
             03 MCODLUN1I    PIC X(2).                                  00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAR1L    COMP PIC S9(4).                            00001560
      *--                                                                       
             03 MCODMAR1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMAR1F    PIC X.                                     00001570
             03 FILLER  PIC X(4).                                       00001580
             03 MCODMAR1I    PIC X(2).                                  00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMER1L    COMP PIC S9(4).                            00001600
      *--                                                                       
             03 MCODMER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMER1F    PIC X.                                     00001610
             03 FILLER  PIC X(4).                                       00001620
             03 MCODMER1I    PIC X(2).                                  00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODJEU1L    COMP PIC S9(4).                            00001640
      *--                                                                       
             03 MCODJEU1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODJEU1F    PIC X.                                     00001650
             03 FILLER  PIC X(4).                                       00001660
             03 MCODJEU1I    PIC X(2).                                  00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODVEN1L    COMP PIC S9(4).                            00001680
      *--                                                                       
             03 MCODVEN1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODVEN1F    PIC X.                                     00001690
             03 FILLER  PIC X(4).                                       00001700
             03 MCODVEN1I    PIC X(2).                                  00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSAM1L    COMP PIC S9(4).                            00001720
      *--                                                                       
             03 MCODSAM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODSAM1F    PIC X.                                     00001730
             03 FILLER  PIC X(4).                                       00001740
             03 MCODSAM1I    PIC X(2).                                  00001750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDIM1L    COMP PIC S9(4).                            00001760
      *--                                                                       
             03 MCODDIM1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODDIM1F    PIC X.                                     00001770
             03 FILLER  PIC X(4).                                       00001780
             03 MCODDIM1I    PIC X(2).                                  00001790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODLUN2L    COMP PIC S9(4).                            00001800
      *--                                                                       
             03 MCODLUN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODLUN2F    PIC X.                                     00001810
             03 FILLER  PIC X(4).                                       00001820
             03 MCODLUN2I    PIC X(2).                                  00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAR2L    COMP PIC S9(4).                            00001840
      *--                                                                       
             03 MCODMAR2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMAR2F    PIC X.                                     00001850
             03 FILLER  PIC X(4).                                       00001860
             03 MCODMAR2I    PIC X(2).                                  00001870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMER2L    COMP PIC S9(4).                            00001880
      *--                                                                       
             03 MCODMER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMER2F    PIC X.                                     00001890
             03 FILLER  PIC X(4).                                       00001900
             03 MCODMER2I    PIC X(2).                                  00001910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODJEU2L    COMP PIC S9(4).                            00001920
      *--                                                                       
             03 MCODJEU2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODJEU2F    PIC X.                                     00001930
             03 FILLER  PIC X(4).                                       00001940
             03 MCODJEU2I    PIC X(2).                                  00001950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODVEN2L    COMP PIC S9(4).                            00001960
      *--                                                                       
             03 MCODVEN2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODVEN2F    PIC X.                                     00001970
             03 FILLER  PIC X(4).                                       00001980
             03 MCODVEN2I    PIC X(2).                                  00001990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSAM2L    COMP PIC S9(4).                            00002000
      *--                                                                       
             03 MCODSAM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODSAM2F    PIC X.                                     00002010
             03 FILLER  PIC X(4).                                       00002020
             03 MCODSAM2I    PIC X(2).                                  00002030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDIM2L    COMP PIC S9(4).                            00002040
      *--                                                                       
             03 MCODDIM2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODDIM2F    PIC X.                                     00002050
             03 FILLER  PIC X(4).                                       00002060
             03 MCODDIM2I    PIC X(2).                                  00002070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODLUN3L    COMP PIC S9(4).                            00002080
      *--                                                                       
             03 MCODLUN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODLUN3F    PIC X.                                     00002090
             03 FILLER  PIC X(4).                                       00002100
             03 MCODLUN3I    PIC X(2).                                  00002110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMAR3L    COMP PIC S9(4).                            00002120
      *--                                                                       
             03 MCODMAR3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMAR3F    PIC X.                                     00002130
             03 FILLER  PIC X(4).                                       00002140
             03 MCODMAR3I    PIC X(2).                                  00002150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODMER3L    COMP PIC S9(4).                            00002160
      *--                                                                       
             03 MCODMER3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODMER3F    PIC X.                                     00002170
             03 FILLER  PIC X(4).                                       00002180
             03 MCODMER3I    PIC X(2).                                  00002190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODJEU3L    COMP PIC S9(4).                            00002200
      *--                                                                       
             03 MCODJEU3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODJEU3F    PIC X.                                     00002210
             03 FILLER  PIC X(4).                                       00002220
             03 MCODJEU3I    PIC X(2).                                  00002230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODVEN3L    COMP PIC S9(4).                            00002240
      *--                                                                       
             03 MCODVEN3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODVEN3F    PIC X.                                     00002250
             03 FILLER  PIC X(4).                                       00002260
             03 MCODVEN3I    PIC X(2).                                  00002270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODSAM3L    COMP PIC S9(4).                            00002280
      *--                                                                       
             03 MCODSAM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODSAM3F    PIC X.                                     00002290
             03 FILLER  PIC X(4).                                       00002300
             03 MCODSAM3I    PIC X(2).                                  00002310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODDIM3L    COMP PIC S9(4).                            00002320
      *--                                                                       
             03 MCODDIM3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCODDIM3F    PIC X.                                     00002330
             03 FILLER  PIC X(4).                                       00002340
             03 MCODDIM3I    PIC X(2).                                  00002350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002360
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002370
           02 FILLER    PIC X(4).                                       00002380
           02 MZONCMDI  PIC X(15).                                      00002390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002400
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002410
           02 FILLER    PIC X(4).                                       00002420
           02 MLIBERRI  PIC X(58).                                      00002430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002440
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002450
           02 FILLER    PIC X(4).                                       00002460
           02 MCODTRAI  PIC X(4).                                       00002470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002480
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002490
           02 FILLER    PIC X(4).                                       00002500
           02 MCICSI    PIC X(5).                                       00002510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002520
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002530
           02 FILLER    PIC X(4).                                       00002540
           02 MNETNAMI  PIC X(8).                                       00002550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002560
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002570
           02 FILLER    PIC X(4).                                       00002580
           02 MSCREENI  PIC X(4).                                       00002590
      ***************************************************************** 00002600
      * SDF: EMU32   EMU32                                              00002610
      ***************************************************************** 00002620
       01   EMU32O REDEFINES EMU32I.                                    00002630
           02 FILLER    PIC X(12).                                      00002640
           02 FILLER    PIC X(2).                                       00002650
           02 MDATJOUA  PIC X.                                          00002660
           02 MDATJOUC  PIC X.                                          00002670
           02 MDATJOUP  PIC X.                                          00002680
           02 MDATJOUH  PIC X.                                          00002690
           02 MDATJOUV  PIC X.                                          00002700
           02 MDATJOUO  PIC X(10).                                      00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MTIMJOUA  PIC X.                                          00002730
           02 MTIMJOUC  PIC X.                                          00002740
           02 MTIMJOUP  PIC X.                                          00002750
           02 MTIMJOUH  PIC X.                                          00002760
           02 MTIMJOUV  PIC X.                                          00002770
           02 MTIMJOUO  PIC X(5).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MNPAGEA   PIC X.                                          00002800
           02 MNPAGEC   PIC X.                                          00002810
           02 MNPAGEP   PIC X.                                          00002820
           02 MNPAGEH   PIC X.                                          00002830
           02 MNPAGEV   PIC X.                                          00002840
           02 MNPAGEO   PIC X(2).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MNENTSOCA      PIC X.                                     00002870
           02 MNENTSOCC PIC X.                                          00002880
           02 MNENTSOCP PIC X.                                          00002890
           02 MNENTSOCH PIC X.                                          00002900
           02 MNENTSOCV PIC X.                                          00002910
           02 MNENTSOCO      PIC X(3).                                  00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNENTLIEUA     PIC X.                                     00002940
           02 MNENTLIEUC     PIC X.                                     00002950
           02 MNENTLIEUP     PIC X.                                     00002960
           02 MNENTLIEUH     PIC X.                                     00002970
           02 MNENTLIEUV     PIC X.                                     00002980
           02 MNENTLIEUO     PIC X(3).                                  00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCSELARTA      PIC X.                                     00003010
           02 MCSELARTC PIC X.                                          00003020
           02 MCSELARTP PIC X.                                          00003030
           02 MCSELARTH PIC X.                                          00003040
           02 MCSELARTV PIC X.                                          00003050
           02 MCSELARTO      PIC X(5).                                  00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNMAGSOCA      PIC X.                                     00003080
           02 MNMAGSOCC PIC X.                                          00003090
           02 MNMAGSOCP PIC X.                                          00003100
           02 MNMAGSOCH PIC X.                                          00003110
           02 MNMAGSOCV PIC X.                                          00003120
           02 MNMAGSOCO      PIC X(3).                                  00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MNMAGLIEUA     PIC X.                                     00003150
           02 MNMAGLIEUC     PIC X.                                     00003160
           02 MNMAGLIEUP     PIC X.                                     00003170
           02 MNMAGLIEUH     PIC X.                                     00003180
           02 MNMAGLIEUV     PIC X.                                     00003190
           02 MNMAGLIEUO     PIC X(3).                                  00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MNMAGLIBA      PIC X.                                     00003220
           02 MNMAGLIBC PIC X.                                          00003230
           02 MNMAGLIBP PIC X.                                          00003240
           02 MNMAGLIBH PIC X.                                          00003250
           02 MNMAGLIBV PIC X.                                          00003260
           02 MNMAGLIBO      PIC X(20).                                 00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MDANNEE1A      PIC X.                                     00003290
           02 MDANNEE1C PIC X.                                          00003300
           02 MDANNEE1P PIC X.                                          00003310
           02 MDANNEE1H PIC X.                                          00003320
           02 MDANNEE1V PIC X.                                          00003330
           02 MDANNEE1O      PIC X(4).                                  00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MDSEMAINE1A    PIC X.                                     00003360
           02 MDSEMAINE1C    PIC X.                                     00003370
           02 MDSEMAINE1P    PIC X.                                     00003380
           02 MDSEMAINE1H    PIC X.                                     00003390
           02 MDSEMAINE1V    PIC X.                                     00003400
           02 MDSEMAINE1O    PIC X(2).                                  00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MDANNEE2A      PIC X.                                     00003430
           02 MDANNEE2C PIC X.                                          00003440
           02 MDANNEE2P PIC X.                                          00003450
           02 MDANNEE2H PIC X.                                          00003460
           02 MDANNEE2V PIC X.                                          00003470
           02 MDANNEE2O      PIC X(4).                                  00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MDSEMAINE2A    PIC X.                                     00003500
           02 MDSEMAINE2C    PIC X.                                     00003510
           02 MDSEMAINE2P    PIC X.                                     00003520
           02 MDSEMAINE2H    PIC X.                                     00003530
           02 MDSEMAINE2V    PIC X.                                     00003540
           02 MDSEMAINE2O    PIC X(2).                                  00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MDANNEE3A      PIC X.                                     00003570
           02 MDANNEE3C PIC X.                                          00003580
           02 MDANNEE3P PIC X.                                          00003590
           02 MDANNEE3H PIC X.                                          00003600
           02 MDANNEE3V PIC X.                                          00003610
           02 MDANNEE3O      PIC X(4).                                  00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MDSEMAINE3A    PIC X.                                     00003640
           02 MDSEMAINE3C    PIC X.                                     00003650
           02 MDSEMAINE3P    PIC X.                                     00003660
           02 MDSEMAINE3H    PIC X.                                     00003670
           02 MDSEMAINE3V    PIC X.                                     00003680
           02 MDSEMAINE3O    PIC X(2).                                  00003690
           02 MJJMMO OCCURS   2 TIMES .                                 00003700
             03 FILLER       PIC X(2).                                  00003710
             03 MDJMLUN1A    PIC X.                                     00003720
             03 MDJMLUN1C    PIC X.                                     00003730
             03 MDJMLUN1P    PIC X.                                     00003740
             03 MDJMLUN1H    PIC X.                                     00003750
             03 MDJMLUN1V    PIC X.                                     00003760
             03 MDJMLUN1O    PIC X(2).                                  00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MDJMMAR1A    PIC X.                                     00003790
             03 MDJMMAR1C    PIC X.                                     00003800
             03 MDJMMAR1P    PIC X.                                     00003810
             03 MDJMMAR1H    PIC X.                                     00003820
             03 MDJMMAR1V    PIC X.                                     00003830
             03 MDJMMAR1O    PIC X(2).                                  00003840
             03 FILLER       PIC X(2).                                  00003850
             03 MDJMMER1A    PIC X.                                     00003860
             03 MDJMMER1C    PIC X.                                     00003870
             03 MDJMMER1P    PIC X.                                     00003880
             03 MDJMMER1H    PIC X.                                     00003890
             03 MDJMMER1V    PIC X.                                     00003900
             03 MDJMMER1O    PIC X(2).                                  00003910
             03 FILLER       PIC X(2).                                  00003920
             03 MDJMJEU1A    PIC X.                                     00003930
             03 MDJMJEU1C    PIC X.                                     00003940
             03 MDJMJEU1P    PIC X.                                     00003950
             03 MDJMJEU1H    PIC X.                                     00003960
             03 MDJMJEU1V    PIC X.                                     00003970
             03 MDJMJEU1O    PIC X(2).                                  00003980
             03 FILLER       PIC X(2).                                  00003990
             03 MDJMVEN1A    PIC X.                                     00004000
             03 MDJMVEN1C    PIC X.                                     00004010
             03 MDJMVEN1P    PIC X.                                     00004020
             03 MDJMVEN1H    PIC X.                                     00004030
             03 MDJMVEN1V    PIC X.                                     00004040
             03 MDJMVEN1O    PIC X(2).                                  00004050
             03 FILLER       PIC X(2).                                  00004060
             03 MDJMSAM1A    PIC X.                                     00004070
             03 MDJMSAM1C    PIC X.                                     00004080
             03 MDJMSAM1P    PIC X.                                     00004090
             03 MDJMSAM1H    PIC X.                                     00004100
             03 MDJMSAM1V    PIC X.                                     00004110
             03 MDJMSAM1O    PIC X(2).                                  00004120
             03 FILLER       PIC X(2).                                  00004130
             03 MDJMDIM1A    PIC X.                                     00004140
             03 MDJMDIM1C    PIC X.                                     00004150
             03 MDJMDIM1P    PIC X.                                     00004160
             03 MDJMDIM1H    PIC X.                                     00004170
             03 MDJMDIM1V    PIC X.                                     00004180
             03 MDJMDIM1O    PIC X(2).                                  00004190
             03 FILLER       PIC X(2).                                  00004200
             03 MDJMLUN2A    PIC X.                                     00004210
             03 MDJMLUN2C    PIC X.                                     00004220
             03 MDJMLUN2P    PIC X.                                     00004230
             03 MDJMLUN2H    PIC X.                                     00004240
             03 MDJMLUN2V    PIC X.                                     00004250
             03 MDJMLUN2O    PIC X(2).                                  00004260
             03 FILLER       PIC X(2).                                  00004270
             03 MDJMMAR2A    PIC X.                                     00004280
             03 MDJMMAR2C    PIC X.                                     00004290
             03 MDJMMAR2P    PIC X.                                     00004300
             03 MDJMMAR2H    PIC X.                                     00004310
             03 MDJMMAR2V    PIC X.                                     00004320
             03 MDJMMAR2O    PIC X(2).                                  00004330
             03 FILLER       PIC X(2).                                  00004340
             03 MDJMMER2A    PIC X.                                     00004350
             03 MDJMMER2C    PIC X.                                     00004360
             03 MDJMMER2P    PIC X.                                     00004370
             03 MDJMMER2H    PIC X.                                     00004380
             03 MDJMMER2V    PIC X.                                     00004390
             03 MDJMMER2O    PIC X(2).                                  00004400
             03 FILLER       PIC X(2).                                  00004410
             03 MDJMJEU2A    PIC X.                                     00004420
             03 MDJMJEU2C    PIC X.                                     00004430
             03 MDJMJEU2P    PIC X.                                     00004440
             03 MDJMJEU2H    PIC X.                                     00004450
             03 MDJMJEU2V    PIC X.                                     00004460
             03 MDJMJEU2O    PIC X(2).                                  00004470
             03 FILLER       PIC X(2).                                  00004480
             03 MDJMVEN2A    PIC X.                                     00004490
             03 MDJMVEN2C    PIC X.                                     00004500
             03 MDJMVEN2P    PIC X.                                     00004510
             03 MDJMVEN2H    PIC X.                                     00004520
             03 MDJMVEN2V    PIC X.                                     00004530
             03 MDJMVEN2O    PIC X(2).                                  00004540
             03 FILLER       PIC X(2).                                  00004550
             03 MDJMSAM2A    PIC X.                                     00004560
             03 MDJMSAM2C    PIC X.                                     00004570
             03 MDJMSAM2P    PIC X.                                     00004580
             03 MDJMSAM2H    PIC X.                                     00004590
             03 MDJMSAM2V    PIC X.                                     00004600
             03 MDJMSAM2O    PIC X(2).                                  00004610
             03 FILLER       PIC X(2).                                  00004620
             03 MDJMDIM2A    PIC X.                                     00004630
             03 MDJMDIM2C    PIC X.                                     00004640
             03 MDJMDIM2P    PIC X.                                     00004650
             03 MDJMDIM2H    PIC X.                                     00004660
             03 MDJMDIM2V    PIC X.                                     00004670
             03 MDJMDIM2O    PIC X(2).                                  00004680
             03 FILLER       PIC X(2).                                  00004690
             03 MDJMLUN3A    PIC X.                                     00004700
             03 MDJMLUN3C    PIC X.                                     00004710
             03 MDJMLUN3P    PIC X.                                     00004720
             03 MDJMLUN3H    PIC X.                                     00004730
             03 MDJMLUN3V    PIC X.                                     00004740
             03 MDJMLUN3O    PIC X(2).                                  00004750
             03 FILLER       PIC X(2).                                  00004760
             03 MDJMMAR3A    PIC X.                                     00004770
             03 MDJMMAR3C    PIC X.                                     00004780
             03 MDJMMAR3P    PIC X.                                     00004790
             03 MDJMMAR3H    PIC X.                                     00004800
             03 MDJMMAR3V    PIC X.                                     00004810
             03 MDJMMAR3O    PIC X(2).                                  00004820
             03 FILLER       PIC X(2).                                  00004830
             03 MDJMMER3A    PIC X.                                     00004840
             03 MDJMMER3C    PIC X.                                     00004850
             03 MDJMMER3P    PIC X.                                     00004860
             03 MDJMMER3H    PIC X.                                     00004870
             03 MDJMMER3V    PIC X.                                     00004880
             03 MDJMMER3O    PIC X(2).                                  00004890
             03 FILLER       PIC X(2).                                  00004900
             03 MDJMJEU3A    PIC X.                                     00004910
             03 MDJMJEU3C    PIC X.                                     00004920
             03 MDJMJEU3P    PIC X.                                     00004930
             03 MDJMJEU3H    PIC X.                                     00004940
             03 MDJMJEU3V    PIC X.                                     00004950
             03 MDJMJEU3O    PIC X(2).                                  00004960
             03 FILLER       PIC X(2).                                  00004970
             03 MDJMVEN3A    PIC X.                                     00004980
             03 MDJMVEN3C    PIC X.                                     00004990
             03 MDJMVEN3P    PIC X.                                     00005000
             03 MDJMVEN3H    PIC X.                                     00005010
             03 MDJMVEN3V    PIC X.                                     00005020
             03 MDJMVEN3O    PIC X(2).                                  00005030
             03 FILLER       PIC X(2).                                  00005040
             03 MDJMSAM3A    PIC X.                                     00005050
             03 MDJMSAM3C    PIC X.                                     00005060
             03 MDJMSAM3P    PIC X.                                     00005070
             03 MDJMSAM3H    PIC X.                                     00005080
             03 MDJMSAM3V    PIC X.                                     00005090
             03 MDJMSAM3O    PIC X(2).                                  00005100
             03 FILLER       PIC X(2).                                  00005110
             03 MDJMDIM3A    PIC X.                                     00005120
             03 MDJMDIM3C    PIC X.                                     00005130
             03 MDJMDIM3P    PIC X.                                     00005140
             03 MDJMDIM3H    PIC X.                                     00005150
             03 MDJMDIM3V    PIC X.                                     00005160
             03 MDJMDIM3O    PIC X(2).                                  00005170
           02 MCODESO OCCURS   8 TIMES .                                00005180
             03 FILLER       PIC X(2).                                  00005190
             03 MCODLUN1A    PIC X.                                     00005200
             03 MCODLUN1C    PIC X.                                     00005210
             03 MCODLUN1P    PIC X.                                     00005220
             03 MCODLUN1H    PIC X.                                     00005230
             03 MCODLUN1V    PIC X.                                     00005240
             03 MCODLUN1O    PIC X(2).                                  00005250
             03 FILLER       PIC X(2).                                  00005260
             03 MCODMAR1A    PIC X.                                     00005270
             03 MCODMAR1C    PIC X.                                     00005280
             03 MCODMAR1P    PIC X.                                     00005290
             03 MCODMAR1H    PIC X.                                     00005300
             03 MCODMAR1V    PIC X.                                     00005310
             03 MCODMAR1O    PIC X(2).                                  00005320
             03 FILLER       PIC X(2).                                  00005330
             03 MCODMER1A    PIC X.                                     00005340
             03 MCODMER1C    PIC X.                                     00005350
             03 MCODMER1P    PIC X.                                     00005360
             03 MCODMER1H    PIC X.                                     00005370
             03 MCODMER1V    PIC X.                                     00005380
             03 MCODMER1O    PIC X(2).                                  00005390
             03 FILLER       PIC X(2).                                  00005400
             03 MCODJEU1A    PIC X.                                     00005410
             03 MCODJEU1C    PIC X.                                     00005420
             03 MCODJEU1P    PIC X.                                     00005430
             03 MCODJEU1H    PIC X.                                     00005440
             03 MCODJEU1V    PIC X.                                     00005450
             03 MCODJEU1O    PIC X(2).                                  00005460
             03 FILLER       PIC X(2).                                  00005470
             03 MCODVEN1A    PIC X.                                     00005480
             03 MCODVEN1C    PIC X.                                     00005490
             03 MCODVEN1P    PIC X.                                     00005500
             03 MCODVEN1H    PIC X.                                     00005510
             03 MCODVEN1V    PIC X.                                     00005520
             03 MCODVEN1O    PIC X(2).                                  00005530
             03 FILLER       PIC X(2).                                  00005540
             03 MCODSAM1A    PIC X.                                     00005550
             03 MCODSAM1C    PIC X.                                     00005560
             03 MCODSAM1P    PIC X.                                     00005570
             03 MCODSAM1H    PIC X.                                     00005580
             03 MCODSAM1V    PIC X.                                     00005590
             03 MCODSAM1O    PIC X(2).                                  00005600
             03 FILLER       PIC X(2).                                  00005610
             03 MCODDIM1A    PIC X.                                     00005620
             03 MCODDIM1C    PIC X.                                     00005630
             03 MCODDIM1P    PIC X.                                     00005640
             03 MCODDIM1H    PIC X.                                     00005650
             03 MCODDIM1V    PIC X.                                     00005660
             03 MCODDIM1O    PIC X(2).                                  00005670
             03 FILLER       PIC X(2).                                  00005680
             03 MCODLUN2A    PIC X.                                     00005690
             03 MCODLUN2C    PIC X.                                     00005700
             03 MCODLUN2P    PIC X.                                     00005710
             03 MCODLUN2H    PIC X.                                     00005720
             03 MCODLUN2V    PIC X.                                     00005730
             03 MCODLUN2O    PIC X(2).                                  00005740
             03 FILLER       PIC X(2).                                  00005750
             03 MCODMAR2A    PIC X.                                     00005760
             03 MCODMAR2C    PIC X.                                     00005770
             03 MCODMAR2P    PIC X.                                     00005780
             03 MCODMAR2H    PIC X.                                     00005790
             03 MCODMAR2V    PIC X.                                     00005800
             03 MCODMAR2O    PIC X(2).                                  00005810
             03 FILLER       PIC X(2).                                  00005820
             03 MCODMER2A    PIC X.                                     00005830
             03 MCODMER2C    PIC X.                                     00005840
             03 MCODMER2P    PIC X.                                     00005850
             03 MCODMER2H    PIC X.                                     00005860
             03 MCODMER2V    PIC X.                                     00005870
             03 MCODMER2O    PIC X(2).                                  00005880
             03 FILLER       PIC X(2).                                  00005890
             03 MCODJEU2A    PIC X.                                     00005900
             03 MCODJEU2C    PIC X.                                     00005910
             03 MCODJEU2P    PIC X.                                     00005920
             03 MCODJEU2H    PIC X.                                     00005930
             03 MCODJEU2V    PIC X.                                     00005940
             03 MCODJEU2O    PIC X(2).                                  00005950
             03 FILLER       PIC X(2).                                  00005960
             03 MCODVEN2A    PIC X.                                     00005970
             03 MCODVEN2C    PIC X.                                     00005980
             03 MCODVEN2P    PIC X.                                     00005990
             03 MCODVEN2H    PIC X.                                     00006000
             03 MCODVEN2V    PIC X.                                     00006010
             03 MCODVEN2O    PIC X(2).                                  00006020
             03 FILLER       PIC X(2).                                  00006030
             03 MCODSAM2A    PIC X.                                     00006040
             03 MCODSAM2C    PIC X.                                     00006050
             03 MCODSAM2P    PIC X.                                     00006060
             03 MCODSAM2H    PIC X.                                     00006070
             03 MCODSAM2V    PIC X.                                     00006080
             03 MCODSAM2O    PIC X(2).                                  00006090
             03 FILLER       PIC X(2).                                  00006100
             03 MCODDIM2A    PIC X.                                     00006110
             03 MCODDIM2C    PIC X.                                     00006120
             03 MCODDIM2P    PIC X.                                     00006130
             03 MCODDIM2H    PIC X.                                     00006140
             03 MCODDIM2V    PIC X.                                     00006150
             03 MCODDIM2O    PIC X(2).                                  00006160
             03 FILLER       PIC X(2).                                  00006170
             03 MCODLUN3A    PIC X.                                     00006180
             03 MCODLUN3C    PIC X.                                     00006190
             03 MCODLUN3P    PIC X.                                     00006200
             03 MCODLUN3H    PIC X.                                     00006210
             03 MCODLUN3V    PIC X.                                     00006220
             03 MCODLUN3O    PIC X(2).                                  00006230
             03 FILLER       PIC X(2).                                  00006240
             03 MCODMAR3A    PIC X.                                     00006250
             03 MCODMAR3C    PIC X.                                     00006260
             03 MCODMAR3P    PIC X.                                     00006270
             03 MCODMAR3H    PIC X.                                     00006280
             03 MCODMAR3V    PIC X.                                     00006290
             03 MCODMAR3O    PIC X(2).                                  00006300
             03 FILLER       PIC X(2).                                  00006310
             03 MCODMER3A    PIC X.                                     00006320
             03 MCODMER3C    PIC X.                                     00006330
             03 MCODMER3P    PIC X.                                     00006340
             03 MCODMER3H    PIC X.                                     00006350
             03 MCODMER3V    PIC X.                                     00006360
             03 MCODMER3O    PIC X(2).                                  00006370
             03 FILLER       PIC X(2).                                  00006380
             03 MCODJEU3A    PIC X.                                     00006390
             03 MCODJEU3C    PIC X.                                     00006400
             03 MCODJEU3P    PIC X.                                     00006410
             03 MCODJEU3H    PIC X.                                     00006420
             03 MCODJEU3V    PIC X.                                     00006430
             03 MCODJEU3O    PIC X(2).                                  00006440
             03 FILLER       PIC X(2).                                  00006450
             03 MCODVEN3A    PIC X.                                     00006460
             03 MCODVEN3C    PIC X.                                     00006470
             03 MCODVEN3P    PIC X.                                     00006480
             03 MCODVEN3H    PIC X.                                     00006490
             03 MCODVEN3V    PIC X.                                     00006500
             03 MCODVEN3O    PIC X(2).                                  00006510
             03 FILLER       PIC X(2).                                  00006520
             03 MCODSAM3A    PIC X.                                     00006530
             03 MCODSAM3C    PIC X.                                     00006540
             03 MCODSAM3P    PIC X.                                     00006550
             03 MCODSAM3H    PIC X.                                     00006560
             03 MCODSAM3V    PIC X.                                     00006570
             03 MCODSAM3O    PIC X(2).                                  00006580
             03 FILLER       PIC X(2).                                  00006590
             03 MCODDIM3A    PIC X.                                     00006600
             03 MCODDIM3C    PIC X.                                     00006610
             03 MCODDIM3P    PIC X.                                     00006620
             03 MCODDIM3H    PIC X.                                     00006630
             03 MCODDIM3V    PIC X.                                     00006640
             03 MCODDIM3O    PIC X(2).                                  00006650
           02 FILLER    PIC X(2).                                       00006660
           02 MZONCMDA  PIC X.                                          00006670
           02 MZONCMDC  PIC X.                                          00006680
           02 MZONCMDP  PIC X.                                          00006690
           02 MZONCMDH  PIC X.                                          00006700
           02 MZONCMDV  PIC X.                                          00006710
           02 MZONCMDO  PIC X(15).                                      00006720
           02 FILLER    PIC X(2).                                       00006730
           02 MLIBERRA  PIC X.                                          00006740
           02 MLIBERRC  PIC X.                                          00006750
           02 MLIBERRP  PIC X.                                          00006760
           02 MLIBERRH  PIC X.                                          00006770
           02 MLIBERRV  PIC X.                                          00006780
           02 MLIBERRO  PIC X(58).                                      00006790
           02 FILLER    PIC X(2).                                       00006800
           02 MCODTRAA  PIC X.                                          00006810
           02 MCODTRAC  PIC X.                                          00006820
           02 MCODTRAP  PIC X.                                          00006830
           02 MCODTRAH  PIC X.                                          00006840
           02 MCODTRAV  PIC X.                                          00006850
           02 MCODTRAO  PIC X(4).                                       00006860
           02 FILLER    PIC X(2).                                       00006870
           02 MCICSA    PIC X.                                          00006880
           02 MCICSC    PIC X.                                          00006890
           02 MCICSP    PIC X.                                          00006900
           02 MCICSH    PIC X.                                          00006910
           02 MCICSV    PIC X.                                          00006920
           02 MCICSO    PIC X(5).                                       00006930
           02 FILLER    PIC X(2).                                       00006940
           02 MNETNAMA  PIC X.                                          00006950
           02 MNETNAMC  PIC X.                                          00006960
           02 MNETNAMP  PIC X.                                          00006970
           02 MNETNAMH  PIC X.                                          00006980
           02 MNETNAMV  PIC X.                                          00006990
           02 MNETNAMO  PIC X(8).                                       00007000
           02 FILLER    PIC X(2).                                       00007010
           02 MSCREENA  PIC X.                                          00007020
           02 MSCREENC  PIC X.                                          00007030
           02 MSCREENP  PIC X.                                          00007040
           02 MSCREENH  PIC X.                                          00007050
           02 MSCREENV  PIC X.                                          00007060
           02 MSCREENO  PIC X(4).                                       00007070
                                                                                
