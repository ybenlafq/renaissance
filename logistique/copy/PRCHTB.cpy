      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *=========================================================*               
      *                                                         *               
      *          MODULE DE CHARGEMENT D'UNE TS                  *               
      *          -----------------------------                  *               
      *                                                         *               
      *            EN TABLE INTERNE AVANT TRI                   *               
      *            --------------------------                   *               
      *                                                         *               
      *                                                         *               
      *      AVANT D'UTILISER CE MODULE, IL FAUT :              *               
      *                                                         *               
      *                                                         *               
      *  1�) CREER ET RENSEIGNER UNE ZONE "TS-CLE-TRI"          *               
      *      INITIALISEE PAR LA OU LES ZONES SUR LESQUELLES     *               
      *      LE TRI DEVRA SE FAIRE.                             *               
      *                                                         *               
      *      EXEMPLE :                                          *               
      *                                                         *               
      *      SI CLES DE TRI = TS-NSOCIETE ET TS-NCODIC, ECRIRE  *               
      *      DANS LA TS :                                       *               
      *                                                         *               
      *      STRING  TS-NSOCIETE(I) TS-NCODIC(I)                *               
      *              DELIMITED SIZE INTO TS-CLE-TRI             *               
      *                                                         *               
      *                                                         *               
      *  2�) FAIRE UN MOVE DU NOMBRE DE PAGES CREEES DE LA TS   *               
      *      DANS LA VARIABLE WNBPMAX.                          *               
      *                                                         *               
      *      EXEMPLE : MOVE COMM-XXXX-POS-MAX TO WNBPMAX        *               
      *                                                         *               
      *                                                         *               
      *  3�) FAIRE UN MOVE DU NOMBRE DE LIGNES MAXI D'UN ITEM   *               
      *      DE LA TS DANS LA VARIABLE WNBLMAX :                *               
      *                                                         *               
      *      EXEMPLE : MOVE IMAX              TO WNBLMAX        *               
      *         OU     MOVE 12                TO WNBLMAX        *               
      *                                                         *               
      *=========================================================*               
       PRCHTB               SECTION.                                            
      *    CALCUL DU NOMBRE MAXI DE LIGNES PROVENANT DE LA TS                   
      *    ( NBRE D'ITEMS DE LA TS * NBRE DE LIGNES D'UN ITEM)                  
           COMPUTE WNBLTSMAX = WNBPMAX * WNBLMAX                                
      *    SI CE NOMBRE MAXI DE LIGNES > NBRE DE POSTES DE LA TABLE             
      *    DE TRI INTERNE -> PLANTAGE + MESSAGE D'ERREUR                        
           IF   WNBLTSMAX > WTIMAX                                              
                STRING 'WTABLETRI TROP PETITE (' WTIMAX '), '                   
                '-> RETAILLEZ A ' WNBLTSMAX ' POSTES MINI.'                     
                DELIMITED SIZE INTO MESS                                        
                PERFORM ABANDON-TACHE                                           
           END-IF                                                               
      *    SINON CHARGEMENT DE LA TS DANS LA TABLE INTERNE                      
           MOVE      0      TO        WTI.                                      
           IF DECROISSANT                                                       
              MOVE  LOW-VALUE TO WTABLTRI                                       
           ELSE                                                                 
              MOVE HIGH-VALUE TO WTABLTRI                                       
           END-IF                                                               
           PERFORM VARYING WTJ FROM 1 BY 1 UNTIL WTJ > WNBPMAX                  
              MOVE   WTJ    TO  RANG-TS                                         
              PERFORM           READ-TS                                         
              MOVE Z-INOUT  TO  TS-DATA                                         
              PERFORM VARYING WTK FROM 1 BY 1 UNTIL WTK > WNBLMAX               
                 IF TS-LIGNE (WTK)   NOT  =  SPACE  AND  LOW-VALUE              
                    ADD           1       TO           WTI                      
                    STRING          :WK1:                                       
                                    :WK2:                                       
                                    :WK3:                                       
                                    :WK4:                                       
                                    :WK5:                                       
                         DELIMITED SIZE INTO  WCLETRI (WTI)                     
                    MOVE             WTJ  TO  WNBITEM (WTI)                     
                    MOVE             WTK  TO  WNLIGNE (WTI)                     
                 END-IF                                                         
              END-PERFORM                                                       
           END-PERFORM.                                                         
       PRCHTB-FIN.          EXIT.                                               
                                                                                
