      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ******************************************************************        
      * PREPARATION DE LA TRACE SQL POUR LE MODELE IN9000                       
      ******************************************************************        
      *                                                                         
       CLEF-IN9000             SECTION.                                         
      *                                                                         
           MOVE 'RVIN9000          '       TO   TABLE-NAME.                     
           MOVE 'IN9000'         TO   MODEL-NAME.                               
      *                                                                         
       FIN-CLEF-IN9000. EXIT.                                                   
                EJECT                                                           
                                                                                
