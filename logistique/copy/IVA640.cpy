      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA640 AU 29/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,06,BI,A,                          *        
      *                           16,04,PD,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA640.                                                        
            05 NOMETAT-IVA640           PIC X(6) VALUE 'IVA640'.                
            05 RUPTURES-IVA640.                                                 
           10 IVA640-NSOCVALO           PIC X(03).                      007  003
           10 IVA640-NLIEU-CCONSO       PIC X(06).                      010  006
           10 IVA640-NSEQPRO            PIC S9(07)      COMP-3.         016  004
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA640-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IVA640-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA640.                                                   
           10 IVA640-CDEVISE            PIC X(06).                      022  006
           10 IVA640-LLIEU-LCONSO       PIC X(20).                      028  020
           10 IVA640-LSEQPRO            PIC X(26).                      048  026
           10 IVA640-PRISTT-ENTREE      PIC S9(07)V9(2) COMP-3.         074  005
           10 IVA640-PRISTT-SORTIE      PIC S9(07)V9(2) COMP-3.         079  005
           10 IVA640-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         084  008
           10 IVA640-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         092  008
           10 IVA640-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         100  008
           10 IVA640-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         108  008
           10 IVA640-QSTOCKENTREE       PIC S9(11)      COMP-3.         116  006
           10 IVA640-QSTOCKFINAL        PIC S9(11)      COMP-3.         122  006
           10 IVA640-QSTOCKINIT         PIC S9(11)      COMP-3.         128  006
           10 IVA640-QSTOCKSORTIE       PIC S9(11)      COMP-3.         134  006
            05 FILLER                      PIC X(373).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA640-LONG           PIC S9(4)   COMP  VALUE +139.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA640-LONG           PIC S9(4) COMP-5  VALUE +139.           
                                                                                
      *}                                                                        
