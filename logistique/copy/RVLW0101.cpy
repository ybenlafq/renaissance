      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      *   COPY DE LA TABLE RVLW0101                                             
      ******************************************************************        
      *                                                                         
      ******************************************************************        
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLW0101                *        
      ******************************************************************        
       01  RVLW0101.                                                            
           10 LW01-NSOCDEPOT       PIC X(3).                                    
           10 LW01-NDEPOT          PIC X(3).                                    
           10 LW01-NCODIC          PIC X(7).                                    
           10 LW01-CUL             PIC X(5).                                    
           10 LW01-DMAJ            PIC X(8).                                    
           10 LW01-DMAJWMS         PIC X(8).                                    
           10 LW01-QPOIDS          PIC S9(7)V USAGE COMP-3.                     
           10 LW01-QLARGEUR        PIC S9(5)V USAGE COMP-3.                     
           10 LW01-QPROFONDEUR     PIC S9(5)V USAGE COMP-3.                     
           10 LW01-QHAUTEUR        PIC S9(5)V USAGE COMP-3.                     
           10 LW01-QCOLIDSTOCK     PIC S9(5)V USAGE COMP-3.                     
           10 LW01-CFETEMPL        PIC X(1).                                    
           10 LW01-QNBRANMAIL      PIC S9(3)V USAGE COMP-3.                     
           10 LW01-QNBNIVGERB      PIC S9(3)V USAGE COMP-3.                     
           10 LW01-CSPECIFSTK      PIC X(1).                                    
           10 LW01-CCOTEHOLD       PIC X(1).                                    
           10 LW01-QNBPVSOL        PIC S9(3)V USAGE COMP-3.                     
           10 LW01-QNBPRACK        PIC S9(5)V USAGE COMP-3.                     
           10 LW01-CLASSE          PIC X(1).                                    
           10 LW01-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 LW01-QCOLIRECEPT     PIC S9(5)V USAGE COMP-3.                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLW0101                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVLW0101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-CUL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-CUL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-DMAJWMS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-DMAJWMS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QLARGEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QLARGEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QPROFONDEUR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QPROFONDEUR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QHAUTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QHAUTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QNBRANMAIL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QNBRANMAIL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QNBNIVGERB-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QNBNIVGERB-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-CSPECIFSTK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-CSPECIFSTK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-CCOTEHOLD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-CCOTEHOLD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QNBPVSOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QNBPVSOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-CLASSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-CLASSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-CFAMSTK-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-CFAMSTK-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LW01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LW01-QCOLIRECEPT-F                                               
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  LW01-QCOLIRECEPT-F                                               
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
