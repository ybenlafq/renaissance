      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES MOUVEMENTS DE VIDAGE DES PI (PGM=BGS003) *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FGS003-ENR.                                                       
           10  FGS003-CFAM            PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-CMARQ           PIC X(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NCODIC          PIC X(07)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-LREFFOURN       PIC X(20)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-TYPE            PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NSOCORIG        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NLIEUORIG       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NSSLIEUORIG     PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-CLIEUTRTORIG    PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NSOCDEST        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NLIEUDEST       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NSSLIEUDEST     PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-CLIEUTRTDEST    PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-NORIGINE        PIC X(09)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-DMVT            PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-QMVT-E          PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-QMVT-S          PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-VAL-E           PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS003-VAL-E0 REDEFINES FGS003-VAL-E                             
                                      PIC -(10),-(2).                           
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS003-VAL-S           PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS003-VAL-S0 REDEFINES FGS003-VAL-S                             
                                      PIC -(10),-(2).                           
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FILLER                 PIC X(19)      VALUE SPACE.               
           10  FGS003-LIEU-TRI        PIC X(3)       VALUE SPACE.               
                                                                                
