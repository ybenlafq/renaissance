      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM26   ERM26                                              00000020
      ***************************************************************** 00000030
       01   ERM26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MYYL      COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MYYL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MYYF      PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MYYI      PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MXXL      COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MXXL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MXXF      PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MXXI      PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBGRPL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDEBGRPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEBGRPF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDEBGRPI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MGRPSELL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MGRPSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MGRPSELF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MGRPSELI  PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEBSEML  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDEBSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEBSEMF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDEBSEMI  PIC X(4).                                       00000330
           02 MDATED OCCURS   8 TIMES .                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MDATEI  PIC X(6).                                       00000380
           02 MNSEMD OCCURS   8 TIMES .                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSEML  COMP PIC S9(4).                                 00000400
      *--                                                                       
             03 MNSEML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSEMF  PIC X.                                          00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MNSEMI  PIC X(2).                                       00000430
           02 MLIGNEI OCCURS   12 TIMES .                               00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREAPPROL    COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MREAPPROL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MREAPPROF    PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MREAPPROI    PIC X(5).                                  00000480
             03 MQTED OCCURS   8 TIMES .                                00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQTEL      COMP PIC S9(4).                            00000500
      *--                                                                       
               04 MQTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
               04 MQTEF      PIC X.                                     00000510
               04 FILLER     PIC X(4).                                  00000520
               04 MQTEI      PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MZONCMDI  PIC X(15).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBERRI  PIC X(58).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCODTRAI  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCICSI    PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNETNAMI  PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCREENI  PIC X(4).                                       00000770
      ***************************************************************** 00000780
      * SDF: ERM26   ERM26                                              00000790
      ***************************************************************** 00000800
       01   ERM26O REDEFINES ERM26I.                                    00000810
           02 FILLER    PIC X(12).                                      00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MDATJOUA  PIC X.                                          00000840
           02 MDATJOUC  PIC X.                                          00000850
           02 MDATJOUP  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUV  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MTIMJOUA  PIC X.                                          00000910
           02 MTIMJOUC  PIC X.                                          00000920
           02 MTIMJOUP  PIC X.                                          00000930
           02 MTIMJOUH  PIC X.                                          00000940
           02 MTIMJOUV  PIC X.                                          00000950
           02 MTIMJOUO  PIC X(5).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MYYA      PIC X.                                          00000980
           02 MYYC      PIC X.                                          00000990
           02 MYYP      PIC X.                                          00001000
           02 MYYH      PIC X.                                          00001010
           02 MYYV      PIC X.                                          00001020
           02 MYYO      PIC X(3).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MXXA      PIC X.                                          00001050
           02 MXXC      PIC X.                                          00001060
           02 MXXP      PIC X.                                          00001070
           02 MXXH      PIC X.                                          00001080
           02 MXXV      PIC X.                                          00001090
           02 MXXO      PIC X(3).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MDEBGRPA  PIC X.                                          00001120
           02 MDEBGRPC  PIC X.                                          00001130
           02 MDEBGRPP  PIC X.                                          00001140
           02 MDEBGRPH  PIC X.                                          00001150
           02 MDEBGRPV  PIC X.                                          00001160
           02 MDEBGRPO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MGRPSELA  PIC X.                                          00001190
           02 MGRPSELC  PIC X.                                          00001200
           02 MGRPSELP  PIC X.                                          00001210
           02 MGRPSELH  PIC X.                                          00001220
           02 MGRPSELV  PIC X.                                          00001230
           02 MGRPSELO  PIC X(5).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MDEBSEMA  PIC X.                                          00001260
           02 MDEBSEMC  PIC X.                                          00001270
           02 MDEBSEMP  PIC X.                                          00001280
           02 MDEBSEMH  PIC X.                                          00001290
           02 MDEBSEMV  PIC X.                                          00001300
           02 MDEBSEMO  PIC X(4).                                       00001310
           02 DFHMS1 OCCURS   8 TIMES .                                 00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MDATEA  PIC X.                                          00001340
             03 MDATEC  PIC X.                                          00001350
             03 MDATEP  PIC X.                                          00001360
             03 MDATEH  PIC X.                                          00001370
             03 MDATEV  PIC X.                                          00001380
             03 MDATEO  PIC X(6).                                       00001390
           02 DFHMS2 OCCURS   8 TIMES .                                 00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MNSEMA  PIC X.                                          00001420
             03 MNSEMC  PIC X.                                          00001430
             03 MNSEMP  PIC X.                                          00001440
             03 MNSEMH  PIC X.                                          00001450
             03 MNSEMV  PIC X.                                          00001460
             03 MNSEMO  PIC X(2).                                       00001470
           02 MLIGNEO OCCURS   12 TIMES .                               00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MREAPPROA    PIC X.                                     00001500
             03 MREAPPROC    PIC X.                                     00001510
             03 MREAPPROP    PIC X.                                     00001520
             03 MREAPPROH    PIC X.                                     00001530
             03 MREAPPROV    PIC X.                                     00001540
             03 MREAPPROO    PIC X(5).                                  00001550
             03 DFHMS3 OCCURS   8 TIMES .                               00001560
               04 FILLER     PIC X(2).                                  00001570
               04 MQTEA      PIC X.                                     00001580
               04 MQTEC PIC X.                                          00001590
               04 MQTEP PIC X.                                          00001600
               04 MQTEH PIC X.                                          00001610
               04 MQTEV PIC X.                                          00001620
               04 MQTEO      PIC X(3).                                  00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MZONCMDA  PIC X.                                          00001650
           02 MZONCMDC  PIC X.                                          00001660
           02 MZONCMDP  PIC X.                                          00001670
           02 MZONCMDH  PIC X.                                          00001680
           02 MZONCMDV  PIC X.                                          00001690
           02 MZONCMDO  PIC X(15).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MLIBERRA  PIC X.                                          00001720
           02 MLIBERRC  PIC X.                                          00001730
           02 MLIBERRP  PIC X.                                          00001740
           02 MLIBERRH  PIC X.                                          00001750
           02 MLIBERRV  PIC X.                                          00001760
           02 MLIBERRO  PIC X(58).                                      00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MCODTRAA  PIC X.                                          00001790
           02 MCODTRAC  PIC X.                                          00001800
           02 MCODTRAP  PIC X.                                          00001810
           02 MCODTRAH  PIC X.                                          00001820
           02 MCODTRAV  PIC X.                                          00001830
           02 MCODTRAO  PIC X(4).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MCICSA    PIC X.                                          00001860
           02 MCICSC    PIC X.                                          00001870
           02 MCICSP    PIC X.                                          00001880
           02 MCICSH    PIC X.                                          00001890
           02 MCICSV    PIC X.                                          00001900
           02 MCICSO    PIC X(5).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNETNAMA  PIC X.                                          00001930
           02 MNETNAMC  PIC X.                                          00001940
           02 MNETNAMP  PIC X.                                          00001950
           02 MNETNAMH  PIC X.                                          00001960
           02 MNETNAMV  PIC X.                                          00001970
           02 MNETNAMO  PIC X(8).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MSCREENA  PIC X.                                          00002000
           02 MSCREENC  PIC X.                                          00002010
           02 MSCREENP  PIC X.                                          00002020
           02 MSCREENH  PIC X.                                          00002030
           02 MSCREENV  PIC X.                                          00002040
           02 MSCREENO  PIC X(4).                                       00002050
                                                                                
