      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL6000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL6000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL6000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL6000.                                                            
      *}                                                                        
           02  FL60-FILIALE                                                     
               PIC X(0003).                                                     
           02  FL60-NCODIC                                                      
               PIC X(0007).                                                     
           02  FL60-D1RECEPT                                                    
               PIC X(0008).                                                     
           02  FL60-CAPPRO                                                      
               PIC X(0005).                                                     
           02  FL60-WSENSAPPRO                                                  
               PIC X(0001).                                                     
           02  FL60-QDELAIAPPRO                                                 
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL6000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL6000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL6000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL60-FILIALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL60-FILIALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL60-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL60-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL60-D1RECEPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL60-D1RECEPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL60-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL60-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL60-WSENSAPPRO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL60-WSENSAPPRO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL60-QDELAIAPPRO-F                                               
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  FL60-QDELAIAPPRO-F                                               
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
