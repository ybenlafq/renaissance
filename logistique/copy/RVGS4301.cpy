      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGS4301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS4301                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS4301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS4301.                                                            
      *}                                                                        
           02  GS43-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  GS43-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GS43-NSSLIEUORIG                                                 
               PIC X(0003).                                                     
           02  GS43-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  GS43-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  GS43-NSSLIEUDEST                                                 
               PIC X(0003).                                                     
           02  GS43-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS43-NORIGINE                                                    
               PIC X(0007).                                                     
           02  GS43-CPROG                                                       
               PIC X(0005).                                                     
           02  GS43-COPER                                                       
               PIC X(0010).                                                     
           02  GS43-NSEQ                                                        
               PIC S9(13) COMP-3.                                               
           02  GS43-WTOPE                                                       
               PIC X(0001).                                                     
           02  GS43-WMUTATION                                                   
               PIC X(0001).                                                     
           02  GS43-QMVT                                                        
               PIC S9(5) COMP-3.                                                
           02  GS43-DMVT                                                        
               PIC X(0008).                                                     
           02  GS43-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GS43-NSOCMAJ                                                     
               PIC X(0003).                                                     
           02  GS43-NLIEUMAJ                                                    
               PIC X(0003).                                                     
           02  GS43-NSSLIEUMAJ                                                  
               PIC X(0003).                                                     
           02  GS43-DTOPE                                                       
               PIC X(0008).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS4301                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS4301-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS4301-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSSLIEUORIG-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSSLIEUDEST-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-CPROG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-CPROG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-COPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-COPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-WTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-WTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-WMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-WMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-QMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-QMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSOCMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSOCMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NLIEUMAJ-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NLIEUMAJ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-NSSLIEUMAJ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-NSSLIEUMAJ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS43-DTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS43-DTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
