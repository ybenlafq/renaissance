      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVIE0500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE0500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE0500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE0500.                                                            
      *}                                                                        
           02  IE05-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IE05-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IE05-NCODIC                                                      
               PIC X(0007).                                                     
           02  IE05-CFAM                                                        
               PIC X(0005).                                                     
           02  IE05-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  IE05-CMARQ                                                       
               PIC X(0005).                                                     
           02  IE05-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  IE05-QSTOCKVAV                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKCAV                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKHEAV                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKHFAV                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKHAAV                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKPRFAV                                                 
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKPAIAV                                                 
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKTAV                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKVAP                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKCAP                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKHEAP                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKHFAP                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKHAAP                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKPRFAP                                                 
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKPAIAP                                                 
               PIC S9(5) COMP-3.                                                
           02  IE05-QSTOCKTAP                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-WREGULV                                                     
               PIC X(0001).                                                     
           02  IE05-QSTOCKVPI                                                   
               PIC S9(5) COMP-3.                                                
           02  IE05-WREGULHE                                                    
               PIC X(0001).                                                     
           02  IE05-QSTOCKHEPI                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-WREGULHF                                                    
               PIC X(0001).                                                     
           02  IE05-QSTOCKHFPI                                                  
               PIC S9(5) COMP-3.                                                
           02  IE05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVIE0500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIE0500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIE0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKVAV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKVAV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKCAV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKCAV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHEAV-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHEAV-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHFAV-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHFAV-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHAAV-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHAAV-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKPRFAV-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKPRFAV-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKPAIAV-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKPAIAV-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKTAV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKTAV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKVAP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKVAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKCAP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKCAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHEAP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHEAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHFAP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHFAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHAAP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHAAP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKPRFAP-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKPRFAP-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKPAIAP-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKPAIAP-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKTAP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKTAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-WREGULV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-WREGULV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKVPI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKVPI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-WREGULHE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-WREGULHE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHEPI-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHEPI-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-WREGULHF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-WREGULHF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-QSTOCKHFPI-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE05-QSTOCKHFPI-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  IE05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
