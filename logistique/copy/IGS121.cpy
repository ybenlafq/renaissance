      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS121 AU 02/08/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,01,BI,A,                          *        
      *                           14,03,PD,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,07,BI,A,                          *        
      *                           29,08,BI,A,                          *        
      *                           37,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS121.                                                        
            05 NOMETAT-IGS121           PIC X(6) VALUE 'IGS121'.                
            05 RUPTURES-IGS121.                                                 
           10 IGS121-NSOCDEPOT          PIC X(03).                      007  003
           10 IGS121-NDEPOT             PIC X(03).                      010  003
           10 IGS121-WTYPPRET           PIC X(01).                      013  001
           10 IGS121-WSEQFAM            PIC S9(05)      COMP-3.         014  003
           10 IGS121-CMARQ              PIC X(05).                      017  005
           10 IGS121-NCODIC             PIC X(07).                      022  007
           10 IGS121-DPRET              PIC X(08).                      029  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS121-SEQUENCE           PIC S9(04) COMP.                037  002
      *--                                                                       
           10 IGS121-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS121.                                                   
           10 IGS121-CFAM               PIC X(05).                      039  005
           10 IGS121-LCOMMENT           PIC X(30).                      044  030
           10 IGS121-LREFFOURN          PIC X(20).                      074  020
           10 IGS121-QPRET              PIC S9(05)      COMP-3.         094  003
           10 IGS121-QPRMP              PIC S9(08)V9(2) COMP-3.         097  006
            05 FILLER                      PIC X(410).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS121-LONG           PIC S9(4)   COMP  VALUE +102.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS121-LONG           PIC S9(4) COMP-5  VALUE +102.           
                                                                                
      *}                                                                        
