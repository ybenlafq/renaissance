      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVVA1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVA1600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA1600.                                                            
           02  VA16-NCODIC                                                      
               PIC X(0007).                                                     
           02  VA16-NSOCVALO                                                    
               PIC X(0003).                                                     
           02  VA16-NLIEUVALO                                                   
               PIC X(0003).                                                     
           02  VA16-PSTOCKRECYCL                                                
               PIC S9(9)V9(0006) COMP-3.                                        
           02  VA16-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVVA1600                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVVA1600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA16-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA16-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA16-NSOCVALO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA16-NSOCVALO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA16-NLIEUVALO-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA16-NLIEUVALO-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA16-PSTOCKRECYCL-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA16-PSTOCKRECYCL-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VA16-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VA16-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
