      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS-RM65 SPECIFIQUE TRM65                                     *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-RM65-LONG            PIC S9(3) COMP-3 VALUE 91.           00060001
       01  TS-RM65-DONNEES.                                             00070001
              05 TS-RM65-NCODIC    PIC X(07).                           00080001
              05 TS-RM65-LREF      PIC X(20).                           00090001
              05 TS-RM65-CMARQ     PIC X(05).                           00100001
              05 TS-RM65-CEXPO     PIC X(01).                           00110001
              05 TS-RM65-WASSORT   PIC X(01).                           00120001
              05 TS-RM65-WASSOSTD  PIC X(01).                           00130001
              05 TS-RM65-QEXPO     PIC X(03).                           00140001
              05 TS-RM65-QLS       PIC X(03).                           00150001
              05 TS-RM65-QV8SP     PIC X(05).                           00160001
              05 TS-RM65-QV8SR     PIC X(05).                           00170001
              05 TS-RM65-QSO       PIC X(03).                           00180001
              05 TS-RM65-QSM       PIC X(03).                           00190001
              05 TS-RM65-QSOP      PIC X(03).                           00200001
              05 TS-RM65-QSMP      PIC X(03).                           00210001
              05 TS-RM65-MODIF     PIC X(01).                           00220001
              05 TS-RM65-QSO-SAVE  PIC X(03).                           00230001
              05 TS-RM65-QSM-SAVE  PIC X(03).                           00240001
              05 TS-RM65-QSOTROUVE PIC X.                               00250001
              05 TS-RM65-QSATROUVE PIC X.                               00260001
              05 TS-RM65-QSOMIN    PIC X(03).                           00270001
              05 TS-RM65-QSMMIN    PIC X(03).                           00280001
              05 TS-RM65-QSOMAX    PIC X(03).                           00290001
              05 TS-RM65-QSMMAX    PIC X(03).                           00300001
              05 TS-RM65-W80       PIC X.                               00310001
              05 TS-RM65-FLAG      PIC X.                               00320001
                                                                                
