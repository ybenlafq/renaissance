      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *===============================================================* 00000010
      *                                                               * 00000020
      *      TS ASSOCIE A LA TRANSACTION TL05                         * 00000030
      *      CONTIENT LES RENSEIGNEMENTS POUR L'EDITION               * 00000040
      *      DES BON DE LIVRAISON                                     * 00000050
      *                                                               * 00000070
      *      NOM: 'TL01' + EIBTRMID                                   * 00000080
      *                                                               * 00000091
      *===============================================================* 00000092
                                                                        00000093
       01  TS-TL01.                                                     00000094
           02 TS-TL01-LONG              PIC S9(5) COMP-3     VALUE +49. 00000095
           02 TS-TL01-DONNEES.                                          00000096
              03  TS-TL01-CPROTOUR-GV11 PIC X(05).                      00000097
              03  TS-TL01-CPROTOUR-TL02 PIC X(05).                      00000097
              03  TS-TL01-CTOURNEE      PIC X(03).                      00000097
              03  TS-TL01-NORDRE        PIC X(02).                      00000098
              03  TS-TL01-NMAG          PIC X(03).                      00000099
              03  TS-TL01-NVENTE        PIC X(07).                      00000100
              03  TS-TL01-CADRTOUR      PIC X(01).                      00000110
              03  TS-TL01-NCASE         PIC X(03).                      00000110
              03  TS-TL01-CLIVR1        PIC X(10).                      00000110
              03  TS-TL01-CLIVR2        PIC X(10).                      00000110
                                                                        00000120
      *===============================================================* 00000130
                                                                                
