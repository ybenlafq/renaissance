      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGL03   EGL03                                              00000020
      ***************************************************************** 00000030
       01   EGL03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFONCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MWFONCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWFONCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MWFONCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRECL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCRECF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCRECI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLRECL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLRECF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLRECI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLSOCI    PIC X(19).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEP1L   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNDEP1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNDEP1F   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNDEP1I   PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLDEPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLDEPF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLDEPI    PIC X(19).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIVRL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIVRF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCLIVRI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIVRL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIVRF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLLIVRI   PIC X(19).                                      00000570
           02 M206I OCCURS   12 TIMES .                                 00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIVRL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNLIVRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIVRF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNLIVRI      PIC X(7).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEP2L      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MNDEP2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNDEP2F      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNDEP2I      PIC X(6).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MDATEI  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDHHL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MDHHL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDHHF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MDHHI   PIC X(2).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDMNL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MDMNL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDMNF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDMNI   PIC X(2).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRANSL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCTRANSL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCTRANSF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCTRANSI     PIC X(5).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBPL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MQNBPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQNBPF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQNBPI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNBUOL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQNBUOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQNBUOF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQNBUOI      PIC X(4).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMML      COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MLCOMML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCOMMF      PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MLCOMMI      PIC X(20).                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MZONCMDI  PIC X(15).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(58).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCICSI    PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNETNAMI  PIC X(8).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MSCREENI  PIC X(4).                                       00001180
      ***************************************************************** 00001190
      * SDF: EGL03   EGL03                                              00001200
      ***************************************************************** 00001210
       01   EGL03O REDEFINES EGL03I.                                    00001220
           02 FILLER    PIC X(12).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPAGEA    PIC X.                                          00001390
           02 MPAGEC    PIC X.                                          00001400
           02 MPAGEP    PIC X.                                          00001410
           02 MPAGEH    PIC X.                                          00001420
           02 MPAGEV    PIC X.                                          00001430
           02 MPAGEO    PIC ZZZ.                                        00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MPAGEMAXA      PIC X.                                     00001460
           02 MPAGEMAXC PIC X.                                          00001470
           02 MPAGEMAXP PIC X.                                          00001480
           02 MPAGEMAXH PIC X.                                          00001490
           02 MPAGEMAXV PIC X.                                          00001500
           02 MPAGEMAXO      PIC ZZZ.                                   00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MWFONCA   PIC X.                                          00001530
           02 MWFONCC   PIC X.                                          00001540
           02 MWFONCP   PIC X.                                          00001550
           02 MWFONCH   PIC X.                                          00001560
           02 MWFONCV   PIC X.                                          00001570
           02 MWFONCO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCRECA    PIC X.                                          00001600
           02 MCRECC    PIC X.                                          00001610
           02 MCRECP    PIC X.                                          00001620
           02 MCRECH    PIC X.                                          00001630
           02 MCRECV    PIC X.                                          00001640
           02 MCRECO    PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLRECA    PIC X.                                          00001670
           02 MLRECC    PIC X.                                          00001680
           02 MLRECP    PIC X.                                          00001690
           02 MLRECH    PIC X.                                          00001700
           02 MLRECV    PIC X.                                          00001710
           02 MLRECO    PIC X(20).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNSOCA    PIC X.                                          00001740
           02 MNSOCC    PIC X.                                          00001750
           02 MNSOCP    PIC X.                                          00001760
           02 MNSOCH    PIC X.                                          00001770
           02 MNSOCV    PIC X.                                          00001780
           02 MNSOCO    PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLSOCA    PIC X.                                          00001810
           02 MLSOCC    PIC X.                                          00001820
           02 MLSOCP    PIC X.                                          00001830
           02 MLSOCH    PIC X.                                          00001840
           02 MLSOCV    PIC X.                                          00001850
           02 MLSOCO    PIC X(19).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MNDEP1A   PIC X.                                          00001880
           02 MNDEP1C   PIC X.                                          00001890
           02 MNDEP1P   PIC X.                                          00001900
           02 MNDEP1H   PIC X.                                          00001910
           02 MNDEP1V   PIC X.                                          00001920
           02 MNDEP1O   PIC X(3).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLDEPA    PIC X.                                          00001950
           02 MLDEPC    PIC X.                                          00001960
           02 MLDEPP    PIC X.                                          00001970
           02 MLDEPH    PIC X.                                          00001980
           02 MLDEPV    PIC X.                                          00001990
           02 MLDEPO    PIC X(19).                                      00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MCLIVRA   PIC X.                                          00002020
           02 MCLIVRC   PIC X.                                          00002030
           02 MCLIVRP   PIC X.                                          00002040
           02 MCLIVRH   PIC X.                                          00002050
           02 MCLIVRV   PIC X.                                          00002060
           02 MCLIVRO   PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MLLIVRA   PIC X.                                          00002090
           02 MLLIVRC   PIC X.                                          00002100
           02 MLLIVRP   PIC X.                                          00002110
           02 MLLIVRH   PIC X.                                          00002120
           02 MLLIVRV   PIC X.                                          00002130
           02 MLLIVRO   PIC X(19).                                      00002140
           02 M206O OCCURS   12 TIMES .                                 00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MNLIVRA      PIC X.                                     00002170
             03 MNLIVRC PIC X.                                          00002180
             03 MNLIVRP PIC X.                                          00002190
             03 MNLIVRH PIC X.                                          00002200
             03 MNLIVRV PIC X.                                          00002210
             03 MNLIVRO      PIC X(7).                                  00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MNDEP2A      PIC X.                                     00002240
             03 MNDEP2C PIC X.                                          00002250
             03 MNDEP2P PIC X.                                          00002260
             03 MNDEP2H PIC X.                                          00002270
             03 MNDEP2V PIC X.                                          00002280
             03 MNDEP2O      PIC X(6).                                  00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MDATEA  PIC X.                                          00002310
             03 MDATEC  PIC X.                                          00002320
             03 MDATEP  PIC X.                                          00002330
             03 MDATEH  PIC X.                                          00002340
             03 MDATEV  PIC X.                                          00002350
             03 MDATEO  PIC X(8).                                       00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MDHHA   PIC X.                                          00002380
             03 MDHHC   PIC X.                                          00002390
             03 MDHHP   PIC X.                                          00002400
             03 MDHHH   PIC X.                                          00002410
             03 MDHHV   PIC X.                                          00002420
             03 MDHHO   PIC X(2).                                       00002430
             03 FILLER       PIC X(2).                                  00002440
             03 MDMNA   PIC X.                                          00002450
             03 MDMNC   PIC X.                                          00002460
             03 MDMNP   PIC X.                                          00002470
             03 MDMNH   PIC X.                                          00002480
             03 MDMNV   PIC X.                                          00002490
             03 MDMNO   PIC X(2).                                       00002500
             03 FILLER       PIC X(2).                                  00002510
             03 MCTRANSA     PIC X.                                     00002520
             03 MCTRANSC     PIC X.                                     00002530
             03 MCTRANSP     PIC X.                                     00002540
             03 MCTRANSH     PIC X.                                     00002550
             03 MCTRANSV     PIC X.                                     00002560
             03 MCTRANSO     PIC X(5).                                  00002570
             03 FILLER       PIC X(2).                                  00002580
             03 MQNBPA  PIC X.                                          00002590
             03 MQNBPC  PIC X.                                          00002600
             03 MQNBPP  PIC X.                                          00002610
             03 MQNBPH  PIC X.                                          00002620
             03 MQNBPV  PIC X.                                          00002630
             03 MQNBPO  PIC ZZZZZ.                                      00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MQNBUOA      PIC X.                                     00002660
             03 MQNBUOC PIC X.                                          00002670
             03 MQNBUOP PIC X.                                          00002680
             03 MQNBUOH PIC X.                                          00002690
             03 MQNBUOV PIC X.                                          00002700
             03 MQNBUOO      PIC ZZZZ.                                  00002710
             03 FILLER       PIC X(2).                                  00002720
             03 MLCOMMA      PIC X.                                     00002730
             03 MLCOMMC PIC X.                                          00002740
             03 MLCOMMP PIC X.                                          00002750
             03 MLCOMMH PIC X.                                          00002760
             03 MLCOMMV PIC X.                                          00002770
             03 MLCOMMO      PIC X(20).                                 00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MZONCMDA  PIC X.                                          00002800
           02 MZONCMDC  PIC X.                                          00002810
           02 MZONCMDP  PIC X.                                          00002820
           02 MZONCMDH  PIC X.                                          00002830
           02 MZONCMDV  PIC X.                                          00002840
           02 MZONCMDO  PIC X(15).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MLIBERRA  PIC X.                                          00002870
           02 MLIBERRC  PIC X.                                          00002880
           02 MLIBERRP  PIC X.                                          00002890
           02 MLIBERRH  PIC X.                                          00002900
           02 MLIBERRV  PIC X.                                          00002910
           02 MLIBERRO  PIC X(58).                                      00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MCODTRAA  PIC X.                                          00002940
           02 MCODTRAC  PIC X.                                          00002950
           02 MCODTRAP  PIC X.                                          00002960
           02 MCODTRAH  PIC X.                                          00002970
           02 MCODTRAV  PIC X.                                          00002980
           02 MCODTRAO  PIC X(4).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MCICSA    PIC X.                                          00003010
           02 MCICSC    PIC X.                                          00003020
           02 MCICSP    PIC X.                                          00003030
           02 MCICSH    PIC X.                                          00003040
           02 MCICSV    PIC X.                                          00003050
           02 MCICSO    PIC X(5).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNETNAMA  PIC X.                                          00003080
           02 MNETNAMC  PIC X.                                          00003090
           02 MNETNAMP  PIC X.                                          00003100
           02 MNETNAMH  PIC X.                                          00003110
           02 MNETNAMV  PIC X.                                          00003120
           02 MNETNAMO  PIC X(8).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MSCREENA  PIC X.                                          00003150
           02 MSCREENC  PIC X.                                          00003160
           02 MSCREENP  PIC X.                                          00003170
           02 MSCREENH  PIC X.                                          00003180
           02 MSCREENV  PIC X.                                          00003190
           02 MSCREENO  PIC X(4).                                       00003200
                                                                                
