      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-------------------------------------------------------------            
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-EF00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-EF00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                               
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      ******************************************************************        
      *                                                                         
      *               COMMAREA MENU CONSULTATION ET EDITION                     
      *                     DES MOUVEMENTS DE STOCKS                            
      *                                                                         
      *  MENU PRIMAIRE   : EF00                                                 
      *  TRANSACTION     : EF00                                                 
      *  PROGRAMME       : TEF00                                                
      *                                                                         
      *  LONGUEUR DE LA ZONE APPLICATIVE RESERVEE : 3724                        
      *                                                                         
      ******************************************************************        
      *                                                                         
      ****              DONNEES COMMUNES AU PROGRAMME TEF00 ET TEF01            
L=3724     02   COMM-EF00-APPLI.                                                
      *                                                                         
L=1540       05 COMM-EF00-SSTAB.                                                
               07 COMM-EF00-ENR-SSTAB       OCCURS 24.                          
                09  COMM-EF00-SSTAB-NOPT         PIC X(2).                      
                09  COMM-EF00-SSTAB-NSOC         PIC X(3).                      
                09  COMM-EF00-SSTAB-NLIEU        PIC X(3).                      
                09  COMM-EF00-SSTAB-CTRAIT       PIC X(5).                      
                09  COMM-EF00-SSTAB-CTYPOPT      PIC X(5).                      
                09  COMM-EF00-SSTAB-MCTAO        PIC X(1).                      
                09  COMM-EF00-SSTAB-DATA.                                       
                 11 COMM-EF00-SSTAB-LTRAIT       PIC X(20).                     
                 11 COMM-EF00-SSTAB-CTIERSHE     PIC X(1).                      
                 11 COMM-EF00-SSTAB-LIBTIERSHE PIC X(15).                       
                 11 COMM-EF00-SSTAB-TOLERANCE    PIC 9(3).                      
                 11 COMM-EF00-SSTAB-CGSTOCK      PIC X(1).                      
                 11 COMM-EF00-SSTAB-MCTA         PIC X(1).                      
                 11 COMM-EF00-SSTAB-TCHRO        PIC X(1).                      
      *                                                                         
             05 COMM-EF00-CISOC                  PIC X(3).                      
             05 COMM-EF00-LEVEL-MAX              PIC X(5).                      
             05 COMM-EF00-LEVEL-SUP              PIC X(5).                      
             05 COMM-EF00-NSOC                   PIC X(3).                      
             05 COMM-EF00-NLIEU                  PIC X(3).                      
             05 COMM-EF00-CTRAIT                 PIC X(5).                      
             05 COMM-EF00-DATA.                                                 
              7 COMM-EF00-LTRAIT                 PIC X(20).                     
              7 COMM-EF00-CTIERS                 PIC X(1).                      
              7 COMM-EF00-LIBTIERS               PIC X(15).                     
              7 COMM-EF00-TAUX-TOLERANCE         PIC 9(3).                      
              7 COMM-EF00-CGSTOCK                PIC X(1).                      
              7 COMM-EF00-MCTA                   PIC X(1).                      
              7 COMM-EF00-MCTAO                  PIC X(1).                      
              7 COMM-EF00-TCHRO                  PIC X(1).                      
             05 COMM-EF00-CTIERSEF               PIC X(5).                      
             05 COMM-EF00-NBOPTION               PIC 9(2).                      
             05 COMM-EF00-NBINT                  PIC 9(2).                      
      *--------INITIALISER A CHAQUE OPTION                                      
L=109        03 COMM-EF00-ZONES.                                                
                05 COMM-EF00-CODRET              PIC X.                         
                   88  COMM-EF00-PAS-MESS        VALUE ' '.                     
                   88  COMM-EF00-MESS-RET        VALUE '1'.                     
                05 COMM-EF00-MESSAGE             PIC X(70).                     
                05 COMM-EF00-FILLER              PIC X(38).                     
      *                                                                         
L=544        03 COMM-EF00-TAB-OPTION.                                           
 *              05 COMM-EF00-ENR-TABOPT    OCCURS 16.                           
                 09 COMM-EF00-NOPTION            PIC X(2).                      
                 09 COMM-EF00-COPTION            PIC X(2).                      
                   07 COMM-EF00-LOPTION          PIC X(30).                     
      *                                                                         
L=200        03 COMM-EF01-ZONES.                                                
                05  COMM-EF01-CTIERS             PIC  X(05).                    
                05  COMM-EF01-CTIERS-AFF         PIC  X(05).                    
                05  COMM-EF01-LTIERS             PIC  X(20).                    
                05  COMM-EF01-NENVOI             PIC  X(07).                    
                05  COMM-EF01-DENVOI             PIC  X(08).                    
                05  COMM-EF01-NACCORD            PIC  X(12).                    
                05  COMM-EF01-DACCORD            PIC  X(08).                    
                05  COMM-EF01-LNOMACCORD         PIC  X(10).                    
                05  COMM-EF01-NSOCORIG           PIC  X(03).                    
                05  COMM-EF01-NLIEUORIG          PIC  X(03).                    
                05  COMM-EF01-NORIGINE           PIC  X(07).                    
                05  COMM-EF01-NRENDU             PIC  X(20).                    
                05  COMM-EF01-MRENDU             PIC  X(05).                    
                05  COMM-EF01-DRENDU             PIC  X(08).                    
                05  COMM-EF01-CIMP               PIC  X(04).                    
                05  COMM-EF01-NENTITE            PIC  X(05).                    
                05  COMM-EF01-FOURGHE            PIC  X(05).                    
                05  COMM-EF01-LRENDU             PIC  X(20).                    
                05  COMM-EF01-WRENDU             PIC  X(01).                    
                05  COMM-EF01-CTYPRENDU          PIC  X(01).                    
                05  COMM-EF01-DESTRET            PIC  X(14).                    
                05  COMM-EF01-COPTION            PIC  X(02).                    
                05  COMM-EF01-IMP                PIC  X(01).                    
                    88 COMM-EF01-IMP-NOT-OK                VALUE '0'.           
                    88 COMM-EF01-IMP-OK                    VALUE '1'.           
                05  COMM-EF01-ITEM               PIC  9(02).                    
                05  COMM-EF01-POS-MAX            PIC  9(02).                    
                05  COMM-EF01-CPT-LIGNE          PIC  9(02).                    
                05  COMM-EF01-CMOTIF             PIC  X(10).                    
                05  COMM-EF01-GLOBAL             PIC  X(01).                    
                05  COMM-EF01-AUTOLG             PIC  X(01).                    
************************                                                        
                05  COMM-EF01-DENVOI-FIN         PIC  X(08).                    
                05  COMM-EF01-CTRAIT-OP          PIC  X(5).                     
                05  COMM-EF01-LTRAIT-OP          PIC  X(20).                    
                05  COMM-EF01-CTYPTIERS-OP       PIC  X(1).                     
                05  COMM-EF01-LIBTIERS-OP        PIC  X(15).                    
                05  COMM-EF01-TAUX-TOLERANCE-OP  PIC  9(3).                     
                05  COMM-EF01-CGSTOCK-OP         PIC  X(1).                     
************************                                                        
                05  COMM-EF01-DERNIER-PASSAGE    PIC  X(01).                    
                05  COMM-EF01-FILLER             PIC  X(07).                    
      *                                                                         
             03 COMM-EF00-SSPRGS                 PIC X(1278).                   
      *                                                                         
      *--------ZONES PROPRES A TEF10                                            
             03 COMM-EF10-SSPRG-10    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF10-APPLI.                                                
                05  COMM-EF10-PREMIER-PASSAGE    PIC  X(01).                    
                05  COMM-EF10-CTIERS             PIC  X(05).                    
                05  COMM-EF10-CTIERS-AFF         PIC  X(05).                    
                05  COMM-EF10-LTIERS             PIC  X(20).                    
                05  COMM-EF10-NENVOI             PIC  X(07).                    
                05  COMM-EF10-DENVOI             PIC  X(08).                    
                05  COMM-EF10-NACCORD            PIC  X(12).                    
                05  COMM-EF10-DACCORD            PIC  X(08).                    
                05  COMM-EF10-LNOMACCORD         PIC  X(10).                    
                05  COMM-EF10-NSOCORIG           PIC  X(03).                    
                05  COMM-EF10-NLIEUORIG          PIC  X(03).                    
                05  COMM-EF10-NORIGINE           PIC  X(07).                    
                05  COMM-EF10-CMOTIF             PIC  X(10).                    
                05  COMM-EF10-CRENDU             PIC  X(05).                    
                05  COMM-EF10-LRENDU             PIC  X(20).                    
                05  COMM-EF10-CGARANTI           PIC  X(05).                    
                05  COMM-EF10-LGARANTI           PIC  X(20).                    
                05  COMM-EF10-NENTCDE            PIC  X(05).                    
                05  COMM-EF10-LENTCDE            PIC  X(20).                    
                05  COMM-EF10-CLITRT             PIC  X(05).                    
                05  COMM-EF10-MLIBERR            PIC  X(78).                    
                05  COMM-EF10-WQTEMUL            PIC  X(01).                    
                05  COMM-EF10-WNHSMUL            PIC  X(01).                    
                05  COMM-EF10-ORIG-NSOC          PIC  X(03).                    
                05  COMM-EF10-ORIG-NLIEU         PIC  X(03).                    
                05  COMM-EF10-ORIG-NSLIEU        PIC  X(03).                    
                05  COMM-EF10-ORIG-LITRT         PIC  X(05).                    
                05  COMM-EF10-ORIG-CTYPLIEU      PIC  X(01).                    
                05  COMM-EF10-DEST-NSOC          PIC  X(03).                    
                05  COMM-EF10-DEST-NLIEU         PIC  X(03).                    
                05  COMM-EF10-DEST-NSLIEU        PIC  X(03).                    
                05  COMM-EF10-DEST-LITRT         PIC  X(05).                    
                05  COMM-EF10-DEST-CTYPLIEU      PIC  X(01).                    
                05  COMM-EF10-FLAG               PIC  X(01).                    
                    88 COMM-EF10-FLAG-NOK                   VALUE '0'.          
                    88 COMM-EF10-FLAG-OK                    VALUE '1'.          
                05  COMM-EF10-ZPRIX              PIC  X(02).                    
                05  COMM-EF10-COPER              PIC  X(10).                    
                05  COMM-EF10-ITEM               PIC  9(02).                    
                05  COMM-EF10-POS-MAX            PIC  9(02).                    
                05  COMM-EF10-CPT-LIGNE          PIC  9(02).                    
                05  COMM-EF10-NSERIE             PIC  X(16).                    
                05  COMM-EF10-NCODIC             PIC  X(07).                    
                05  COMM-EF10-QTE                PIC  9(03).                    
                05  COMM-EF10-QTE-X              PIC  X(03).                    
      *                                                                         
                05  COMM-EF10-HEGSA.                                            
                    07 COMM-EF10-HEGSA-TAB       PIC  X(700).                   
                    07 COMM-EF10-NB-HEGSA        PIC  9(2).                     
FC              05  COMM-EF10-NBP-TVA            PIC 9.                         
FC              05  COMM-EF10-TAB-TVA.                                          
FC                  07 COMM-EF10-POSTE-TVA OCCURS 5.                            
FC                     09 COMM-EF10-CTVA      PIC X(5).                         
FC                     09 COMM-EF10-TX1       PIC S9V9(4) COMP-3.               
FC                     09 COMM-EF10-TX2       PIC S9V9(4) COMP-3.               
FC                     09 COMM-EF10-DTVA      PIC X(8).                         
      *       4 FILLER                        PIC X(163).                       
              4 FILLER                        PIC X(100).                       
      *--------ZONES PROPRES A TEF15                                            
             03 COMM-EF15-SSPRG-15    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF15-APPLI.                                                
AD05  *         05  COMM-EF15-NPAGE              PIC  9(02).                    
AD05  *         05  COMM-EF15-NPAGE-MAX          PIC  9(02).                    
AD05            05  COMM-EF15-NPAGE              PIC  9(03).                    
AD05            05  COMM-EF15-NPAGE-MAX          PIC  9(03).                    
                05  COMM-EF15-CTRAIT             PIC  X(05).                    
                05  COMM-EF15-LTRAIT             PIC  X(30).                    
                05  COMM-EF15-CTIERS             PIC  X(05).                    
                05  COMM-EF15-CTIERS-AFF         PIC  X(05).                    
                05  COMM-EF15-LTIERS             PIC  X(20).                    
                05  COMM-EF15-NENVOI             PIC  X(07).                    
                05  COMM-EF15-DENVOI             PIC  X(08).                    
                05  COMM-EF15-NACCORD            PIC  X(12).                    
                05  COMM-EF15-DACCORD            PIC  X(08).                    
                05  COMM-EF15-NSOCORIG           PIC  X(03).                    
                05  COMM-EF15-NLIEUORIG          PIC  X(03).                    
                05  COMM-EF15-NLIEUHS            PIC  X(03).                    
                05  COMM-EF15-NORIGINE           PIC  X(07).                    
                05  COMM-EF15-TOTAL              PIC  S9(11)V99 COMP-3.         
AD05  *       4 FILLER                           PIC  X(1151).                  
AD05          4 FILLER                           PIC  X(1149).                  
      *                                                                         
      *--------ZONES PROPRES A TEF15                                            
             03 COMM-EF16-SSPRG-16    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF16-APPLI.                                                
                05  COMM-EF16-NPAGE              PIC  9(02).                    
                05  COMM-EF16-NPAGE-MAX          PIC  9(02).                    
                05  COMM-EF16-CTRAIT             PIC  X(05).                    
                05  COMM-EF16-LTRAIT             PIC  X(30).                    
                05  COMM-EF16-CTIERS             PIC  X(05).                    
                05  COMM-EF16-CTIERS-AFF         PIC  X(05).                    
                05  COMM-EF16-LTIERS             PIC  X(20).                    
                05  COMM-EF16-NENVOI             PIC  X(07).                    
                05  COMM-EF16-DENVOI             PIC  X(08).                    
                05  COMM-EF16-NACCORD            PIC  X(12).                    
                05  COMM-EF16-DACCORD            PIC  X(08).                    
                05  COMM-EF16-NSOCORIG           PIC  X(03).                    
                05  COMM-EF16-NLIEUORIG          PIC  X(03).                    
                05  COMM-EF16-NLIEUHS            PIC  X(03).                    
                05  COMM-EF16-NORIGINE           PIC  X(07).                    
                05  COMM-EF16-CRENDU             PIC  X(05).                    
                05  COMM-EF16-LRENDU             PIC  X(20).                    
                05  COMM-EF16-NRENDU             PIC  X(20).                    
                05  COMM-EF16-DRENDU             PIC  X(08).                    
             4  FILLER                           PIC  X(1105).                  
      *                                                                         
      *--------ZONES PROPRES A TEF20                                            
             03  COMM-EF20-SSPRG-20    REDEFINES COMM-EF00-SSPRGS.              
                 04  COMM-EF20-APPLI.                                           
                     05  COMM-EF20-ITEM               PIC 9(03).                
                     05  COMM-EF20-ITEM-MAX           PIC 9(03).                
                     05  COMM-EF20-PASSAGE            PIC X(01).                
                         88  COMM-EF20-PREMIER-PASSAGE    VALUE '0'.            
                         88  COMM-EF20-STANDARD           VALUE '1'.            
                         88  COMM-EF20-PF5                VALUE '2'.            
                         88  COMM-EF20-PF12               VALUE '3'.            
                     05  COMM-EF20-NRENDU             PIC X(20).                
                     05  COMM-EF20-CRENDU             PIC X(05).                
                     05  COMM-EF20-LRENDU             PIC X(20).                
                     05  COMM-EF20-CTYPRENDU          PIC X(01).                
                     05  COMM-EF20-CTRL-TAUX          PIC 9(01).                
                     05  COMM-EF20-CMOTIF             PIC X(10).                
                     05  COMM-EF20-DESTRET.                                     
                         07  COMM-EF20-D-NSOC         PIC X(03).                
                         07  COMM-EF20-D-NLIEU        PIC X(03).                
                         07  COMM-EF20-D-NSLIEU       PIC X(03).                
                         07  COMM-EF20-D-CLIEUTRT.                              
                             09 COMM-EF20-D-CLIEU-A   PIC X(02).                
                             09 COMM-EF20-D-CLIEU-B   PIC X(03).                
                     05  COMM-EF20-DRENDU             PIC X(08).                
                     05  COMM-EF20-NENVOI             PIC X(07).                
                     05  COMM-EF20-DENVOI             PIC X(08).                
                     05  COMM-EF20-NACCORD            PIC X(12).                
                     05  COMM-EF20-DACCORD            PIC X(08).                
                     05  COMM-EF20-NSOCORIG           PIC X(03).                
                     05  COMM-EF20-NLIEUORIG          PIC X(03).                
                     05  COMM-EF20-NORIGINE           PIC X(07).                
                     05  COMM-EF20-NCODIC             PIC X(07).                
                     05  COMM-EF20-NENTCDE            PIC X(05).                
                     05  COMM-EF20-CTIERS             PIC X(05).                
                     05  COMM-EF20-CTIERS-AFF         PIC X(05).                
                     05  COMM-EF20-MLIBERR            PIC X(78).                
                     05  COMM-EF20-MTOTAVO            PIC S9(7)V9(2)            
                                                              COMP-3.           
                     05  COMM-EF20-MTOTPROV           PIC S9(7)V9(2)            
                                                              COMP-3.           
                     05  COMM-EF20-MCTA               PIC X(01).                
                     05  COMM-EF20-CHARGE-AUTO        PIC X(01).                
                     05  COMM-EF20-AUTOLG             PIC X(01).                
                     05  COMM-EF20-GLOBAL             PIC X(01).                
                     05  COMM-EF20-DENVOI-FIN         PIC X(08).                
                     05  COMM-EF20-LIBCENTRE          PIC X(15).                
                     05  COMM-EF20-CCENTRE            PIC X(05).                
                     05  COMM-EF20-LCENTRE            PIC X(20).                
                     05  COMM-EF20-CTYPTIERS          PIC X(01).                
                     05  COMM-EF20-LIBTIERS           PIC X(15).                
                     05  COMM-EF20-CGSTOCK            PIC X(01).                
                     05  COMM-EF20-TAUX-TOLERANCE     PIC 9(03).                
                     05  COMM-EF20-NOM-TS             PIC X(08).                
AM1195               05  COMM-EF20-LTIERS             PIC X(20).                
PM0796               05  COMM-EF20-AVENANT            PIC X(01).                
MD0401               05  COMM-EF20-CDEVISE            PIC X(03).                
MD0401               05  COMM-EF20-CDEVREF            PIC X(03).                
MD0401               05  COMM-EF20-CDEVEQU            PIC X(03).                
MD0401               05  COMM-EF20-TAUXDEV            PIC S9V9(6)               
                                                           COMP-3.              
CP1007               05  COMM-EF20-PFKEY              PIC X(01).                
CP1007                   88  COMM-EF20-PF13           VALUE '1'.                
                 04  FILLER                           PIC X(919).               
      *                                                                         
      *                                                                         
      *--------ZONES PROPRES A TEF25                                            
             03 COMM-EF25-SSPRG-25    REDEFINES COMM-EF00-SSPRGS.               
L=31          4 COMM-EF25-APPLI.                                                
                05 COMM-EF25-DATEDEBUT           PIC X(6).                      
                05 COMM-EF25-DATEFIN             PIC X(6).                      
                05 COMM-EF25-CTIERS              PIC X(5).                      
                05 COMM-EF25-CTIERSEF            PIC X(5).                      
                05 COMM-EF25-DATLIM              PIC X(8).                      
                05 COMM-EF25-NUMREL              PIC X(1).                      
              4 FILLER                           PIC X(1247).                   
      *                                                                         
      *--------ZONES PROPRES A TEF26                                            
             03 COMM-EF26-SSPRG-26    REDEFINES COMM-EF00-SSPRGS.               
L=135         04 COMM-EF26-APPLI.                                               
                05 COMM-EF26-NHS-WOK             PIC X(01).                     
                   88 COMM-EF26-NHS-KO VALUE ' '.                               
                   88 COMM-EF26-NHS-OK VALUE '1'.                               
                05 COMM-EF26-CTIERS              PIC X(05).                     
                05 COMM-EF26-CTIERSAFF           PIC X(05).                     
                05 COMM-EF26-LNOM                PIC X(20).                     
                05 COMM-EF26-NENVOI              PIC X(07).                     
                05 COMM-EF26-DENVOI              PIC X(08).                     
                05 COMM-EF26-NSOCORIG            PIC X(03).                     
                05 COMM-EF26-NLIEUORIG           PIC X(03).                     
                05 COMM-EF26-NORIGINE            PIC X(07).                     
                05 COMM-EF26-NCODIC              PIC X(07).                     
                05 COMM-EF26-CFAM                PIC X(05).                     
                05 COMM-EF26-CMARQ               PIC X(05).                     
                05 COMM-EF26-LREF                PIC X(20).                     
                05 COMM-EF26-NUMREL              PIC X(01).                     
                05 COMM-EF26-D1REL               PIC X(08).                     
                05 COMM-EF26-D2REL               PIC X(08).                     
                05 COMM-EF26-D3REL               PIC X(08).                     
                05 COMM-EF26-CTAIRE              PIC X(15).                     
              4 FILLER                           PIC X(1142).                   
      *                                                                         
      *--------ZONES PROPRES A TEF30                                            
             03 COMM-EF30-SSPRG-30    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF30-APPLI.                                                
                05  COMM-EF30-ITEM               PIC  9(03).                    
                05  COMM-EF30-POS-MAX            PIC  9(03).                    
                05  COMM-EF30-CPT-LIGNE          PIC  9(02).                    
                05  COMM-EF30-DERNIER-PASSAGE PIC X(01).                        
                   88  COMM-EF30-STANDARD          VALUE '0'.                   
                   88  COMM-EF30-AJOUT             VALUE '1'.                   
                   88  COMM-EF30-ERREUR            VALUE '2'.                   
                05  COMM-EF30-NSOCORIG           PIC  X(03).                    
                05  COMM-EF30-NLIEUORIG          PIC  X(03).                    
                05  COMM-EF30-MLIBERR            PIC  X(78).                    
      *                                                                         
                05  COMM-EF30-HEGSA.                                            
                    07 COMM-EF30-HEGSA-TAB       PIC  X(700).                   
                    07 COMM-EF30-NB-HEGSA        PIC  9(2).                     
      *                                                                         
              4   FILLER                      PIC  X(450).                      
      *                                                                         
      *--------ZONES PROPRES A TEF31                                            
             03 COMM-EF31-SSPRG-31    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF31-APPLI.                                                
                05  COMM-EF31-ITEM               PIC  9(03).                    
                05  COMM-EF31-POS-MAX            PIC  9(03).                    
                05  COMM-EF31-CPT-LIGNE          PIC  9(02).                    
                05  COMM-EF31-DERNIER-PASSAGE PIC X(01).                        
                   88  COMM-EF31-STANDARD          VALUE '0'.                   
                   88  COMM-EF31-AJOUT             VALUE '1'.                   
                   88  COMM-EF31-ERREUR            VALUE '2'.                   
                05  COMM-EF31-MRENDU             PIC  X(05).                    
                05  COMM-EF31-CTRAIT             PIC  X(05).                    
                05  COMM-EF31-CMOTIF             PIC  X(10).                    
                05  COMM-EF31-CGSTOCK            PIC  X(01).                    
                05  COMM-EF31-TAUX               PIC  9(03).                    
                05  COMM-EF31-IR                 PIC  X(01).                    
                05  COMM-EF31-DESTRET.                                          
                    07  COMM-EF31-D-NSOC         PIC  X(03).                    
                    07  COMM-EF31-D-NLIEU        PIC  X(03).                    
                    07  COMM-EF31-D-NSLIEU       PIC  X(03).                    
                    07  COMM-EF31-D-CLIEUTRT.                                   
                        10 COMM-EF31-D-CLIEU-A   PIC  X(02).                    
                        10 COMM-EF31-D-CLIEU-B   PIC  X(03).                    
                05  COMM-EF31-PF5                PIC  X.                        
                05  COMM-EF31-MLIBERR            PIC  X(78).                    
              4   FILLER                      PIC  X(1151).                     
      *--------ZONES PROPRES A TEF50/51                                         
             03 COMM-EF50-SSPRG-50    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF50-APPLI.                                                
                05  COMM-EF50-NPAGE              PIC  9(02).                    
                05  COMM-EF50-NPAGE-MAX          PIC  9(02).                    
                05  COMM-EF50-CTRAIT-OP          PIC  X(5).                     
                05  COMM-EF50-LTRAIT-OP          PIC  X(20).                    
      *                                                                         
                05  COMM-EF51-NPAGE              PIC  9(02).                    
                05  COMM-EF51-NPAGE-MAX          PIC  9(02).                    
                05  COMM-EF51-NENVOI             PIC  X(07).                    
                05  COMM-EF51-DENVOI             PIC  X(08).                    
                05  COMM-EF51-CODE-TIERS         PIC  X(05).                    
      *--------ZONES PROPRES A TEF55/56                                         
             03 COMM-EF55-SSPRG-55    REDEFINES COMM-EF00-SSPRGS.               
              4 COMM-EF55-APPLI.                                                
                05  COMM-EF55-NPAGE              PIC  9(02).                    
                05  COMM-EF55-NPAGE-MAX          PIC  9(02).                    
                05  COMM-EF55-CTRAIT-OP          PIC  X(5).                     
                05  COMM-EF55-LTRAIT-OP          PIC  X(20).                    
      *                                                                         
                05  COMM-EF56-NPAGE              PIC  9(02).                    
                05  COMM-EF56-NPAGE-MAX          PIC  9(02).                    
                05  COMM-EF56-NENVOI             PIC  X(07).                    
                05  COMM-EF56-DENVOI             PIC  X(08).                    
                05  COMM-EF56-CODE-TIERS         PIC  X(05).                    
MBEN00*--------ZONES PROPRES A TEF90/91                                         
  "          03 COMM-EF91-SSPRG-90    REDEFINES COMM-EF00-SSPRGS.               
  "          04 COMM-EF91-APPLI.                                                
  "             05 COMM-EF91-RPROG            PIC X(05).                        
  "             05 COMM-EF91-INDMAX           PIC S9(5) COMP-3.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *         05 COMM-EF91-PAGE             PIC S9(4) COMP.                   
      *--                                                                       
                05 COMM-EF91-PAGE             PIC S9(4) COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *         05 COMM-EF91-NBP              PIC S9(4) COMP.                   
      *--                                                                       
                05 COMM-EF91-NBP              PIC S9(4) COMP-5.                 
      *}                                                                        
  "             05 COMM-EF91-NLIGNE           PIC 9(02).                        
  "             05 COMM-EF91-LLIEUA           PIC X(20).                        
  "             05 COMM-EF91-CTIERSA          PIC X(05).                        
  "             05 COMM-EF91-LTIERSA          PIC X(20).                        
  "             05 COMM-EF91-NENVOIA          PIC X(07).                        
  "             05 COMM-EF91-DENVOIA          PIC X(10).                        
  "             05 COMM-EF91-NENTCDEA         PIC X(05).                        
  "             05 COMM-EF91-LENTCDEA         PIC X(20).                        
  "             05 COMM-EF91-NACCORDA         PIC X(12).                        
  "             05 COMM-EF91-DACCORDA         PIC X(10).                        
  "             05 COMM-EF91-NSOCORIGA        PIC X(03).                        
  "             05 COMM-EF91-NLIEUORIGA       PIC X(03).                        
  "             05 COMM-EF91-NORIGINEA        PIC X(07).                        
  "             05 COMM-EF91-NRMAA            PIC X(40).                        
  "             05 COMM-EF91-TABLEAU OCCURS 9.                                  
  "                10 COMM-EF91-VALIDE        PIC X(01).                        
  "                10 COMM-EF91-FLAG-E-D      PIC X(01).                        
  "                10 COMM-EF91-LIGNE         PIC X(80).                        
  "             05 COMM-EF90-INFOS-EF91.                                        
  "                10 COMM-EF91-NCODIC        PIC X(07).                        
  "                10 COMM-EF91-CFAM          PIC X(05).                        
  "                10 COMM-EF91-CMARQ         PIC X(05).                        
  "                10 COMM-EF91-CTIERS        PIC X(05).                        
  "                10 COMM-EF91-LTIERS        PIC X(20).                        
  "                10 COMM-EF91-NENVOI        PIC X(07).                        
  "                10 COMM-EF91-DENVOI        PIC X(10).                        
  "                10 COMM-EF91-NENTCDE       PIC X(05).                        
  "                10 COMM-EF91-LENTCDE       PIC X(20).                        
  "                10 COMM-EF91-NACCORD       PIC X(12).                        
  "                10 COMM-EF91-DACCORD       PIC X(10).                        
  "                10 COMM-EF91-LNOMACCORD    PIC X(10).                        
  "                10 COMM-EF91-NRMA          PIC X(40).                        
  "                10 COMM-EF91-LADR1         PIC X(32).                        
  "                10 COMM-EF91-LADR2         PIC X(32).                        
  "                10 COMM-EF91-CPOSTAL       PIC X(05).                        
  "                10 COMM-EF91-LCOMMUNE      PIC X(26).                        
  "                10 COMM-EF91-NSERIE        PIC X(16).                        
  "                10 COMM-EF91-QTENV         PIC 9(05).                        
  "                10 COMM-EF91-CGARANTIE     PIC X(05).                        
  "                10 COMM-EF91-NSOCORIG      PIC X(03).                        
  "                10 COMM-EF91-NLIEUORIG     PIC X(03).                        
  "                10 COMM-EF91-NORIGINE      PIC X(07).                        
  "                10 COMM-EF91-PABASEFACT    PIC 9(09)V9(2).                   
MBEN00             10 COMM-EF91-DSAP          PIC X(08).                        
MBEN00             10 COMM-EF91-MODIF         PIC X(01).                        
                                                                                
