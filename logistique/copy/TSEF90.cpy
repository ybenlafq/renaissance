      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-EF90-RECORD.                                                      
           05 TS-EF90-TABLE            OCCURS 09.                               
              10 TS-EF90-VALIDE                PIC X(01).                       
              10 TS-EF90-FLAG-E-D              PIC X(01).                       
              10 TS-EF90-ENREG                 PIC X(80).                       
              10 TS-EF90-INFOS-EF91.                                            
                 15 TS-EF90-NSOCIETE              PIC X(03).                    
                 15 TS-EF90-CTRAIT                PIC X(05).                    
                 15 TS-EF90-NENVOI                PIC X(07).                    
                 15 TS-EF90-DENVOI                PIC X(08).                    
                 15 TS-EF90-CTIERS                PIC X(05).                    
                 15 TS-EF90-LTIERS                PIC X(20).                    
                 15 TS-EF90-NENTCDE               PIC X(05).                    
                 15 TS-EF90-LENTCDE               PIC X(20).                    
                 15 TS-EF90-NSOCORIG              PIC X(03).                    
                 15 TS-EF90-NLIEUORIG             PIC X(03).                    
                 15 TS-EF90-NSOCMVTD              PIC X(03).                    
                 15 TS-EF90-NLIEUMVTD             PIC X(03).                    
                 15 TS-EF90-NSSLIEUMVTD           PIC X(03).                    
                 15 TS-EF90-CLIEUTRTMVTD          PIC X(05).                    
                 15 TS-EF90-NSOCMVTO              PIC X(03).                    
                 15 TS-EF90-NLIEUMVTO             PIC X(03).                    
                 15 TS-EF90-NSSLIEUMVTO           PIC X(03).                    
                 15 TS-EF90-CLIEUTRTMVTO          PIC X(05).                    
                 15 TS-EF90-NCODIC                PIC X(07).                    
                 15 TS-EF90-LREFFOURN             PIC X(20).                    
                 15 TS-EF90-NORIGINE              PIC X(07).                    
                 15 TS-EF90-NACCORD               PIC X(12).                    
                 15 TS-EF90-DACCORD               PIC X(08).                    
                 15 TS-EF90-CFAM                  PIC X(05).                    
                 15 TS-EF90-LFAM                  PIC X(20).                    
                 15 TS-EF90-CMARQ                 PIC X(05).                    
                 15 TS-EF90-LNOMACCORD            PIC X(10).                    
                 15 TS-EF90-NRMA                  PIC X(40).                    
                 15 TS-EF90-LADR1                 PIC X(32).                    
                 15 TS-EF90-LADR2                 PIC X(32).                    
                 15 TS-EF90-CPOSTAL               PIC X(05).                    
                 15 TS-EF90-LCOMMUNE              PIC X(26).                    
                 15 TS-EF90-NSERIE                PIC X(16).                    
                 15 TS-EF90-QTENV                 PIC 9(05).                    
                 15 TS-EF90-PABASEFACT            PIC 9(07)V9(2).               
                 15 TS-EF90-MTPROVSAV             PIC 9(07)V9(2).               
                 15 TS-EF90-CGARANTIE             PIC X(05).                    
                 15 TS-EF90-NPOSTE                PIC X(05).                    
                 15 TS-EF90-DSAP                  PIC X(08).                    
                                                                                
