      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM4400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM4400                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4400.                                                            
           02  RM44-NCODIC                                                      
               PIC X(0007).                                                     
           02  RM44-CTYPREG                                                     
               PIC X(0002).                                                     
           02  RM44-DEFFET                                                      
               PIC X(0008).                                                     
           02  RM44-QEXPO                                                       
               PIC S9(05) COMP-3.                                               
           02  RM44-QLS                                                         
               PIC S9(05) COMP-3.                                               
           02  RM44-CACID                                                       
               PIC X(0008).                                                     
           02  RM44-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM4400                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM4400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-CTYPREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-CTYPREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-DEFFET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-DEFFET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-QEXPO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-QEXPO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-QLS-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-QLS-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM44-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM44-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
