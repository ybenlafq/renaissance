      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU71   EMU71                                              00000020
      ***************************************************************** 00000030
       01   EMU71I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCENTI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPENTL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNDEPENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNDEPENTF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPENTI      PIC X(3).                                  00000290
           02 MLIGNEI OCCURS   16 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELIVL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MDDELIVL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDDELIVF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MDDELIVI     PIC X(8).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000350
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNSOCI  PIC X(3).                                       00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNMAGI  PIC X(3).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNVENTEI     PIC X(7).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCLIENTL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNCLIENTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNCLIENTF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNCLIENTI    PIC X(15).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVOLL   COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MVOLL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MVOLF   PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MVOLI   PIC X(6).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEVTEL     COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MQTEVTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTEVTEF     PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQTEVTEI     PIC X(3).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNMUTI  PIC X(7).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVOLMUTL     COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MVOLMUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MVOLMUTF     PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MVOLMUTI     PIC X(3).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELARTL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MSELARTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELARTF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSELARTI     PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCPTFL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MSOCPTFL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSOCPTFF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSOCPTFI     PIC X(3).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPTFL   COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MPTFL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPTFF   PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MPTFI   PIC X(3).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MSELI   PIC X.                                          00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MZONCMDI  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(58).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EMU71   EMU71                                              00001080
      ***************************************************************** 00001090
       01   EMU71O REDEFINES EMU71I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MNPAGEA   PIC X.                                          00001270
           02 MNPAGEC   PIC X.                                          00001280
           02 MNPAGEP   PIC X.                                          00001290
           02 MNPAGEH   PIC X.                                          00001300
           02 MNPAGEV   PIC X.                                          00001310
           02 MNPAGEO   PIC ZZZ9.                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNBPA     PIC X.                                          00001340
           02 MNBPC     PIC X.                                          00001350
           02 MNBPP     PIC X.                                          00001360
           02 MNBPH     PIC X.                                          00001370
           02 MNBPV     PIC X.                                          00001380
           02 MNBPO     PIC X(4).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNSOCENTA      PIC X.                                     00001410
           02 MNSOCENTC PIC X.                                          00001420
           02 MNSOCENTP PIC X.                                          00001430
           02 MNSOCENTH PIC X.                                          00001440
           02 MNSOCENTV PIC X.                                          00001450
           02 MNSOCENTO      PIC X(3).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNDEPENTA      PIC X.                                     00001480
           02 MNDEPENTC PIC X.                                          00001490
           02 MNDEPENTP PIC X.                                          00001500
           02 MNDEPENTH PIC X.                                          00001510
           02 MNDEPENTV PIC X.                                          00001520
           02 MNDEPENTO      PIC X(3).                                  00001530
           02 MLIGNEO OCCURS   16 TIMES .                               00001540
             03 FILLER       PIC X(2).                                  00001550
             03 MDDELIVA     PIC X.                                     00001560
             03 MDDELIVC     PIC X.                                     00001570
             03 MDDELIVP     PIC X.                                     00001580
             03 MDDELIVH     PIC X.                                     00001590
             03 MDDELIVV     PIC X.                                     00001600
             03 MDDELIVO     PIC X(8).                                  00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNSOCA  PIC X.                                          00001630
             03 MNSOCC  PIC X.                                          00001640
             03 MNSOCP  PIC X.                                          00001650
             03 MNSOCH  PIC X.                                          00001660
             03 MNSOCV  PIC X.                                          00001670
             03 MNSOCO  PIC X(3).                                       00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MNMAGA  PIC X.                                          00001700
             03 MNMAGC  PIC X.                                          00001710
             03 MNMAGP  PIC X.                                          00001720
             03 MNMAGH  PIC X.                                          00001730
             03 MNMAGV  PIC X.                                          00001740
             03 MNMAGO  PIC X(3).                                       00001750
             03 FILLER       PIC X(2).                                  00001760
             03 MNVENTEA     PIC X.                                     00001770
             03 MNVENTEC     PIC X.                                     00001780
             03 MNVENTEP     PIC X.                                     00001790
             03 MNVENTEH     PIC X.                                     00001800
             03 MNVENTEV     PIC X.                                     00001810
             03 MNVENTEO     PIC X(7).                                  00001820
             03 FILLER       PIC X(2).                                  00001830
             03 MNCLIENTA    PIC X.                                     00001840
             03 MNCLIENTC    PIC X.                                     00001850
             03 MNCLIENTP    PIC X.                                     00001860
             03 MNCLIENTH    PIC X.                                     00001870
             03 MNCLIENTV    PIC X.                                     00001880
             03 MNCLIENTO    PIC X(15).                                 00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MVOLA   PIC X.                                          00001910
             03 MVOLC   PIC X.                                          00001920
             03 MVOLP   PIC X.                                          00001930
             03 MVOLH   PIC X.                                          00001940
             03 MVOLV   PIC X.                                          00001950
             03 MVOLO   PIC X(6).                                       00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MQTEVTEA     PIC X.                                     00001980
             03 MQTEVTEC     PIC X.                                     00001990
             03 MQTEVTEP     PIC X.                                     00002000
             03 MQTEVTEH     PIC X.                                     00002010
             03 MQTEVTEV     PIC X.                                     00002020
             03 MQTEVTEO     PIC X(3).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MNMUTA  PIC X.                                          00002050
             03 MNMUTC  PIC X.                                          00002060
             03 MNMUTP  PIC X.                                          00002070
             03 MNMUTH  PIC X.                                          00002080
             03 MNMUTV  PIC X.                                          00002090
             03 MNMUTO  PIC X(7).                                       00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MVOLMUTA     PIC X.                                     00002120
             03 MVOLMUTC     PIC X.                                     00002130
             03 MVOLMUTP     PIC X.                                     00002140
             03 MVOLMUTH     PIC X.                                     00002150
             03 MVOLMUTV     PIC X.                                     00002160
             03 MVOLMUTO     PIC X(3).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MSELARTA     PIC X.                                     00002190
             03 MSELARTC     PIC X.                                     00002200
             03 MSELARTP     PIC X.                                     00002210
             03 MSELARTH     PIC X.                                     00002220
             03 MSELARTV     PIC X.                                     00002230
             03 MSELARTO     PIC X(5).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MSOCPTFA     PIC X.                                     00002260
             03 MSOCPTFC     PIC X.                                     00002270
             03 MSOCPTFP     PIC X.                                     00002280
             03 MSOCPTFH     PIC X.                                     00002290
             03 MSOCPTFV     PIC X.                                     00002300
             03 MSOCPTFO     PIC X(3).                                  00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MPTFA   PIC X.                                          00002330
             03 MPTFC   PIC X.                                          00002340
             03 MPTFP   PIC X.                                          00002350
             03 MPTFH   PIC X.                                          00002360
             03 MPTFV   PIC X.                                          00002370
             03 MPTFO   PIC X(3).                                       00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MSELA   PIC X.                                          00002400
             03 MSELC   PIC X.                                          00002410
             03 MSELP   PIC X.                                          00002420
             03 MSELH   PIC X.                                          00002430
             03 MSELV   PIC X.                                          00002440
             03 MSELO   PIC X.                                          00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MZONCMDA  PIC X.                                          00002470
           02 MZONCMDC  PIC X.                                          00002480
           02 MZONCMDP  PIC X.                                          00002490
           02 MZONCMDH  PIC X.                                          00002500
           02 MZONCMDV  PIC X.                                          00002510
           02 MZONCMDO  PIC X(15).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(58).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
