      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ERM24   ERM24                                              00000020
      ***************************************************************** 00000030
       01   ERM64I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFI    PIC X(20).                                      00000290
           02 MLIGNE1I OCCURS   3 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCDEPOTL  COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNSOCDEPOTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNSOCDEPOTF  PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNSOCDEPOTI  PIC X(3).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDEPOTL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNDEPOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDEPOTF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNDEPOTI     PIC X(3).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MQSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSTOCKF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MQSTOCKI     PIC X(6).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNRML  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MQNRML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQNRMF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MQNRMI  PIC X(6).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPACTML      COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MIMPACTML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MIMPACTMF      PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MIMPACTMI      PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MCFAMI    PIC X(5).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAGREGL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLAGREGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAGREGF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MLAGREGI  PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPACTRL      COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MIMPACTRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MIMPACTRF      PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MIMPACTRI      PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCMARQI   PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSAL      COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSAL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MSAF      PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MSAI      PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEXPL     COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MEXPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MEXPF     PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MEXPI     PIC X(10).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPPOSL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MIMPPOSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMPPOSF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MIMPPOSI  PIC X(6).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREMPLL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLREMPLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLREMPLF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLREMPLI  PIC X(14).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICLIEL    COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MNCODICLIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNCODICLIEF    PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNCODICLIEI    PIC X(7).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFLIEL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MLREFLIEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLREFLIEF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLREFLIEI      PIC X(20).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MIMPNEGL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MIMPNEGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MIMPNEGF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MIMPNEGI  PIC X(6).                                       00000940
      * Somme des stocks magasins                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTQSTKMAGL     COMP PIC S9(4).                            00000960
      *--                                                                       
           02 MTQSTKMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTQSTKMAGF     PIC X.                                     00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MTQSTKMAGI     PIC X(6).                                  00000990
      * Somme des ventes reelles                                        00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTQV8SRL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MTQV8SRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTQV8SRF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MTQV8SRI  PIC X(6).                                       00001040
      * Somme des ventes prevues                                        00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTQV8SPL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MTQV8SPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTQV8SPF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MTQV8SPI  PIC X(6).                                       00001090
      * Somme stock maxi                                                00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTQSML    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MTQSML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTQSMF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MTQSMI    PIC X(5).                                       00001140
      * somme stock maxi prevu                                          00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTQSMPL   COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MTQSMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTQSMPF   PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MTQSMPI   PIC X(5).                                       00001190
           02 MLIGNEI OCCURS   12 TIMES .                               00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCMAGL    COMP PIC S9(4).                            00001210
      *--                                                                       
             03 MNSOCMAGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCMAGF    PIC X.                                     00001220
             03 FILLER  PIC X(4).                                       00001230
             03 MNSOCMAGI    PIC X(6).                                  00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMAGL  COMP PIC S9(4).                                 00001250
      *--                                                                       
             03 MLMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLMAGF  PIC X.                                          00001260
             03 FILLER  PIC X(4).                                       00001270
             03 MLMAGI  PIC X(15).                                      00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOMAGL   COMP PIC S9(4).                            00001290
      *--                                                                       
             03 MCEXPOMAGL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCEXPOMAGF   PIC X.                                     00001300
             03 FILLER  PIC X(4).                                       00001310
             03 MCEXPOMAGI   PIC X(10).                                 00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSOSTDL   COMP PIC S9(4).                            00001330
      *--                                                                       
             03 MWASSOSTDL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MWASSOSTDF   PIC X.                                     00001340
             03 FILLER  PIC X(4).                                       00001350
             03 MWASSOSTDI   PIC X.                                     00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWASSORTL    COMP PIC S9(4).                            00001370
      *--                                                                       
             03 MWASSORTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWASSORTF    PIC X.                                     00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MWASSORTI    PIC X.                                     00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPOL      COMP PIC S9(4).                            00001410
      *--                                                                       
             03 MQEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQEXPOF      PIC X.                                     00001420
             03 FILLER  PIC X(4).                                       00001430
             03 MQEXPOI      PIC X(3).                                  00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLSL   COMP PIC S9(4).                                 00001450
      *--                                                                       
             03 MQLSL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQLSF   PIC X.                                          00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MQLSI   PIC X(3).                                       00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTKMAGL    COMP PIC S9(4).                            00001490
      *--                                                                       
             03 MQSTKMAGL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQSTKMAGF    PIC X.                                     00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MQSTKMAGI    PIC X(3).                                  00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV8SRL      COMP PIC S9(4).                            00001530
      *--                                                                       
             03 MQV8SRL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQV8SRF      PIC X.                                     00001540
             03 FILLER  PIC X(4).                                       00001550
             03 MQV8SRI      PIC X(5).                                  00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQV8SPL      COMP PIC S9(4).                            00001570
      *--                                                                       
             03 MQV8SPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQV8SPF      PIC X.                                     00001580
             03 FILLER  PIC X(4).                                       00001590
             03 MQV8SPI      PIC X(5).                                  00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFLAGL  COMP PIC S9(4).                                 00001610
      *--                                                                       
             03 MFLAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MFLAGF  PIC X.                                          00001620
             03 FILLER  PIC X(4).                                       00001630
             03 MFLAGI  PIC X.                                          00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOL   COMP PIC S9(4).                                 00001650
      *--                                                                       
             03 MQSOL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSOF   PIC X.                                          00001660
             03 FILLER  PIC X(4).                                       00001670
             03 MQSOI   PIC X(3).                                       00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSML   COMP PIC S9(4).                                 00001690
      *--                                                                       
             03 MQSML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQSMF   PIC X.                                          00001700
             03 FILLER  PIC X(4).                                       00001710
             03 MQSMI   PIC X(3).                                       00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSOPL  COMP PIC S9(4).                                 00001730
      *--                                                                       
             03 MQSOPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSOPF  PIC X.                                          00001740
             03 FILLER  PIC X(4).                                       00001750
             03 MQSOPI  PIC X(3).                                       00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSMPL  COMP PIC S9(4).                                 00001770
      *--                                                                       
             03 MQSMPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQSMPF  PIC X.                                          00001780
             03 FILLER  PIC X(4).                                       00001790
             03 MQSMPI  PIC X(3).                                       00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001810
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001820
           02 FILLER    PIC X(4).                                       00001830
           02 MLIBERRI  PIC X(78).                                      00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001850
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001860
           02 FILLER    PIC X(4).                                       00001870
           02 MCODTRAI  PIC X(4).                                       00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001890
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001900
           02 FILLER    PIC X(4).                                       00001910
           02 MCICSI    PIC X(5).                                       00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001930
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001940
           02 FILLER    PIC X(4).                                       00001950
           02 MNETNAMI  PIC X(8).                                       00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001970
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001980
           02 FILLER    PIC X(4).                                       00001990
           02 MSCREENI  PIC X(4).                                       00002000
      ***************************************************************** 00002010
      * SDF: ERM24   ERM24                                              00002020
      ***************************************************************** 00002030
       01   ERM64O REDEFINES ERM64I.                                    00002040
           02 FILLER    PIC X(12).                                      00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MDATJOUA  PIC X.                                          00002070
           02 MDATJOUC  PIC X.                                          00002080
           02 MDATJOUP  PIC X.                                          00002090
           02 MDATJOUH  PIC X.                                          00002100
           02 MDATJOUV  PIC X.                                          00002110
           02 MDATJOUO  PIC X(10).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MTIMJOUA  PIC X.                                          00002140
           02 MTIMJOUC  PIC X.                                          00002150
           02 MTIMJOUP  PIC X.                                          00002160
           02 MTIMJOUH  PIC X.                                          00002170
           02 MTIMJOUV  PIC X.                                          00002180
           02 MTIMJOUO  PIC X(5).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MNUMPAGEA      PIC X.                                     00002210
           02 MNUMPAGEC PIC X.                                          00002220
           02 MNUMPAGEP PIC X.                                          00002230
           02 MNUMPAGEH PIC X.                                          00002240
           02 MNUMPAGEV PIC X.                                          00002250
           02 MNUMPAGEO      PIC X(2).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MPAGEMAXA      PIC X.                                     00002280
           02 MPAGEMAXC PIC X.                                          00002290
           02 MPAGEMAXP PIC X.                                          00002300
           02 MPAGEMAXH PIC X.                                          00002310
           02 MPAGEMAXV PIC X.                                          00002320
           02 MPAGEMAXO      PIC X(2).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNCODICA  PIC X.                                          00002350
           02 MNCODICC  PIC X.                                          00002360
           02 MNCODICP  PIC X.                                          00002370
           02 MNCODICH  PIC X.                                          00002380
           02 MNCODICV  PIC X.                                          00002390
           02 MNCODICO  PIC 9(07).                                      00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLREFA    PIC X.                                          00002420
           02 MLREFC    PIC X.                                          00002430
           02 MLREFP    PIC X.                                          00002440
           02 MLREFH    PIC X.                                          00002450
           02 MLREFV    PIC X.                                          00002460
           02 MLREFO    PIC X(20).                                      00002470
           02 MLIGNE1O OCCURS   3 TIMES .                               00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MNSOCDEPOTA  PIC X.                                     00002500
             03 MNSOCDEPOTC  PIC X.                                     00002510
             03 MNSOCDEPOTP  PIC X.                                     00002520
             03 MNSOCDEPOTH  PIC X.                                     00002530
             03 MNSOCDEPOTV  PIC X.                                     00002540
             03 MNSOCDEPOTO  PIC X(3).                                  00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MNDEPOTA     PIC X.                                     00002570
             03 MNDEPOTC     PIC X.                                     00002580
             03 MNDEPOTP     PIC X.                                     00002590
             03 MNDEPOTH     PIC X.                                     00002600
             03 MNDEPOTV     PIC X.                                     00002610
             03 MNDEPOTO     PIC X(3).                                  00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MQSTOCKA     PIC X.                                     00002640
             03 MQSTOCKC     PIC X.                                     00002650
             03 MQSTOCKP     PIC X.                                     00002660
             03 MQSTOCKH     PIC X.                                     00002670
             03 MQSTOCKV     PIC X.                                     00002680
             03 MQSTOCKO     PIC X(6).                                  00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MQNRMA  PIC X.                                          00002710
             03 MQNRMC  PIC X.                                          00002720
             03 MQNRMP  PIC X.                                          00002730
             03 MQNRMH  PIC X.                                          00002740
             03 MQNRMV  PIC X.                                          00002750
             03 MQNRMO  PIC X(6).                                       00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MIMPACTMA      PIC X.                                     00002780
           02 MIMPACTMC PIC X.                                          00002790
           02 MIMPACTMP PIC X.                                          00002800
           02 MIMPACTMH PIC X.                                          00002810
           02 MIMPACTMV PIC X.                                          00002820
           02 MIMPACTMO      PIC X(6).                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MCFAMA    PIC X.                                          00002850
           02 MCFAMC    PIC X.                                          00002860
           02 MCFAMP    PIC X.                                          00002870
           02 MCFAMH    PIC X.                                          00002880
           02 MCFAMV    PIC X.                                          00002890
           02 MCFAMO    PIC X(5).                                       00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MLAGREGA  PIC X.                                          00002920
           02 MLAGREGC  PIC X.                                          00002930
           02 MLAGREGP  PIC X.                                          00002940
           02 MLAGREGH  PIC X.                                          00002950
           02 MLAGREGV  PIC X.                                          00002960
           02 MLAGREGO  PIC X(20).                                      00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MIMPACTRA      PIC X.                                     00002990
           02 MIMPACTRC PIC X.                                          00003000
           02 MIMPACTRP PIC X.                                          00003010
           02 MIMPACTRH PIC X.                                          00003020
           02 MIMPACTRV PIC X.                                          00003030
           02 MIMPACTRO      PIC X(6).                                  00003040
           02 FILLER    PIC X(2).                                       00003050
           02 MCMARQA   PIC X.                                          00003060
           02 MCMARQC   PIC X.                                          00003070
           02 MCMARQP   PIC X.                                          00003080
           02 MCMARQH   PIC X.                                          00003090
           02 MCMARQV   PIC X.                                          00003100
           02 MCMARQO   PIC X(5).                                       00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MSAA      PIC X.                                          00003130
           02 MSAC      PIC X.                                          00003140
           02 MSAP      PIC X.                                          00003150
           02 MSAH      PIC X.                                          00003160
           02 MSAV      PIC X.                                          00003170
           02 MSAO      PIC X.                                          00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MEXPA     PIC X.                                          00003200
           02 MEXPC     PIC X.                                          00003210
           02 MEXPP     PIC X.                                          00003220
           02 MEXPH     PIC X.                                          00003230
           02 MEXPV     PIC X.                                          00003240
           02 MEXPO     PIC X(10).                                      00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MIMPPOSA  PIC X.                                          00003270
           02 MIMPPOSC  PIC X.                                          00003280
           02 MIMPPOSP  PIC X.                                          00003290
           02 MIMPPOSH  PIC X.                                          00003300
           02 MIMPPOSV  PIC X.                                          00003310
           02 MIMPPOSO  PIC X(6).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MLREMPLA  PIC X.                                          00003340
           02 MLREMPLC  PIC X.                                          00003350
           02 MLREMPLP  PIC X.                                          00003360
           02 MLREMPLH  PIC X.                                          00003370
           02 MLREMPLV  PIC X.                                          00003380
           02 MLREMPLO  PIC X(14).                                      00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNCODICLIEA    PIC X.                                     00003410
           02 MNCODICLIEC    PIC X.                                     00003420
           02 MNCODICLIEP    PIC X.                                     00003430
           02 MNCODICLIEH    PIC X.                                     00003440
           02 MNCODICLIEV    PIC X.                                     00003450
           02 MNCODICLIEO    PIC X(7).                                  00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MLREFLIEA      PIC X.                                     00003480
           02 MLREFLIEC PIC X.                                          00003490
           02 MLREFLIEP PIC X.                                          00003500
           02 MLREFLIEH PIC X.                                          00003510
           02 MLREFLIEV PIC X.                                          00003520
           02 MLREFLIEO      PIC X(20).                                 00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MIMPNEGA  PIC X.                                          00003550
           02 MIMPNEGC  PIC X.                                          00003560
           02 MIMPNEGP  PIC X.                                          00003570
           02 MIMPNEGH  PIC X.                                          00003580
           02 MIMPNEGV  PIC X.                                          00003590
           02 MIMPNEGO  PIC X(6).                                       00003600
      * Somme des stocks magasins                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MTQSTKMAGA     PIC X.                                     00003630
           02 MTQSTKMAGC     PIC X.                                     00003640
           02 MTQSTKMAGP     PIC X.                                     00003650
           02 MTQSTKMAGH     PIC X.                                     00003660
           02 MTQSTKMAGV     PIC X.                                     00003670
           02 MTQSTKMAGO     PIC 9(06).                                 00003680
      * Somme des ventes reelles                                        00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MTQV8SRA  PIC X.                                          00003710
           02 MTQV8SRC  PIC X.                                          00003720
           02 MTQV8SRP  PIC X.                                          00003730
           02 MTQV8SRH  PIC X.                                          00003740
           02 MTQV8SRV  PIC X.                                          00003750
           02 MTQV8SRO  PIC 9(06).                                      00003760
      * Somme des ventes prevues                                        00003770
           02 FILLER    PIC X(2).                                       00003780
           02 MTQV8SPA  PIC X.                                          00003790
           02 MTQV8SPC  PIC X.                                          00003800
           02 MTQV8SPP  PIC X.                                          00003810
           02 MTQV8SPH  PIC X.                                          00003820
           02 MTQV8SPV  PIC X.                                          00003830
           02 MTQV8SPO  PIC 9(06).                                      00003840
      * Somme stock maxi                                                00003850
           02 FILLER    PIC X(2).                                       00003860
           02 MTQSMA    PIC X.                                          00003870
           02 MTQSMC    PIC X.                                          00003880
           02 MTQSMP    PIC X.                                          00003890
           02 MTQSMH    PIC X.                                          00003900
           02 MTQSMV    PIC X.                                          00003910
           02 MTQSMO    PIC 9(05).                                      00003920
      * somme stock maxi prevu                                          00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MTQSMPA   PIC X.                                          00003950
           02 MTQSMPC   PIC X.                                          00003960
           02 MTQSMPP   PIC X.                                          00003970
           02 MTQSMPH   PIC X.                                          00003980
           02 MTQSMPV   PIC X.                                          00003990
           02 MTQSMPO   PIC 9(05).                                      00004000
           02 MLIGNEO OCCURS   12 TIMES .                               00004010
             03 FILLER       PIC X(2).                                  00004020
             03 MNSOCMAGA    PIC X.                                     00004030
             03 MNSOCMAGC    PIC X.                                     00004040
             03 MNSOCMAGP    PIC X.                                     00004050
             03 MNSOCMAGH    PIC X.                                     00004060
             03 MNSOCMAGV    PIC X.                                     00004070
             03 MNSOCMAGO    PIC X(6).                                  00004080
             03 FILLER       PIC X(2).                                  00004090
             03 MLMAGA  PIC X.                                          00004100
             03 MLMAGC  PIC X.                                          00004110
             03 MLMAGP  PIC X.                                          00004120
             03 MLMAGH  PIC X.                                          00004130
             03 MLMAGV  PIC X.                                          00004140
             03 MLMAGO  PIC X(15).                                      00004150
             03 FILLER       PIC X(2).                                  00004160
             03 MCEXPOMAGA   PIC X.                                     00004170
             03 MCEXPOMAGC   PIC X.                                     00004180
             03 MCEXPOMAGP   PIC X.                                     00004190
             03 MCEXPOMAGH   PIC X.                                     00004200
             03 MCEXPOMAGV   PIC X.                                     00004210
             03 MCEXPOMAGO   PIC X(10).                                 00004220
             03 FILLER       PIC X(2).                                  00004230
             03 MWASSOSTDA   PIC X.                                     00004240
             03 MWASSOSTDC   PIC X.                                     00004250
             03 MWASSOSTDP   PIC X.                                     00004260
             03 MWASSOSTDH   PIC X.                                     00004270
             03 MWASSOSTDV   PIC X.                                     00004280
             03 MWASSOSTDO   PIC X.                                     00004290
             03 FILLER       PIC X(2).                                  00004300
             03 MWASSORTA    PIC X.                                     00004310
             03 MWASSORTC    PIC X.                                     00004320
             03 MWASSORTP    PIC X.                                     00004330
             03 MWASSORTH    PIC X.                                     00004340
             03 MWASSORTV    PIC X.                                     00004350
             03 MWASSORTO    PIC X.                                     00004360
             03 FILLER       PIC X(2).                                  00004370
             03 MQEXPOA      PIC X.                                     00004380
             03 MQEXPOC PIC X.                                          00004390
             03 MQEXPOP PIC X.                                          00004400
             03 MQEXPOH PIC X.                                          00004410
             03 MQEXPOV PIC X.                                          00004420
             03 MQEXPOO      PIC X(3).                                  00004430
             03 FILLER       PIC X(2).                                  00004440
             03 MQLSA   PIC X.                                          00004450
             03 MQLSC   PIC X.                                          00004460
             03 MQLSP   PIC X.                                          00004470
             03 MQLSH   PIC X.                                          00004480
             03 MQLSV   PIC X.                                          00004490
             03 MQLSO   PIC X(3).                                       00004500
             03 FILLER       PIC X(2).                                  00004510
             03 MQSTKMAGA    PIC X.                                     00004520
             03 MQSTKMAGC    PIC X.                                     00004530
             03 MQSTKMAGP    PIC X.                                     00004540
             03 MQSTKMAGH    PIC X.                                     00004550
             03 MQSTKMAGV    PIC X.                                     00004560
             03 MQSTKMAGO    PIC X(3).                                  00004570
             03 FILLER       PIC X(2).                                  00004580
             03 MQV8SRA      PIC X.                                     00004590
             03 MQV8SRC PIC X.                                          00004600
             03 MQV8SRP PIC X.                                          00004610
             03 MQV8SRH PIC X.                                          00004620
             03 MQV8SRV PIC X.                                          00004630
             03 MQV8SRO      PIC X(5).                                  00004640
             03 FILLER       PIC X(2).                                  00004650
             03 MQV8SPA      PIC X.                                     00004660
             03 MQV8SPC PIC X.                                          00004670
             03 MQV8SPP PIC X.                                          00004680
             03 MQV8SPH PIC X.                                          00004690
             03 MQV8SPV PIC X.                                          00004700
             03 MQV8SPO      PIC X(5).                                  00004710
             03 FILLER       PIC X(2).                                  00004720
             03 MFLAGA  PIC X.                                          00004730
             03 MFLAGC  PIC X.                                          00004740
             03 MFLAGP  PIC X.                                          00004750
             03 MFLAGH  PIC X.                                          00004760
             03 MFLAGV  PIC X.                                          00004770
             03 MFLAGO  PIC X.                                          00004780
             03 FILLER       PIC X(2).                                  00004790
             03 MQSOA   PIC X.                                          00004800
             03 MQSOC   PIC X.                                          00004810
             03 MQSOP   PIC X.                                          00004820
             03 MQSOH   PIC X.                                          00004830
             03 MQSOV   PIC X.                                          00004840
             03 MQSOO   PIC X(3).                                       00004850
             03 FILLER       PIC X(2).                                  00004860
             03 MQSMA   PIC X.                                          00004870
             03 MQSMC   PIC X.                                          00004880
             03 MQSMP   PIC X.                                          00004890
             03 MQSMH   PIC X.                                          00004900
             03 MQSMV   PIC X.                                          00004910
             03 MQSMO   PIC X(3).                                       00004920
             03 FILLER       PIC X(2).                                  00004930
             03 MQSOPA  PIC X.                                          00004940
             03 MQSOPC  PIC X.                                          00004950
             03 MQSOPP  PIC X.                                          00004960
             03 MQSOPH  PIC X.                                          00004970
             03 MQSOPV  PIC X.                                          00004980
             03 MQSOPO  PIC X(3).                                       00004990
             03 FILLER       PIC X(2).                                  00005000
             03 MQSMPA  PIC X.                                          00005010
             03 MQSMPC  PIC X.                                          00005020
             03 MQSMPP  PIC X.                                          00005030
             03 MQSMPH  PIC X.                                          00005040
             03 MQSMPV  PIC X.                                          00005050
             03 MQSMPO  PIC X(3).                                       00005060
           02 FILLER    PIC X(2).                                       00005070
           02 MLIBERRA  PIC X.                                          00005080
           02 MLIBERRC  PIC X.                                          00005090
           02 MLIBERRP  PIC X.                                          00005100
           02 MLIBERRH  PIC X.                                          00005110
           02 MLIBERRV  PIC X.                                          00005120
           02 MLIBERRO  PIC X(78).                                      00005130
           02 FILLER    PIC X(2).                                       00005140
           02 MCODTRAA  PIC X.                                          00005150
           02 MCODTRAC  PIC X.                                          00005160
           02 MCODTRAP  PIC X.                                          00005170
           02 MCODTRAH  PIC X.                                          00005180
           02 MCODTRAV  PIC X.                                          00005190
           02 MCODTRAO  PIC X(4).                                       00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MCICSA    PIC X.                                          00005220
           02 MCICSC    PIC X.                                          00005230
           02 MCICSP    PIC X.                                          00005240
           02 MCICSH    PIC X.                                          00005250
           02 MCICSV    PIC X.                                          00005260
           02 MCICSO    PIC X(5).                                       00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MNETNAMA  PIC X.                                          00005290
           02 MNETNAMC  PIC X.                                          00005300
           02 MNETNAMP  PIC X.                                          00005310
           02 MNETNAMH  PIC X.                                          00005320
           02 MNETNAMV  PIC X.                                          00005330
           02 MNETNAMO  PIC X(8).                                       00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MSCREENA  PIC X.                                          00005360
           02 MSCREENC  PIC X.                                          00005370
           02 MSCREENP  PIC X.                                          00005380
           02 MSCREENH  PIC X.                                          00005390
           02 MSCREENV  PIC X.                                          00005400
           02 MSCREENO  PIC X(4).                                       00005410
                                                                                
