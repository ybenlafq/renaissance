      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000010*                                                                 00010000
000020**************************************************************    00020000
000030*        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00030000
000040**************************************************************    00040000
000050*                                                                 00050000
000060* XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00060000
000070*      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00070000
000080*      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00080000
000090*      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00090000
000091*                                                                 00100000
000092* COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00110000
000093* COMPRENANT :                                                    00120000
000094* 1 - LES ZONES RESERVEES A AIDA                                  00130000
000095* 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00140000
000096* 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00150000
000097* 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00160000
000098* 5 - LES ZONES RESERVEES APPLICATIVES                            00170000
000099*                                                                 00180000
000100* COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00190000
000110* PAR AIDA                                                        00200000
000120*                                                                 00210000
000130*-------------------------------------------------------------    00220000
000140*                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000150*01  COM-GS10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00240001
      *--                                                                       
       01  COM-GS10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
000160*                                                                 00250000
000170 01  Z-COMMAREA.                                                  00260000
000180*                                                                 00270000
000190* ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
000191     02 FILLER-COM-AIDA      PIC X(100).                          00290000
000192*                                                                 00300000
000193* ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
000194     02 COMM-CICS-APPLID     PIC X(8).                            00320000
000195     02 COMM-CICS-NETNAM     PIC X(8).                            00330000
000196     02 COMM-CICS-TRANSA     PIC X(4).                            00340000
000197*                                                                 00350000
000198* ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
000199     02 COMM-DATE-SIECLE     PIC XX.                              00370000
000200     02 COMM-DATE-ANNEE      PIC XX.                              00380000
000210     02 COMM-DATE-MOIS       PIC XX.                              00390000
000220     02 COMM-DATE-JOUR       PIC 99.                              00400000
000230*   QUANTIEMES CALENDAIRE ET STANDARD                             00410000
000240     02 COMM-DATE-QNTA       PIC 999.                             00420000
000250     02 COMM-DATE-QNT0       PIC 99999.                           00430000
000260*   ANNEE BISSEXTILE 1=OUI 0=NON                                  00440000
000270     02 COMM-DATE-BISX       PIC 9.                               00450000
000280*    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
000290     02 COMM-DATE-JSM        PIC 9.                               00470000
000291*   LIBELLES DU JOUR COURT - LONG                                 00480000
000292     02 COMM-DATE-JSM-LC     PIC XXX.                             00490000
000293     02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00500000
000294*   LIBELLES DU MOIS COURT - LONG                                 00510000
000295     02 COMM-DATE-MOIS-LC    PIC XXX.                             00520000
000296     02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00530000
000297*   DIFFERENTES FORMES DE DATE                                    00540000
000298     02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00550000
000299     02 COMM-DATE-AAMMJJ     PIC X(6).                            00560000
000300     02 COMM-DATE-JJMMSSAA   PIC X(8).                            00570000
000310     02 COMM-DATE-JJMMAA     PIC X(6).                            00580000
000320     02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00590000
000330     02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00600000
000340*   TRAITEMENT DU NUMERO DE SEMAINE                               00610000
000350     02 COMM-DATE-WEEK.                                           00620000
000360        05 COMM-DATE-SEMSS   PIC 99.                              00630000
000370        05 COMM-DATE-SEMAA   PIC 99.                              00640000
000380        05 COMM-DATE-SEMNU   PIC 99.                              00650000
000390*                                                                 00660000
000391     02 COMM-DATE-FILLER     PIC X(08).                           00670000
000392*                                                                 00680000
000393* ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000394*    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00700000
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
000395     02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00710000
000396*                                                                 00720000
000397* ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00730000
000398     02 COMM-GS10-MENU.                                           00740000
000399*                                                                 00720000
000400*                                              ENTREPOT                 00
000401        03 COMM-GS10-NSOC           PIC X(3).                     00970003
000402        03 COMM-GS10-DEPOT          PIC X(3).                     00970004
000402        03 COMM-GS10-LDEPOT         PIC X(20).                    00970004
000403*                                                                 00720000
000404*                                              FAMILLE                  00
000410        03 COMM-GS10-FAMILLE        PIC X(5).                     00970005
000411*                                                                 00720000
000412*                                              LIBELLE FAMILLE          00
000413        03 COMM-GS10-LIB-FAMILLE    PIC X(20).                          05
000414*                                                                 00720000
000415*                                              CODIC                    00
000420        03 COMM-GS10-CODIC          PIC X(7).                     00970006
000421*                                                                 00720000
000422*                                              MARQUE                   00
000423        03 COMM-GS10-MARQUE         PIC X(5).                     00970006
000424*                                                                 00720000
000425*                                              STATUT                   00
000426        03 COMM-GS10-STATUT         PIC X(3).                     00970006
000427*                                                                 00720000
000428*                                              SENSIBILITE APPRO        00
000429        03 COMM-GS10-SENS-APPRO     PIC X.                        00970006
000430*                                                                 00720000
000431*                                              SENSIBILITE VENTE        00
000432        03 COMM-GS10-SENS-VENTE     PIC X.                        00970006
000433*                                                                 00720000
000434*                                              REFERENCE                00
000435        03 COMM-GS10-REFERENCE      PIC X(20).                    00970006
000436*                                                                 00720000
000437*                                              TYPE CONDIT. CODIC       00
000438        03 COMM-GS10-TYP-CONDT      PIC X(5).                     00970006
000439*                                                                 00720000
000440*                                              STATUT ASSOR             00
000441        03 COMM-GS10-ASSOR          PIC X(5).                     00970007
000442*                                                                 00720000
000443*                                              LIBELLE STAT ASSOR       00
000444        03 COMM-GS10-LIB-ASSOR      PIC X(20).                    00970007
000445*                                                                 00720000
000446*                                              STATUT APPRO             00
000447        03 COMM-GS10-APPRO          PIC X(5).                     00970007
000448*                                                                 00720000
000449*                                              LIBELLE STAT APPRO       00
000450        03 COMM-GS10-LIB-APPRO      PIC X(20).                    00970007
000451*                                                                 00720000
000452*                                              STATUT EXPO              00
000453        03 COMM-GS10-EXPO           PIC X(5).                     00970007
000454*                                                                 00720000
000455*                                              LIBELLE STAT EXPO        00
000456        03 COMM-GS10-LIB-EXPO       PIC X(20).                    00970007
000457*                                                                 00720000
000458* AJOUT ML                                   ENTREPOTS AUTORISES        00
              03 COMM-GS10-TYPUSER        PIC X(08).                            
                 88 COMM-GS10-88-MONODEP  VALUE 'MONODEP '.                     
                 88 COMM-GS10-88-MULTIDEP VALUE 'MULTIDEP'.                     
                 88 COMM-GS10-88-MULTISOC VALUE 'MULTISOC'.                     
                 88 COMM-GS10-88-INTEGRAL VALUE 'INTEGRAL'.                     
              03 COMM-GS10-TAB-DEP.                                         0001
NT               04 COMM-GS10-TAB-DEPOTS  OCCURS 100.                       0002
                    05 COMM-GS10-AUTOR-NSOC   PIC X(3).                        0
                    05 COMM-GS10-AUTOR-NDEPOT PIC X(3).                        0
              03 COMM-GS10-NB-AUTOR-DEP       PIC S9(3) COMP-3.             0003
              03 COMM-GS10-CFLGMAG            PIC X(5).                     0003
000458*                                              SOCIETE                  00
000459        03 COMM-GS10-SOCIETE        PIC X(3).                     00970003
              03 COMM-GS10-TAB-SOC.                                         0001
                 04 COMM-GS10-OCC-SOC        PIC X(3) OCCURS 10.            0002
              03 COMM-GS10-NBSOC             PIC S9(3) COMP-3.              0003
              03 COMM-GS10-CTYPLIEU       PIC X(1).                             
              03 COMM-GS10-CTYPSOC        PIC X(3).                             
000457*                                                                 00720000
000458*                                              MAGASIN                  00
000459        03 COMM-GS10-MAG            PIC X(3).                     00970003
000460*                                                                 00720000
000461*                                              OCTETS LIBRES            00
000462     02 COMM-GS10-APPLI.                                          00970010
000470        03 COMM-GS10-CACID          PIC X(4).                     00970020
000470        03 COMM-GS10-NPAGE          PIC 9(2).                     00970020
000470        03 COMM-GS10-NLIGNE         PIC 9(2).                     00970020
SM    *       03 COMM-GS10-FILLER         PIC X(3530).                  00970020
SM    *       03 COMM-GS10-FILLER         PIC X(3431).                  00970020
NT    *       03 COMM-GS10-FILLER         PIC X(2891).                  00970020
         03 COMM-SL30-DATA.                                                     
          05 COMM-DATE-DEMAIN-SAMJ   PIC X(08).                                 
          05 COMM-DATE-DEMAIN-JMA    PIC X(08).                                 
          05 COMM-PGM-APPEL          PIC X(05).                                 
          05 COMM-SL30-INDTS         PIC S9(5) COMP-3.                          
          05 COMM-SL30-INDMAX        PIC S9(5) COMP-3.                          
          05 COMM-SL30-ENTETE.                                                  
             10 COMM-CODLANG            PIC X(02).                         00760
             10 COMM-CODPIC             PIC X(02).                         00760
             10 COMM-SL30-NSOCIETE   PIC X(3).                                  
             10 COMM-SL30-NCODIC     PIC X(07).                                 
             10 COMM-SL30-DONNEES.                                              
                15 COMM-SL30-LREFFOURN  PIC X(20).                              
                15 COMM-SL30-CFAM       PIC X(05).                              
                15 COMM-SL30-STATUT     PIC X(03).                              
                15 COMM-SL30-CMARQ      PIC X(05).                              
          05 COMM-SL30-CHOIX-CRSL.                                              
             10 COMM-SL30-LISTE-CRSL.                                           
                15 COMM-SL30-CRSL       PIC X(05) OCCURS 6.                     
             10 COMM-SL30-MSGENT     PIC X(07) OCCURS 6.                        
      *      FILLER.                                                            
HV           03 COMM-GS10-FILLER          PIC X(2729).                          
                                                                                
