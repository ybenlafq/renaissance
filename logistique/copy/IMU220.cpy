      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT  IMU220 AU 26/02/2003   *      
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,15,BI,A,                          *        
      *                           35,03,PD,A,                          *        
      *                           38,20,BI,A,                          *        
      *                           58,05,PD,A,                          *        
      *                           63,01,PD,A,                          *        
      *                           64,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-:IMU220:.                                                      
LM0906*     05 NOMETAT-:IMU220:         PIC X(6).                               
LM0906      05 NOMETAT-:IMU220:         PIC X(7).                               
            05 RUPTURES-:IMU220:.                                               
           10 :IMU220:-NSOCIETE         PIC X(03).                      007  003
           10 :IMU220:-NLIEU            PIC X(03).                      010  003
           10 :IMU220:-NMUTATION        PIC X(07).                      013  007
           10 :IMU220:-SAUT             PIC X(15).                      020  015
           10 :IMU220:-WSEQFAM          PIC S9(05)      COMP-3.         035  003
           10 :IMU220:-LAGREGATED       PIC X(20).                      038  020
           10 :IMU220:-ZTRI             PIC S9(07)V9(2) COMP-3.         058  005
           10 :IMU220:-NSEQ             PIC S9(01)      COMP-3.         063  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 FMU220-SEQUENCE         PIC S9(04) COMP.                          
      *--                                                                       
           10 :IMU220:-SEQUENCE         PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-:IMU220:.                                                 
           10 :IMU220:-CMARQ            PIC X(05).                      066  005
           10 :IMU220:-COLACTIF         PIC X(01).                      071  001
           10 :IMU220:-COMMENTAIRE      PIC X(09).                      072  009
           10 :IMU220:-CVDESCRIPT1      PIC X(05).                      081  005
           10 :IMU220:-CVDESCRIPT2      PIC X(05).                      086  005
           10 :IMU220:-CVDESCRIPT3      PIC X(05).                      091  005
           10 :IMU220:-FLAG-CALCUL      PIC X(01).                      096  001
           10 :IMU220:-LFAM             PIC X(20).                      097  020
           10 :IMU220:-LIEU-DEPOT       PIC X(20).                      117  020
           10 :IMU220:-LLIEU            PIC X(20).                      137  020
           10 :IMU220:-LREFFOURN        PIC X(20).                      157  020
           10 :IMU220:-LSTATCOMP        PIC X(03).                      177  003
           10 :IMU220:-NCODIC9          PIC X(09).                      180  009
           10 :IMU220:-NDEPOT           PIC X(03).                      189  003
           10 :IMU220:-NSOCENTR         PIC X(03).                      192  003
           10 :IMU220:-STATUT-PRODUIT   PIC X(01).                      195  001
           10 :IMU220:-TOP0             PIC X(01).                      196  001
           10 :IMU220:-WFAM             PIC X(01).                      197  001
           10 :IMU220:-W8020-RUP        PIC X(01).                      198  001
           10 :IMU220:-PRIX-VENTE       PIC S9(07)V9(2) COMP-3.         199  005
           10 :IMU220:-QCOLIDSTOCK      PIC S9(05)      COMP-3.         204  003
           10 :IMU220:-QMUT-ATT         PIC S9(05)      COMP-3.         207  003
           10 :IMU220:-QSTOCK-DEP       PIC S9(05)      COMP-3.         210  003
           10 :IMU220:-QSTOCK-MAG       PIC S9(05)      COMP-3.         213  003
           10 :IMU220:-QSTOCK-MAX       PIC S9(05)      COMP-3.         216  003
           10 :IMU220:-QSTOCK-MUT       PIC S9(05)      COMP-3.         219  003
           10 :IMU220:-QSTOCK-PRO       PIC S9(05)      COMP-3.         222  003
           10 :IMU220:-RANG             PIC S9(03)      COMP-3.         225  002
           10 :IMU220:-S-1              PIC S9(05)      COMP-3.         227  003
           10 :IMU220:-S-2              PIC S9(05)      COMP-3.         230  003
           10 :IMU220:-S-3              PIC S9(05)      COMP-3.         233  003
           10 :IMU220:-S-4              PIC S9(05)      COMP-3.         236  003
           10 :IMU220:-DATE-STO-PRO     PIC X(08).                      239  008
           10 :IMU220:-DEXTRACTION      PIC X(08).                      247  008
           10 :IMU220:-DFINSAIS         PIC X(08).                      255  008
           10 :IMU220:-DMUTATION        PIC X(08).                      263  008
            05 FILLER                   PIC X(241).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-FMU220-LONG         PIC S9(4)   COMP  VALUE +512.             
      *                                                                         
      *--                                                                       
        01  DSECT-:IMU220:-LONG         PIC S9(4) COMP-5  VALUE +512.           
                                                                                
      *}                                                                        
