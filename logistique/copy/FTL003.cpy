      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * - COPY SERVANT DANS BTL000 ET DANS BSF010 COPY DU FICHIER FTL003      13
      *  LONGUEUR = 260 OCTETS                                         *      13
      *  TYPE ENREG = 1 TOURNEE DE LIVRAISON -> COPY FTL003T           *      13
      *  TYPE ENREG = 2 VENTES               -> COPY FTL003V           *      13
      ******************************************************************        
      * MESSAGE DE CONTROLE TSF02C + ENTETE DE MUTATION                *        
      ******************************************************************        
       01 FTL003-DSECT.                                                         
              05 FTL003-NSOCDEPOT             PIC X(3).                         
              05 FTL003-NDEPOT                PIC X(3).                         
              05 FTL003-NSOC                  PIC X(3).                         
              05 FTL003-CPROTOUR              PIC X(5).                         
              05 FTL003-TYPE-ENREGISTREMENT   PIC X.                            
              05 FTL003-DATA.                                                   
      * LONGUEUR DE 260 OCTETS                                                  
                 10 FTL003T.                                                    
                    15 FTL003T-DDEMANDE       PIC X(8).                         
                    15 FTL003T-NSOC           PIC X(3).                         
                    15 FTL003T-CPROTOUR       PIC X(5).                         
                    15 FTL003T-DDELIV         PIC X(8).                         
                    15 FTL003T-NSOCDEPOT      PIC X(3).                         
                    15 FTL003T-NDEPOT         PIC X(3).                         
                    15 FTL003T-WTOURNEE       PIC X(1).                         
                    15 FTL003T-DHTOURNEE      PIC X(4).                         
                    15 FTL003T-WDESTOCK       PIC X(1).                         
                    15 FTL003T-DHDESTOCK      PIC X(4).                         
                    15 FTL003T-QCDENONCOM     PIC S9(5) COMP-3.                 
                    15 FTL003T-QPCENONCOM     PIC S9(5) COMP-3.                 
                    15 FTL003T-QCDECOM        PIC S9(5) COMP-3.                 
                    15 FTL003T-QPCECOM        PIC S9(5) COMP-3.                 
                    15 FTL003T-DRAFALE        PIC X(8).                         
                    15 FTL003T-NRAFALE        PIC X(3).                         
                    15 FTL003T-WRAFALE        PIC X(1).                         
                    15 FTL003T-FILLER         PIC X(181).                       
      ******************************************************************        
      * MESSAGE DE DONNEES FTL003V1 CONCERNANT LE DETAIL DESTOCK RTGD05*        
      * MESS DE DONNEES TL003V2 CONCERNANT LES VENTES A COMMUTER RTWS15*        
      * MESSAGE DE DONNEES FTL003V3 CONCERNANT LES VENTES LDM RTWS35   *        
      ******************************************************************        
              05 FTL003V-DATA REDEFINES FTL003-DATA.                            
      * LONGUEUR DE 239 OCTETS                                                  
                 10 FTL003V.                                                    
                    15 FTL003V1-WWS15          PIC X(1).                        
                    15 FTL003V1-WWS35          PIC X(1).                        
                    15 FTL003V1-WGD05          PIC X(1).                        
                    15 FTL003V1-NSOCDEPOT      PIC X(3).                        
                    15 FTL003V1-NDEPOT         PIC X(3).                        
                    15 FTL003V1-DATELIVR       PIC X(8).                        
                    15 FTL003V1-CPROTOUR       PIC X(5).                        
                    15 FTL003V1-CTOURNEE       PIC X(3).                        
                    15 FTL003V1-NSOC           PIC X(3).                        
                    15 FTL003V1-NMAGASIN       PIC X(3).                        
                    15 FTL003V1-NVENTE         PIC X(7).                        
                    15 FTL003V1-CMODLIVR       PIC X(3).                        
                    15 FTL003V1-NCODIC         PIC X(7).                        
                    15 FTL003V1-NSEQ           PIC X(2).                        
                    15 FTL003V1-QVENDUE        PIC S9(5) COMP-3.                
                    15 FTL003V1-DRAFALE        PIC X(8).                        
                    15 FTL003V1-NRAFALE        PIC X(3).                        
                    15 FTL003V1-NSATELLITE     PIC X(2).                        
                    15 FTL003V1-NCASE          PIC X(3).                        
                    15 FTL003V1-WCOMMUT        PIC X(1).                        
                    15 FTL003V1-NBONENLV       PIC X(7).                        
                    15 FTL003V2-NSOCIETE       PIC X(3).                        
                    15 FTL003V2-NLIEU          PIC X(3).                        
                    15 FTL003V2-NVENTE         PIC X(7).                        
                    15 FTL003V2-NCODIC         PIC X(7).                        
                    15 FTL003V2-NSEQ           PIC X(02).                       
                    15 FTL003V2-LCOMMENT       PIC X(35).                       
                    15 FTL003V2-CARACTSPE1     PIC X(05).                       
                    15 FTL003V2-CVCARACTSPE1   PIC X(05).                       
                    15 FTL003V2-CARACTSPE2     PIC X(05).                       
                    15 FTL003V2-CVCARACTSPE2   PIC X(05).                       
                    15 FTL003V2-CARACTSPE3     PIC X(05).                       
                    15 FTL003V2-CVCARACTSPE3   PIC X(05).                       
                    15 FTL003V2-CARACTSPE4     PIC X(05).                       
                    15 FTL003V2-CVCARACTSPE4   PIC X(05).                       
                    15 FTL003V2-CARACTSPE5     PIC X(05).                       
                    15 FTL003V2-CVCARACTSPE5   PIC X(05).                       
                    15 FTL003V3-NSOCIETE       PIC X(3).                        
                    15 FTL003V3-NBONENLV       PIC X(7).                        
                    15 FTL003V3-NLIEUORIG      PIC X(3).                        
                    15 FTL003V3-NLIEUDEST      PIC X(3).                        
                    15 FTL003V3-NSSLIEUDEST    PIC X(3).                        
                    15 FTL003V3-LNOM           PIC X(25).                       
                    15 FTL003V-FILLER          PIC X(17).                       
                                                                                
