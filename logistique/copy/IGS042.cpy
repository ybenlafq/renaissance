      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS042 AU 06/10/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,PD,A,                          *        
      *                           13,06,BI,A,                          *        
      *                           19,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS042.                                                        
            05 NOMETAT-IGS042           PIC X(6) VALUE 'IGS042'.                
            05 RUPTURES-IGS042.                                                 
           10 IGS042-NFILIALE           PIC X(03).                      007  003
           10 IGS042-WSEQFAM            PIC S9(05)      COMP-3.         010  003
           10 IGS042-NMAG               PIC X(06).                      013  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS042-SEQUENCE           PIC S9(04) COMP.                019  002
      *--                                                                       
           10 IGS042-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS042.                                                   
           10 IGS042-CFAM               PIC X(05).                      021  005
           10 IGS042-DATEDEB            PIC X(08).                      026  008
           10 IGS042-DATEFIN            PIC X(08).                      034  008
           10 IGS042-LMDLIV1            PIC X(20).                      042  020
           10 IGS042-LMDLIV2            PIC X(20).                      062  020
           10 IGS042-LTOTAL             PIC X(05).                      082  005
           10 IGS042-MDLIV1             PIC X(03).                      087  003
           10 IGS042-MDLIV2             PIC X(03).                      090  003
           10 IGS042-QTE1               PIC S9(08)      COMP-3.         093  005
           10 IGS042-QTE2               PIC S9(08)      COMP-3.         098  005
           10 IGS042-VALEUR1            PIC S9(12)V9(2) COMP-3.         103  008
           10 IGS042-VALEUR2            PIC S9(12)V9(2) COMP-3.         111  008
            05 FILLER                      PIC X(394).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS042-LONG           PIC S9(4)   COMP  VALUE +118.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS042-LONG           PIC S9(4) COMP-5  VALUE +118.           
                                                                                
      *}                                                                        
