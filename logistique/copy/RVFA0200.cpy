      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVFA0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFA0200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVFA0200.                                                            
           02  FA02-NSOCF                                                       
               PIC X(0003).                                                     
           02  FA02-NLIEUF                                                      
               PIC X(0003).                                                     
           02  FA02-NFACT                                                       
               PIC X(0007).                                                     
           02  FA02-NSOCV                                                       
               PIC X(0003).                                                     
           02  FA02-NLIEUV                                                      
               PIC X(0003).                                                     
           02  FA02-NORDV                                                       
               PIC X(0008).                                                     
           02  FA02-NBCDE                                                       
               PIC X(0007).                                                     
           02  FA02-NORDRE                                                      
               PIC X(0005).                                                     
           02  FA02-NSEQFV                                                      
               PIC X(0002).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFA0200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVFA0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NSOCF-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NSOCF-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NLIEUF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NLIEUF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NFACT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NFACT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NSOCV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NSOCV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NLIEUV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NLIEUV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NORDV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NORDV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NBCDE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NBCDE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FA02-NSEQFV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FA02-NSEQFV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
