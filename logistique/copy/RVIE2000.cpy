      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVIE2000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIE2000                         
      **********************************************************                
       01  RVIE2000.                                                            
           02  IE20-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  IE20-NDEPOT                                                      
               PIC X(0003).                                                     
           02  IE20-CACID                                                       
               PIC X(0008).                                                     
           02  IE20-DJOUR                                                       
               PIC X(0008).                                                     
           02  IE20-DHEURE                                                      
               PIC X(0004).                                                     
           02  IE20-NFICHE                                                      
               PIC X(0005).                                                     
           02  IE20-CMODSTOCK                                                   
               PIC X(0005).                                                     
           02  IE20-NCODIC3                                                     
               PIC X(0007).                                                     
           02  IE20-NCODIC4                                                     
               PIC X(0007).                                                     
           02  IE20-CEMP13                                                      
               PIC X(0002).                                                     
           02  IE20-CEMP23                                                      
               PIC X(0002).                                                     
           02  IE20-CEMP33                                                      
               PIC X(0003).                                                     
           02  IE20-CTYPEMPL3                                                   
               PIC X(0001).                                                     
           02  IE20-QSTOCKU3                                                    
               PIC S9(5) COMP-3.                                                
           02  IE20-CEMP14                                                      
               PIC X(0002).                                                     
           02  IE20-CEMP24                                                      
               PIC X(0002).                                                     
           02  IE20-CEMP34                                                      
               PIC X(0003).                                                     
           02  IE20-CTYPEMPL4                                                   
               PIC X(0001).                                                     
           02  IE20-QSTOCKU4                                                    
               PIC S9(5) COMP-3.                                                
           02  IE20-CFAM                                                        
               PIC X(0005).                                                     
           02  IE20-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  IE20-CMARQ                                                       
               PIC X(0005).                                                     
           02  IE20-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  IE20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVIE2000                                  
      **********************************************************                
       01  RVIE2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-DHEURE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-DHEURE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-NFICHE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-NFICHE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CMODSTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CMODSTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-NCODIC3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-NCODIC3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-NCODIC4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-NCODIC4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CEMP13-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CEMP13-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CEMP23-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CEMP23-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CEMP33-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CEMP33-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CTYPEMPL3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CTYPEMPL3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-QSTOCKU3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-QSTOCKU3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CEMP14-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CEMP14-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CEMP24-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CEMP24-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CEMP34-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CEMP34-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CTYPEMPL4-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CTYPEMPL4-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-QSTOCKU4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-QSTOCKU4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  IE20-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IE20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  IE20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
