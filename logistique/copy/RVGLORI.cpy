      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GLORI GESTION LIENS : ORIGINE SAISIE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGLORI.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGLORI.                                                             
      *}                                                                        
           05  GLORI-CTABLEG2    PIC X(15).                                     
           05  GLORI-CTABLEG2-REDEF REDEFINES GLORI-CTABLEG2.                   
               10  GLORI-CORIG           PIC X(05).                             
           05  GLORI-WTABLEG     PIC X(80).                                     
           05  GLORI-WTABLEG-REDEF  REDEFINES GLORI-WTABLEG.                    
               10  GLORI-WACTIF          PIC X(01).                             
               10  GLORI-LIBELLE         PIC X(20).                             
               10  GLORI-WDATA           PIC X(30).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGLORI-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGLORI-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GLORI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GLORI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GLORI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GLORI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
