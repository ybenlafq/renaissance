      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * TRANSFERT MUTATIONS VERS LM7                                    00000020
      ***************************************************************** 00000030
       01   ELW02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCDEPL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCDEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCDEPF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCDEPI      PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNDEPOTI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMUTL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MMUTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMUTF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MMUTI     PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOCI     PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIEUI    PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSELARL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MSELARL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSELARF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MSELARI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQPL      COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MQPL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MQPF      PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MQPI      PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQVL      COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MQVL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MQVF      PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MQVI      PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQQPL     COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MQQPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQQPF     PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQQPI     PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQQVL     COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MQQVL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MQQVF     PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQQVI     PIC X(6).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDESTL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDDESTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDESTF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDDESTI   PIC X(10).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHCHARGTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MHCHARGTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MHCHARGTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MHCHARGTI      PIC X(5).                                  00000690
           02 M172I OCCURS   12 TIMES .                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MSELI   PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMUTL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MNMUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMUTF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNMUTI  PIC X(7).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNSOCI  PIC X(3).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MNLIEUI      PIC X(3).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSELARL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MNSELARL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNSELARF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MNSELARI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQPL   COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MNQPL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNQPF   PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MNQPI   PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQVL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MNQVL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNQVF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNQVI   PIC X(6).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQQPL  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MNQQPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNQQPF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNQQPI  PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNQQVL  COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MNQQVL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNQQVF  PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNQQVI  PIC X(6).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDDESTL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MNDDESTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDDESTF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MNDDESTI     PIC X(10).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNHCHRGL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MNHCHRGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNHCHRGF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MNHCHRGI     PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLIBERRI  PIC X(80).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCICSI    PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MNETNAMI  PIC X(8).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MSCREENI  PIC X(4).                                       00001340
      ***************************************************************** 00001350
      * TRANSFERT MUTATIONS VERS LM7                                    00001360
      ***************************************************************** 00001370
       01   ELW02O REDEFINES ELW02I.                                    00001380
           02 FILLER    PIC X(12).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDATJOUA  PIC X.                                          00001410
           02 MDATJOUC  PIC X.                                          00001420
           02 MDATJOUP  PIC X.                                          00001430
           02 MDATJOUH  PIC X.                                          00001440
           02 MDATJOUV  PIC X.                                          00001450
           02 MDATJOUO  PIC X(10).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MPAGEA    PIC X.                                          00001550
           02 MPAGEC    PIC X.                                          00001560
           02 MPAGEP    PIC X.                                          00001570
           02 MPAGEH    PIC X.                                          00001580
           02 MPAGEV    PIC X.                                          00001590
           02 MPAGEO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MPAGEMAXA      PIC X.                                     00001620
           02 MPAGEMAXC PIC X.                                          00001630
           02 MPAGEMAXP PIC X.                                          00001640
           02 MPAGEMAXH PIC X.                                          00001650
           02 MPAGEMAXV PIC X.                                          00001660
           02 MPAGEMAXO      PIC X(3).                                  00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNSOCDEPA      PIC X.                                     00001690
           02 MNSOCDEPC PIC X.                                          00001700
           02 MNSOCDEPP PIC X.                                          00001710
           02 MNSOCDEPH PIC X.                                          00001720
           02 MNSOCDEPV PIC X.                                          00001730
           02 MNSOCDEPO      PIC X(3).                                  00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNDEPOTA  PIC X.                                          00001760
           02 MNDEPOTC  PIC X.                                          00001770
           02 MNDEPOTP  PIC X.                                          00001780
           02 MNDEPOTH  PIC X.                                          00001790
           02 MNDEPOTV  PIC X.                                          00001800
           02 MNDEPOTO  PIC X(3).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MMUTA     PIC X.                                          00001830
           02 MMUTC     PIC X.                                          00001840
           02 MMUTP     PIC X.                                          00001850
           02 MMUTH     PIC X.                                          00001860
           02 MMUTV     PIC X.                                          00001870
           02 MMUTO     PIC X(7).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MSOCA     PIC X.                                          00001900
           02 MSOCC     PIC X.                                          00001910
           02 MSOCP     PIC X.                                          00001920
           02 MSOCH     PIC X.                                          00001930
           02 MSOCV     PIC X.                                          00001940
           02 MSOCO     PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MLIEUA    PIC X.                                          00001970
           02 MLIEUC    PIC X.                                          00001980
           02 MLIEUP    PIC X.                                          00001990
           02 MLIEUH    PIC X.                                          00002000
           02 MLIEUV    PIC X.                                          00002010
           02 MLIEUO    PIC X(3).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MSELARA   PIC X.                                          00002040
           02 MSELARC   PIC X.                                          00002050
           02 MSELARP   PIC X.                                          00002060
           02 MSELARH   PIC X.                                          00002070
           02 MSELARV   PIC X.                                          00002080
           02 MSELARO   PIC X(5).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MQPA      PIC X.                                          00002110
           02 MQPC      PIC X.                                          00002120
           02 MQPP      PIC X.                                          00002130
           02 MQPH      PIC X.                                          00002140
           02 MQPV      PIC X.                                          00002150
           02 MQPO      PIC X(5).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MQVA      PIC X.                                          00002180
           02 MQVC      PIC X.                                          00002190
           02 MQVP      PIC X.                                          00002200
           02 MQVH      PIC X.                                          00002210
           02 MQVV      PIC X.                                          00002220
           02 MQVO      PIC X(6).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MQQPA     PIC X.                                          00002250
           02 MQQPC     PIC X.                                          00002260
           02 MQQPP     PIC X.                                          00002270
           02 MQQPH     PIC X.                                          00002280
           02 MQQPV     PIC X.                                          00002290
           02 MQQPO     PIC X(5).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MQQVA     PIC X.                                          00002320
           02 MQQVC     PIC X.                                          00002330
           02 MQQVP     PIC X.                                          00002340
           02 MQQVH     PIC X.                                          00002350
           02 MQQVV     PIC X.                                          00002360
           02 MQQVO     PIC X(6).                                       00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MDDESTA   PIC X.                                          00002390
           02 MDDESTC   PIC X.                                          00002400
           02 MDDESTP   PIC X.                                          00002410
           02 MDDESTH   PIC X.                                          00002420
           02 MDDESTV   PIC X.                                          00002430
           02 MDDESTO   PIC X(10).                                      00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MHCHARGTA      PIC X.                                     00002460
           02 MHCHARGTC PIC X.                                          00002470
           02 MHCHARGTP PIC X.                                          00002480
           02 MHCHARGTH PIC X.                                          00002490
           02 MHCHARGTV PIC X.                                          00002500
           02 MHCHARGTO      PIC X(5).                                  00002510
           02 M172O OCCURS   12 TIMES .                                 00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MSELA   PIC X.                                          00002540
             03 MSELC   PIC X.                                          00002550
             03 MSELP   PIC X.                                          00002560
             03 MSELH   PIC X.                                          00002570
             03 MSELV   PIC X.                                          00002580
             03 MSELO   PIC X.                                          00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MNMUTA  PIC X.                                          00002610
             03 MNMUTC  PIC X.                                          00002620
             03 MNMUTP  PIC X.                                          00002630
             03 MNMUTH  PIC X.                                          00002640
             03 MNMUTV  PIC X.                                          00002650
             03 MNMUTO  PIC X(7).                                       00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MNSOCA  PIC X.                                          00002680
             03 MNSOCC  PIC X.                                          00002690
             03 MNSOCP  PIC X.                                          00002700
             03 MNSOCH  PIC X.                                          00002710
             03 MNSOCV  PIC X.                                          00002720
             03 MNSOCO  PIC X(3).                                       00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MNLIEUA      PIC X.                                     00002750
             03 MNLIEUC PIC X.                                          00002760
             03 MNLIEUP PIC X.                                          00002770
             03 MNLIEUH PIC X.                                          00002780
             03 MNLIEUV PIC X.                                          00002790
             03 MNLIEUO      PIC X(3).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 MNSELARA     PIC X.                                     00002820
             03 MNSELARC     PIC X.                                     00002830
             03 MNSELARP     PIC X.                                     00002840
             03 MNSELARH     PIC X.                                     00002850
             03 MNSELARV     PIC X.                                     00002860
             03 MNSELARO     PIC X(5).                                  00002870
             03 FILLER       PIC X(2).                                  00002880
             03 MNQPA   PIC X.                                          00002890
             03 MNQPC   PIC X.                                          00002900
             03 MNQPP   PIC X.                                          00002910
             03 MNQPH   PIC X.                                          00002920
             03 MNQPV   PIC X.                                          00002930
             03 MNQPO   PIC X(5).                                       00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MNQVA   PIC X.                                          00002960
             03 MNQVC   PIC X.                                          00002970
             03 MNQVP   PIC X.                                          00002980
             03 MNQVH   PIC X.                                          00002990
             03 MNQVV   PIC X.                                          00003000
             03 MNQVO   PIC X(6).                                       00003010
             03 FILLER       PIC X(2).                                  00003020
             03 MNQQPA  PIC X.                                          00003030
             03 MNQQPC  PIC X.                                          00003040
             03 MNQQPP  PIC X.                                          00003050
             03 MNQQPH  PIC X.                                          00003060
             03 MNQQPV  PIC X.                                          00003070
             03 MNQQPO  PIC X(5).                                       00003080
             03 FILLER       PIC X(2).                                  00003090
             03 MNQQVA  PIC X.                                          00003100
             03 MNQQVC  PIC X.                                          00003110
             03 MNQQVP  PIC X.                                          00003120
             03 MNQQVH  PIC X.                                          00003130
             03 MNQQVV  PIC X.                                          00003140
             03 MNQQVO  PIC X(6).                                       00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MNDDESTA     PIC X.                                     00003170
             03 MNDDESTC     PIC X.                                     00003180
             03 MNDDESTP     PIC X.                                     00003190
             03 MNDDESTH     PIC X.                                     00003200
             03 MNDDESTV     PIC X.                                     00003210
             03 MNDDESTO     PIC X(10).                                 00003220
             03 FILLER       PIC X(2).                                  00003230
             03 MNHCHRGA     PIC X.                                     00003240
             03 MNHCHRGC     PIC X.                                     00003250
             03 MNHCHRGP     PIC X.                                     00003260
             03 MNHCHRGH     PIC X.                                     00003270
             03 MNHCHRGV     PIC X.                                     00003280
             03 MNHCHRGO     PIC X(5).                                  00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBERRA  PIC X.                                          00003310
           02 MLIBERRC  PIC X.                                          00003320
           02 MLIBERRP  PIC X.                                          00003330
           02 MLIBERRH  PIC X.                                          00003340
           02 MLIBERRV  PIC X.                                          00003350
           02 MLIBERRO  PIC X(80).                                      00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MCODTRAA  PIC X.                                          00003380
           02 MCODTRAC  PIC X.                                          00003390
           02 MCODTRAP  PIC X.                                          00003400
           02 MCODTRAH  PIC X.                                          00003410
           02 MCODTRAV  PIC X.                                          00003420
           02 MCODTRAO  PIC X(4).                                       00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCICSA    PIC X.                                          00003450
           02 MCICSC    PIC X.                                          00003460
           02 MCICSP    PIC X.                                          00003470
           02 MCICSH    PIC X.                                          00003480
           02 MCICSV    PIC X.                                          00003490
           02 MCICSO    PIC X(5).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MNETNAMA  PIC X.                                          00003520
           02 MNETNAMC  PIC X.                                          00003530
           02 MNETNAMP  PIC X.                                          00003540
           02 MNETNAMH  PIC X.                                          00003550
           02 MNETNAMV  PIC X.                                          00003560
           02 MNETNAMO  PIC X(8).                                       00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MSCREENA  PIC X.                                          00003590
           02 MSCREENC  PIC X.                                          00003600
           02 MSCREENP  PIC X.                                          00003610
           02 MSCREENH  PIC X.                                          00003620
           02 MSCREENV  PIC X.                                          00003630
           02 MSCREENO  PIC X(4).                                       00003640
                                                                                
