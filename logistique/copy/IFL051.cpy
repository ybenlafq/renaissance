      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFL051 AU 14/02/2002  *        
      *                                                                *        
      *          CRITERES DE TRI  07,07,BI,A,                          *        
      *                           14,02,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,05,BI,A,                          *        
      *                           31,07,BI,A,                          *        
      *                           38,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFL051.                                                        
            05 NOMETAT-IFL051           PIC X(6) VALUE 'IFL051'.                
            05 RUPTURES-IFL051.                                                 
           10 IFL051-RAYON              PIC X(07).                      007  007
           10 IFL051-WBLBR              PIC X(02).                      014  002
           10 IFL051-WSEQFAM            PIC 9(05).                      016  005
           10 IFL051-CFAM               PIC X(05).                      021  005
           10 IFL051-CMARQ              PIC X(05).                      026  005
           10 IFL051-NCODIC             PIC X(07).                      031  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFL051-SEQUENCE           PIC S9(04) COMP.                038  002
      *--                                                                       
           10 IFL051-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFL051.                                                   
           10 IFL051-CAPPRO1            PIC X(05).                      040  005
           10 IFL051-CAPPRO2            PIC X(05).                      045  005
           10 IFL051-CAPPRO3            PIC X(05).                      050  005
           10 IFL051-CAPPRO4            PIC X(05).                      055  005
           10 IFL051-CAPPRO5            PIC X(05).                      060  005
           10 IFL051-CAPPRO6            PIC X(05).                      065  005
           10 IFL051-CAPPRO7            PIC X(05).                      070  005
           10 IFL051-ENTREPOT1          PIC X(06).                      075  006
           10 IFL051-ENTREPOT2          PIC X(06).                      081  006
           10 IFL051-ENTREPOT3          PIC X(06).                      087  006
           10 IFL051-ENTREPOT4          PIC X(06).                      093  006
           10 IFL051-ENTREPOT5          PIC X(06).                      099  006
           10 IFL051-ENTREPOT6          PIC X(06).                      105  006
           10 IFL051-ENTREPOT7          PIC X(06).                      111  006
           10 IFL051-LFAM               PIC X(20).                      117  020
           10 IFL051-LREFFOURN          PIC X(20).                      137  020
           10 IFL051-WELATLM            PIC X(03).                      157  003
            05 FILLER                      PIC X(353).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFL051-LONG           PIC S9(4)   COMP  VALUE +159.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFL051-LONG           PIC S9(4) COMP-5  VALUE +159.           
                                                                                
      *}                                                                        
