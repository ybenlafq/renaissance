      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : APPLICATION LOGISTIQUE PARTENAIRE                *        
      *  PROGRAMMES : MLV099                                           *        
      *  TITRE      : COMMAREA DU MODULE D'ALIMENTATION TABLE RTLV99   *        
      *                   TABLE COMPTE-RENDU ACTIVITE DES CHAINES      *        
      *  LONGUEUR   :      C                                           *        
      *                                                                *        
      ******************************************************************        
       01  COMM-LV99-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 12        
           05 COMM-LV99-DONNEES-ENTREE.                                         
              10 COMM-LV99-CPROGRAMME       PIC X(06).                          
              10 COMM-LV99-NSOCIETE         PIC X(03).                          
              10 COMM-LV99-NLIEU            PIC X(03).                          
              10 COMM-LV99-DTRAIT           PIC X(08).                          
              10 COMM-LV99-NSEQ             PIC X(03).                          
              10 COMM-LV99-LTRAIT           PIC X(25).                          
              10 COMM-LV99-QTE1             PIC 9(7) COMP-3.                    
              10 COMM-LV99-QTE2             PIC 9(7) COMP-3.                    
      *--- CODE RETOUR + MESSAGE ------------------------------------ 59        
           05 COMM-LV99-MESSAGE.                                                
              10 COMM-LV99-CODRET           PIC X(01).                          
                 88 COMM-LV99-CODRET-OK                  VALUE ' '.             
                 88 COMM-LV99-CODRET-ERREUR              VALUE '1'.             
              10 COMM-LV99-LIBERR           PIC X(58).                          
                                                                                
