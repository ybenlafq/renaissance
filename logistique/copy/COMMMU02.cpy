      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************    00000010
      * COMMAREA SPECIFIQUE PRG TMU02 (TMU00 -> MENU)    TR: MU00  *    00000020
      *               GESTION DES MUTATIONS                             00000030
      *                                                                 00000040
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3610  00000050
      *                                                                 00000060
      *        TRANSACTION MU02 : MODIF DES ENTETES DE MUTATION         00000070
      *                                                                 00000080
          04 COMM-MU02-APPLI REDEFINES COMM-MU01-LIBRE.                 00000090
      *------------------------------ ZONE DONNEES TMU02                00000091
             05 COMM-MU02-DONNEES-TMU02.                                00000092
      *------------------------------                                   00000093
                 06  COMM-MU02-WMS             PIC X(01).               00000094
                 06  COMM-MU02-PAGESUIV        PIC X(01).               00000094
                 06  COMM-MU02-NPAGE           PIC 9(02).               00000095
                 06  COMM-MU02-DDEBUT-ET       PIC X(08).               00000096
                 06  COMM-MU02-TABLE-CLE.                               00000120
                     10  COMM-MU02-LAST-CLE  OCCURS 125.                00000121
                         15  COMM-MU02-LASTMUT     PIC X(07).           00000130
                         15  COMM-MU02-LASTDATE    PIC X(08).           00000131
                 06  COMM-MU02-TABLE-PAGE.                              00000132
                   08  COMM-MU02-LIGNE OCCURS 13.                       00000133
                     10  COMM-MU02-NMUTATION       PIC X(07).           00000140
                     10  COMM-MU02-DDEBSAIS        PIC X(04).           00000150
                     10  COMM-MU02-DFINSAIS        PIC X(04).           00000160
                     10  COMM-MU02-DDESTOCK        PIC X(04).           00000170
                     10  COMM-MU02-LIBJD           PIC X(02).           00000171
                     10  COMM-MU02-DCHARGEMENT     PIC X(04).           00000180
                     10  COMM-MU02-DHEURCHAR       PIC X(05).           00000200
                     10  COMM-MU02-DMUTATION       PIC X(04).           00000180
                     10  COMM-MU02-PROPERMIS       PIC X(01).           00000190
                     10  COMM-MU02-DHEURMUT        PIC 9(02).           00000200
                     10  COMM-MU02-DMINUMUT        PIC 9(02).           00000210
                     10  COMM-MU02-QNBPIECES       PIC 9(05).           00000221
                     10  COMM-MU02-QNBM3QUOTA      PIC 9(04)V9.         00000230
                     10  COMM-MU02-QNBPQUOTA       PIC 9(05).           00000240
                     10  COMM-MU02-LHEURLIMIT      PIC X(05).           00000250
                     10  COMM-MU02-GB05-PLAGE      PIC X(05).           00000250
                     10  COMM-MU02-GB40-PLAGE      PIC X(05).           00000250
                 06  COMM-MU02-ATTR   PIC X(01)  OCCURS 273.            00000260
                 06  COMM-MU02-EXPED.                                   00000260
                   08  COMM-MU02-LIGNE-EXP OCCURS 13.                   00000133
                     10  COMM-MU02-WMULTI          PIC X(01).           00000140
                 06  COMM-MU02-LIBRE               PIC X(540).          00000260
      *                                                                 00000291
                                                                                
