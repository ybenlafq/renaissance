      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVIN0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIN0100                         
      *   TABLE DES RESULTATS D'INVENTAIRE                                      
      *---------------------------------------------------------                
      *                                                                         
       01  RVIN0100.                                                            
           02  IN01-NSOCIETE                                                    
               PIC   X(03).                                                     
           02  IN01-NLIEU                                                       
               PIC   X(03).                                                     
           02  IN01-SSLIEU                                                      
               PIC   X(03).                                                     
           02  IN01-CSTATUT                                                     
               PIC   X(01).                                                     
           02  IN01-NCODIC                                                      
               PIC   X(07).                                                     
           02  IN01-QSTOCK                                                      
               PIC   S9(05) COMP-3.                                             
           02  IN01-LTRT                                                        
               PIC   X(05).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVIN0100                                    
      *---------------------------------------------------------                
      *                                                                         
       01  RVIN0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-NSOCIETE-F                                                  
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  IN01-NSOCIETE-F                                                  
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-NLIEU-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  IN01-NLIEU-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-SSLIEU-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  IN01-SSLIEU-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-CSTATUT-F                                                   
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  IN01-CSTATUT-F                                                   
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-NCODIC-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  IN01-NCODIC-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-QSTOCK-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  IN01-QSTOCK-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IN01-LTRT-F                                                      
      *        PIC   S9(4) COMP.                                                
      *                                                                         
      *--                                                                       
           02  IN01-LTRT-F                                                      
               PIC   S9(4) COMP-5.                                              
                                                                                
      *}                                                                        
