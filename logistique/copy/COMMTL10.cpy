      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COMMAREA PROPRE AU PROGRAMME TTL10                  3500     00000100
          02 COMM-TL10-APPLI REDEFINES COMM-TL00-APPLI.                 00000110
      *                                                                 00000111
      *                     ZONES DE PAGINATION                         00000120
      *                                        NUMERO DE PAGE           00000130
             03 COMM-TL10-NPAGE           PIC 9(3).                     00000200
      *                                        DERNIER NUMERO DE LIGNE  00000201
      *                                        UTILISE                  00000202
             03 COMM-TL10-NLIGNE          PIC 9(3).                     00000203
      *                                        DERNIERE CLE LUE         00000210
             03 COMM-TL10-WCLELU          PIC X(5).                     00000300
      *                                        TABLE DE PAGINATION      00000310
             03 COMM-TL10-PAGINATION      OCCURS 10.                    00000400
                04 COMM-TL10-WCLEPAG      PIC X(5).                     00000500
      *                                                                 00000501
      *                     PAGE EN COURS (MAP)                         00000510
             03 COMM-TL10-PAGE-EN-COURS OCCURS 12.                      00000600
      *                                        CODE PROFIL              00000610
                04 COMM-TL10-CPROFIL      PIC X(5).                     00000700
      *                                        LIBELLE PROFIL           00000701
                04 COMM-TL10-LPROFIL      PIC X(20).                    00000710
      *                                        TOP CREATION TOURNEE     00000711
                04 COMM-TL10-WTOURNEE     PIC X(1).                     00000720
      *                                        HEURE CREATION TOURNEE   00000721
                04 COMM-TL10-DHTOURNEE    PIC X(4).                     00000730
      *                                        TOP DESTOCKAGE EFFECTUE  00000731
                04 COMM-TL10-WDESTOCK     PIC X(1).                     00000740
      *                                        HEURE DESTOCK. EFFECTUE  00000741
                04 COMM-TL10-DHDESTOCK    PIC X(4).                     00000750
      *                                        NUMERO DE RAFALE         00000751
                04 COMM-TL10-NRAFALE      PIC X(3).                     00000760
      *                                        SELECTION PROFIL         00000761
                04 COMM-TL10-WSEL         PIC X(1).                     00000762
      *                                        PROFIL TRAITE PAR TTL11  00000763
                04 COMM-TL10-WTRAIT       PIC X(1).                     00000764
      *                                        EXISTANCE DE DIFFERENCES 00000765
      *                                        ENTRE VENTE ET TOURNEE   00000766
                04 COMM-TL10-EXIST-DIFF   PIC X(1).                     00000767
                88 COMM-TL10-DIFF                  VALUE '1'.           00000768
      *                                        NUMERO ITEM CORRESPONDANT00000769
      *                                        AU PREMIER ITEM POUR LE  00000770
      *                                        PROFIL, DE LA TS TL11    00000771
                04 COMM-TL10-NITEM        PIC 9(5) COMP-3.              00000772
      *                                        DERNIERE ITEM ECRIT      00000773
      *                                        DANS LA TS TL11          00000774
             03 COMM-TL10-NITEM-TL11      PIC 9(5) COMP-3.              00000775
      *                                        FILLER                   00001200
             03 COMM-TL10-FILLER          PIC X(2894).                  00001300
                                                                                
