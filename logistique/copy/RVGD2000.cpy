      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGD2000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGD2000                         
      **********************************************************                
       01  RVGD2000.                                                            
           02  GD20-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  GD20-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GD20-DATEDEST                                                    
               PIC X(0008).                                                     
           02  GD20-NSOC                                                        
               PIC X(0003).                                                     
           02  GD20-NLIEU                                                       
               PIC X(0003).                                                     
           02  GD20-CMODLIVR                                                    
               PIC X(0003).                                                     
           02  GD20-CEQUIPLIV                                                   
               PIC X(0005).                                                     
           02  GD20-LHEURELIM                                                   
               PIC X(0010).                                                     
           02  GD20-QNBLTOT                                                     
               PIC S9(5) COMP-3.                                                
           02  GD20-QNBPTOT                                                     
               PIC S9(7) COMP-3.                                                
           02  GD20-QNBLSOL                                                     
               PIC S9(5) COMP-3.                                                
           02  GD20-QNBPSOL                                                     
               PIC S9(7) COMP-3.                                                
           02  GD20-QNBLRACK                                                    
               PIC S9(5) COMP-3.                                                
           02  GD20-QNBPRACK                                                    
               PIC S9(7) COMP-3.                                                
           02  GD20-QNBLVRAC                                                    
               PIC S9(5) COMP-3.                                                
           02  GD20-QNBPVRAC                                                    
               PIC S9(7) COMP-3.                                                
           02  GD20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGD2000                                  
      **********************************************************                
       01  RVGD2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-DATEDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-DATEDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-CMODLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-CMODLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-CEQUIPLIV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-CEQUIPLIV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-LHEURELIM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-LHEURELIM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBLTOT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBLTOT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBPTOT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBPTOT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBLSOL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBLSOL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBPSOL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBPSOL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBLRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBLRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBLVRAC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBLVRAC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-QNBPVRAC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GD20-QNBPVRAC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GD20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GD20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
