      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TFL05 TFL06                                    *  00020033
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
      *01  TS-LONG              PIC S9(5) COMP-3 VALUE 1001.            00080026
       01  TS-LONG              PIC S9(5) COMP-3 VALUE 1027.                    
       01  TS-RECORD.                                                   00090022
         05  TS-LIGNE         OCCURS 13.                                00100022
           10 TS-NSOCDEPOT    PIC X(3).                                 00120324
           10 TS-NDEPOT       PIC X(3).                                 00120424
           10 TS-DEPOTPAL     PIC X(1).                                         
           10 TS-LLIEU        PIC X(20).                                00120525
           10 TS-CFAM         PIC X(5).                                 00120624
           10 TS-QDELAIAPPRO  PIC X(3).                                 00120731
           10 TS-QCOLICDE     PIC X(3).                                 00120831
           10 TS-QCOLIRECEPT  PIC X(5).                                 00121031
           10 TS-QCOLIDSTOCK  PIC X(5).                                 00121131
           10 TS-CMODSTOCK    PIC X(5).                                 00121224
           10 TS-QNBRANMAIL   PIC X(3).                                 00121332
           10 TS-QNBNIVGERB   PIC X(3).                                 00121432
           10 TS-CZONEACCES   PIC X.                                    00121524
           10 TS-CCONTENEUR   PIC X.                                    00121624
           10 TS-CSPECIFSTK   PIC X.                                    00121724
           10 TS-CCOTEHOLD    PIC X.                                    00121824
           10 TS-QNBPVSOL     PIC X(3).                                 00121932
           10 TS-QNBPRACK     PIC X(5).                                 00122032
           10 TS-CTPSUP       PIC X.                                    00122124
           10 TS-CQUOTA       PIC X(5).                                 00122224
           10 TS-QUOTAPAL     PIC X(1).                                         
           10 TS-MAJ-FL14     PIC X.                                    00122324
      *    10 TS-FILLER       PIC X(02).                                00126019
                                                                                
