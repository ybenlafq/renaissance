      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TRM64                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
AA0213*01  TS-LONG              PIC S9(3) COMP-3 VALUE 92.              00060002
AA0213*01  TS-LONG              PIC S9(3) COMP-3 VALUE 97.              00061003
AA0814 01  TS-LONG              PIC S9(3) COMP-3 VALUE 103.             00062004
       01  TS-DONNEES.                                                  00070000
              05 TS-NSOC       PIC X(03).                               00080000
              05 TS-NMAG       PIC X(03).                               00090000
              05 TS-MLMAG      PIC X(20).                               00100000
              05 TS-MLSTAT     PIC X(03).                               00110000
AA0213*       05 TS-CEXPOMAG   PIC X(05).                               00120001
AA0213        05 TS-CEXPOMAG   PIC X(10).                               00121001
              05 TS-WASSORT    PIC X(01).                               00130000
              05 TS-WASSOSTD   PIC X(01).                               00140000
              05 TS-QEXPO      PIC X(03).                               00150000
              05 TS-QLS        PIC X(03).                               00160000
AA0814        05 TS-QSTKMAG    PIC X(03).                               00161003
AA0814        05 TS-QMUT-ENCOURS PIC X(03).                             00162004
              05 TS-QV8SP      PIC X(05).                               00170000
              05 TS-QV8SR      PIC X(05).                               00180000
              05 TS-QSO        PIC X(03).                               00190000
              05 TS-QSM        PIC X(03).                               00200000
              05 TS-QSOP       PIC X(03).                               00210000
              05 TS-QSMP       PIC X(03).                               00220000
              05 TS-MODIF      PIC X(01).                               00230000
              05 TS-QSO-SAVE   PIC X(03).                               00240000
              05 TS-QSM-SAVE   PIC X(03).                               00250000
              05 TS-QSOTROUVE  PIC X.                                   00260000
              05 TS-QSATROUVE  PIC X.                                   00270000
              05 TS-QSOMIN     PIC X(03).                               00280000
              05 TS-QSMMIN     PIC X(03).                               00290000
              05 TS-QSOMAX     PIC X(03).                               00300000
              05 TS-QSMMAX     PIC X(03).                               00310000
              05 TS-W80        PIC X.                                   00320000
              05 TS-FLAG       PIC X.                                   00330000
              05 TS-CGROUP     PIC X(5).                                00340001
                                                                                
