      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IVA580 AU 22/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,01,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IVA580.                                                        
            05 NOMETAT-IVA580           PIC X(6) VALUE 'IVA580'.                
            05 RUPTURES-IVA580.                                                 
           10 IVA580-NSOC               PIC X(03).                      007  003
           10 IVA580-CSEQ               PIC X(02).                      010  002
           10 IVA580-CTYPMVT            PIC X(01).                      012  001
           10 IVA580-NSOCMVT            PIC X(03).                      013  003
           10 IVA580-NLIEUMVT           PIC X(03).                      016  003
           10 IVA580-NLIEU              PIC X(03).                      019  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IVA580-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 IVA580-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IVA580.                                                   
           10 IVA580-CRAYON             PIC X(05).                      024  005
           10 IVA580-DEVISE             PIC X(03).                      029  003
           10 IVA580-LIB1               PIC X(01).                      032  001
           10 IVA580-LIB11              PIC X(01).                      033  001
           10 IVA580-LIB2               PIC X(01).                      034  001
           10 IVA580-LIB21              PIC X(01).                      035  001
           10 IVA580-LIB3               PIC X(01).                      036  001
           10 IVA580-LIB31              PIC X(01).                      037  001
           10 IVA580-LIB4               PIC X(01).                      038  001
           10 IVA580-LIB41              PIC X(01).                      039  001
           10 IVA580-LIB5               PIC X(01).                      040  001
           10 IVA580-LIB51              PIC X(01).                      041  001
           10 IVA580-LIB6               PIC X(01).                      042  001
           10 IVA580-LIB61              PIC X(01).                      043  001
           10 IVA580-LIB7               PIC X(01).                      044  001
           10 IVA580-LIB71              PIC X(01).                      045  001
           10 IVA580-LIB8               PIC X(01).                      046  001
           10 IVA580-LIB81              PIC X(01).                      047  001
           10 IVA580-LIB9               PIC X(01).                      048  001
           10 IVA580-LIB91              PIC X(01).                      049  001
           10 IVA580-LLIEU              PIC X(20).                      050  020
           10 IVA580-LLIEU-LIEU         PIC X(20).                      070  020
           10 IVA580-LTYPMVT            PIC X(26).                      090  026
           10 IVA580-NLIEUMVT-LIEU      PIC X(03).                      116  003
           10 IVA580-NSOCMVT-LIEU       PIC X(03).                      119  003
           10 IVA580-PRECYCLENTREE      PIC S9(09)V9(6) COMP-3.         122  008
           10 IVA580-PRECYCLSORTIE      PIC S9(09)V9(6) COMP-3.         130  008
           10 IVA580-PSTOCKENTREE       PIC S9(09)V9(6) COMP-3.         138  008
           10 IVA580-PSTOCKFINAL        PIC S9(09)V9(6) COMP-3.         146  008
           10 IVA580-PSTOCKINIT         PIC S9(09)V9(6) COMP-3.         154  008
           10 IVA580-PSTOCKSORTIE       PIC S9(09)V9(6) COMP-3.         162  008
           10 IVA580-QSTOCKENTREE       PIC S9(11)      COMP-3.         170  006
           10 IVA580-QSTOCKFINAL        PIC S9(11)      COMP-3.         176  006
           10 IVA580-QSTOCKINIT         PIC S9(11)      COMP-3.         182  006
           10 IVA580-QSTOCKSORTIE       PIC S9(11)      COMP-3.         188  006
           10 IVA580-TXEURO             PIC S9(01)V9(5) COMP-3.         194  004
            05 FILLER                      PIC X(315).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IVA580-LONG           PIC S9(4)   COMP  VALUE +197.           
      *                                                                         
      *--                                                                       
        01  DSECT-IVA580-LONG           PIC S9(4) COMP-5  VALUE +197.           
                                                                                
      *}                                                                        
