      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGS800 AU 28/01/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,BI,A,                          *        
      *                           16,05,BI,A,                          *        
      *                           21,03,PD,A,                          *        
      *                           24,05,BI,A,                          *        
      *                           29,20,BI,A,                          *        
      *                           49,07,BI,A,                          *        
      *                           56,03,BI,A,                          *        
      *                           59,08,BI,A,                          *        
      *                           67,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGS800.                                                        
            05 NOMETAT-IGS800           PIC X(6) VALUE 'IGS800'.                
            05 RUPTURES-IGS800.                                                 
           10 IGS800-NSOCGEN            PIC X(03).                      007  003
           10 IGS800-NDEPGEN            PIC X(03).                      010  003
           10 IGS800-NSOCIETE           PIC X(03).                      013  003
           10 IGS800-TEBRB              PIC X(05).                      016  005
           10 IGS800-WSEQFAM            PIC S9(05)      COMP-3.         021  003
           10 IGS800-CMARQ              PIC X(05).                      024  005
           10 IGS800-LREFFOURN          PIC X(20).                      029  020
           10 IGS800-NCODIC             PIC X(07).                      049  007
           10 IGS800-NDEPOT             PIC X(03).                      056  003
           10 IGS800-DLIVRAISON         PIC X(08).                      059  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGS800-SEQUENCE           PIC S9(04) COMP.                067  002
      *--                                                                       
           10 IGS800-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGS800.                                                   
           10 IGS800-CAPPRO             PIC X(05).                      069  005
           10 IGS800-CFAM               PIC X(05).                      074  005
           10 IGS800-WGENGRO            PIC X(03).                      079  003
           10 IGS800-QCDE               PIC S9(07)      COMP-3.         082  004
           10 IGS800-QCDEPREL           PIC S9(07)      COMP-3.         086  004
           10 IGS800-QDISPO             PIC S9(07)      COMP-3.         090  004
           10 IGS800-QNBJOURS           PIC S9(05)V9(2) COMP-3.         094  004
           10 IGS800-QSTOCK             PIC S9(07)      COMP-3.         098  004
           10 IGS800-QSTOCKDEP          PIC S9(07)      COMP-3.         102  004
           10 IGS800-QSTOCKGEN          PIC S9(07)      COMP-3.         106  004
            05 FILLER                      PIC X(403).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGS800-LONG           PIC S9(4)   COMP  VALUE +109.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGS800-LONG           PIC S9(4) COMP-5  VALUE +109.           
                                                                                
      *}                                                                        
