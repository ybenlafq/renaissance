      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      *       TS SPECIFIQUE  OPTIMISATION TRANSPORT TMU71            *          
      ****************************************************************          
       01  TS-MU71-LONG             PIC S9(4) COMP-3 VALUE +1184.               
       01  TS-MU71-DATA.                                                        
      *************************************** LONGUEUR LIGNE = 74               
           05 TS-MU71-LIGNE   OCCURS  16.                                       
              10 TS-MU71-DDELIV     PIC X(8).                                   
              10 TS-MU71-NSOC       PIC X(3).                                   
              10 TS-MU71-NMAG       PIC X(3).                                   
              10 TS-MU71-NVENTE     PIC X(7).                                   
              10 TS-MU71-NOMCLI     PIC X(20).                                  
              10 TS-MU71-VOLTOTVT   PIC S9(3)V99    COMP-3.                     
AD01  *       10 TS-MU71-QTETOT     PIC 9(3).                                   
AD01          10 TS-MU71-QTETOT     PIC 9(5).                                   
              10 TS-MU71-QPIECES    PIC 9(3).                                   
              10 TS-MU71-NMUT       PIC X(7).                                   
              10 TS-MU71-VOLMUT     PIC S9(3)V99    COMP-3.                     
              10 TS-MU71-SELART     PIC X(5).                                   
              10 TS-MU71-SOCPTF     PIC X(3).                                   
              10 TS-MU71-PTF        PIC X(3).                                   
              10 TS-MU71-ACTION     PIC X(1).                                   
                                                                                
