      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVRM1900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1900                         
      **********************************************************                
       01  RVRM1900.                                                            
           02  RM19-CFAM                                                        
               PIC X(0005).                                                     
           02  RM19-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  RM19-NLIEU                                                       
               PIC X(0003).                                                     
           02  RM19-GAMMEMANU                                                   
               PIC X(0010).                                                     
           02  RM19-DSYST                                                       
               PIC S9(0013) COMP-3.                                             
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVRM1900                                  
      **********************************************************                
       01  RVRM1900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM19-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM19-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM19-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM19-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM19-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM19-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM19-GAMMEMANU-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM19-GAMMEMANU-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM19-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  RM19-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
