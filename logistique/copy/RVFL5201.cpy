      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVFL5201                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVFL5201                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL5201.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL5201.                                                            
      *}                                                                        
           02  FL52-NCODIC                                                      
               PIC X(0007).                                                     
           02  FL52-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  FL52-NDEPOT                                                      
               PIC X(0003).                                                     
           02  FL52-WMODSTOCK1                                                  
               PIC X(0001).                                                     
           02  FL52-WMODSTOCK2                                                  
               PIC X(0001).                                                     
           02  FL52-WMODSTOCK3                                                  
               PIC X(0001).                                                     
           02  FL52-CMODSTOCK1                                                  
               PIC X(0005).                                                     
           02  FL52-CMODSTOCK2                                                  
               PIC X(0005).                                                     
           02  FL52-CMODSTOCK3                                                  
               PIC X(0005).                                                     
           02  FL52-QCOLIDSTOCK                                                 
               PIC S9(5) COMP-3.                                                
           02  FL52-CFETEMPL                                                    
               PIC X(0001).                                                     
           02  FL52-CFAM                                                        
               PIC X(0005).                                                     
           02  FL52-CMARQ                                                       
               PIC X(0005).                                                     
           02  FL52-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  FL52-QHAUTEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  FL52-QLARGEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  FL52-QPROFONDEUR                                                 
               PIC S9(3) COMP-3.                                                
           02  FL52-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  FL52-NLIEU                                                       
               PIC X(0003).                                                     
           02  FL52-QV8SR                                                       
               PIC S9(7) COMP-3.                                                
           02  FL52-QSTOCKOBJR                                                  
               PIC S9(7) COMP-3.                                                
           02  FL52-QV8SD                                                       
               PIC S9(7) COMP-3.                                                
           02  FL52-QSTOCKOBJD                                                  
               PIC S9(7) COMP-3.                                                
           02  FL52-QV8SC                                                       
               PIC S9(7) COMP-3.                                                
           02  FL52-QSTOCKOBJC                                                  
               PIC S9(7) COMP-3.                                                
           02  FL52-QV8SP                                                       
               PIC S9(7) COMP-3.                                                
           02  FL52-QSTOCKOBJP                                                  
               PIC S9(7) COMP-3.                                                
           02  FL52-QSO                                                         
               PIC S9(7) COMP-3.                                                
           02  FL52-QSA                                                         
               PIC S9(7) COMP-3.                                                
           02  FL52-CSELART                                                     
               PIC X(0005).                                                     
           02  FL52-NMUTATION                                                   
               PIC X(0007).                                                     
           02  FL52-QFREQUENCE                                                  
               PIC S9(1)V9(0002) COMP-3.                                        
           02  FL52-DMAJ                                                        
               PIC X(0008).                                                     
           02  FL52-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  FL52-QSOS                                                        
               PIC S9(7) COMP-3.                                                
           02  FL52-QSAS                                                        
               PIC S9(7) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVFL5201                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFL5201-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFL5201-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-WMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-WMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-WMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-WMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-WMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-WMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QHAUTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QHAUTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QLARGEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QLARGEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QPROFONDEUR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QPROFONDEUR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QV8SR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QV8SR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSTOCKOBJR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSTOCKOBJR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QV8SD-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QV8SD-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSTOCKOBJD-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSTOCKOBJD-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QV8SC-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QV8SC-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSTOCKOBJC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSTOCKOBJC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QV8SP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QV8SP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSTOCKOBJP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSTOCKOBJP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSO-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSO-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QFREQUENCE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QFREQUENCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSOS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSOS-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  FL52-QSAS-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  FL52-QSAS-F                                                      
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
