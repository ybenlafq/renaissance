      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGD51   EGD51                                              00000020
      ***************************************************************** 00000030
       01   EGD51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNCODICI  PIC X(7).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLREFI    PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCZONEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCZONEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCZONEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCZONEI   PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNIVEAUL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCNIVEAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCNIVEAUF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCNIVEAUI      PIC X(2).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPOSITIONL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNPOSITIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNPOSITIONF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNPOSITIONI    PIC X(4).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLFAMI    PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTDEML   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MQTDEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQTDEMF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MQTDEMI   PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTRESL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MQTRESL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQTRESF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MQTRESI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTPRELL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MQTPRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQTPRELF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MQTPRELI  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNRAFALEL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNRAFALEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNRAFALEF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNRAFALEI      PIC X(3).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRAFALEL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MDRAFALEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDRAFALEF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDRAFALEI      PIC X(8).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLDEML   COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MQLDEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQLDEMF   PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MQLDEMI   PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLRESL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MQLRESL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQLRESF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MQLRESI   PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLPRELL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MQLPRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQLPRELF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MQLPRELI  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000860
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNSOCIETEI     PIC X(3).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MNDEPOTI  PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMDEML   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MQMDEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQMDEMF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MQMDEMI   PIC X(5).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMRESL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MQMRESL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQMRESF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MQMRESI   PIC X(5).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQMPRELL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MQMPRELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQMPRELF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MQMPRELI  PIC X(5).                                       00001050
           02 M314I OCCURS   11 TIMES .                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSOCL   COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MSOCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSOCF   PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MSOCI   PIC X(3).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIEUL  COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLIEUF  PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MLIEUI  PIC X(3).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBL   COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MLIBL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIBF   PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MLIBI   PIC X(20).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMUTL   COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MMUTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMUTF   PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MMUTI   PIC X(7).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDEML  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MQDEML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQDEMF  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MQDEMI  PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRESL  COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MQRESL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRESF  PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MQRESI  PIC X(5).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQPRELL      COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MQPRELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQPRELF      PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MQPRELI      PIC X(5).                                  00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MZONCMDI  PIC X(15).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(58).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * SDF: EGD51   EGD51                                              00001600
      ***************************************************************** 00001610
       01   EGD51O REDEFINES EGD51I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MPAGEA    PIC X.                                          00001790
           02 MPAGEC    PIC X.                                          00001800
           02 MPAGEP    PIC X.                                          00001810
           02 MPAGEH    PIC X.                                          00001820
           02 MPAGEV    PIC X.                                          00001830
           02 MPAGEO    PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNCODICA  PIC X.                                          00001860
           02 MNCODICC  PIC X.                                          00001870
           02 MNCODICP  PIC X.                                          00001880
           02 MNCODICH  PIC X.                                          00001890
           02 MNCODICV  PIC X.                                          00001900
           02 MNCODICO  PIC X(7).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLREFA    PIC X.                                          00001930
           02 MLREFC    PIC X.                                          00001940
           02 MLREFP    PIC X.                                          00001950
           02 MLREFH    PIC X.                                          00001960
           02 MLREFV    PIC X.                                          00001970
           02 MLREFO    PIC X(20).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MCZONEA   PIC X.                                          00002000
           02 MCZONEC   PIC X.                                          00002010
           02 MCZONEP   PIC X.                                          00002020
           02 MCZONEH   PIC X.                                          00002030
           02 MCZONEV   PIC X.                                          00002040
           02 MCZONEO   PIC X(2).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCNIVEAUA      PIC X.                                     00002070
           02 MCNIVEAUC PIC X.                                          00002080
           02 MCNIVEAUP PIC X.                                          00002090
           02 MCNIVEAUH PIC X.                                          00002100
           02 MCNIVEAUV PIC X.                                          00002110
           02 MCNIVEAUO      PIC X(2).                                  00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNPOSITIONA    PIC X.                                     00002140
           02 MNPOSITIONC    PIC X.                                     00002150
           02 MNPOSITIONP    PIC X.                                     00002160
           02 MNPOSITIONH    PIC X.                                     00002170
           02 MNPOSITIONV    PIC X.                                     00002180
           02 MNPOSITIONO    PIC X(4).                                  00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCFAMA    PIC X.                                          00002210
           02 MCFAMC    PIC X.                                          00002220
           02 MCFAMP    PIC X.                                          00002230
           02 MCFAMH    PIC X.                                          00002240
           02 MCFAMV    PIC X.                                          00002250
           02 MCFAMO    PIC X(5).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MLFAMA    PIC X.                                          00002280
           02 MLFAMC    PIC X.                                          00002290
           02 MLFAMP    PIC X.                                          00002300
           02 MLFAMH    PIC X.                                          00002310
           02 MLFAMV    PIC X.                                          00002320
           02 MLFAMO    PIC X(20).                                      00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCMARQA   PIC X.                                          00002350
           02 MCMARQC   PIC X.                                          00002360
           02 MCMARQP   PIC X.                                          00002370
           02 MCMARQH   PIC X.                                          00002380
           02 MCMARQV   PIC X.                                          00002390
           02 MCMARQO   PIC X(5).                                       00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MLMARQA   PIC X.                                          00002420
           02 MLMARQC   PIC X.                                          00002430
           02 MLMARQP   PIC X.                                          00002440
           02 MLMARQH   PIC X.                                          00002450
           02 MLMARQV   PIC X.                                          00002460
           02 MLMARQO   PIC X(20).                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MQTDEMA   PIC X.                                          00002490
           02 MQTDEMC   PIC X.                                          00002500
           02 MQTDEMP   PIC X.                                          00002510
           02 MQTDEMH   PIC X.                                          00002520
           02 MQTDEMV   PIC X.                                          00002530
           02 MQTDEMO   PIC X(5).                                       00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MQTRESA   PIC X.                                          00002560
           02 MQTRESC   PIC X.                                          00002570
           02 MQTRESP   PIC X.                                          00002580
           02 MQTRESH   PIC X.                                          00002590
           02 MQTRESV   PIC X.                                          00002600
           02 MQTRESO   PIC X(5).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MQTPRELA  PIC X.                                          00002630
           02 MQTPRELC  PIC X.                                          00002640
           02 MQTPRELP  PIC X.                                          00002650
           02 MQTPRELH  PIC X.                                          00002660
           02 MQTPRELV  PIC X.                                          00002670
           02 MQTPRELO  PIC X(5).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MNRAFALEA      PIC X.                                     00002700
           02 MNRAFALEC PIC X.                                          00002710
           02 MNRAFALEP PIC X.                                          00002720
           02 MNRAFALEH PIC X.                                          00002730
           02 MNRAFALEV PIC X.                                          00002740
           02 MNRAFALEO      PIC X(3).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MDRAFALEA      PIC X.                                     00002770
           02 MDRAFALEC PIC X.                                          00002780
           02 MDRAFALEP PIC X.                                          00002790
           02 MDRAFALEH PIC X.                                          00002800
           02 MDRAFALEV PIC X.                                          00002810
           02 MDRAFALEO      PIC X(8).                                  00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MQLDEMA   PIC X.                                          00002840
           02 MQLDEMC   PIC X.                                          00002850
           02 MQLDEMP   PIC X.                                          00002860
           02 MQLDEMH   PIC X.                                          00002870
           02 MQLDEMV   PIC X.                                          00002880
           02 MQLDEMO   PIC X(5).                                       00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MQLRESA   PIC X.                                          00002910
           02 MQLRESC   PIC X.                                          00002920
           02 MQLRESP   PIC X.                                          00002930
           02 MQLRESH   PIC X.                                          00002940
           02 MQLRESV   PIC X.                                          00002950
           02 MQLRESO   PIC X(5).                                       00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MQLPRELA  PIC X.                                          00002980
           02 MQLPRELC  PIC X.                                          00002990
           02 MQLPRELP  PIC X.                                          00003000
           02 MQLPRELH  PIC X.                                          00003010
           02 MQLPRELV  PIC X.                                          00003020
           02 MQLPRELO  PIC X(5).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MNSOCIETEA     PIC X.                                     00003050
           02 MNSOCIETEC     PIC X.                                     00003060
           02 MNSOCIETEP     PIC X.                                     00003070
           02 MNSOCIETEH     PIC X.                                     00003080
           02 MNSOCIETEV     PIC X.                                     00003090
           02 MNSOCIETEO     PIC X(3).                                  00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MNDEPOTA  PIC X.                                          00003120
           02 MNDEPOTC  PIC X.                                          00003130
           02 MNDEPOTP  PIC X.                                          00003140
           02 MNDEPOTH  PIC X.                                          00003150
           02 MNDEPOTV  PIC X.                                          00003160
           02 MNDEPOTO  PIC X(3).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MQMDEMA   PIC X.                                          00003190
           02 MQMDEMC   PIC X.                                          00003200
           02 MQMDEMP   PIC X.                                          00003210
           02 MQMDEMH   PIC X.                                          00003220
           02 MQMDEMV   PIC X.                                          00003230
           02 MQMDEMO   PIC X(5).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MQMRESA   PIC X.                                          00003260
           02 MQMRESC   PIC X.                                          00003270
           02 MQMRESP   PIC X.                                          00003280
           02 MQMRESH   PIC X.                                          00003290
           02 MQMRESV   PIC X.                                          00003300
           02 MQMRESO   PIC X(5).                                       00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MQMPRELA  PIC X.                                          00003330
           02 MQMPRELC  PIC X.                                          00003340
           02 MQMPRELP  PIC X.                                          00003350
           02 MQMPRELH  PIC X.                                          00003360
           02 MQMPRELV  PIC X.                                          00003370
           02 MQMPRELO  PIC X(5).                                       00003380
           02 M314O OCCURS   11 TIMES .                                 00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MSOCA   PIC X.                                          00003410
             03 MSOCC   PIC X.                                          00003420
             03 MSOCP   PIC X.                                          00003430
             03 MSOCH   PIC X.                                          00003440
             03 MSOCV   PIC X.                                          00003450
             03 MSOCO   PIC X(3).                                       00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MLIEUA  PIC X.                                          00003480
             03 MLIEUC  PIC X.                                          00003490
             03 MLIEUP  PIC X.                                          00003500
             03 MLIEUH  PIC X.                                          00003510
             03 MLIEUV  PIC X.                                          00003520
             03 MLIEUO  PIC X(3).                                       00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MLIBA   PIC X.                                          00003550
             03 MLIBC   PIC X.                                          00003560
             03 MLIBP   PIC X.                                          00003570
             03 MLIBH   PIC X.                                          00003580
             03 MLIBV   PIC X.                                          00003590
             03 MLIBO   PIC X(20).                                      00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MMUTA   PIC X.                                          00003620
             03 MMUTC   PIC X.                                          00003630
             03 MMUTP   PIC X.                                          00003640
             03 MMUTH   PIC X.                                          00003650
             03 MMUTV   PIC X.                                          00003660
             03 MMUTO   PIC X(7).                                       00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MQDEMA  PIC X.                                          00003690
             03 MQDEMC  PIC X.                                          00003700
             03 MQDEMP  PIC X.                                          00003710
             03 MQDEMH  PIC X.                                          00003720
             03 MQDEMV  PIC X.                                          00003730
             03 MQDEMO  PIC X(5).                                       00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MQRESA  PIC X.                                          00003760
             03 MQRESC  PIC X.                                          00003770
             03 MQRESP  PIC X.                                          00003780
             03 MQRESH  PIC X.                                          00003790
             03 MQRESV  PIC X.                                          00003800
             03 MQRESO  PIC X(5).                                       00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MQPRELA      PIC X.                                     00003830
             03 MQPRELC PIC X.                                          00003840
             03 MQPRELP PIC X.                                          00003850
             03 MQPRELH PIC X.                                          00003860
             03 MQPRELV PIC X.                                          00003870
             03 MQPRELO      PIC X(5).                                  00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MZONCMDA  PIC X.                                          00003900
           02 MZONCMDC  PIC X.                                          00003910
           02 MZONCMDP  PIC X.                                          00003920
           02 MZONCMDH  PIC X.                                          00003930
           02 MZONCMDV  PIC X.                                          00003940
           02 MZONCMDO  PIC X(15).                                      00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLIBERRA  PIC X.                                          00003970
           02 MLIBERRC  PIC X.                                          00003980
           02 MLIBERRP  PIC X.                                          00003990
           02 MLIBERRH  PIC X.                                          00004000
           02 MLIBERRV  PIC X.                                          00004010
           02 MLIBERRO  PIC X(58).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCODTRAA  PIC X.                                          00004040
           02 MCODTRAC  PIC X.                                          00004050
           02 MCODTRAP  PIC X.                                          00004060
           02 MCODTRAH  PIC X.                                          00004070
           02 MCODTRAV  PIC X.                                          00004080
           02 MCODTRAO  PIC X(4).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCICSA    PIC X.                                          00004110
           02 MCICSC    PIC X.                                          00004120
           02 MCICSP    PIC X.                                          00004130
           02 MCICSH    PIC X.                                          00004140
           02 MCICSV    PIC X.                                          00004150
           02 MCICSO    PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MNETNAMA  PIC X.                                          00004180
           02 MNETNAMC  PIC X.                                          00004190
           02 MNETNAMP  PIC X.                                          00004200
           02 MNETNAMH  PIC X.                                          00004210
           02 MNETNAMV  PIC X.                                          00004220
           02 MNETNAMO  PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSCREENA  PIC X.                                          00004250
           02 MSCREENC  PIC X.                                          00004260
           02 MSCREENP  PIC X.                                          00004270
           02 MSCREENH  PIC X.                                          00004280
           02 MSCREENV  PIC X.                                          00004290
           02 MSCREENO  PIC X(4).                                       00004300
                                                                                
