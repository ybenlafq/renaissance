      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVRM3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM3000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRM3000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRM3000.                                                            
      *}                                                                        
           02  RM30-NLOI                                                        
               PIC X(0002).                                                     
           02  RM30-QINDISPO                                                    
               PIC S9(3)V9(0002) COMP-3.                                        
           02  RM30-QV8S                                                        
               PIC S9(7) COMP-3.                                                
           02  RM30-NINDICE                                                     
               PIC X(0001).                                                     
           02  RM30-QSTOCKOBJ                                                   
               PIC S9(7) COMP-3.                                                
           02  RM30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM3000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVRM3000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVRM3000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM30-NLOI-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM30-NLOI-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM30-QINDISPO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM30-QINDISPO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM30-QV8S-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM30-QV8S-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM30-NINDICE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM30-NINDICE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM30-QSTOCKOBJ-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM30-QSTOCKOBJ-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
