      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * COMMAREA COMMGQ10                                               00000001
      * DU MENU TGQ10 "GESTION DES QUOTAS DE DELIVRANCE"                00000002
      *                                                                 00000010
      **************************************************************    00000020
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *    00000030
      **************************************************************    00000040
      *                                                                 00000050
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET            00000060
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR  00000070
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU          00000080
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                    00000090
      *                                                                 00000091
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096    00000092
      * COMPRENANT :                                                    00000093
      * 1 - LES ZONES RESERVEES A AIDA                                  00000094
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                   00000095
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                 00000096
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                      00000097
      * 5 - LES ZONES RESERVEES APPLICATIVES                            00000098
      *                                                                 00000099
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES       00000100
      * PAR AIDA                                                        00000110
      *                                                                 00000120
      *-------------------------------------------------------------    00000130
      *                                                                 00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GQ10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.           00000150
      *--                                                                       
       01  COM-GQ10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                 00000160
       01  Z-COMMAREA.                                                  00000170
      *                                                                 00000180
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00000190
           02 FILLER-COM-AIDA      PIC X(100).                          00000191
      *                                                                 00000192
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00000193
           02 COMM-CICS-APPLID     PIC X(8).                            00000194
           02 COMM-CICS-NETNAM     PIC X(8).                            00000195
           02 COMM-CICS-TRANSA     PIC X(4).                            00000196
      *                                                                 00000197
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00000198
           02 COMM-DATE-SIECLE     PIC XX.                              00000199
           02 COMM-DATE-ANNEE      PIC XX.                              00000200
           02 COMM-DATE-MOIS       PIC XX.                              00000210
           02 COMM-DATE-JOUR       PIC 99.                              00000220
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00000230
           02 COMM-DATE-QNTA       PIC 999.                             00000240
           02 COMM-DATE-QNT0       PIC 99999.                           00000250
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00000260
           02 COMM-DATE-BISX       PIC 9.                               00000270
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00000280
           02 COMM-DATE-JSM        PIC 9.                               00000290
      *   LIBELLES DU JOUR COURT - LONG                                 00000291
           02 COMM-DATE-JSM-LC     PIC XXX.                             00000292
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                        00000293
      *   LIBELLES DU MOIS COURT - LONG                                 00000294
           02 COMM-DATE-MOIS-LC    PIC XXX.                             00000295
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                        00000296
      *   DIFFERENTES FORMES DE DATE                                    00000297
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                            00000298
           02 COMM-DATE-AAMMJJ     PIC X(6).                            00000299
           02 COMM-DATE-JJMMSSAA   PIC X(8).                            00000300
           02 COMM-DATE-JJMMAA     PIC X(6).                            00000310
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                            00000320
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                           00000330
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00000340
           02 COMM-DATE-WEEK.                                           00000350
              05 COMM-DATE-SEMSS   PIC 99.                              00000360
              05 COMM-DATE-SEMAA   PIC 99.                              00000370
              05 COMM-DATE-SEMNU   PIC 99.                              00000380
      *                                                                 00000390
           02 COMM-DATE-FILLER     PIC X(08).                           00000391
      *                                                                 00000392
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152  00000393
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.             00000394
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 150 PIC X(1).                       00000395
      *                                                                 00000396
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00000397
           02 COMM-GQ10-APPLI.                                          00000398
              03 COMM-GQ10-OPTION        PIC X(01).                     00000399
              03 COMM-GQ10-FONC          PIC X(03).                     00000400
              03 COMM-GQ10-CMODEL        PIC X(03).                     00000401
              03 COMM-GQ10-CPROFIL       PIC X(05).                     00000410
              03 COMM-GQ10-CEQUIP        PIC X(05).                     00000420
              03 COMM-GQ10-CSOC          PIC X(03).                     00000430
              03 COMM-GQ10-CLIEU         PIC X(03).                     00000440
              03 COMM-GQ10-TYPLIEU       PIC X(08).                     00000441
              03 COMM-GQ10-DATE-SEL      PIC X(08).                     00000450
              03 COMM-GQ10-DATE-ED       PIC X(08).                     00000451
              03 COMM-GQ10-LPROFIL       PIC X(20).                     00000460
              03 COMM-GQ10-LEQUIP        PIC X(20).                     00000470
              03 COMM-GQ10-TOP           PIC 9(01).                     00000480
                88 CREATION-IMPOSSIBLE   VALUE 2.                       00000490
                88 CREE                  VALUE 1.                       00000491
                88 NON-CREE              VALUE 0.                       00000492
           02 COMM-GQ10-APPLI.                                          00000510
              03 COMM-GQ10-FILLER        PIC X(3636).                   00000520
                                                                                
