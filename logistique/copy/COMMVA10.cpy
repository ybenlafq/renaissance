      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * LONGUEUR 13700                                                          
       01  COMM-VA10-APPLI.                                                     
           05  COMM-VA10-NSOCIETE       PIC  X(3).                              
           05  COMM-VA10-PARAM          PIC  X(6).                              
           05  MVA010-SORTIE.                                                   
              10 NIVEAU-1 OCCURS 500.                                           
                 15 COMM-VA10-NSOCVAL   PIC X(3).                               
                 15 COMM-VA10-NLIEUVAL  PIC X(3).                               
                 15 NIVEAU-2 OCCURS 50.                                         
                    20 COMM-VA10-CONSO    PIC X(6).                             
                    20 COMM-VA10-LCONSO   PIC X(20).                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-VA10-CODRET         PIC  S9(4) COMP.                        
      *--                                                                       
           05  COMM-VA10-CODRET         PIC  S9(4) COMP-5.                      
      *}                                                                        
           05  COMM-VA10-MESS           PIC  X(50).                             
           05  COMM-VA10-FILLER         PIC  X(39).                             
                                                                                
