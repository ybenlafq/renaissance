      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESU51   ESU51                                              00000020
      ***************************************************************** 00000030
       01   ESU51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFFOURNL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MREFFOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MREFFOURNF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MREFFOURNI     PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCFAMI    PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLFAMI    PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCMARQI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLMARQI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDJJL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDATEDJJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDJJF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDATEDJJI      PIC X(2).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDMML      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MDATEDMML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDMMF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDATEDMMI      PIC X(2).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEDAAL      COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MDATEDAAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEDAAF      PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEDAAI      PIC X(4).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFJJL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MDATEFJJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFJJF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATEFJJI      PIC X(2).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFMML      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MDATEFMML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFMMF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATEFMMI      PIC X(2).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEFAAL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDATEFAAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATEFAAF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDATEFAAI      PIC X(4).                                  00000610
           02 M8I OCCURS   12 TIMES .                                   00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNSOCI  PIC X(3).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNLIEUI      PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQTEI   PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(12).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(61).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: ESU51   ESU51                                              00001000
      ***************************************************************** 00001010
       01   ESU51O REDEFINES ESU51I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNCODICA  PIC X.                                          00001190
           02 MNCODICC  PIC X.                                          00001200
           02 MNCODICP  PIC X.                                          00001210
           02 MNCODICH  PIC X.                                          00001220
           02 MNCODICV  PIC X.                                          00001230
           02 MNCODICO  PIC X(7).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MREFFOURNA     PIC X.                                     00001260
           02 MREFFOURNC     PIC X.                                     00001270
           02 MREFFOURNP     PIC X.                                     00001280
           02 MREFFOURNH     PIC X.                                     00001290
           02 MREFFOURNV     PIC X.                                     00001300
           02 MREFFOURNO     PIC X(20).                                 00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCFAMA    PIC X.                                          00001330
           02 MCFAMC    PIC X.                                          00001340
           02 MCFAMP    PIC X.                                          00001350
           02 MCFAMH    PIC X.                                          00001360
           02 MCFAMV    PIC X.                                          00001370
           02 MCFAMO    PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MLFAMA    PIC X.                                          00001400
           02 MLFAMC    PIC X.                                          00001410
           02 MLFAMP    PIC X.                                          00001420
           02 MLFAMH    PIC X.                                          00001430
           02 MLFAMV    PIC X.                                          00001440
           02 MLFAMO    PIC X(20).                                      00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCMARQA   PIC X.                                          00001470
           02 MCMARQC   PIC X.                                          00001480
           02 MCMARQP   PIC X.                                          00001490
           02 MCMARQH   PIC X.                                          00001500
           02 MCMARQV   PIC X.                                          00001510
           02 MCMARQO   PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLMARQA   PIC X.                                          00001540
           02 MLMARQC   PIC X.                                          00001550
           02 MLMARQP   PIC X.                                          00001560
           02 MLMARQH   PIC X.                                          00001570
           02 MLMARQV   PIC X.                                          00001580
           02 MLMARQO   PIC X(20).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MDATEDJJA      PIC X.                                     00001610
           02 MDATEDJJC PIC X.                                          00001620
           02 MDATEDJJP PIC X.                                          00001630
           02 MDATEDJJH PIC X.                                          00001640
           02 MDATEDJJV PIC X.                                          00001650
           02 MDATEDJJO      PIC X(2).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MDATEDMMA      PIC X.                                     00001680
           02 MDATEDMMC PIC X.                                          00001690
           02 MDATEDMMP PIC X.                                          00001700
           02 MDATEDMMH PIC X.                                          00001710
           02 MDATEDMMV PIC X.                                          00001720
           02 MDATEDMMO      PIC X(2).                                  00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MDATEDAAA      PIC X.                                     00001750
           02 MDATEDAAC PIC X.                                          00001760
           02 MDATEDAAP PIC X.                                          00001770
           02 MDATEDAAH PIC X.                                          00001780
           02 MDATEDAAV PIC X.                                          00001790
           02 MDATEDAAO      PIC X(4).                                  00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MDATEFJJA      PIC X.                                     00001820
           02 MDATEFJJC PIC X.                                          00001830
           02 MDATEFJJP PIC X.                                          00001840
           02 MDATEFJJH PIC X.                                          00001850
           02 MDATEFJJV PIC X.                                          00001860
           02 MDATEFJJO      PIC X(2).                                  00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MDATEFMMA      PIC X.                                     00001890
           02 MDATEFMMC PIC X.                                          00001900
           02 MDATEFMMP PIC X.                                          00001910
           02 MDATEFMMH PIC X.                                          00001920
           02 MDATEFMMV PIC X.                                          00001930
           02 MDATEFMMO      PIC X(2).                                  00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MDATEFAAA      PIC X.                                     00001960
           02 MDATEFAAC PIC X.                                          00001970
           02 MDATEFAAP PIC X.                                          00001980
           02 MDATEFAAH PIC X.                                          00001990
           02 MDATEFAAV PIC X.                                          00002000
           02 MDATEFAAO      PIC X(4).                                  00002010
           02 M8O OCCURS   12 TIMES .                                   00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MNSOCA  PIC X.                                          00002040
             03 MNSOCC  PIC X.                                          00002050
             03 MNSOCP  PIC X.                                          00002060
             03 MNSOCH  PIC X.                                          00002070
             03 MNSOCV  PIC X.                                          00002080
             03 MNSOCO  PIC X(3).                                       00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MNLIEUA      PIC X.                                     00002110
             03 MNLIEUC PIC X.                                          00002120
             03 MNLIEUP PIC X.                                          00002130
             03 MNLIEUH PIC X.                                          00002140
             03 MNLIEUV PIC X.                                          00002150
             03 MNLIEUO      PIC X(3).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MQTEA   PIC X.                                          00002180
             03 MQTEC   PIC X.                                          00002190
             03 MQTEP   PIC X.                                          00002200
             03 MQTEH   PIC X.                                          00002210
             03 MQTEV   PIC X.                                          00002220
             03 MQTEO   PIC X(5).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(12).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(61).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
