      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CTCTA TRANSCO CLE COMPTABILISATION     *        
      *----------------------------------------------------------------*        
       01  RVCTCTA.                                                             
           05  CTCTA-CTABLEG2    PIC X(15).                                     
           05  CTCTA-CTABLEG2-REDEF REDEFINES CTCTA-CTABLEG2.                   
               10  CTCTA-TYPCPT          PIC X(01).                             
               10  CTCTA-SENS            PIC X(01).                             
               10  CTCTA-CGSG            PIC X(01).                             
           05  CTCTA-WTABLEG     PIC X(80).                                     
           05  CTCTA-WTABLEG-REDEF  REDEFINES CTCTA-WTABLEG.                    
               10  CTCTA-CTRANSAC        PIC X(04).                             
               10  CTCTA-CCOMPTA         PIC X(02).                             
               10  CTCTA-LTYPCPT         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCTCTA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTCTA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CTCTA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CTCTA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CTCTA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
