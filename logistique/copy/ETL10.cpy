      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ETL10   ETL10                                              00000020
      ***************************************************************** 00000030
       01   ETL10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MJOURI    PIC X(8).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLSOCI    PIC X(20).                                      00000290
           02 M148I OCCURS   12 TIMES .                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLPROFILL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLPROFILL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLPROFILF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLPROFILI    PIC X(20).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTOURNL     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MWTOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWTOURNF     PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MWTOURNI     PIC X.                                     00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHTOURNL     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MHTOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MHTOURNF     PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MHTOURNI     PIC X(2).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTOURNL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MMTOURNL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMTOURNF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MMTOURNI     PIC X(2).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWDESTOCL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWDESTOCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWDESTOCF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MWDESTOCI    PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHDESTOCL    COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MHDESTOCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MHDESTOCF    PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MHDESTOCI    PIC X(2).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMDESTOCL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MMDESTOCL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MMDESTOCF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MMDESTOCI    PIC X(2).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCRAFL  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCRAFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCRAFF  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCRAFI  PIC X(3).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MWSELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MWSELF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWSELI  PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(58).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: ETL10   ETL10                                              00000920
      ***************************************************************** 00000930
       01   ETL10O REDEFINES ETL10I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MPAGEA    PIC X.                                          00001110
           02 MPAGEC    PIC X.                                          00001120
           02 MPAGEP    PIC X.                                          00001130
           02 MPAGEH    PIC X.                                          00001140
           02 MPAGEV    PIC X.                                          00001150
           02 MPAGEO    PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MJOURA    PIC X.                                          00001180
           02 MJOURC    PIC X.                                          00001190
           02 MJOURP    PIC X.                                          00001200
           02 MJOURH    PIC X.                                          00001210
           02 MJOURV    PIC X.                                          00001220
           02 MJOURO    PIC X(8).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MNSOCA    PIC X.                                          00001250
           02 MNSOCC    PIC X.                                          00001260
           02 MNSOCP    PIC X.                                          00001270
           02 MNSOCH    PIC X.                                          00001280
           02 MNSOCV    PIC X.                                          00001290
           02 MNSOCO    PIC X(3).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MLSOCA    PIC X.                                          00001320
           02 MLSOCC    PIC X.                                          00001330
           02 MLSOCP    PIC X.                                          00001340
           02 MLSOCH    PIC X.                                          00001350
           02 MLSOCV    PIC X.                                          00001360
           02 MLSOCO    PIC X(20).                                      00001370
           02 M148O OCCURS   12 TIMES .                                 00001380
             03 FILLER       PIC X(2).                                  00001390
             03 MLPROFILA    PIC X.                                     00001400
             03 MLPROFILC    PIC X.                                     00001410
             03 MLPROFILP    PIC X.                                     00001420
             03 MLPROFILH    PIC X.                                     00001430
             03 MLPROFILV    PIC X.                                     00001440
             03 MLPROFILO    PIC X(20).                                 00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MWTOURNA     PIC X.                                     00001470
             03 MWTOURNC     PIC X.                                     00001480
             03 MWTOURNP     PIC X.                                     00001490
             03 MWTOURNH     PIC X.                                     00001500
             03 MWTOURNV     PIC X.                                     00001510
             03 MWTOURNO     PIC X.                                     00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MHTOURNA     PIC X.                                     00001540
             03 MHTOURNC     PIC X.                                     00001550
             03 MHTOURNP     PIC X.                                     00001560
             03 MHTOURNH     PIC X.                                     00001570
             03 MHTOURNV     PIC X.                                     00001580
             03 MHTOURNO     PIC X(2).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MMTOURNA     PIC X.                                     00001610
             03 MMTOURNC     PIC X.                                     00001620
             03 MMTOURNP     PIC X.                                     00001630
             03 MMTOURNH     PIC X.                                     00001640
             03 MMTOURNV     PIC X.                                     00001650
             03 MMTOURNO     PIC X(2).                                  00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MWDESTOCA    PIC X.                                     00001680
             03 MWDESTOCC    PIC X.                                     00001690
             03 MWDESTOCP    PIC X.                                     00001700
             03 MWDESTOCH    PIC X.                                     00001710
             03 MWDESTOCV    PIC X.                                     00001720
             03 MWDESTOCO    PIC X.                                     00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MHDESTOCA    PIC X.                                     00001750
             03 MHDESTOCC    PIC X.                                     00001760
             03 MHDESTOCP    PIC X.                                     00001770
             03 MHDESTOCH    PIC X.                                     00001780
             03 MHDESTOCV    PIC X.                                     00001790
             03 MHDESTOCO    PIC X(2).                                  00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MMDESTOCA    PIC X.                                     00001820
             03 MMDESTOCC    PIC X.                                     00001830
             03 MMDESTOCP    PIC X.                                     00001840
             03 MMDESTOCH    PIC X.                                     00001850
             03 MMDESTOCV    PIC X.                                     00001860
             03 MMDESTOCO    PIC X(2).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MCRAFA  PIC X.                                          00001890
             03 MCRAFC  PIC X.                                          00001900
             03 MCRAFP  PIC X.                                          00001910
             03 MCRAFH  PIC X.                                          00001920
             03 MCRAFV  PIC X.                                          00001930
             03 MCRAFO  PIC X(3).                                       00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MWSELA  PIC X.                                          00001960
             03 MWSELC  PIC X.                                          00001970
             03 MWSELP  PIC X.                                          00001980
             03 MWSELH  PIC X.                                          00001990
             03 MWSELV  PIC X.                                          00002000
             03 MWSELO  PIC X.                                          00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(58).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
