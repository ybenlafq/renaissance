      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVTL0500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVTL0500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTL0500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTL0500.                                                            
      *}                                                                        
           02  TL05-DDEMANDE                                                    
               PIC X(0008).                                                     
           02  TL05-NSOC                                                        
               PIC X(0003).                                                     
           02  TL05-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  TL05-DDELIV                                                      
               PIC X(0008).                                                     
           02  TL05-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  TL05-NDEPOT                                                      
               PIC X(0003).                                                     
           02  TL05-WTOURNEE                                                    
               PIC X(0001).                                                     
           02  TL05-DHTOURNEE                                                   
               PIC X(0004).                                                     
           02  TL05-WDESTOCK                                                    
               PIC X(0001).                                                     
           02  TL05-DHDESTOCK                                                   
               PIC X(0004).                                                     
           02  TL05-QCDENONCOM                                                  
               PIC S9(5) COMP-3.                                                
           02  TL05-QPCENONCOM                                                  
               PIC S9(5) COMP-3.                                                
           02  TL05-QCDECOM                                                     
               PIC S9(5) COMP-3.                                                
           02  TL05-QPCECOM                                                     
               PIC S9(5) COMP-3.                                                
           02  TL05-DRAFALE                                                     
               PIC X(0008).                                                     
           02  TL05-NRAFALE                                                     
               PIC X(0003).                                                     
           02  TL05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVTL0500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTL0500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTL0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-DDEMANDE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-DDEMANDE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-WTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-WTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-DHTOURNEE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-DHTOURNEE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-WDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-WDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-DHDESTOCK-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-DHDESTOCK-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-QCDENONCOM-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-QCDENONCOM-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-QPCENONCOM-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-QPCENONCOM-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-QCDECOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-QCDECOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-QPCECOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-QPCECOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-DRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-DRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-NRAFALE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  TL05-NRAFALE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TL05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  TL05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
