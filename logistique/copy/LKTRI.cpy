      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *=====================================================*                   
      *                                                     *                   
      *           MODULE DE TRI INTERNE D'UNE TABLE         *                   
      *           DEPASSANT LA LIMITE CICS DES 64K          *                   
      *           ---------------------------------         *                   
      *                                                     *                   
      *   EXEMPLE D'UTILISATION :                           *                   
      *                                                     *                   
      *   POUR UN TRI, EN ASCENDANT, SUR UNE CLE DE 20      *                   
      *   OCTETS D'UNE TABLE DE 240 POSTES                  *                   
      *                                                     *                   
      *   1�) DEFINIR EN WORKING :                          *                   
      *                                                     *                   
      *       WSTRI EN "DUR" SAUF WTABLETRI                 *                   
      *                                                     *                   
      *   2�) DEFINIR EN LINKAGE : WTABLETRI                *                   
      *                                                     *                   
      *       COPY LKTRI REPLACING :NB-OCCURS: BY ==240==   *                   
      *                            :LG-CLETRI: BY ==020==.  *                   
      *                                                     *                   
      *   3�) INTEGRER EN PROCEDURE DIVISION :              *                   
      *                                                     *                   
      *       COPY PRTRI.                                   *                   
      *                                                     *                   
      *=====================================================*                   
      *                                                                         
      *                                                                         
      *       TABLE INTERNE DE TRI                                              
      *                                                                         
       1      WTABLETRI.                                                        
         2    WTABLTRI.                                                         
          4   WTABTRI                OCCURS         :NB-OCCURS:.                
            8 WCLETRI     PIC  X(:LG-CLETRI:).                                  
            8 WNBITEM     PIC  9(03).                                           
            8 WNLIGNE     PIC  9(03).                                           
      *                                                                         
                                                                                
