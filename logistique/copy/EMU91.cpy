      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU91   EMU91                                              00000020
      ***************************************************************** 00000030
       01   EMU91I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(2).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(2).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(2).                                       00000160
           02 MPAGEI    PIC X(5).                                       00000170
           02 LIGTRANI OCCURS   12 TIMES .                              00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIGT1L     COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MNLIGT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIGT1F     PIC X.                                     00000200
             03 FILLER  PIC X(2).                                       00000210
             03 MNLIGT1I     PIC X(6).                                  00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIGT1L     COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MLLIGT1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLLIGT1F     PIC X.                                     00000240
             03 FILLER  PIC X(2).                                       00000250
             03 MLLIGT1I     PIC X(20).                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPLI1L     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MTYPLI1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPLI1F     PIC X.                                     00000280
             03 FILLER  PIC X(2).                                       00000290
             03 MTYPLI1I     PIC X(5).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTI1L     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MWACTI1L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTI1F     PIC X.                                     00000320
             03 FILLER  PIC X(2).                                       00000330
             03 MWACTI1I     PIC X.                                     00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIGT2L     COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNLIGT2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIGT2F     PIC X.                                     00000360
             03 FILLER  PIC X(2).                                       00000370
             03 MNLIGT2I     PIC X(6).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIGT2L     COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLLIGT2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLLIGT2F     PIC X.                                     00000400
             03 FILLER  PIC X(2).                                       00000410
             03 MLLIGT2I     PIC X(20).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPLI2L     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MTYPLI2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTYPLI2F     PIC X.                                     00000440
             03 FILLER  PIC X(2).                                       00000450
             03 MTYPLI2I     PIC X(5).                                  00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWACTI2L     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MWACTI2L COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWACTI2F     PIC X.                                     00000480
             03 FILLER  PIC X(2).                                       00000490
             03 MWACTI2I     PIC X.                                     00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIGTRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MNLIGTRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIGTRF  PIC X.                                          00000520
           02 FILLER    PIC X(2).                                       00000530
           02 MNLIGTRI  PIC X(6).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIGTRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MLLIGTRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIGTRF  PIC X.                                          00000560
           02 FILLER    PIC X(2).                                       00000570
           02 MLLIGTRI  PIC X(20).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPLIGL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MTYPLIGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPLIGF  PIC X.                                          00000600
           02 FILLER    PIC X(2).                                       00000610
           02 MTYPLIGI  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWACTIFL  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MWACTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWACTIFF  PIC X.                                          00000640
           02 FILLER    PIC X(2).                                       00000650
           02 MWACTIFI  PIC X.                                          00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUPPL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSUPPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSUPPF    PIC X.                                          00000680
           02 FILLER    PIC X(2).                                       00000690
           02 MSUPPI    PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSUPPL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLSUPPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLSUPPF   PIC X.                                          00000720
           02 FILLER    PIC X(2).                                       00000730
           02 MLSUPPI   PIC X(23).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MLIBERRI  PIC X(78).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MCODTRAI  PIC X(4).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MNETNAMI  PIC X(8).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MSCREENI  PIC X(4).                                       00000940
      ***************************************************************** 00000950
      * SDF: EMU91   EMU91                                              00000960
      ***************************************************************** 00000970
       01   EMU91O REDEFINES EMU91I.                                    00000980
           02 FILLER    PIC X(12).                                      00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MDATJOUA  PIC X.                                          00001010
           02 MDATJOUC  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUO  PIC X(10).                                      00001040
           02 FILLER    PIC X(2).                                       00001050
           02 MTIMJOUA  PIC X.                                          00001060
           02 MTIMJOUC  PIC X.                                          00001070
           02 MTIMJOUH  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MPAGEA    PIC X.                                          00001110
           02 MPAGEC    PIC X.                                          00001120
           02 MPAGEH    PIC X.                                          00001130
           02 MPAGEO    PIC X(5).                                       00001140
           02 LIGTRANO OCCURS   12 TIMES .                              00001150
             03 FILLER       PIC X(2).                                  00001160
             03 MNLIGT1A     PIC X.                                     00001170
             03 MNLIGT1C     PIC X.                                     00001180
             03 MNLIGT1H     PIC X.                                     00001190
             03 MNLIGT1O     PIC X(6).                                  00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MLLIGT1A     PIC X.                                     00001220
             03 MLLIGT1C     PIC X.                                     00001230
             03 MLLIGT1H     PIC X.                                     00001240
             03 MLLIGT1O     PIC X(20).                                 00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MTYPLI1A     PIC X.                                     00001270
             03 MTYPLI1C     PIC X.                                     00001280
             03 MTYPLI1H     PIC X.                                     00001290
             03 MTYPLI1O     PIC X(5).                                  00001300
             03 FILLER       PIC X(2).                                  00001310
             03 MWACTI1A     PIC X.                                     00001320
             03 MWACTI1C     PIC X.                                     00001330
             03 MWACTI1H     PIC X.                                     00001340
             03 MWACTI1O     PIC X.                                     00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MNLIGT2A     PIC X.                                     00001370
             03 MNLIGT2C     PIC X.                                     00001380
             03 MNLIGT2H     PIC X.                                     00001390
             03 MNLIGT2O     PIC X(6).                                  00001400
             03 FILLER       PIC X(2).                                  00001410
             03 MLLIGT2A     PIC X.                                     00001420
             03 MLLIGT2C     PIC X.                                     00001430
             03 MLLIGT2H     PIC X.                                     00001440
             03 MLLIGT2O     PIC X(20).                                 00001450
             03 FILLER       PIC X(2).                                  00001460
             03 MTYPLI2A     PIC X.                                     00001470
             03 MTYPLI2C     PIC X.                                     00001480
             03 MTYPLI2H     PIC X.                                     00001490
             03 MTYPLI2O     PIC X(5).                                  00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MWACTI2A     PIC X.                                     00001520
             03 MWACTI2C     PIC X.                                     00001530
             03 MWACTI2H     PIC X.                                     00001540
             03 MWACTI2O     PIC X.                                     00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNLIGTRA  PIC X.                                          00001570
           02 MNLIGTRC  PIC X.                                          00001580
           02 MNLIGTRH  PIC X.                                          00001590
           02 MNLIGTRO  PIC X(6).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MLLIGTRA  PIC X.                                          00001620
           02 MLLIGTRC  PIC X.                                          00001630
           02 MLLIGTRH  PIC X.                                          00001640
           02 MLLIGTRO  PIC X(20).                                      00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MTYPLIGA  PIC X.                                          00001670
           02 MTYPLIGC  PIC X.                                          00001680
           02 MTYPLIGH  PIC X.                                          00001690
           02 MTYPLIGO  PIC X(5).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MWACTIFA  PIC X.                                          00001720
           02 MWACTIFC  PIC X.                                          00001730
           02 MWACTIFH  PIC X.                                          00001740
           02 MWACTIFO  PIC X.                                          00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MSUPPA    PIC X.                                          00001770
           02 MSUPPC    PIC X.                                          00001780
           02 MSUPPH    PIC X.                                          00001790
           02 MSUPPO    PIC X.                                          00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLSUPPA   PIC X.                                          00001820
           02 MLSUPPC   PIC X.                                          00001830
           02 MLSUPPH   PIC X.                                          00001840
           02 MLSUPPO   PIC X(23).                                      00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MLIBERRA  PIC X.                                          00001870
           02 MLIBERRC  PIC X.                                          00001880
           02 MLIBERRH  PIC X.                                          00001890
           02 MLIBERRO  PIC X(78).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MCODTRAA  PIC X.                                          00001920
           02 MCODTRAC  PIC X.                                          00001930
           02 MCODTRAH  PIC X.                                          00001940
           02 MCODTRAO  PIC X(4).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCICSA    PIC X.                                          00001970
           02 MCICSC    PIC X.                                          00001980
           02 MCICSH    PIC X.                                          00001990
           02 MCICSO    PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNETNAMA  PIC X.                                          00002020
           02 MNETNAMC  PIC X.                                          00002030
           02 MNETNAMH  PIC X.                                          00002040
           02 MNETNAMO  PIC X(8).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MSCREENA  PIC X.                                          00002070
           02 MSCREENC  PIC X.                                          00002080
           02 MSCREENH  PIC X.                                          00002090
           02 MSCREENO  PIC X(4).                                       00002100
                                                                                
