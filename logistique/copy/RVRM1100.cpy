      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRM1100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRM1100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1100.                                                            
           02  RM11-NTABLE                                                      
               PIC X(0002).                                                     
           02  RM11-QSOA                                                        
               PIC S9(7) COMP-3.                                                
           02  RM11-QSOMIN                                                      
               PIC S9(7) COMP-3.                                                
           02  RM11-QSOMAX                                                      
               PIC S9(7) COMP-3.                                                
           02  RM11-QSAMIN                                                      
               PIC S9(7) COMP-3.                                                
           02  RM11-QSAMAX                                                      
               PIC S9(7) COMP-3.                                                
           02  RM11-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRM1100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRM1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-NTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-NTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-QSOA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-QSOA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-QSOMIN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-QSOMIN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-QSOMAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-QSOMAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-QSAMIN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-QSAMIN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-QSAMAX-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-QSAMAX-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RM11-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RM11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
