      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *  TS : RETOUR DE LIVRAISON                                     * 00020000
      ***************************************************************** 00030000
       01  TS-TL28.                                                             
           02 TS28-NOM.                                                         
              03 TS28-NOM1           PIC X(04) VALUE 'TL28'.                    
              03 TS28-TERM           PIC X(04) VALUE SPACES.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 TS28-LONG              PIC S9(04) COMP VALUE  53.                 
      *--                                                                       
           02 TS28-LONG              PIC S9(04) COMP-5 VALUE  53.               
      *}                                                                        
           02 TS28-DONNEES.                                                     
              03 TS28-NMAG           PIC X(03).                                 
              03 TS28-NVENTE         PIC X(07).                                 
              03 TS28-NCODICGRP      PIC X(07).                                 
              03 TS28-NCODIC         PIC X(07).                                 
      *                              TYPE DE VENTE                              
      *                              'R' -> REPRISE                             
      *                              'L' -> LIVRAISON                           
              03 TS28-WREP           PIC X(01).                                 
              03 TS28-NSEQ           PIC X(02).                                 
              03 TS28-CADRTOUR       PIC X(01).                                 
              03 TS28-CMOTIF-I       PIC X(05).                                 
              03 TS28-CMOTIF-F       PIC X(05).                                 
              03 TS28-QVENDUE        PIC 9(05).                                 
              03 TS28-QLIVREE-I      PIC 9(05).                                 
              03 TS28-QLIVREE-F      PIC 9(05).                                 
                                                                                
