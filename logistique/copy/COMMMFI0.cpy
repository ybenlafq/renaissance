      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00002000
      * COMMAREA SPECIFIQUE PRG TFI01/TFI02/MFI00        TR: FI00  *    00002213
      *          GESTION FACTURATION INTERNE                       *    00002301
      *                                                                 00008900
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724  00009000
      *                                                                 00410100
      *  TRANSACTION FI00 : OPTION DETERMINATION REGLES PCF        *    00411013
      *                                                                 00412000
          02 COMM-MFI0-APPLI REDEFINES COMM-FI00-APPLI.                 00420013
      *------------------------------ ZONE DONNEES TFI00                00510001
             03 COMM-MFI0-DONNEES-1-TFI00.                              00520017
      *------------------------------ CODE FONCTION                     00521001
                04 COMM-MFI0-FONCT             PIC X(03).               00522013
      *------------------------------ CODE FOURNISSEUR                  00523001
                04 COMM-MFI0-NENTCDE-FI00      PIC X(05).               00524013
      *------------------------------ CODE FAMILLE                      00525001
                04 COMM-MFI0-CFAM-FI00         PIC X(05).               00526013
      *------------------------------ ZONE LIBRE                        00526119
                04 COMM-MFI0-FILLER-FI00       PIC X(25).               00526219
      *------------------------------ CODE RETOUR MFI00                 00526319
                04 COMM-MFI0-CODRET            PIC X(01).               00526419
      *------------------------------ LIBELLE MESSAGE ERREUR            00526519
                04 COMM-MFI0-MESSAGE           PIC X(60).               00526619
      *------------------------------ ZONE LIBRE                        00527001
                04 COMM-MFI0-FILLER-FI00       PIC X(625).              00528019
      *------------------------------ ZONE DONNEES TMFI0                00530013
             03 COMM-MFI0-DONNEES-1-MFI00.                              00540017
      *------------------------------ DERNIER IND-TS                    00743406
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MFI0-PAGSUI            PIC S9(4) COMP.          00743513
      *--                                                                       
                04 COMM-MFI0-PAGSUI            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ ANCIEN IND-TS                     00743601
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MFI0-PAGENC            PIC S9(4) COMP.          00743713
      *--                                                                       
                04 COMM-MFI0-PAGENC            PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ NUMERO PAGE ECRAN                 00743801
                04 COMM-MFI0-NUMPAG            PIC 9(3).                00743913
      *------------------------------ INDICATEUR FIN DE TABLE           00744001
                04 COMM-MFI0-INDPAG            PIC 9.                   00744113
      *------------------------------ OK MAJ SUR RTFI05 => VALID = '1'  00744201
                04 COMM-MFI0-VALID-RTFI05      PIC  9.                  00744313
      *------------------------------ EDITION A EFFECTUEE        = '1'  00744409
                04 COMM-MFI0-EDIT              PIC  9.                  00744513
      *------------------------------ TABLE N� TS PR PAGINATION         00744601
                04 COMM-MFI0-TABTS          OCCURS 100.                 00744713
      *------------------------------ 1ERS IND TS DES PAGES <=> FAMILLE 00744801
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *               06 COMM-MFI0-NUMTS       PIC S9(4) COMP.          00744913
      *--                                                                       
                      06 COMM-MFI0-NUMTS       PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND POUR RECH DS TS               00745001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MFI0-IND-RECH-TS       PIC S9(4) COMP.          00745113
      *--                                                                       
                04 COMM-MFI0-IND-RECH-TS       PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ IND TS MAX                        00746001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-MFI0-IND-TS-MAX        PIC S9(4) COMP.          00747013
      *--                                                                       
                04 COMM-MFI0-IND-TS-MAX        PIC S9(4) COMP-5.                
      *}                                                                        
      *------------------------------ DATE MAJ                          00747102
                04 COMM-MFI0-DSAISIE           PIC X(08).               00748013
      *------------------------------ TIME  MAJ                         00748116
                04 COMM-MFI0-HSAISIE.                                   00748216
      *------------------------------ HEURE MAJ                         00748316
                   06 COMM-MFI0-HHSAISIE          PIC X(02).            00748416
      *------------------------------ MINUTE MAJ                        00748516
                   06 COMM-MFI0-HMSAISIE          PIC X(02).            00748616
      *------------------------------ SECONDE MAJ                       00748716
                   06 COMM-MFI0-HSSAISIE          PIC X(02).            00748816
      *------------------------------ ZONE DONNEES 2 TMFI0              00760013
             03 COMM-MFI0-DONNEES-2-MFI00.                              00761017
      *------------------------------ ZONE FAMILLES PAGE                00770001
                04 COMM-MFI0-TABREGLE.                                  00780013
      *------------------------------ ZONE LIGNES FAMILLES              00790001
                   05 COMM-MFI0-LIGREGLE OCCURS 10.                     00800013
      *------------------------------ CODE ENTITE COMMANDE              00810001
                      06 COMM-MFI0-NENTCDE        PIC X(5).             00820013
      *------------------------------ CODE FAMILLE                      00821001
                      06 COMM-MFI0-CFAM           PIC X(5).             00822013
      *------------------------------ CODE TYPE REGLE                   00830001
                      06 COMM-MFI0-CTYPE          PIC X(04).            00840013
      *------------------------------ DATE EFFET REGLE                  00850001
                      06 COMM-MFI0-DEFFET         PIC X(08).            00860013
      *------------------------------ CODE MOUVEMENT (M,C)              00870018
                      06 COMM-MFI0-CMVT           PIC X(01).            00880013
      *------------------------------ ZONE DONNEES 3 TMFI0              01650013
             03 COMM-MFI0-DONNEES-3-MFI00.                              01651017
      *------------------------------ ZONES SWAP                        01670001
                   05 COMM-MFI0-ATTR           PIC X OCCURS 500.        01680013
      *------------------------------ ZONE LIBRE                        01710001
             03 COMM-MFI0-LIBRE          PIC X(1981).                   01720013
      ***************************************************************** 02170001
                                                                                
