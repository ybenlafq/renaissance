      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGQ0200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGQ0200                         
      **********************************************************                
       01  RVGQ0200.                                                            
           02  GQ02-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GQ02-NSOC                                                        
               PIC X(0003).                                                     
           02  GQ02-NLIEU                                                       
               PIC X(0003).                                                     
           02  GQ02-DJOUR                                                       
               PIC X(0008).                                                     
           02  GQ02-QQUOTA                                                      
               PIC S9(5) COMP-3.                                                
           02  GQ02-QPRIS                                                       
               PIC S9(5) COMP-3.                                                
           02  GQ02-WILLIM                                                      
               PIC X(0001).                                                     
           02  GQ02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGQ0200                                  
      **********************************************************                
       01  RVGQ0200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-QQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-QQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-QPRIS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-QPRIS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-WILLIM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GQ02-WILLIM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GQ02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GQ02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
