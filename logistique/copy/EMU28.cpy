      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EMU28   EMU28                                              00000020
      ***************************************************************** 00000030
       01   EMU28I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLLIEUI   PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNBPI     PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPQUOL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNBPQUOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPQUOF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNBPQUOI  PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCENTL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNSOCENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCENTF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCENTI      PIC X(3).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDEPOTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNDEPOTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNDEPOTI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEPOTL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDEPOTF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLDEPOTI  PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBM3L    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNBM3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBM3F    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNBM3I    PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBM3QUOL      COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNBM3QUOL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBM3QUOF      PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNBM3QUOI      PIC X(5).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMUTATIF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNMUTATII      PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMUTATIL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDMUTATIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDMUTATIF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDMUTATII      PIC X(10).                                 00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSELAL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCSELAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSELAF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCSELAI   PIC X(5).                                       00000650
           02 M4I OCCURS   11 TIMES .                                   00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCFAMI  PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCMARQI      PIC X(5).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFFOUL    COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MLREFFOUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLREFFOUF    PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MLREFFOUI    PIC X(20).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNCODICI     PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQAUNITL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQAUNITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQAUNITF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQAUNITI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQALIEEL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQALIEEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQALIEEF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQALIEEI     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQNUNITL     COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQNUNITL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQNUNITF     PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQNUNITI     PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNDISPOL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MNDISPOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNDISPOF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MNDISPOI     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MGEPENUL     COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MGEPENUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MGEPENUF     PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MGEPENUI     PIC X.                                     00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIENL      COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MNLIENL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIENF      PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MNLIENI      PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMDCLIL     COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MCMDCLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCMDCLIF     PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCMDCLII     PIC X(2).                                  00001100
           02 MCQUOTAD OCCURS   6 TIMES .                               00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCQUOTAL     COMP PIC S9(4).                            00001120
      *--                                                                       
             03 MCQUOTAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCQUOTAF     PIC X.                                     00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MCQUOTAI     PIC X(5).                                  00001150
           02 MUOD OCCURS   6 TIMES .                                   00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MUOL    COMP PIC S9(4).                                 00001170
      *--                                                                       
             03 MUOL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MUOF    PIC X.                                          00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MUOI    PIC X(5).                                       00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MZONCMDI  PIC X(12).                                      00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MLIBERRI  PIC X(61).                                      00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MCODTRAI  PIC X(4).                                       00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MCICSI    PIC X(5).                                       00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MNETNAMI  PIC X(8).                                       00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001410
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001420
           02 FILLER    PIC X(4).                                       00001430
           02 MSCREENI  PIC X(4).                                       00001440
      ***************************************************************** 00001450
      * SDF: EMU28   EMU28                                              00001460
      ***************************************************************** 00001470
       01   EMU28O REDEFINES EMU28I.                                    00001480
           02 FILLER    PIC X(12).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MDATJOUA  PIC X.                                          00001510
           02 MDATJOUC  PIC X.                                          00001520
           02 MDATJOUP  PIC X.                                          00001530
           02 MDATJOUH  PIC X.                                          00001540
           02 MDATJOUV  PIC X.                                          00001550
           02 MDATJOUO  PIC X(10).                                      00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MTIMJOUA  PIC X.                                          00001580
           02 MTIMJOUC  PIC X.                                          00001590
           02 MTIMJOUP  PIC X.                                          00001600
           02 MTIMJOUH  PIC X.                                          00001610
           02 MTIMJOUV  PIC X.                                          00001620
           02 MTIMJOUO  PIC X(5).                                       00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MNSOCA    PIC X.                                          00001650
           02 MNSOCC    PIC X.                                          00001660
           02 MNSOCP    PIC X.                                          00001670
           02 MNSOCH    PIC X.                                          00001680
           02 MNSOCV    PIC X.                                          00001690
           02 MNSOCO    PIC X(3).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNLIEUA   PIC X.                                          00001720
           02 MNLIEUC   PIC X.                                          00001730
           02 MNLIEUP   PIC X.                                          00001740
           02 MNLIEUH   PIC X.                                          00001750
           02 MNLIEUV   PIC X.                                          00001760
           02 MNLIEUO   PIC X(3).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MLLIEUA   PIC X.                                          00001790
           02 MLLIEUC   PIC X.                                          00001800
           02 MLLIEUP   PIC X.                                          00001810
           02 MLLIEUH   PIC X.                                          00001820
           02 MLLIEUV   PIC X.                                          00001830
           02 MLLIEUO   PIC X(20).                                      00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNBPA     PIC X.                                          00001860
           02 MNBPC     PIC X.                                          00001870
           02 MNBPP     PIC X.                                          00001880
           02 MNBPH     PIC X.                                          00001890
           02 MNBPV     PIC X.                                          00001900
           02 MNBPO     PIC X(5).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNBPQUOA  PIC X.                                          00001930
           02 MNBPQUOC  PIC X.                                          00001940
           02 MNBPQUOP  PIC X.                                          00001950
           02 MNBPQUOH  PIC X.                                          00001960
           02 MNBPQUOV  PIC X.                                          00001970
           02 MNBPQUOO  PIC X(5).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNSOCENTA      PIC X.                                     00002000
           02 MNSOCENTC PIC X.                                          00002010
           02 MNSOCENTP PIC X.                                          00002020
           02 MNSOCENTH PIC X.                                          00002030
           02 MNSOCENTV PIC X.                                          00002040
           02 MNSOCENTO      PIC X(3).                                  00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNDEPOTA  PIC X.                                          00002070
           02 MNDEPOTC  PIC X.                                          00002080
           02 MNDEPOTP  PIC X.                                          00002090
           02 MNDEPOTH  PIC X.                                          00002100
           02 MNDEPOTV  PIC X.                                          00002110
           02 MNDEPOTO  PIC X(3).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLDEPOTA  PIC X.                                          00002140
           02 MLDEPOTC  PIC X.                                          00002150
           02 MLDEPOTP  PIC X.                                          00002160
           02 MLDEPOTH  PIC X.                                          00002170
           02 MLDEPOTV  PIC X.                                          00002180
           02 MLDEPOTO  PIC X(20).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MNBM3A    PIC X.                                          00002210
           02 MNBM3C    PIC X.                                          00002220
           02 MNBM3P    PIC X.                                          00002230
           02 MNBM3H    PIC X.                                          00002240
           02 MNBM3V    PIC X.                                          00002250
           02 MNBM3O    PIC X(7).                                       00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MNBM3QUOA      PIC X.                                     00002280
           02 MNBM3QUOC PIC X.                                          00002290
           02 MNBM3QUOP PIC X.                                          00002300
           02 MNBM3QUOH PIC X.                                          00002310
           02 MNBM3QUOV PIC X.                                          00002320
           02 MNBM3QUOO      PIC X(5).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MNMUTATIA      PIC X.                                     00002350
           02 MNMUTATIC PIC X.                                          00002360
           02 MNMUTATIP PIC X.                                          00002370
           02 MNMUTATIH PIC X.                                          00002380
           02 MNMUTATIV PIC X.                                          00002390
           02 MNMUTATIO      PIC X(7).                                  00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDMUTATIA      PIC X.                                     00002420
           02 MDMUTATIC PIC X.                                          00002430
           02 MDMUTATIP PIC X.                                          00002440
           02 MDMUTATIH PIC X.                                          00002450
           02 MDMUTATIV PIC X.                                          00002460
           02 MDMUTATIO      PIC X(10).                                 00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MCSELAA   PIC X.                                          00002490
           02 MCSELAC   PIC X.                                          00002500
           02 MCSELAP   PIC X.                                          00002510
           02 MCSELAH   PIC X.                                          00002520
           02 MCSELAV   PIC X.                                          00002530
           02 MCSELAO   PIC X(5).                                       00002540
           02 M4O OCCURS   11 TIMES .                                   00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MCFAMA  PIC X.                                          00002570
             03 MCFAMC  PIC X.                                          00002580
             03 MCFAMP  PIC X.                                          00002590
             03 MCFAMH  PIC X.                                          00002600
             03 MCFAMV  PIC X.                                          00002610
             03 MCFAMO  PIC X(5).                                       00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MCMARQA      PIC X.                                     00002640
             03 MCMARQC PIC X.                                          00002650
             03 MCMARQP PIC X.                                          00002660
             03 MCMARQH PIC X.                                          00002670
             03 MCMARQV PIC X.                                          00002680
             03 MCMARQO      PIC X(5).                                  00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MLREFFOUA    PIC X.                                     00002710
             03 MLREFFOUC    PIC X.                                     00002720
             03 MLREFFOUP    PIC X.                                     00002730
             03 MLREFFOUH    PIC X.                                     00002740
             03 MLREFFOUV    PIC X.                                     00002750
             03 MLREFFOUO    PIC X(20).                                 00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MNCODICA     PIC X.                                     00002780
             03 MNCODICC     PIC X.                                     00002790
             03 MNCODICP     PIC X.                                     00002800
             03 MNCODICH     PIC X.                                     00002810
             03 MNCODICV     PIC X.                                     00002820
             03 MNCODICO     PIC X(7).                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MQAUNITA     PIC X.                                     00002850
             03 MQAUNITC     PIC X.                                     00002860
             03 MQAUNITP     PIC X.                                     00002870
             03 MQAUNITH     PIC X.                                     00002880
             03 MQAUNITV     PIC X.                                     00002890
             03 MQAUNITO     PIC X(5).                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MQALIEEA     PIC X.                                     00002920
             03 MQALIEEC     PIC X.                                     00002930
             03 MQALIEEP     PIC X.                                     00002940
             03 MQALIEEH     PIC X.                                     00002950
             03 MQALIEEV     PIC X.                                     00002960
             03 MQALIEEO     PIC X(5).                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MQNUNITA     PIC X.                                     00002990
             03 MQNUNITC     PIC X.                                     00003000
             03 MQNUNITP     PIC X.                                     00003010
             03 MQNUNITH     PIC X.                                     00003020
             03 MQNUNITV     PIC X.                                     00003030
             03 MQNUNITO     PIC X(5).                                  00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MNDISPOA     PIC X.                                     00003060
             03 MNDISPOC     PIC X.                                     00003070
             03 MNDISPOP     PIC X.                                     00003080
             03 MNDISPOH     PIC X.                                     00003090
             03 MNDISPOV     PIC X.                                     00003100
             03 MNDISPOO     PIC X(5).                                  00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MGEPENUA     PIC X.                                     00003130
             03 MGEPENUC     PIC X.                                     00003140
             03 MGEPENUP     PIC X.                                     00003150
             03 MGEPENUH     PIC X.                                     00003160
             03 MGEPENUV     PIC X.                                     00003170
             03 MGEPENUO     PIC X.                                     00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MNLIENA      PIC X.                                     00003200
             03 MNLIENC PIC X.                                          00003210
             03 MNLIENP PIC X.                                          00003220
             03 MNLIENH PIC X.                                          00003230
             03 MNLIENV PIC X.                                          00003240
             03 MNLIENO      PIC X(5).                                  00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MCMDCLIA     PIC X.                                     00003270
             03 MCMDCLIC     PIC X.                                     00003280
             03 MCMDCLIP     PIC X.                                     00003290
             03 MCMDCLIH     PIC X.                                     00003300
             03 MCMDCLIV     PIC X.                                     00003310
             03 MCMDCLIO     PIC X(2).                                  00003320
           02 DFHMS1 OCCURS   6 TIMES .                                 00003330
             03 FILLER       PIC X(2).                                  00003340
             03 MCQUOTAA     PIC X.                                     00003350
             03 MCQUOTAC     PIC X.                                     00003360
             03 MCQUOTAP     PIC X.                                     00003370
             03 MCQUOTAH     PIC X.                                     00003380
             03 MCQUOTAV     PIC X.                                     00003390
             03 MCQUOTAO     PIC X(5).                                  00003400
           02 DFHMS2 OCCURS   6 TIMES .                                 00003410
             03 FILLER       PIC X(2).                                  00003420
             03 MUOA    PIC X.                                          00003430
             03 MUOC    PIC X.                                          00003440
             03 MUOP    PIC X.                                          00003450
             03 MUOH    PIC X.                                          00003460
             03 MUOV    PIC X.                                          00003470
             03 MUOO    PIC X(5).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MZONCMDA  PIC X.                                          00003500
           02 MZONCMDC  PIC X.                                          00003510
           02 MZONCMDP  PIC X.                                          00003520
           02 MZONCMDH  PIC X.                                          00003530
           02 MZONCMDV  PIC X.                                          00003540
           02 MZONCMDO  PIC X(12).                                      00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MLIBERRA  PIC X.                                          00003570
           02 MLIBERRC  PIC X.                                          00003580
           02 MLIBERRP  PIC X.                                          00003590
           02 MLIBERRH  PIC X.                                          00003600
           02 MLIBERRV  PIC X.                                          00003610
           02 MLIBERRO  PIC X(61).                                      00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MCODTRAA  PIC X.                                          00003640
           02 MCODTRAC  PIC X.                                          00003650
           02 MCODTRAP  PIC X.                                          00003660
           02 MCODTRAH  PIC X.                                          00003670
           02 MCODTRAV  PIC X.                                          00003680
           02 MCODTRAO  PIC X(4).                                       00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MCICSA    PIC X.                                          00003710
           02 MCICSC    PIC X.                                          00003720
           02 MCICSP    PIC X.                                          00003730
           02 MCICSH    PIC X.                                          00003740
           02 MCICSV    PIC X.                                          00003750
           02 MCICSO    PIC X(5).                                       00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MNETNAMA  PIC X.                                          00003780
           02 MNETNAMC  PIC X.                                          00003790
           02 MNETNAMP  PIC X.                                          00003800
           02 MNETNAMH  PIC X.                                          00003810
           02 MNETNAMV  PIC X.                                          00003820
           02 MNETNAMO  PIC X(8).                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MSCREENA  PIC X.                                          00003850
           02 MSCREENC  PIC X.                                          00003860
           02 MSCREENP  PIC X.                                          00003870
           02 MSCREENH  PIC X.                                          00003880
           02 MSCREENV  PIC X.                                          00003890
           02 MSCREENO  PIC X(4).                                       00003900
                                                                                
