      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      *  TS : TRAITEMENT DES REPRISES HORS SERVICE                    * 00020000
      ***************************************************************** 00030000
       01  TS-TLHS.                                                     00040000
           02 TSHS-NOM.                                                 00050000
               03 TSHS-NOM1       PIC X(04) VALUE 'TLHS'.               00060000
               03 TSHS-TERM       PIC X(04).                            00070000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *     02 TSHS-LONG       PIC S9(04) COMP VALUE +176.              00080001
      *--                                                                       
            02 TSHS-LONG       PIC S9(04) COMP-5 VALUE +176.                    
      *}                                                                        
            02 TSHS-DONNEES.                                            00090000
               03 TSHS-NLIEUENT            PIC X(03).                   00100001
               03 TSHS-NHS                 PIC X(07).                   00100101
               03 TSHS-NCODIC              PIC X(07).                   00101001
               03 TSHS-NATURE              PIC X(02).                   00110000
               03 TSHS-WARTINEX            PIC X.                       00120000
               03 TSHS-QREPRISE            PIC 9(05).                   00130000
               03 TSHS-QCONDT              PIC 9(05).                   00140000
               03 TSHS-NSOCIETE            PIC X(03).                   00150000
               03 TSHS-NLIEU               PIC X(03).                   00160000
               03 TSHS-MOTIFHS             PIC X(10).                   00170000
               03 TSHS-COMMENTHS.                                       00180000
                  04 TSHS-NSOC             PIC X(03).                   00190000
                  04 TSHS-NMAGASIN         PIC X(03).                   00200000
                  04 TSHS-NVENTE           PIC X(07).                   00210000
                  04 TSHS-NSEQ             PIC X(02).                   00220000
                  04 TSHS-FIL              PIC X(05).                   00230000
               03 TSHS-LIEUSTOCK           PIC X(9).                    00240000
               03 TSHS-EMPLACEMENT         PIC X(5).                    00250000
               03 TSHS-LIEUTRAIT           PIC X(5).                    00260000
               03 TSHS-STATUT              PIC X(5).                    00270000
               03 TSHS-COMMENTTRAIT        PIC X(20).                   00280000
               03 TSHS-TOPHS               PIC X.                       00290000
               03 TSHS-NSOCLIVR            PIC X(3).                    00300000
               03 TSHS-NDEPOT              PIC X(3).                    00310000
               03 TSHS-CMODDEL             PIC X(3).                    00320000
               03 TSHS-NMAG                PIC X(3).                    00330000
               03 TSHS-DDELIV              PIC X(8).                    00340000
               03 TSHS-CVENDEUR            PIC X(6).                    00350000
               03 TSHS-PVUNITF             PIC S9(7)V99 COMP-3.         00360000
               03 TSHS-CINSEE              PIC X(5).                    00370000
               03 TSHS-PVUNIT              PIC S9(7)V99 COMP-3.         00380001
               03 TSHS-TAUXTVA             PIC S9(3)V99 COMP-3.         00390001
               03 TSHS-DCREATION           PIC X(8).                    00400001
               03 TSHS-WEXPORT             PIC X.                       00410001
               03 TSHS-REMISE              PIC S9(7)V99 COMP-3.         00420001
               03 TSHS-NCODICGRP           PIC X(07).                   00430001
                                                                                
