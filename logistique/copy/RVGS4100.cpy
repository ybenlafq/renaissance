      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGS4100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGS4100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS4100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS4100.                                                            
      *}                                                                        
           02  GS41-NSOCORIG                                                    
               PIC X(0003).                                                     
           02  GS41-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GS41-NSSLIEUORIG                                                 
               PIC X(0003).                                                     
           02  GS41-NSOCDEST                                                    
               PIC X(0003).                                                     
           02  GS41-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  GS41-NSSLIEUDEST                                                 
               PIC X(0003).                                                     
           02  GS41-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS41-WSTOCKMASQ                                                  
               PIC X(0001).                                                     
           02  GS41-NSOCVTE                                                     
               PIC X(0003).                                                     
           02  GS41-NLIEUVTE                                                    
               PIC X(0003).                                                     
           02  GS41-NORIGINE                                                    
               PIC X(0007).                                                     
           02  GS41-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GS41-DMVT                                                        
               PIC X(0008).                                                     
           02  GS41-DHMVT                                                       
               PIC X(0002).                                                     
           02  GS41-DMMVT                                                       
               PIC X(0002).                                                     
           02  GS41-CPROG                                                       
               PIC X(0005).                                                     
           02  GS41-COPER                                                       
               PIC X(0010).                                                     
           02  GS41-DOPER                                                       
               PIC X(0008).                                                     
           02  GS41-QMVT                                                        
               PIC S9(5) COMP-3.                                                
           02  GS41-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GS41-CINSEE                                                      
               PIC X(0005).                                                     
           02  GS41-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  GS41-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GS41-DCREATION                                                   
               PIC X(0008).                                                     
           02  GS41-CLIEUTRTORIG                                                
               PIC X(0005).                                                     
           02  GS41-CLIEUTRTDEST                                                
               PIC X(0005).                                                     
           02  GS41-DSTAT                                                       
               PIC X(0006).                                                     
           02  GS41-PVTOTALSR                                                   
               PIC S9(7)V99 COMP-3.                                             
           02  GS41-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GS41-NSEQ                                                        
               PIC X(0002).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGS4100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGS4100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGS4100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NSOCORIG-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NSOCORIG-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NSSLIEUORIG-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NSOCDEST-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NSOCDEST-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NSSLIEUDEST-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-WSTOCKMASQ-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-WSTOCKMASQ-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NSOCVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NSOCVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NLIEUVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NLIEUVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NORIGINE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NORIGINE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DHMVT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DHMVT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DMMVT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DMMVT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-CPROG-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-CPROG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-COPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-COPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-QMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-QMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-CLIEUTRTORIG-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-CLIEUTRTORIG-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-CLIEUTRTDEST-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-CLIEUTRTDEST-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-PVTOTALSR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-PVTOTALSR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GS41-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GS41-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
