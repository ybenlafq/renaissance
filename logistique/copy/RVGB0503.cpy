      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ********************************************************** CC00002        
      *   COPY DE LA TABLE RVGB0503                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGB0503                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0503.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0503.                                                            
      *}                                                                        
           02  GB05-NSOCENTR                                                    
               PIC X(0003).                                                     
           02  GB05-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GB05-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GB05-NLIEU                                                       
               PIC X(0003).                                                     
           02  GB05-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GB05-DDEBSAIS                                                    
               PIC X(0008).                                                     
           02  GB05-DFINSAIS                                                    
               PIC X(0008).                                                     
           02  GB05-DDESTOCK                                                    
               PIC X(0008).                                                     
           02  GB05-DMUTATION                                                   
               PIC X(0008).                                                     
           02  GB05-CSELART                                                     
               PIC X(0005).                                                     
           02  GB05-DHEURMUT                                                    
               PIC X(0002).                                                     
           02  GB05-DMINUMUT                                                    
               PIC X(0002).                                                     
           02  GB05-QNBCAMIONS                                                  
               PIC S9(2) COMP-3.                                                
           02  GB05-QNBM3QUOTA                                                  
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GB05-QNBPQUOTA                                                   
               PIC S9(5) COMP-3.                                                
           02  GB05-QVOLUME                                                     
               PIC S9(11) COMP-3.                                               
           02  GB05-QNBPIECES                                                   
               PIC S9(5) COMP-3.                                                
           02  GB05-QNBLIGNES                                                   
               PIC S9(5) COMP-3.                                                
           02  GB05-QNBPLANCE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB05-QNBLLANCE                                                   
               PIC S9(5) COMP-3.                                                
           02  GB05-WPROPERMIS                                                  
               PIC X(0001).                                                     
           02  GB05-WVAL                                                        
               PIC X(0001).                                                     
           02  GB05-LHEURLIMIT                                                  
               PIC X(0010).                                                     
           02  GB05-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GB05-DVALID                                                      
               PIC X(0008).                                                     
           02  GB05-QNBM3PROPOS                                                 
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GB05-QNBPPROPOS                                                  
               PIC S9(5) COMP-3.                                                
           02  GB05-DCHARGT                                                     
               PIC X(0008).                                                     
           02  GB05-HCHARGT                                                     
               PIC X(0005).                                                     
           02  GB05-NLIGTRANS                                                   
               PIC X(0006).                                                     
           02  GB05-NSEQUENCE                                                   
               PIC X(0002).                                                     
           02  GB05-WMULTI                                                      
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGB0502                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGB0502-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGB0502-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NSOCENTR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NSOCENTR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DDEBSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DDEBSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DFINSAIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DFINSAIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DDESTOCK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DDESTOCK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-CSELART-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-CSELART-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DHEURMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DHEURMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DMINUMUT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DMINUMUT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBCAMIONS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBCAMIONS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBM3QUOTA-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBM3QUOTA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBPQUOTA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBPQUOTA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QVOLUME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QVOLUME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBPIECES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBPIECES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBLIGNES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBLIGNES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBPLANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBPLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBLLANCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBLLANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-WPROPERMIS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-WPROPERMIS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-WVAL-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-WVAL-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-LHEURLIMIT-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-LHEURLIMIT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DVALID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DVALID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBM3PROPOS-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBM3PROPOS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-QNBPPROPOS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-QNBPPROPOS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-DCHARGT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-DCHARGT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-HCHARGT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-HCHARGT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NLIGTRANS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NLIGTRANS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-NSEQUENCE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-NSEQUENCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GB05-WMULTI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GB05-WMULTI-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
