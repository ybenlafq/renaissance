      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TMU19                                          *  00020000
      ****************************************************************  00050000
      *                                                                 00060000
      *------------------------------ LONGUEUR                          00070000
       01  TS-LONG              PIC S9(2) COMP-3 VALUE 89.              00080000
       01  TS-DONNEES.                                                  00090000
              10 TS-NMUTATION   PIC X(07).                              00100000
              10 TS-DO          PIC 9(08).                              00120000
              10 TS-DF          PIC 9(08).                              00121000
              10 TS-DD          PIC 9(08).                              00122000
              10 TS-DM          PIC 9(08).                              00123000
              10 TS-SOC         PIC X(03).                              00130000
              10 TS-LIEU        PIC X(03).                              00140000
              10 TS-SELART      PIC X(05).                              00141000
              10 TS-PRO         PIC X(01).                              00150000
              10 TS-HEURE       PIC 9(02).                              00160000
              10 TS-MINUTE      PIC 9(02).                              00161000
              10 TS-QM3         PIC S9(3)V99 COMP-3.                    00180000
              10 TS-QP          PIC S9(4)    COMP-3.                    00182000
              10 TS-QPM3        PIC S9(3)V99 COMP-3.                    00183000
              10 TS-QPP         PIC S9(4)    COMP-3.                    00184000
              10 TS-QEMD        PIC S9(5)    COMP-3.                    00185000
              10 TS-COMMENT     PIC X(05).                              00200000
              10 TS-DC          PIC 9(08).                              00210000
              10 TS-HCHAR       PIC X(05).                              00220000
              10 TS-VALID       PIC X(01).                              00230000
                 88 TS-NON-VALIDEE-DSTKEE   VALUE 'O'.                  00240000
                 88 TS-VALIDEE-OU-DSTKEE    VALUE 'N'.                  00250000
                                                                                
