      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/09/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  MGD71.                                                      
       AUTHOR. BITTON M.                                                        
      *       MODULE TP D'ORDONNANCEMENT DES EMPLACEMENTS RACK                  
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
      *}                                                                        
                                                                                
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
       DATA DIVISION.                                                           
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-I                         PIC S9(4)     COMP.                      
      *--                                                                       
       77  W-I                         PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-J                         PIC S9(4)     COMP.                      
      *--                                                                       
       77  W-J                         PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-K                         PIC S9(4)     COMP.                      
      *--                                                                       
       77  W-K                         PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-L                         PIC S9(4)     COMP.                      
      *--                                                                       
       77  W-L                         PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-F                         PIC S9(4)     COMP.                      
      *--                                                                       
       77  W-F                         PIC S9(4) COMP-5.                        
      *}                                                                        
       77  W-SAVE                      PIC X(10).                               
       77  W-VAL1                      PIC 9(2).                                
       77  W-VAL2                      PIC 9(2).                                
      *                                                                         
       01  W-COURS                     PIC X(2).                                
       01  N-COURS  REDEFINES W-COURS  PIC 9(2).                                
       01  W-DEBUT                     PIC X(2).                                
       01  N-DEBUT  REDEFINES W-DEBUT  PIC 9(2).                                
       01  W-FIN                       PIC X(2).                                
       01  N-FIN    REDEFINES W-FIN    PIC 9(2).                                
      *                                                                         
       01  WI-RUPT.                                                             
           05  WI-NPOSITION            PIC X(3).                                
           05  WI-CNIVEAU              PIC X(2).                                
           05  WI-NPRIORITE            PIC X(1).                                
      *                                                                         
       01  WJ-RUPT.                                                             
           05  WJ-NPOSITION            PIC X(3).                                
           05  WJ-CNIVEAU              PIC X(2).                                
           05  WJ-NPRIORITE            PIC X(1).                                
      *                                                                         
       01  T2-TABLEGEN.                                                         
           05  T2-TABLE    OCCURS  200.                                         
               10  T2-CALLEE           PIC X(2).                                
               10  T2-CNIVEAU          PIC X(2).                                
               10  FILLER              PIC X(4).                                
      *                                                                         
      ***************************************************************           
      *         L I N K A G E    S E C T I O N                      *           
      ***************************************************************           
      *                                                                         
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05 COMM-GD71-COURS                      PIC  X(2).                   
           05 COMM-GD71-COMM.                                                   
               10 COMM-GD71-TABLE    OCCURS  200.                               
                  15 COMM-GD71-CALLEE              PIC  X(2).                   
                  15 COMM-GD71-CNIVEAU             PIC  X(2).                   
                  15 COMM-GD71-WRUPT.                                           
                     20 COMM-GD71-NPOSITION        PIC  X(3).                   
                     20 COMM-GD71-NPRIORITE        PIC  X(1).                   
               10 FILLER                           PIC  X(98).                  
1     *                                                                         
      ***************************************************************           
      *            P   R   O   C   E   D   U   R   E                *           
      ***************************************************************           
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      ***************************************************************           
      *     TRI  DE  LA  TABLE  PAR  NPOSITION  ET  CNIVEAU         *           
      ***************************************************************           
      *                                                                         
           PERFORM  VARYING W-I  FROM  1  BY 1  UNTIL W-I > 199                 
                                 OR  COMM-GD71-CALLEE (W-I) = SPACES            
            PERFORM  VARYING W-J  FROM  W-I  BY 1  UNTIL W-J > 200              
                                    OR  COMM-GD71-CALLEE (W-J) = SPACES         
      *                                                                         
               MOVE COMM-GD71-CNIVEAU   (W-I)   TO  WI-CNIVEAU                  
               MOVE COMM-GD71-CNIVEAU   (W-J)   TO  WJ-CNIVEAU                  
               MOVE COMM-GD71-NPOSITION (W-I)   TO  WI-NPOSITION                
               MOVE COMM-GD71-NPOSITION (W-J)   TO  WJ-NPOSITION                
               MOVE COMM-GD71-NPRIORITE (W-I)   TO  WI-NPRIORITE                
               MOVE COMM-GD71-NPRIORITE (W-J)   TO  WJ-NPRIORITE                
               MOVE W-J                         TO  W-F                         
      *                                                                         
               IF WI-RUPT  >  WJ-RUPT                                           
                   MOVE  COMM-GD71-TABLE (W-I)  TO W-SAVE                       
                   MOVE  COMM-GD71-TABLE (W-J)  TO COMM-GD71-TABLE (W-I)        
                   MOVE  W-SAVE                 TO COMM-GD71-TABLE (W-J)        
               END-IF                                                           
      *                                                                         
             END-PERFORM                                                        
      *                                                                         
           END-PERFORM.                                                         
1     *                                                                         
      ***************************************************************           
      *    DETERMINATION  DU  DEBUT  DU  DESTOCKAGE                 *           
      ***************************************************************           
      *                                                                         
           MOVE   COMM-GD71-COURS            TO   W-COURS.                      
           MOVE   COMM-GD71-CNIVEAU (1)      TO   W-DEBUT.                      
           MOVE   COMM-GD71-CNIVEAU (W-F)    TO   W-FIN.                        
           IF  W-COURS IS NUMERIC   AND   W-DEBUT IS NUMERIC   AND              
                             W-FIN IS NUMERIC                                   
               COMPUTE  W-VAL1  =  N-DEBUT - N-COURS                            
               COMPUTE  W-VAL2  =  N-FIN   - N-COURS                            
               IF W-VAL1   >  W-VAL2                                            
                   PERFORM  INVERSEMENT-COMMAREA                                
           END-IF.                                                              
      *                                                                         
           MOVE   COMM-GD71-CNIVEAU (W-F)  TO   COMM-GD71-COURS.                
      *                                                                         
      * RAJOUT SM LE 070792 SI DEUX EMPLACEMENTS DE MEME NIVEAU                 
      * IL FAUT METTRE EN PREMIER DE PREFERENCE CELUI DE MEME ALLEE             
      * QUE L' EMPLACEMENT PRECEDANT CES DEUX EMPLACEMENTS                      
      *                                                                         
           PERFORM VARYING W-I FROM 1 BY 1 UNTIL W-I > W-F                      
              IF W-I < W-F                                                      
                 ADD 1 W-I GIVING W-J                                           
                 PERFORM VARYING W-K FROM W-J BY 1 UNTIL W-K > W-F OR           
                   COMM-GD71-CNIVEAU (W-K) NOT = COMM-GD71-CNIVEAU (W-I)        
                 END-PERFORM                                                    
                 IF W-K < W-F                                                   
                    ADD 1 W-K GIVING W-L                                        
                    IF COMM-GD71-CNIVEAU (W-L) = COMM-GD71-CNIVEAU (W-K)        
                       IF COMM-GD71-CALLEE (W-L) = COMM-GD71-CALLEE(W-I)        
                          AND COMM-GD71-CALLEE (W-K) NOT =                      
                              COMM-GD71-CALLEE (W-I)                            
                          MOVE COMM-GD71-TABLE (W-K) TO W-SAVE                  
                          MOVE COMM-GD71-TABLE (W-L) TO                         
                               COMM-GD71-TABLE (W-K)                            
                          MOVE W-SAVE TO COMM-GD71-TABLE (W-L)                  
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *                                                                         
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC   CICS   RETURN   END-EXEC.                                     
      *--                                                                       
           EXEC   CICS   RETURN                                                 
           END-EXEC.                                                            
      *}                                                                        
           GOBACK.                                                              
1     *                                                                         
      ***************************************************************           
      *    CHARGEMENT DE LA TABLE INTERNE SI DEBUT EN HAUT          *           
      ***************************************************************           
      *                                                                         
       INVERSEMENT-COMMAREA.                                                    
      *                                                                         
           MOVE   COMM-GD71-COMM    TO   T2-TABLEGEN.                           
           MOVE   1                 TO   W-J.                                   
           PERFORM      VARYING  W-I  FROM  W-F BY -1 UNTIL W-I NOT > 0         
               MOVE  T2-TABLE  (W-I)   TO  COMM-GD71-TABLE (W-J)                
               ADD   1                 TO  W-J                                  
           END-PERFORM.                                                         
      *                                                                         
