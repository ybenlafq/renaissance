      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     MGQ98.                                   
       AUTHOR. GILLES E.                                                        
      *                                                                         
      *   MODULE DE DECLENCHEMENT DU PROGRAMME DE COMMUNICATION APPC            
      *   TGX02 (MODULE DECLANCH� PAR MGQ99)                                    
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
       01  TS-LIEU.                                                             
           03 TSL-APPLID              PIC  X(8).                                
           03 TSL-LIEU                PIC  X(6).                                
           03 TSL-TERM                PIC  X(4).                                
           03 TSL-REMOTESYST          PIC  X(4).                                
           03 TSL-OLD-NEW             PIC  X(1).                                
           03 FILLER                  PIC  X(4073).                             
       01  CTR                     PIC 9 VALUE 0.                               
           COPY  SYKWZINO.                                                      
           COPY  SYKWDIV0.                                                      
           COPY  SYKWERRO.                                                      
           COPY  SYKWEIB0.                                                      
           COPY  COMMDATC.                                                      
       01  Z-COMMAREA.                                                  00260000
           02 COMM-CICS-APPLID     PIC X(8).                            00320000
           02 COMM-GQ99-TS         PIC X(8).                            00320000
           02 COMM-GQ99-ITEM-LIEU  PIC 9(3).                            00320000
           02 COMM-GQ99-DATA-LIEU  PIC X(23).                           00320000
           02 COMM-GQ99-PROC-BIBL  PIC X(17).                           00320000
           COPY  SYKWSTAR.                                                      
       01  Z-COMMAREA-LINK         PIC X(200).                          00260000
       01  ZONE-LINK.                                                           
           02 LINK-NOM-TS          PIC X(8).                            00320000
           02 LINK-PROC-BIBL       PIC X(17).                                   
           02 LINK-LIEU            PIC X(6).                                    
           02 LINK-TERM            PIC X(4).                                    
           02 LINK-REMOTESYST      PIC X(4).                                    
           02 LINK-RET             PIC 9(1).                            00320000
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05  FILLER PIC X(200).                                               
      ************************                                                  
       PROCEDURE DIVISION.                                                      
      ************************                                                  
       MODULE-GQ98                    SECTION.                                  
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
       F-MODULE-GQ98.    EXIT.                                                  
      *-----------------------------------------------------------------        
      *                          ENTREE                                         
      *-----------------------------------------------------------------        
       MODULE-ENTREE              SECTION.                                      
           MOVE DFHCOMMAREA TO ZONE-LINK                                        
           EXEC CICS RETRIEVE INTO  (Z-COMMAREA)                                
                              NOHANDLE                                          
           END-EXEC.                                                            
           MOVE EIBRCODE TO EIB-RCODE                                           
           IF NOT EIB-NORMAL                                                    
              PERFORM ABANDON-TACHE                                             
           END-IF.                                                              
           MOVE 1 TO LINK-RET.                                                  
           INITIALIZE TS-LIEU.                                                  
           MOVE COMM-GQ99-DATA-LIEU  TO  TS-LIEU.                               
       F-MODULE-ENTREE.   EXIT.                                                 
      *-----------------------------------------------------------------        
      *                       TRAITEMENT                                        
      *-----------------------------------------------------------------        
       MODULE-TRAITEMENT          SECTION.                                      
      *    PERFORM LINK-COMMUNICATION UNTIL (LINK-RET = 0) OR (CTR > 5)         
           PERFORM LINK-COMMUNICATION                                           
           IF LINK-RET = 1                                                      
              PERFORM ABANDON-TACHE                                             
           END-IF                                                               
           MOVE 4096                 TO  LONG-TS                                
           MOVE 'TSGX01'             TO  IDENT-TS                               
           MOVE COMM-GQ99-ITEM-LIEU  TO  RANG-TS                                
           IF LINK-RET = 0  AND  TSL-OLD-NEW = 'N'                              
              MOVE 'O'     TO  TSL-OLD-NEW                                      
              MOVE TS-LIEU TO  Z-INOUT                                          
              PERFORM REWRITE-TS                                                
           END-IF.                                                              
       F-MODULE-TRAITEMENT. EXIT.                                               
      *-----------------------------------------------------------------        
      *              LINK AU PROGRAMME DE COMMUNICATION TGX02                   
      *-----------------------------------------------------------------        
       LINK-COMMUNICATION  SECTION.                                             
           ADD    1             TO CTR                                          
           MOVE COMM-GQ99-TS         TO LINK-NOM-TS                             
           MOVE COMM-GQ99-PROC-BIBL  TO LINK-PROC-BIBL                          
           MOVE TSL-LIEU             TO LINK-LIEU                               
           MOVE TSL-TERM             TO LINK-TERM                               
           MOVE TSL-REMOTESYST       TO LINK-REMOTESYST                         
           MOVE 39                   TO LONG-COMMAREA-LINK              26      
           MOVE ZONE-LINK            TO Z-COMMAREA-LINK                 26      
           MOVE 'TGX02'              TO NOM-PROG-LINK                   26      
           PERFORM LINK-PROG                                            26      
           MOVE Z-COMMAREA-LINK TO ZONE-LINK.                           26      
       F-LINK-COMMUNICATION.  EXIT.                                             
      *-----------------------------------------------------------------        
      *                          SORTIE                                         
      *-----------------------------------------------------------------        
       MODULE-SORTIE              SECTION.                                      
           PERFORM RETOUR-PROGRAMME.                                            
       F-MODULE-SORTIE. EXIT.                                                   
      *                                                                         
       RETOUR-PROGRAMME             SECTION.                                    
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
       FIN-RETOUR-PROGRAMME. EXIT.                                              
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *        DESCRIPTION DES BRIQUES AIDA SQL           * SYKS....  *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSDE.                            
       TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRD.                            
       TEMP03-COPY SECTION. CONTINUE. COPY SYKCTSRW.                            
       TEMP04-COPY SECTION. CONTINUE. COPY SYKCTSWR.                            
       S15-COPY    SECTION. CONTINUE. COPY SYKCLINK.                            
      *                                                                         
       ABANDON-TACHE              SECTION.                                      
           PERFORM RETOUR-PROGRAMME.                                            
       F-ABANDON-TACHE.      EXIT.                                              
      *                                                                         
       ABANDON-CICS               SECTION.                                      
           PERFORM RETOUR-PROGRAMME.                                            
       F-ABANDON-CICS.     EXIT.                                                
