#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE54O.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGIE54 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/03/20 AT 01.20.12 BY OPERAT1                      
#    STANDARDS: P  JOBSET: GIE54O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   LE PASSAGE DE LA CHAINE DOIT SE FAIRE APRES QUE LE DEPOT EST FINI          
#                    SON INVENTAIRE                                            
# ********************************************************************         
#  BIE540 : CREATION DU FICHIER DES ECARTS D'INVENTAIRE ENTREPOT               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GIE54OA
       ;;
(GIE54OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GIE54OAA
       ;;
(GIE54OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGA00M  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
#    RSGA11M  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
#    RSIE05M  : NAME=RSIE05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05M /dev/null
#    RSIE60M  : NAME=RSIE60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60M /dev/null
#    RSIE10M  : NAME=RSIE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10M /dev/null
#    RSGG50M  : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50M /dev/null
#    RSGG55M  : NAME=RSGG55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55M /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIE540 ${DATA}/PGI916/F16.BIE540AO
# ******  PARAMETRE SOCIETE = 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE540 
       JUMP_LABEL=GIE54OAB
       ;;
(GIE54OAB)
       m_CondExec 04,GE,GIE54OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OAD
       ;;
(GIE54OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI916/F16.BIE540AO
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GIE54OAD.BIE540DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54OAE
       ;;
(GIE54OAE)
       m_CondExec 00,EQ,GIE54OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135 : EDITION DES ECARTS D'INVENTAIRE PAR FAMILLE LIEU S/LIEU            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OAG
       ;;
(GIE54OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  FICHIER DES ECARTS                                                   
       m_FileAssign -d SHR -g ${G_A2} FIV130 ${DATA}/PTEM/GIE54OAD.BIE540DO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE (IIV135)                       
       m_OutputAssign -c 9 -w IIV135 IIV135
       m_OutputAssign -c Z IIV135B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=GIE54OAH
       ;;
(GIE54OAH)
       m_CondExec 04,GE,GIE54OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OAJ
       ;;
(GIE54OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PGI916/F16.BIE540AO
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GIE54OAJ.BIE540BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 0
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /CONDITION CND_1 FLD_PD_85_3 EQ CST_1_11 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54OAK
       ;;
(GIE54OAK)
       m_CondExec 00,EQ,GIE54OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140   EDITION DES ECARTS D'INVENTAIRE PAR LIEU S/LIEU                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OAM
       ;;
(GIE54OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  FICHIER DES ECARTS TRIE                                              
       m_FileAssign -d SHR -g ${G_A4} FIV140 ${DATA}/PTEM/GIE54OAJ.BIE540BO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE (IIV140)                       
       m_OutputAssign -c Z IIV140
       m_OutputAssign -c Z IIV140A
       m_OutputAssign -c Z IIV140M
# ******  ETAT DES ECARTS D INVENTAIRE NON VALORISE (IIV140O)                  
       m_OutputAssign -c 9 -w IIV140O IIV140O
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=GIE54OAN
       ;;
(GIE54OAN)
       m_CondExec 04,GE,GIE54OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS DEPOTS                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OAQ
       ;;
(GIE54OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PGI916/F16.BIE540AO
# ******  FICHIERS DES ECARTS MAGASINS TRIE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GIE54OAQ.BIE540FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_3 65 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54OAR
       ;;
(GIE54OAR)
       m_CondExec 00,EQ,GIE54OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144  : EDITION DES ECARTS SOCIETE PAR RAYON                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OAT
       ;;
(GIE54OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A6} FIV144 ${DATA}/PTEM/GIE54OAQ.BIE540FO
# ******  EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                        
       m_OutputAssign -c 9 -w IIV144 IIV144
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=GIE54OAU
       ;;
(GIE54OAU)
       m_CondExec 04,GE,GIE54OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GIE54OZA
       ;;
(GIE54OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE54OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
