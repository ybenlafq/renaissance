#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS903P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS903 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/08 AT 16.51.15 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GS903P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGS222 : EDITION DES MVTS DE STOCKS HORS VENTE                              
#  CRITERE: DMVT <= "INVDA" (SOUS TABLE)                                       
#                    (DEFAUT = 2 MOIS)                                         
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS903PA
       ;;
(GS903PA)
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       C_A3=${C_A3:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS903PAA
       ;;
(GS903PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE GENERALISEE: SOUS TABLE INVDA                                  
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ARTICLE                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  HISTO DES MOUVEMENTS DE STOCK                                        
#    RSGS41   : NAME=RSGS41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS41 /dev/null
#                                                                              
# ******  FICHIER DATE JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  EDITION POUR LES MICRO FICHES                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IGS2221 ${DATA}/PXX0.FICHEML.GS901P.BGS222
       m_OutputAssign -c 9 -w IGS2222 IGS2222
       m_OutputAssign -c 9 -w IGS2223 IGS2223
       m_OutputAssign -c 9 -w IGS2224 IGS2224
       m_OutputAssign -c 9 -w IGS2225 IGS2225
       m_OutputAssign -c 9 -w IGS2226 IGS2226
       m_OutputAssign -c 9 -w IGS2227 IGS2227
       m_OutputAssign -c 9 -w IGS2228 IGS2228
       m_OutputAssign -c 9 -w IGS2229 IGS2229
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS222 
       JUMP_LABEL=GS903PAB
       ;;
(GS903PAB)
       m_CondExec 04,GE,GS903PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS223 : EDITION DES MVTS DE VENTES ET REPRISES CLIENTS                     
#  CRITERE: DMVT <= "INVDA" (SOUS TABLE)                                       
#                    (DEFAUT = 2 MOIS)                                         
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAD
       ;;
(GS903PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE GENERALISEE: SOUS TABLE INVDA                                  
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ARTICLE                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  HISTO DES MOUVEMENTS DE STOCK                                        
#    RSGS41   : NAME=RSGS41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS41 /dev/null
#                                                                              
# ******  FICHIER DATE JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  EDITION EN KSET POUR LES MICRO FICHES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IGS2231 ${DATA}/PXX0.FICHEML.GS901P.BGS223
       m_OutputAssign -c 9 -w IGS2232 IGS2232
       m_OutputAssign -c 9 -w IGS2233 IGS2233
       m_OutputAssign -c 9 -w IGS2234 IGS2234
       m_OutputAssign -c 9 -w IGS2235 IGS2235
       m_OutputAssign -c 9 -w IGS2236 IGS2236
       m_OutputAssign -c 9 -w IGS2237 IGS2237
       m_OutputAssign -c 9 -w IGS2238 IGS2238
       m_OutputAssign -c 9 -w IGS2239 IGS2239
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS223 
       JUMP_LABEL=GS903PAE
       ;;
(GS903PAE)
       m_CondExec 04,GE,GS903PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTGS41                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAG
       ;;
(GS903PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
#  TABLE DES VENTES DETAIL                                                     
#    RSGS41   : NAME=RSGS41,MODE=I - DYNAM=YES                                 
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/GS903PAG.BGS221AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BGS221 : EPURATION DE L'HISTO  DES MOUVEMENTS DE STOCK RTGS41               
#  CRITERE: DMVT <= "INVDA" (SOUS TABLE)                                       
#                    (DEFAUT = 2 MOIS)                                         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAJ
       ;;
(GS903PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE GENERALISEE: SOUS TABLE INVDA                                  
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******  FICHIER DATE JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FTGS41 ${DATA}/PTEM/GS903PAG.BGS221AP
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 FTGS41O ${DATA}/PTEM/GS903PAJ.BGS221ZP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS221 
       JUMP_LABEL=GS903PAK
       ;;
(GS903PAK)
       m_CondExec 04,GE,GS903PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PROGRAM(BGS221)                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAM
       ;;
(GS903PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GS903PAJ.BGS221ZP
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS903PAM.BGS221BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS903PAN
       ;;
(GS903PAN)
       m_CondExec 00,EQ,GS903PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD REPLACE DE LA TABLE RTGS41                                            
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAQ
       ;;
(GS903PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PTEM/GS903PAM.BGS221BP
#    RSGS41   : NAME=RSGS41,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS41 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GS903P_GS903PAQ_RTGS41.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GS903PAR
       ;;
(GS903PAR)
       m_CondExec 04,GE,GS903PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY BGS222                                           
# ********************************************************************         
# ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CDROM133,                                                           
#      FNAME=":BGS222"",                                                       
#      NFNAME=GS903222                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY BGS223                                           
# ********************************************************************         
# ABJ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CDROM133,                                                           
#      FNAME=":BGS223"",                                                       
#      NFNAME=GS903223                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT PAR CFT BL MICROLIST                                              
# ********************************************************************         
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  CLASS=VAR,PARMS=GS903P                                        
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGS903P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAT
       ;;
(GS903PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PAT.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGS903P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS903PAX
       ;;
(GS903PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PAX.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** PREPARATION DU SEND FTP DU BL MICROFICHE          ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GS903PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS903PBA
       ;;
(GS903PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GS903PBA.FTGS903P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU BORDEREAU DE LAISON GS903PA_DU                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS903PBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS903PBD
       ;;
(GS903PBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PBD.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GS903PBA.FTGS903P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS903PZA
       ;;
(GS903PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS903PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
