#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA420L.ksh                       --- VERSION DU 08/10/2016 13:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLVA420 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 02/11/07 AT 17.14.46 BY BURTECC                      
#    STANDARDS: P  JOBSET: VA420L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  REPROS DES FICHIERS BVA511AL ET BVA520AL                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA420LA
       ;;
(VA420LA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2002/11/07 AT 17.14.46 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: VA420L                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'EXTRAC COMPTA GCT'                     
# *                           APPL...: REPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA420LAA
       ;;
(VA420LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******  FIC MVTS DU MOIS POUR LES LIEUX 'AVO' ET RCY'                        
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PNCGL/F61.BVA511AL
# ******  FIC DES ACHATS TRIES PAR FOURNISSEUR ET RAYON                        
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PNCGL/F61.BVA520AL
# ******  FIC REPRO POUR FT420L                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 OUT1 ${DATA}/PNCGL/F61.BVA511ZL
# ******  FIC REPRO POUR FT420L                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 OUT2 ${DATA}/PNCGL/F61.BVA520ZL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA420LAA.sysin
       m_UtilityExec
# **************************                                                   
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VA420LAB
       ;;
(VA420LAB)
       m_CondExec 16,NE,VA420LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
