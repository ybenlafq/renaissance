#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE51P.ksh                       --- VERSION DU 08/10/2016 22:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGIE51 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/09/22 AT 10.24.24 BY BURTECA                      
#    STANDARDS: P  JOBSET: GIE51P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE51PA
       ;;
(GIE51PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2008/09/22 AT 10.24.24 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: GIE51P                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK/ECARTS'                      
# *                           APPL...: IMPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE51PAA
       ;;
(GIE51PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGIE51P
#    RSIE10   : NAME=RSIE10,MODE=(U,N) - DYNAM=YES                             
# -X-RSIE10   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSIE10 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,N) - DYNAM=YES                             
# -X-RSGS10   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=(U,N) - DYNAM=YES                             
# -X-RSGS15   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSGS70   : NAME=RSGS70,MODE=(U,N) - DYNAM=YES                             
# -X-RSGS70   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGS70 /dev/null
#    RSGS60   : NAME=RSGS60,MODE=(U,N) - DYNAM=YES                             
# -X-RSGS60   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGS60 /dev/null
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GIE51PAA
       m_ProgramExec IEFBR14 "RDAR,GIE51P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51PAD
       ;;
(GIE51PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#    RSIE05   : NAME=RSIE05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE05 /dev/null
#    RSGS60   : NAME=RSGS60,MODE=(U,N) - DYNAM=YES                             
# -X-GIE51PR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGS60 /dev/null
#    RSGS70   : NAME=RSGS70,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS70 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSIE10   : NAME=RSIE10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSIE10 /dev/null
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
# -X-RSAN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS A PARIS                             
       m_FileAssign -d SHR FIIE515 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE515 
       JUMP_LABEL=GIE51PAE
       ;;
(GIE51PAE)
       m_CondExec 04,GE,GIE51PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE516                                                                      
#  REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS                        
#  REPRISE: SURTOUT PAS CAR COMMIT RECOVER TO RBA                              
#         : DES TABLES RTGS10 RTGS15 RTGS60 RTGS70                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE51PAG
       ;;
(GIE51PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#    RSIE60   : NAME=RSIE60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE60 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGS60   : NAME=RSGS60,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS60 /dev/null
#    RSGS70   : NAME=RSGS70,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS70 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
# -X-RSAN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS A PARIS                             
       m_FileAssign -d SHR FIIE516 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE516 
       JUMP_LABEL=GIE51PAH
       ;;
(GIE51PAH)
       m_CondExec 04,GE,GIE51PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
