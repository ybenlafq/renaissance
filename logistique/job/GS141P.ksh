#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS141P.ksh                       --- VERSION DU 08/10/2016 17:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS141 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/23 AT 14.04.52 BY OPERATB                      
#    STANDARDS: P  JOBSET: GS141P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BGS140                                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS141PA
       ;;
(GS141PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS141PAA
       ;;
(GS141PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10 /dev/null
#    RTGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS36 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
#                                                                              
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 FGS140A ${DATA}/PTEM/GS141PAA.BGS140AP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 FGS140B ${DATA}/PTEM/GS141PAA.BGS140BP
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS140 
       JUMP_LABEL=GS141PAB
       ;;
(GS141PAB)
       m_CondExec 04,GE,GS141PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI/FUSION DES FICHIERS FGS140A ET FGS140B                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS141PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS141PAD
       ;;
(GS141PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS141PAA.BGS140AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GS141PAA.BGS140BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BGS140CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_9_20  ";"
 /DERIVEDFIELD CST_1_12  ";"
 /DERIVEDFIELD CST_3_14  ";"
 /DERIVEDFIELD CST_7_18  ";"
 /DERIVEDFIELD CST_5_16  ";"
 /FIELDS FLD_CH_45_3 45 CH 3
 /FIELDS FLD_CH_48_3 48 CH 3
 /FIELDS FLD_CH_54_3 54 CH 3
 /FIELDS FLD_CH_63_3 63 CH 3
 /FIELDS FLD_CH_51_3 51 CH 3
 /FIELDS FLD_CH_39_3 39 CH 3
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_42_3 42 CH 3
 /FIELDS FLD_CH_16_20 16 CH 20
 /FIELDS FLD_CH_60_3 60 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_57_3 57 CH 3
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_39_3,
    TOTAL FLD_CH_42_3,
    TOTAL FLD_CH_45_3,
    TOTAL FLD_CH_48_3,
    TOTAL FLD_CH_51_3,
    TOTAL FLD_CH_54_3,
    TOTAL FLD_CH_57_3,
    TOTAL FLD_CH_60_3,
    TOTAL FLD_CH_63_3
 /* MT_ERROR (unknown field) (1,3,C';',4,7,C';',11,5,C';',16,20,C';',36,3,C';' */
 /* Record Type = F  Record Length = 120 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,CST_1_12,FLD_CH_4_7,CST_3_14,FLD_CH_11_5,CST_5_16,FLD_CH_16_20,CST_7_18,FLD_CH_36_3,CST_9_20
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS141PAE
       ;;
(GS141PAE)
       m_CondExec 00,EQ,GS141PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER FGS140C                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# *                                                                            
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BGS140CP,                                                           
#      FNAME=":BGS140CP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  CHANGEMENT PCL => GS141P <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGS141P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS141PAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS141PAG
       ;;
(GS141PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS141PAG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GS141PAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS141PAJ
       ;;
(GS141PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS141PAJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS141PZA
       ;;
(GS141PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS141PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
