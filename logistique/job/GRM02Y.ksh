#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM02Y.ksh                       --- VERSION DU 08/10/2016 23:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGRM02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/03 AT 15.37.59 BY BURTECN                      
#    STANDARDS: P  JOBSET: GRM02Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BRM000 : EXTRACTION DES DONNEES ARTICLE-FAMILLE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GRM02YA
       ;;
(GRM02YA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2004/03/03 AT 15.37.59 BY BURTECN                
# *    JOBSET INFORMATION:    NAME...: GRM02Y                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'REAPPRO MAG'                           
# *                           APPL...: IMPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GRM02YAA
       ;;
(GRM02YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSGA09   : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
# ******* LIEUX                                                                
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* RELATION ETAT / FAMILLE RAYON                                        
#    RSGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******* RELATION FAMILLE / CODE MARKETING                                    
#    RSGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* LIBELLES IMBRICATIONS DES CODES MARKETING                            
#    RSGA29   : NAME=RSGA29Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
# ******* RELATION ARTICLE MODES DE DELIVRANCES                                
#    RSGA64   : NAME=RSGA64Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# ******* RELATION ARTICLE RAYON CODE MARKETING                                
#    RSGA83   : NAME=RSGA83Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83 /dev/null
#                                                                              
# ******* SOCIETE = 945                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE EDITION= 'IMU205'                                          
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/GRM02YAA
#                                                                              
# ******* DONNEES DESTINEES AU LOAD RTRM60                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -g +1 RTRM60 ${DATA}/PXX0/F45.GRM002BY
# ******* DONNEES DESTINEES AU LOAD RTRM65                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 RTRM65 ${DATA}/PXX0/F45.GRM002CY
#                                                                              
# ******* DONNEES MUTATIONS                                                    
       m_FileAssign -d SHR FMU235 /dev/null
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d SHR FRMCOM /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM000 
       JUMP_LABEL=GRM02YAB
       ;;
(GRM02YAB)
       m_CondExec 04,GE,GRM02YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
