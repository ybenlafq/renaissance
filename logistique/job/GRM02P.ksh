#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM02P.ksh                       --- VERSION DU 08/10/2016 13:55
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGRM02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/28 AT 09.40.54 BY BURTECA                      
#    STANDARDS: P  JOBSET: GRM02P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRM000 : EXTRACTION DES DONNEES ARTICLE-FAMILLE                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM02PA
       ;;
(GRM02PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRM02PAA
       ;;
(GRM02PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ----------------------                                                      
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA64   : NAME=RSGA64,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA64 /dev/null
#    RSGA83   : NAME=RSGA83,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA83 /dev/null
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE EDITION= 'IMU205'                                          
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/GRM02PAA
# ******* DONNEES DESTINEES AU LOAD RTRM60                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -g +1 RTRM60 ${DATA}/PXX0/F07.GRM002BP
# ******* DONNEES DESTINEES AU LOAD RTRM65                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 RTRM65 ${DATA}/PXX0/F07.GRM002CP
# ******* DONNEES MUTATIONS                                                    
       m_FileAssign -d SHR FMU235 /dev/null
# ******* FIC ARTICLE ET FAMILLE                                               
       m_FileAssign -d SHR FRMCOM /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM000 
       JUMP_LABEL=GRM02PAB
       ;;
(GRM02PAB)
       m_CondExec 04,GE,GRM02PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS GRM002B POUR LOADER RTRM60                                 
#  (FICHIERS ISSUS DU BRM000)                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAD
       ;;
(GRM02PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.GRM002BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GRM002BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.GRM002BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GRM002BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GRM002BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.GRM002BY
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.GRM002BX
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.RM60RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_29_7 29 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_29_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PAE
       ;;
(GRM02PAE)
       m_CondExec 00,EQ,GRM02PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS GRM002C POUR LOADER RTRM65                                 
#  (FICHIERS ISSUS DU BRM000)                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAG
       ;;
(GRM02PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.GRM002CP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.GRM002CD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.GRM002CL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.GRM002CM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GRM002CO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.GRM002CY
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.GRM002CX
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.RM65RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_28 1 CH 28
 /KEYS
   FLD_CH_1_28 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PAH
       ;;
(GRM02PAH)
       m_CondExec 00,EQ,GRM02PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD SUR RTRM60 : LIBELLE AGREGAT PAR CODIC                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAJ
       ;;
(GRM02PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F07.RELOAD.RM60RP
# ******* TABLE RTRM60                                                         
#    RSRM60   : NAME=RSRM60,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSRM60 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM02PAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM02P_GRM02PAJ_RTRM60.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM02PAK
       ;;
(GRM02PAK)
       m_CondExec 04,GE,GRM02PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD SUR RTRM65 : LIBELLE DE SEGMENT DE REAPPRO                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAM
       ;;
(GRM02PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F07.RELOAD.RM65RP
# ******* TABLE RTRM65                                                         
#    RSRM65   : NAME=RSRM65,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSRM65 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM02PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM02P_GRM02PAM_RTRM65.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM02PAN
       ;;
(GRM02PAN)
       m_CondExec 04,GE,GRM02PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM005 : EXTRACTION DES VENTES DES 4 DERNIERES SEMAINES                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAQ
       ;;
(GRM02PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM60   : NAME=RSRM60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM60 /dev/null
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
# ******* TABLES EN MAJ                                                        
#    RSRM58   : NAME=RSRM58,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSRM58 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES VENTES DES 8 DERNIERES SEMAINES ISSU DE GM065P               
       m_FileAssign -d SHR -g +0 FVENTES ${DATA}/PXX0/F07.BGM065FP
# ******* FIC DETAILS DES VENTES 4 SEMAINES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGROUPE ${DATA}/PTEM/GRM02PAQ.BRM005AP
# ******* SERT A LOADER RTRM95                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 RTRM95 ${DATA}/PTEM/GRM02PAQ.BRM005BP
# ******* FIC VENTE ENRICHI (GROUPE DE MAG , LIBELLE AGREGAT ,FAMILLE)         
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FMAGASIN ${DATA}/PTEM/GRM02PAQ.BRM005CP
# ******* FICHIER POUR ETAT ANOMALIE SEGMENTATION MARKETING (GRM05P)           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IRM005 ${DATA}/PXX0/F07.BRM005GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM005 
       JUMP_LABEL=GRM02PAR
       ;;
(GRM02PAR)
       m_CondExec 04,GE,GRM02PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BRM010                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAT
       ;;
(GRM02PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GRM02PAQ.BRM005AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PAT.BRM010AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_37 4 CH 37
 /FIELDS FLD_PD_41_4 41 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_45_4 45 PD 4
 /FIELDS FLD_PD_49_4 49 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_37 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_4,
    TOTAL FLD_PD_45_4,
    TOTAL FLD_PD_49_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PAU
       ;;
(GRM02PAU)
       m_CondExec 00,EQ,GRM02PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BRM010                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PAX
       ;;
(GRM02PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GRM02PAQ.BRM005AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PAX.BRM010BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_30 4 CH 30
 /FIELDS FLD_PD_41_4 41 PD 4
 /FIELDS FLD_PD_49_4 49 PD 4
 /FIELDS FLD_PD_45_4 45 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_30 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_4,
    TOTAL FLD_PD_45_4,
    TOTAL FLD_PD_49_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PAY
       ;;
(GRM02PAY)
       m_CondExec 00,EQ,GRM02PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM010 : PREVISION DE LA QUANTITE HEBDO DE REAPPRO                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBA
       ;;
(GRM02PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSRM70   : NAME=RSRM70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM70 /dev/null
#    RSRM75   : NAME=RSRM75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM75 /dev/null
#    RSRM80   : NAME=RSRM80,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM80 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC1 TRI� DES VENTES DES 4 DERNIERES SEMAINES                        
       m_FileAssign -d SHR -g ${G_A7} FGROUPE1 ${DATA}/PTEM/GRM02PAT.BRM010AP
# ******* FIC2 TRI� DES VENTES DES 4 DERNIERES SEMAINES                        
       m_FileAssign -d SHR -g ${G_A8} FGROUPE2 ${DATA}/PTEM/GRM02PAX.BRM010BP
# ******* VENTES PREVISIONNELLES                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FGROUPE ${DATA}/PTEM/GRM02PBA.BRM010CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM010 
       JUMP_LABEL=GRM02PBB
       ;;
(GRM02PBB)
       m_CondExec 04,GE,GRM02PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI    FIC DE LOAD RTRM95                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBD
       ;;
(GRM02PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GRM02PAQ.BRM005BP
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.RM95RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_49 1 CH 49
 /KEYS
   FLD_CH_1_49 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PBE
       ;;
(GRM02PBE)
       m_CondExec 00,EQ,GRM02PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD SUR RTRM95 : REAPPRO MAG ET VENTES 8 DERNIERES SEMAINES                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBG
       ;;
(GRM02PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* TABLE RTRM95                                                         
#    RSRM95   : NAME=RSRM95,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSRM95 /dev/null
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A10} SYSREC ${DATA}/PXX0/F07.RELOAD.RM95RP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM02PBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM02P_GRM02PBG_RTRM95.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM02PBH
       ;;
(GRM02PBH)
       m_CondExec 04,GE,GRM02PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM220 : MAJ DE LA TABLE RTRM70                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBJ
       ;;
(GRM02PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PLAN DE MARCHE DE REAPPRO                                            
#    RSRM70   : NAME=RSRM70,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM70 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM220 
       JUMP_LABEL=GRM02PBK
       ;;
(GRM02PBK)
       m_CondExec 04,GE,GRM02PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BMU240                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBM
       ;;
(GRM02PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PBM.FMU240AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_41_4 41 PD 4
 /FIELDS FLD_CH_4_30 4 CH 30
 /KEYS
   FLD_CH_4_30 ASCENDING,
   FLD_PD_41_4 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PBN
       ;;
(GRM02PBN)
       m_CondExec 00,EQ,GRM02PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BMU240                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBQ
       ;;
(GRM02PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PBQ.FMU240BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_30 4 CH 30
 /FIELDS FLD_PD_49_4 49 PD 4
 /KEYS
   FLD_CH_4_30 ASCENDING,
   FLD_PD_49_4 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PBR
       ;;
(GRM02PBR)
       m_CondExec 00,EQ,GRM02PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BMU240                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBT
       ;;
(GRM02PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PBT.FMU240CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_30 4 CH 30
 /FIELDS FLD_PD_53_4 53 PD 4
 /KEYS
   FLD_CH_4_30 ASCENDING,
   FLD_PD_53_4 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PBU
       ;;
(GRM02PBU)
       m_CondExec 00,EQ,GRM02PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BMU240                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PBX
       ;;
(GRM02PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PBX.FMU240DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_41_4 41 PD 4
 /FIELDS FLD_CH_4_30 4 CH 30
 /KEYS
   FLD_CH_4_30 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PBY
       ;;
(GRM02PBY)
       m_CondExec 00,EQ,GRM02PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BMU240                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCA
       ;;
(GRM02PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCA.FMU240EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_30 4 CH 30
 /FIELDS FLD_PD_49_4 49 PD 4
 /KEYS
   FLD_CH_4_30 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_49_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCB
       ;;
(GRM02PCB)
       m_CondExec 00,EQ,GRM02PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DETAILS DES VENTES 4 SEMAINES POUR BMU240                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCD
       ;;
(GRM02PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCD.FMU240FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_53_4 53 PD 4
 /FIELDS FLD_CH_4_30 4 CH 30
 /KEYS
   FLD_CH_4_30 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCE
       ;;
(GRM02PCE)
       m_CondExec 00,EQ,GRM02PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU240 : ENRICHISSEMENT DES FICHIERS SUR LE RANG PAR VALEUR MARKETI         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCG
       ;;
(GRM02PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS ISSUS DES TRIS PRECEDENTS                                   
       m_FileAssign -d SHR -g ${G_A17} FCQV4S ${DATA}/PTEM/GRM02PBM.FMU240AP
       m_FileAssign -d SHR -g ${G_A18} FSQV4S ${DATA}/PTEM/GRM02PBX.FMU240DP
       m_FileAssign -d SHR -g ${G_A19} FCQV4SER ${DATA}/PTEM/GRM02PBQ.FMU240BP
       m_FileAssign -d SHR -g ${G_A20} FSQV4SER ${DATA}/PTEM/GRM02PCA.FMU240EP
       m_FileAssign -d SHR -g ${G_A21} FCQPV ${DATA}/PTEM/GRM02PBT.FMU240CP
       m_FileAssign -d SHR -g ${G_A22} FSQPV ${DATA}/PTEM/GRM02PCD.FMU240FP
# ******  FICHIERS ENRICHIS                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRQV4S ${DATA}/PTEM/GRM02PCG.IMU240AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSR4S ${DATA}/PTEM/GRM02PCG.IMU240DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRQV4SER ${DATA}/PTEM/GRM02PCG.IMU240BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSR4SER ${DATA}/PTEM/GRM02PCG.IMU240EP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRQPV ${DATA}/PTEM/GRM02PCG.IMU240CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSRQPV ${DATA}/PTEM/GRM02PCG.IMU240FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU240 
       JUMP_LABEL=GRM02PCH
       ;;
(GRM02PCH)
       m_CondExec 04,GE,GRM02PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR BRM015                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCJ
       ;;
(GRM02PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/GRM02PCG.IMU240AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCJ.BRM015AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_37 4 CH 37
 /KEYS
   FLD_CH_4_37 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCK
       ;;
(GRM02PCK)
       m_CondExec 00,EQ,GRM02PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR BRM015                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCM
       ;;
(GRM02PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PTEM/GRM02PCG.IMU240BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCM.BRM015BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_37 4 CH 37
 /KEYS
   FLD_CH_4_37 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCN
       ;;
(GRM02PCN)
       m_CondExec 00,EQ,GRM02PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR BRM015                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCQ
       ;;
(GRM02PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/GRM02PCG.IMU240CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCQ.BRM015CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_37 4 CH 37
 /KEYS
   FLD_CH_4_37 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCR
       ;;
(GRM02PCR)
       m_CondExec 00,EQ,GRM02PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR BRM015                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCT
       ;;
(GRM02PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PTEM/GRM02PCG.IMU240DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCT.BRM015DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_30 4 CH 30
 /KEYS
   FLD_CH_4_30 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCU
       ;;
(GRM02PCU)
       m_CondExec 00,EQ,GRM02PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR BRM015                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PCX
       ;;
(GRM02PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PTEM/GRM02PCG.IMU240EP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PCX.BRM015EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_30 4 CH 30
 /KEYS
   FLD_CH_4_30 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PCY
       ;;
(GRM02PCY)
       m_CondExec 00,EQ,GRM02PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR BRM015                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDA
       ;;
(GRM02PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/GRM02PCG.IMU240FP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GRM02PDA.BRM015FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_30 4 CH 30
 /KEYS
   FLD_CH_4_30 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PDB
       ;;
(GRM02PDB)
       m_CondExec 00,EQ,GRM02PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM015 : PREPARATION DU FIC DE LOAD DE RTRM85                               
#         ( RANG PAR CODIC ET GROUPE DE SEGMENT)                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDD PGM=BRM015     ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDD
       ;;
(GRM02PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS ISSUS DES TRIS PRECEDENTS                                   
       m_FileAssign -d SHR -g ${G_A29} FRQV4S ${DATA}/PTEM/GRM02PCJ.BRM015AP
       m_FileAssign -d SHR -g ${G_A30} FRQV4SER ${DATA}/PTEM/GRM02PCM.BRM015BP
       m_FileAssign -d SHR -g ${G_A31} FRQPV ${DATA}/PTEM/GRM02PCQ.BRM015CP
       m_FileAssign -d SHR -g ${G_A32} FSR4S ${DATA}/PTEM/GRM02PCT.BRM015DP
       m_FileAssign -d SHR -g ${G_A33} FSR4SER ${DATA}/PTEM/GRM02PCX.BRM015EP
       m_FileAssign -d SHR -g ${G_A34} FSRQPV ${DATA}/PTEM/GRM02PDA.BRM015FP
# ******  FIC DE LOAD RTRM85                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 FRM85 ${DATA}/PTEM/GRM02PDD.BRM015GP
       m_ProgramExec BRM015 
# ********************************************************************         
#  TRI DU FICHIER DE LOAD RTRM85                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDG
       ;;
(GRM02PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/GRM02PDD.BRM015GP
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.RM85RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_31_7 31 CH 7
 /KEYS
   FLD_CH_31_7 ASCENDING,
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PDH
       ;;
(GRM02PDH)
       m_CondExec 00,EQ,GRM02PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD SUR RTRM85 : RANG PAR CODIC ET GROUPE DE SEGMENTS                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDJ PGM=DSNUTILB   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDJ
       ;;
(GRM02PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* TABLE RTRM85                                                         
#    RSRM85   : NAME=RSRM85,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSRM85 /dev/null
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A36} SYSREC ${DATA}/PXX0/F07.RELOAD.RM85RP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM02PDJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM02P_GRM02PDJ_RTRM85.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM02PDK
       ;;
(GRM02PDK)
       m_CondExec 04,GE,GRM02PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI1 DU FIC VENTE ENRICHI (GROUPE DE MAG , LIBELLE AGREGAT ,FAMILLE         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDM
       ;;
(GRM02PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/GRM02PAQ.BRM005CP
       m_FileAssign -d NEW,CATLG,DELETE -r 102 -g +1 SORTOUT ${DATA}/PTEM/GRM02PDM.BRM020AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_104_4 104 CH 4
 /FIELDS FLD_CH_1_30 1 CH 30
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_CH_84_4 84 CH 4
 /FIELDS FLD_CH_88_4 88 CH 4
 /FIELDS FLD_CH_116_4 116 CH 4
 /FIELDS FLD_CH_56_4 56 CH 4
 /FIELDS FLD_CH_116_8 116 CH 8
 /FIELDS FLD_CH_132_8 132 CH 8
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_52_4 52 CH 4
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_100_4 100 CH 4
 /FIELDS FLD_CH_100_8 100 CH 8
 /FIELDS FLD_CH_84_8 84 CH 8
 /KEYS
   FLD_CH_1_30 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_52_4,
    TOTAL FLD_CH_56_4,
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_84_4,
    TOTAL FLD_CH_88_4,
    TOTAL FLD_CH_100_4,
    TOTAL FLD_CH_104_4,
    TOTAL FLD_CH_116_4
 /* MT_ERROR (unknown field) (1,30,52,8,68,8,84,8,100,8,116,8,132,8 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_30,FLD_CH_52_8,FLD_CH_68_8,FLD_CH_84_8,FLD_CH_100_8,FLD_CH_116_8,FLD_CH_132_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRM02PDN
       ;;
(GRM02PDN)
       m_CondExec 00,EQ,GRM02PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 DU FIC VENTE ENRICHI (GROUPE DE MAG , LIBELLE AGREGAT ,FAMILLE         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDQ
       ;;
(GRM02PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GRM02PAQ.BRM005CP
       m_FileAssign -d NEW,CATLG,DELETE -r 164 -g +1 SORTOUT ${DATA}/PTEM/GRM02PDQ.BRM020BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_4 52 CH 4
 /FIELDS FLD_CH_1_30 1 CH 30
 /FIELDS FLD_CH_56_4 56 CH 4
 /FIELDS FLD_CH_44_128 44 CH 128
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_64_4 64 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_60_4 60 CH 4
 /FIELDS FLD_CH_44_4 44 CH 4
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_38_6 38 CH 6
 /KEYS
   FLD_CH_1_30 ASCENDING,
   FLD_CH_38_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_44_4,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_4,
    TOTAL FLD_CH_56_4,
    TOTAL FLD_CH_60_4,
    TOTAL FLD_CH_64_4,
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_30,FLD_CH_38_6,FLD_CH_44_128
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRM02PDR
       ;;
(GRM02PDR)
       m_CondExec 00,EQ,GRM02PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI3 DU FIC VENTE ENRICHI (GROUPE DE MAG , LIBELLE AGREGAT ,FAMILLE         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDT
       ;;
(GRM02PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/GRM02PAQ.BRM005CP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/GRM02PDT.BRM020CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_60_4 60 CH 4
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_1_43 1 CH 43
 /FIELDS FLD_CH_52_4 52 CH 4
 /FIELDS FLD_CH_64_4 64 CH 4
 /FIELDS FLD_CH_56_4 56 CH 4
 /FIELDS FLD_CH_44_4 44 CH 4
 /KEYS
   FLD_CH_1_43 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_44_4,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_4,
    TOTAL FLD_CH_56_4,
    TOTAL FLD_CH_60_4,
    TOTAL FLD_CH_64_4,
    TOTAL FLD_CH_68_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PDU
       ;;
(GRM02PDU)
       m_CondExec 00,EQ,GRM02PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FIC VENTES PREVISIONNELLES                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PDX
       ;;
(GRM02PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/GRM02PBA.BRM010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 SORTOUT ${DATA}/PTEM/GRM02PDX.BRM020DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_41_16 41 CH 16
 /FIELDS FLD_PD_49_4 49 PD 4
 /FIELDS FLD_PD_53_4 53 PD 4
 /FIELDS FLD_CH_4_30 4 CH 30
 /FIELDS FLD_PD_41_4 41 PD 4
 /FIELDS FLD_PD_45_4 45 PD 4
 /KEYS
   FLD_CH_4_30 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_4,
    TOTAL FLD_PD_45_4,
    TOTAL FLD_PD_49_4,
    TOTAL FLD_PD_53_4
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_30,FLD_CH_41_16
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRM02PDY
       ;;
(GRM02PDY)
       m_CondExec 00,EQ,GRM02PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM020 : PREPARATION DU FIC DE LOAD DE RTRM90                               
#            ( VENTES ET OBJECTIF DE STOCKS)                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PEA
       ;;
(GRM02PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES VENTES DES 4 DERNIERES ET FIC VENTES ENRICHIS           
       m_FileAssign -d SHR -g ${G_A41} FRM006 ${DATA}/PTEM/GRM02PDM.BRM020AP
       m_FileAssign -d SHR -g ${G_A42} FRM007 ${DATA}/PTEM/GRM02PDQ.BRM020BP
       m_FileAssign -d SHR -g ${G_A43} FRM005 ${DATA}/PTEM/GRM02PDT.BRM020CP
       m_FileAssign -d SHR -g ${G_A44} FRM010 ${DATA}/PTEM/GRM02PDX.BRM020DP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** TABLES AJOUTEES LE 22/04/03                                           
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
# ******  FIC DE LOAD RTRM90                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 114 -g +1 FRM020 ${DATA}/PTEM/GRM02PEA.BRM020EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM020 
       JUMP_LABEL=GRM02PEB
       ;;
(GRM02PEB)
       m_CondExec 04,GE,GRM02PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FIC DE LOAD POUR RTRM90                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PED
       ;;
(GRM02PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PTEM/GRM02PEA.BRM020EP
       m_FileAssign -d NEW,CATLG,DELETE -r 114 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.RM90RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM02PEE
       ;;
(GRM02PEE)
       m_CondExec 00,EQ,GRM02PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD SUR RTRM90 : VENTES ET OBJECTIF DE STOCK                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM02PEG PGM=DSNUTILB   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PEG
       ;;
(GRM02PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE RTRM90                                                         
#    RSRM90   : NAME=RSRM90,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSRM90 /dev/null
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A46} SYSREC ${DATA}/PXX0/F07.RELOAD.RM90RP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM02PEG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM02P_GRM02PEG_RTRM90.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM02PEH
       ;;
(GRM02PEH)
       m_CondExec 04,GE,GRM02PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM02PZA
       ;;
(GRM02PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM02PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
