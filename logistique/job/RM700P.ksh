#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RM700P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRM700 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 02/12/04 AT 10.04.00 BY BURTEC9                      
#    STANDARDS: P  JOBSET: RM700P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI FICHIER VENTE MGI VENANT DES AS400                                      
#   REPRISE : OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RM700PA
       ;;
(RM700PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       RM700PA=${LUNDI}
       RUN=${RUN}
       JUMP_LABEL=RM700PAA
       ;;
(RM700PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****************** BASCULE TT FILIALE SOUS LGT                               
# PRED     LINK  NAME=$GRM21D,MODE=I                                           
# PRED     LINK  NAME=$GRM21L,MODE=I                                           
# PRED     LINK  NAME=$GRM21M,MODE=I                                           
# PRED     LINK  NAME=$GRM02D,MODE=I                                           
# PRED     LINK  NAME=$GRM02L,MODE=I                                           
# PRED     LINK  NAME=$GRM02M,MODE=I                                           
# PRED     LINK  NAME=$GRM02Y,MODE=I                                           
# PRED     LINK  NAME=$GRM21Y,MODE=I                                           
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FICMGIAP
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 SORTOUT ${DATA}/PTEM/RM700PAA.BRM700AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RM700PAB
       ;;
(RM700PAB)
       m_CondExec 00,EQ,RM700PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM700 : CREATION DES FICHIERS DE LOAD RTHV04 RTHV12                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM700PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RM700PAD
       ;;
(RM700PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  LECTURE                                                              
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM50   : NAME=RSRM50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM50 /dev/null
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#    RSRM90   : NAME=RSRM90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM90 /dev/null
#                                                                              
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER RECYCLAGE J-1                                                
       m_FileAssign -d SHR -g ${G_A1} FICMGIE ${DATA}/PTEM/RM700PAA.BRM700AP
#                                                                              
# ******  FICHIER RECYCLAGE J                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 FICMGIV ${DATA}/PXX0/F07.FICMGIAP
# ******  FICHIER POUR REAPPRO MGI                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 FICMGIS ${DATA}/PXX0/F07.BRM700BP
# ******  FICHIER A ENVOYER SUR AS400                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -g +1 FRANG ${DATA}/PXX0/F07.BRM700DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM700 
       JUMP_LABEL=RM700PAE
       ;;
(RM700PAE)
       m_CondExec 04,GE,RM700PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   FICHIER POUR REAPPRO MGI A ENVOYER AUX MGI FICHIER COMPLET                 
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM700PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RM700PAG
       ;;
(RM700PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.BRM700BP
#   FICHIER POUR REAPPRO MGI COMPLET                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 SORTOUT ${DATA}/PTEM/RM700PAG.BRM700CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RM700PAH
       ;;
(RM700PAH)
       m_CondExec 00,EQ,RM700PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU FICHIER PARAMETRE REAPPRO QUOTIDIEN                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM700PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RM700PAJ
       ;;
(RM700PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RM700PAG.BRM700CP
#   FICHIER POUR REAPPRO MGI A ENVOYER AUX MGI QUOTIDIEN (DXREBP)              
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -g +1 SORTOUT ${DATA}/PXX0/F07.BRM700EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "OO"
 /FIELDS FLD_CH_18_2 18 CH 2
 /FIELDS FLD_CH_1_10 1 CH 10
 /CONDITION CND_2 FLD_CH_18_2 EQ CST_1_6 
 /KEYS
   FLD_CH_1_10 ASCENDING
 /OMIT CND_2
 /* Record Type = F  Record Length = 80 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_10,FLD_CH_18_2
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RM700PAK
       ;;
(RM700PAK)
       m_CondExec 00,EQ,RM700PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO FICHIER PARAMETRE REAPPRO HEBDO A VIDE SAUF LUNDI             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM700PAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RM700PAM
       ;;
(RM700PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
#   FICHIER POUR REAPPRO MGI A ENVOYER AUX MGI HEBDO     (DXREBP)              
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 OUT1 ${DATA}/PXX0/F07.BRM700FP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM700PAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RM700PAN
       ;;
(RM700PAN)
       m_CondExec 16,NE,RM700PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREAT FICHIER     PARAMETRE REAPPRO HEBDO SI LUNDI                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM700PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RM700PAQ
       ;;
(RM700PAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[RM700PA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/RM700PAG.BRM700CP
#   FICHIER POUR REAPPRO MGI A ENVOYER AUX MGI HEBDO     (DXREBP)              
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g ${G_A5} SORTOUT ${DATA}/PXX0/F07.BRM700FP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 15 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_15
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RM700PAR
       ;;
(RM700PAR)
       m_CondExec 00,EQ,RM700PAQ ${EXAAZ},NE,YES 1,EQ,$[RM700PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RM700PZA
       ;;
(RM700PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM700PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
