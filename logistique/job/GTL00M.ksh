#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL00M.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGTL00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 10.55.48 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL00M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BTL100 : AFFECTE UN PROFIL DE TOURNEES AUX LIVRAISONS DU JOUR              
#   REPRISE: OUI APRES ABEND                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL00MA
       ;;
(GTL00MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL00MAA
       ;;
(GTL00MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LES TABLES             *                                       
#    DE VENTES A JOUY                  *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  CALANDRIER DES LIVRAISONS                                            
#    RSTL10M  : NAME=RSTL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL10M /dev/null
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11M  : NAME=RSGV11M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  ANOMALIES                                                            
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL100 
       JUMP_LABEL=GTL00MAB
       ;;
(GTL00MAB)
       m_CondExec 04,GE,GTL00MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREE UNE GENERATION  A VIDE DE FACON A LES PRENDRE EN DISP=MOD LORS         
#  DES REPRISES DE BTL000                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAD
       ;;
(GTL00MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT1 ${DATA}/PTL989/F89.BTL001CM
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT2 ${DATA}/PTL989/F89.BTL002DM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00MAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GTL00MAE
       ;;
(GTL00MAE)
       m_CondExec 16,NE,GTL00MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BTL000 : EXTRACTION DES VENTES A LIVRER                                    
#   FIC FTL000 : FIC DES VENTES POUR LOGICIEL DES LIVRAISONS                   
#   FIC FTL001 : FIC DES ANOMALIES,CUMUL DE PROFIL(1 LOGNE PAR PROFIL)         
#   PGM = BTL000                                                               
#   REPRISE: OUI APRES ABEND                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAG
       ;;
(GTL00MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  LIEUX                                                                
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  ADRESSES                                                             
#    RSGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02M /dev/null
# ******  RESERVATION STOCK ENTREPOT                                           
#    RSGV21M  : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21M /dev/null
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11M  : NAME=RSGV11M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  LIVRAISON CLIENT A DESTOCKEY                                         
#    RSGD05M  : NAME=RSGD05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD05M /dev/null
# ******  PROFIL DE TOURNEES DEMANDEES                                         
#    RSTL05M  : NAME=RSTL05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05M /dev/null
# ******  CALANDRIER DES LIVRAISONS                                            
#    RSTL10M  : NAME=RSTL10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL10M /dev/null
       m_FileAssign -d SHR FTL000 /dev/null
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A1} FTL001 ${DATA}/PTL989/F89.BTL001CM
#  FICHIER DES PROFILS A EDITER                                                
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A2} FTL002 ${DATA}/PTL989/F89.BTL002DM
# ******  FICHIER EN DUMMY                                                     
       m_FileAssign -d SHR FTL003 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL000 
       JUMP_LABEL=GTL00MAH
       ;;
(GTL00MAH)
       m_CondExec 04,GE,GTL00MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL001CM POUR ELIMINER LES DOUBLONS                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAJ
       ;;
(GTL00MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTL989/F89.BTL001CM
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL989/F89.BTL001BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00MAK
       ;;
(GTL00MAK)
       m_CondExec 00,EQ,GTL00MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL002DM POUR ELIMINER LES DOUBLONS CREES LORS DES           
#  REPRISES (CREATION ENTRE 2 COMMITS)                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAM
       ;;
(GTL00MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTL989/F89.BTL002DM
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL989/F89.BTL002AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00MAN
       ;;
(GTL00MAN)
       m_CondExec 00,EQ,GTL00MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LES CHAINES : GTL45M GTL55M                       
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAQ
       ;;
(GTL00MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTL989/F89.BTL001BM
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTL989/F89.BTL002AM
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL989/F89.BTL001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00MAR
       ;;
(GTL00MAR)
       m_CondExec 00,EQ,GTL00MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   FUSION 9,3,A,1,8,A,12,23,A                                                 
#   SOC,JOUR,PLAN,TYPE D ENR,ZONE TRI                                          
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAT
       ;;
(GTL00MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTL989/F89.BTL001AM
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL00MAT.BTL030AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_12_23 12 CH 23
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_23 ASCENDING
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00MAU
       ;;
(GTL00MAU)
       m_CondExec 00,EQ,GTL00MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    ECRITURE   DANS LA BASE IMPRESSION GENERALISEE                            
#    1 LISTE NOMBRE DE PROFILS TRAITES                                         
#    1 LISTE ANOMALIES                                                         
#    1 LISTE DES TRANSITAIRES (POUR L ETRANGER)                                
#    REPRISE: NON  BACKOUT DU STEP PAR LOGGING IMS                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MAX
       ;;
(GTL00MAX)
       m_CondExec ${EXABJ},NE,YES 
# BJ      IMSSTEP PGM=BTL030,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL00MR1)                                             
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL00MR1
       m_OutputAssign -c "*" DDOTV02
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DM,MODE=IU,REST=(YES,GTL00MR1)                      
# DIGVIP   FILE NAME=DI0000IM,MODE=I                                           
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL00MAX
       m_FileAssign -d SHR -g ${G_A8} FTL030 ${DATA}/PTEM/GTL00MAT.BTL030AM
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGM 
# ********************************************************************         
#  MISE A JOUR DES RETOURS DE LIVRAISON RECYCLEES                              
#  PGM = BTL085                                                                
#  REPRISE: OUI APRES ABEND                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MBA
       ;;
(GTL00MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DETAILS DES VENTES                                                   
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  RETOURS DES TOURNEES                                                 
#    RSTL04M  : NAME=RSTL04M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04M /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL085 
       JUMP_LABEL=GTL00MBB
       ;;
(GTL00MBB)
       m_CondExec 04,GE,GTL00MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL00MZA
       ;;
(GTL00MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
