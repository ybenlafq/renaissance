#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LG110P.ksh                       --- VERSION DU 09/10/2016 05:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLG110 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 10.27.12 BY BURTEC6                      
#    STANDARDS: P  JOBSET: LG110P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   DELETE DES FICHIERS SEQUENTIEL                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LG110PA
       ;;
(LG110PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXAQO=${EXAQO:-0}
       EXAQT=${EXAQT:-0}
       EXAQY=${EXAQY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A71=${G_A71:-'+1'}
       G_A72=${G_A72:-'+1'}
       G_A73=${G_A73:-'+1'}
       G_A74=${G_A74:-'+1'}
       G_A75=${G_A75:-'+1'}
       G_A76=${G_A76:-'+1'}
       G_A77=${G_A77:-'+1'}
       G_A78=${G_A78:-'+1'}
       G_A79=${G_A79:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A80=${G_A80:-'+1'}
       G_A81=${G_A81:-'+1'}
       G_A82=${G_A82:-'+1'}
       G_A83=${G_A83:-'+1'}
       G_A84=${G_A84:-'+1'}
       G_A85=${G_A85:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LG110PAA
       ;;
(LG110PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG110PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=LG110PAB
       ;;
(LG110PAB)
       m_CondExec 16,NE,LG110PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   UNLOAD CODE PARAM�TRE LGTPS DE CHAQUE FILIALE                              
#   UNLOAD DES SOUS TABLE LGTPS ET LGTPM + RTGA30                              
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAD
       ;;
(LG110PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
#    RSGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
#    RSGA30L  : NAME=RSGA30L,MODE=I - DYNAM=YES                                
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
#    RSGA30M  : NAME=RSGA30M,MODE=I - DYNAM=YES                                
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
#    RSGA30D  : NAME=RSGA30D,MODE=I - DYNAM=YES                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/LG110PAD.BG100UAP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG110PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   PROG    BLG100 : PGM  D EXTRACTION                                         
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAG
       ;;
(LG110PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE ARTICLES                                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA04   : NAME=RSGA04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA04 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA80   : NAME=RSGA83,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA80 /dev/null
#  TABLE DES VENTES                                                            
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV10   : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#  TABLE DES TOURNEES                                                          
#    RTTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL02 /dev/null
#  INTERROGATION DES TABLES FILIALES VIA SYNONYMES                             
#    RTTL02   : NAME=RSTL02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#                                                                              
#    RTTL11   : NAME=RSTL11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL11 /dev/null
#  TABLE EN MAJ                                                                
#    RTGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB05 /dev/null
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/LG110PAG
       m_FileAssign -d SHR -g ${G_A1} FRTGA01 ${DATA}/PTEM/LG110PAD.BG100UAP
#  POUR LIVRAISON _A J+1                                                        
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F07.BLG110CP
#  FICHIER EN SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FLG360 ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FSYSIN ${DATA}/PXX0/F07.BLG110BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG100 
       JUMP_LABEL=LG110PAH
       ;;
(LG110PAH)
       m_CondExec 04,GE,LG110PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907130                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAJ
       ;;
(LG110PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907130F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907130"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PAK
       ;;
(LG110PAK)
       m_CondExec 00,EQ,LG110PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907131                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAM
       ;;
(LG110PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907131F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907131"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PAN
       ;;
(LG110PAN)
       m_CondExec 00,EQ,LG110PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907132                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAQ
       ;;
(LG110PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907132F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907132"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PAR
       ;;
(LG110PAR)
       m_CondExec 00,EQ,LG110PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907133                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAT
       ;;
(LG110PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907133F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907133"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PAU
       ;;
(LG110PAU)
       m_CondExec 00,EQ,LG110PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907134                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PAX
       ;;
(LG110PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907134F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907134"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PAY
       ;;
(LG110PAY)
       m_CondExec 00,EQ,LG110PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916001                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBA
       ;;
(LG110PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916001F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916001"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBB
       ;;
(LG110PBB)
       m_CondExec 00,EQ,LG110PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916015                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBD
       ;;
(LG110PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916015F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916015"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBE
       ;;
(LG110PBE)
       m_CondExec 00,EQ,LG110PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916018                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBG
       ;;
(LG110PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916018F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916018"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBH
       ;;
(LG110PBH)
       m_CondExec 00,EQ,LG110PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916019                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBJ
       ;;
(LG110PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916019F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916019"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBK
       ;;
(LG110PBK)
       m_CondExec 00,EQ,LG110PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916020                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBM
       ;;
(LG110PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916020F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916020"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBN
       ;;
(LG110PBN)
       m_CondExec 00,EQ,LG110PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916024                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBQ
       ;;
(LG110PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916024F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916024"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBR
       ;;
(LG110PBR)
       m_CondExec 00,EQ,LG110PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916025                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBT
       ;;
(LG110PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916025F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916025"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBU
       ;;
(LG110PBU)
       m_CondExec 00,EQ,LG110PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916026                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=LG110PBX
       ;;
(LG110PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916026F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916026"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PBY
       ;;
(LG110PBY)
       m_CondExec 00,EQ,LG110PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916031                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCA
       ;;
(LG110PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916031F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916031"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCB
       ;;
(LG110PCB)
       m_CondExec 00,EQ,LG110PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916036                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCD
       ;;
(LG110PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916036F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916036"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCE
       ;;
(LG110PCE)
       m_CondExec 00,EQ,LG110PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916037                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCG
       ;;
(LG110PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916037F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916037"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCH
       ;;
(LG110PCH)
       m_CondExec 00,EQ,LG110PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916039                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCJ
       ;;
(LG110PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916039F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916039"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCK
       ;;
(LG110PCK)
       m_CondExec 00,EQ,LG110PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916045                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCM
       ;;
(LG110PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916045F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916045"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCN
       ;;
(LG110PCN)
       m_CondExec 00,EQ,LG110PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916047                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCQ
       ;;
(LG110PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916047F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916047"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCR
       ;;
(LG110PCR)
       m_CondExec 00,EQ,LG110PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916052                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCT
       ;;
(LG110PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916052F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916052"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCU
       ;;
(LG110PCU)
       m_CondExec 00,EQ,LG110PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916058                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=LG110PCX
       ;;
(LG110PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916058F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916058"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PCY
       ;;
(LG110PCY)
       m_CondExec 00,EQ,LG110PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916160                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDA
       ;;
(LG110PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916160F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916160"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDB
       ;;
(LG110PDB)
       m_CondExec 00,EQ,LG110PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916161                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDD
       ;;
(LG110PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916161F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916161"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDE
       ;;
(LG110PDE)
       m_CondExec 00,EQ,LG110PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916162                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDG
       ;;
(LG110PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916162F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916162"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDH
       ;;
(LG110PDH)
       m_CondExec 00,EQ,LG110PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916163                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDJ
       ;;
(LG110PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916163F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916163"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDK
       ;;
(LG110PDK)
       m_CondExec 00,EQ,LG110PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916164                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDM
       ;;
(LG110PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916164F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916164"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDN
       ;;
(LG110PDN)
       m_CondExec 00,EQ,LG110PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916165                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDQ
       ;;
(LG110PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916165F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916165"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDR
       ;;
(LG110PDR)
       m_CondExec 00,EQ,LG110PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916166                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDT
       ;;
(LG110PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916166F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916166"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDU
       ;;
(LG110PDU)
       m_CondExec 00,EQ,LG110PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916167                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PDX
       ;;
(LG110PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916167F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916167"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PDY
       ;;
(LG110PDY)
       m_CondExec 00,EQ,LG110PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916168                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=LG110PEA
       ;;
(LG110PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916168F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916168"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEB
       ;;
(LG110PEB)
       m_CondExec 00,EQ,LG110PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916169                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PED
       ;;
(LG110PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916169F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916169"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEE
       ;;
(LG110PEE)
       m_CondExec 00,EQ,LG110PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916170                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=LG110PEG
       ;;
(LG110PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916170F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916170"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEH
       ;;
(LG110PEH)
       m_CondExec 00,EQ,LG110PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916171                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=LG110PEJ
       ;;
(LG110PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916171F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916171"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEK
       ;;
(LG110PEK)
       m_CondExec 00,EQ,LG110PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916172                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=LG110PEM
       ;;
(LG110PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916172F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916172"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEN
       ;;
(LG110PEN)
       m_CondExec 00,EQ,LG110PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916173                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=LG110PEQ
       ;;
(LG110PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916173F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916173"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PER
       ;;
(LG110PER)
       m_CondExec 00,EQ,LG110PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916174                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=LG110PET
       ;;
(LG110PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916174F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916174"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEU
       ;;
(LG110PEU)
       m_CondExec 00,EQ,LG110PET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916175                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PEX PGM=SORT       ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=LG110PEX
       ;;
(LG110PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916175F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916175"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PEY
       ;;
(LG110PEY)
       m_CondExec 00,EQ,LG110PEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916176                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFA
       ;;
(LG110PFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916176F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916176"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFB
       ;;
(LG110PFB)
       m_CondExec 00,EQ,LG110PFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916177                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFD PGM=SORT       ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFD
       ;;
(LG110PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916177F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916177"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFE
       ;;
(LG110PFE)
       m_CondExec 00,EQ,LG110PFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916178                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFG
       ;;
(LG110PFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916178F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916178"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFH
       ;;
(LG110PFH)
       m_CondExec 00,EQ,LG110PFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916179                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFJ PGM=SORT       ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFJ
       ;;
(LG110PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A42} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916179F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916179"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFK
       ;;
(LG110PFK)
       m_CondExec 00,EQ,LG110PFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916210                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFM PGM=SORT       ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFM
       ;;
(LG110PFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916210F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916210"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFN
       ;;
(LG110PFN)
       m_CondExec 00,EQ,LG110PFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945069                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFQ PGM=SORT       ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFQ
       ;;
(LG110PFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A44} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945069F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945069"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFR
       ;;
(LG110PFR)
       m_CondExec 00,EQ,LG110PFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945070                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFT PGM=SORT       ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFT
       ;;
(LG110PFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945070F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945070"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFU
       ;;
(LG110PFU)
       m_CondExec 00,EQ,LG110PFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945071                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PFX PGM=SORT       ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=LG110PFX
       ;;
(LG110PFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945071F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945071"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PFY
       ;;
(LG110PFY)
       m_CondExec 00,EQ,LG110PFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945072                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGA PGM=SORT       ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGA
       ;;
(LG110PGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A47} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945072F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945072"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGB
       ;;
(LG110PGB)
       m_CondExec 00,EQ,LG110PGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945073                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGD PGM=SORT       ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGD
       ;;
(LG110PGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945073F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945073"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGE
       ;;
(LG110PGE)
       m_CondExec 00,EQ,LG110PGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945074                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGG PGM=SORT       ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGG
       ;;
(LG110PGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A49} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945074F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945074"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGH
       ;;
(LG110PGH)
       m_CondExec 00,EQ,LG110PGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945075                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGJ PGM=SORT       ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGJ
       ;;
(LG110PGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A50} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945075F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945075"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGK
       ;;
(LG110PGK)
       m_CondExec 00,EQ,LG110PGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945076                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGM PGM=SORT       ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGM
       ;;
(LG110PGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945076F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945076"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGN
       ;;
(LG110PGN)
       m_CondExec 00,EQ,LG110PGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945077                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGQ PGM=SORT       ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGQ
       ;;
(LG110PGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A52} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945077F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945077"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGR
       ;;
(LG110PGR)
       m_CondExec 00,EQ,LG110PGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945078                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGT PGM=SORT       ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGT
       ;;
(LG110PGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A53} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945078F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945078"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGU
       ;;
(LG110PGU)
       m_CondExec 00,EQ,LG110PGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945080                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PGX PGM=SORT       ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=LG110PGX
       ;;
(LG110PGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A54} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945080F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945080"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PGY
       ;;
(LG110PGY)
       m_CondExec 00,EQ,LG110PGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945081                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHA PGM=SORT       ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHA
       ;;
(LG110PHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A55} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945081F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945081"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHB
       ;;
(LG110PHB)
       m_CondExec 00,EQ,LG110PHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945082                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHD PGM=SORT       ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHD
       ;;
(LG110PHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A56} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945082F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945082"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHE
       ;;
(LG110PHE)
       m_CondExec 00,EQ,LG110PHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945083                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHG PGM=SORT       ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHG
       ;;
(LG110PHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A57} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945083F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945083"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHH
       ;;
(LG110PHH)
       m_CondExec 00,EQ,LG110PHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945084                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHJ PGM=SORT       ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHJ
       ;;
(LG110PHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A58} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945084F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945084"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHK
       ;;
(LG110PHK)
       m_CondExec 00,EQ,LG110PHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945086                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHM PGM=SORT       ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHM
       ;;
(LG110PHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A59} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945086F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945086"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHN
       ;;
(LG110PHN)
       m_CondExec 00,EQ,LG110PHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961071                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHQ PGM=SORT       ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHQ
       ;;
(LG110PHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A60} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961071F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961071"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHR
       ;;
(LG110PHR)
       m_CondExec 00,EQ,LG110PHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961072                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHT PGM=SORT       ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHT
       ;;
(LG110PHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A61} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961072F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961072"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHU
       ;;
(LG110PHU)
       m_CondExec 00,EQ,LG110PHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961073                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PHX PGM=SORT       ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=LG110PHX
       ;;
(LG110PHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A62} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961073F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961073"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PHY
       ;;
(LG110PHY)
       m_CondExec 00,EQ,LG110PHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961074                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIA PGM=SORT       ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIA
       ;;
(LG110PIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A63} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961074F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961074"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIB
       ;;
(LG110PIB)
       m_CondExec 00,EQ,LG110PIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961075                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PID PGM=SORT       ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=LG110PID
       ;;
(LG110PID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A64} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961075F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961075"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIE
       ;;
(LG110PIE)
       m_CondExec 00,EQ,LG110PID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961076                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIG PGM=SORT       ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIG
       ;;
(LG110PIG)
       m_CondExec ${EXAMS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A65} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961076F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961076"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIH
       ;;
(LG110PIH)
       m_CondExec 00,EQ,LG110PIG ${EXAMS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961077                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIJ PGM=SORT       ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIJ
       ;;
(LG110PIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A66} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961077F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961077"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIK
       ;;
(LG110PIK)
       m_CondExec 00,EQ,LG110PIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961079                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIM PGM=SORT       ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIM
       ;;
(LG110PIM)
       m_CondExec ${EXANC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A67} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961079F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961079"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIN
       ;;
(LG110PIN)
       m_CondExec 00,EQ,LG110PIM ${EXANC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961080                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIQ PGM=SORT       ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIQ
       ;;
(LG110PIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A68} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961080F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961080"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIR
       ;;
(LG110PIR)
       m_CondExec 00,EQ,LG110PIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989072                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIT PGM=SORT       ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIT
       ;;
(LG110PIT)
       m_CondExec ${EXANM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A69} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989072F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989072"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIU
       ;;
(LG110PIU)
       m_CondExec 00,EQ,LG110PIT ${EXANM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989075                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PIX PGM=SORT       ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=LG110PIX
       ;;
(LG110PIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A70} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989075F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989075"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PIY
       ;;
(LG110PIY)
       m_CondExec 00,EQ,LG110PIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989076                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJA PGM=SORT       ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJA
       ;;
(LG110PJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A71} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989076F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989076"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJB
       ;;
(LG110PJB)
       m_CondExec 00,EQ,LG110PJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989078                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJD PGM=SORT       ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJD
       ;;
(LG110PJD)
       m_CondExec ${EXAOB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A72} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989078F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989078"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJE
       ;;
(LG110PJE)
       m_CondExec 00,EQ,LG110PJD ${EXAOB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991007                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJG PGM=SORT       ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJG
       ;;
(LG110PJG)
       m_CondExec ${EXAOG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A73} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991007F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991007"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJH
       ;;
(LG110PJH)
       m_CondExec 00,EQ,LG110PJG ${EXAOG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991010                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJJ PGM=SORT       ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJJ
       ;;
(LG110PJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A74} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991010F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991010"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJK
       ;;
(LG110PJK)
       m_CondExec 00,EQ,LG110PJJ ${EXAOL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991014                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJM PGM=SORT       ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJM
       ;;
(LG110PJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A75} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991014F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991014"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJN
       ;;
(LG110PJN)
       m_CondExec 00,EQ,LG110PJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991018                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJQ PGM=SORT       ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJQ
       ;;
(LG110PJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A76} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991018F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991018"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJR
       ;;
(LG110PJR)
       m_CondExec 00,EQ,LG110PJQ ${EXAOV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991077                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJT
       ;;
(LG110PJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A77} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991077F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991077"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJU
       ;;
(LG110PJU)
       m_CondExec 00,EQ,LG110PJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991078                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PJX PGM=SORT       ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=LG110PJX
       ;;
(LG110PJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A78} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991078F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991078"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PJY
       ;;
(LG110PJY)
       m_CondExec 00,EQ,LG110PJX ${EXAPF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991079                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKA
       ;;
(LG110PKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A79} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991079F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991079"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PKB
       ;;
(LG110PKB)
       m_CondExec 00,EQ,LG110PKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991080                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKD PGM=SORT       ** ID=APP                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKD
       ;;
(LG110PKD)
       m_CondExec ${EXAPP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A80} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991080F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991080"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PKE
       ;;
(LG110PKE)
       m_CondExec 00,EQ,LG110PKD ${EXAPP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991081                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKG PGM=SORT       ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKG
       ;;
(LG110PKG)
       m_CondExec ${EXAPU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A81} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991081F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991081"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PKH
       ;;
(LG110PKH)
       m_CondExec 00,EQ,LG110PKG ${EXAPU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991084                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKJ PGM=SORT       ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKJ
       ;;
(LG110PKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A82} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991084F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991084"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PKK
       ;;
(LG110PKK)
       m_CondExec 00,EQ,LG110PKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991088                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKM PGM=SORT       ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKM
       ;;
(LG110PKM)
       m_CondExec ${EXAQE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A83} SORTIN ${DATA}/PXX0/F07.BLG110AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991088F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991088"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG110PKN
       ;;
(LG110PKN)
       m_CondExec 00,EQ,LG110PKM ${EXAQE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES DIFF�RENTS FICHIERS PLATEFORME                                      
#  REPRISE : OUI                                                               
# ***********************************                                          
# AQJ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    FILE  NAME=BLG110BP,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKQ PGM=JVMLDM76   ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKQ
       ;;
(LG110PKQ)
       m_CondExec ${EXAQJ},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 256 -t LSEQ FICZIP ${DATA}/PXX0.F99.F361ZIP
# *                                                                            
       m_FileAssign -d SHR -g ${G_A84} SYSIN ${DATA}/PXX0/F07.BLG110BP
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI VERS SERVEUR 360 DE PROD                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AOL      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=F360ZIP,                                                            
#      FNAME=":F360ZIP"",                                                      
#      SAPPL=F360                                                              
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLG110P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKT PGM=EZACFSM1   ** ID=AQO                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKT
       ;;
(LG110PKT)
       m_CondExec ${EXAQO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG110PKT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/LG110PKT.FTLG110P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLG110P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG110PKX PGM=FTP        ** ID=AQT                                   
# ***********************************                                          
       JUMP_LABEL=LG110PKX
       ;;
(LG110PKX)
       m_CondExec ${EXAQT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LG110PKX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LG110PKT.FTLG110P(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP LG110PLA PGM=EZACFSM1   ** ID=AQY                                   
# ***********************************                                          
       JUMP_LABEL=LG110PLA
       ;;
(LG110PLA)
       m_CondExec ${EXAQY},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG110PLA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#   UPDATE DE LA TABLE RTLI00 POUR REMETTRE LE PARAM D'ORIGINE                 
#   REPRISE: OUI                                                               
# ********************************************************************         
# AQT      STEP  PGM=IKJEFT01                                                  
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
#                                                                              
# *******  MAJ TABLE ARTICLE INFOCENTRE                                        
# RSLI00   FILE  DYNAM=YES,NAME=RSLI00,MODE=I                                  
#                                                                              
# SYSTSIN  DATA  CLASS=FIX1,MBR=RDARTIAD                                       
# SYSIN    DATA  *                                                             
#  UPDATE PDARTY.RTLI00                                                        
#   SET LVPARAM  =                                                             
#      SUBSTR (LVPARAM , 1 , 1 )!!' '!!SUBSTR (LVPARAM , 3 )                   
#        WHERE CPARAM   = 'PF360' ;                                            
#          DATAEND                                                             
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LG110PZA
       ;;
(LG110PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG110PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
