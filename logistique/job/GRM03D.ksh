#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM03D.ksh                       --- VERSION DU 08/10/2016 22:21
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGRM03 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/09/30 AT 14.16.46 BY BURTECO                      
#    STANDARDS: P  JOBSET: GRM03D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FICHIER JRM221 ISSU DE LA CHAINE GRM02P                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM03DA
       ;;
(GRM03DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRM03DAA
       ;;
(GRM03DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BRM221AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRM03DAA.BRM221BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "991"
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_CH_166_3 166 CH 3
 /FIELDS FLD_PD_32_3 32 PD 3
 /FIELDS FLD_BI_35_20 35 CH 20
 /FIELDS FLD_BI_55_2 55 CH 2
 /FIELDS FLD_BI_7_20 7 CH 20
 /CONDITION CND_1 FLD_CH_166_3 EQ CST_1_8 
 /KEYS
   FLD_BI_27_5 ASCENDING,
   FLD_BI_7_20 ASCENDING,
   FLD_PD_32_3 ASCENDING,
   FLD_BI_35_20 ASCENDING,
   FLD_BI_55_2 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM03DAB
       ;;
(GRM03DAB)
       m_CondExec 00,EQ,GRM03DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION DE L'ETAT JRM221                                           
#                                                                              
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
#  REPRISE =  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM03DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRM03DAD
       ;;
(GRM03DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES SOUS/TABLES                                                
#    RSGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA71 /dev/null
# ******* TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG00 /dev/null
#    RSEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG05 /dev/null
#    RSEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG10 /dev/null
#    RSEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG11 /dev/null
#    RSEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG15 /dev/null
#    RSEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG20 /dev/null
#    RSEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG25 /dev/null
#    RSEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG30 /dev/null
# ******* FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A1} FEXTRAC ${DATA}/PTEM/GRM03DAA.BRM221BD
# ******* FICHIER FCUMULS                                                      
       m_FileAssign -d SHR FCUMULS /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE : 991                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAMETRE  FICHIER S/36(156)                                         
       m_FileAssign -d SHR FEG132 /dev/null
# ******* PARAMETRE  FICHIER S/36(222)                                         
       m_FileAssign -d SHR FEG198 /dev/null
#                                                                              
# ******* EDITION                                                              
       m_OutputAssign -c 9 -w JRM221 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM03DAE
       ;;
(GRM03DAE)
       m_CondExec 04,GE,GRM03DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER JRM231 ISSU DE LA CHAINE GRM02P                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM03DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRM03DAG
       ;;
(GRM03DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BRM231AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRM03DAG.BRM231BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "991"
 /FIELDS FLD_BI_7_20 7 CH 20
 /FIELDS FLD_CH_184_3 184 CH 3
 /FIELDS FLD_PD_32_3 32 PD 3
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_57_1 57 CH 1
 /FIELDS FLD_PD_55_2 55 PD 2
 /FIELDS FLD_BI_35_20 35 CH 20
 /CONDITION CND_1 FLD_CH_184_3 EQ CST_1_9 
 /KEYS
   FLD_BI_27_5 ASCENDING,
   FLD_BI_7_20 ASCENDING,
   FLD_PD_32_3 ASCENDING,
   FLD_BI_35_20 ASCENDING,
   FLD_PD_55_2 ASCENDING,
   FLD_BI_57_1 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM03DAH
       ;;
(GRM03DAH)
       m_CondExec 00,EQ,GRM03DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION DE L'ETAT JRM231                                           
#                                                                              
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
#  REPRISE =  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM03DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRM03DAJ
       ;;
(GRM03DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES SOUS/TABLES                                                
#    RSGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA71 /dev/null
# ******* TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG00 /dev/null
#    RSEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG05 /dev/null
#    RSEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG10 /dev/null
#    RSEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG11 /dev/null
#    RSEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG15 /dev/null
#    RSEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG20 /dev/null
#    RSEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG25 /dev/null
#    RSEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSEG30 /dev/null
# ******* FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GRM03DAG.BRM231BD
# ******* FICHIER FCUMULS                                                      
       m_FileAssign -d SHR FCUMULS /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE : 991                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAMETRE  FICHIER S/36(156)                                         
       m_FileAssign -d SHR FEG132 /dev/null
# ******* PARAMETRE  FICHIER S/36(222)                                         
       m_FileAssign -d SHR FEG198 /dev/null
#                                                                              
# ******* EDITION                                                              
       m_OutputAssign -c 9 -w JRM231 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM03DAK
       ;;
(GRM03DAK)
       m_CondExec 04,GE,GRM03DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM03DZA
       ;;
(GRM03DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM03DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
