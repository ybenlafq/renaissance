#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA600O.ksh                       --- VERSION DU 14/10/2016 10:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVA600 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/10/06 AT 16.55.32 BY BURTECA                      
#    STANDARDS: P  JOBSET: VA600O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BIE600                                                                
# ********************************************************************         
#  CREATION DES RAYONS TLMBL,TLMBR,ELABL,ELABR SUR RTGA20 ET RTGA21            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA600OA
       ;;
(VA600OA)
#
#VA600OAJ
#VA600OAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#VA600OAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2003/10/06 AT 16.55.32 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: VA600O                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'INIT RTVA05'                           
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA600OAA
       ;;
(VA600OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTIE05   : NAME=RSIE05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTIE05 /dev/null
#    RTIE60   : NAME=RSIE60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTIE60 /dev/null
#    RTIN00   : NAME=RSIN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTIN00 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGG55   : NAME=RSGG55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG55 /dev/null
# *****   TABLES EN M.A.J                                                      
#    RTIE10   : NAME=RSIE10O,MODE=(U,N) - DYNAM=YES                            
# -X-RSIE10O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTIE10 /dev/null
#    RTGG50   : NAME=RSGG50O,MODE=(U,N) - DYNAM=YES                            
# -X-RSGG50O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTVA10   : NAME=RSVA10O,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA10O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTVA10 /dev/null
# *****   FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *****   DELAI DE RETENTION SUR TABLE HISTO                                   
       m_FileAssign -d SHR FDELAI ${DATA}/CORTEX4.P.MTXTFIX1/VA600OAA
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -g +1 FIE600 ${DATA}/PTEM/VA600OAA.FIE600AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE600 
       JUMP_LABEL=VA600OAB
       ;;
(VA600OAB)
       m_CondExec 04,GE,VA600OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER ISSU DU STEP PRECEDENT                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA600OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA600OAD
       ;;
(VA600OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/VA600OAA.FIE600AO
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -g +1 SORTOUT ${DATA}/PXX0/F16.RELOAD.VA05RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_14_6 14 PD 6
 /FIELDS FLD_PD_46_8 46 PD 8
 /FIELDS FLD_PD_38_8 38 PD 8
 /FIELDS FLD_PD_26_6 26 PD 6
 /FIELDS FLD_PD_54_8 54 PD 8
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_PD_62_8 62 PD 8
 /FIELDS FLD_PD_32_6 32 PD 6
 /FIELDS FLD_PD_20_6 20 PD 6
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_14_6,
    TOTAL FLD_PD_20_6,
    TOTAL FLD_PD_26_6,
    TOTAL FLD_PD_32_6,
    TOTAL FLD_PD_38_8,
    TOTAL FLD_PD_46_8,
    TOTAL FLD_PD_54_8,
    TOTAL FLD_PD_62_8
 /* Record Type = F  Record Length = 79 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA600OAE
       ;;
(VA600OAE)
       m_CondExec 00,EQ,VA600OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : LOAD                                                                  
# ********************************************************************         
#  LOAD DE LA TABLE RTVA05                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA600OAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA600OAG
       ;;
(VA600OAG)
       m_CondExec ${EXAAK},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RTVA05   : NAME=RSVA05O,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA05O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTVA05 /dev/null
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F16.RELOAD.VA05RO
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA600OAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA600O_VA600OAG_RTVA05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA600OAH
       ;;
(VA600OAH)
       m_CondExec 04,GE,VA600OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : REPAIR                                                                
# ********************************************************************         
#  REPAIR NOCOPY TABLESPACE RSVA05O DE LA D.B PODVA00                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA600OAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA600OZA
       ;;
(VA600OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA600OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
