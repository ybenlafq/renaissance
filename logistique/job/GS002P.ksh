#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS002P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/01 AT 17.00.10 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GS002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BGS002 :  ALIMENTATION DU FICHIER DES INFOS POUR L'EDITION                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS002PA
       ;;
(GS002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS002PAA
       ;;
(GS002PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FMOIS : MMSSAA                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# *** TABLE EN LECTURE                                                         
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******                                                                       
# *** TABLE EN ECRITURE                                                        
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  ETAT                                                                 
       m_OutputAssign -c 9 -w IGS002 IGS002
#                                                                              
# ******  FICHIER EN TETE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 EGS002 ${DATA}/PTEM/GS002PAA.FGS002AP
# ******  FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 FGS002 ${DATA}/PTEM/GS002PAA.FGS002BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS002 
       JUMP_LABEL=GS002PAB
       ;;
(GS002PAB)
       m_CondExec 04,GE,GS002PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES EN TETE ET MVTS                                                  
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAD PGM=MERGE      ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAD
       ;;
(GS002PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS002PAA.FGS002AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GS002PAA.FGS002BP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS002CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 166 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS002PAE
       ;;
(GS002PAE)
       m_CondExec 00,EQ,GS002PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS003CP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAG
       ;;
(GS002PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS002PAG.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ECLATEMENT DES FICHIERS                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAJ
       ;;
(GS002PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS002PAA.FGS002BP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF1 ${DATA}/PTEM/GS002PAJ.FGS090AP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF2 ${DATA}/PTEM/GS002PAJ.FGS095AP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF3 ${DATA}/PTEM/GS002PAJ.FGS120AP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF4 ${DATA}/PTEM/GS002PAJ.FGS125AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "907"
 /DERIVEDFIELD CST_3_12 "090")
 /DERIVEDFIELD CST_1_24 "907"
 /DERIVEDFIELD CST_3_36 "125")
 /DERIVEDFIELD CST_3_28 "120")
 /DERIVEDFIELD CST_1_32 "907"
 /DERIVEDFIELD CST_3_20 "095")
 /DERIVEDFIELD CST_1_16 "907"
 /FIELDS FLD_CH_13_7 13 CH 7
 /FIELDS FLD_CH_50_3 50 CH 3
 /FIELDS FLD_CH_46_3 46 CH 3
 /FIELDS FLD_CH_86_7 86 CH 7
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_96_8 96 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /CONDITION CND_1 FLD_CH_46_3 EQ CST_1_8 AND FLD_CH_50_3 EQ CST_3_12 OR 
 /CONDITION CND_2 FLD_CH_46_3 EQ CST_1_16 AND FLD_CH_50_3 EQ CST_3_20 OR 
 /CONDITION CND_4 FLD_CH_46_3 EQ CST_1_32 AND FLD_CH_50_3 EQ CST_3_36 OR 
 /CONDITION CND_3 FLD_CH_46_3 EQ CST_1_24 AND FLD_CH_50_3 EQ CST_3_28 OR 
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_13_7 ASCENDING,
   FLD_CH_96_8 ASCENDING,
   FLD_CH_86_7 ASCENDING
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /INCLUDE CND_3
 /MT_OUTFILE_SUF 4
 /INCLUDE CND_4
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS002PAK
       ;;
(GS002PAK)
       m_CondExec 00,EQ,GS002PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES EN TETE ET MVTS POUR 907090                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAM PGM=MERGE      ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAM
       ;;
(GS002PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS002PAA.FGS002AP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GS002PAJ.FGS090AP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS090BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS002PAN
       ;;
(GS002PAN)
       m_CondExec 00,EQ,GS002PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS090BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAQ
       ;;
(GS002PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS002PAQ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FUSION DES EN TETE ET MVTS POUR 907095                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAT PGM=MERGE      ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAT
       ;;
(GS002PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GS002PAA.FGS002AP
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GS002PAJ.FGS095AP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS095BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS002PAU
       ;;
(GS002PAU)
       m_CondExec 00,EQ,GS002PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS095BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS002PAX
       ;;
(GS002PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS002PAX.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FUSION DES EN TETE ET MVTS POUR 907120                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PBA PGM=MERGE      ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS002PBA
       ;;
(GS002PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GS002PAA.FGS002AP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GS002PAJ.FGS120AP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS120BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS002PBB
       ;;
(GS002PBB)
       m_CondExec 00,EQ,GS002PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS120BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS002PBD
       ;;
(GS002PBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS002PBD.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FUSION DES EN TETE ET MVTS POUR 907125                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PBG PGM=MERGE      ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GS002PBG
       ;;
(GS002PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GS002PAA.FGS002AP
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GS002PAJ.FGS125AP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS125BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS002PBH
       ;;
(GS002PBH)
       m_CondExec 00,EQ,GS002PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS125BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS002PBJ PGM=FTP        ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GS002PBJ
       ;;
(GS002PBJ)
       m_CondExec ${EXACD},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS002PBJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS002PZA
       ;;
(GS002PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS002PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
