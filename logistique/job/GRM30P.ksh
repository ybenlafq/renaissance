#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM30P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGRM30 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 19.18.55 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GRM30P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BRM034                                                                
#  EXTRACTION POUR CREER LES FICHIERS D'EDITIONS                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM30PA
       ;;
(GRM30PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRM30PAA
       ;;
(GRM30PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
#    RSGS37   : NAME=RSGS37,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS37 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM26   : NAME=RSRM26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM26 /dev/null
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#    RSRM90   : NAME=RSRM90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM90 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRM034 ${DATA}/PXX0/F07.BRM034AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM034 
       JUMP_LABEL=GRM30PAB
       ;;
(GRM30PAB)
       m_CondExec 04,GE,GRM30PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BRM034 POUR EDITION DE L'ETAT IRM035             
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAD
       ;;
(GRM30PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BRM034AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PAD.IRM035AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "907"
 /FIELDS FLD_BI_17_20 17 CH 20
 /FIELDS FLD_BI_7_5 07 CH 05
 /FIELDS FLD_CH_225_3 225 CH 3
 /FIELDS FLD_BI_12_5 12 CH 05
 /FIELDS FLD_BI_37_5 37 CH 5
 /CONDITION CND_1 FLD_CH_225_3 EQ CST_1_7 
 /KEYS
   FLD_BI_37_5 ASCENDING,
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_20 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM30PAE
       ;;
(GRM30PAE)
       m_CondExec 00,EQ,GRM30PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRM035                                                                
#  CREATION DE L'ETAT IRM035                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAG PGM=BRM035     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAG
       ;;
(GRM30PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A2} FRM035 ${DATA}/PTEM/GRM30PAD.IRM035AP
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EDITION                                                      
       m_OutputAssign -c 9 -w IRM035 IRM035
       m_ProgramExec BRM035 
# *******************************************************************          
#  CREATION DE L'ETAT : IRM036                                                 
#  ---------------------------                                                 
#  TRI DU FICHIER ISSU DU PGM BRM034 POUR EDITION DE L'ETAT IRM036             
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAJ
       ;;
(GRM30PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.BRM034AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PAJ.IRM036BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_14  "IRM036"
 /DERIVEDFIELD CST_1_23 "907"
 /FIELDS FLD_CH_49_3 49 CH 03
 /FIELDS FLD_CH_209_4 209 CH 4
 /FIELDS FLD_CH_197_4 197 CH 4
 /FIELDS FLD_CH_154_4 154 CH 04
 /FIELDS FLD_CH_201_4 201 CH 4
 /FIELDS FLD_CH_37_5 37 CH 05
 /FIELDS FLD_CH_225_3 225 CH 3
 /FIELDS FLD_BI_17_20 17 CH 20
 /FIELDS FLD_CH_217_4 217 CH 4
 /FIELDS FLD_CH_205_4 205 CH 4
 /FIELDS FLD_CH_213_4 213 CH 4
 /FIELDS FLD_BI_12_5 12 CH 05
 /FIELDS FLD_CH_12_5 12 CH 05
 /FIELDS FLD_BI_37_5 37 CH 5
 /FIELDS FLD_CH_48_1 48 CH 01
 /FIELDS FLD_CH_166_4 166 CH 04
 /FIELDS FLD_CH_221_4 221 CH 4
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_BI_7_5 07 CH 05
 /FIELDS FLD_CH_7_5 07 CH 05
 /CONDITION CND_3 FLD_CH_225_3 EQ CST_1_23 
 /KEYS
   FLD_BI_37_5 ASCENDING,
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_154_4,
    TOTAL FLD_CH_166_4,
    TOTAL FLD_CH_209_4,
    TOTAL FLD_CH_213_4,
    TOTAL FLD_CH_217_4,
    TOTAL FLD_CH_221_4,
    TOTAL FLD_CH_197_4,
    TOTAL FLD_CH_201_4,
    TOTAL FLD_CH_205_4
 /* MT_ERROR (unknown field) (C'IRM036',07,05,12,05,17,20,37,05,48,01,49,03 */
 /INCLUDE CND_3
 /* Record Type = F  Record Length = 512 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_14,FLD_CH_7_5,FLD_CH_12_5,FLD_CH_17_20,FLD_CH_37_5,FLD_CH_48_1,FLD_CH_49_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRM30PAK
       ;;
(GRM30PAK)
       m_CondExec 00,EQ,GRM30PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM036                                                 
#  ---------------------------                                                 
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAM
       ;;
(GRM30PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GRM30PAJ.IRM036BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRM30PAM.IRM036CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRM30PAN
       ;;
(GRM30PAN)
       m_CondExec 04,GE,GRM30PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM036                                                 
#  ---------------------------                                                 
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAQ
       ;;
(GRM30PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GRM30PAM.IRM036CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PAQ.IRM036DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM30PAR
       ;;
(GRM30PAR)
       m_CondExec 00,EQ,GRM30PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT IRM036                                                   
#  ---------------------------                                                 
#  PGM : BEG060                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAT
       ;;
(GRM30PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/GRM30PAJ.IRM036BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FCUMULS ${DATA}/PTEM/GRM30PAQ.IRM036DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ -g +1 FEDITION ${DATA}/PXX0/F07.JRM036AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM30PAU
       ;;
(GRM30PAU)
       m_CondExec 04,GE,GRM30PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM037                                                 
#  ---------------------------                                                 
#  TRI DU FICHIER ISSU DU PGM BRM034 POUR EDITION DE L'ETAT IRM037             
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GRM30PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PAX
       ;;
(GRM30PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.BRM034AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PAX.IRM037BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_21 "907"
 /DERIVEDFIELD CST_0_12  "IRM037"
 /FIELDS FLD_CH_166_4 166 CH 04
 /FIELDS FLD_CH_217_4 217 CH 04
 /FIELDS FLD_CH_7_5 07 CH 05
 /FIELDS FLD_CH_45_3 45 CH 03
 /FIELDS FLD_CH_225_3 225 CH 3
 /FIELDS FLD_CH_221_4 221 CH 04
 /FIELDS FLD_BI_7_5 07 CH 05
 /FIELDS FLD_CH_213_4 213 CH 04
 /FIELDS FLD_BI_37_5 37 CH 5
 /FIELDS FLD_BI_12_5 12 CH 05
 /FIELDS FLD_CH_42_3 42 CH 03
 /FIELDS FLD_CH_197_4 197 CH 04
 /FIELDS FLD_BI_17_20 17 CH 20
 /FIELDS FLD_CH_12_5 12 CH 05
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_209_4 209 CH 04
 /FIELDS FLD_CH_162_4 162 CH 04
 /FIELDS FLD_CH_37_5 37 CH 05
 /CONDITION CND_3 FLD_CH_225_3 EQ CST_1_21 
 /KEYS
   FLD_BI_37_5 ASCENDING,
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_162_4,
    TOTAL FLD_CH_166_4,
    TOTAL FLD_CH_209_4,
    TOTAL FLD_CH_213_4,
    TOTAL FLD_CH_217_4,
    TOTAL FLD_CH_221_4,
    TOTAL FLD_CH_197_4
 /* MT_ERROR (unknown field) (C'IRM037',07,05,12,05,17,20,37,05,42,03,45,03 */
 /INCLUDE CND_3
 /* Record Type = F  Record Length = 512 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_12,FLD_CH_7_5,FLD_CH_12_5,FLD_CH_17_20,FLD_CH_37_5,FLD_CH_42_3,FLD_CH_45_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRM30PAY
       ;;
(GRM30PAY)
       m_CondExec 00,EQ,GRM30PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM037                                                 
#  ---------------------------                                                 
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBA
       ;;
(GRM30PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GRM30PAX.IRM037BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRM30PBA.IRM037CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRM30PBB
       ;;
(GRM30PBB)
       m_CondExec 04,GE,GRM30PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM037                                                 
#  ---------------------------                                                 
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBD
       ;;
(GRM30PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GRM30PBA.IRM037CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PBD.IRM037DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM30PBE
       ;;
(GRM30PBE)
       m_CondExec 00,EQ,GRM30PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM037                                                 
#  ---------------------------                                                 
#  PGM : BEG060                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBG
       ;;
(GRM30PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/GRM30PAX.IRM037BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/GRM30PBD.IRM037DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRM037 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM30PBH
       ;;
(GRM30PBH)
       m_CondExec 04,GE,GRM30PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM038                                                 
#  ---------------------------                                                 
#  TRI DU FICHIER IRM036BP POUR EDITION DE L'ETAT IRM038                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBJ
       ;;
(GRM30PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GRM30PAJ.IRM036BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PBJ.IRM038BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_11  "IRM038"
 /FIELDS FLD_BI_17_20 17 CH 20
 /FIELDS FLD_CH_37_5 37 CH 05
 /FIELDS FLD_BI_12_5 12 CH 05
 /FIELDS FLD_CH_46_2 46 CH 02
 /FIELDS FLD_CH_151_4 151 CH 04
 /FIELDS FLD_BI_37_5 37 CH 05
 /FIELDS FLD_CH_143_4 143 CH 04
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_12_5 12 CH 05
 /FIELDS FLD_BI_7_5 07 CH 05
 /FIELDS FLD_CH_55_20 55 CH 20
 /FIELDS FLD_CH_7_5 07 CH 05
 /FIELDS FLD_CH_139_4 139 CH 04
 /FIELDS FLD_CH_123_4 123 CH 04
 /FIELDS FLD_CH_127_4 127 CH 04
 /FIELDS FLD_CH_147_4 147 CH 04
 /KEYS
   FLD_BI_37_5 ASCENDING,
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_123_4,
    TOTAL FLD_CH_127_4,
    TOTAL FLD_CH_139_4,
    TOTAL FLD_CH_143_4,
    TOTAL FLD_CH_147_4,
    TOTAL FLD_CH_151_4
 /* MT_ERROR (unknown field) (C'IRM038',07,05,37,05,12,05,17,20,46,02,55,20 */
 /* Record Type = F  Record Length = 512 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_11,FLD_CH_7_5,FLD_CH_37_5,FLD_CH_12_5,FLD_CH_17_20,FLD_CH_46_2,FLD_CH_55_20
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRM30PBK
       ;;
(GRM30PBK)
       m_CondExec 00,EQ,GRM30PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM038                                                 
#  ---------------------------                                                 
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBM
       ;;
(GRM30PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/GRM30PBJ.IRM038BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRM30PBM.IRM038CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRM30PBN
       ;;
(GRM30PBN)
       m_CondExec 04,GE,GRM30PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM038                                                 
#  ---------------------------                                                 
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBQ
       ;;
(GRM30PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GRM30PBM.IRM038CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM30PBQ.IRM038DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM30PBR
       ;;
(GRM30PBR)
       m_CondExec 00,EQ,GRM30PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT : IRM038                                                 
#  ---------------------------                                                 
#  PGM : BEG060                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBT
       ;;
(GRM30PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/GRM30PBJ.IRM038BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/GRM30PBQ.IRM038DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRM038 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM30PBU
       ;;
(GRM30PBU)
       m_CondExec 04,GE,GRM30PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT DANS EOS                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PBX PGM=IEBGENER   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PBX
       ;;
(GRM30PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A18} SYSUT1 ${DATA}/PXX0/F07.JRM036AP
       m_OutputAssign -c 9 -w IRM036 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRM30PBY
       ;;
(GRM30PBY)
       m_CondExec 00,EQ,GRM30PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# ADC      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=JRM036AP                                                            
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGRM30P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM30PCA PGM=FTP        ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PCA
       ;;
(GRM30PCA)
       m_CondExec ${EXADC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM30PCA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GRM30PCD PGM=EZACFSM1   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PCD
       ;;
(GRM30PCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM30PCD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM30PZA
       ;;
(GRM30PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM30PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
