#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE54D.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGIE54 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/15 AT 15.53.12 BY BURTEC3                      
#    STANDARDS: P  JOBSET: GIE54D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   LE PASSAGE DE LA CHAINE DOIT SE FAIRE APRES QUE LE DEPOT EST FINI          
#                    SON INVENTAIRE                                            
# ********************************************************************         
#  BIE540 : CREATION DU FICHIER DES ECARTS D'INVENTAIRE ENTREPOT               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GIE54DA
       ;;
(GIE54DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GIE54DAA
       ;;
(GIE54DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  ETATS                                                                
#    RSGA11   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  INVENTAIRE                                                           
#    RSIE05   : NAME=RSIE05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  INVENTAIRE                                                           
#    RSIE60   : NAME=RSIE60D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  AVANCEMENT D'INVENTAIRE                                              
#    RSIE10   : NAME=RSIE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  PRMP LYON                                                            
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE LOGISTIQUE                                                     
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIE540 ${DATA}/PGI991/F91.BIE540AD
#                                                                              
# ******  PARAMETRE SOCIETE = 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE540 
       JUMP_LABEL=GIE54DAB
       ;;
(GIE54DAB)
       m_CondExec 04,GE,GIE54DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
#   SUR SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQUENCE RAYON            
#       SEQUENCE FAMILLE                                                       
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAD
       ;;
(GIE54DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI991/F91.BIE540AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PBI0/GIE54DAD.BIE540DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_55_5 55 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54DAE
       ;;
(GIE54DAE)
       m_CondExec 00,EQ,GIE54DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135 : EDITION DES ECARTS D'INVENTAIRE PAR FAMILLE LIEU S/LIEU            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAG
       ;;
(GIE54DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS                                                   
       m_FileAssign -d SHR -g ${G_A2} FIV130 ${DATA}/PBI0/GIE54DAD.BIE540DD
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE (IIV135)                       
       m_OutputAssign -c 9 -w IIV135 IIV135
       m_OutputAssign -c Z IIV135B
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=GIE54DAH
       ;;
(GIE54DAH)
       m_CondExec 04,GE,GIE54DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
#   SUR SOCIETE MAGASIN SEQUENCE RAYON SEQUENCE FAMILLE                        
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAJ
       ;;
(GIE54DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PGI991/F91.BIE540AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PBI0/GIE54DAJ.BIE540ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54DAK
       ;;
(GIE54DAK)
       m_CondExec 00,EQ,GIE54DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV136   EDITION DES ECARTS D'INVENTAIRE PAR FAMILLE PAR LIEU               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAM
       ;;
(GIE54DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS TRIES                                             
       m_FileAssign -d SHR -g ${G_A4} FIV130 ${DATA}/PBI0/GIE54DAJ.BIE540ED
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  ETAT DES ECARTS D'INVENTAIRE VALORISE                                
       m_OutputAssign -c 9 -w IIV136 IIV136
       m_OutputAssign -c Z IIV136B
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV136 
       JUMP_LABEL=GIE54DAN
       ;;
(GIE54DAN)
       m_CondExec 04,GE,GIE54DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
#   SUR SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQUENCE RAYON            
#       SEQUENCE FAMILLE LIBELLE RAYON LIBELLE FAMILLE                         
#       LIBELLE MARQUE ARTICLE                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAQ
       ;;
(GIE54DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PGI991/F91.BIE540AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PBI0/GIE54DAQ.BIE540BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 0
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /CONDITION CND_1 FLD_PD_85_3 EQ CST_1_11 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54DAR
       ;;
(GIE54DAR)
       m_CondExec 00,EQ,GIE54DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140   EDITION DES ECARTS D'INVENTAIRE PAR LIEU S/LIEU                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAT
       ;;
(GIE54DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS TRIE                                              
       m_FileAssign -d SHR -g ${G_A6} FIV140 ${DATA}/PBI0/GIE54DAQ.BIE540BD
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE (IIV140)                       
       m_OutputAssign -c Z IIV140
# ******  ETAT DES ECARTS D INVENTAIRE NON VALORISE (IIV140)                   
       m_OutputAssign -c 9 -w IIV140M IIV140M
       m_OutputAssign -c Z IIV140A
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=GIE54DAU
       ;;
(GIE54DAU)
       m_CondExec 04,GE,GIE54DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
#   SUR SOCIETE MAGASIN SEQUENCE RAYON                                         
#       SEQUENCE FAMILLE LIBELLE RAYON LIBELLE FAMILLE                         
#       LIBELLE MARQUE ARTICLE                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DAX
       ;;
(GIE54DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PGI991/F91.BIE540AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PBI0/GIE54DAX.BIE540CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54DAY
       ;;
(GIE54DAY)
       m_CondExec 00,EQ,GIE54DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV141  : EDITION DES ECARTS D'INVENTAIRE PRODUIT PAR LIEU                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DBA
       ;;
(GIE54DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS TRIES                                             
       m_FileAssign -d SHR -g ${G_A8} FIV141 ${DATA}/PBI0/GIE54DAX.BIE540CD
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE                                
       m_OutputAssign -c 9 -w IIV141 IIV141
       m_OutputAssign -c Z IIV141A
       m_OutputAssign -c Z IIV141M
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV141 
       JUMP_LABEL=GIE54DBB
       ;;
(GIE54DBB)
       m_CondExec 04,GE,GIE54DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GIE54DZA
       ;;
(GIE54DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE54DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
