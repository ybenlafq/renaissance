#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEF10M.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGEF10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/07/04 AT 16.33.05 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GEF10M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BEF000                                                                
#  ------------                                                                
#  LISTE COMPLETE DE L'ENCOURS FOURNISSEUR                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEF10MA
       ;;
(GEF10MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GEF10MAA
       ;;
(GEF10MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSHE00   : NAME=RSHE00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  FICHIER D'EDITION                                                    
#  IEF000   REPORT SYSOUT=(9,IEF000),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF000 ${DATA}/PXX0/F89.IEF000AM
#                                                                              
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 209 -g +1 FEFCSV ${DATA}/PXX0/F89.BEF000AM.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF000 
       JUMP_LABEL=GEF10MAB
       ;;
(GEF10MAB)
       m_CondExec 04,GE,GEF10MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF005                                                                
#  ------------                                                                
#  CREATION DES FICHIERS D'EXTRACTION POUR EDITION                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAD
       ;;
(GEF10MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE00   : NAME=RSHE00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FEF010 ${DATA}/PTEM/GEF10MAD.FEF010AM
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 194 -g +1 FDFCSV ${DATA}/PXX0/F89.BEF005AM.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF005 
       JUMP_LABEL=GEF10MAE
       ;;
(GEF10MAE)
       m_CondExec 04,GE,GEF10MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEF010AM ENTRANT DANS LE BEF010                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAG
       ;;
(GEF10MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GEF10MAD.FEF010AM
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GEF10MAG.FEF010BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_80_5 80 CH 5
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_CH_72_8 72 CH 8
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_CH_65_7 65 CH 7
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_80_5 ASCENDING,
   FLD_CH_72_8 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10MAH
       ;;
(GEF10MAH)
       m_CondExec 00,EQ,GEF10MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF010                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF010 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PV TTC                                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAJ PGM=BEF010     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAJ
       ;;
(GEF10MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A2} FEF010 ${DATA}/PTEM/GEF10MAG.FEF010BM
# ------  EDITION DE L'ETAT IEF010                                             
#  IEF010   REPORT SYSOUT=(9,IEF010),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF010 ${DATA}/PXX0/F89.IEF010AM
       m_ProgramExec BEF010 
# ********************************************************************         
#  TRI DU FICHIER FEF010AM ENTRANT DANS LES PGMS BEF011 ET BEF012              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAM
       ;;
(GEF10MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GEF10MAD.FEF010AM
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GEF10MAM.FEF010CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_72_6 72 CH 6
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_CH_51_5 51 CH 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_78_2 78 CH 2
 /KEYS
   FLD_CH_51_5 ASCENDING,
   FLD_CH_72_6 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_78_2 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10MAN
       ;;
(GEF10MAN)
       m_CondExec 00,EQ,GEF10MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF011                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF011 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAQ PGM=BEF011     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAQ
       ;;
(GEF10MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A4} FEF010 ${DATA}/PTEM/GEF10MAM.FEF010CM
# ------  EDITION DE L'ETAT IEF010                                             
#  IEF011   REPORT SYSOUT=(9,IEF011),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF011 ${DATA}/PXX0/F89.IEF011AM
       m_ProgramExec BEF011 
# ********************************************************************         
#  PGM : BEF012                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF012 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAT PGM=BEF012     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAT
       ;;
(GEF10MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BEF005                                          
       m_FileAssign -d SHR -g ${G_A5} FEF010 ${DATA}/PTEM/GEF10MAM.FEF010CM
# ------  EDITION DE L'ETAT IEF010                                             
#  IEF012   REPORT SYSOUT=(9,IEF012),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF012 ${DATA}/PXX0/F89.IEF012AM
       m_ProgramExec BEF012 
# ********************************************************************         
#  PGM : BEF013                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF013 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PRMP                                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MAX PGM=BEF013     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MAX
       ;;
(GEF10MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A6} FEF010 ${DATA}/PTEM/GEF10MAG.FEF010BM
# ------  EDITION DE L'ETAT IEF013                                             
#  IEF013   REPORT SYSOUT=(9,IEF013),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF013 ${DATA}/PXX0/F89.IEF013AM
       m_ProgramExec BEF013 
# ********************************************************************         
#    CREATION DE L'ETAT DANS EOS                                               
# ********************************************************************         
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MBA PGM=IEBGENER   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MBA
       ;;
(GEF10MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A7} SYSUT1 ${DATA}/PXX0/F89.IEF000AM
       m_OutputAssign -c 9 -w IEF000 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10MBB
       ;;
(GEF10MBB)
       m_CondExec 00,EQ,GEF10MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MBD PGM=IEBGENER   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MBD
       ;;
(GEF10MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A8} SYSUT1 ${DATA}/PXX0/F89.IEF010AM
       m_OutputAssign -c 9 -w IEF010 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10MBE
       ;;
(GEF10MBE)
       m_CondExec 00,EQ,GEF10MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MBG PGM=IEBGENER   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MBG
       ;;
(GEF10MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A9} SYSUT1 ${DATA}/PXX0/F89.IEF011AM
       m_OutputAssign -c 9 -w IEF011 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10MBH
       ;;
(GEF10MBH)
       m_CondExec 00,EQ,GEF10MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MBJ
       ;;
(GEF10MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A10} SYSUT1 ${DATA}/PXX0/F89.IEF012AM
       m_OutputAssign -c 9 -w IEF012 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10MBK
       ;;
(GEF10MBK)
       m_CondExec 00,EQ,GEF10MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10MBM PGM=IEBGENER   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MBM
       ;;
(GEF10MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A11} SYSUT1 ${DATA}/PXX0/F89.IEF013AM
       m_OutputAssign -c 9 -w IEF013 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10MBN
       ;;
(GEF10MBN)
       m_CondExec 00,EQ,GEF10MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GEF10MZA
       ;;
(GEF10MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
