#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF00P.ksh                       --- VERSION DU 17/10/2016 18:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQMF00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/29 AT 16.24.10 BY BURTEC2                      
#    STANDARDS: P  JOBSET: QMF00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BQ028D : REQUETE Q028D                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF00PA
       ;;
(QMF00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+2'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+2'}
       LASTJO=${LASTJO:-YES}
       QMF00PA=${DIMANCH}
       QMF00PB=${LUNDI}
       QMF00PC=${SAMEDI}
       QMF00PD=${QMF00PD:-'YES'}
       QMF00PE=${VENDRED}
       QMF00PF=${JEUDI}
       RUN=${RUN}
       JUMP_LABEL=QMF00PAA
       ;;
(QMF00PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES SAUVE MATIN (SA202P)        *                                       
# **            GM070P PLANTE =>> QMF028 EST FAUX                              
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FIC EDITION POUR IMP. GENERALISEE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FQ028D ${DATA}/PTEM/QMF00PAA.Q028DAP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQ028D 
       JUMP_LABEL=QMF00PAB
       ;;
(QMF00PAB)
       m_CondExec 04,GE,QMF00PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (Q028DAP)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAD
       ;;
(QMF00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/QMF00PAA.Q028DAP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF00PAD.Q028DBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_48_7 48 CH 07
 /FIELDS FLD_CH_30_5 30 CH 5
 /FIELDS FLD_CH_35_5 35 CH 5
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_CH_40_5 40 CH 5
 /FIELDS FLD_CH_45_3 45 CH 03
 /FIELDS FLD_CH_7_3 7 CH 3
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_5 ASCENDING,
   FLD_CH_35_5 ASCENDING,
   FLD_CH_40_5 ASCENDING,
   FLD_CH_45_3 ASCENDING,
   FLD_CH_48_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF00PAE
       ;;
(QMF00PAE)
       m_CondExec 00,EQ,QMF00PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAG
       ;;
(QMF00PAG)
       m_CondExec ${EXAAK},NE,YES 1,NE,$[QMF00PE] 1,NE,$[QMF00PC] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/QMF00PAD.Q028DBP
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/QMF00PAG.Q028DCP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF00PAH
       ;;
(QMF00PAH)
       m_CondExec 04,GE,QMF00PAG ${EXAAK},NE,YES 1,NE,$[QMF00PE] 1,NE,$[QMF00PC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAJ
       ;;
(QMF00PAJ)
       m_CondExec ${EXAAP},NE,YES 1,NE,$[QMF00PE] 1,NE,$[QMF00PC] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/QMF00PAG.Q028DCP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF00PAJ.Q028DDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF00PAK
       ;;
(QMF00PAK)
       m_CondExec 00,EQ,QMF00PAJ ${EXAAP},NE,YES 1,NE,$[QMF00PE] 1,NE,$[QMF00PC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : Q028D                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAM
       ;;
(QMF00PAM)
       m_CondExec ${EXAAU},NE,YES 1,NE,$[QMF00PE] 1,NE,$[QMF00PC] 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/QMF00PAD.Q028DBP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/QMF00PAJ.Q028DDP
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w Q028D FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF00PAN
       ;;
(QMF00PAN)
       m_CondExec 04,GE,QMF00PAM ${EXAAU},NE,YES 1,NE,$[QMF00PE] 1,NE,$[QMF00PC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAQ
       ;;
(QMF00PAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[QMF00PF] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/QMF00PAD.Q028DBP
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g ${G_A7} FCUMULS ${DATA}/PTEM/QMF00PAG.Q028DCP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF00PAR
       ;;
(QMF00PAR)
       m_CondExec 04,GE,QMF00PAQ ${EXAAZ},NE,YES 1,EQ,$[QMF00PF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAT
       ;;
(QMF00PAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[QMF00PF] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/QMF00PAG.Q028DCP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g ${G_A9} SORTOUT ${DATA}/PTEM/QMF00PAJ.Q028DDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF00PAU
       ;;
(QMF00PAU)
       m_CondExec 00,EQ,QMF00PAT ${EXABE},NE,YES 1,EQ,$[QMF00PF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : Q028D                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PAX
       ;;
(QMF00PAX)
       m_CondExec ${EXABJ},NE,YES 1,EQ,$[QMF00PF] 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/QMF00PAD.Q028DBP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/QMF00PAJ.Q028DDP
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w Q028D1 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF00PAY
       ;;
(QMF00PAY)
       m_CondExec 04,GE,QMF00PAX ${EXABJ},NE,YES 1,EQ,$[QMF00PF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q023                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBA
       ;;
(QMF00PBA)
       m_CondExec ${EXABO},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF023 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023P (&&DEBUT='$QMFDEB_ANNMMJJ' &&FIN='$QMFFIN_ANNMMJJ' &&SOC='$DIFDEP_1_3' FORM=F023
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBB
       ;;
(QMF00PBB)
       m_CondExec 04,GE,QMF00PBA ${EXABO},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q023 DU PREMIER AU JOUR DE PASSAGE HEBDO                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBD
       ;;
(QMF00PBD)
       m_CondExec ${EXABT},NE,YES 1,EQ,$[QMF00PD] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF0232 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023P (&&DEBUT='$VDATEJ_ANNMM01' &&FIN='$VDATEJ_ANNMMJJ' &&SOC='$DIFDEP_1_3' FORM=F023
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00PY -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBE
       ;;
(QMF00PBE)
       m_CondExec 04,GE,QMF00PBD ${EXABT},NE,YES 1,EQ,$[QMF00PD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q023 DU PREMIER AU JOUR DE PASSAGE MENS                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBG
       ;;
(QMF00PBG)
       m_CondExec ${EXABY},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF023M DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023P (&&DEBUT='$VDATEJ_ANNMM01' &&FIN='$VDATEJ_ANNMMJJ' &&SOC='$DIFDEP_1_3' FORM=F023
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00PY -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBH
       ;;
(QMF00PBH)
       m_CondExec 04,GE,QMF00PBG ${EXABY},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q155                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBJ
       ;;
(QMF00PBJ)
       m_CondExec ${EXACD},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF155 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q155 (&&JOUR='$FDATE_5_4-$FDATE_3_2-$FDATE_1_2' FORM=F155
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBK
       ;;
(QMF00PBK)
       m_CondExec 04,GE,QMF00PBJ ${EXACD},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q018                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBM
       ;;
(QMF00PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF018 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q018 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=F018
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBN
       ;;
(QMF00PBN)
       m_CondExec 04,GE,QMF00PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q020                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBQ
       ;;
(QMF00PBQ)
       m_CondExec ${EXACN},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF020 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q020 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=F020
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P8 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBR
       ;;
(QMF00PBR)
       m_CondExec 04,GE,QMF00PBQ ${EXACN},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q176 (LISTE DE SUIVI DES MVTS DES HS AAF DRF RFC         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBT
       ;;
(QMF00PBT)
       m_CondExec ${EXACS},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF176 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF00P11
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P11 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBU
       ;;
(QMF00PBU)
       m_CondExec 04,GE,QMF00PBT ${EXACS},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q035 (EN COURS DE B.E SUR RTGV35)                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PBX
       ;;
(QMF00PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF035 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF00P12
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P12 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PBY
       ;;
(QMF00PBY)
       m_CondExec 04,GE,QMF00PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q035 (EN COURS DE B.E SUR RTGV35)                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCA
       ;;
(QMF00PCA)
       m_CondExec ${EXADC},NE,YES 1,EQ,$[QMF00PF] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF035 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF00P13
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P13 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCB
       ;;
(QMF00PCB)
       m_CondExec 04,GE,QMF00PCA ${EXADC},NE,YES 1,EQ,$[QMF00PF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q024                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCD
       ;;
(QMF00PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF024 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q024 (&&JOUR='$VDATEJ_ANNMMDD' FORM=F024
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P14 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCE
       ;;
(QMF00PCE)
       m_CondExec 04,GE,QMF00PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q164                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCG
       ;;
(QMF00PCG)
       m_CondExec ${EXADM},NE,YES 1,EQ,$[QMF00PB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF164 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q164 (&&DEBUT='$QMFDEB8_ANNMMJJ' &&FIN='$QMFFIN8_ANNMMJJ' FORM=F164
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P15 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCH
       ;;
(QMF00PCH)
       m_CondExec 04,GE,QMF00PCG ${EXADM},NE,YES 1,EQ,$[QMF00PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q175 (LISTE DES MVTS HS DES LIEUX AAF DRF RFC )          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCJ
       ;;
(QMF00PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF175 DSQPRINT
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -i QMFPARM
RUN Q175 (&&JOUR='$VDATEJ_ANNMMDD' &&SOC='$DIFDEP_1_3' FORM=F175
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P16 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCK
       ;;
(QMF00PCK)
       m_CondExec 04,GE,QMF00PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QB05 (LISTE QTE VOLUME PAR MAGS DES MUTS DU JOUR         
# ********************************************************************         
# ACX      STEP  PGM=IKJEFT01,PATTERN=EO4,PARMS=(L=QGB05,S=RDAR,               
#                M=QMF00P17)                                                   
# QMFPARM  DATA  CLASS=VAR,PARMS=QGB05P,MBR=QMF00P17                           
# ********************************************************************         
#  QMFBATCH : REQUETE Q178 (RESERVATIONS DES CMDS MAGASINS)                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCM
       ;;
(QMF00PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF178 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q178 (&&DATE='$VDATEJ_ANNMMDD' FORM=F178
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P18 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCN
       ;;
(QMF00PCN)
       m_CondExec 04,GE,QMF00PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q023 TOUS LES JOURS                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCQ
       ;;
(QMF00PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF0233 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023 (&&DEBUT='$VDATEJ_ANNMMJJ' &&FIN='$VDATEJ_ANNMMJJ' &&SOC='$DIFDEP_1_3' FORM=F023
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P22 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCR
       ;;
(QMF00PCR)
       m_CondExec 04,GE,QMF00PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QQF QPR103                                                           
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCT
       ;;
(QMF00PCT)
       m_CondExec ${EXAEG},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QPR103 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
       m_FileAssign -i HOSTIN
DATEDEB=$QMFDEB_ANNMMJJ
_end
       m_FileAssign -i -C
DATEFIN=$QMFFIN_ANNMMJJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QPR103 
       JUMP_LABEL=QMF00PCU
       ;;
(QMF00PCU)
       m_CondExec 04,GE,QMF00PCT ${EXAEG},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGF001                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PCX
       ;;
(QMF00PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGF001 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QGF001P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QGF001P -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PCY
       ;;
(QMF00PCY)
       m_CondExec 04,GE,QMF00PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGF002                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDA
       ;;
(QMF00PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGF002 DSQPRINT
       m_FileAssign -i QMFPARM
RUN QGF002 (&&JOUR='$VDATEJ_ANNMMJJ' FORM=FGF002
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P23 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PDB
       ;;
(QMF00PDB)
       m_CondExec 04,GE,QMF00PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q035A (B.E ANNULES)                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDD
       ;;
(QMF00PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF035A DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF00P24
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P24 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PDE
       ;;
(QMF00PDE)
       m_CondExec 04,GE,QMF00PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QMUTVAL                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDG
       ;;
(QMF00PDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMUTVAL DSQPRINT
       m_FileAssign -i QMFPARM
RUN QMUTVAL (&&DATE1='$QMFDEB_ANNMMJJ' &&DATE2='$QMFFIN_ANNMMJJ' FORM=FMUTVAL
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P27 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PDH
       ;;
(QMF00PDH)
       m_CondExec 04,GE,QMF00PDG ${EXAFA},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QMVTGSM (HEBDO)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDJ
       ;;
(QMF00PDJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMVTGSM DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QMVTGSM (&&DEB='$QMFDEB_ANNMM01' &&FIN='$QMFFIN_ANNMMJJ' FORM=ADMFIL.FMVTGSM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P28 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PDK
       ;;
(QMF00PDK)
       m_CondExec 04,GE,QMF00PDJ ${EXAFF},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QMVTGSM (MENSUEL)                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDM
       ;;
(QMF00PDM)
       m_CondExec ${EXAFK},NE,YES 1,EQ,$[QMF00PD] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMVTGSM DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QMVTGSM (&&DEB='$VDATEJ_ANNMM01' &&FIN='$VDATEJ_ANNMMJJ' FORM=ADMFIL.FMVTGSM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P29 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PDN
       ;;
(QMF00PDN)
       m_CondExec 04,GE,QMF00PDM ${EXAFK},NE,YES 1,EQ,$[QMF00PD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGSM7H                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDQ PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDQ
       ;;
(QMF00PDQ)
       m_CondExec ${EXAFU},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGSM7H DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QGSM7H (&&DATEDEB='$QMFDEB_ANNMMJJ' &&DATEFIN='$QMFFIN_ANNMMJJ' FORM=ADMFIL.FGSM7H
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P32 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PDR
       ;;
(QMF00PDR)
       m_CondExec 04,GE,QMF00PDQ ${EXAFU},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BABTLS : ABONNEMENTS LIBERTY SURF                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDT PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDT
       ;;
(QMF00PDT)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC EDITION POUR IMP. GENERALISEE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FQABTLSJ ${DATA}/PTEM/QMF00PDT.QABTLSAP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BABTLS 
       JUMP_LABEL=QMF00PDU
       ;;
(QMF00PDU)
       m_CondExec 04,GE,QMF00PDT ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (QABTLSAP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PDX PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PDX
       ;;
(QMF00PDX)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/QMF00PDT.QABTLSAP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF00PDX.QABTLSBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_2 20 CH 2
 /FIELDS FLD_CH_28_3 28 CH 03
 /FIELDS FLD_CH_22_3 22 CH 3
 /FIELDS FLD_CH_25_3 25 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_12_8 12 CH 8
 /KEYS
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_8 ASCENDING,
   FLD_CH_20_2 ASCENDING,
   FLD_CH_22_3 ASCENDING,
   FLD_CH_25_3 ASCENDING,
   FLD_CH_28_3 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF00PDY
       ;;
(QMF00PDY)
       m_CondExec 00,EQ,QMF00PDX ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PEA PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PEA
       ;;
(QMF00PEA)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/QMF00PDX.QABTLSBP
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/QMF00PEA.QABTLSCP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF00PEB
       ;;
(QMF00PEB)
       m_CondExec 04,GE,QMF00PEA ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PED PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PED
       ;;
(QMF00PED)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/QMF00PEA.QABTLSCP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF00PED.QABTLSDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF00PEE
       ;;
(QMF00PEE)
       m_CondExec 00,EQ,QMF00PED ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : QABTLS                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PEG PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PEG
       ;;
(QMF00PEG)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/QMF00PDX.QABTLSBP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/QMF00PED.QABTLSDP
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w QABTLSJ FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF00PEH
       ;;
(QMF00PEH)
       m_CondExec 04,GE,QMF00PEG ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q023F (QMF FILIALES)                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PEJ PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PEJ
       ;;
(QMF00PEJ)
       m_CondExec ${EXAGY},NE,YES 1,EQ,$[QMF00PB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF023F DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023F (&&DEBUT='$QMFDEB8_ANNMMJJ' &&FIN='$QMFFIN8_ANNMMJJ' FORM=F023F
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P33 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PEK
       ;;
(QMF00PEK)
       m_CondExec 04,GE,QMF00PEJ ${EXAGY},NE,YES 1,EQ,$[QMF00PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q023F (QMF FILIALES)                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PEM PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PEM
       ;;
(QMF00PEM)
       m_CondExec ${EXAHD},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF023F DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023F (&&DEBUT='$VDATEJ_ANNMM01' &&FIN='$VDATEJ_ANNMMJJ' FORM=F023F
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P34 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PEN
       ;;
(QMF00PEN)
       m_CondExec 04,GE,QMF00PEM ${EXAHD},NE,YES ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q176VA (LISTE SUIVI DES MVTS DES HS AAF DRF RFC)         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PEQ PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PEQ
       ;;
(QMF00PEQ)
       m_CondExec ${EXAHI},NE,YES 1,EQ,$[QMF00PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF176VA DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF00P35
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P35 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PER
       ;;
(QMF00PER)
       m_CondExec 04,GE,QMF00PEQ ${EXAHI},NE,YES 1,EQ,$[QMF00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QDIFMARGT                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PET PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PET
       ;;
(QMF00PET)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMARGT DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QMARGT (&&FDATE='$VDATEJ_ANNMMJJ' FORM=ADMFIL.FMARGT
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P36 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PEU
       ;;
(QMF00PEU)
       m_CondExec 04,GE,QMF00PET ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QDIFMARGD                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF00PEX PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PEX
       ;;
(QMF00PEX)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMARGD DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QMARGD (&&FDATE='$VDATEJ_ANNMMJJ' FORM=ADMFIL.FMARGD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00P37 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF00PEY
       ;;
(QMF00PEY)
       m_CondExec 04,GE,QMF00PEX ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=QMF00PZA
       ;;
(QMF00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QMF00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
