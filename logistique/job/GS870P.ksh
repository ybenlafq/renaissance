#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS870P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS870 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/04/13 AT 11.43.31 BY PREPA2                       
#    STANDARDS: P  JOBSET: GS870P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FLG863 (VENANT DE LG860P)                                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS870PA
       ;;
(GS870PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS870PAA
       ;;
(GS870PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG863P
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -g +1 SORTOUT ${DATA}/PTEM/GS870PAA.BGS870AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_7 10 CH 07
 /KEYS
   FLD_BI_10_7 ASCENDING
 /* Record Type = F  Record Length = 305 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS870PAB
       ;;
(GS870PAB)
       m_CondExec 00,EQ,GS870PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FLG865 (VENANT DE LG860P)                                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS870PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS870PAD
       ;;
(GS870PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PTEM/GS870PAD.BGS870BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_7 07 CH 07
 /KEYS
   FLD_BI_7_7 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS870PAE
       ;;
(GS870PAE)
       m_CondExec 00,EQ,GS870PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FLG866 (VENANT DE LG860P)                                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS870PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS870PAG
       ;;
(GS870PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG866P
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 SORTOUT ${DATA}/PTEM/GS870PAG.BGS870CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 01 CH 07
 /KEYS
   FLD_BI_1_7 ASCENDING
 /* Record Type = F  Record Length = 140 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS870PAH
       ;;
(GS870PAH)
       m_CondExec 00,EQ,GS870PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGS870                                                                
#  ------------                                                                
#  EXTRACTION DES DONNEES SERVANT AUX EDITIONS ET CREATION DU FICHIER          
#  IGS870 POUR LE GENERATEUR D'ETAT                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS870PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS870PAJ
       ;;
(GS870PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLES EN LECTURE                                                     
#    RSGA10P  : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10P /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A1} FLG863 ${DATA}/PTEM/GS870PAA.BGS870AP
       m_FileAssign -d SHR -g ${G_A2} FLG865 ${DATA}/PTEM/GS870PAD.BGS870BP
       m_FileAssign -d SHR -g ${G_A3} FLG866 ${DATA}/PTEM/GS870PAG.BGS870CP
# ******                                                                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GS870PAJ
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -g +1 FGS870 ${DATA}/PTEM/GS870PAJ.BGS870DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS870 
       JUMP_LABEL=GS870PAK
       ;;
(GS870PAK)
       m_CondExec 04,GE,GS870PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGS870                                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS870PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS870PAM
       ;;
(GS870PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS870PAJ.BGS870DP
# SORTIE - ON CONSERVE LE FICHIER TRIE SI BESOIN POUR RESSORTIR ETATS          
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -g +1 SORTOUT ${DATA}/PTEM/F07.BGS870EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_6 01 CH 06
 /FIELDS FLD_CH_73_3 73 CH 03
 /FIELDS FLD_BI_58_3 58 CH 03
 /FIELDS FLD_CH_76_3 76 CH 03
 /FIELDS FLD_CH_79_9 79 CH 09
 /FIELDS FLD_CH_64_9 64 CH 09
 /KEYS
   FLD_BI_1_6 ASCENDING,
   FLD_BI_58_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_64_9,
    TOTAL FLD_CH_73_3,
    TOTAL FLD_CH_76_3,
    TOTAL FLD_CH_79_9
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS870PAN
       ;;
(GS870PAN)
       m_CondExec 00,EQ,GS870PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGS871                                                                
#  ------------                                                                
#  CREATION ETATS IGS870 / IGS871                                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS870PAQ PGM=BGS871     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS870PAQ
       ;;
(GS870PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A5} FGS870 ${DATA}/PTEM/F07.BGS870EP
# ------  ETATS EN SORTIE                                                      
# IGS870  FILE  NAME=BGS871AP,MODE=O                                           
       m_OutputAssign -c 9 -w IGS870 IGS870
# IGS871  FILE  NAME=BGS871BP,MODE=O                                           
       m_OutputAssign -c 9 -w IGS871 IGS871
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
       m_ProgramExec BGS871 
# ********************************************************************         
#  PGM : BGS872                                                                
#  ------------                                                                
#  CREATION ETAT  IGS872                                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS870PAT PGM=BGS872     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GS870PAT
       ;;
(GS870PAT)
       m_CondExec ${EXABE},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A6} FGS870 ${DATA}/PTEM/F07.BGS870EP
# ------  ETATS EN SORTIE                                                      
# IGS872   FILE  NAME=BGS872AP,MODE=O                                          
       m_OutputAssign -c 9 -w IGS872 IGS872
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
       m_ProgramExec BGS872 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS870PZA
       ;;
(GS870PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS870PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
