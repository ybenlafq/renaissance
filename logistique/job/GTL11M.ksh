#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL11M.ksh                       --- VERSION DU 20/10/2016 12:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGTL11 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 09.10.12 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GTL11M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BTL110                                                                
#  EXTRACTION PERMETTANT D'ALIMENTER LA TABLE RTTL09                           
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
#           NON SI FIN NORMALE FAIRE UN RECOVER A LA F.I.C DE RTTL09           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL11MA
       ;;
(GTL11MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL11MAA
       ;;
(GTL11MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES QUOTAS DE LIVRAISON         
#    RSGQ01M  : NAME=RSGQ01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01M /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40M  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40M /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02M /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23M  : NAME=RSGV23M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23M /dev/null
# ************************************** TABLE DES TOUNEES (GENERALITE         
#    RSTL01M  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL01M /dev/null
# ************************************** TABLE DES TOURNEES (DETAILS)          
#    RSTL02M  : NAME=RSTL02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02M /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04M  : NAME=RSTL04M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09M  : NAME=RSTL09M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09M /dev/null
# ************************************** TABLE HISTO VENTES                    
#    RSHV15M  : NAME=RSHV15M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15M /dev/null
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DALDEP
# ************************************** PARAMETRE  H:HEBDO  M:MENSUEL         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11MAA
# ************************************** FTL110 ISSU DU DERNIER TRAIT          
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL110N ${DATA}/PXX0/F89.FTL110AM
# ************************************** FIC D'EXTRACT FTL111                  
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111 ${DATA}/PXX0/F89.BTL111AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL11MAB
       ;;
(GTL11MAB)
       m_CondExec 04,GE,GTL11MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE : CUMUL DU FICHIER BTL111AM CREE PAR LE PROG BTL110 AVEC              
#          L'ANCIEN BTL111AY DU DERNIER TRAITEMENT                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAD
       ;;
(GTL11MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F89.BTL111AM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F89.BTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A3} SORTOUT ${DATA}/PXX0/F89.BTL111AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_15_5 15 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MAE
       ;;
(GTL11MAE)
       m_CondExec 00,EQ,GTL11MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL111                                                                
#  INITIALISATION DES FICHIERS FTL111A  ET FTL111B                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAG
       ;;
(GTL11MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISE                      
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FIC D'EXTRACT ISSU DU BLT110          
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** FIC D'EXTRACT ISSU DU TRI 1A          
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F89.BTL111AM
# ************************************** FICHIER FTL111A                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111A ${DATA}/PXX0/F89.FTL111AM
# ************************************** FICHIER  TL111B                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111B ${DATA}/PXX0/F89.FTL111BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL11MAH
       ;;
(GTL11MAH)
       m_CondExec 04,GE,GTL11MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2A : TRI DU FICHIER FTL111AM POUR CREATION DU MLT112AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAJ
       ;;
(GTL11MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MAJ.MTL112AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MAK
       ;;
(GTL11MAK)
       m_CondExec 00,EQ,GTL11MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2B : TRI DU FICHIER FTL111AM POUR CREATION DU MTL112BM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAM
       ;;
(GTL11MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MAM.MTL112BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MAN
       ;;
(GTL11MAN)
       m_CondExec 00,EQ,GTL11MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2C : TRI DU FICHIER FTL111BM POUR CREATION DU MTL112CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAQ
       ;;
(GTL11MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MAQ.MTL112CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MAR
       ;;
(GTL11MAR)
       m_CondExec 00,EQ,GTL11MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL112                                                                
#  CREATION DES ETATS ITL112A ET ITL112B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAT
       ;;
(GTL11MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** FICHIERS ISSU DU TRI 2A               
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL11MAJ.MTL112AM
# ************************************** FICHIERS ISSU DU TRI 2B               
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL11MAM.MTL112BM
# ************************************** FICHIERS ISSU DU TRI 2C               
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL11MAQ.MTL112CM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L                  
#  ITL112A  REPORT SYSOUT=(9,ITL112A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL112A ${DATA}/PXX0/F89.ITL112AF.MICROFIC
# ************************************** ETAT DE STAT PAR PIECES               
#  ITL112B  REPORT SYSOUT=(9,ITL112B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL112B ${DATA}/PXX0/F89.ITL112BF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL11MAU
       ;;
(GTL11MAU)
       m_CondExec 04,GE,GTL11MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3A : TRI DU FICHIER FTL111AM POUR CREATION DU MTL113AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MAX
       ;;
(GTL11MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MAX.MTL113AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_95_2 95 CH 2
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MAY
       ;;
(GTL11MAY)
       m_CondExec 00,EQ,GTL11MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3B : TRI DU FICHIER FTL111AM POUR CREATION DU MTL113BM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBA
       ;;
(GTL11MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MBA.MTL113BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MBB
       ;;
(GTL11MBB)
       m_CondExec 00,EQ,GTL11MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3C : TRI DU FICHIER FTL111BM POUR CREATION DU MTL113CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBD
       ;;
(GTL11MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MBD.MTL113CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MBE
       ;;
(GTL11MBE)
       m_CondExec 00,EQ,GTL11MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3D : TRI DU FICHIER FTL111BM POUR CREATION DU MTL113CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBG
       ;;
(GTL11MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MBG.MTL113DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MBH
       ;;
(GTL11MBH)
       m_CondExec 00,EQ,GTL11MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL113                                                                
#  CREATION DE L'ETAT ITL113                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBJ
       ;;
(GTL11MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** FICHIERS ISSU DU TRI 3A               
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL11MAX.MTL113AM
# ************************************** FICHIERS ISSU DU TRI 3B               
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL11MBA.MTL113BM
# ************************************** FICHIERS ISSU DU TRI 3C               
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL11MBD.MTL113CM
# ************************************** FICHIERS ISSU DU TRI 3D               
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL11MBG.MTL113DM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR         
#  ITL113   REPORT SYSOUT=(9,ITL113),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL113 ${DATA}/PXX0/F89.ITL113AF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL11MBK
       ;;
(GTL11MBK)
       m_CondExec 04,GE,GTL11MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4A : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBM
       ;;
(GTL11MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MBM.FTL114AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MBN
       ;;
(GTL11MBN)
       m_CondExec 00,EQ,GTL11MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4B : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114BM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBQ
       ;;
(GTL11MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MBQ.FTL114BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MBR
       ;;
(GTL11MBR)
       m_CondExec 00,EQ,GTL11MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4C : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBT
       ;;
(GTL11MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MBT.FTL114CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MBU
       ;;
(GTL11MBU)
       m_CondExec 00,EQ,GTL11MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL114                                                                
#  CREATION DES ETATS ITL114A ET ITL114B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MBX
       ;;
(GTL11MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02M /dev/null
# ************************************** TABLE DES ENTETES DE VENTES           
#    RSGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23M  : NAME=RSGV23M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23M /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04M  : NAME=RSTL04M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A               
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL11MBM.FTL114AM
# ************************************** FICHIERS ISSU DU TRI 4B               
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL11MBQ.FTL114BM
# ************************************** FICHIERS ISSU DU TRI 4C               
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL11MBT.FTL114CM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L             
#  ITL114A  REPORT SYSOUT=(9,ITL114A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL114A ${DATA}/PXX0/F89.ITL114AF.MICROFIC
# ************************************** STAT RETOURS MAGS PAR PIECES          
#  ITL114B  REPORT SYSOUT=(9,ITL114B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL114B ${DATA}/PXX0/F89.ITL114BF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL11MBY
       ;;
(GTL11MBY)
       m_CondExec 04,GE,GTL11MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 5A : TRI DU FICHIER FTL111BM POUR CREATION DU FTL115AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCA
       ;;
(GTL11MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MCA.FTL115AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_106_2 106 CH 2
 /FIELDS FLD_CH_99_2 99 CH 2
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_106_2 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_99_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MCB
       ;;
(GTL11MCB)
       m_CondExec 00,EQ,GTL11MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL115                                                                
#  CREATION DES ETATS ITL115A ET ITL115B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCD
       ;;
(GTL11MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 5A               
       m_FileAssign -d SHR -g ${G_A29} FTL115 ${DATA}/PTEM/GTL11MCA.FTL115AM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** SYNTHESE RETOUR MAG PAR B.L           
#  ITL115A  REPORT SYSOUT=(9,ITL115A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL115A ${DATA}/PXX0/F89.ITL115AF.MICROFIC
# ************************************** SYNTHESE RETOUR MAG PAR PIECE         
#  ITL115B  REPORT SYSOUT=(9,ITL115B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL115B ${DATA}/PXX0/F89.ITL115BF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL115 
       JUMP_LABEL=GTL11MCE
       ;;
(GTL11MCE)
       m_CondExec 04,GE,GTL11MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL116                                                                
#  CREATION DE L'ETAT ITL116                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCG
       ;;
(GTL11MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIVREURS                    
#    RSTL04M  : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09M  : NAME=RSTL09M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU BTL110               
       m_FileAssign -d SHR -g ${G_A30} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE             
#  ITL116   REPORT SYSOUT=(9,ITL116),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL116 ${DATA}/PXX0/F89.ITL116AF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL11MCH
       ;;
(GTL11MCH)
       m_CondExec 04,GE,GTL11MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL117                                                                
#  CREATION DU FICHIER FTL117AY                                                
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCJ
       ;;
(GTL11MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV         
#    RSGA13M  : NAME=RSGA13M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13M /dev/null
# ************************************** TABLE DES FAMILLES                    
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES           
#    RSGA30M  : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30M /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40M  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT                       
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DALDEP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11MCJ
# ************************************** FIC D'EXTRACT FTL117AY                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117 ${DATA}/PTEM/GTL10MCG.FTL117AM
# *****   FICHIER A DESTINATION DE COPERNIC                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117B ${DATA}/PTEM/GTL10MCG.FTL117BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       #Untranslated IKJEFT01 with unknown command
# DATA UNTRANSLATED: 
# SYSTSIN  DATA  *,CLASS=FIX2                                              
#  DSN SYSTEM(RDAR)                                                        
#  RUN PROGRAM(BTL117) PLAN(BTL117M)                                       
#  END                                                                     

       JUMP_LABEL=GTL11MCK
       ;;
(GTL11MCK)
       m_CondExec 04,GE,GTL11MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 6A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL118AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCM
       ;;
(GTL11MCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MCJ.FTL118AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_12_5 12 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MCN
       ;;
(GTL11MCN)
       m_CondExec 00,EQ,GTL11MCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL118                                                                
#  CREATION DE L'ETAT ITL118                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCQ
       ;;
(GTL11MCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A               
       m_FileAssign -d SHR -g ${G_A32} FTL118 ${DATA}/PTEM/GTL10MCJ.FTL118AM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11MCQ
# ************************************** MODE DE DELIV/MAG ET FAM              
#  ITL118   REPORT SYSOUT=(9,ITL118),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL118 ${DATA}/PXX0/F89.ITL118AF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL11MCR
       ;;
(GTL11MCR)
       m_CondExec 04,GE,GTL11MCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 7A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL119AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCT
       ;;
(GTL11MCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MCQ.FTL119AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_52_4 52 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MCU
       ;;
(GTL11MCU)
       m_CondExec 00,EQ,GTL11MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL119                                                                
#  CREATION DE L'ETAT ITL119                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MCX
       ;;
(GTL11MCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 7A               
       m_FileAssign -d SHR -g ${G_A34} FTL119 ${DATA}/PTEM/GTL10MCQ.FTL119AM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ************************************** SYNTHESE DES MODES DE DELIV/F         
#  ITL119   REPORT SYSOUT=(9,ITL119),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL119 ${DATA}/PXX0/F89.ITL119AF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL119 
       JUMP_LABEL=GTL11MCY
       ;;
(GTL11MCY)
       m_CondExec 04,GE,GTL11MCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 8A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL120AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDA
       ;;
(GTL11MDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MDA.FTL120AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_38_2 38 CH 2
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_38_2 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MDB
       ;;
(GTL11MDB)
       m_CondExec 00,EQ,GTL11MDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 8B : TRI DU FICHIER FTL117AM POUR CREATION DU FTL120BM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDD
       ;;
(GTL11MDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MDD.FTL120BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_44_4 44 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MDE
       ;;
(GTL11MDE)
       m_CondExec 00,EQ,GTL11MDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL120                                                                
#  CREATION DE L'ETAT ITL120                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDG
       ;;
(GTL11MDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 8A               
       m_FileAssign -d SHR -g ${G_A37} FTL120 ${DATA}/PTEM/GTL11MDA.FTL120AM
# ************************************** FICHIERS ISSU DU TRI 8B               
       m_FileAssign -d SHR -g ${G_A38} FTL120C ${DATA}/PTEM/GTL11MDD.FTL120BM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11MDG
# ************************************** MODE DE DELIV/FAM ET MAGS             
#  ITL120   REPORT SYSOUT=(9,ITL120),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL120 ${DATA}/PXX0/F89.ITL120AF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL120 
       JUMP_LABEL=GTL11MDH
       ;;
(GTL11MDH)
       m_CondExec 04,GE,GTL11MDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 9A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL121AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDJ
       ;;
(GTL11MDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MDJ.FTL121AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_38_2 38 CH 2
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_52_4 52 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_38_2 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MDK
       ;;
(GTL11MDK)
       m_CondExec 00,EQ,GTL11MDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 9B : TRI DU FICHIER FTL117AM POUR CREATION DU FTL121BM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDM
       ;;
(GTL11MDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11MDM.FTL121BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_44_4 44 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11MDN
       ;;
(GTL11MDN)
       m_CondExec 00,EQ,GTL11MDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL121                                                                
#  CREATION DE L'ETAT ITL121                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDQ
       ;;
(GTL11MDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 9A               
       m_FileAssign -d SHR -g ${G_A41} FTL121 ${DATA}/PTEM/GTL11MDJ.FTL121AM
# ************************************** FICHIERS ISSU DU TRI 9B               
       m_FileAssign -d SHR -g ${G_A42} FTL121C ${DATA}/PTEM/GTL11MDM.FTL121BM
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11MDQ
# ************************************** SYNTHESE MODE DE DELIV/MAGS           
#  ITL121   REPORT SYSOUT=(9,ITL121),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL121 ${DATA}/PXX0/F89.ITL121AF.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL121 
       JUMP_LABEL=GTL11MDR
       ;;
(GTL11MDR)
       m_CondExec 04,GE,GTL11MDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL112A                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDT PGM=IEBGENER   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDT
       ;;
(GTL11MDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A43} SYSUT1 ${DATA}/PXX0/F89.ITL112AF.MICROFIC
       m_OutputAssign -c 9 -w ITL112A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MDU
       ;;
(GTL11MDU)
       m_CondExec 00,EQ,GTL11MDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL112B                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MDX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MDX
       ;;
(GTL11MDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A44} SYSUT1 ${DATA}/PXX0/F89.ITL112BF.MICROFIC
       m_OutputAssign -c 9 -w ITL112B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MDY
       ;;
(GTL11MDY)
       m_CondExec 00,EQ,GTL11MDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL113                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MEA
       ;;
(GTL11MEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A45} SYSUT1 ${DATA}/PXX0/F89.ITL113AF.MICROFIC
       m_OutputAssign -c 9 -w ITL113 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEB
       ;;
(GTL11MEB)
       m_CondExec 00,EQ,GTL11MEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL114A                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MED
       ;;
(GTL11MED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A46} SYSUT1 ${DATA}/PXX0/F89.ITL114AF.MICROFIC
       m_OutputAssign -c 9 -w ITL114A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEE
       ;;
(GTL11MEE)
       m_CondExec 00,EQ,GTL11MED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL114B                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MEG PGM=IEBGENER   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MEG
       ;;
(GTL11MEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A47} SYSUT1 ${DATA}/PXX0/F89.ITL114BF.MICROFIC
       m_OutputAssign -c 9 -w ITL114B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEH
       ;;
(GTL11MEH)
       m_CondExec 00,EQ,GTL11MEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL115A                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MEJ PGM=IEBGENER   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MEJ
       ;;
(GTL11MEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A48} SYSUT1 ${DATA}/PXX0/F89.ITL115AF.MICROFIC
       m_OutputAssign -c 9 -w ITL115A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEK
       ;;
(GTL11MEK)
       m_CondExec 00,EQ,GTL11MEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL115B                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MEM PGM=IEBGENER   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MEM
       ;;
(GTL11MEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A49} SYSUT1 ${DATA}/PXX0/F89.ITL115BF.MICROFIC
       m_OutputAssign -c 9 -w ITL115B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEN
       ;;
(GTL11MEN)
       m_CondExec 00,EQ,GTL11MEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL116                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MEQ PGM=IEBGENER   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MEQ
       ;;
(GTL11MEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A50} SYSUT1 ${DATA}/PXX0/F89.ITL116AF.MICROFIC
       m_OutputAssign -c 9 -w ITL116 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MER
       ;;
(GTL11MER)
       m_CondExec 00,EQ,GTL11MEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL118                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MET PGM=IEBGENER   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MET
       ;;
(GTL11MET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A51} SYSUT1 ${DATA}/PXX0/F89.ITL118AF.MICROFIC
       m_OutputAssign -c 9 -w ITL118 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEU
       ;;
(GTL11MEU)
       m_CondExec 00,EQ,GTL11MET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL119                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MEX PGM=IEBGENER   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MEX
       ;;
(GTL11MEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A52} SYSUT1 ${DATA}/PXX0/F89.ITL119AF.MICROFIC
       m_OutputAssign -c 9 -w ITL119 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MEY
       ;;
(GTL11MEY)
       m_CondExec 00,EQ,GTL11MEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL120                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFA PGM=IEBGENER   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFA
       ;;
(GTL11MFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A53} SYSUT1 ${DATA}/PXX0/F89.ITL120AF.MICROFIC
       m_OutputAssign -c 9 -w ITL120 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MFB
       ;;
(GTL11MFB)
       m_CondExec 00,EQ,GTL11MFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL121                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFD PGM=IEBGENER   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFD
       ;;
(GTL11MFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A54} SYSUT1 ${DATA}/PXX0/F89.ITL121AF.MICROFIC
       m_OutputAssign -c 9 -w ITL121 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11MFE
       ;;
(GTL11MFE)
       m_CondExec 00,EQ,GTL11MFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 10A : TRI DU FICHIER FTL117BM ISSU DU BTL117                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFG
       ;;
(GTL11MFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A55} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117BM
# ******  FICHIER A ENVOYER A COPERNIC VIA LA GATEWAY                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FTL117CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/REFORMAT LAYOUT rl_sumDBYTE x"20"
/FIELDS 
       FD_CH_1_3     1    3 CH,
       FD_sep_01     4    1 CH,
        
       FD_CH_5_4     5    4 CH,
       FD_sep_02     9    1 CH,
        
       FD_CH_10_3    10   3 CH,
       FD_sep_03     13   1 CH,
       FD_CH_14_3    14   3 CH,
       FD_sep_04     17   1 CH,
       FD_CH_18_7    18   7 CH,
       FD_sep_05     25   1 CH,
       FD_EN_26_10   26   EN 10,
       FD_sep_06     36   1 CH,
       FD_CH_37_3    37    3 CH,
       FD_sep_07     40    1 CH,
       FD_CH_41_3    41   3 CH,
       FD_sep_08     44   1 CH
/DERIVEDFIELD total_SFF   FD_EN_26_10   (9999999V99)
/KEYS FD_CH_1_3, FD_CH_5_4, FD_CH_10_3,   FD_CH_14_3, FD_CH_18_7, FD_CH_37_3, FD_CH_41_3
/SUMMARIZE TOTAL FD_EN_26_10
/MT_OUTFILE_ASG SORTOUT
/DYNAMICLAYOUT rl_sum POSITIONAL RETAINCOMPOSITE VALUE
        FD_CH_1_3    ALIAS BFD_CH_1_3,
        FD_sep_01       ALIAS BFD_sep_01,
        FD_CH_5_4    ALIAS BFD_CH_5_4,
        FD_sep_02       ALIAS BFD_sep_02,
        FD_CH_10_3  ALIAS BFD_CH_10_3,
        FD_sep_03       ALIAS BFD_sep_03,
        FD_CH_14_3  ALIAS BFD_CH_14_3,
        FD_sep_04       ALIAS BFD_sep_04,
        FD_CH_18_7  ALIAS BFD_CH_18_7,
        FD_sep_05       ALIAS BFD_sep_05,
        total_SFF   ALIAS total_OVERLAY,
        FD_sep_06       ALIAS BFD_sep_06,
        FD_CH_37_3  ALIAS AFTER_FD_CH_37_3,
        FD_sep_07       ALIAS BFD_sep_07,
        FD_CH_41_3  ALIAS AFTER_FD_CH_41_3   PRESERVENULLINFORMATION
        FD_sep_08       ALIAS BFD_sep_08
/REFORMAT LAYOUT rl_sum

 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_18_7 18 CH 7
 /FIELDS FLD_CH_5_4 5 CH 4
 /FIELDS FLD_ZD_26_10 26 ZD 10
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_37_3 37 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_5_4 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_18_7 ASCENDING,
   FLD_CH_37_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_26_10
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GTL11MFH
       ;;
(GTL11MFH)
       m_CondExec 00,EQ,GTL11MFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL122                                                                
#  ------------                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFJ PGM=BTL122     ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFJ
       ;;
(GTL11MFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER FTL111B ISSU DU BTL111                                       
       m_FileAssign -d SHR -g ${G_A56} FTL111B ${DATA}/PXX0/F89.FTL111BM
# ******  FICHIER FTL122                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL122 ${DATA}/PTEM/GTL11MFJ.FTL122AM
# ****    PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_ProgramExec BTL122 
# ********************************************************************         
#  TRI 11A : TRI DU FICHIER FTL122AP                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFM PGM=SORT       ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFM
       ;;
(GTL11MFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A57} SORTIN ${DATA}/PTEM/GTL11MFJ.FTL122AM
# ******  FICHIER A ENVOYER A COPERNIC VIA LA GATEWAY                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FTL122BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS
    FD_CH_1_3     1   3 CH,
    FD_sep_01     4   1 CH,
 
    FD_CH_5_4     5   4  CH,
    FD_sep_02     9   1 CH,
 
    FD_CH_10_3    10  3 CH,
    FD_sep_03     13  1 CH,
 
    FD_CH_14_3    14  3 CH,
    FD_sep_04     17  1 CH,
 
    FD_CH_18_5    18  5 CH,
    FD_sep_05     23  1 CH,
 
    FD_CH_24_5    24  5 CH,
    FD_sep_06     29  1 CH,
 
    FD_CH_30_1    30  1 CH,
    FD_sep_07     31  1 CH,
 
    FD_CH_32_2    32  2 CH,
    FD_sep_08     34  1 CH,
 
    FD_EN_35_10   35  EN 10,
    FD_sep_09     45  1 CH,
 
    FD_CH_46_3    46  3 CH,
    FD_sep_10     49  1 CH,
 
    FD_CH_50_3    50  3 CH,
    FD_sep_11     53  1 CH
 
/DERIVEDFIELD total_SFF   FD_EN_35_10  (9999999V99)
 
/KEYS FD_CH_1_3, FD_CH_5_4, FD_CH_10_3, FD_CH_14_3, FD_CH_18_5, FD_CH_24_5, FD_CH_30_1,
FD_CH_32_2, FD_CH_46_3, FD_CH_50_3
/SUMMARIZE TOTAL FD_EN_35_10
/MT_OUTFILE_ASG SORTOUT
/DYNAMICLAYOUT rl_sum POSITIONAL RETAINCOMPOSITE VALUE
  FD_CH_1_3    ALIAS BFD_CH_1_3,
  FD_sep_01       ALIAS BFD_sep_01,
  FD_CH_5_4    ALIAS BFD_CH_5_4,
  FD_sep_02       ALIAS BFD_sep_02,
  FD_CH_10_3  ALIAS BFD_CH_10_3,
  FD_sep_03       ALIAS BFD_sep_03,
  FD_CH_14_3  ALIAS BFD_CH_14_3,
  FD_sep_04       ALIAS BFD_sep_04,
  FD_CH_18_5  ALIAS BFD_CH_18_5,
  FD_sep_05       ALIAS BFD_sep_05,
  FD_CH_24_5  ALIAS BFD_CH_24_5,
  FD_sep_06       ALIAS BFD_sep_06,
  FD_CH_30_1  ALIAS BFD_CH_30_1,
  FD_sep_07       ALIAS BFD_sep_07,
  FD_CH_32_2  ALIAS BFD_CH_32_2,
  FD_sep_08       ALIAS BFD_sep_08,
  total_SFF   ALIAS total_OVERLAY,
  FD_sep_09       ALIAS AFD_sep_09,
  FD_CH_46_3  ALIAS AFTER_FD_CH_46_3,
  FD_sep_10       ALIAS AFD_sep_10,
  FD_CH_50_3  ALIAS AFTER_FD_CH_50_3,
  FD_sep_11       ALIAS AFD_sep_11
/REFORMAT LAYOUT rl_sum
 /FIELDS FLD_CH_5_4 5 CH 4
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_24_5 24 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_ZD_35_10 35 ZD 10
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_5_4 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_24_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_35_10
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GTL11MFN
       ;;
(GTL11MFN)
       m_CondExec 00,EQ,GTL11MFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AIR      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTL117XX,                                                           
#      FNAME=":FTL117CM""(0),                                                  
#      NFNAME=LIVPL_M_COP                                                      
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AIW      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTL122XX,                                                           
#      FNAME=":FTL122BM""(0),                                                  
#      NFNAME=LIVACT_M_COP                                                     
#          DATAEND                                                             
# ********************************************************************         
#  DEPENDANCE POUR PLAN                                                        
#  CHANGEMENT PCL => GTL11M <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGTL11M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFQ PGM=FTP        ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFQ
       ;;
(GTL11MFQ)
       m_CondExec ${EXAIR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11MFQ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFT PGM=EZACFSM1   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFT
       ;;
(GTL11MFT)
       m_CondExec ${EXAIW},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11MFT.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGTL11M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11MFX PGM=FTP        ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MFX
       ;;
(GTL11MFX)
       m_CondExec ${EXAJB},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11MFX.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GTL11MGA PGM=EZACFSM1   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MGA
       ;;
(GTL11MGA)
       m_CondExec ${EXAJG},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11MGA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL11MZA
       ;;
(GTL11MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
