#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE51O.ksh                       --- VERSION DU 09/10/2016 05:42
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGIE51 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/28 AT 11.41.20 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GIE51O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   IMAGECOPY DU TABLESPACE RSIE10R DE LA D BASE PODGI00                       
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE51OA
       ;;
(GIE51OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2009/03/28 AT 11.41.20 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GIE51O                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK/ECARTS'                      
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE51OAA
       ;;
(GIE51OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F16.IE10IO
#                                                                              
# ******  AVANCEMENT INVENTAIRE ENTREPOT                                       
#    TABLES   : NAME=RSIE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLES /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51OAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51OAB
       ;;
(GIE51OAB)
       m_CondExec 04,GE,GIE51OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS10  DE LA D BASE PPDGS00                       
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAD
       ;;
(GIE51OAD)
       m_CondExec ${EXAAF},NE,YES 
#    RSGS10   : NAME=RSGS10,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F07.GS10IP
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51OAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51OAE
       ;;
(GIE51OAE)
       m_CondExec 04,GE,GIE51OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS15   DE LA D BASE PPDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAG
       ;;
(GIE51OAG)
       m_CondExec ${EXAAK},NE,YES 
#    RSGS15   : NAME=RSGS15,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS15 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F07.GS15IP
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51OAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51OAH
       ;;
(GIE51OAH)
       m_CondExec 04,GE,GIE51OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS70R  DE LA D BASE PODGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAJ
       ;;
(GIE51OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F16.GS70IO
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51OAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51OAK
       ;;
(GIE51OAK)
       m_CondExec 04,GE,GIE51OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS60R  DE LA D BASE PODGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAM
       ;;
(GIE51OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F16.GS60IO
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51OAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51OAN
       ;;
(GIE51OAN)
       m_CondExec 04,GE,GIE51OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE510  : EPURATION DES TABLES RTGS60 ET RTGS70                             
#            A SAVOIR HS SORTI DU HS => REPARES ET                             
#            DONC PARTIS DANS UN AUTRE LIEU QUE HS                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAQ
       ;;
(GIE51OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGS60   : NAME=RSGS60O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
#    RSGS70   : NAME=RSGS70O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE510 
       JUMP_LABEL=GIE51OAR
       ;;
(GIE51OAR)
       m_CondExec 04,GE,GIE51OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE515   REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS               
#  REPRISE: SURTOUT PAS CAR COMMIT RESTAURE A FULL IMAGE COPY PRECEDEN         
#           DES TABLES RTGS10 RTGS15 RTGS60 RTGS70 RTIE10                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAT
       ;;
(GIE51OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK THEORIC AVANT INVENTAIRE                                       
#    RSIE05   : NAME=RSIE05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  STOCK HS                                                             
#    RSGS60   : NAME=RSGS60O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  HISTO DES HS                                                         
#    RSGS70   : NAME=RSGS70O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
# ******  PRET A L'ENTREPOT LGT                                                
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# *****   STOCK SS LIEU ENTREPOT LGT                                           
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  AVANCEMENT INVENTAIRE ENTREPOT                                       
#    RSIE10   : NAME=RSIE10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS                                     
       m_FileAssign -d SHR FIIE515 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE515 
       JUMP_LABEL=GIE51OAU
       ;;
(GIE51OAU)
       m_CondExec 04,GE,GIE51OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE516   REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS               
#  REPRISE: SURTOUT PAS CAR COMMIT RESTAURE A FULL IMAGE COPY PRECEDEN         
#           DES TABLES RTGS10 RTGS15 RTGS60 RTGS70 RTIE10                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51OAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GIE51OAX
       ;;
(GIE51OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK THEORIC AVANT INVENTAIRE                                       
#    RSIE60   : NAME=RSIE60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  STOCK HS                                                             
#    RSGS60   : NAME=RSGS60O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  HISTO DES HS                                                         
#    RSGS70   : NAME=RSGS70O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
# ******  PRET A L'ENTREPOT LGT                                                
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  STOCK SS LIEU ENTREPOT LGT                                           
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS                                     
       m_FileAssign -d SHR FIIE516 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE516 
       JUMP_LABEL=GIE51OAY
       ;;
(GIE51OAY)
       m_CondExec 04,GE,GIE51OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
