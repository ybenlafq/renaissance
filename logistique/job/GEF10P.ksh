#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEF10P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGEF10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/15 AT 09.49.57 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GEF10P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BEF000                                                                
#  ------------                                                                
#  LISTE COMPLETE DE L'ENCOURS FOURNISSEUR                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEF10PA
       ;;
(GEF10PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+2'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GEF10PAA
       ;;
(GEF10PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSHE00   : NAME=RSHE00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ------  FICHIER D'EDITION                                                    
       m_OutputAssign -c 9 -w IEF000 IEF000
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 209 -t LSEQ -g +1 FEFCSV ${DATA}/PXX0/F07.BEF000AP.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF000 
       JUMP_LABEL=GEF10PAB
       ;;
(GEF10PAB)
       m_CondExec 04,GE,GEF10PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF005                                                                
#  ------------                                                                
#  CREATION DES FICHIERS D'EXTRACTION POUR EDITION                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAD
       ;;
(GEF10PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE00   : NAME=RSHE00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FEF010 ${DATA}/PTEM/GEF10PAD.FEF010AP
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 194 -t LSEQ -g +1 FDFCSV ${DATA}/PXX0/F07.BEF005AP.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF005 
       JUMP_LABEL=GEF10PAE
       ;;
(GEF10PAE)
       m_CondExec 04,GE,GEF10PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEF010AP ENTRANT DANS LE BEF010                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAG
       ;;
(GEF10PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GEF10PAD.FEF010AP
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GEF10PAG.FEF010BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_CH_80_5 80 CH 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_CH_72_8 72 CH 8
 /FIELDS FLD_PD_85_3 85 PD 3
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_80_5 ASCENDING,
   FLD_CH_72_8 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10PAH
       ;;
(GEF10PAH)
       m_CondExec 00,EQ,GEF10PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF010                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF010 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PV TTC                                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAJ PGM=BEF010     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAJ
       ;;
(GEF10PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A2} FEF010 ${DATA}/PTEM/GEF10PAG.FEF010BP
# ------  EDITION DE L'ETAT IEF010                                             
       m_OutputAssign -c 9 -w IEF010 IEF010
       m_ProgramExec BEF010 
# ********************************************************************         
#  TRI DU FICHIER FEF010AP ENTRANT DANS LES PGMS BEF011 ET BEF012              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAM
       ;;
(GEF10PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GEF10PAD.FEF010AP
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GEF10PAM.FEF010CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_51_5 51 CH 5
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_72_6 72 CH 6
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_CH_78_2 78 CH 2
 /KEYS
   FLD_CH_51_5 ASCENDING,
   FLD_CH_72_6 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_78_2 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10PAN
       ;;
(GEF10PAN)
       m_CondExec 00,EQ,GEF10PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF011                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF011 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAQ PGM=BEF011     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAQ
       ;;
(GEF10PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A4} FEF010 ${DATA}/PTEM/GEF10PAM.FEF010CP
# ------  EDITION DE L'ETAT IEF010                                             
# IEF011   REPORT SYSOUT=(9,IEF011),RECFM=FBA,LRECL=133,SPIN=UNALLOC           
       m_OutputAssign -c "*" IEF011
       m_ProgramExec BEF011 
# ********************************************************************         
#  PGM : BEF012                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF012 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAT PGM=BEF012     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAT
       ;;
(GEF10PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BEF005                                          
       m_FileAssign -d SHR -g ${G_A5} FEF010 ${DATA}/PTEM/GEF10PAM.FEF010CP
# ------  EDITION DE L'ETAT IEF010                                             
# IEF012   REPORT SYSOUT=(9,IEF012),RECFM=FBA,LRECL=133,SPIN=UNALLOC           
       m_OutputAssign -c "*" IEF012
       m_ProgramExec BEF012 
# ********************************************************************         
#  PGM : BEF013                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF013 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PRMP                                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PAX PGM=BEF013     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PAX
       ;;
(GEF10PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A6} FEF010 ${DATA}/PTEM/GEF10PAG.FEF010BP
# ------  EDITION DE L'ETAT IEF013                                             
       m_OutputAssign -c 9 -w IEF013 IEF013
       m_ProgramExec BEF013 
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU IEF000                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBA
       ;;
(GEF10PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBA.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU IEF010                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBD
       ;;
(GEF10PBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBD.sysin
       m_UtilityExec INPUT
# *************************************************************                
#  DELETE DES FICHIERS ZIP                                                     
#  REPRISE : OUI                                                               
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBG PGM=IDCAMS     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBG
       ;;
(GEF10PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GEF10PBH
       ;;
(GEF10PBH)
       m_CondExec 16,NE,GEF10PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHIERS                                                            
#  REPRISE : OUI                                                               
#  ZIP390 AJOUT STEPLIB ET SUP CARTE -COMPRESSION_LEVEL(NORMAL)                
# ********************************************************************         
# ACD      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ****** FICHIER EN ENTREE DE ZIP                                              
# DD1      FILE  NAME=BEF000AP,MODE=I                                          
# ****** FICHIER EN SORTIE DE ZIP                                              
# FICZIP   FILE  NAME=BEF000HP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -ZIPPED_DSN(PXX0.F07.BEF000AP.*,IEF000.CSV)                                 
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP 040                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBJ PGM=JVMLDM76   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBJ
       ;;
(GEF10PBJ)
       m_CondExec ${EXACD},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A7} DD1 ${DATA}/PXX0/F07.BEF000AP.ETAT
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 272 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.BEF000HP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGEF10P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBM PGM=EZACFSM1   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBM
       ;;
(GEF10PBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GEF10PBM.FTGEF10P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGEF10P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBQ PGM=FTP        ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBQ
       ;;
(GEF10PBQ)
       m_CondExec ${EXACN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GEF10PBM.FTGEF10P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  ZIP DES FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# ACS      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=BEF005AP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=BEF005HP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -ZIPPED_DSN(PXX0.F07.BEF005AP.*,IEF010.CSV)                                 
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP 040                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBT PGM=JVMLDM76   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBT
       ;;
(GEF10PBT)
       m_CondExec ${EXACS},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A9} DD1 ${DATA}/PXX0/F07.BEF005AP.ETAT
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 272 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.BEF005HP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBT.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGEF10P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PBX PGM=EZACFSM1   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PBX
       ;;
(GEF10PBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PBX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A10} SYSOUT ${DATA}/PTEM/GEF10PBM.FTGEF10P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGEF10P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10PCA PGM=FTP        ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PCA
       ;;
(GEF10PCA)
       m_CondExec ${EXADC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PCA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GEF10PBM.FTGEF10P(+2),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GEF10PZA
       ;;
(GEF10PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
