#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF50D.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDQMF50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/02 AT 11.26.28 BY PREPA2                       
#    STANDARDS: P  JOBSET: QMF50D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF115D                                  
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF50DA
       ;;
(QMF50DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       RUN=${RUN}
       JUMP_LABEL=QMF50DAA
       ;;
(QMF50DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i SORTIN
RUN ADMFIL.Q115D (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F115D
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50DAB
       ;;
(QMF50DAB)
       m_CondExec 00,EQ,QMF50DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115D                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAD
       ;;
(QMF50DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF115D DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50DAE
       ;;
(QMF50DAE)
       m_CondExec 04,GE,QMF50DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAG PGM=IEFBR14    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAG
       ;;
(QMF50DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF115T                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAJ
       ;;
(QMF50DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q115T (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F115T
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50DAK
       ;;
(QMF50DAK)
       m_CondExec 00,EQ,QMF50DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115T                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAM
       ;;
(QMF50DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF115T DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50DAN
       ;;
(QMF50DAN)
       m_CondExec 04,GE,QMF50DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAQ PGM=IEFBR14    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAQ
       ;;
(QMF50DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF116D                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAT
       ;;
(QMF50DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q116D (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F116D
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50DAU
       ;;
(QMF50DAU)
       m_CondExec 00,EQ,QMF50DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF116D                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DAX
       ;;
(QMF50DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF116D DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50DAY
       ;;
(QMF50DAY)
       m_CondExec 04,GE,QMF50DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBA PGM=IEFBR14    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBA
       ;;
(QMF50DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF116T                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBD
       ;;
(QMF50DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q116T (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F116T
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50DBE
       ;;
(QMF50DBE)
       m_CondExec 00,EQ,QMF50DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF116T                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBG
       ;;
(QMF50DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF116T DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50DBH
       ;;
(QMF50DBH)
       m_CondExec 04,GE,QMF50DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBJ PGM=IEFBR14    ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBJ
       ;;
(QMF50DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF115G                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBM
       ;;
(QMF50DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q115G (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&NSOC='$DPMDEP_1_3' FORM=ADMFIL.F115G
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50DBN
       ;;
(QMF50DBN)
       m_CondExec 00,EQ,QMF50DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115G                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBQ
       ;;
(QMF50DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF115G DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50DBR
       ;;
(QMF50DBR)
       m_CondExec 04,GE,QMF50DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBT PGM=IEFBR14    ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBT
       ;;
(QMF50DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF116G                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DBX
       ;;
(QMF50DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q116G (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&NSOC='$DPMDEP_1_3' FORM=ADMFIL.F116G
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50DBY
       ;;
(QMF50DBY)
       m_CondExec 00,EQ,QMF50DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF116G                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DCA
       ;;
(QMF50DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF116G DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50DCB
       ;;
(QMF50DCB)
       m_CondExec 04,GE,QMF50DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DCD PGM=IEFBR14    ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DCD
       ;;
(QMF50DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   LISTE HEBDO SUIVI DES REFERENCES                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=QMF50DCG
       ;;
(QMF50DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w Q115GEM DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q115GEM (&&DMOIS='$FMOISJ_5_4$FMOISJ_3_2' FORM=ADMFIL.F115GEM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF50D7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF50DCH
       ;;
(QMF50DCH)
       m_CondExec 04,GE,QMF50DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
