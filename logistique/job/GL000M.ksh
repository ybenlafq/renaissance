#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GL000M.ksh                       --- VERSION DU 08/10/2016 13:01
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGL000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/01/31 AT 12.02.12 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GL000M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  B G L 0 0 0                                                                 
# ********************************************************************         
#   EXTRACTION DES LIVRAISONS EN ATTENTE DEPUIS S-5                            
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GL000MA
       ;;
(GL000MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GL000MAA
       ;;
(GL000MAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    APRES SAUVEGARDES CICS                  *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
#  TABLE DES LIVRAISONS FOURNISSEURS                                           
#    RSGL00M  : NAME=RSGL00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGL00M /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#    RSGF10M  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10M /dev/null
#  TABLE DES LIGNES DE COMMANDES ECHEANCEES                                    
#    RSGF30M  : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30M /dev/null
#  FICHIER DES LIVRAISONS EN ATTENTE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGL000 ${DATA}/PXX0/GL000MAA.GL0000AM
#  DATE A PARTIR DE LAQUELLE ON FAIT L EXTRACTION  JJMMSSAA                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGL000 
       JUMP_LABEL=GL000MAB
       ;;
(GL000MAB)
       m_CondExec 04,GE,GL000MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  S O R T                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GL000MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GL000MAD
       ;;
(GL000MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES LIVRAISONS EN ATTENTE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GL000MAA.GL0000AM
#  FICHIER DES LIVRAISONS EN ATTENTE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PXX0/GL000MAD.GL0000BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_6 1 CH 6
 /FIELDS FLD_BI_15_41 15 CH 41
 /FIELDS FLD_BI_7_8 7 CH 8
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_1_6 ASCENDING,
   FLD_BI_15_41 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GL000MAE
       ;;
(GL000MAE)
       m_CondExec 00,EQ,GL000MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G L 0 0 5                                                                 
# ********************************************************************         
#   LISTE DES LIVRAISONS EN ATTENTE PAR ENTREPOT                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GL000MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GL000MAG
       ;;
(GL000MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES LIVRAISONS EN ATTENTES TRIEES                                   
       m_FileAssign -d SHR -g ${G_A2} FGL005 ${DATA}/PXX0/GL000MAD.GL0000BM
#  TABLE DU PLANNING LIVRAISON ENTREPOT                                        
#    RSGF35M  : NAME=RSGF35M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGF35M /dev/null
#  LISTE DES LIVRAISON EN ATTENTE                                              
       m_OutputAssign -c 9 -w BGL005 IGL005
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGL005 
       JUMP_LABEL=GL000MAH
       ;;
(GL000MAH)
       m_CondExec 04,GE,GL000MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GL000MZA
       ;;
(GL000MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GL000MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
