#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA601D.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDVA601 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/03 AT 15.09.15 BY PREPA2                       
#    STANDARDS: P  JOBSET: VA601D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BVA031                                                                
# ********************************************************************         
#  SELECTIONNE LES MVTS DE STOCKS NON PRIS EN COMPTE ENTRE                     
#  FDINV (DATE D'INVENTAIRE) ET LA FIN DE MOIS(FDFMOIS)                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VA601DA
       ;;
(VA601DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VA601DAA
       ;;
(VA601DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******* MOUVEMENTS DE STOCK                                                  
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ******* DATE DE FIN D'INVENTAIRE                                             
       m_FileAssign -i FDINV
$FDINV
_end
# ******* DATE DE PASSGE DES VALO DE STOCK(DERNIER JOUR DU MOIS)               
       m_FileAssign -i FDFMOIS
$FMOISJ
_end
# ******* MVTS SELECTIONNéS                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 FRTGS40 ${DATA}/PXX0/F91.BVA031AD
# *****   FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA031 
       JUMP_LABEL=VA601DAB
       ;;
(VA601DAB)
       m_CondExec 04,GE,VA601DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER ISSU DU PGM BVA031                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA601DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA601DAD
       ;;
(VA601DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F91.BVA031AD
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/VA601DAD.BVA500ID
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_70_8 70 CH 8
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA601DAE
       ;;
(VA601DAE)
       m_CondExec 00,EQ,VA601DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA500                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA601DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA601DAG
       ;;
(VA601DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGG55   : NAME=RSGG55D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTGG70   : NAME=RSGG70D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG70 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A2} FGS40 ${DATA}/PTEM/VA601DAD.BVA500ID
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 FVA00 ${DATA}/PXX0/F91.FVA00AD
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FVA500 ${DATA}/PXX0/F91.FVA500AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA500 
       JUMP_LABEL=VA601DAH
       ;;
(VA601DAH)
       m_CondExec 04,GE,VA601DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA00D POUR CREATION FICHIER BVA505AD ENTRANT                
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA601DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA601DAJ
       ;;
(VA601DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F91.FVA00AD
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/VA601DAJ.BVA505ID
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_CH_36_6 36 CH 6
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 44 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA601DAK
       ;;
(VA601DAK)
       m_CondExec 00,EQ,VA601DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA505                                                                
# ********************************************************************         
#  MAJ DE LA RTVA05 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA506AD POUR MAJ DE LA TABLE               
#  RTGG50 DANS LA PGM BVA506                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA601DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA601DAM
       ;;
(VA601DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG55   : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTVA10   : NAME=RSVA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA10 /dev/null
#    RTVA15   : NAME=RSVA15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA15 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA05   : NAME=RSVA05D,MODE=(U,U) - DYNAM=YES                            
# -X-RSVA05D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A4} FVA505 ${DATA}/PTEM/VA601DAJ.BVA505ID
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15 DANS VA001D                 
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -g +1 FVA515 ${DATA}/PXX0/F91.FVA15AD
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG50 DANS LE BVA506                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -g +1 FGG500 ${DATA}/PTEM/VA601DAM.BVA506ID
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
       m_FileAssign -d NEW,CATLG,KEEP -r 82 -g +1 FVA510 ${DATA}/PXX0/HISTO.F91.RTVA10
# ******  FICHIER ENTRANT DANS VA001R ET SERVANT A LOADER RTVA30               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 RTVA30 ${DATA}/PXX0/F91.FVA30AD
# *****   FICHIER PARAMETRE 'I' POUR TRAIT. INVT                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/VA601DAM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA505 
       JUMP_LABEL=VA601DAN
       ;;
(VA601DAN)
       m_CondExec 04,GE,VA601DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA506                                                                
# ********************************************************************         
#  MAJ DE LA RTGG50 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA506AD ISSU DU BVA505                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA601DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA601DAQ
       ;;
(VA601DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50D,MODE=(U,U) - DYNAM=YES                            
# -X-RSGG50D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU BVA505                                               
       m_FileAssign -d SHR -g ${G_A5} FGG500 ${DATA}/PTEM/VA601DAM.BVA506ID
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA506 
       JUMP_LABEL=VA601DAR
       ;;
(VA601DAR)
       m_CondExec 04,GE,VA601DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA601DZA
       ;;
(VA601DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA601DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
