#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GP840P.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGP840 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/02 AT 15.50.27 BY BURTECL                      
#    STANDARDS: P  JOBSET: GP840P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
# (1.1) TRI DU FICHIER FLG863 (ISSU CHAINE PPLG860)                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GP840PA
       ;;
(GP840PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GP840PAA
       ;;
(GP840PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG863P
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -g +1 SORTOUT ${DATA}/PTEM/GP840PAA.FLG863FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_7 10 CH 07
 /KEYS
   FLD_BI_10_7 ASCENDING
 /* Record Type = F  Record Length = 305 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PAB
       ;;
(GP840PAB)
       m_CondExec 00,EQ,GP840PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.2) TRI DU FICHIER FLG865                                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAD
       ;;
(GP840PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PTEM/GP840PAD.FLG865AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_14_3 14 CH 03
 /FIELDS FLD_BI_7_7 07 CH 07
 /KEYS
   FLD_BI_7_7 ASCENDING,
   FLD_BI_14_3 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PAE
       ;;
(GP840PAE)
       m_CondExec 00,EQ,GP840PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.3) TRI DU FICHIER FLG866                                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAG
       ;;
(GP840PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG866P
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 SORTOUT ${DATA}/PTEM/GP840PAG.FLG866AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_3 27 CH 03
 /FIELDS FLD_BI_1_7 01 CH 07
 /FIELDS FLD_BI_24_3 24 CH 03
 /KEYS
   FLD_BI_1_7 ASCENDING,
   FLD_BI_24_3 ASCENDING,
   FLD_BI_27_3 ASCENDING
 /* Record Type = F  Record Length = 140 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PAH
       ;;
(GP840PAH)
       m_CondExec 00,EQ,GP840PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.4) TRI DU FICHIER FLG868                                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAJ
       ;;
(GP840PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG868P
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PTEM/GP840PAJ.FLG868AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_7 08 CH 07
 /KEYS
   FLD_BI_8_7 ASCENDING
 /* Record Type = F  Record Length = 20 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PAK
       ;;
(GP840PAK)
       m_CondExec 00,EQ,GP840PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.5) EXTRACTION FGP847                                                     
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAM PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAM
       ;;
(GP840PAM)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******                                                                       
       m_FileAssign -d SHR -g ${G_A1} FLG863 ${DATA}/PTEM/GP840PAA.FLG863FP
       m_FileAssign -d SHR -g ${G_A2} FLG865 ${DATA}/PTEM/GP840PAD.FLG865AP
       m_FileAssign -d SHR -g ${G_A3} FLG866 ${DATA}/PTEM/GP840PAG.FLG866AP
       m_FileAssign -d SHR -g ${G_A4} FLG868 ${DATA}/PTEM/GP840PAJ.FLG868AP
# ******                                                                       
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGF56   : NAME=RSGF56,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF56 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EN SORTIE DU PGM BGP847                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 810 -g +1 FGP847 ${DATA}/PTEM/GP840PAM.FGP847AP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 996                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP847 
       JUMP_LABEL=GP840PAN
       ;;
(GP840PAN)
       m_CondExec 04,GE,GP840PAM ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.1) TRI  *   TRI DU SEQUENTIEL FGP847                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAQ PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAQ
       ;;
(GP840PAQ)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP847                                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GP840PAM.FGP847AP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 810 -g +1 SORTOUT ${DATA}/PTEM/GP840PAQ.FGP847BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_191_6 191 CH 06
 /FIELDS FLD_BI_170_7 170 CH 07
 /FIELDS FLD_BI_184_1 184 CH 01
 /FIELDS FLD_BI_177_7 177 CH 07
 /KEYS
   FLD_BI_177_7 ASCENDING,
   FLD_BI_184_1 ASCENDING,
   FLD_BI_170_7 ASCENDING,
   FLD_BI_191_6 ASCENDING
 /* Record Type = F  Record Length = 810 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PAR
       ;;
(GP840PAR)
       m_CondExec 00,EQ,GP840PAQ ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.2) BGP848 : EXTRACTION FGP848                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAT PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAT
       ;;
(GP840PAT)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A6} FGP847 ${DATA}/PTEM/GP840PAQ.FGP847BP
# ******  FICHIER  EN SORTIE (LRECL 750)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 810 -g +1 FGP848 ${DATA}/PTEM/GP840PAT.FGP848AP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP848 
       JUMP_LABEL=GP840PAU
       ;;
(GP840PAU)
       m_CondExec 04,GE,GP840PAT ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.1) TRI1 *   TRI DU SEQUENTIEL FGP848                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PAX PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GP840PAX
       ;;
(GP840PAX)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP848                                                           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GP840PAT.FGP848AP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 810 -g +1 SORTOUT ${DATA}/PTEM/GP840PAX.FGP848BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_71_2 071 CH 02
 /FIELDS FLD_BI_186_5 186 CH 05
 /FIELDS FLD_BI_46_5 046 CH 05
 /FIELDS FLD_BI_177_7 177 CH 07
 /KEYS
   FLD_BI_46_5 ASCENDING,
   FLD_BI_71_2 ASCENDING,
   FLD_BI_186_5 ASCENDING,
   FLD_BI_177_7 ASCENDING
 /* Record Type = F  Record Length = 810 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PAY
       ;;
(GP840PAY)
       m_CondExec 00,EQ,GP840PAX ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (7.2) TRI1 *   TRI DU SEQUENTIEL FLG863                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PBA PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GP840PBA
       ;;
(GP840PBA)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FLG863                                                           
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG863P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -g +1 SORTOUT ${DATA}/PTEM/GP840PBA.FLG863BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 001 CH 03
 /FIELDS FLD_CH_20_5 020 CH 05
 /FIELDS FLD_BI_1_3 001 CH 03
 /FIELDS FLD_BI_20_5 020 CH 05
 /FIELDS FLD_CH_209_5 209 CH 05
 /FIELDS FLD_BI_209_5 209 CH 05
 /KEYS
   FLD_BI_209_5 ASCENDING,
   FLD_BI_1_3 ASCENDING,
   FLD_BI_20_5 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 13 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_209_5,FLD_CH_1_3,FLD_CH_20_5
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GP840PBB
       ;;
(GP840PBB)
       m_CondExec 00,EQ,GP840PBA ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.3) BGP889 : CREATION   FGP849                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PBD PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GP840PBD
       ;;
(GP840PBD)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A8} FLG863P ${DATA}/PTEM/GP840PBA.FLG863BP
       m_FileAssign -d SHR -g ${G_A9} FGP848 ${DATA}/PTEM/GP840PAX.FGP848BP
# ******  FICHIER  EN SORTIE (LRECL 512)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGP849 ${DATA}/PTEM/GP840PBD.FGP849AP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 996                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP849 
       JUMP_LABEL=GP840PBE
       ;;
(GP840PBE)
       m_CondExec 04,GE,GP840PBD ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (4.1) TRI1 *   TRI DU SEQUENTIEL FGP849                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PBG PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GP840PBG
       ;;
(GP840PBG)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP849                                                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GP840PBD.FGP849AP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GP840PBG.FGP849BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_6 01 CH 06
 /FIELDS FLD_BI_14_3 014 CH 03
 /FIELDS FLD_BI_7_7 07 CH 07
 /FIELDS FLD_BI_37_5 037 CH 05
 /KEYS
   FLD_BI_7_7 ASCENDING,
   FLD_BI_1_6 ASCENDING,
   FLD_BI_14_3 ASCENDING,
   FLD_BI_37_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GP840PBH
       ;;
(GP840PBH)
       m_CondExec 00,EQ,GP840PBG ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (4.2) BGP870 : CREATION   IGP855                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GP840PBJ PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GP840PBJ
       ;;
(GP840PBJ)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A11} FGP849 ${DATA}/PTEM/GP840PBG.FGP849BP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
# FICHIER D IMPRESSION                                                         
# IGP855   FILE  NAME=IGP855P,MODE=O                                           
       m_OutputAssign -c 9 -w IGP855 IGP855
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP855 
       JUMP_LABEL=GP840PBK
       ;;
(GP840PBK)
       m_CondExec 04,GE,GP840PBJ ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GP840PZA
       ;;
(GP840PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GP840PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
