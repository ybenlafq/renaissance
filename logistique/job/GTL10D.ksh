#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL10D.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/11/25 AT 11.15.27 BY PREPA2                       
#    STANDARDS: P  JOBSET: GTL10D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  PGM : BTL110                                                                
#  EXTRACTION PERMETTANT D'ALIMENTER LA TABLE RTTL09                           
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
#           NON SI FIN NORMALE FAIRE UN RECOVER A LA F.I.C DE RTTL09           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL10DA
       ;;
(GTL10DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL10DAA
       ;;
(GTL10DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ************************************** TABLE DES QUOTAS DE LIVRAISON         
#    RSGQ01D  : NAME=RSGQ01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01D /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40D  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40D /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02D /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23D  : NAME=RSGV23D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23D /dev/null
# ************************************** TABLE DES TOUNEES (GENERALITE         
#    RSTL01D  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL01D /dev/null
# ************************************** TABLE DES TOURNEES (DETAILS)          
#    RSTL02D  : NAME=RSTL02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02D /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04D  : NAME=RSTL04D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04D /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09D  : NAME=RSTL09D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09D /dev/null
# ************************************** TABLE HISTO VENTES                    
#    RSHV15D  : NAME=RSHV15D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15D /dev/null
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DPMDEP
# ************************************** PARAMETRE  H:HEBDO  M:MENSUEL         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10DAA
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F91.FTL110AD
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL110N ${DATA}/PXX0/F91.FTL110AD
# ************************************** FIC D'EXTRACT FTL111                  
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111 ${DATA}/PXX0/F91.BTL111AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL10DAB
       ;;
(GTL10DAB)
       m_CondExec 04,GE,GTL10DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE : CUMUL DU FICHIER BTL111AM CREE PAR LE PROG BTL110 AVEC              
#          L'ANCIEN BTL111AR DU DERNIER TRAITEMENT                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAD
       ;;
(GTL10DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F91.BTL111AD
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F91.BTL111AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g ${G_A3} SORTOUT ${DATA}/PXX0/F91.BTL111AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DAE
       ;;
(GTL10DAE)
       m_CondExec 00,EQ,GTL10DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL111                                                                
#  INITIALISATION DES FICHIERS FTL111A  ET FTL111B                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAG
       ;;
(GTL10DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISE                      
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ************************************** FIC D'EXTRACT ISSU DU BLT110          
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F91.FTL110AD
# ************************************** FIC D'EXTRACT ISSU DU TRI 1A          
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F91.BTL111AD
# ************************************** FICHIER FTL111A                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111A ${DATA}/PXX0/F91.FTL111AD
# ************************************** FICHIER FTL111B                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111B ${DATA}/PXX0/F91.FTL111BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL10DAH
       ;;
(GTL10DAH)
       m_CondExec 04,GE,GTL10DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2A : TRI DU FICHIER FTL111AD POUR CREATION DU FLT112AD                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAJ
       ;;
(GTL10DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F91.FTL111AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DAJ.FTL112AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DAK
       ;;
(GTL10DAK)
       m_CondExec 00,EQ,GTL10DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2B : TRI DU FICHIER FTL111AD POUR CREATION DU FTL112BD                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAM
       ;;
(GTL10DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F91.FTL111AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DAM.FTL112BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_40_1 40 CH 1
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DAN
       ;;
(GTL10DAN)
       m_CondExec 00,EQ,GTL10DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2C : TRI DU FICHIER FTL111BD POUR CREATION FU FTL112CD                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAQ
       ;;
(GTL10DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F91.FTL111BD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DAQ.FTL112CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DAR
       ;;
(GTL10DAR)
       m_CondExec 00,EQ,GTL10DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL112                                                                
#  CREATION DES ETATS ITL112A ET ITL112B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAT
       ;;
(GTL10DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F91.FTL110AD
# ************************************** FICHIERS ISSU DU TRI 2A               
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL10DAJ.FTL112AD
# ************************************** FICHIERS ISSU DU TRI 2B               
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL10DAM.FTL112BD
# ************************************** FICHIERS ISSU DU TRI 2C               
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL10DAQ.FTL112CD
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L                  
       m_OutputAssign -c 9 -w ITL112A ITL112A
# ************************************** ETAT DE STAT PAR PIECES               
       m_OutputAssign -c 9 -w ITL112B ITL112B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL10DAU
       ;;
(GTL10DAU)
       m_CondExec 04,GE,GTL10DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3A : TRI DU FICHIER FTL111AD POUR CREATION DU FTL113AD                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DAX
       ;;
(GTL10DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F91.FTL111AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DAX.FTL113AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_95_2 95 CH 2
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DAY
       ;;
(GTL10DAY)
       m_CondExec 00,EQ,GTL10DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3B : TRI DU FICHIER FTL111AD POUR CREATION DU FTL113BD                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DBA
       ;;
(GTL10DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F91.FTL111AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DBA.FTL113BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DBB
       ;;
(GTL10DBB)
       m_CondExec 00,EQ,GTL10DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3C : TRI DES FICHIERS FTL111AD,FTL111BD POUR CREATION DU FTL113         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DBD
       ;;
(GTL10DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F91.FTL111BD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DBD.FTL113CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DBE
       ;;
(GTL10DBE)
       m_CondExec 00,EQ,GTL10DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3D : TRI DES FICHIERS FTL111AD,FTL111BD POUR CREATION FU FTL113         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DBG
       ;;
(GTL10DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F91.FTL111BD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10DBG.FTL113DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10DBH
       ;;
(GTL10DBH)
       m_CondExec 00,EQ,GTL10DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL113                                                                
#  CREATION DE L'ETAT ITL113                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DBJ
       ;;
(GTL10DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F91.FTL110AD
# ************************************** FICHIERS ISSU DU TRI 3A               
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL10DAX.FTL113AD
# ************************************** FICHIERS ISSU DU TRI 3B               
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL10DBA.FTL113BD
# ************************************** FICHIERS ISSU DU TRI 3C               
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL10DBD.FTL113CD
# ************************************** FICHIERS ISSU DU TRI 3D               
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL10DBG.FTL113DD
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR         
       m_OutputAssign -c 9 -w ITL113 ITL113
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL10DBK
       ;;
(GTL10DBK)
       m_CondExec 04,GE,GTL10DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL116                                                                
#  CREATION DE L'ETAT ITL116                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DBM
       ;;
(GTL10DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIVREURS                    
#    RSTL04   : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09   : NAME=RSTL09D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BTL110               
       m_FileAssign -d SHR -g ${G_A22} FTL110 ${DATA}/PXX0/F91.FTL110AD
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE             
       m_OutputAssign -c 9 -w ITL116 ITL116
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL10DBN
       ;;
(GTL10DBN)
       m_CondExec 04,GE,GTL10DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL10DZA
       ;;
(GTL10DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL10DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
