#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS140O.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGS140 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/07/11 AT 14.58.01 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GS140O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  AUCUNE MISE A JOUR DANS CETTE CHAINE                                        
#          PEUT ETRE RELANCEE SI PROBLEME                                      
# ********************************************************************         
#  BSM140 : EXTRACTION A PARTIR DES TABLES DB2 POUR CREATION DU FICHIE         
#           FSM140 (VENTES ET STOCKS DE MGIO)                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS140OA
       ;;
(GS140OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS140OAA
       ;;
(GS140OAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# * FAYCAL==> AJOUT DE SA202O POUR QUE LE JOB TOURNE APRES LES SAUVES          
# * CAR QUAND PLANTAGE DE LA SAUVE SA55TP OU SA55MP CE JOB DEMARRE             
# * ET ON SE PLANTE EN -904                                                    
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 916                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  ARTICLES                                                             
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : VTK)                                
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  ZONE DE PRIX                                                         
#    RSGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59O /dev/null
# ******  COMMISSIONS PAR ZONE DE PRIX                                         
#    RSGA62O  : NAME=RSGA62O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62O /dev/null
# ******  COMMISSIONS BBTE (PRIMES VENDEURS)                                   
#    RSGA75O  : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75O /dev/null
# ******  RESERVATION SUR CMDE FOURNISSEUR                                     
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
# ******  STOCK SOUS-LIEU/ENTREPOT                                             
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  STOCK SOUS-LIEU/MAGASIN                                              
#    RSGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  VENTES MAGASINS                                                      
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
# ******  FICHIER VENTES ET STOCKS MGIO                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 FSM140 ${DATA}/PTEM/GS140OAA.BSM140AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM140 
       JUMP_LABEL=GS140OAB
       ;;
(GS140OAB)
       m_CondExec 04,GE,GS140OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR PGM BSM141                                            
#       TRIE PAR FAMILLE , CODIC ET GROUPE D'EDTION                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS140OAD
       ;;
(GS140OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS MGIO                                        
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS140OAA.BSM140AO
# ******  FICHIER VENTES ET STOCKS MGIO  ( TRIE )                              
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 SORTOUT ${DATA}/PTEM/GS140OAD.BSM140BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_241_5 241 CH 5
 /FIELDS FLD_CH_234_7 234 CH 7
 /KEYS
   FLD_CH_241_5 ASCENDING,
   FLD_CH_234_7 ASCENDING,
   FLD_CH_4_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140OAE
       ;;
(GS140OAE)
       m_CondExec 00,EQ,GS140OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM141 :     PRE-FORMATAGE DU FICHIER D'EDITION                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS140OAG
       ;;
(GS140OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS MGIO  ( TRIE )                              
       m_FileAssign -d SHR -g ${G_A2} FSM140 ${DATA}/PTEM/GS140OAD.BSM140BO
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00O  : NAME=RSAN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : NCGFC)                              
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING IMBRICATION)             
#    RSGA09O  : NAME=RSGA09O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09O /dev/null
# ******  TABLE FAMILLE (EDITIONS DES ETATS)                                   
#    RSGA11O  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11O /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING)                         
#    RSGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  TABLE ASSOCIATION CODES VALEURS MARKETING/CODES VALEUR DECRI         
#    RSGA25O  : NAME=RSGA25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25O /dev/null
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26O  : NAME=RSGA26O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26O /dev/null
# ******  RESERVATIONS SUR COMMANDE FOURNISSEURS                               
#    RSGA53O  : NAME=RSGA53O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53O /dev/null
# ******  FORMATAGE DU FICHIER D'EDITION STOCK ET VENTES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 FSM141 ${DATA}/PTEM/GS140OAG.BSM141AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM141 
       JUMP_LABEL=GS140OAH
       ;;
(GS140OAH)
       m_CondExec 04,GE,GS140OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI AVANT EDITION                                                          
#        CRITERES GROUPE / WSEQED/(CMARKETING/CVMARKETING)                     
#                 TOTVTE DECROISSANT/CODIC                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS140OAJ
       ;;
(GS140OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE                            
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS140OAG.BSM141AO
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 SORTOUT ${DATA}/PTEM/GS140OAJ.BSM141BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_1_3 1 PD 3
 /FIELDS FLD_CH_338_10 338 CH 10
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_CH_353_10 353 CH 10
 /KEYS
   FLD_CH_7_2 ASCENDING,
   FLD_PD_1_3 ASCENDING,
   FLD_CH_338_10 ASCENDING,
   FLD_CH_353_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140OAK
       ;;
(GS140OAK)
       m_CondExec 00,EQ,GS140OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM142 : GENERATION DE L'EDITION                                            
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS140OAM
       ;;
(GS140OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26O  : NAME=RSGA26O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26O /dev/null
# ******  FICHIER D'EDITION STOCK ET VENTES TRIE                               
       m_FileAssign -d SHR -g ${G_A4} FSM141 ${DATA}/PTEM/GS140OAJ.BSM141BO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 916                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ****** ETAT STOCK ET VENTES POUR LE SIEGE(DV ET ACHATS)                      
       m_OutputAssign -c 9 -w ISM140 ISM140
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM142 
       JUMP_LABEL=GS140OAN
       ;;
(GS140OAN)
       m_CondExec 04,GE,GS140OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS140OZA
       ;;
(GS140OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS140OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
