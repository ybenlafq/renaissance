#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CLIE1P.ksh                       --- VERSION DU 09/10/2016 05:45
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCLIE1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/05/11 AT 16.32.34 BY PREPA2                       
#    STANDARDS: P  JOBSET: CLIE1P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# *******************************************************************          
#   BBC092                                                                     
#   EXTRACTION VENTES REDRESSEES OU DEDOUBLONNEES PAR UCM                      
# **************************************************************               
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CLIE1PA
       ;;
(CLIE1PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2009/05/11 AT 16.32.34 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: CLIE1P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'PRESELECTION VENTES'                   
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CLIE1PAA
       ;;
(CLIE1PAA)
       m_CondExec ${EXAAA},NE,YES 
# ****================================**                                       
#  DEPENDANCE POUR PLAN                                                        
# ****================================**                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RSBC92   : NAME=RSBC92P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC92 /dev/null
#  FICHIER POUR HISTORIQUE DARTY ALLANT VERS LA MAJ GV02                       
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FBC092 ${DATA}/PEX0/F07.BBC092AP
# *********************************                                            
#  TRI DES FICHIERS EXTRACTION DES VENTES                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC092 
       JUMP_LABEL=CLIE1PAB
       ;;
(CLIE1PAB)
       m_CondExec 04,GE,CLIE1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP CLIE1PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CLIE1PAD
       ;;
(CLIE1PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PEX0/F07.BBC092AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F07.BBC048BP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PEX0/F07.BBC015CP
# *************************************************************                
#  DEPENDANCE POUR PLAN                                                        
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_14 1 CH 14
 /KEYS
   FLD_CH_1_14 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CLIE1PAE
       ;;
(CLIE1PAE)
       m_CondExec 00,EQ,CLIE1PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
