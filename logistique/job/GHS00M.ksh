#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHS00M.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGHS00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/01/31 AT 12.39.22 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GHS00M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BHS000                                                                
#  ------------                                                                
#  EXTRACTION DE LA TABLE RTGS60 POUR LES LIEUX DE TRAITEMENT DEFINIS          
#  DANS LA SOUS-TABLE (HSAPP) ET POUR LES HS QUI NE SONT PAS SUR LE            
#  LIEU D'INIT (HSGES)                                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GHS00MA
       ;;
(GHS00MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GHS00MAA
       ;;
(GHS00MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGS60   : NAME=RSGS60M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 FHS000 ${DATA}/PXX0/GHS00MAA.BHS000AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHS000 
       JUMP_LABEL=GHS00MAB
       ;;
(GHS00MAB)
       m_CondExec 04,GE,GHS00MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHS005                                                                
#  ------------                                                                
#  APPEL POUR CHAQUE CODE APPLICATION DE LA SOUS-TABLE (HSAPP)                 
#  DU MODULE D'EXTRACTION DE L'ENCOURS. MHS00C MHS00E MHS00F                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAD
       ;;
(GHS00MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSHE00   : NAME=RSHE00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 FHS000 ${DATA}/PXX0/GHS00MAD.BHS005AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHS005 
       JUMP_LABEL=GHS00MAE
       ;;
(GHS00MAE)
       m_CondExec 04,GE,GHS00MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHS020                                                                
#  ------------                                                                
#  EXTRACTION DES TABLES RTGS15 ET RTEF00 POUR LES SOU-LIEUX PRET              
#  DEFINIS DANS LA SOUS TABLE HSAPP.                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAG
       ;;
(GHS00MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 FHS000 ${DATA}/PXX0/GHS00MAG.BHS020AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHS020 
       JUMP_LABEL=GHS00MAH
       ;;
(GHS00MAH)
       m_CondExec 04,GE,GHS00MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS D'EXTRACTION  POUR SUM                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAJ
       ;;
(GHS00MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS D'EXTRACTIONS FHS000                                        
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GHS00MAA.BHS000AM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/GHS00MAD.BHS005AM
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PXX0/GHS00MAG.BHS020AM
# ------  LES MEMES TRI�S ET SUM�S                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PXX0/GHS00MAJ.BHS000BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_38_3 38 PD 3
 /FIELDS FLD_PD_41_3 41 PD 3
 /FIELDS FLD_BI_6_32 06 CH 32
 /FIELDS FLD_ZD_44_1 44 ZD 1
 /KEYS
   FLD_BI_6_32 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_38_3,
    TOTAL FLD_PD_41_3,
    TOTAL FLD_ZD_44_1
 /* Record Type = F  Record Length = 44 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHS00MAK
       ;;
(GHS00MAK)
       m_CondExec 00,EQ,GHS00MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS DE COMPARAISON DU STOCK ET DE L'ENCOURS                    
#  TRIES ET SUM�S POUR BHS010                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAM
       ;;
(GHS00MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS TRI�S SUM�S                                                 
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/GHS00MAJ.BHS000BM
# ------  LES MEMES RETRI�S SUR NHS                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PXX0/GHS00MAM.BHS000CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_9 07 CH 09
 /FIELDS FLD_BI_31_7 31 CH 7
 /KEYS
   FLD_BI_7_9 ASCENDING,
   FLD_BI_31_7 ASCENDING
 /* Record Type = F  Record Length = 44 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHS00MAN
       ;;
(GHS00MAN)
       m_CondExec 00,EQ,GHS00MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHS010                                                                
#  ------------                                                                
#  EXTRACTION DU FICHIER IMPRESSION DES ECARTS ENTRE LE STOCK ET               
#  L'ENCOURS.                                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAQ
       ;;
(GHS00MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ------  FICHIERS D'EXTRACTIONS TRI�S ET SUM�S                                
       m_FileAssign -d SHR -g ${G_A5} FHS000 ${DATA}/PXX0/GHS00MAM.BHS000CM
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  FICHIER PREPARATOIRE A L'EDITION                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IHS010 ${DATA}/PXX0/GHS00MAQ.IHS010AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHS010 
       JUMP_LABEL=GHS00MAR
       ;;
(GHS00MAR)
       m_CondExec 04,GE,GHS00MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EDITION POUR LE GENERATEUR D'ETATS                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAT
       ;;
(GHS00MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER PREPARATOIRE A L'EDITION                                     
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/GHS00MAQ.IHS010AM
# ------  FICHIER PREPARATOIRE A L'EDITION VERS GENERATEUR                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GHS00MAT.IHS010BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_59 07 CH 59
 /KEYS
   FLD_BI_7_59 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHS00MAU
       ;;
(GHS00MAU)
       m_CondExec 00,EQ,GHS00MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MAX
       ;;
(GHS00MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PXX0/GHS00MAT.IHS010BM
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GHS00MAX.IHS010CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GHS00MAY
       ;;
(GHS00MAY)
       m_CondExec 04,GE,GHS00MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MBA
       ;;
(GHS00MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/GHS00MAX.IHS010CM
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GHS00MBA.IHS010DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHS00MBB
       ;;
(GHS00MBB)
       m_CondExec 00,EQ,GHS00MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IHS010  RAPPROCHEMENT STOCK HS ET ENCOURS                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS00MBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MBD
       ;;
(GHS00MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PXX0/GHS00MAT.IHS010BM
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PXX0/GHS00MBA.IHS010DM
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156)                                             
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222)                                             
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT DE RAPPROCHEMENT STOCK HS ET PRET ET DE L'ENCOURS               
       m_OutputAssign -c 9 -w IHS010 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GHS00MBE
       ;;
(GHS00MBE)
       m_CondExec 04,GE,GHS00MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GHS00MZA
       ;;
(GHS00MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHS00MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
