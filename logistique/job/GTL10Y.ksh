#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL10Y.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGTL10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/11/25 AT 11.18.46 BY PREPA2                       
#    STANDARDS: P  JOBSET: GTL10Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
#                                                                              
# ********************************************************************         
#  PGM : BTL110                                                                
#  EXTRACTION PERMETTANT D'ALIMENTER LA TABLE RTTL09                           
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
#           NON SI FIN NORMALE FAIRE UN RECOVER A LA F.I.C DE RTTL09           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL10YA
       ;;
(GTL10YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL10YAA
       ;;
(GTL10YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES QUOTAS DE LIVRAISON         
#    RSGQ01Y  : NAME=RSGQ01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01Y /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40Y  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40Y /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23Y  : NAME=RSGV23Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23Y /dev/null
# ************************************** TABLE DES TOUNEES (GENERALITE         
#    RSTL01Y  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL01Y /dev/null
# ************************************** TABLE DES TOURNEES (DETAILS)          
#    RSTL02Y  : NAME=RSTL02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02Y /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04Y  : NAME=RSTL04Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04Y /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09Y  : NAME=RSTL09Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09Y /dev/null
# ************************************** TABLE HISTO VENTES                    
#    RSHV15Y  : NAME=RSHV15Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15Y /dev/null
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DRADEP
# ************************************** PARAMETRE  H:HEBDO  M:MENSUEL         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10YAA
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F45.FTL110AY
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL110N ${DATA}/PXX0/F45.FTL110AY
# ************************************** FIC D'EXTRACT FTL111                  
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111 ${DATA}/PXX0/F45.BTL111AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL10YAB
       ;;
(GTL10YAB)
       m_CondExec 04,GE,GTL10YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE : CUMUL DU FICHIER BTL111AY CREE PAR LE PROG BTL110 AVEC              
#          L'ANCIEN BTL111AR DU DERNIER TRAITEMENT                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAD
       ;;
(GTL10YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F45.BTL111AY
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F45.BTL111AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g ${G_A3} SORTOUT ${DATA}/PXX0/F45.BTL111AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_15_5 15 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YAE
       ;;
(GTL10YAE)
       m_CondExec 00,EQ,GTL10YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL111                                                                
#  INITIALISATION DES FICHIERS FTL111A  ET FTL111B                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAG
       ;;
(GTL10YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISE                      
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** FIC D'EXTRACT ISSU DU BLT110          
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F45.FTL110AY
# ************************************** FIC D'EXTRACT ISSU DU TRI 1A          
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F45.BTL111AY
# ************************************** FICHIER FTL111A                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111A ${DATA}/PXX0/F45.FTL111AY
# ************************************** FICHIER FTL111B                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111B ${DATA}/PXX0/F45.FTL111BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL10YAH
       ;;
(GTL10YAH)
       m_CondExec 04,GE,GTL10YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2A : TRI DU FICHIER FTL111AY POUR CREATION DU FLT112AY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAJ
       ;;
(GTL10YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F45.FTL111AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YAJ.FTL112AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_51_8 51 CH 8
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YAK
       ;;
(GTL10YAK)
       m_CondExec 00,EQ,GTL10YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2B : TRI DU FICHIER FTL111AY POUR CREATION DU FTL112BY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAM
       ;;
(GTL10YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F45.FTL111AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YAM.FTL112BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YAN
       ;;
(GTL10YAN)
       m_CondExec 00,EQ,GTL10YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2C : TRI DU FICHIER FTL111BY POUR CREATION FU FTL112CY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAQ
       ;;
(GTL10YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F45.FTL111BY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YAQ.FTL112CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YAR
       ;;
(GTL10YAR)
       m_CondExec 00,EQ,GTL10YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL112                                                                
#  CREATION DES ETATS ITL112A ET ITL112B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAT
       ;;
(GTL10YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F45.FTL110AY
# ************************************** FICHIERS ISSU DU TRI 2A               
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL10YAJ.FTL112AY
# ************************************** FICHIERS ISSU DU TRI 2B               
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL10YAM.FTL112BY
# ************************************** FICHIERS ISSU DU TRI 2C               
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL10YAQ.FTL112CY
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L                  
       m_OutputAssign -c 9 -w ITL112A ITL112A
# ************************************** ETAT DE STAT PAR PIECES               
       m_OutputAssign -c 9 -w ITL112B ITL112B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL10YAU
       ;;
(GTL10YAU)
       m_CondExec 04,GE,GTL10YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3A : TRI DU FICHIER FTL111AY POUR CREATION DU FTL113AY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YAX
       ;;
(GTL10YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F45.FTL111AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YAX.FTL113AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_93_2 93 CH 2
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YAY
       ;;
(GTL10YAY)
       m_CondExec 00,EQ,GTL10YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3B : TRI DU FICHIER FTL111AY POUR CREATION DU FTL113BY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBA
       ;;
(GTL10YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F45.FTL111AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YBA.FTL113BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YBB
       ;;
(GTL10YBB)
       m_CondExec 00,EQ,GTL10YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3C : TRI DES FICHIERS FTL111AY,FTL111BY POUR CREATION FU FTL113         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBD
       ;;
(GTL10YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F45.FTL111BY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YBD.FTL113CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YBE
       ;;
(GTL10YBE)
       m_CondExec 00,EQ,GTL10YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3D : TRI DES FICHIERS FTL111AY,FTL111BY POUR CREATION FU FTL113         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBG
       ;;
(GTL10YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F45.FTL111BY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YBG.FTL113DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YBH
       ;;
(GTL10YBH)
       m_CondExec 00,EQ,GTL10YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL113                                                                
#  CREATION DE L'ETAT ITL113                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBJ
       ;;
(GTL10YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F45.FTL110AY
# ************************************** FICHIERS ISSU DU TRI 3A               
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL10YAX.FTL113AY
# ************************************** FICHIERS ISSU DU TRI 3B               
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL10YBA.FTL113BY
# ************************************** FICHIERS ISSU DU TRI 3C               
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL10YBD.FTL113CY
# ************************************** FICHIERS ISSU DU TRI 3D               
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL10YBG.FTL113DY
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR         
# ************ETAT NON UTILISE SYSOUT=*  CGA LE 181198                         
       m_OutputAssign -c "*" ITL113
# ITL113   REPORT SYSOUT=(9,ITL113),RECFM=FBA,LRECL=133,BLKSIZE=1330           
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL10YBK
       ;;
(GTL10YBK)
       m_CondExec 04,GE,GTL10YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4A : TRI DU FICHIER FTL111BY POUR CREATION DU FTL114AY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBM
       ;;
(GTL10YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F45.FTL111BY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YBM.FTL114EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_65_5 65 CH 5
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YBN
       ;;
(GTL10YBN)
       m_CondExec 00,EQ,GTL10YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4B : TRI DU FICHIER FTL111BY POUR CREATION DU FTL114BY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBQ
       ;;
(GTL10YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F45.FTL111BY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YBQ.FTL114FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_97_2 97 CH 2
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YBR
       ;;
(GTL10YBR)
       m_CondExec 00,EQ,GTL10YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4C : TRI DU FICHIER FTL111BY POUR CREATION DU FTL114CY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBT
       ;;
(GTL10YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F45.FTL111BY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10YBT.FTL114GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YBU
       ;;
(GTL10YBU)
       m_CondExec 00,EQ,GTL10YBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL114                                                                
#  CREATION DES ETATS ITL114A ET ITL114B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YBX
       ;;
(GTL10YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
# ************************************** TABLE DES ENTETES DE VENTES           
#    RSGV10Y  : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23Y  : NAME=RSGV23Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23Y /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04Y  : NAME=RSTL04Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A               
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL10YBM.FTL114EY
# ************************************** FICHIERS ISSU DU TRI 4B               
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL10YBQ.FTL114FY
# ************************************** FICHIERS ISSU DU TRI 4C               
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL10YBT.FTL114GY
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L             
       m_OutputAssign -c 9 -w ITL114A ITL114A
# ************************************** STAT RETOURS MAGS PAR PIECES          
#  ITL114B  REPORT SYSOUT=(9,ITL114B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL10YBY
       ;;
(GTL10YBY)
       m_CondExec 04,GE,GTL10YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL116                                                                
#  CREATION DE L'ETAT ITL116                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YCA
       ;;
(GTL10YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIVREURS                    
#    RSTL04   : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09   : NAME=RSTL09Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BTL110               
       m_FileAssign -d SHR -g ${G_A28} FTL110 ${DATA}/PXX0/F45.FTL110AY
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE             
       m_OutputAssign -c 9 -w ITL116 ITL116
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL10YCB
       ;;
(GTL10YCB)
       m_CondExec 04,GE,GTL10YCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL117                                                                
#  CREATION DU FICHIER FTL117AY                                                
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YCD
       ;;
(GTL10YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV         
#    RSGA13Y  : NAME=RSGA13Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13Y /dev/null
# ************************************** TABLE DES FAMILLES                    
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES           
#    RSGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30Y /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40Y  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT                       
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DRADEP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10YCD
# ************************************** FIC D'EXTRACT FTL117AY                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL117 ${DATA}/PTEM/GTL10YCD.FTL117AY
# *****   FICHIER A DESTINATION DE COPERNIC                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL117B ${DATA}/PTEM/GTL10YCD.FTL117BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL117 
       JUMP_LABEL=GTL10YCE
       ;;
(GTL10YCE)
       m_CondExec 04,GE,GTL10YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 6A : TRI DU FICHIER FTL117AY POUR CREATION DU FTL118AY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YCG
       ;;
(GTL10YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/GTL10YCD.FTL117AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GTL10YCG.FTL118AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YCH
       ;;
(GTL10YCH)
       m_CondExec 00,EQ,GTL10YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL118                                                                
#  CREATION DE L'ETAT ITL118                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YCJ
       ;;
(GTL10YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A               
       m_FileAssign -d SHR -g ${G_A30} FTL118 ${DATA}/PTEM/GTL10YCG.FTL118AY
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10YCJ
# ************************************** MODE DE DELIV/MAG ET FAM              
       m_OutputAssign -c 9 -w ITL118 ITL118
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL10YCK
       ;;
(GTL10YCK)
       m_CondExec 04,GE,GTL10YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 7A : TRI DU FICHIER FTL117AY POUR CREATION DU FTL119AY                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YCM
       ;;
(GTL10YCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GTL10YCD.FTL117AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GTL10YCM.FTL119AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10YCN
       ;;
(GTL10YCN)
       m_CondExec 00,EQ,GTL10YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL119                                                                
#  CREATION DE L'ETAT ITL119                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10YCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YCQ
       ;;
(GTL10YCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ************************************** FICHIERS ISSU DU TRI 7A               
       m_FileAssign -d SHR -g ${G_A32} FTL119 ${DATA}/PTEM/GTL10YCM.FTL119AY
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ************************************** SYNTHESE DES MODES DE DELIV/F         
       m_OutputAssign -c 9 -w ITL119 ITL119
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL119 
       JUMP_LABEL=GTL10YCR
       ;;
(GTL10YCR)
       m_CondExec 04,GE,GTL10YCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL10YZA
       ;;
(GTL10YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL10YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
