#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM60P.ksh                       --- VERSION DU 08/10/2016 23:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGRM60 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/28 AT 17.20.07 BY PREPA2                       
#    STANDARDS: P  JOBSET: GRM60P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRM060 : MAJ DES TAUX D'INDISPONIBILITE DES PRODUITS PAR MAGS RTRM2         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM60PA
       ;;
(GRM60PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       GRM60PA=${VENDRED}
       RUN=${RUN}
       JUMP_LABEL=GRM60PAA
       ;;
(GRM60PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLE                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* SOUS TABLE                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* LIEUX                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* RELATION ARTICLE/ASSORTIMENT MAG                                     
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
# ******* MUTATIONS                                                            
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******* DETAILS MUTATIONS                                                    
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# ******* STOCKS MAGASINS REMONTES VIA APPC                                    
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
# ******* TABLE DETAILS DES VENTES                                             
#    RSGS43   : NAME=RSGS43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43 /dev/null
# ******* TABLE GROUPES DE MAGASINS                                            
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******* POIDS JOURNEE PAR TYPE DE MAGS                                       
#    RSRM16   : NAME=RSRM16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM16 /dev/null
# ******* TAUX D'INDISPONIBILITE DES PRODUITS PAR MAGS                         
#    RSRM26   : NAME=RSRM26,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM26 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ****** TABLES AJOUTEES LE 22/04/03                                           
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC SERVANT AU LOAD RTGS37                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -g +1 RTGS37 ${DATA}/PTEM/GRM60PAA.BRM060AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM060 
       JUMP_LABEL=GRM60PAB
       ;;
(GRM60PAB)
       m_CondExec 04,GE,GRM60PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  POUR LOADER RTGS37                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM60PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRM60PAD
       ;;
(GRM60PAD)
       m_CondExec ${EXAAF},NE,YES 1,EQ,$[GRM60PA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GRM60PAA.BRM060AP
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.GS37RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_3 8 CH 3
 /FIELDS FLD_PD_17_3 17 PD 03
 /FIELDS FLD_BI_1_7 1 CH 7
 /FIELDS FLD_PD_14_3 14 PD 03
 /FIELDS FLD_BI_11_3 11 CH 3
 /KEYS
   FLD_BI_1_7 ASCENDING,
   FLD_BI_11_3 ASCENDING,
   FLD_BI_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_14_3,
    TOTAL FLD_PD_17_3
 /* Record Type = F  Record Length = 34 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM60PAE
       ;;
(GRM60PAE)
       m_CondExec 00,EQ,GRM60PAD ${EXAAF},NE,YES 1,EQ,$[GRM60PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DU TABLESPACES RSGS37 AVANT LE LOAD                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM60PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRM60PAG
       ;;
(GRM60PAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[GRM60PA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGRM60P
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GRM60PAG
       m_ProgramExec IEFBR14 "RDAR,GRM60P.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM60PAJ
       ;;
(GRM60PAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[GRM60PA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SYSUT1 ${DATA}/PXX0/RBA.QGRM60P
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRM60PAK
       ;;
(GRM60PAK)
       m_CondExec 00,EQ,GRM60PAJ ${EXAAP},NE,YES 1,EQ,$[GRM60PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD SUR RTGS37 : STOCK DISPO ET MUT ATTENDUE                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM60PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRM60PAM
       ;;
(GRM60PAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[GRM60PA] 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* TABLE STOCK DISPO ET MUT ATTENDUE                                    
#    RSGS37   : NAME=RSGS37,MODE=(U,N) - DYNAM=YES                             
# -X-GRM60PR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGS37 /dev/null
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F07.RELOAD.GS37RP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM60PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM60P_GRM60PAM_RTGS37.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM60PAN
       ;;
(GRM60PAN)
       m_CondExec 04,GE,GRM60PAM ${EXAAU},NE,YES 1,EQ,$[GRM60PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM60PZA
       ;;
(GRM60PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM60PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
