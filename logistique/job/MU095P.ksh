#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU095P.ksh                       --- VERSION DU 08/10/2016 22:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU095 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/21 AT 14.31.06 BY BURTECA                      
#    STANDARDS: P  JOBSET: MU095P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMU095 : REMPLISSAGE AUTO DES MUTATIONS SOUMISES � QUOTA                    
#  CE PGM FAIT DES COMMIT                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU095PA
       ;;
(MU095PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=MU095PAA
       ;;
(MU095PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
# ******  TABLES EN MISE A JOUR                                                
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV21Y  : NAME=RSGV21Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV21Y /dev/null
#    RSGV21M  : NAME=RSGV21M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV21M /dev/null
#    RSGV21D  : NAME=RSGV21D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV21D /dev/null
#    RSGV21L  : NAME=RSGV21L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV21L /dev/null
#    RSGV21O  : NAME=RSGV21O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV21O /dev/null
#    RSGV23   : NAME=RSGV23,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV23Y  : NAME=RSGV23Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23Y /dev/null
#    RSGV23M  : NAME=RSGV23M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23M /dev/null
#    RSGV23D  : NAME=RSGV23D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23D /dev/null
#    RSGV23L  : NAME=RSGV23L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23L /dev/null
#    RSGV23O  : NAME=RSGV23O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23O /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU095 
       JUMP_LABEL=MU095PAB
       ;;
(MU095PAB)
       m_CondExec 04,GE,MU095PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
