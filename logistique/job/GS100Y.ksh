#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS100Y.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGS100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/22 AT 09.43.23 BY BURTECA                      
#    STANDARDS: P  JOBSET: GS100Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGS107 :   CREATION FICHIER FGS100  :  MAJ DES STOCKS + HISTO STOC         
#                       FICHIER FEL030  :  ANOMALIES                           
#              RTGA01 - NCGFC : SI CODE GS107 = N => TRAITEMENT PARIS          
#                               SI CODE GS107 = O => TRAITEMENT FILIAL         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS100YA
       ;;
(GS100YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS100YAA
       ;;
(GS100YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  PARAMETRAGE PGM: SOUS TABLE NCGFC POUR 'GS107'                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  MARQUES                                                              
#    RSGA22   : NAME=RSGA22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# ******  CODES VENDEURS                                                       
#    RSGV31   : NAME=RSGV31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31 /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER ENCAISSEMENT VENANT DE EL001R                                
       m_FileAssign -d SHR -g +0 FEMPORTE ${DATA}/PXX0/F45.BGS105AY
#                                                                              
# ******  FICHIER SERVANT A LA MAJ DES STOCKS                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FGS100 ${DATA}/PTEM/GS100YAA.BGS107AY
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FEL030 ${DATA}/PTEM/GS100YAA.BGS107BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS107 
       JUMP_LABEL=GS100YAB
       ;;
(GS100YAB)
       m_CondExec 04,GE,GS100YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FONCTION DE LA CARTE FCFONC FIC STOCK ET STAT          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAD
       ;;
(GS100YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****  PARAMETRAGE PGM                                                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GS100YAD
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GS100YAD
#                                                                              
# *****  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                          
#    RSMQ15   : NAME=RSMQ15Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FTS054 ${DATA}/PXX0/F45.BNM154AY
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FTS056 ${DATA}/PXX0/F45.BNM156AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GS100YAE
       ;;
(GS100YAE)
       m_CondExec 04,GE,GS100YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAG
       ;;
(GS100YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F45.BNM154AY
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100YAG.BNM154BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_8 18 CH 8
 /FIELDS FLD_BI_1_17 1 CH 17
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100YAH
       ;;
(GS100YAH)
       m_CondExec 00,EQ,GS100YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAJ
       ;;
(GS100YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F45.BNM156AY
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100YAJ.BNM156BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_8 18 CH 8
 /FIELDS FLD_BI_1_17 1 CH 17
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 116 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100YAK
       ;;
(GS100YAK)
       m_CondExec 00,EQ,GS100YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAM
       ;;
(GS100YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS100YAA.BGS107AY
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100YAM.BGS107CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4  "   "
 /FIELDS FLD_CH_89_24 89 CH 24
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_1_88 01 CH 88
 /FIELDS FLD_CH_18_8 18 CH 8
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = (113,,116) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_88,CST_1_4,FLD_CH_89_24
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS100YAN
       ;;
(GS100YAN)
       m_CondExec 00,EQ,GS100YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR                                           
#  DATE(VENTE)  SOCIETE   LIEU   ZONE(PRIX)  CAISSE  TERMINAL TRANSACT         
#           SORT FIELDS = (18,8,A,1,17,A)                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAQ
       ;;
(GS100YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS100YAM.BGS107CY
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GS100YAJ.BNM156BY
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100YAQ.BGS100BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_18_8 18 CH 8
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100YAR
       ;;
(GS100YAR)
       m_CondExec 00,EQ,GS100YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAT PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAT
       ;;
(GS100YAT)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# SORTIN   FILE  NAME=BGS107CY,MODE=I                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GS100YAG.BNM154BY
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100YAT.BGS122AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_96_8 96 CH 8
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_96_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100YAU
       ;;
(GS100YAU)
       m_CondExec 00,EQ,GS100YAT ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS122 :                                                                    
#  REPRISE : OUI SI FIN ANORMALE .                                             
#           SINON  FAIRE 1 RECOVER AVEC LE RBA DU QUIESCE 1� STEP              
#            POUR : PYDMQ00.RSMQ15Y ET                                         
#                 : PYDGS00.RSGS30Y ET                                         
#                   PPDGS00.RSGS40  EST LOGISTIQUE                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YAX PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS100YAX
       ;;
(GS100YAX)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE (SOUS TABLES PARAMETRES)                           
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  RELATION ARTICLE ET ZONE DE PRIX                                     
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  TABLE DES STOCKS                                                     
#    RSGS30   : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER LG 113                                                       
       m_FileAssign -d SHR -g ${G_A7} FGS100 ${DATA}/PTEM/GS100YAT.BGS122AY
# ******  FICHIER DES ANOMALIES LG 110                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FGS103 ${DATA}/PTEM/GS100YAX.BGS103AY
#                                                                              
# ******  STOCK MAGASIN                                                        
#    RSGS30   : NAME=RSGS30Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  MOUVEMENT DE STOCK LGT                                               
#    RSGS40   : NAME=RSGS40,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGS40Y  : NAME=RSGS40Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS40Y /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS122 
       JUMP_LABEL=GS100YAY
       ;;
(GS100YAY)
       m_CondExec 04,GE,GS100YAX ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU FICHIER DES ANOMALIES TRAITEMENT DU DES CAISSES                 
#            FEL032 = FICHIER ANOS DU PROG BGS100                              
#            FGS103 = FICHIER ANOS DU PROG BGS103                              
#  TRI     : SORT FIELDS=(1,6,A,19,3,A,22,5,A,7,7,A)                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YBA PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS100YBA
       ;;
(GS100YBA)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GS100YAA.BGS107BY
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GS100YAX.BGS103AY
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100YBA.BGS106AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_19_3 19 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_19_3 ASCENDING,
   FLD_CH_22_5 ASCENDING,
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100YBB
       ;;
(GS100YBB)
       m_CondExec 00,EQ,GS100YBA ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES ANOMALIES DU TRAITEMENT DES CAISSES (BGS100+BGS122)             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100YBD PGM=BGS106     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GS100YBD
       ;;
(GS100YBD)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  FICHIER D'EDITION                                                    
       m_FileAssign -d SHR -g ${G_A10} FEL032 ${DATA}/PTEM/GS100YBA.BGS106AY
# ******  EDITION DES ANOMALIES                                                
# ******  MISE EN COMMENT PLUS UTILISE A DRA                                   
       m_OutputAssign -c "*" IFEL031
       m_ProgramExec BGS106 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS100YZA
       ;;
(GS100YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS100YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
