#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF01D.ksh                       --- VERSION DU 17/10/2016 18:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDQMF01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/11/06 AT 16.10.52 BY PREPA3                       
#    STANDARDS: P  JOBSET: QMF01D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='DPM'                                                               
# ********************************************************************         
#   LISTE EN-COURS DES BE                                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF01DA
       ;;
(QMF01DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXA98=${EXA98:-0}
       FIRSTME=${FIRSTME}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       LASTJO=${LASTJO}
       QMF01DA=${DIMANCH}
       QMF01DB=${LUNDI}
       QMF01DC=${MARDI}
       QMF01DD=${SAMEDI}
       QMF01DE=${MERCRED}
       QMF01DF=${VENDRED}
       RUN=${RUN}
       TROISME=${TROISME}
       JUMP_LABEL=QMF01DAA
       ;;
(QMF01DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF001I DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES TRAITEMENT DE NUIT ET SAUVE *                                       
# **************************************                                       
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAB
       ;;
(QMF01DAB)
       m_CondExec 04,GE,QMF01DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DE MVTS DE STOCK ENTREPOT (REGUL) ETAT1                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAD
       ;;
(QMF01DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF06 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q006 (&&JOUR='$VDATEJ_ANNMMDD' &&NSOC='$DPMDEP_1_3' FORM=ADMFIL.F006
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAE
       ;;
(QMF01DAE)
       m_CondExec 04,GE,QMF01DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENTREES HS                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAG
       ;;
(QMF01DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF18 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q018 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=ADMFIL.F018
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D11 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAH
       ;;
(QMF01DAH)
       m_CondExec 04,GE,QMF01DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENCOURS DE HS                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAJ
       ;;
(QMF01DAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF20 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q020 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=ADMFIL.F020
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D12 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAK
       ;;
(QMF01DAK)
       m_CondExec 04,GE,QMF01DAJ ${EXAAP},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENCOURS DE HS                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAM
       ;;
(QMF01DAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF20 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q020 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=ADMFIL.F020
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D12 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAN
       ;;
(QMF01DAN)
       m_CondExec 04,GE,QMF01DAM ${EXAAU},NE,YES 1,EQ,$[LASTJO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES MISES A JOUR DE PRIX                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAQ
       ;;
(QMF01DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF021 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q021 (&&JOUR='$VDATEJ1_ANNMMDD' FORM=ADMFIL.F021
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D13 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAR
       ;;
(QMF01DAR)
       m_CondExec 04,GE,QMF01DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES REPRISES EN ATTENTES   QMF022                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAT
       ;;
(QMF01DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF22 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q022 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.F022
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D14 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAU
       ;;
(QMF01DAU)
       m_CondExec 04,GE,QMF01DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENTREES                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DAX
       ;;
(QMF01DAX)
       m_CondExec ${EXABJ},NE,YES 1,EQ,$[QMF01DA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF25 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q025 (&&DEBUT='$QMFDEB_ANNMMJJ' &&FIN='$QMFFIN_ANNMMJJ' FORM=ADMFIL.F025
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D16 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DAY
       ;;
(QMF01DAY)
       m_CondExec 04,GE,QMF01DAX ${EXABJ},NE,YES 1,EQ,$[QMF01DA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BQ028A : REQUETE Q028A                                                      
#   LISTE STOCK POUR LES ACHATS                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBA
       ;;
(QMF01DBA)
       m_CondExec ${EXABO},NE,YES 1,NE,$[QMF01DD] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGM01   : NAME=RSGM01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
#                                                                              
# ******* SOCIETE = 991                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FIC EDITION POUR IMP. GENERALISEE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FQ028A ${DATA}/PTEM/QMF01DBA.Q028AAD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQ028A 
       JUMP_LABEL=QMF01DBB
       ;;
(QMF01DBB)
       m_CondExec 04,GE,QMF01DBA ${EXABO},NE,YES 1,NE,$[QMF01DD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (Q028DAB)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBD
       ;;
(QMF01DBD)
       m_CondExec ${EXABT},NE,YES 1,NE,$[QMF01DD] 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/QMF01DBA.Q028AAD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF01DBD.Q028ABD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_3 45 CH 03
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_CH_48_7 48 CH 07
 /FIELDS FLD_CH_40_5 40 CH 5
 /FIELDS FLD_CH_35_5 35 CH 5
 /FIELDS FLD_CH_30_5 30 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_5 ASCENDING,
   FLD_CH_35_5 ASCENDING,
   FLD_CH_40_5 ASCENDING,
   FLD_CH_45_3 ASCENDING,
   FLD_CH_48_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01DBE
       ;;
(QMF01DBE)
       m_CondExec 00,EQ,QMF01DBD ${EXABT},NE,YES 1,NE,$[QMF01DD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBG
       ;;
(QMF01DBG)
       m_CondExec ${EXABY},NE,YES 1,NE,$[QMF01DD] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/QMF01DBD.Q028ABD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/QMF01DBG.Q028ACD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF01DBH
       ;;
(QMF01DBH)
       m_CondExec 04,GE,QMF01DBG ${EXABY},NE,YES 1,NE,$[QMF01DD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBJ
       ;;
(QMF01DBJ)
       m_CondExec ${EXACD},NE,YES 1,NE,$[QMF01DD] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/QMF01DBG.Q028ACD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF01DBJ.Q028ADD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01DBK
       ;;
(QMF01DBK)
       m_CondExec 00,EQ,QMF01DBJ ${EXACD},NE,YES 1,NE,$[QMF01DD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   LISTE STOCK POUR LES ACHATS                                                
#  CREATION DE L'ETAT : Q028A                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBM
       ;;
(QMF01DBM)
       m_CondExec ${EXACI},NE,YES 1,NE,$[QMF01DD] 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/QMF01DBD.Q028ABD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/QMF01DBJ.Q028ADD
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w Q028A FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF01DBN
       ;;
(QMF01DBN)
       m_CondExec 04,GE,QMF01DBM ${EXACI},NE,YES 1,NE,$[QMF01DD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BQ028B : REQUETE Q028B                                                      
#   LISTE STOCK POUR LES ACHATS                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBQ
       ;;
(QMF01DBQ)
       m_CondExec ${EXACN},NE,YES 1,EQ,$[QMF01DC] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******* SOCIETE = 991                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FIC EDITION POUR IMP. GENERALISEE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FQ028B ${DATA}/PTEM/QMF01DBQ.Q028BAD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQ028B 
       JUMP_LABEL=QMF01DBR
       ;;
(QMF01DBR)
       m_CondExec 04,GE,QMF01DBQ ${EXACN},NE,YES 1,EQ,$[QMF01DC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (Q028BAD)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBT
       ;;
(QMF01DBT)
       m_CondExec ${EXACS},NE,YES 1,EQ,$[QMF01DC] 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/QMF01DBQ.Q028BAD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF01DBT.Q028BBD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_3 45 CH 03
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_CH_48_7 48 CH 07
 /FIELDS FLD_CH_35_5 35 CH 5
 /FIELDS FLD_CH_30_5 30 CH 5
 /FIELDS FLD_CH_40_5 40 CH 5
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_5 ASCENDING,
   FLD_CH_35_5 ASCENDING,
   FLD_CH_40_5 ASCENDING,
   FLD_CH_45_3 ASCENDING,
   FLD_CH_48_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01DBU
       ;;
(QMF01DBU)
       m_CondExec 00,EQ,QMF01DBT ${EXACS},NE,YES 1,EQ,$[QMF01DC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DBX
       ;;
(QMF01DBX)
       m_CondExec ${EXACX},NE,YES 1,EQ,$[QMF01DC] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/QMF01DBT.Q028BBD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/QMF01DBX.Q028BCD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF01DBY
       ;;
(QMF01DBY)
       m_CondExec 04,GE,QMF01DBX ${EXACX},NE,YES 1,EQ,$[QMF01DC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCA
       ;;
(QMF01DCA)
       m_CondExec ${EXADC},NE,YES 1,EQ,$[QMF01DC] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/QMF01DBX.Q028BCD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/QMF01DCA.Q028BDD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01DCB
       ;;
(QMF01DCB)
       m_CondExec 00,EQ,QMF01DCA ${EXADC},NE,YES 1,EQ,$[QMF01DC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   LISTE STOCK POUR LES ACHATS                                                
#  CREATION DE L'ETAT : Q028B                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCD
       ;;
(QMF01DCD)
       m_CondExec ${EXADH},NE,YES 1,EQ,$[QMF01DC] 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/QMF01DBT.Q028BBD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/QMF01DCA.Q028BDD
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w Q028B FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF01DCE
       ;;
(QMF01DCE)
       m_CondExec 04,GE,QMF01DCD ${EXADH},NE,YES 1,EQ,$[QMF01DC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES BE DU JOUR                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCG
       ;;
(QMF01DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF33 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q033 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.F033
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D22 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DCH
       ;;
(QMF01DCH)
       m_CondExec 04,GE,QMF01DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES MOUVEMENTS PAR ARTICLE (VENTES ET REPRISES)                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCJ
       ;;
(QMF01DCJ)
       m_CondExec ${EXADR},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF155 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q155 (&&JOUR='$VDATEJ__ANN_MM_JJ' &&SOC='$DPMDEP_1_3' FORM=ADMFIL.F155
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D24 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DCK
       ;;
(QMF01DCK)
       m_CondExec 04,GE,QMF01DCJ ${EXADR},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF164                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCM
       ;;
(QMF01DCM)
       m_CondExec ${EXADW},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF164 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q164 (&&DEBUT='$QMFDEB8_ANNMMJJ' &&FIN='$QMFFIN8_ANNMMJJ' FORM=ADMFIL.F164
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D26 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DCN
       ;;
(QMF01DCN)
       m_CondExec 04,GE,QMF01DCM ${EXADW},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   ETAT DE STOCK PERMETTANT D'AIDER A ARBRITER LES PRIORITES POUR LES         
#   RENDEZ-VOUS DES RECEPTIONS FOURNISSEURS  LE 1ER MERCREDI                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCQ
       ;;
(QMF01DCQ)
       m_CondExec ${EXAEB},NE,YES 1,EQ,$[FIRSTME] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFGS28C DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D29
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D29 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DCR
       ;;
(QMF01DCR)
       m_CondExec 04,GE,QMF01DCQ ${EXAEB},NE,YES 1,EQ,$[FIRSTME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   ETAT DE STOCK PERMETTANT D'AIDER A ARBRITER LES PRIORITES POUR LES         
#   RENDEZ-VOUS DES RECEPTIONS FOURNISSEURS LE 3EM MERCREDI                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCT
       ;;
(QMF01DCT)
       m_CondExec ${EXAEG},NE,YES 1,EQ,$[TROISME] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFGS28C DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D29
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D29 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DCU
       ;;
(QMF01DCU)
       m_CondExec 04,GE,QMF01DCT ${EXAEG},NE,YES 1,EQ,$[TROISME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PERMET DE DISPOSER D'UN ETAT DE STOCK PERMETTANT DE SUIVRE LA              
#   CODIFICATION DES LIEUX DE STOCKAGE DES PRODUITS EN HEBDO                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DCX
       ;;
(QMF01DCX)
       m_CondExec ${EXAEL},NE,YES 1,EQ,$[QMF01DE] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFGD31B DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D31
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D31 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DCY
       ;;
(QMF01DCY)
       m_CondExec 04,GE,QMF01DCX ${EXAEL},NE,YES 1,EQ,$[QMF01DE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PERMET DE DISPOSER D'UN ETAT DE STOCK PERMETTANT DE SUIVRE LA              
#   CODIFICATION DES LIEUX DE STOCKAGE DES PRODUITS EN HEBDO                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDA
       ;;
(QMF01DDA)
       m_CondExec ${EXAEQ},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFGD31B DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D31
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D31 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDB
       ;;
(QMF01DDB)
       m_CondExec 04,GE,QMF01DDA ${EXAEQ},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE CONTROLE DE RECUPERATION DE CAISSE                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDD
       ;;
(QMF01DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w CONTMGIS DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.CON02 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.FCON01
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D32 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDE
       ;;
(QMF01DDE)
       m_CondExec 04,GE,QMF01DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES MUTATIONS NON VALIDEES                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDG
       ;;
(QMF01DDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[QMF01DA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w MUTNOVAL DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q001NV (&&SOC='$DPMDEP_1_3' FORM=ADMFIL.F001NV
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D33 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDH
       ;;
(QMF01DDH)
       m_CondExec 04,GE,QMF01DDG ${EXAFA},NE,YES 1,EQ,$[QMF01DA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q035 (EN COURS DE B.E SUR RTGV35)                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDJ
       ;;
(QMF01DDJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[QMF01DF] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF035 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D34
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D34 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDK
       ;;
(QMF01DDK)
       m_CondExec 04,GE,QMF01DDJ ${EXAFF},NE,YES 1,EQ,$[QMF01DF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   B.E DANS LE LIEU TRANSIT                                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDM
       ;;
(QMF01DDM)
       m_CondExec ${EXAFK},NE,YES 1,EQ,$[QMF01DF] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF041 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D35
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D35 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDN
       ;;
(QMF01DDN)
       m_CondExec 04,GE,QMF01DDM ${EXAFK},NE,YES 1,EQ,$[QMF01DF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.Q182 FORM : ADMFIL.F182                                     
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI FAMILLE CODIC DAR         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDQ
       ;;
(QMF01DDQ)
       m_CondExec ${EXAFP},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF182 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D36
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D36 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDR
       ;;
(QMF01DDR)
       m_CondExec 04,GE,QMF01DDQ ${EXAFP},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.183  FORM : ADMFIL.F183                                     
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI FAMILLE CODIC DAC         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDT
       ;;
(QMF01DDT)
       m_CondExec ${EXAFU},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF183 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D37
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D37 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDU
       ;;
(QMF01DDU)
       m_CondExec 04,GE,QMF01DDT ${EXAFU},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.Q184 FORM : F184                                            
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI MARQUE CODIC DART         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DDX
       ;;
(QMF01DDX)
       m_CondExec ${EXAFZ},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF184 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D38
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D38 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DDY
       ;;
(QMF01DDY)
       m_CondExec 04,GE,QMF01DDX ${EXAFZ},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.Q185 FORM : F185                                            
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI MARQUE CODIC DART         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DEA
       ;;
(QMF01DEA)
       m_CondExec ${EXAGE},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF185 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D39
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D39 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DEB
       ;;
(QMF01DEB)
       m_CondExec 04,GE,QMF01DEA ${EXAGE},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.Q182 FORM : ADMFIL.F182                                     
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI FAMILLE CODIC DAR         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DED
       ;;
(QMF01DED)
       m_CondExec ${EXAGJ},NE,YES 1,EQ,$[QMF01DE] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF182 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D36
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D36 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DEE
       ;;
(QMF01DEE)
       m_CondExec 04,GE,QMF01DED ${EXAGJ},NE,YES 1,EQ,$[QMF01DE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.183  FORM : ADMFIL.F183                                     
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI FAMILLE CODIC DAC         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DEG
       ;;
(QMF01DEG)
       m_CondExec ${EXAGO},NE,YES 1,EQ,$[QMF01DE] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF183 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D37
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D37 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DEH
       ;;
(QMF01DEH)
       m_CondExec 04,GE,QMF01DEG ${EXAGO},NE,YES 1,EQ,$[QMF01DE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.Q184 FORM : F184                                            
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI MARQUE CODIC DART         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DEJ
       ;;
(QMF01DEJ)
       m_CondExec ${EXAGT},NE,YES 1,EQ,$[QMF01DE] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF184 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D38
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D38 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DEK
       ;;
(QMF01DEK)
       m_CondExec 04,GE,QMF01DEJ ${EXAGT},NE,YES 1,EQ,$[QMF01DE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.Q185 FORM : F185                                            
#   LISTE DES H.S PAR LIEUX DE TRT CUMUL CODIC - TRI MARQUE CODIC DART         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DEM
       ;;
(QMF01DEM)
       m_CondExec ${EXAGY},NE,YES 1,EQ,$[QMF01DE] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF185 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01D39
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D39 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DEN
       ;;
(QMF01DEN)
       m_CondExec 04,GE,QMF01DEM ${EXAGY},NE,YES 1,EQ,$[QMF01DE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LA QUERY QMF (QHSDEP)                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DEQ
       ;;
(QMF01DEQ)
       m_CondExec ${EXAHD},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.QHSDEP (&&NSOC='$DPMDEP_1_3' FORM=ADMFIL.FHSDEP
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F91.QMF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01DER
       ;;
(QMF01DER)
       m_CondExec 00,EQ,QMF01DEQ ${EXAHD},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DE DEPHASAGE STOCK HS GS10/GS60                                      
#                                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DET
       ;;
(QMF01DET)
       m_CondExec ${EXAHI},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QHSDEP DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F91.QMF001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01DEU
       ;;
(QMF01DEU)
       m_CondExec 04,GE,QMF01DET ${EXAHI},NE,YES 1,EQ,$[QMF01DB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DEX PGM=IEFBR14    ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DEX
       ;;
(QMF01DEX)
       m_CondExec ${EXAHN},NE,YES 1,EQ,$[QMF01DB] 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F91.QMF001AD
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   ETAT DE DESTOCKAGE HEBDO QGD91                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFA PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFA
       ;;
(QMF01DFA)
       m_CondExec ${EXAHS},NE,YES 1,EQ,$[QMF01DA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGD91 DSQPRINT
       m_FileAssign -i QMFPARM
RUN QGD91 (&&DRAF1='$QMFDEB_ANNMMJJ' &&DRAF2='$QMFFIN_ANNMMJJ' &&SOC='$DPMDEP_1_3' FORM=FGD91
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D40 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFB
       ;;
(QMF01DFB)
       m_CondExec 04,GE,QMF01DFA ${EXAHS},NE,YES 1,EQ,$[QMF01DA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGF002                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFD
       ;;
(QMF01DFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGF002 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QGF002 (&&JOUR='$VDATEJ_ANNMMJJ' FORM=ADMFIL.FGF002
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D04 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFE
       ;;
(QMF01DFE)
       m_CondExec 04,GE,QMF01DFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGSPRET (DEPHASAGE RTGS10 ZET RTGS15                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFG PGM=IKJEFT01   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFG
       ;;
(QMF01DFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGSPRET DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QGSPRET (&&NSOC='$DPMDEP_1_3' FORM=ADMFIL.FGSPRET
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D08 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFH
       ;;
(QMF01DFH)
       m_CondExec 04,GE,QMF01DFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q329509 (CONCOURS VTE ABONNEMENT)                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFJ
       ;;
(QMF01DFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w Q329509 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q329509 (&&FDATE='$VDATEJ__ANNMMDD' &&SOC='$DPMDEP_1_3' FORM=ADMFIL.F329509
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D09 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFK
       ;;
(QMF01DFK)
       m_CondExec 04,GE,QMF01DFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   QUERY : ADMFIL.QGS60DPM FORM : FFGS60DPM                                   
#   LISTE DE PRETS                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFM
       ;;
(QMF01DFM)
       m_CondExec ${EXAIM},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGS60DPM DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QGS60D01
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QGS60D01 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFN
       ;;
(QMF01DFN)
       m_CondExec 04,GE,QMF01DFM ${EXAIM},NE,YES 1,EQ,$[LASTJO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGSM7H                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFQ
       ;;
(QMF01DFQ)
       m_CondExec ${EXAIR},NE,YES 1,EQ,$[QMF01DA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGSM7H DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QGSM7H (&&DATEDEB='$QMFDEB_ANNMMJJ' &&DATEFIN='$QMFFIN_ANNMMJJ' FORM=ADMFIL.FGSM7H
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00D32 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFR
       ;;
(QMF01DFR)
       m_CondExec 04,GE,QMF01DFQ ${EXAIR},NE,YES 1,EQ,$[QMF01DA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q002 (TRAITEMENT MENSUEL)                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01DFT PGM=IKJEFT01   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DFT
       ;;
(QMF01DFT)
       m_CondExec ${EXAIW},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF02M DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q002M (&&DEBUT='$DEBMOIS_ANNMMDD' &&FIN='$FINMOIS_ANNMMDD' &&SOC='$DPMDEP_1_3' FORM=F002
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01D41 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01DFU
       ;;
(QMF01DFU)
       m_CondExec 04,GE,QMF01DFT ${EXAIW},NE,YES 1,EQ,$[LASTJO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=QMF01DZA
       ;;
(QMF01DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QMF01DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
