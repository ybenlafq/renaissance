#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GVA10M.ksh                       --- VERSION DU 08/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGVA10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/09/26 AT 12.10.28 BY BURTECR                      
#    STANDARDS: P  JOBSET: GVA10M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BSP905 : MAJ HISTO SEUIL REVENTE A PERTE RTSP10                            
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GVA10MA
       ;;
(GVA10MA)
       EXABE=${EXABE:-0}
       RUN=${RUN}
       JUMP_LABEL=GVA10MAA
       ;;
(GVA10MAA)
       m_CondExec ${EXABE},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  DEPENDANCE POUR OPTIMISATION DE PLAN                                        
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** HISTO DU SRP SEUIL DE REVENTE A PERTE                                 
#    RSSP10M  : NAME=RSSP10M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP10M /dev/null
# ****** PRM                                                                   
#    RSGG50M  : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50M /dev/null
# ****** TABLE COMMANDES FOURNISSEURS                                          
#    RSGF10M  : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10M /dev/null
#                                                                              
# ****** PARAMETRE : DACEM                                                     
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GVA10MAA
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ****** JJMMSSAA                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP905 
       JUMP_LABEL=GVA10MAB
       ;;
(GVA10MAB)
       m_CondExec 04,GE,GVA10MAA ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
