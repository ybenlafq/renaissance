#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM03P.ksh                       --- VERSION DU 08/10/2016 12:47
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGRM03 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/09/30 AT 14.21.16 BY BURTECO                      
#    STANDARDS: P  JOBSET: GRM03P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRI DU FICHIER JRM221 ISSU DE LA CHAINE GRM02P                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM03PA
       ;;
(GRM03PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRM03PAA
       ;;
(GRM03PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BRM221AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRM03PAA.BRM221BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "907"
 /FIELDS FLD_BI_55_2 55 CH 2
 /FIELDS FLD_CH_166_3 166 CH 3
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_7_20 7 CH 20
 /FIELDS FLD_PD_32_3 32 PD 3
 /FIELDS FLD_BI_35_20 35 CH 20
 /CONDITION CND_1 FLD_CH_166_3 EQ CST_1_8 
 /KEYS
   FLD_BI_27_5 ASCENDING,
   FLD_BI_7_20 ASCENDING,
   FLD_PD_32_3 ASCENDING,
   FLD_BI_35_20 ASCENDING,
   FLD_BI_55_2 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM03PAB
       ;;
(GRM03PAB)
       m_CondExec 00,EQ,GRM03PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION DE L'ETAT JRM221                                           
#                                                                              
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
#  REPRISE =  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM03PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRM03PAD
       ;;
(GRM03PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA01R  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01R /dev/null
# ******* TABLE DES SOUS/TABLES                                                
#    RSGA71R  : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA71R /dev/null
# ******* TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00R  : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG00R /dev/null
#    RSEG05R  : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG05R /dev/null
#    RSEG10R  : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG10R /dev/null
#    RSEG11R  : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG11R /dev/null
#    RSEG15R  : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG15R /dev/null
#    RSEG20R  : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG20R /dev/null
#    RSEG25R  : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG25R /dev/null
#    RSEG30R  : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG30R /dev/null
# ******* FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A1} FEXTRAC ${DATA}/PTEM/GRM03PAA.BRM221BP
# ******* FICHIER FCUMULS                                                      
       m_FileAssign -d SHR FCUMULS /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE : 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAMETRE  FICHIER S/36(156)                                         
       m_FileAssign -d SHR FEG132 /dev/null
# ******* PARAMETRE  FICHIER S/36(222)                                         
       m_FileAssign -d SHR FEG198 /dev/null
#                                                                              
# ******* EDITION                                                              
       m_OutputAssign -c 9 -w JRM221 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM03PAE
       ;;
(GRM03PAE)
       m_CondExec 04,GE,GRM03PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER JRM231 ISSU DE LA CHAINE GRM02P                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM03PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRM03PAG
       ;;
(GRM03PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BRM231AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRM03PAG.BRM231BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "907"
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_PD_55_2 55 PD 2
 /FIELDS FLD_CH_184_3 184 CH 3
 /FIELDS FLD_BI_57_1 57 CH 1
 /FIELDS FLD_BI_35_20 35 CH 20
 /FIELDS FLD_PD_32_3 32 PD 3
 /FIELDS FLD_BI_7_20 7 CH 20
 /CONDITION CND_1 FLD_CH_184_3 EQ CST_1_9 
 /KEYS
   FLD_BI_27_5 ASCENDING,
   FLD_BI_7_20 ASCENDING,
   FLD_PD_32_3 ASCENDING,
   FLD_BI_35_20 ASCENDING,
   FLD_PD_55_2 ASCENDING,
   FLD_BI_57_1 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM03PAH
       ;;
(GRM03PAH)
       m_CondExec 00,EQ,GRM03PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION DE L'ETAT JRM231                                           
#                                                                              
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
#  REPRISE =  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM03PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRM03PAJ
       ;;
(GRM03PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA01R  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01R /dev/null
# ******* TABLE DES SOUS/TABLES                                                
#    RSGA71R  : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA71R /dev/null
# ******* TABLE DU GENERATEUR D'ETATS                                          
#    RSEG00R  : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG00R /dev/null
#    RSEG05R  : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG05R /dev/null
#    RSEG10R  : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG10R /dev/null
#    RSEG11R  : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG11R /dev/null
#    RSEG15R  : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG15R /dev/null
#    RSEG20R  : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG20R /dev/null
#    RSEG25R  : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG25R /dev/null
#    RSEG30R  : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEG30R /dev/null
# ******* FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GRM03PAG.BRM231BP
# ******* FICHIER FCUMULS                                                      
       m_FileAssign -d SHR FCUMULS /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE : 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAMETRE  FICHIER S/36(156)                                         
       m_FileAssign -d SHR FEG132 /dev/null
# ******* PARAMETRE  FICHIER S/36(222)                                         
       m_FileAssign -d SHR FEG198 /dev/null
#                                                                              
# ******* EDITION                                                              
       m_OutputAssign -c 9 -w JRM231 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM03PAK
       ;;
(GRM03PAK)
       m_CondExec 04,GE,GRM03PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM03PZA
       ;;
(GRM03PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM03PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
