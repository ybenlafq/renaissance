#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS006F.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGS006 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/16 AT 09.53.25 BY BURTECA                      
#    STANDARDS: P  JOBSET: GS006F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGS006 : EXTRACTS MVTS REGULS DE STOCK MGD 907 DIF                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS006FA
       ;;
(GS006FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS006FAA
       ;;
(GS006FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHV15   : NAME=RSHV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
# ------  FICHIER FPARAM                                                       
       m_FileAssign -i FPARAM
H 907 $FDATE
_end
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 EGS006 ${DATA}/PTEM/GS006FAA.BGS006AP
# ------  FICHIER CSV                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 FGS006 ${DATA}/PTEM/GS006FAA.BGS006BP
# ------  ETAT IGS006                                                          
       m_OutputAssign -c 9 -w IGS006P IGS006
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS006 
       JUMP_LABEL=GS006FAB
       ;;
(GS006FAB)
       m_CondExec 04,GE,GS006FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS006 : EXTRACTS MVTS REGULS DE STOCK 916 DGO                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAD
       ;;
(GS006FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHV15   : NAME=RSHV15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15 /dev/null
# ------  FICHIER FPARAM                                                       
       m_FileAssign -i FPARAM
H 916 $FDATE
_end
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 EGS006 ${DATA}/PTEM/GS006FAD.BGS006AO
# ------  FICHIER CSV                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 FGS006 ${DATA}/PTEM/GS006FAD.BGS006BO
# ------  ETAT IGS006                                                          
       m_OutputAssign -c 9 -w IGS006O IGS006
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS006 
       JUMP_LABEL=GS006FAE
       ;;
(GS006FAE)
       m_CondExec 04,GE,GS006FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS006 : EXTRACTS MVTS REGULS DE STOCK 945 DGE                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAG
       ;;
(GS006FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHV15   : NAME=RSHV15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15 /dev/null
# ------  FICHIER FPARAM                                                       
       m_FileAssign -i FPARAM
H 945 $FDATE
_end
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 EGS006 ${DATA}/PTEM/GS006FAG.BGS006AY
# ------  FICHIER CSV                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 FGS006 ${DATA}/PTEM/GS006FAG.BGS006BY
# ------  ETAT IGS006                                                          
       m_OutputAssign -c 9 -w IGS006Y IGS006
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS006 
       JUMP_LABEL=GS006FAH
       ;;
(GS006FAH)
       m_CondExec 04,GE,GS006FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS006 : EXTRACTS MVTS REGULS DE STOCK 961 DGO                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAJ
       ;;
(GS006FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHV15   : NAME=RSHV15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15 /dev/null
# ------  FICHIER FPARAM                                                       
       m_FileAssign -i FPARAM
H 961 $FDATE
_end
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 EGS006 ${DATA}/PTEM/GS006FAJ.BGS006AL
# ------  FICHIER CSV                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 FGS006 ${DATA}/PTEM/GS006FAJ.BGS006BL
# ------  ETAT IGS006                                                          
       m_OutputAssign -c 9 -w IGS006L IGS006
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS006 
       JUMP_LABEL=GS006FAK
       ;;
(GS006FAK)
       m_CondExec 04,GE,GS006FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS006 : EXTRACTS MVTS REGULS DE STOCK 989 DGE                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAM
       ;;
(GS006FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHV15   : NAME=RSHV15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15 /dev/null
# ------  FICHIER FPARAM                                                       
       m_FileAssign -i FPARAM
H 989 $FDATE
_end
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 EGS006 ${DATA}/PTEM/GS006FAM.BGS006AM
# ------  FICHIER CSV                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 FGS006 ${DATA}/PTEM/GS006FAM.BGS006BM
# ------  ETAT IGS006                                                          
       m_OutputAssign -c 9 -w IGS006M IGS006
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS006 
       JUMP_LABEL=GS006FAN
       ;;
(GS006FAN)
       m_CondExec 04,GE,GS006FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS006 : EXTRACTS MVTS REGULS DE STOCK 991 DGE                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAQ
       ;;
(GS006FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHV15   : NAME=RSHV15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15 /dev/null
# ------  FICHIER FPARAM                                                       
       m_FileAssign -i FPARAM
H 991 $FDATE
_end
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 EGS006 ${DATA}/PTEM/GS006FAQ.BGS006AD
# ------  FICHIER CSV                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 FGS006 ${DATA}/PTEM/GS006FAQ.BGS006BD
# ------  ETAT IGS006                                                          
       m_OutputAssign -c 9 -w IGS006D IGS006
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS006 
       JUMP_LABEL=GS006FAR
       ;;
(GS006FAR)
       m_CondExec 04,GE,GS006FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS DE REGUL DE CHAQUE FILIALE                                 
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAT
       ;;
(GS006FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ENTETE                                                       
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS006FAA.BGS006AP
# ------  FICHIER REGULS TTES FILIALES                                         
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GS006FAA.BGS006BP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GS006FAD.BGS006BO
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GS006FAG.BGS006BY
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GS006FAJ.BGS006BL
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GS006FAM.BGS006BM
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GS006FAQ.BGS006BD
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BGS006ZZ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 95 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS006FAU
       ;;
(GS006FAU)
       m_CondExec 00,EQ,GS006FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP BGS006ZZ                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS006FAX
       ;;
(GS006FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS006FAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GS006FAX.FGS006AP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU BGS006ZZ                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS006FBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS006FBA
       ;;
(GS006FBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS006FBA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GS006FAX.FGS006AP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
#                                                                              
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS006FZA
       ;;
(GS006FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS006FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
