#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL00Y.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGTL00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 10.56.10 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL00Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  IMPERATIVE PREPARATION DES B.L                                              
# ********************************************************************         
# ********************************************************************         
#   BTL100 : AFFECTE UN PROFIL DE TOURNEES AUX LIVRAISONS DU JOUY              
#   REPRISE: OUI APRES ABEND                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL00YA
       ;;
(GTL00YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL00YAA
       ;;
(GTL00YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LES TABLES             *                                       
#    DE VENTES A JOUY                  *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  DETAILS DES VENTES                                                   
#    RSGV11Y  : NAME=RSGV11Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  ANOMALIES                                                            
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ******  CALENDRIER DES LIVRAISONS                                            
#    RSTL10Y  : NAME=RSTL10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL10Y /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *******                                                                      
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL100 
       JUMP_LABEL=GTL00YAB
       ;;
(GTL00YAB)
       m_CondExec 04,GE,GTL00YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREE UNE GENERATION  A VIDE DE FACON A LES PRENDRE EN DISP=MOD LORS         
#  DES REPRISES DE BTL000                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAD
       ;;
(GTL00YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT1 ${DATA}/PTL945/F45.BTL001CY
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT2 ${DATA}/PTL945/F45.BTL002DY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00YAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GTL00YAE
       ;;
(GTL00YAE)
       m_CondExec 16,NE,GTL00YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BTL000 : EXTRACTION DES VENTES A LIVRER                                    
#   FIC FTL000 : FIC DES VENTES POUR LOGICIEL DES LIVRAISONS                   
#   FIC FTL001 : FIC DES ANOMALIES,CUMUL DE PROFIL(1 LOGNE PAR PROFIL)         
#   PGM = BTL000                                                               
#   REPRISE: OUI APRES ABEND                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAG
       ;;
(GTL00YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  LIEUX                                                                
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  ADRESSES                                                             
#    RSGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
# ******  RESERVATION STOCK ENTREPOT                                           
#    RSGV21Y  : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21Y /dev/null
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11Y  : NAME=RSGV11Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  LIVRAISON CLIENT A DESTOCKEY                                         
#    RSGD05Y  : NAME=RSGD05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD05Y /dev/null
# ******  PROFIL DE TOURNEES DEMANDEES                                         
#    RSTL05Y  : NAME=RSTL05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05Y /dev/null
#                                                                              
       m_FileAssign -d SHR FTL000 /dev/null
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A1} FTL001 ${DATA}/PTL945/F45.BTL001CY
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A2} FTL002 ${DATA}/PTL945/F45.BTL002DY
# ******  FICHIER EN DUMMY                                                     
       m_FileAssign -d SHR FTL003 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL000 
       JUMP_LABEL=GTL00YAH
       ;;
(GTL00YAH)
       m_CondExec 04,GE,GTL00YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL001CY POUR ELIMINER LES DOUBLONS                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAJ
       ;;
(GTL00YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTL945/F45.BTL001CY
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL945/F45.BTL001BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00YAK
       ;;
(GTL00YAK)
       m_CondExec 00,EQ,GTL00YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL002DY POUR ELIMINER LES DOUBLONS CREES LORS DES           
#  REPRISES (CREATION ENTRE 2 COMMITS)                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAM
       ;;
(GTL00YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTL945/F45.BTL002DY
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL945/F45.BTL002AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00YAN
       ;;
(GTL00YAN)
       m_CondExec 00,EQ,GTL00YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LES CHAINES : GTL45Y GTL55Y                       
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAQ
       ;;
(GTL00YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTL945/F45.BTL001BY
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTL945/F45.BTL002AY
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL945/F45.BTL001AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00YAR
       ;;
(GTL00YAR)
       m_CondExec 00,EQ,GTL00YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   FUSION 9,3,A,1,8,A,12,23,A                                                 
#   SOC,JOUR,PLAN,TYPE D ENR,ZONE TRI                                          
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAT
       ;;
(GTL00YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTL945/F45.BTL001AY
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL00YAT.BTL030AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_12_23 12 CH 23
 /FIELDS FLD_CH_9_3 9 CH 3
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_23 ASCENDING
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00YAU
       ;;
(GTL00YAU)
       m_CondExec 00,EQ,GTL00YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    ECRITURE   DANS LA BASE IMPRESSION GENERALISEE                            
#    1 LISTE NOMBRE DE PROFILS TRAITES                                         
#    1 LISTE ANOMALIES                                                         
#    1 LISTE DES TRANSITAIRES (POUR L ETRANGER)                                
#    REPRISE: NON  BACKOUT DU STEP PAR LOGGING IMS                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YAX
       ;;
(GTL00YAX)
       m_CondExec ${EXABJ},NE,YES 
# BJ      IMSSTEP PGM=BTL030,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL00YR1)                                             
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL00YR1
       m_OutputAssign -c "*" DDOTV02
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DY,MODE=U,REST=(YES,GTL00YR1)                       
# DIGVIP   FILE NAME=DI0000IY,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL00YAX
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FTL030 ${DATA}/PTEM/GTL00YAT.BTL030AY
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  MISE A JOUR DES RETOURS DE LIVRAISON RECYCLEES                              
#  PGM = BTL085                                                                
#  REPRISE: OUI APRES ABEND                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YBA
       ;;
(GTL00YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  RETOURS DES TOURNEES                                                 
#    RSTL04Y  : NAME=RSTL04Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04Y /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL085 
       JUMP_LABEL=GTL00YBB
       ;;
(GTL00YBB)
       m_CondExec 04,GE,GTL00YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL00YZA
       ;;
(GTL00YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
