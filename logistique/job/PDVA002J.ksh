#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PDVA002J.ksh                       --- VERSION DU 19/10/2016 19:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDVA002J -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: VA002D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#  EDITION IVA510                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA002DJ
       ;;
(VA002DJ)
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXCAK=${EXCAK:-0}
       EXCAP=${EXCAP:-0}
       EXCAU=${EXCAU:-0}
       EXCAZ=${EXCAZ:-0}
       EXCBE=${EXCBE:-0}
       EXCBJ=${EXCBJ:-0}
       EXCBO=${EXCBO:-0}
       EXCBT=${EXCBT:-0}
       EXCBY=${EXCBY:-0}
       EXCCD=${EXCCD:-0}
       EXCCI=${EXCCI:-0}
       EXCCN=${EXCCN:-0}
       EXCCS=${EXCCS:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2014/06/03 AT 16.23.35 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: VA002D                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'ETATS COMPTA VALO'                     
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA002DJA
       ;;
(VA002DJA)
       m_CondExec ${EXCAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA510ADD
       m_OutputAssign -c 9 -w IVA510 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJB
       ;;
(VA002DJB)
       m_CondExec 00,EQ,VA002DJA ${EXCAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA511                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJD PGM=IEBGENER   ** ID=CAF                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJD
       ;;
(VA002DJD)
       m_CondExec ${EXCAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA511ADD
       m_OutputAssign -c 9 -w IVA511 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJE
       ;;
(VA002DJE)
       m_CondExec 00,EQ,VA002DJD ${EXCAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA520                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJG PGM=IEBGENER   ** ID=CAK                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJG
       ;;
(VA002DJG)
       m_CondExec ${EXCAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA520ADD
       m_OutputAssign -c 9 -w IVA520 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJH
       ;;
(VA002DJH)
       m_CondExec 00,EQ,VA002DJG ${EXCAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA540                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJJ PGM=IEBGENER   ** ID=CAP                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJJ
       ;;
(VA002DJJ)
       m_CondExec ${EXCAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA540ADD
       m_OutputAssign -c 9 -w IVA540D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJK
       ;;
(VA002DJK)
       m_CondExec 00,EQ,VA002DJJ ${EXCAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA530                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJM PGM=IEBGENER   ** ID=CAU                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJM
       ;;
(VA002DJM)
       m_CondExec ${EXCAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA530ADD
       m_OutputAssign -c 9 -w IVA530 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJN
       ;;
(VA002DJN)
       m_CondExec 00,EQ,VA002DJM ${EXCAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA550D                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJQ PGM=IEBGENER   ** ID=CAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJQ
       ;;
(VA002DJQ)
       m_CondExec ${EXCAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA550ADD
       m_OutputAssign -c 9 -w IVA550D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJR
       ;;
(VA002DJR)
       m_CondExec 00,EQ,VA002DJQ ${EXCAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA560                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJT PGM=IEBGENER   ** ID=CBE                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJT
       ;;
(VA002DJT)
       m_CondExec ${EXCBE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA560ADD
       m_OutputAssign -c 9 -w IVA560 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJU
       ;;
(VA002DJU)
       m_CondExec 00,EQ,VA002DJT ${EXCBE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA570D                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DJX PGM=IEBGENER   ** ID=CBJ                                   
# ***********************************                                          
       JUMP_LABEL=VA002DJX
       ;;
(VA002DJX)
       m_CondExec ${EXCBJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA570DDD
       m_OutputAssign -c 9 -w IVA570D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DJY
       ;;
(VA002DJY)
       m_CondExec 00,EQ,VA002DJX ${EXCBJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA580D                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKA PGM=IEBGENER   ** ID=CBO                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKA
       ;;
(VA002DKA)
       m_CondExec ${EXCBO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA581DDD
       m_OutputAssign -c 9 -w IVA580D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DKB
       ;;
(VA002DKB)
       m_CondExec 00,EQ,VA002DKA ${EXCBO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA590                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKD PGM=IEBGENER   ** ID=CBT                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKD
       ;;
(VA002DKD)
       m_CondExec ${EXCBT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA590ADD
       m_OutputAssign -c 9 -w IVA590 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DKE
       ;;
(VA002DKE)
       m_CondExec 00,EQ,VA002DKD ${EXCBT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA540O                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKG PGM=IEBGENER   ** ID=CBY                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKG
       ;;
(VA002DKG)
       m_CondExec ${EXCBY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA540ODD
       m_OutputAssign -c 9 -w IVA540O SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DKH
       ;;
(VA002DKH)
       m_CondExec 00,EQ,VA002DKG ${EXCBY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA580O                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKJ PGM=IEBGENER   ** ID=CCD                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKJ
       ;;
(VA002DKJ)
       m_CondExec ${EXCCD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA580ODD
       m_OutputAssign -c 9 -w IVA580O SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DKK
       ;;
(VA002DKK)
       m_CondExec 00,EQ,VA002DKJ ${EXCCD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA585                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA585 (SUIV. STOCKS CONSOMMABLE)         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKM PGM=IKJEFT01   ** ID=CCI                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKM
       ;;
(VA002DKM)
       m_CondExec ${EXCCI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTVA25   : NAME=RSVA25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA05   : NAME=RSVA05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGA55   : NAME=RSGA55D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA55 /dev/null
#    RTGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIERS D'EXTRACTION DARTY                                          
# * IVA585   REPORT SYSOUT=(9,IVA585),FREE=CLOSE,RECFM=FBA,LRECL=133,          
# *          BLKSIZE=0                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IVA585 ${DATA}/PXX0.FICHEML.EXVA0D.EXVA85AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA585 
       JUMP_LABEL=VA002DKN
       ;;
(VA002DKN)
       m_CondExec 04,GE,VA002DKM ${EXCCI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA585                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKQ PGM=IEBGENER   ** ID=CCN                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKQ
       ;;
(VA002DKQ)
       m_CondExec ${EXCCN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.EXVA85AD
       m_OutputAssign -c 9 -w IVA585 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DKR
       ;;
(VA002DKR)
       m_CondExec 00,EQ,VA002DKQ ${EXCCN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA595                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002DKT PGM=IEBGENER   ** ID=CCS                                   
# ***********************************                                          
       JUMP_LABEL=VA002DKT
       ;;
(VA002DKT)
       m_CondExec ${EXCCS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0D.VA595ADD
       m_OutputAssign -c 9 -w IVA595 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002DKU
       ;;
(VA002DKU)
       m_CondExec 00,EQ,VA002DKT ${EXCCS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
