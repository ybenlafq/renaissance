#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF50M.ksh                       --- VERSION DU 08/10/2016 23:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMQMF50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/08/02 AT 08.38.50 BY BURTECR                      
#    STANDARDS: P  JOBSET: QMF50M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF115D                                  
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF50MA
       ;;
(QMF50MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       RUN=${RUN}
       JUMP_LABEL=QMF50MAA
       ;;
(QMF50MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i SORTIN
RUN ADMFIL.Q115D (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F115D
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50MAB
       ;;
(QMF50MAB)
       m_CondExec 00,EQ,QMF50MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115D                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAD
       ;;
(QMF50MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF115D DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50MAE
       ;;
(QMF50MAE)
       m_CondExec 04,GE,QMF50MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAG PGM=IEFBR14    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAG
       ;;
(QMF50MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF116D                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAJ
       ;;
(QMF50MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q116D (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F116D
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50MAK
       ;;
(QMF50MAK)
       m_CondExec 00,EQ,QMF50MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF116D                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAM
       ;;
(QMF50MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF116D DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50MAN
       ;;
(QMF50MAN)
       m_CondExec 04,GE,QMF50MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAQ PGM=IEFBR14    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAQ
       ;;
(QMF50MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF115G                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAT
       ;;
(QMF50MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q115G (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&NSOC='$DALDEP_1_3' FORM=ADMFIL.F115G
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50MAU
       ;;
(QMF50MAU)
       m_CondExec 00,EQ,QMF50MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115G                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MAX
       ;;
(QMF50MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF115G DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50MAY
       ;;
(QMF50MAY)
       m_CondExec 04,GE,QMF50MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBA PGM=IEFBR14    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBA
       ;;
(QMF50MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF116G                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBD
       ;;
(QMF50MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q116G (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&NSOC='$DALDEP_1_3' FORM=ADMFIL.F116G
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF50MBE
       ;;
(QMF50MBE)
       m_CondExec 00,EQ,QMF50MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF116G                                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBG
       ;;
(QMF50MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF116G DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF50MBH
       ;;
(QMF50MBH)
       m_CondExec 04,GE,QMF50MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBJ PGM=IEFBR14    ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBJ
       ;;
(QMF50MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  QMFBATCH : REQUETE Q115GEM                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBM
       ;;
(QMF50MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w Q115GEM DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q115GEM (&&DMOIS='$FMOISJ_5_4$FMOISJ_3_2' FORM=ADMFIL.F115GEM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF50M4 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF50MBN
       ;;
(QMF50MBN)
       m_CondExec 04,GE,QMF50MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE Q115GEM POUR FICHIER                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBQ
       ;;
(QMF50MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.Q115GEM
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q115GEM (&&DMOIS='$FMOISJ_5_4$FMOISJ_3_2' FORM=ADMFIL.F115GEM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF50M4 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF50MBR
       ;;
(QMF50MBR)
       m_CondExec 04,GE,QMF50MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115D                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBT
       ;;
(QMF50MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF115D
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q115D (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F115D
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF50M1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF50MBU
       ;;
(QMF50MBU)
       m_CondExec 04,GE,QMF50MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF115T                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MBX
       ;;
(QMF50MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF115T
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q115T (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F115T
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF50M2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF50MBY
       ;;
(QMF50MBY)
       m_CondExec 04,GE,QMF50MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF QMF116D                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF50MCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF50MCA
       ;;
(QMF50MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF116D
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q116D (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F116D
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF50M3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF50MCB
       ;;
(QMF50MCB)
       m_CondExec 04,GE,QMF50MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
