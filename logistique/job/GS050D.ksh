#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS050D.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGS050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/07/04 AT 15.10.43 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GS050D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BSM050 : EXTRACTION DES CODIS AYANT DU STOCK NEGATIF                        
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS050DA
       ;;
(GS050DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS050DAA
       ;;
(GS050DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00D /dev/null
#    RTGA14D  : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14D /dev/null
#    RTGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10D /dev/null
#    RTGS30D  : NAME=RSGS30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGS30D /dev/null
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FSM050 ${DATA}/PXX0/GS050DAA.BSM050AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM050 
       JUMP_LABEL=GS050DAB
       ;;
(GS050DAB)
       m_CondExec 04,GE,GS050DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BSM050AP POUR LE GENERATEUR D'ETAT ISM050                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS050DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS050DAD
       ;;
(GS050DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GS050DAA.BSM050AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GS050DAD.BSM050BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_24_5 24 CH 5
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_PD_21_3 21 PD 3
 /FIELDS FLD_BI_18_3 18 CH 3
 /FIELDS FLD_BI_7_8 7 CH 8
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_PD_21_3 ASCENDING,
   FLD_BI_24_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS050DAE
       ;;
(GS050DAE)
       m_CondExec 00,EQ,GS050DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS050DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS050DAG
       ;;
(GS050DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00D  : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30D /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/GS050DAD.BSM050BD
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GS050DAG.BSM050CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GS050DAH
       ;;
(GS050DAH)
       m_CondExec 04,GE,GS050DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS050DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS050DAJ
       ;;
(GS050DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GS050DAG.BSM050CD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GS050DAJ.BSM050DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS050DAK
       ;;
(GS050DAK)
       m_CondExec 00,EQ,GS050DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT ISM050 (CODIC AYANT DU STOCK NEGATIF)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS050DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS050DAM
       ;;
(GS050DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01D /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71D  : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71D /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00D  : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30D /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/GS050DAD.BSM050BD
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PXX0/GS050DAJ.BSM050DD
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DIF                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DIF                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT ISM050                                                          
       m_OutputAssign -c 9 -w ISM050 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GS050DAN
       ;;
(GS050DAN)
       m_CondExec 04,GE,GS050DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS050DZA
       ;;
(GS050DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS050DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
