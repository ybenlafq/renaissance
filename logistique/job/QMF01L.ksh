#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF01L.ksh                       --- VERSION DU 17/10/2016 18:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLQMF01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/15 AT 15.02.44 BY PREPA3                       
#    STANDARDS: P  JOBSET: QMF01L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='DNPC'                                                              
# ********************************************************************         
#   LISTE EN-COURS DES BE                                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF01LA
       ;;
(QMF01LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       LASTJO=${LASTJO}
       QMF01LA=${DIMANCH}
       QMF01LB=${LUNDI}
       QMF01LD=${MERCRED}
       QMF01LE=${SAMEDI}
       QMF01LF=${VENDRED}
       RUN=${RUN}
       JUMP_LABEL=QMF01LAA
       ;;
(QMF01LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF001I DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX2/QMF01L2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAB
       ;;
(QMF01LAB)
       m_CondExec 04,GE,QMF01LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DE MVTS DE STOCK ENTREPOT (REGUL) ETAT1                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAD
       ;;
(QMF01LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF06 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q006 (&&JOUR='$VDATEJ_ANNMMDD' &&NSOC='$DNPCDEP_1_3' FORM=ADMFIL.F006
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAE
       ;;
(QMF01LAE)
       m_CondExec 04,GE,QMF01LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENTREES HS                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAG
       ;;
(QMF01LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF18 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q018 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=ADMFIL.F018
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L11 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAH
       ;;
(QMF01LAH)
       m_CondExec 04,GE,QMF01LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENCOURS DE HS                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAJ
       ;;
(QMF01LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF20 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q020 (&&DEBUT='$QMFDEB_AAQQQ' &&FIN='$QMFFIN_AAQQQ' FORM=ADMFIL.F020
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L12 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAK
       ;;
(QMF01LAK)
       m_CondExec 04,GE,QMF01LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES MISES A JOUR DE PRIX                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAM
       ;;
(QMF01LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF021 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q021 (&&JOUR='$VDATEJ1_ANNMMDD' FORM=ADMFIL.F021
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L13 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAN
       ;;
(QMF01LAN)
       m_CondExec 04,GE,QMF01LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES REPRISES EN ATTENTES   QMF022                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAQ
       ;;
(QMF01LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF22 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q022 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.F022
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L14 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAR
       ;;
(QMF01LAR)
       m_CondExec 04,GE,QMF01LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENTREES                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAT
       ;;
(QMF01LAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[QMF01LA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF25 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q025 (&&DEBUT='$QMFDEB_ANNMMJJ' &&FIN='$QMFFIN_ANNMMJJ' FORM=ADMFIL.F025
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L16 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAU
       ;;
(QMF01LAU)
       m_CondExec 04,GE,QMF01LAT ${EXABE},NE,YES 1,EQ,$[QMF01LA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES SORTIES                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LAX
       ;;
(QMF01LAX)
       m_CondExec ${EXABJ},NE,YES 1,EQ,$[QMF01LA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF180 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q180 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.F180
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L18 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LAY
       ;;
(QMF01LAY)
       m_CondExec 04,GE,QMF01LAX ${EXABJ},NE,YES 1,EQ,$[QMF01LA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BQ028A : REQUETE Q028A                                                      
#   LISTE STOCK POUR LES ACHATS                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBA
       ;;
(QMF01LBA)
       m_CondExec ${EXABO},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******* SOCIETE = 961                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FIC EDITION POUR IMP. GENERALISEE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FQ028A ${DATA}/PTEM/QMF01LBA.Q028AAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQ028A 
       JUMP_LABEL=QMF01LBB
       ;;
(QMF01LBB)
       m_CondExec 04,GE,QMF01LBA ${EXABO},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (Q028AAL)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBD
       ;;
(QMF01LBD)
       m_CondExec ${EXABT},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/QMF01LBA.Q028AAL
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/QMF01LBD.Q028ABL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_CH_45_3 45 CH 03
 /FIELDS FLD_CH_48_7 48 CH 07
 /FIELDS FLD_CH_40_5 40 CH 5
 /FIELDS FLD_CH_30_5 30 CH 5
 /FIELDS FLD_CH_35_5 35 CH 5
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_5 ASCENDING,
   FLD_CH_35_5 ASCENDING,
   FLD_CH_40_5 ASCENDING,
   FLD_CH_45_3 ASCENDING,
   FLD_CH_48_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01LBE
       ;;
(QMF01LBE)
       m_CondExec 00,EQ,QMF01LBD ${EXABT},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBG
       ;;
(QMF01LBG)
       m_CondExec ${EXABY},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/QMF01LBD.Q028ABL
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/QMF01LBG.Q028ACL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF01LBH
       ;;
(QMF01LBH)
       m_CondExec 04,GE,QMF01LBG ${EXABY},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBJ
       ;;
(QMF01LBJ)
       m_CondExec ${EXACD},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/QMF01LBG.Q028ACL
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/QMF01LBJ.Q028ADL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01LBK
       ;;
(QMF01LBK)
       m_CondExec 00,EQ,QMF01LBJ ${EXACD},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   LISTE STOCK POUR LES ACHATS                                                
#  CREATION DE L'ETAT : Q028A                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBM
       ;;
(QMF01LBM)
       m_CondExec ${EXACI},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/QMF01LBD.Q028ABL
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/QMF01LBJ.Q028ADL
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w Q028A FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF01LBN
       ;;
(QMF01LBN)
       m_CondExec 04,GE,QMF01LBM ${EXACI},NE,YES 1,NE,$[QMF01LB] 1,NE,$[QMF01LD] 1,NE,$[QMF01LF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BQ028B : REQUETE Q028B                                                      
#   LISTE STOCK POUR LES ACHATS                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBQ
       ;;
(QMF01LBQ)
       m_CondExec ${EXACN},NE,YES 1,NE,$[QMF01LE] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******* SOCIETE = 961                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FIC EDITION POUR IMP. GENERALISEE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FQ028B ${DATA}/PTEM/QMF01LBQ.Q028BAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQ028B 
       JUMP_LABEL=QMF01LBR
       ;;
(QMF01LBR)
       m_CondExec 04,GE,QMF01LBQ ${EXACN},NE,YES 1,NE,$[QMF01LE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (Q028BAL)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBT
       ;;
(QMF01LBT)
       m_CondExec ${EXACS},NE,YES 1,NE,$[QMF01LE] 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/QMF01LBQ.Q028BAL
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/QMF01LBT.Q028BBL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_30_5 30 CH 5
 /FIELDS FLD_CH_35_5 35 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_48_7 48 CH 07
 /FIELDS FLD_CH_40_5 40 CH 5
 /FIELDS FLD_CH_45_3 45 CH 03
 /FIELDS FLD_CH_10_20 10 CH 20
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_5 ASCENDING,
   FLD_CH_35_5 ASCENDING,
   FLD_CH_40_5 ASCENDING,
   FLD_CH_45_3 ASCENDING,
   FLD_CH_48_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01LBU
       ;;
(QMF01LBU)
       m_CondExec 00,EQ,QMF01LBT ${EXACS},NE,YES 1,NE,$[QMF01LE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LBX
       ;;
(QMF01LBX)
       m_CondExec ${EXACX},NE,YES 1,NE,$[QMF01LE] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/QMF01LBT.Q028BBL
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/QMF01LBX.Q028BCL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=QMF01LBY
       ;;
(QMF01LBY)
       m_CondExec 04,GE,QMF01LBX ${EXACX},NE,YES 1,NE,$[QMF01LE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCA
       ;;
(QMF01LCA)
       m_CondExec ${EXADC},NE,YES 1,NE,$[QMF01LE] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/QMF01LBX.Q028BCL
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/QMF01LCA.Q028BDL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01LCB
       ;;
(QMF01LCB)
       m_CondExec 00,EQ,QMF01LCA ${EXADC},NE,YES 1,NE,$[QMF01LE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#   LISTE STOCK POUR LES ACHATS                                                
#  CREATION DE L'ETAT : Q028B                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCD
       ;;
(QMF01LCD)
       m_CondExec ${EXADH},NE,YES 1,NE,$[QMF01LE] 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/QMF01LBT.Q028BBL
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/QMF01LCA.Q028BDL
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w Q028B FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=QMF01LCE
       ;;
(QMF01LCE)
       m_CondExec 04,GE,QMF01LCD ${EXADH},NE,YES 1,NE,$[QMF01LE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES BE DU JOUR                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCG
       ;;
(QMF01LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF33 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q033 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.F033
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L23 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LCH
       ;;
(QMF01LCH)
       m_CondExec 04,GE,QMF01LCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES MOUVEMENTS PAR ARTICLE (VENTES ET REPRISES)                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCJ
       ;;
(QMF01LCJ)
       m_CondExec ${EXADR},NE,YES 1,EQ,$[QMF01LB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF155 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q155 (&&JOUR='$VDATEJ__ANN_MM_JJ' &&SOC='$DNPCDEP_1_3' FORM=ADMFIL.F155
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L25 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LCK
       ;;
(QMF01LCK)
       m_CondExec 04,GE,QMF01LCJ ${EXADR},NE,YES 1,EQ,$[QMF01LB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF164                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCM
       ;;
(QMF01LCM)
       m_CondExec ${EXADW},NE,YES 1,EQ,$[QMF01LB] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF164 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q164 (&&DEBUT='$QMFDEB8_ANNMMJJ' &&FIN='$QMFFIN8_ANNMMJJ' FORM=ADMFIL.F164
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L27 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LCN
       ;;
(QMF01LCN)
       m_CondExec 04,GE,QMF01LCM ${EXADW},NE,YES 1,EQ,$[QMF01LB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE CONTROLE DE RECUPERATION DE CAISSE                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCQ
       ;;
(QMF01LCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w CONTCAIS DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.CON01 (&&JOUR='$VDATEJ_ANNMMDD' &&NSOC='$DNPCDEP_1_3' FORM=ADMFIL.FCON01
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L29 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LCR
       ;;
(QMF01LCR)
       m_CondExec 04,GE,QMF01LCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE CONTROLE DE RECUPERATION DES VENTES EN MGIS                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCT
       ;;
(QMF01LCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w CONTMGIS DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.CON02 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.FCON01
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L30 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LCU
       ;;
(QMF01LCU)
       m_CondExec 04,GE,QMF01LCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PERMET DE DISPOSER D'UN ETAT DE STOCK PERMETTANT DE SUIVRE LA              
#   CODIFICATION DES LIEUX DE STOCKAGE DES PRODUITS                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LCX
       ;;
(QMF01LCX)
       m_CondExec ${EXAEL},NE,YES 1,NE,$[QMF01LE] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFGD31A DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX2/QMF01L31
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L31 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LCY
       ;;
(QMF01LCY)
       m_CondExec 04,GE,QMF01LCX ${EXAEL},NE,YES 1,NE,$[QMF01LE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PERMET DE DISPOSER D'UN ETAT DE STOCK PERMETTANT DE SUIVRE LA              
#   CODIFICATION DES LIEUX DE STOCKAGE DES PRODUITS EN HEBDO                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDA
       ;;
(QMF01LDA)
       m_CondExec ${EXAEQ},NE,YES 1,EQ,$[QMF01LA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFGD31B DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01L32
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L32 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LDB
       ;;
(QMF01LDB)
       m_CondExec 04,GE,QMF01LDA ${EXAEQ},NE,YES 1,EQ,$[QMF01LA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES H.S PAR LIEUX DE TRAITEMENT                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDD
       ;;
(QMF01LDD)
       m_CondExec ${EXAEV},NE,YES 1,EQ,$[QMF01LA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF181 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01L33
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L33 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LDE
       ;;
(QMF01LDE)
       m_CondExec 04,GE,QMF01LDD ${EXAEV},NE,YES 1,EQ,$[QMF01LA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES H.S PAR LIEUX DE TRAITEMENT FIN DE MOIS                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDG
       ;;
(QMF01LDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF181 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF01L34
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L34 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LDH
       ;;
(QMF01LDH)
       m_CondExec 04,GE,QMF01LDG ${EXAFA},NE,YES 1,EQ,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LA QUERY QMF (QHSDEP)                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDJ
       ;;
(QMF01LDJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[QMF01LB] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.QHSDEP (&&NSOC='$DNPCDEP_1_3' FORM=ADMFIL.FHSDEP
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F61.QMF001AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01LDK
       ;;
(QMF01LDK)
       m_CondExec 00,EQ,QMF01LDJ ${EXAFF},NE,YES 1,EQ,$[QMF01LB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DE DEPHASAGE STOCK HS GS10/GS60                                      
#                                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDM
       ;;
(QMF01LDM)
       m_CondExec ${EXAFK},NE,YES 1,EQ,$[QMF01LB] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QHSDEP DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F61.QMF001AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01LDN
       ;;
(QMF01LDN)
       m_CondExec 04,GE,QMF01LDM ${EXAFK},NE,YES 1,EQ,$[QMF01LB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDQ PGM=IEFBR14    ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDQ
       ;;
(QMF01LDQ)
       m_CondExec ${EXAFP},NE,YES 1,EQ,$[QMF01LB] 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F61.QMF001AL
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  QMFBATCH : REQUETE QGF002                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDT PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDT
       ;;
(QMF01LDT)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGF002 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QGF002 (&&JOUR='$VDATEJ_ANNMMJJ' FORM=ADMFIL.FGF002
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01L04 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LDU
       ;;
(QMF01LDU)
       m_CondExec 04,GE,QMF01LDT ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QGSM7H                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01LDX PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LDX
       ;;
(QMF01LDX)
       m_CondExec ${EXAGE},NE,YES 1,EQ,$[QMF01LA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QGSM7H DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QGSM7H (&&DATEDEB='$QMFDEB_ANNMMJJ' &&DATEFIN='$QMFFIN_ANNMMJJ' FORM=ADMFIL.FGSM7H
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00L32 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01LDY
       ;;
(QMF01LDY)
       m_CondExec 04,GE,QMF01LDX ${EXAGE},NE,YES 1,EQ,$[QMF01LA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=QMF01LZA
       ;;
(QMF01LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QMF01LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
