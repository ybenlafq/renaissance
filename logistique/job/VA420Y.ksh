#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA420Y.ksh                       --- VERSION DU 08/10/2016 22:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYVA420 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 02/11/07 AT 17.17.16 BY BURTECC                      
#    STANDARDS: P  JOBSET: VA420Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  REPROS DES FICHIERS BVA511AY ET BVA520AY                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA420YA
       ;;
(VA420YA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2002/11/07 AT 17.17.16 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: VA420Y                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'EXTRAC COMPTA GCT'                     
# *                           APPL...: REPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA420YAA
       ;;
(VA420YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******  FIC MVTS DU MOIS POUR LES LIEUX 'AVO' ET RCY'                        
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PNCGY/F45.BVA511AY
# ******  FIC DES ACHATS TRIES PAR FOURNISSEUR ET RAYON                        
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PNCGY/F45.BVA520AY
# ******  FIC REPRO POUR FT420Y                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 OUT1 ${DATA}/PNCGY/F45.BVA511ZY
# ******  FIC REPRO POUR FT420Y                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 OUT2 ${DATA}/PNCGY/F45.BVA520ZY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA420YAA.sysin
       m_UtilityExec
# **************************                                                   
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VA420YAB
       ;;
(VA420YAB)
       m_CondExec 16,NE,VA420YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
