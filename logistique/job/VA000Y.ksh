#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA000Y.ksh                       --- VERSION DU 13/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYVA000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/08/30 AT 11.25.49 BY PREPA2                       
#    STANDARDS: P  JOBSET: VA000Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTGA20 ET RTGA21                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VA000YA
       ;;
(VA000YA)
#
#VA000YAX
#VA000YAX Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#VA000YAX
#
#
#VA000YAA
#VA000YAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#VA000YAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VA000YAD
       ;;
(VA000YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE LIBELLE RAYONS                                                 
#    RTGA20   : NAME=RSGA20Y,MODE=U - DYNAM=YES                                
# -X-RSGA20Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTGA20 /dev/null
# *****   TABLE COMPOSANTS DES RAYONS                                          
#    RTGA21   : NAME=RSGA21Y,MODE=U - DYNAM=YES                                
# -X-RSGA21Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTGA21 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA501 
       JUMP_LABEL=VA000YAE
       ;;
(VA000YAE)
       m_CondExec 04,GE,VA000YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA502                                                                
# ********************************************************************         
#  MISE JOUR DE LA TABLE RTVA25 EN FONCTION DE LA SOUS-TABLE 'VARAY'           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA000YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA000YAG
       ;;
(VA000YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *****   TABLE DES RAYONS DE VALORISATION                                     
#    RTVA25   : NAME=RSVA25Y,MODE=U - DYNAM=YES                                
# -X-RSVA25Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTVA25 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA502 
       JUMP_LABEL=VA000YAH
       ;;
(VA000YAH)
       m_CondExec 04,GE,VA000YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEX001                                                                
# ********************************************************************         
#  EXTRACTION DES ARTICLES                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA000YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA000YAJ
       ;;
(VA000YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# *****   PARAMETRE CODE TRAITEMENT 009                                        
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/VA000YAJ
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/VA000YAJ.BVA000AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=VA000YAK
       ;;
(VA000YAK)
       m_CondExec 04,GE,VA000YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER ISSU DU STEP PRECEDENT                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA000YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA000YAM
       ;;
(VA000YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/VA000YAJ.BVA000AY
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/VA000YAM.BVA000BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_26 7 CH 26
 /FIELDS FLD_CH_559_7 559 CH 7
 /FIELDS FLD_CH_90_7 90 CH 7
 /FIELDS FLD_CH_177_4 177 CH 4
 /KEYS
   FLD_CH_559_7 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 600 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_559_7,FLD_CH_177_4,FLD_CH_7_26,FLD_CH_90_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA000YAN
       ;;
(VA000YAN)
       m_CondExec 00,EQ,VA000YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA503                                                                
# ********************************************************************         
#  CREATION DU FICHIER SERVANT A LOADER LA TABLE RTVA20                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA000YAQ PGM=BVA503     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA000YAQ
       ;;
(VA000YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A2} FEX001T ${DATA}/PTEM/VA000YAM.BVA000BY
# *****   FICHIER SERVANT A LOADER LA RTVA20                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 RTVA20 ${DATA}/PVA945/F45.VA20RY
       m_ProgramExec BVA503 
# ********************************************************************         
#  PGM : LOAD                                                                  
# ********************************************************************         
#  LOAD DE LA TABLE RTVA20                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA000YAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA000YAT
       ;;
(VA000YAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RTVA20   : NAME=RSVA20Y,MODE=U - DYNAM=YES                                
# -X-RSVA20Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RTVA20 /dev/null
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PVA945/F45.VA20RY
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA000YAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA000Y_VA000YAT_RTVA20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA000YAU
       ;;
(VA000YAU)
       m_CondExec 04,GE,VA000YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : REPAIY                                                                
# ********************************************************************         
#  REPAIR NOCOPY TABLESPACE RSVA20Y DE LA D.B PYDVA00                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA000YAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VA000YZA
       ;;
(VA000YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA000YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
