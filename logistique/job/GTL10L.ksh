#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL10L.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGTL10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/11/25 AT 11.16.24 BY PREPA2                       
#    STANDARDS: P  JOBSET: GTL10L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BTL110                                                                
#  EXTRACTION PERMETTANT D'ALIMENTER LA TABLE RTTL09                           
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
#           NON SI FIN NORMALE FAIRE UN RECOVER A LA F.I.C DE RTTL09           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL10LA
       ;;
(GTL10LA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL10LAA
       ;;
(GTL10LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES QUOTAS DE LIVRAISON         
#    RSGQ01L  : NAME=RSGQ01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01L /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40L  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40L /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02L  : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02L /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23L  : NAME=RSGV23L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23L /dev/null
# ************************************** TABLE DES TOUNEES (GENERALITE         
#    RSTL01L  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL01L /dev/null
# ************************************** TABLE DES TOURNEES (DETAILS)          
#    RSTL02L  : NAME=RSTL02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02L /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04L  : NAME=RSTL04L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04L /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09L  : NAME=RSTL09L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09L /dev/null
# ************************************** TABLE HISTO VENTES                    
#    RSHV15L  : NAME=RSHV15L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15L /dev/null
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DNPCDEP
# ************************************** PARAMETRE  H:HEBDO  M:MENSUEL         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10LAA
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F61.FTL110AL
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL110N ${DATA}/PXX0/F61.FTL110AL
# ************************************** FIC D'EXTRACT FTL111                  
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111 ${DATA}/PXX0/F61.BTL111AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL10LAB
       ;;
(GTL10LAB)
       m_CondExec 04,GE,GTL10LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE : CUMUL DU FICHIER BTL111AL CREE PAR LE PROG BTL110 AVEC              
#          L'ANCIEN BTL111AL DU DERNIER TRAITEMENT                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAD
       ;;
(GTL10LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F61.BTL111AL
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F61.BTL111AL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g ${G_A3} SORTOUT ${DATA}/PXX0/F61.BTL111AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LAE
       ;;
(GTL10LAE)
       m_CondExec 00,EQ,GTL10LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL111                                                                
#  INITIALISATION DES FICHIERS FTL111A  ET FTL111B                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAG
       ;;
(GTL10LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISE                      
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00L  : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ************************************** FIC D'EXTRACT ISSU DU BLT110          
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F61.FTL110AL
# ************************************** FIC D'EXTRACT ISSU DU TRI 1A          
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F61.BTL111AL
# ************************************** FICHIER FTL111A                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111A ${DATA}/PXX0/F61.FTL111AL
# ************************************** FICHIER FTL111B                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FTL111B ${DATA}/PXX0/F61.FTL111BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL10LAH
       ;;
(GTL10LAH)
       m_CondExec 04,GE,GTL10LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2A : TRI DU FICHIER FTL111AL POUR CREATION DU FLT112AL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAJ
       ;;
(GTL10LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F61.FTL111AL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LAJ.FTL112AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LAK
       ;;
(GTL10LAK)
       m_CondExec 00,EQ,GTL10LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2B : TRI DU FICHIER FTL111AL POUR CREATION DU FTL112BL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAM
       ;;
(GTL10LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F61.FTL111AL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LAM.FTL112BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LAN
       ;;
(GTL10LAN)
       m_CondExec 00,EQ,GTL10LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2C : TRI DU FICHIER FTL111BL POUR CREATION FU FTL112CL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAQ
       ;;
(GTL10LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F61.FTL111BL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LAQ.FTL112CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LAR
       ;;
(GTL10LAR)
       m_CondExec 00,EQ,GTL10LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL112                                                                
#  CREATION DES ETATS ITL112A ET ITL112B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAT
       ;;
(GTL10LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00L  : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F61.FTL110AL
# ************************************** FICHIERS ISSU DU TRI 2A               
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL10LAJ.FTL112AL
# ************************************** FICHIERS ISSU DU TRI 2B               
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL10LAM.FTL112BL
# ************************************** FICHIERS ISSU DU TRI 2C               
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL10LAQ.FTL112CL
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L                  
       m_OutputAssign -c 9 -w ITL112A ITL112A
# ************************************** ETAT DE STAT PAR PIECES               
       m_OutputAssign -c 9 -w ITL112B ITL112B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL10LAU
       ;;
(GTL10LAU)
       m_CondExec 04,GE,GTL10LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3A : TRI DU FICHIER FTL111AL POUR CREATION DU FTL113AL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LAX
       ;;
(GTL10LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F61.FTL111AL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LAX.FTL113AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LAY
       ;;
(GTL10LAY)
       m_CondExec 00,EQ,GTL10LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3B : TRI DU FICHIER FTL111AL POUR CREATION DU FTL113BL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBA
       ;;
(GTL10LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F61.FTL111AL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LBA.FTL113BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LBB
       ;;
(GTL10LBB)
       m_CondExec 00,EQ,GTL10LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3C : TRI DES FICHIERS FTL111AL,FTL111BL POUR CREATION DU FTL113         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBD
       ;;
(GTL10LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F61.FTL111BL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LBD.FTL113CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LBE
       ;;
(GTL10LBE)
       m_CondExec 00,EQ,GTL10LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3D : TRI DES FICHIERS FTL111AL,FTL111BL POUR CREATION FU FTL113         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBG
       ;;
(GTL10LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F61.FTL111BL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LBG.FTL113DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LBH
       ;;
(GTL10LBH)
       m_CondExec 00,EQ,GTL10LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL113                                                                
#  CREATION DE L'ETAT ITL113                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBJ
       ;;
(GTL10LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00L  : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F61.FTL110AL
# ************************************** FICHIERS ISSU DU TRI 3A               
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL10LAX.FTL113AL
# ************************************** FICHIERS ISSU DU TRI 3B               
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL10LBA.FTL113BL
# ************************************** FICHIERS ISSU DU TRI 3C               
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL10LBD.FTL113CL
# ************************************** FICHIERS ISSU DU TRI 3D               
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL10LBG.FTL113DL
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR         
       m_OutputAssign -c 9 -w ITL113 ITL113
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL10LBK
       ;;
(GTL10LBK)
       m_CondExec 04,GE,GTL10LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4A : TRI DU FICHIER FTL111BL POUR CREATION DU FTL114EL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBM
       ;;
(GTL10LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F61.FTL111BL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LBM.FTL114EL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LBN
       ;;
(GTL10LBN)
       m_CondExec 00,EQ,GTL10LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4B : TRI DU FICHIER FTL111BL POUR CREATION DU FTL114FL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBQ
       ;;
(GTL10LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F61.FTL111BL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LBQ.FTL114FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_44_7 44 CH 7
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LBR
       ;;
(GTL10LBR)
       m_CondExec 00,EQ,GTL10LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4C : TRI DU FICHIER FTL111BL POUR CREATION DU FTL114GL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBT
       ;;
(GTL10LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F61.FTL111BL
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GTL10LBT.FTL114GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LBU
       ;;
(GTL10LBU)
       m_CondExec 00,EQ,GTL10LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL114                                                                
#  CREATION DES ETATS ITL114A ET ITL114B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LBX
       ;;
(GTL10LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02L  : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02L /dev/null
# ************************************** TABLE DES ENTETES DE VENTES           
#    RSGV10L  : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10L /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23L  : NAME=RSGV23L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23L /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04L  : NAME=RSTL04L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04L /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00L  : NAME=RSAN00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A               
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL10LBM.FTL114EL
# ************************************** FICHIERS ISSU DU TRI 4B               
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL10LBQ.FTL114FL
# ************************************** FICHIERS ISSU DU TRI 4C               
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL10LBT.FTL114GL
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L             
       m_OutputAssign -c 9 -w ITL114A ITL114A
# ************************************** STAT RETOURS MAGS PAR PIECES          
#  ITL114B  REPORT SYSOUT=(9,ITL114B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_OutputAssign -c "*" ITL114B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL10LBY
       ;;
(GTL10LBY)
       m_CondExec 04,GE,GTL10LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL116                                                                
#  CREATION DE L'ETAT ITL116                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LCA
       ;;
(GTL10LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIVREURS                    
#    RSTL04   : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09   : NAME=RSTL09L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BTL110               
       m_FileAssign -d SHR -g ${G_A28} FTL110 ${DATA}/PXX0/F61.FTL110AL
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE             
       m_OutputAssign -c 9 -w ITL116 ITL116
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL10LCB
       ;;
(GTL10LCB)
       m_CondExec 04,GE,GTL10LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL117                                                                
#  CREATION DU FICHIER FTL117AL                                                
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LCD
       ;;
(GTL10LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV         
#    RSGA13L  : NAME=RSGA13L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13L /dev/null
# ************************************** TABLE DES FAMILLES                    
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES           
#    RSGA30L  : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30L /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40L  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40L /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00L  : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT                       
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DNPCDEP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10LCD
# ************************************** FIC D'EXTRACT FTL117AL                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL117 ${DATA}/PTEM/GTL10LCD.FTL117DL
# *****   FICHIER A DESTINATION DE COPERNIC                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FTL117B ${DATA}/PTEM/GTL10LCD.FTL117EL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL117 
       JUMP_LABEL=GTL10LCE
       ;;
(GTL10LCE)
       m_CondExec 04,GE,GTL10LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 6A : TRI DU FICHIER FTL117DL POUR CREATION DU FTL118BL                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LCG
       ;;
(GTL10LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/GTL10LCD.FTL117DL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GTL10LCG.FTL118BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_60_4 60 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10LCH
       ;;
(GTL10LCH)
       m_CondExec 00,EQ,GTL10LCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL118                                                                
#  CREATION DE L'ETAT ITL118                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL10LCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LCJ
       ;;
(GTL10LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00L  : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A               
       m_FileAssign -d SHR -g ${G_A30} FTL118 ${DATA}/PTEM/GTL10LCG.FTL118BL
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10LCJ
# ************************************** MODE DE DELIV/MAG ET FAM              
       m_OutputAssign -c 9 -w ITL118 ITL118
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL10LCK
       ;;
(GTL10LCK)
       m_CondExec 04,GE,GTL10LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL10LZA
       ;;
(GTL10LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL10LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
