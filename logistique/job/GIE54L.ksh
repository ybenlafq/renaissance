#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE54L.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGIE54 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/23 AT 11.23.57 BY BURTECA                      
#    STANDARDS: P  JOBSET: GIE54L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   LE PASSAGE DE LA CHAINE DOIT SE FAIRE APRES QUE LE DEPOT EST FINI          
#                    SON INVENTAIRE                                            
# ********************************************************************         
#  BIE540 : CREATION DU FICHIER DES ECARTS D'INVENTAIRE ENTREPOT               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GIE54LA
       ;;
(GIE54LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GIE54LAA
       ;;
(GIE54LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  ETATS                                                                
#    RSGA11   : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  INVENTAIRE                                                           
#    RSIE05   : NAME=RSIE05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  INVENTAIRE                                                           
#    RSIE60   : NAME=RSIE60L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  AVANCEMENT D'INVENTAIRE                                              
#    RSIE10   : NAME=RSIE10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  PRMP LILLE                                                           
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE LOGISTIQUE                                                     
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIE540 ${DATA}/PGI961/F61.BIE540AL
#                                                                              
# ******  PARAMETRE SOCIETE = 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE540 
       JUMP_LABEL=GIE54LAB
       ;;
(GIE54LAB)
       m_CondExec 04,GE,GIE54LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                             
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LAD
       ;;
(GIE54LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI961/F61.BIE540AL
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GIE54LAD.BIE540DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54LAE
       ;;
(GIE54LAE)
       m_CondExec 00,EQ,GIE54LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135 : EDITION DES ECARTS D'INVENTAIRE PAR FAMILLE LIEU S/LIEU            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LAG
       ;;
(GIE54LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS                                                   
       m_FileAssign -d SHR -g ${G_A2} FIV130 ${DATA}/PTEM/GIE54LAD.BIE540DL
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE (IIV135)                       
       m_OutputAssign -c 9 -w IIV135 IIV135
       m_OutputAssign -c Z IIV135B
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=GIE54LAH
       ;;
(GIE54LAH)
       m_CondExec 04,GE,GIE54LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D'EXTRACTION DES ECARTS ENTREPOT                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LAJ
       ;;
(GIE54LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PGI961/F61.BIE540AL
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GIE54LAJ.BIE540BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 0
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_92_5 92 CH 5
 /CONDITION CND_1 FLD_PD_85_3 EQ CST_1_11 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54LAK
       ;;
(GIE54LAK)
       m_CondExec 00,EQ,GIE54LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140   EDITION DES ECARTS D'INVENTAIRE PAR LIEU S/LIEU                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LAM
       ;;
(GIE54LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS TRIE                                              
       m_FileAssign -d SHR -g ${G_A4} FIV140 ${DATA}/PTEM/GIE54LAJ.BIE540BL
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c Z IIV140
       m_OutputAssign -c Z IIV140A
       m_OutputAssign -c Z IIV140M
# ******  ETAT DES ECARTS D INVENTAIRE VALORISE (IIV140)                       
       m_OutputAssign -c 9 -w IIV140L IIV140L
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=GIE54LAN
       ;;
(GIE54LAN)
       m_CondExec 04,GE,GIE54LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS DEPOTS                                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LAQ
       ;;
(GIE54LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PGI961/F61.BIE540AL
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GIE54LAQ.BIE540FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_65_3 65 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE54LAR
       ;;
(GIE54LAR)
       m_CondExec 00,EQ,GIE54LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144  : EDITION DES ECARTS DEPOT PAR RAYON                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE54LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LAT
       ;;
(GIE54LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A6} FIV144 ${DATA}/PTEM/GIE54LAQ.BIE540FL
# ******  EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                        
       m_OutputAssign -c 9 -w IIV144 IIV144
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=GIE54LAU
       ;;
(GIE54LAU)
       m_CondExec 04,GE,GIE54LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GIE54LZA
       ;;
(GIE54LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE54LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
