#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL45D.ksh                       --- VERSION DU 09/10/2016 05:56
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL45 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.12.49 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL45D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#    IMPERATIF :  CETTE CHAINE DOIT ETRE EXECUTEE IMPERATIVEMENT               
#    -=-=-=-=-    CAR LES BONS DE LIVRAISON SONT IMPERATIFS POUR LE            
#                 DEPOT.                                                       
#                                                                              
# ********************************************************************         
# ********************************************************************         
#  B T L 0 4 5                                                                 
# ********************************************************************         
#   CREATION DES BONS DE LIVRAISON                                             
#   REPRISE: OUI SI ABEND                                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL45DA
       ;;
(GTL45DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL45DAA
       ;;
(GTL45DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ TABLES DES BONS DE LIVRAISON  *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES PROFILS ET DATE DE DELIVRANCES                           
# ******  POUR LEQUELS LES B.L. SONT A EDITER                                  
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL991/F91.BTL001BD
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES ARTICLES                                                   
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  TABLE GENERALISEES (LPLGE CARSP)                                     
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  TABLE DES MAYQUES                                                    
#    RSGA22D  : NAME=RSGA22D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22D /dev/null
# ******  TABLE DES VENTES/ADRESSES                                            
#    RSGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02D /dev/null
# ******  TABLE DES VENTES/REGLEMENT                                           
#    RSGV10D  : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ******  TABLE DES LIGNES DE VENTES ARTICLES                                  
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
# ******  TABLE DES LIGNES DE PRESTATION                                       
#    RSGV13D  : NAME=RSGV13D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV13D /dev/null
# ******  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                        
#    RSGV15D  : NAME=RSGV15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15D /dev/null
# ******  TABLE DES VENTES/QUANTITES DISPONIBLES                               
#    RSGV21D  : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21D /dev/null
# ****** LIBELLE DEVISE                                                        
#    RSFM04D  : NAME=RSFM04D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM04D /dev/null
#                                                                              
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FTL045 ${DATA}/PTL991/F91.TL0045AD
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL065 ${DATA}/PTL991/F91.TL0065AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL045 
       JUMP_LABEL=GTL45DAB
       ;;
(GTL45DAB)
       m_CondExec 04,GE,GTL45DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 5 0                                                                 
# ********************************************************************         
#   CHARGEMENT DES BONS DE LIVRAISONS DANS LA D BASE IMPRESSION                
#   REPRISE: NON BACKOUT DU STEP LOGGING IMS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45DAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL45DAD
       ;;
(GTL45DAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BTL050,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL45DR1)                                             
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL45DR1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#                                                                              
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d SHR -g ${G_A1} FTL045 ${DATA}/PTL991/F91.TL0045AD
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DD,MODE=U,REST=(YES,GTL45DR1)                       
# DIGVIP   FILE NAME=DI0000ID,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL45DAD
#                                                                              
# ******  FDATE JJMMSSAA DATE D EDITION DES BL                                 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#                TRI DES LIVRAISONS DU JOUR                                    
#               ****************************                                   
#  (DDELIV/NLIEU/NVENTE)                                                       
#  SORT FIELDS=(1,8,A,9,3,A,12,7,A),FORMAT=CH                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL45DAG
       ;;
(GTL45DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTL991/F91.TL0065AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F91.TL0065BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_7 12 CH 7
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_3 9 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_7 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL45DAH
       ;;
(GTL45DAH)
       m_CondExec 00,EQ,GTL45DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 6 5                                                                 
# ********************************************************************         
#   CREATION DE LA LISTE DES LIVRAISONS PREVUES                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL45DAJ
       ;;
(GTL45DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES LIVRAISONS PREVUES                                       
       m_FileAssign -d SHR -g ${G_A3} FTL065 ${DATA}/PTL0/F91.TL0065BD
# ******  LISTE DES LIVRAISONS PREVUES                                         
       m_OutputAssign -c 9 -w ITL065 ITL065
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL065 
       JUMP_LABEL=GTL45DAK
       ;;
(GTL45DAK)
       m_CondExec 04,GE,GTL45DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
