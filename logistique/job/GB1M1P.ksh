#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GB1M1P.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGB1M1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/05/07 AT 15.36.42 BY PREPA2                       
#    STANDARDS: P  JOBSET: GB1M1P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **************************************                                       
#   PROG    BGB101 : PROG D EXTRACTION                                         
#  REPRISE: NON                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GB1M1PA
       ;;
(GB1M1PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GB1M1PAA
       ;;
(GB1M1PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE ARTICLES                                                              
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE FAMILLES                                                              
#    RTGA14   : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA14 /dev/null
#  TABLE                                                                       
#    RTGA30   : NAME=RSGA30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA30 /dev/null
#  TABLE                                                                       
#    RTGA67   : NAME=RSGA67,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA67 /dev/null
#  TABLE                                                                       
#    RTGB05   : NAME=RSGB05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGB05 /dev/null
#  TABLE                                                                       
#    RTGB15   : NAME=RSGB15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGB15 /dev/null
#  TABLE                                                                       
#    RTGB55   : NAME=RSGB55,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGB55 /dev/null
#  TABLE                                                                       
#    RTGB65   : NAME=RSGB65,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGB65 /dev/null
#                                                                              
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER EN ENTREE : FMOIS                                                   
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GB1M1PAA
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGB101 ${DATA}/PTEM/GB1M1PAA.BGB1M1AP
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGB102 ${DATA}/PTEM/GB1M1PAA.BGB1M2AP
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGB103 ${DATA}/PTEM/GB1M1PAA.BGB1M3AP
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGB104 ${DATA}/PTEM/GB1M1PAA.BGB1M4AP
# ******* FICHIER EN SORTIE : ACTIVITE DE MAGASINAGE                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGB105 ${DATA}/PTEM/GB1M1PAA.BGB1M5AP
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGB201 ${DATA}/PTEM/GB1M1PAA.BGB2M1AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGB101 
       JUMP_LABEL=GB1M1PAB
       ;;
(GB1M1PAB)
       m_CondExec 04,GE,GB1M1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BGB1M1AP)                                           
# ********************************************************************         
# AAF      SORT                                                                
# ********************************* FICHIER FEXTRAC                            
# SORTIN   FILE  NAME=BGB1M1AP,MODE=I                                          
# ********************************* FICHIER FEXTRAC TRIE                       
# SORTOUT  FILE  NAME=BGB1M1BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(7,3,BI,A,10,3,BI,A,13,24,BI,A,37,5,BI,A,42,3,BI,A,             
#               45,3,BI,A,48,3,BI,A,51,7,BI,A,58,3,PD,A,61,5,BI,A,             
#               66,7,BI,A,73,2,BI,A),FORMAT=CH                                 
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BGB1M1CP                                      
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01,LANG=UTIL                                        
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE DU GENERATEUR D'ETATS             
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# ************************************ FICHIER FEXTRAC TRIE                    
# FEXTRAC  FILE  NAME=BGB1M1BP,MODE=I                                          
# ************************************ FICHIER FCUMULS  RECL=512               
# FCUMULS  FILE  NAME=BGB1M1CP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG050) PLAN(BEG050)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AAP      SORT                                                                
# ************************************ FICHIER FCUMULS ISSU DU BEG050          
# SORTIN   FILE  NAME=BGB1M1CP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# SORTOUT  FILE  NAME=BGB1M1DP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,512,A),FORMAT=BI                                             
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGB101                                                 
# ********************************************************************         
# AAU      STEP  PGM=IKJEFT01                                                  
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE GENERALISEE                       
# RTGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=(I,U)                              
# ************************************ TABLE DES SOUS/TABLES                   
# RTGA71   FILE  DYNAM=YES,NAME=RSGA71,MODE=(I,U)                              
# ************************************ TABLE DU GENERATEUR D'ETATS             
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG11   FILE  DYNAM=YES,NAME=RSEG11,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG20   FILE  DYNAM=YES,NAME=RSEG20,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# ************************************ FICHIER FEXTRAC TRIE                    
# FEXTRAC  FILE  NAME=BGB1M1BP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# FCUMULS  FILE  NAME=BGB1M1DP,MODE=I                                          
# ************************************ PARAMETRE DATE                          
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ************************************ PARAMETRE SOCIETE                       
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ************************************ PARAMETRE  MOIS                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP                                        
# ************************************ FICHIER D'IMPRESSION                    
#  FEDITION REPORT SYSOUT=(9,IGB101),RECFM=VA                                  
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG060) PLAN(BEG060)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BGB1M2AP)                                           
# ********************************************************************         
# AAZ      SORT                                                                
# ********************************** FICHIER FEXTRAC                           
# SORTIN   FILE  NAME=BGB1M2AP,MODE=I                                          
# ********************************** FICHIER FEXTRAC TRIE                      
# SORTOUT  FILE  NAME=BGB1M2BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(7,3,BI,A,10,3,BI,A,13,24,BI,A,37,5,BI,A,42,3,BI,A,             
#               45,3,BI,A,48,3,BI,A,51,7,BI,A,58,2,BI,A),FORMAT=CH             
#  SUM FIELDS=(93,4,PD,97,4,PD),FORMAT=BI                                      
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BGB1M2CP                                      
# ********************************************************************         
# ABE      STEP  PGM=IKJEFT01,LANG=UTIL                                        
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE DU GENERATEUR D'ETATS             
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# *********************************** FICHIER FEXTRAC TRIE                     
# FEXTRAC  FILE  NAME=BGB1M2BP,MODE=I                                          
# ************************************ FICHIER FCUMULS  RECL=512               
# FCUMULS  FILE  NAME=BGB1M2CP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG050) PLAN(BEG050)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABJ      SORT                                                                
# ************************************ FICHIER FCUMULS ISSU DU BEG050          
# SORTIN   FILE  NAME=BGB1M2CP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# SORTOUT  FILE  NAME=BGB1M2DP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,512,A),FORMAT=BI                                             
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGB102                                                 
# ********************************************************************         
# ABO      STEP  PGM=IKJEFT01                                                  
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE GENERALISEE                       
# RTGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=(I,U)                              
# ************************************ TABLE DES SOUS/TABLES                   
# RTGA71   FILE  DYNAM=YES,NAME=RSGA71,MODE=(I,U)                              
# ************************************ TABLE DU GENERATEUR D'ETATS             
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG11   FILE  DYNAM=YES,NAME=RSEG11,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG20   FILE  DYNAM=YES,NAME=RSEG20,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# ************************************ FICHIER FEXTRAC TRIE                    
# FEXTRAC  FILE  NAME=BGB1M2BP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# FCUMULS  FILE  NAME=BGB1M2DP,MODE=I                                          
# ************************************ PARAMETRE DATE                          
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ************************************ PARAMETRE SOCIETE                       
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ************************************ PARAMETRE  MOIS                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP                                        
# ************************************ FICHIER D'IMPRESSION                    
# FEDITION REPORT SYSOUT=(9,IGB102),RECFM=VA                                   
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG060) PLAN(BEG060)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BGB1M3AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAD
       ;;
(GB1M1PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GB1M1PAA.BGB1M3AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GB1M1PAD.BGB1M3BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_42_7 42 CH 7
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_3 10 CH 3
 /FIELDS FLD_PD_144_4 144 PD 4
 /FIELDS FLD_PD_136_4 136 PD 4
 /FIELDS FLD_PD_140_4 140 PD 4
 /FIELDS FLD_PD_128_4 128 PD 4
 /FIELDS FLD_PD_124_4 124 PD 4
 /FIELDS FLD_BI_37_5 37 CH 5
 /FIELDS FLD_PD_132_4 132 PD 4
 /FIELDS FLD_BI_13_24 13 CH 24
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_24 ASCENDING,
   FLD_BI_37_5 ASCENDING,
   FLD_BI_42_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_124_4,
    TOTAL FLD_PD_128_4,
    TOTAL FLD_PD_132_4,
    TOTAL FLD_PD_136_4,
    TOTAL FLD_PD_140_4,
    TOTAL FLD_PD_144_4
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GB1M1PAE
       ;;
(GB1M1PAE)
       m_CondExec 00,EQ,GB1M1PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BGB1M3CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAG
       ;;
(GB1M1PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GB1M1PAD.BGB1M3BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GB1M1PAG.BGB1M3CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GB1M1PAH
       ;;
(GB1M1PAH)
       m_CondExec 04,GE,GB1M1PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAJ
       ;;
(GB1M1PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GB1M1PAG.BGB1M3CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GB1M1PAJ.BGB1M3DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GB1M1PAK
       ;;
(GB1M1PAK)
       m_CondExec 00,EQ,GB1M1PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGB103                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAM
       ;;
(GB1M1PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GB1M1PAD.BGB1M3BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GB1M1PAJ.BGB1M3DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGB103 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GB1M1PAN
       ;;
(GB1M1PAN)
       m_CondExec 04,GE,GB1M1PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BGB1M4AP)                                           
# ********************************************************************         
# ACN      SORT                                                                
# ********************************** FICHIER FEXTRAC                           
# SORTIN   FILE  NAME=BGB1M4AP,MODE=I                                          
# ********************************** FICHIER FEXTRAC TRIE                      
# SORTOUT  FILE  NAME=BGB1M4BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(7,3,BI,A,10,3,BI,A,13,24,BI,A,37,5,BI,A,42,3,BI,A,             
#               45,3,BI,A,48,3,BI,A,51,7,BI,A,58,2,BI,A),FORMAT=CH             
#  SUM FIELDS=(093,4,PD,097,6,PD,103,4,PD),FORMAT=BI                           
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
# * PGM : BEG050                                                               
# ********************************************************************         
# * CREATION D'UN FICHIER FCUMULS BGB1M4CP                                     
# ********************************************************************         
# ACS      STEP  PGM=IKJEFT01,LANG=UTIL                                        
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE DU GENERATEUR D'ETATS             
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# *********************************** FICHIER FEXTRAC TRIE                     
# FEXTRAC  FILE  NAME=BGB1M4BP,MODE=I                                          
# *********************************** FICHIER FCUMULS  RECL=512                
# FCUMULS  FILE  NAME=BGB1M4CP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG050) PLAN(BEG050)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# ACX      SORT                                                                
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
# SORTIN   FILE  NAME=BGB1M4CP,MODE=I                                          
# *********************************** FICHIER FCUMULS TRIE                     
# SORTOUT  FILE  NAME=BGB1M4DP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,512,A),FORMAT=BI                                             
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGB104                                                 
# ********************************************************************         
# ADC      STEP  PGM=IKJEFT01                                                  
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *********************************** TABLE GENERALISEE                        
# RTGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=(I,U)                              
# *********************************** TABLE DES SOUS/TABLES                    
# RTGA71   FILE  DYNAM=YES,NAME=RSGA71,MODE=(I,U)                              
# *********************************** TABLE DU GENERATEUR D'ETATS              
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG11   FILE  DYNAM=YES,NAME=RSEG11,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG20   FILE  DYNAM=YES,NAME=RSEG20,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# ************************************ FICHIER FEXTRAC TRIE                    
# FEXTRAC  FILE  NAME=BGB1M4BP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# FCUMULS  FILE  NAME=BGB1M4DP,MODE=I                                          
# ************************************ PARAMETRE DATE                          
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ************************************ PARAMETRE SOCIETE                       
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ************************************ PARAMETRE  MOIS                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP                                        
# ************************************ FICHIER D'IMPRESSION                    
# FEDITION REPORT SYSOUT=(9,IGB104),RECFM=VA                                   
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG060) PLAN(BEG060)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BGB2M1AP)                                           
# ********************************************************************         
# ADH      SORT                                                                
# ********************************** FICHIER FEXTRAC                           
# SORTIN   FILE  NAME=BGB2M1AP,MODE=I                                          
# ********************************** FICHIER FEXTRAC TRIE                      
# SORTOUT  FILE  NAME=BGB2M1BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(7,3,BI,A,10,3,BI,A,13,03,BI,A,16,5,BI,A,21,3,BI,A,             
#               24,8,BI,A,32,7,BI,A,39,3,PD,A,42,5,BI,A,47,7,BI,A,             
#               54,1,BI,A,55,2,BI,A),FORMAT=CH                                 
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BGB2M1CP                                      
# ********************************************************************         
# ADM      STEP  PGM=IKJEFT01,LANG=UTIL                                        
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE DU GENERATEUR D'ETATS             
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# ************************************ FICHIER FEXTRAC TRIE                    
# FEXTRAC  FILE  NAME=BGB2M1BP,MODE=I                                          
# ************************************ FICHIER FCUMULS  RECL=512               
# FCUMULS  FILE  NAME=BGB2M1CP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG050) PLAN(BEG050)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# ADR      SORT                                                                
# ************************************ FICHIER FCUMULS ISSU DU BEG050          
# SORTIN   FILE  NAME=BGB2M1CP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# SORTOUT  FILE  NAME=BGB2M1DP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,512,A),FORMAT=BI                                             
#  RECORD TYPE=F,LENGTH=512                                                    
#          DATAEND                                                             
# ********************************************************************         
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IGB201                                                 
# ********************************************************************         
# ADW      STEP  PGM=IKJEFT01                                                  
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ************************************ TABLE GENERALISEE                       
# RTGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=(I,U)                              
# ************************************ TABLE DES SOUS/TABLES                   
# RTGA71   FILE  DYNAM=YES,NAME=RSGA71,MODE=(I,U)                              
# *********************************** TABLE DU GENERATEUR D'ETATS              
# RTEG00   FILE  DYNAM=YES,NAME=RSEG00,MODE=(I,U)                              
# RTEG05   FILE  DYNAM=YES,NAME=RSEG05,MODE=(I,U)                              
# RTEG10   FILE  DYNAM=YES,NAME=RSEG10,MODE=(I,U)                              
# RTEG11   FILE  DYNAM=YES,NAME=RSEG11,MODE=(I,U)                              
# RTEG15   FILE  DYNAM=YES,NAME=RSEG15,MODE=(I,U)                              
# RTEG20   FILE  DYNAM=YES,NAME=RSEG20,MODE=(I,U)                              
# RTEG25   FILE  DYNAM=YES,NAME=RSEG25,MODE=(I,U)                              
# RTEG30   FILE  DYNAM=YES,NAME=RSEG30,MODE=(I,U)                              
# ************************************ FICHIER FEXTRAC TRIE                    
# FEXTRAC  FILE  NAME=BGB2M1BP,MODE=I                                          
# ************************************ FICHIER FCUMULS TRIE                    
# FCUMULS  FILE  NAME=BGB2M1DP,MODE=I                                          
# ************************************ PARAMETRE DATE                          
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ************************************ PARAMETRE SOCIETE                       
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ************************************ PARAMETRE  MOIS                         
# FMOIS    DATA  CLASS=VAR,PARMS=FMOISP                                        
# ************************************ FICHIER D'IMPRESSION                    
# FEDITION REPORT SYSOUT=(9,IGB201),RECFM=VA                                   
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BEG060) PLAN(BEG060)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BGB1M5) : ACTIVITE MAGASINAGE                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAQ
       ;;
(GB1M1PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GB1M1PAA.BGB1M5AP
# *****   FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GB1M1PAQ.BGB1M5BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_19_3 19 CH 3
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_13_3 13 CH 03
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_64_4 064 PD 4
 /FIELDS FLD_BI_10_3 10 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_3 ASCENDING,
   FLD_BI_19_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_64_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GB1M1PAR
       ;;
(GB1M1PAR)
       m_CondExec 00,EQ,GB1M1PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BGB1M5CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAT
       ;;
(GB1M1PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *****   FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/GB1M1PAQ.BGB1M5BP
# *****   FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GB1M1PAT.BGB1M5CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GB1M1PAU
       ;;
(GB1M1PAU)
       m_CondExec 04,GE,GB1M1PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PAX
       ;;
(GB1M1PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FCUMULS ISSU DU BEG050                                               
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GB1M1PAT.BGB1M5CP
# *****   FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GB1M1PAX.BGB1M5DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GB1M1PAY
       ;;
(GB1M1PAY)
       m_CondExec 00,EQ,GB1M1PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGB105                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GB1M1PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PBA
       ;;
(GB1M1PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *****   TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *****   TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *****   FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GB1M1PAQ.BGB1M5BP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/GB1M1PAX.BGB1M5DP
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w IGB105 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GB1M1PBB
       ;;
(GB1M1PBB)
       m_CondExec 04,GE,GB1M1PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GB1M1PZA
       ;;
(GB1M1PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GB1M1PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
