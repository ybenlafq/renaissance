#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS010P.ksh                       --- VERSION DU 08/10/2016 17:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/02/13 AT 10.25.20 BY BURTECC                      
#    STANDARDS: P  JOBSET: GS010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BGS005 :  ALIMENTATION DU FICHIER DES INFOS POUR L'EDITION                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS010PA
       ;;
(GS010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS010PAA
       ;;
(GS010PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***  TABLE ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# *** TABLE EDITION DES ETATS (CODE MARKETING)                                 
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
# *** TABLE RELATION ETATS FAMILLE/RAYON                                       
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
# *** TABLE RELATION FAMILLE/CODE MARKETING                                    
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
# *** TABLE DES MARQUES                                                        
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
# *** TABLE ASSOCIATIONS CODE VALEURS/MARKETING                                
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
# *** TABLE CODES VALEURS DESCRIPTIFS/RELATION ARTICLES                        
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
# *** TABLE DES EMPLACEMENTS  SOL                                              
#    RSGE25   : NAME=RSGE25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGE25 /dev/null
# *** TABLE DES EMPLACEMENTS  RACK                                             
#    RSGE35   : NAME=RSGE35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGE35 /dev/null
# *** TABLE DES EMPLACEMENTS  RACK/PICKING                                     
#    RSGE36   : NAME=RSGE36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGE36 /dev/null
# *** TABLE DES EMPLACEMENTS  VRAC                                             
#    RSGE45   : NAME=RSGE45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGE45 /dev/null
# *** TABLE DES SOUS LIEUX DES ENTREPOTS                                       
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# *** TABLE DES LIEUX EXTERNES SPECIFIQUES                                     
#    RSGS35   : NAME=RSGS35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS35 /dev/null
# ******                                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
#  FICHIER DES PARAMETRES TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 179 -g +1 FGS005 ${DATA}/PTEM/GS010PAA.BGS005AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS005 
       JUMP_LABEL=GS010PAB
       ;;
(GS010PAB)
       m_CondExec 04,GE,GS010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT   :  TRI DU FICHIER DES INFOS SERVANT A L'EDITION                      
#         :  OMIT SOC ET LIEUX A BLANC BUG TRANSACTION VENTE  J.M              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP GS010PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS010PAD
       ;;
(GS010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS010PAA.BGS005AP
       m_FileAssign -d NEW,CATLG,DELETE -r 179 -g +1 SORTOUT ${DATA}/PTEM/GS010PAD.BGS010AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "      "
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_67_7 67 CH 7
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_42_5 42 CH 5
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_32_5 32 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_47_20 47 CH 20
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_12 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_15_2 ASCENDING,
   FLD_CH_22_5 ASCENDING,
   FLD_CH_32_5 ASCENDING,
   FLD_CH_42_5 ASCENDING,
   FLD_CH_47_20 ASCENDING,
   FLD_CH_67_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 179 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS010PAE
       ;;
(GS010PAE)
       m_CondExec 00,EQ,GS010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS010 :  EDITION DES STOCKS ENTREPOTS                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS010PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS010PAG
       ;;
(GS010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***  TABLE DES LIEUX                                                         
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# *** TABLE DES FAMILLES                                                       
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# *** TABLE DES AGREGATIONS                                                    
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
#                                                                              
#  FICHIER DE TRI                                                              
       m_FileAssign -d SHR -g ${G_A2} FGS010 ${DATA}/PTEM/GS010PAD.BGS010AP
#                                                                              
#  DATE JJMMSSAA                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  EDITION DES STOCKS                                                          
       m_OutputAssign -c 9 -w IGS010 IGS010
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS010 
       JUMP_LABEL=GS010PAH
       ;;
(GS010PAH)
       m_CondExec 04,GE,GS010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS015 :  ECART DE STOCK ENTRE WMS ET NCG                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS010PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS010PAJ
       ;;
(GS010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****  FICHIER PARAM�TRE                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****  FICHIER EN ENTR�E                                                     
       m_FileAssign -d SHR -g ${G_A3} FGS010 ${DATA}/PTEM/GS010PAD.BGS010AP
#                                                                              
# *****  ETAT : EDITION DES STOCKS                                             
       m_OutputAssign -c 9 -w IGS015 IGS015
#                                                                              
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS015 
       JUMP_LABEL=GS010PAK
       ;;
(GS010PAK)
       m_CondExec 04,GE,GS010PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS010PZA
       ;;
(GS010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
