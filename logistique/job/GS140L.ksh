#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS140L.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGS140 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/07/11 AT 14.56.06 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GS140L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BSM140 : EXTRACTION A PARTIR DES TABLES DB2 POUR CREATION DU FICHIE         
#           FSM140 (VENTES ET STOCKS DE DNPC)                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS140LA
       ;;
(GS140LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS140LAA
       ;;
(GS140LAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# * FAYCAL==> AJOUT DE SA202L POUR QUE LE JOB TOURNE APRES LES SAUVES          
# * CAR QUAND PLANTAGE DE LA SAUVE SA55TP OU SA55MP CE JOB DEMARRE             
# * ET ON SE PLANTE EN -904                                                    
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 961                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : VTK)                                
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  ZONE DE PRIX                                                         
#    RSGA59L  : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59L /dev/null
# ******  COMMISSIONS PAR ZONE DE PRIX                                         
#    RSGA62L  : NAME=RSGA62L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62L /dev/null
# ******  COMMISSIONS BBTE (PRIMES VENDEURS)                                   
#    RSGA75L  : NAME=RSGA75L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75L /dev/null
# ******  RESERVATION SUR CMDE FOURNISSEUR                                     
#    RSGF55L  : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55L /dev/null
# ******  STOCK SOUS-LIEU/ENTREPOT                                             
#    RSGS10L  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10L /dev/null
# ******  STOCK SOUS-LIEU/MAGASIN                                              
#    RSGS30L  : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30L /dev/null
# ******  VENTES MAGASINS                                                      
#    RSHV04L  : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04L /dev/null
# ******  FICHIER VENTES ET STOCKS DNPC                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 FSM140 ${DATA}/PTEM/GS140LAA.BSM140AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM140 
       JUMP_LABEL=GS140LAB
       ;;
(GS140LAB)
       m_CondExec 04,GE,GS140LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR PGM BSM141                                            
#       TRIE PAR FAMILLE , CODIC ET GROUPE D'EDTION                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS140LAD
       ;;
(GS140LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS DNPC                                        
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS140LAA.BSM140AL
# ******  FICHIER VENTES ET STOCKS DNPC  ( TRIE )                              
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 SORTOUT ${DATA}/PTEM/GS140LAD.BSM140BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_234_7 234 CH 7
 /FIELDS FLD_CH_241_5 241 CH 5
 /KEYS
   FLD_CH_241_5 ASCENDING,
   FLD_CH_234_7 ASCENDING,
   FLD_CH_4_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140LAE
       ;;
(GS140LAE)
       m_CondExec 00,EQ,GS140LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM141 :     PRE-FORMATAGE DU FICHIER D'EDITION                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS140LAG
       ;;
(GS140LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS DNPC  ( TRIE )                              
       m_FileAssign -d SHR -g ${G_A2} FSM140 ${DATA}/PTEM/GS140LAD.BSM140BL
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00L  : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00L /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : NCGFC)                              
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING IMBRICATION)             
#    RSGA09L  : NAME=RSGA09L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09L /dev/null
# ******  TABLE FAMILLE (EDITIONS DES ETATS)                                   
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING)                         
#    RSGA12L  : NAME=RSGA12L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12L /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ******  TABLE ASSOCIATION CODES VALEURS MARKETING/CODES VALEUR DECRI         
#    RSGA25L  : NAME=RSGA25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25L /dev/null
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26L  : NAME=RSGA26L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26L /dev/null
# ******  RESERVATIONS SUR COMMANDE FOURNISSEURS                               
#    RSGA53L  : NAME=RSGA53L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53L /dev/null
# ******  FORMATAGE DU FICHIER D'EDITION STOCK ET VENTES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 FSM141 ${DATA}/PTEM/GS140LAG.BSM141AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM141 
       JUMP_LABEL=GS140LAH
       ;;
(GS140LAH)
       m_CondExec 04,GE,GS140LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI AVANT EDITION                                                          
#        CRITERES GROUPE / WSEQED/(CMARKETING/CVMARKETING)                     
#                 TOTVTE DECROISSANT/CODIC                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS140LAJ
       ;;
(GS140LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE                            
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS140LAG.BSM141AL
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 SORTOUT ${DATA}/PTEM/GS140LAJ.BSM141BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_1_3 1 PD 3
 /FIELDS FLD_CH_338_10 338 CH 10
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_CH_353_10 353 CH 10
 /KEYS
   FLD_CH_7_2 ASCENDING,
   FLD_PD_1_3 ASCENDING,
   FLD_CH_338_10 ASCENDING,
   FLD_CH_353_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140LAK
       ;;
(GS140LAK)
       m_CondExec 00,EQ,GS140LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM142 : GENERATION DE L'EDITION                                            
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS140LAM
       ;;
(GS140LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26L  : NAME=RSGA26L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26L /dev/null
# ******  FICHIER D'EDITION STOCK ET VENTES TRIE                               
       m_FileAssign -d SHR -g ${G_A4} FSM141 ${DATA}/PTEM/GS140LAJ.BSM141BL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 961                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ****** ETAT STOCK ET VENTES POUR LE SIEGE(DV ET ACHATS                       
       m_OutputAssign -c 9 -w ISM140 ISM140
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM142 
       JUMP_LABEL=GS140LAN
       ;;
(GS140LAN)
       m_CondExec 04,GE,GS140LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS140LZA
       ;;
(GS140LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS140LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
