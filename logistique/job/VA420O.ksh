#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA420O.ksh                       --- VERSION DU 08/10/2016 22:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVA420 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/13 AT 11.25.18 BY BURTECN                      
#    STANDARDS: P  JOBSET: VA420O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  REPROS DES FICHIERS BVA511AO ET BVA520AO                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA420OA
       ;;
(VA420OA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2003/03/13 AT 11.25.17 BY BURTECN                
# *    JOBSET INFORMATION:    NAME...: VA420O                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'EXTRAC COMPTA GCT'                     
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA420OAA
       ;;
(VA420OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******  FIC MVTS DU MOIS POUR LES LIEUX 'AVO' ET RCY'                        
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PNCGO/F16.BVA511AO
# ******  FIC DES ACHATS TRIES PAR FOURNISSEUR ET RAYON                        
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PNCGO/F16.BVA520AO
# ******  FIC REPRO POUR FT420O                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 OUT1 ${DATA}/PNCGO/F16.BVA511ZO
# ******  FIC REPRO POUR FT420O                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 OUT2 ${DATA}/PNCGO/F16.BVA520ZO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA420OAA.sysin
       m_UtilityExec
# **************************                                                   
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VA420OAB
       ;;
(VA420OAB)
       m_CondExec 16,NE,VA420OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
