#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL75Y.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGTL75 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/01/27 AT 11.00.35 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL75Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EDITION NON IMPERATIVE PEUT ETRE REPRISE DANS LA JOURNEE                    
# ********************************************************************         
#  TRI DU FICHIER FTL076 (FICHIER DES VENTES DE LA VEILLE)                     
#  SUR : DATE LIVRAISON,CODE MAG,NUM DE VENTE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL75YA
       ;;
(GTL75YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL75YAA
       ;;
(GTL75YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DES NUMEROS DE FOLIOS        *                                        
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PTL945/F45.BTL076AY
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PTEM/GTL75YAA.BTL075AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_14_8 14 CH 8
 /KEYS
   FLD_CH_14_8 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 40 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75YAB
       ;;
(GTL75YAB)
       m_CondExec 00,EQ,GTL75YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL075                                                                
# ********************************************************************         
#   GENERATION DU FICHIER DES VENTES EN ANOMALIE DE LIVRAISON PAR MAGA         
#   POUR UNE DATE DE LIVRAISON                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL75YAD
       ;;
(GTL75YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
# FDATE    FILE  NAME=FDATE,MODE=I                                             
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02Y /dev/null
# ******  TABLE DES ENTETES LIGNE DE VENTE                                     
#    RTGV10Y  : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10Y /dev/null
# ******  TABLE DES LIGNES DE VENTE                                            
#    RTGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11Y /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01Y  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01Y /dev/null
# ******  TABLE DES DETAILS TOURNEES DE LIVRAISON                              
#    RTTL02Y  : NAME=RSTL02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02Y /dev/null
# ******  FICHIER DES TOURNEES PREVISIONNELLES                                 
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL945/F45.BTL001AY
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A1} FTL075 ${DATA}/PTEM/GTL75YAA.BTL075AY
# ******  FICHIER VENTE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FTL076 ${DATA}/PTL945/F45.BTL076AY
# ******  FICHIER VENTE EN ANOMALIE DE LIVRAISON PAR MAGASIN                   
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FTL077 ${DATA}/PTEM/GTL75YAD.BTL077AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL075 
       JUMP_LABEL=GTL75YAE
       ;;
(GTL75YAE)
       m_CondExec 04,GE,GTL75YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTL077 (FICHIER DES VENTES EN ANOMALIE)                      
#  SUR : CODE PROFIL,CODE MAG,CODE TOURNEE,NUM DE VENTE                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL75YAG
       ;;
(GTL75YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GTL75YAD.BTL077AY
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/GTL75YAG.BTL078AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_43_6 43 CH 6
 /FIELDS FLD_CH_34_1 34 CH 1
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_43_6 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_34_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75YAH
       ;;
(GTL75YAH)
       m_CondExec 00,EQ,GTL75YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL080                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#   EDITION DES VENTES EN ANOMALIE DE LIVRAISON                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL75YAJ
       ;;
(GTL75YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
# FDATE    FILE  NAME=FDATE,MODE=I                                             
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02Y /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01Y  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01Y /dev/null
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A3} FTL080 ${DATA}/PTEM/GTL75YAG.BTL078AY
# ******  FICHIER EDITION                                                      
       m_OutputAssign -c 9 -w ITL080 ITL080
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL080 
       JUMP_LABEL=GTL75YAK
       ;;
(GTL75YAK)
       m_CondExec 04,GE,GTL75YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL75YZA
       ;;
(GTL75YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL75YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
