#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA60EO.ksh                       --- VERSION DU 14/10/2016 10:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVA60E -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/12 AT 16.17.15 BY BURTECA                      
#    STANDARDS: P  JOBSET: VA60EO                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BIE600                                                                
# ********************************************************************         
#  CREATION DES RAYONS TLMBL,TLMBR,ELABL,ELABR SUR RTGA20 ET RTGA21            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA60EOA
       ;;
(VA60EOA)
#
#VA60EOAJ
#VA60EOAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#VA60EOAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2014/03/12 AT 16.17.15 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: VA60EO                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'INIT RTVA06'                           
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA60EOAA
       ;;
(VA60EOAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01O /dev/null
#    RTIE05O  : NAME=RSIE05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTIE05O /dev/null
#    RTIE60O  : NAME=RSIE60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTIE60O /dev/null
#    RTIN00O  : NAME=RSIN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTIN00O /dev/null
# *****   TABLES EN M.A.J                                                      
#    RTGG51O  : NAME=RSGG51O,MODE=(U,N) - DYNAM=YES                            
# -X-RSGG51O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTGG51O /dev/null
# *****  .FICHIER.FNSOC.XXX.                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGO
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 FIE600 ${DATA}/PTEM/VA60EOAA.FIE600EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE610 
       JUMP_LABEL=VA60EOAB
       ;;
(VA60EOAB)
       m_CondExec 04,GE,VA60EOAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER ISSU DU STEP PRECEDENT                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA60EOAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA60EOAD
       ;;
(VA60EOAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/VA60EOAA.FIE600EO
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.RELOAD.VA06EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_32_6 32 PD 6
 /FIELDS FLD_PD_46_8 46 PD 8
 /FIELDS FLD_PD_14_6 14 PD 6
 /FIELDS FLD_PD_38_8 38 PD 8
 /FIELDS FLD_PD_26_6 26 PD 6
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_PD_62_8 62 PD 8
 /FIELDS FLD_PD_54_8 54 PD 8
 /FIELDS FLD_PD_20_6 20 PD 6
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_14_6,
    TOTAL FLD_PD_20_6,
    TOTAL FLD_PD_26_6,
    TOTAL FLD_PD_32_6,
    TOTAL FLD_PD_38_8,
    TOTAL FLD_PD_46_8,
    TOTAL FLD_PD_54_8,
    TOTAL FLD_PD_62_8
 /* Record Type = F  Record Length = 79 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA60EOAE
       ;;
(VA60EOAE)
       m_CondExec 00,EQ,VA60EOAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : LOAD                                                                  
# ********************************************************************         
#  LOAD DE LA TABLE RTVA06                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA60EOAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA60EOAG
       ;;
(VA60EOAG)
       m_CondExec ${EXAAK},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RTVA06O  : NAME=RSVA06O,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA06O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTVA06O /dev/null
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F16.RELOAD.VA06EO
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA60EOAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA60EO_VA60EOAG_RTVA06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA60EOAH
       ;;
(VA60EOAH)
       m_CondExec 04,GE,VA60EOAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : REPAIR                                                                
# ********************************************************************         
#  REPAIL NOCOPY TABLESPACE RSVA06L DE LA D.B PODVA00                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA60EOAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA60EOZA
       ;;
(VA60EOZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA60EOZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
