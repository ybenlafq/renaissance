#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LG870P.ksh                       --- VERSION DU 08/10/2016 17:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLG870 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/10/09 AT 16.11.42 BY BURTECA                      
#    STANDARDS: P  JOBSET: LG870P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# (5.1) TRI DU FICHIER FLG863                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LG870PA
       ;;
(LG870PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LG870PAA
       ;;
(LG870PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG863P
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PAA.FLG863CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_7 10 CH 07
 /KEYS
   FLD_BI_10_7 ASCENDING
 /* Record Type = F  Record Length = 305 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PAB
       ;;
(LG870PAB)
       m_CondExec 00,EQ,LG870PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (5.2) TRI DU FICHIER -LG865                                                 
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAD
       ;;
(LG870PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PAD.FLG865CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_14_3 14 CH 03
 /FIELDS FLD_BI_7_7 07 CH 07
 /KEYS
   FLD_BI_7_7 ASCENDING,
   FLD_BI_14_3 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PAE
       ;;
(LG870PAE)
       m_CondExec 00,EQ,LG870PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (5.3) TRI DU FICHIER FLG866                                                 
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAG
       ;;
(LG870PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG866P
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PAG.FLG866CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_3 27 CH 03
 /FIELDS FLD_BI_24_3 24 CH 03
 /FIELDS FLD_BI_1_7 01 CH 07
 /KEYS
   FLD_BI_1_7 ASCENDING,
   FLD_BI_24_3 ASCENDING,
   FLD_BI_27_3 ASCENDING
 /* Record Type = F  Record Length = 140 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PAH
       ;;
(LG870PAH)
       m_CondExec 00,EQ,LG870PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (5.4) TRI DU FICHIER FLG868                                                 
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAJ
       ;;
(LG870PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG868P
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PAJ.FLG868CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_7 08 CH 07
 /KEYS
   FLD_BI_8_7 ASCENDING
 /* Record Type = F  Record Length = 20 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PAK
       ;;
(LG870PAK)
       m_CondExec 00,EQ,LG870PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (5.5) TRI DU FICHIER FLG865                                                 
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAM
       ;;
(LG870PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PAM.FLG865SP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 01 CH 03
 /FIELDS FLD_CH_4_3 04 CH 03
 /FIELDS FLD_CH_1_3 1 CH 03
 /FIELDS FLD_CH_25_8 25 CH 08
 /FIELDS FLD_BI_14_3 14 CH 03
 /FIELDS FLD_CH_14_3 14 CH 03
 /FIELDS FLD_BI_4_3 04 CH 03
 /KEYS
   FLD_BI_14_3 ASCENDING,
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 17 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_4_3,FLD_CH_14_3,FLD_CH_25_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LG870PAN
       ;;
(LG870PAN)
       m_CondExec 00,EQ,LG870PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (5.6) EXTRACTION FGP869                                                     
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAQ
       ;;
(LG870PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******                                                                       
       m_FileAssign -d SHR -g ${G_A1} FLG863 ${DATA}/PTEM/LG870PAA.FLG863CP
       m_FileAssign -d SHR -g ${G_A2} FLG865 ${DATA}/PTEM/LG870PAD.FLG865CP
       m_FileAssign -d SHR -g ${G_A3} FLG866 ${DATA}/PTEM/LG870PAG.FLG866CP
       m_FileAssign -d SHR -g ${G_A4} FLG868 ${DATA}/PTEM/LG870PAJ.FLG868CP
       m_FileAssign -d SHR -g ${G_A5} FLG865S ${DATA}/PTEM/LG870PAM.FLG865SP
# ******                                                                       
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGF56   : NAME=RSGF56,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF56 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
#    RSFL05P  : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05P /dev/null
#    RSFL06P  : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06P /dev/null
#                                                                              
# ******* FICHIER EN SORTIE DU PGM BGP869                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 676 -t LSEQ -g +1 FGP869 ${DATA}/PTEM/LG870PAQ.FGP869P
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP869 
       JUMP_LABEL=LG870PAR
       ;;
(LG870PAR)
       m_CondExec 04,GE,LG870PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (6.1) TRI  *   TRI DU SEQUENTIEL FGP869                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAT
       ;;
(LG870PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP869                                                           
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/LG870PAQ.FGP869P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 676 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PAT.FGP869CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_170_7 170 CH 07
 /FIELDS FLD_BI_184_1 184 CH 01
 /FIELDS FLD_BI_191_6 191 CH 06
 /FIELDS FLD_BI_177_7 177 CH 07
 /KEYS
   FLD_BI_177_7 ASCENDING,
   FLD_BI_184_1 ASCENDING,
   FLD_BI_170_7 ASCENDING,
   FLD_BI_191_6 ASCENDING
 /* Record Type = F  Record Length = 676 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PAU
       ;;
(LG870PAU)
       m_CondExec 00,EQ,LG870PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (6.2) BGP879 : EXTRACTION FGP869                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LG870PAX
       ;;
(LG870PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A7} FGP869 ${DATA}/PTEM/LG870PAT.FGP869CP
# ******  FICHIER  EN SORTIE (LRECL 676)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 676 -t LSEQ -g +1 FGP879 ${DATA}/PTEM/LG870PAX.FGP879P
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP879 
       JUMP_LABEL=LG870PAY
       ;;
(LG870PAY)
       m_CondExec 04,GE,LG870PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (7.1) TRI1 *   TRI DU SEQUENTIEL FGP879                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBA
       ;;
(LG870PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP879                                                           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/LG870PAX.FGP879P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 676 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PBA.FGP879CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_46_5 046 CH 05
 /FIELDS FLD_BI_71_2 071 CH 02
 /FIELDS FLD_BI_186_5 186 CH 05
 /FIELDS FLD_BI_177_7 177 CH 07
 /KEYS
   FLD_BI_46_5 ASCENDING,
   FLD_BI_71_2 ASCENDING,
   FLD_BI_186_5 ASCENDING,
   FLD_BI_177_7 ASCENDING
 /* Record Type = F  Record Length = 676 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PBB
       ;;
(LG870PBB)
       m_CondExec 00,EQ,LG870PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (7.2) TRI1 *   TRI DU SEQUENTIEL FLG863                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBD
       ;;
(LG870PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FLG863                                                           
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG863P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PBD.FLG863DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_5 020 CH 05
 /FIELDS FLD_CH_133_5 133 CH 05
 /FIELDS FLD_BI_20_5 020 CH 05
 /FIELDS FLD_BI_106_2 106 CH 02
 /FIELDS FLD_CH_106_2 106 CH 02
 /FIELDS FLD_BI_133_5 133 CH 05
 /FIELDS FLD_CH_1_3 01 CH 03
 /FIELDS FLD_BI_1_3 001 CH 03
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_133_5 ASCENDING,
   FLD_BI_20_5 ASCENDING,
   FLD_BI_106_2 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 676 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_133_5,FLD_CH_20_5,FLD_CH_106_2
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LG870PBE
       ;;
(LG870PBE)
       m_CondExec 00,EQ,LG870PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (7.3) BGP889 : CREATION   FGP889                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBG
       ;;
(LG870PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A9} FLG863P ${DATA}/PTEM/LG870PBD.FLG863DP
       m_FileAssign -d SHR -g ${G_A10} FGP879 ${DATA}/PTEM/LG870PBA.FGP879CP
# ******  FICHIER  EN SORTIE (LRECL 512)                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FGP889 ${DATA}/PTEM/LG870PBG.FGP889P
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP889 
       JUMP_LABEL=LG870PBH
       ;;
(LG870PBH)
       m_CondExec 04,GE,LG870PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (9.3) TRI1 *   TRI DU SEQUENTIEL FGP889                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBJ
       ;;
(LG870PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP889                                                           
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/LG870PBG.FGP889P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PBJ.FGP889EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_229_3 229 CH 3
 /FIELDS FLD_CH_249_7 249 CH 7
 /FIELDS FLD_CH_226_3 226 CH 3
 /KEYS
   FLD_CH_249_7 ASCENDING,
   FLD_CH_226_3 ASCENDING,
   FLD_CH_229_3 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PBK
       ;;
(LG870PBK)
       m_CondExec 00,EQ,LG870PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGP890                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBM
       ;;
(LG870PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLES EN LECTURE                                                           
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGF80   : NAME=RSGF80,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF80 /dev/null
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A12} FGP889 ${DATA}/PTEM/LG870PBJ.FGP889EP
# ******  FICHIER  EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FGP890 ${DATA}/PXX0/F07.FGP889FP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP890 
       JUMP_LABEL=LG870PBN
       ;;
(LG870PBN)
       m_CondExec 04,GE,LG870PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (8.1) TRI1 *   TRI DU SEQUENTIEL FGP889                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBQ
       ;;
(LG870PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP889                                                           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.FGP889FP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PBQ.FGP889CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_14_3 014 CH 03
 /FIELDS FLD_BI_1_6 01 CH 06
 /FIELDS FLD_BI_7_7 07 CH 07
 /FIELDS FLD_BI_37_5 037 CH 05
 /KEYS
   FLD_BI_7_7 ASCENDING,
   FLD_BI_1_6 ASCENDING,
   FLD_BI_14_3 ASCENDING,
   FLD_BI_37_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PBR
       ;;
(LG870PBR)
       m_CondExec 00,EQ,LG870PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (8.2) BGP870 : CREATION   IGP870                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBT
       ;;
(LG870PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A14} FGP889 ${DATA}/PTEM/LG870PBQ.FGP889CP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
# FICHIER D IMPRESSION                                                         
# IGP870   FILE  NAME=IGP870P,MODE=O                                           
       m_OutputAssign -c 9 -w IGP870 IGP870
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP870 
       JUMP_LABEL=LG870PBU
       ;;
(LG870PBU)
       m_CondExec 04,GE,LG870PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (9.1) TRI2 *   TRI DU SEQUENTIEL FGP889                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=LG870PBX
       ;;
(LG870PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGP889                                                           
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F07.FGP889FP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PBX.FGP889DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_37_5 037 CH 05
 /FIELDS FLD_BI_1_6 01 CH 06
 /FIELDS FLD_BI_7_7 07 CH 07
 /FIELDS FLD_BI_14_3 014 CH 03
 /KEYS
   FLD_BI_7_7 ASCENDING,
   FLD_BI_1_6 ASCENDING,
   FLD_BI_14_3 ASCENDING,
   FLD_BI_37_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PBY
       ;;
(LG870PBY)
       m_CondExec 00,EQ,LG870PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (9.2) BGP865 : CREATION   IGP865                                            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCA
       ;;
(LG870PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A16} FGP889 ${DATA}/PTEM/LG870PBX.FGP889DP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
# FICHIER D IMPRESSION                                                         
# IGP865   FILE  NAME=IGP865P,MODE=O                                           
       m_OutputAssign -c 9 -w IGP865 IGP865
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP865 
       JUMP_LABEL=LG870PCB
       ;;
(LG870PCB)
       m_CondExec 04,GE,LG870PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# (10.1) TRI DU FICHIER FLG863                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCD
       ;;
(LG870PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG863P
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PCD.FLG863KP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "907"
 /FIELDS FLD_CH_4_3 04 CH 03
 /FIELDS FLD_CH_10_7 10 CH 07
 /FIELDS FLD_CH_7_3 07 CH 03
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_6 
 /KEYS
   FLD_CH_10_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 305 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PCE
       ;;
(LG870PCE)
       m_CondExec 00,EQ,LG870PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (10.2) TRI DU FICHIER -LG865                                                
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCG
       ;;
(LG870PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PCG.FLG865LP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "907"
 /FIELDS FLD_CH_20_3 20 CH 03
 /FIELDS FLD_CH_17_3 17 CH 03
 /FIELDS FLD_CH_7_7 07 CH 07
 /CONDITION CND_1 FLD_CH_17_3 EQ CST_1_6 
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_17_3 ASCENDING,
   FLD_CH_20_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PCH
       ;;
(LG870PCH)
       m_CondExec 00,EQ,LG870PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (10.3) TRI DU FICHIER FLG866                                                
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCJ
       ;;
(LG870PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG866P
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PCJ.FLG866EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "907"
 /FIELDS FLD_CH_8_6 08 CH 06
 /FIELDS FLD_CH_18_6 18 CH 06
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_14_3 14 CH 03
 /FIELDS FLD_CH_1_7 01 CH 07
 /CONDITION CND_1 FLD_CH_18_3 EQ CST_1_7 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_18_6 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_8_6 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 140 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PCK
       ;;
(LG870PCK)
       m_CondExec 00,EQ,LG870PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGP805                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCM
       ;;
(LG870PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES EN LECTURE                                                           
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGF49   : NAME=RSGF49,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF49 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGF56   : NAME=RSGF56,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF56 /dev/null
#    RSGD10   : NAME=RSGD10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD10 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A17} FLG863 ${DATA}/PTEM/LG870PCD.FLG863KP
       m_FileAssign -d SHR -g ${G_A18} FLG865 ${DATA}/PTEM/LG870PCG.FLG865LP
       m_FileAssign -d SHR -g ${G_A19} FLG866 ${DATA}/PTEM/LG870PCJ.FLG866EP
# ******  FICHIER  EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FGP805 ${DATA}/PTEM/LG870PCM.FGP805AP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP805 
       JUMP_LABEL=LG870PCN
       ;;
(LG870PCN)
       m_CondExec 04,GE,LG870PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (10.4) TRI DU FICHIER FGP805                                                
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCQ
       ;;
(LG870PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/LG870PCM.FGP805AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PCQ.FGP805BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_5 16 CH 05
 /FIELDS FLD_PD_107_3 107 PD 03
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_1_3 01 CH 03
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_PD_107_3 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PCR
       ;;
(LG870PCR)
       m_CondExec 00,EQ,LG870PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGP806                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PCT PGM=BGP806     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCT
       ;;
(LG870PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER FGP805 TRIE                                                  
       m_FileAssign -d SHR -g ${G_A21} FGP805 ${DATA}/PTEM/LG870PCQ.FGP805BP
# ******  FICHIER D'EDITION                                                    
       m_OutputAssign -c 9 -w IGP805 IGP805
       m_ProgramExec BGP806 
# *******************************************************************          
#  SORT  TRI DU FICHIER FLG866                                                 
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=LG870PCX
       ;;
(LG870PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG866P
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PCX.FLG866BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "907"
 /DERIVEDFIELD CST_3_10 "907"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_14_3 14 CH 03
 /FIELDS FLD_CH_18_6 18 CH 06
 /FIELDS FLD_CH_1_7 01 CH 07
 /CONDITION CND_1 FLD_CH_18_3 EQ CST_1_6 AND FLD_CH_8_3 EQ CST_3_10 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_18_6 ASCENDING,
   FLD_CH_14_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 140 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PCY
       ;;
(LG870PCY)
       m_CondExec 00,EQ,LG870PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGP810                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=LG870PDA
       ;;
(LG870PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES EN LECTURE                                                           
#    RSGF01   : NAME=RSGF01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF01 /dev/null
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGF49   : NAME=RSGF49,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF49 /dev/null
#    RSGF80   : NAME=RSGF80,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF80 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGD10   : NAME=RSGD10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD10 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A22} FLG863 ${DATA}/PTEM/LG870PCD.FLG863KP
       m_FileAssign -d SHR -g ${G_A23} FLG865 ${DATA}/PTEM/LG870PCG.FLG865LP
       m_FileAssign -d SHR -g ${G_A24} FLG866 ${DATA}/PTEM/LG870PCX.FLG866BP
# ******  FICHIER  EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FGP810 ${DATA}/PTEM/LG870PDA.FGP805CP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP810 
       JUMP_LABEL=LG870PDB
       ;;
(LG870PDB)
       m_CondExec 04,GE,LG870PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT  TRI DU FICHIER FGP810                                                 
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG870PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=LG870PDD
       ;;
(LG870PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/LG870PDA.FGP805CP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG870PDD.FGP805DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_135_1 135 CH 1
 /FIELDS FLD_CH_4_6 4 CH 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_34_5 34 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_135_1 ASCENDING,
   FLD_CH_4_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 200 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG870PDE
       ;;
(LG870PDE)
       m_CondExec 00,EQ,LG870PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGP811                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG870PDG PGM=BGP811     ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=LG870PDG
       ;;
(LG870PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER FGP810 TRIE                                                  
       m_FileAssign -d SHR -g ${G_A26} FGP810 ${DATA}/PTEM/LG870PDD.FGP805DP
# ******  FICHIER D'EDITION                                                    
       m_OutputAssign -c 9 -w IGP810 IGP810
       m_ProgramExec BGP811 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LG870PZA
       ;;
(LG870PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG870PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
