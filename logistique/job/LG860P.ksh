#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LG860P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLG860 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/11 AT 08.55.52 BY BURTECA                      
#    STANDARDS: P  JOBSET: LG860P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# (1.1) UNLOAD DES DONNEES DES TABLES RTGA00,RTGA12,RTGA14,RTGA22,RTGA         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LG860PA
       ;;
(LG860PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LG860PAA
       ;;
(LG860PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# **************************************                                       
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
#    RTGA83   : NAME=RSGA83,MODE=I - DYNAM=YES                                 
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/LG860PAA.UNGA83P
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/LG860PAA.UNFL40P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG860PAA.sysin
       
       m_DBHpuUnload -f SYSIN
# *******************************************************************          
#  (1.2) TRI DE L'UNLOAD  (SYSREC01)                                           
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG860PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAD
       ;;
(LG860PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/LG860PAA.UNGA83P
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PAD.UNGA83AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 01 CH 07
 /KEYS
   FLD_BI_1_7 ASCENDING
 /* Record Type = F  Record Length = 23 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PAE
       ;;
(LG860PAE)
       m_CondExec 00,EQ,LG860PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.3) TRI DE L'UNLOAD  (SYSREC02)                                           
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG860PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAG
       ;;
(LG860PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/LG860PAA.UNFL40P
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PAG.UNFL40AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_130 03 CH 130
 /FIELDS FLD_BI_6_7 06 CH 07
 /KEYS
   FLD_BI_6_7 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_130
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LG860PAH
       ;;
(LG860PAH)
       m_CondExec 00,EQ,LG860PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.4) EXTRACTION FLG863                                                     
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAJ
       ;;
(LG860PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******                                                                       
       m_FileAssign -d SHR -g ${G_A3} FGA83 ${DATA}/PTEM/LG860PAD.UNGA83AP
       m_FileAssign -d SHR -g ${G_A4} FFL40 ${DATA}/PTEM/LG860PAG.UNFL40AP
# ******                                                                       
#    RSGA01P  : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01P /dev/null
#    RSGA09P  : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09P /dev/null
#    RSGA10P  : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10P /dev/null
#    RSGA11P  : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11P /dev/null
#    RSGA29P  : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29P /dev/null
#    RSGA30P  : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30P /dev/null
#    RSGA58P  : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58P /dev/null
#    RSGA67P  : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67P /dev/null
#    RSGA69P  : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69P /dev/null
#                                                                              
#    RSFL05P  : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05P /dev/null
#    RSFL06P  : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06P /dev/null
#                                                                              
#    RSGQ05P  : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05P /dev/null
# ******* FICHIER EN SORTIE DU PGM BLG863                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -t LSEQ -g +1 FLG863 ${DATA}/PXX0/F07.FLG863P
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE = 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE EDITION= 'IGP765'                                          
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/LG860PAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG863 
       JUMP_LABEL=LG860PAK
       ;;
(LG860PAK)
       m_CondExec 04,GE,LG860PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.1BIS) TRI DU FICHIER FLG865P DE LA VEILLE(MODIF DU 22/01/03)             
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP LG860PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAM
       ;;
(LG860PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PAM.FLG865JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_7 07 CH 07
 /KEYS
   FLD_BI_7_7 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PAN
       ;;
(LG860PAN)
       m_CondExec 00,EQ,LG860PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (1.2BIS) BGP894 : EXTRACTION POUR FICHIER FGP894(MODIF 22/01/03)            
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAQ
       ;;
(LG860PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A5} FLG865 ${DATA}/PTEM/LG860PAM.FLG865JP
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******** TABLES EN LECTURE *************                                     
# *****   TABLES                                                               
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  FICHIER  EN SORTIE (LRECL 050)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FGP894 ${DATA}/PXX0/F07.FGP894AP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGP894 
       JUMP_LABEL=LG860PAR
       ;;
(LG860PAR)
       m_CondExec 04,GE,LG860PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.1) UNLOAD DES DONNEES DES TABLES RTHV04,RTGS36,RTGA54,RTGA69             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PAT PGM=PTLDRIVM   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAT
       ;;
(LG860PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/LG860PAT.HV04UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/LG860PAT.GS36U1AP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/LG860PAT.GS36U2AP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SYSREC04 ${DATA}/PTEM/LG860PAT.GA54UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 32 -t LSEQ -g +1 SYSREC05 ${DATA}/PTEM/LG860PAT.GA69UNLP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG860PAT.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  (2.1BIS) TRI1 *   TRI OUTREC DU FICHIER TEMPORAIRE GS36U2AP                 
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LG860PAX
       ;;
(LG860PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC GS36U2AP                                                         
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/LG860PAT.GS36U2AP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PAX.GS36U2BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_7 024 CH 07
 /FIELDS FLD_CH_16_6 16 CH 06
 /FIELDS FLD_CH_1_13 01 CH 13
 /FIELDS FLD_BI_14_1 014 CH 01
 /FIELDS FLD_CH_33_15 033 CH 15
 /KEYS
   FLD_BI_14_1 ASCENDING
 /* Record Type = F  Record Length = 41 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_13,FLD_CH_16_6,FLD_CH_24_7,FLD_CH_33_15
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LG860PAY
       ;;
(LG860PAY)
       m_CondExec 00,EQ,LG860PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.1TER) MERGE DES FICHIERS GS36U1AP ET GS36U2BP POUR CREATION              
#           FICHIER GS36UNLP                                                   
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBA
       ;;
(LG860PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC GS36U1AP                                                         
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/LG860PAT.GS36U1AP
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/LG860PAX.GS36U2BP
# ******* FIC EN MERGE GS36UNLP                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PBA.GS36UNLP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 41 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PBB
       ;;
(LG860PBB)
       m_CondExec 00,EQ,LG860PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.2)  BLG864 : EXTRACTION TABLES  POUR FORMATTER UN SEQUENTIEL             
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBD
       ;;
(LG860PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A9} FGA54 ${DATA}/PTEM/LG860PAT.GA54UNLP
       m_FileAssign -d SHR -g ${G_A10} FGS36 ${DATA}/PTEM/LG860PBA.GS36UNLP
       m_FileAssign -d SHR -g ${G_A11} FHV04 ${DATA}/PTEM/LG860PAT.HV04UNLP
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
# *****   TABLE                                                                
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# *****   TABLE                                                                
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# *****   TABLE                                                                
#    RSGB20   : NAME=RSGB20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB20 /dev/null
# *****   TABLE                                                                
#    RSGS43   : NAME=RSGS43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43 /dev/null
# *****   TABLE                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# *****   TABLE                                                                
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# *****   TABLE                                                                
#    RSRM90   : NAME=RSRM90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM90 /dev/null
# *****   TABLE                                                                
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ******  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER  EN SORTIE (LRECL 015)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 FLG864 ${DATA}/PXX0/F07.FLG864P
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG864 
       JUMP_LABEL=LG860PBE
       ;;
(LG860PBE)
       m_CondExec 04,GE,LG860PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.3) TRI1 *   TRI DU SEQUENTIEL FGA69                                      
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBG
       ;;
(LG860PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGA69                                                            
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/LG860PAT.GA69UNLP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 32 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PBG.GA69UNAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_11_2 11 CH 2
 /FIELDS FLD_BI_4_7 4 CH 7
 /KEYS
   FLD_BI_4_7 ASCENDING,
   FLD_BI_1_3 ASCENDING,
   FLD_BI_11_2 ASCENDING
 /* Record Type = F  Record Length = 32 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PBH
       ;;
(LG860PBH)
       m_CondExec 00,EQ,LG860PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.4) TRI2 *   TRI DU SEQUENTIEL FLG863                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBJ
       ;;
(LG860PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FLG863                                                           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.FLG863P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PBJ.FLG863AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_7 10 CH 7
 /FIELDS FLD_BI_1_3 1 CH 3
 /KEYS
   FLD_BI_10_7 ASCENDING,
   FLD_BI_1_3 ASCENDING
 /* Record Type = F  Record Length = 305 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PBK
       ;;
(LG860PBK)
       m_CondExec 00,EQ,LG860PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.5) TRI3 *   TRI DU SEQUENTIEL FLG864                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBM
       ;;
(LG860PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FLG864                                                           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F07.FLG864P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PBM.FLG864AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_9_2 9 PD 2
 /FIELDS FLD_PD_5_4 5 PD 4
 /FIELDS FLD_PD_11_5 11 PD 5
 /FIELDS FLD_PD_3_2 3 PD 2
 /FIELDS FLD_PD_1_2 1 PD 2
 /KEYS
   FLD_PD_5_4 ASCENDING,
   FLD_PD_1_2 ASCENDING,
   FLD_PD_3_2 ASCENDING,
   FLD_PD_9_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_11_5
 /* Record Type = F  Record Length = 15 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PBN
       ;;
(LG860PBN)
       m_CondExec 00,EQ,LG860PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (2.6) BLG865 : EXTRACTION TABLES  POUR FORMATTER UN SEQUENTIEL              
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBQ
       ;;
(LG860PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A15} FLG863 ${DATA}/PTEM/LG860PBJ.FLG863AP
       m_FileAssign -d SHR -g ${G_A16} FLG864 ${DATA}/PTEM/LG860PBM.FLG864AP
       m_FileAssign -d SHR -g ${G_A17} FGA69 ${DATA}/PTEM/LG860PBG.GA69UNAP
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
# *****   TABLE                                                                
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# *****   TABLE                                                                
#    RSGS25   : NAME=RSGS25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS25 /dev/null
# ******  FICHIER  EN SORTIE (LRECL 015)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 FLG865 ${DATA}/PXX0/F07.FLG865P
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG865 
       JUMP_LABEL=LG860PBR
       ;;
(LG860PBR)
       m_CondExec 04,GE,LG860PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.1) UNLOAD DES DONNEES DES TABLES RTFL50,RTFL65,RTGS10                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBT PGM=PTLDRIVM   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBT
       ;;
(LG860PBT)
       m_CondExec ${EXACS},NE,YES 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/LG860PBT.FL50UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/LG860PBT.FL65UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/LG860PBT.GS10UNLP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG860PBT.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  (3.2) TRI1 *   TRI DU SEQUENTIEL FFL50                                      
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=LG860PBX
       ;;
(LG860PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FFL50                                                            
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/LG860PBT.FL50UNLP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PBX.FL50UNAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_13 1 CH 13
 /KEYS
   FLD_BI_1_13 ASCENDING
 /* Record Type = F  Record Length = 68 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PBY
       ;;
(LG860PBY)
       m_CondExec 00,EQ,LG860PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.3) TRI2 *   TRI DU SEQUENTIEL FFL65                                      
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=LG860PCA
       ;;
(LG860PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FFL65                                                            
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/LG860PBT.FL65UNLP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PCA.FL65UNAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_14_8 14 CH 8
 /FIELDS FLD_BI_1_13 1 CH 13
 /KEYS
   FLD_BI_1_13 ASCENDING,
   FLD_BI_14_8 DESCENDING
 /* Record Type = F  Record Length = 21 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PCB
       ;;
(LG860PCB)
       m_CondExec 00,EQ,LG860PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.4) TRI3 *   TRI DU SEQUENTIEL FGS10                                      
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=LG860PCD
       ;;
(LG860PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGS10                                                            
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/LG860PBT.GS10UNLP
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PCD.GS10UNBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_13 1 CH 13
 /KEYS
   FLD_BI_1_13 ASCENDING
 /* Record Type = F  Record Length = 19 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PCE
       ;;
(LG860PCE)
       m_CondExec 00,EQ,LG860PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.5) TRI4 *   TRI DU SEQUENTIEL FLG863                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=LG860PCG
       ;;
(LG860PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FGS10                                                            
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PXX0/F07.FLG863P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LG860PCG.FLG863GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_7 10 CH 07
 /KEYS
   FLD_BI_10_7 ASCENDING
 /* Record Type = F  Record Length = 305 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PCH
       ;;
(LG860PCH)
       m_CondExec 00,EQ,LG860PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (3.6) BLG866 : EXTRACTION TABLES  POUR FORMATTER UN SEQUENTIEL              
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=LG860PCJ
       ;;
(LG860PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A22} FLG863 ${DATA}/PTEM/LG860PCG.FLG863GP
       m_FileAssign -d SHR -g ${G_A23} FFL50 ${DATA}/PTEM/LG860PBX.FL50UNAP
       m_FileAssign -d SHR -g ${G_A24} FFL65 ${DATA}/PTEM/LG860PCA.FL65UNAP
       m_FileAssign -d SHR -g ${G_A25} FGS10 ${DATA}/PTEM/LG860PCD.GS10UNBP
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
# *****   TABLE                                                                
#    RSGF56   : NAME=RSGF56,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF56 /dev/null
# *****   TABLE                                                                
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
# *****   TABLE                                                                
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# *****   TABLE                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER  EN SORTIE (LRECL 140)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -t LSEQ -g +1 FLG866 ${DATA}/PXX0/F07.FLG866P
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG866 
       JUMP_LABEL=LG860PCK
       ;;
(LG860PCK)
       m_CondExec 04,GE,LG860PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (4.1) TRI5 *   TRI DU SEQUENTIEL FLG865                                     
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=LG860PCM
       ;;
(LG860PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC FLG865                                                           
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PXX0/F07.FLG865P
# ******* FIC TRIE                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GP840PAD.FLG865AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_7 07 CH 07
 /KEYS
   FLD_BI_7_7 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG860PCN
       ;;
(LG860PCN)
       m_CondExec 00,EQ,LG860PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  (4.2) BLG868 : EXTRACTION TABLES  POUR FORMATTER UN SEQUENTIEL              
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG860PCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=LG860PCQ
       ;;
(LG860PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A27} FLG865 ${DATA}/PTEM/GP840PAD.FLG865AP
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  FICHIER  EN SORTIE (LRECL 015)                                       
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FLG868 ${DATA}/PXX0/F07.FLG868P
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG868 
       JUMP_LABEL=LG860PCR
       ;;
(LG860PCR)
       m_CondExec 04,GE,LG860PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LG860PZA
       ;;
(LG860PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG860PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
