#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL10M.ksh                       --- VERSION DU 19/10/2016 11:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGTL10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/11/25 AT 11.16.55 BY PREPA2                       
#    STANDARDS: P  JOBSET: GTL10M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSTL09M                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL10MA
       ;;
(GTL10MA)
#
#GTL10MAA
#GTL10MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GTL10MAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL10MAD
       ;;
(GTL10MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ******** TABLE DES QUOTAS DE LIVRAISON
#    RSGQ01M  : NAME=RSGQ01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGQ01M /dev/null
# ******** TABLE DES MVTS DE STOCK
#    RSGS40M  : NAME=RSGS40,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGS40M /dev/null
# ******** TABLE DES VENTES/ADRESSE
#    RSGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV02M /dev/null
# ******** TABLE DES VENTES
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV11M /dev/null
# ******** TABLE DES VENTES/MODE DE DDELIV
#    RSGV23M  : NAME=RSGV23M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV23M /dev/null
# ******** TABLE DES TOUNEES (GENERALITES)
#    RSTL01M  : NAME=RSTL01,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL01M /dev/null
# ******** TABLE DES TOURNEES (DETAILS)
#    RSTL02M  : NAME=RSTL02M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL02M /dev/null
# ******** TABLE DES TOURNEES (RETOURS)
#    RSTL04M  : NAME=RSTL04M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL04M /dev/null
# 
# ******** TABLE DES TOURNESS (HISTOS)
#    RSTL09M  : NAME=RSTL09M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSTL09M /dev/null
# ************************************** TABLE HISTO VENTES
#    RSHV15M  : NAME=RSHV15M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSHV15M /dev/null
# 
# ******** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ******** PARAMETRE SOCIETE
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DALDEP
# ******** PARAMETRE  H:HEBDO  M:MENSUEL
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10MAD
# ******** FIC D'EXTRACT FTL110
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F89.FTL110AM
# ******** FIC D'EXTRACT FTL110
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL110N ${DATA}/PXX0/F89.FTL110AM
# ******** FIC D'EXTRACT FTL111
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111 ${DATA}/PXX0/F89.BTL111AM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL10MAE
       ;;
(GTL10MAE)
       m_CondExec 04,GE,GTL10MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  MERGE : CUMUL DU FICHIER BTL111AM CREE PAR LE PROG BTL110 AVEC
#          L'ANCIEN BTL111AR DU DERNIER TRAITEMENT
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MAG PGM=SORT       ** ID=AAK
# ***********************************
       JUMP_LABEL=GTL10MAG
       ;;
(GTL10MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F89.BTL111AM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F89.BTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A3} SORTOUT ${DATA}/PXX0/F89.BTL111AM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_20_5 20 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MAH
       ;;
(GTL10MAH)
       m_CondExec 00,EQ,GTL10MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  BTL111 : INITIALISATION DES FICHIERS FTL111A  ET FTL111B
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MAJ PGM=IKJEFT01   ** ID=AAP
# ***********************************
       JUMP_LABEL=GTL10MAJ
       ;;
(GTL10MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLE GENERALISE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ******** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ******** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ******** FIC D'EXTRACT ISSU DU BLT110
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ******** FIC D'EXTRACT ISSU DU TRI 1A
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F89.BTL111AM
# ******** FICHIER FTL111A
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111A ${DATA}/PXX0/F89.FTL111AM
# ******** FICHIER FTL111B
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111B ${DATA}/PXX0/F89.FTL111BM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL10MAK
       ;;
(GTL10MAK)
       m_CondExec 04,GE,GTL10MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 2A : TRI DU FICHIER FTL111AM POUR CREATION DU FLT112AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MAM PGM=SORT       ** ID=AAU
# ***********************************
       JUMP_LABEL=GTL10MAM
       ;;
(GTL10MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MAM.FTL112AM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MAN
       ;;
(GTL10MAN)
       m_CondExec 00,EQ,GTL10MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 2B : TRI DU FICHIER FTL111AM POUR CREATION DU FTL112BM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MAQ PGM=SORT       ** ID=AAZ
# ***********************************
       JUMP_LABEL=GTL10MAQ
       ;;
(GTL10MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MAQ.FTL112BM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MAR
       ;;
(GTL10MAR)
       m_CondExec 00,EQ,GTL10MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 2C : TRI DU FICHIER FTL111BM POUR CREATION FU FTL112CM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MAT PGM=SORT       ** ID=ABE
# ***********************************
       JUMP_LABEL=GTL10MAT
       ;;
(GTL10MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MAT.FTL112CM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MAU
       ;;
(GTL10MAU)
       m_CondExec 00,EQ,GTL10MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL112
#  CREATION DES ETATS ITL112A ET ITL112B
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MAX PGM=IKJEFT01   ** ID=ABJ
# ***********************************
       JUMP_LABEL=GTL10MAX
       ;;
(GTL10MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU BLT110
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** FICHIERS ISSU DU TRI 2A
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL10MAM.FTL112AM
# ************************************** FICHIERS ISSU DU TRI 2B
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL10MAQ.FTL112BM
# ************************************** FICHIERS ISSU DU TRI 2C
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL10MAT.FTL112CM
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L
       m_OutputAssign -c 9 -w ITL112A ITL112A
# ************************************** ETAT DE STAT PAR PIECES
       m_OutputAssign -c 9 -w ITL112B ITL112B
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL10MAY
       ;;
(GTL10MAY)
       m_CondExec 04,GE,GTL10MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3A : TRI DU FICHIER FTL111AM POUR CREATION DU FTL113AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBA PGM=SORT       ** ID=ABO
# ***********************************
       JUMP_LABEL=GTL10MBA
       ;;
(GTL10MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBA.FTL113AM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBB
       ;;
(GTL10MBB)
       m_CondExec 00,EQ,GTL10MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3B : TRI DU FICHIER FTL111AM POUR CREATION DU FTL113BM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBD PGM=SORT       ** ID=ABT
# ***********************************
       JUMP_LABEL=GTL10MBD
       ;;
(GTL10MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F89.FTL111AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBD.FTL113BM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBE
       ;;
(GTL10MBE)
       m_CondExec 00,EQ,GTL10MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3C : TRI DES FICHIERS FTL111AM,FTL111BM POUR CREATION DU FTL113
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBG PGM=SORT       ** ID=ABY
# ***********************************
       JUMP_LABEL=GTL10MBG
       ;;
(GTL10MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBG.FTL113CM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBH
       ;;
(GTL10MBH)
       m_CondExec 00,EQ,GTL10MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3D : TRI DES FICHIERS FTL111AM,FTL111BM POUR CREATION FU FTL113
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBJ PGM=SORT       ** ID=ACD
# ***********************************
       JUMP_LABEL=GTL10MBJ
       ;;
(GTL10MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBJ.FTL113DM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBK
       ;;
(GTL10MBK)
       m_CondExec 00,EQ,GTL10MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL113
#  CREATION DE L'ETAT ITL113
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBM PGM=IKJEFT01   ** ID=ACI
# ***********************************
       JUMP_LABEL=GTL10MBM
       ;;
(GTL10MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU BLT110
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** FICHIERS ISSU DU TRI 3A
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL10MBA.FTL113AM
# ************************************** FICHIERS ISSU DU TRI 3B
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL10MBD.FTL113BM
# ************************************** FICHIERS ISSU DU TRI 3C
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL10MBG.FTL113CM
# ************************************** FICHIERS ISSU DU TRI 3D
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL10MBJ.FTL113DM
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR
       m_OutputAssign -c 9 -w ITL113 ITL113
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL10MBN
       ;;
(GTL10MBN)
       m_CondExec 04,GE,GTL10MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 4A : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114EM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBQ PGM=SORT       ** ID=ACN
# ***********************************
       JUMP_LABEL=GTL10MBQ
       ;;
(GTL10MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBQ.FTL114EM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBR
       ;;
(GTL10MBR)
       m_CondExec 00,EQ,GTL10MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 4B : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114FM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBT PGM=SORT       ** ID=ACS
# ***********************************
       JUMP_LABEL=GTL10MBT
       ;;
(GTL10MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBT.FTL114FM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_CH_41_3 41 CH 3
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBU
       ;;
(GTL10MBU)
       m_CondExec 00,EQ,GTL10MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 4C : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114GM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MBX PGM=SORT       ** ID=ACX
# ***********************************
       JUMP_LABEL=GTL10MBX
       ;;
(GTL10MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F89.FTL111BM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MBX.FTL114GM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MBY
       ;;
(GTL10MBY)
       m_CondExec 00,EQ,GTL10MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL114
#  CREATION DES ETATS ITL114A ET ITL114B
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCA PGM=IKJEFT01   ** ID=ADC
# ***********************************
       JUMP_LABEL=GTL10MCA
       ;;
(GTL10MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES VENTES/ADRESSE
#    RSGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV02M /dev/null
# ************************************** TABLE DES ENTETES DE VENTES
#    RSGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV10M /dev/null
# ************************************** TABLE DES VENTES
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV11M /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL
#    RSGV23M  : NAME=RSGV23M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV23M /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)
#    RSTL04M  : NAME=RSTL04M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL10MBQ.FTL114EM
# ************************************** FICHIERS ISSU DU TRI 4B
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL10MBT.FTL114FM
# ************************************** FICHIERS ISSU DU TRI 4C
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL10MBX.FTL114GM
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L
       m_OutputAssign -c 9 -w ITL114A ITL114A
# ************************************** STAT RETOURS MAGS PAR PIECES
#  ITL114B  REPORT SYSOUT=(9,ITL114B),RECFM=FBA,LRECL=133,BLKSIZE=1330
       m_OutputAssign -c "*" ITL114B
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL10MCB
       ;;
(GTL10MCB)
       m_CondExec 04,GE,GTL10MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL116
#  CREATION DE L'ETAT ITL116
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCD PGM=IKJEFT01   ** ID=ADH
# ***********************************
       JUMP_LABEL=GTL10MCD
       ;;
(GTL10MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIVREURS
#    RSTL04   : NAME=RSTL03,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)
#    RSTL09   : NAME=RSTL09M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00   : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BTL110
       m_FileAssign -d SHR -g ${G_A28} FTL110 ${DATA}/PXX0/F89.FTL110AM
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE
       m_OutputAssign -c 9 -w ITL116 ITL116
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL10MCE
       ;;
(GTL10MCE)
       m_CondExec 04,GE,GTL10MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL117
#  CREATION DU FICHIER FTL117AY
# ********************************************************************
#  REPRISE: OUI SI ABEND
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCG PGM=IKJEFT01   ** ID=ADM
# ***********************************
       JUMP_LABEL=GTL10MCG
       ;;
(GTL10MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV
#    RSGA13M  : NAME=RSGA13M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA13M /dev/null
# ************************************** TABLE DES FAMILLES
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA14M /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES
#    RSGA30M  : NAME=RSGA30M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA30M /dev/null
# ************************************** TABLE DES MVTS DE STOCK
#    RSGS40M  : NAME=RSGS40,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGS40M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** PARAMETRE FMOIS
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DALDEP
# ******  DATE JJMMSSAA
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10MCG
# ************************************** FIC D'EXTRACT FTL117AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117 ${DATA}/PTEM/GTL10MCG.FTL117AM
# *****   FICHIER A DESTINATION DE COPERNIC
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117B ${DATA}/PTEM/GTL10MCG.FTL117BM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       #Untranslated IKJEFT01 with unknown command
# DATA UNTRANSLATED: 
# SYSTSIN  DATA  *,CLASS=FIX2                                              
#  DSN SYSTEM(RDAR)                                                        
#  RUN PROGRAM(BTL117) PLAN(BTL117M)                                       
#  END                                                                     

       JUMP_LABEL=GTL10MCH
       ;;
(GTL10MCH)
       m_CondExec 04,GE,GTL10MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 6A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL118AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCJ PGM=SORT       ** ID=ADR
# ***********************************
       JUMP_LABEL=GTL10MCJ
       ;;
(GTL10MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MCJ.FTL118AM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MCK
       ;;
(GTL10MCK)
       m_CondExec 00,EQ,GTL10MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL118
#  CREATION DE L'ETAT ITL118
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCM PGM=IKJEFT01   ** ID=ADW
# ***********************************
       JUMP_LABEL=GTL10MCM
       ;;
(GTL10MCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A
       m_FileAssign -d SHR -g ${G_A30} FTL118 ${DATA}/PTEM/GTL10MCJ.FTL118AM
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  MENSUEL OU HEBDO
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10MCM
# ************************************** MODE DE DELIV/MAG ET FAM
       m_OutputAssign -c 9 -w ITL118 ITL118
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL10MCN
       ;;
(GTL10MCN)
       m_CondExec 04,GE,GTL10MCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 7A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL119AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCQ PGM=SORT       ** ID=AEB
# ***********************************
       JUMP_LABEL=GTL10MCQ
       ;;
(GTL10MCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GTL10MCG.FTL117AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10MCQ.FTL119AM
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10MCR
       ;;
(GTL10MCR)
       m_CondExec 00,EQ,GTL10MCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL119
#  CREATION DE L'ETAT ITL119
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10MCT PGM=IKJEFT01   ** ID=AEG
# ***********************************
       JUMP_LABEL=GTL10MCT
       ;;
(GTL10MCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 7A
       m_FileAssign -d SHR -g ${G_A32} FTL119 ${DATA}/PTEM/GTL10MCQ.FTL119AM
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ************************************** SYNTHESE DES MODES DE DELIV/F
       m_OutputAssign -c 9 -w ITL119 ITL119
# ************************************** SYNTHESE DES MODES DE DELIV/F
#  DEPENDANCE POUR PLAN
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL119 
       JUMP_LABEL=GTL10MCU
       ;;
(GTL10MCU)
       m_CondExec 04,GE,GTL10MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=GTL10MZA
       ;;
(GTL10MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL10MZA.sysin
       m_UtilityExec
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
