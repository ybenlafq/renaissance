#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEF10Y.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGEF10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/02/06 AT 18.22.03 BY PREPA2                       
#    STANDARDS: P  JOBSET: GEF10Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BEF000                                                                
#  ------------                                                                
#  LISTE COMPLETE DE L'ENCOURS FOURNISSEUR                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEF10YA
       ;;
(GEF10YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       GEF10YA=${LASTJO}
       RUN=${RUN}
       JUMP_LABEL=GEF10YAA
       ;;
(GEF10YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSHE00   : NAME=RSHE00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ------  FICHIER D'EDITION                                                    
       m_OutputAssign -c 9 -w IEF000 IEF000
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 209 -g +1 FEFCSV ${DATA}/PXX0/F45.BEF000AY.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF000 
       JUMP_LABEL=GEF10YAB
       ;;
(GEF10YAB)
       m_CondExec 04,GE,GEF10YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF005                                                                
#  ------------                                                                
#  CREATION DES FICHIERS D'EXTRACTION POUR EDITION                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAD
       ;;
(GEF10YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE00   : NAME=RSHE00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FEF010 ${DATA}/PTEM/GEF10YAD.FEF010AY
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 194 -g +1 FDFCSV ${DATA}/PXX0/F45.BEF005AY.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF005 
       JUMP_LABEL=GEF10YAE
       ;;
(GEF10YAE)
       m_CondExec 04,GE,GEF10YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEF010AY ENTRANT DANS LE BEF010                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAG
       ;;
(GEF10YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GEF10YAD.FEF010AY
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GEF10YAG.FEF010BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_72_8 72 CH 8
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_80_5 80 CH 5
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_PD_98_5 98 PD 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_80_5 ASCENDING,
   FLD_CH_72_8 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10YAH
       ;;
(GEF10YAH)
       m_CondExec 00,EQ,GEF10YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF010                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF010 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PV TTC                                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAJ PGM=BEF010     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAJ
       ;;
(GEF10YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A2} FEF010 ${DATA}/PTEM/GEF10YAG.FEF010BY
# ------  EDITION DE L'ETAT IEF010                                             
       m_OutputAssign -c 9 -w IEF010 IEF010
       m_ProgramExec BEF010 
# ********************************************************************         
#  TRI DU FICHIER FEF010AY ENTRANT DANS LES PGMS BEF011 ET BEF012              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAM
       ;;
(GEF10YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GEF10YAD.FEF010AY
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GEF10YAM.FEF010CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_72_6 72 CH 6
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_CH_51_5 51 CH 5
 /FIELDS FLD_CH_78_2 78 CH 2
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_PD_93_5 93 PD 5
 /KEYS
   FLD_CH_51_5 ASCENDING,
   FLD_CH_72_6 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_78_2 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10YAN
       ;;
(GEF10YAN)
       m_CondExec 00,EQ,GEF10YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF011                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF011 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAQ PGM=BEF011     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAQ
       ;;
(GEF10YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A4} FEF010 ${DATA}/PTEM/GEF10YAM.FEF010CY
# ------  EDITION DE L'ETAT IEF010                                             
       m_OutputAssign -c 9 -w IEF011 IEF011
       m_ProgramExec BEF011 
# ********************************************************************         
#  PGM : BEF012                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF012 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAT PGM=BEF012     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAT
       ;;
(GEF10YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BEF005                                          
       m_FileAssign -d SHR -g ${G_A5} FEF010 ${DATA}/PTEM/GEF10YAM.FEF010CY
# ------  EDITION DE L'ETAT IEF010                                             
       m_OutputAssign -c 9 -w IEF012 IEF012
       m_ProgramExec BEF012 
# ********************************************************************         
#  PGM : BEF013                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF013 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PRMP                                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10YAX PGM=BEF013     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YAX
       ;;
(GEF10YAX)
       m_CondExec ${EXABJ},NE,YES 1,EQ,$[GEF10YA] 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A6} FEF010 ${DATA}/PTEM/GEF10YAG.FEF010BY
# ------  EDITION DE L'ETAT IEF013                                             
       m_OutputAssign -c 9 -w IEF013 IEF013
       m_ProgramExec BEF013 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GEF10YZA
       ;;
(GEF10YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
