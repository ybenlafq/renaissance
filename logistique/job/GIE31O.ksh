#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE31O.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGIE31 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 06/03/27 AT 12.06.06 BY PREPA2                       
#    STANDARDS: P  JOBSET: GIE31O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   LE PASSAGE DE LA CHAINE DOIT SE FAIRE APRES LA REMONTEE DU STOCK           
#   INVENTORIE                                                                 
# ********************************************************************         
#  BIE310 : EXTRACTION SUR LA TABLE RTIE60                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE31OA
       ;;
(GIE31OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2006/03/27 AT 12.06.06 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: GIE31O                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'ETAT INVENTAIRE'                       
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE31OAA
       ;;
(GIE31OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  REMONTEE STOCK H.S DEPOT                                                    
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLE                                                              
#    RSGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  GENERALISEE                                                          
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  FAMILLES                                                             
#    RSGA14O  : NAME=RSGA14O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  STOCKS INVT H.S                                                      
#    RSIE60O  : NAME=RSIE60O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSIE60O /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FIE310 ${DATA}/PTEM/GIE31OAA.BIE310AO
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE310 
       JUMP_LABEL=GIE31OAB
       ;;
(GIE31OAB)
       m_CondExec 04,GE,GIE31OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC D 'EXTRACTION                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE31OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE31OAD
       ;;
(GIE31OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GIE31OAA.BIE310AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/GIE31OAD.BIE310BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_24 1 CH 24
 /KEYS
   FLD_CH_1_24 ASCENDING
 /* Record Type = F  Record Length = 080 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE31OAE
       ;;
(GIE31OAE)
       m_CondExec 00,EQ,GIE31OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BIE311                                                                
# ********************************************************************         
#  EDITION DES ECARTS POUR DEPOT                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE31OAG PGM=BIE311     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE31OAG
       ;;
(GIE31OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  FICHIER D EXTRACTION TRIE                                                   
       m_FileAssign -d SHR -g ${G_A2} FIE310 ${DATA}/PTEM/GIE31OAD.BIE310BO
#  EDITION DES ECART SUR RTIE60                                                
       m_OutputAssign -c 9 -w IIE311 IIE311
       m_ProgramExec BIE311 
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GIE31OZA
       ;;
(GIE31OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE31OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
