#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA01EY.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYVA01E -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/08 AT 14.43.38 BY BURTEC6                      
#    STANDARDS: P  JOBSET: VA01EY                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FVA01GE SERVANT A LOADER LA TABLE RTVA01                     
#     ( TRI SUR L'INDEX CLUSTER)                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VA01EYA
       ;;
(VA01EYA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VA01EYAA
       ;;
(VA01EYAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
#  APRES TRONC COMMUN STAT                                                     
#  APRES TRONC COMMUN STAT                                                     
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FVA01GE
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -t LSEQ -g +1 SORTOUT ${DATA}/PVA945/F45.RELOAD.VA01RGE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_35_1 35 CH 1
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_35_1 ASCENDING
 /* Record Type = F  Record Length = 106 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYAB
       ;;
(VA01EYAB)
       m_CondExec 00,EQ,VA01EYAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTVA01                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAD
       ;;
(VA01EYAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RTVA01   : NAME=RSVA01Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA01Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTVA01 /dev/null
       m_FileAssign -d SHR -g ${G_A1} SYSREC ${DATA}/PVA945/F45.RELOAD.VA01RGE
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA01EYAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA01EY_VA01EYAD_RTVA01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA01EYAE
       ;;
(VA01EYAE)
       m_CondExec 04,GE,VA01EYAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVA16AE SERVANT A LOADER LA TABLE RTVA16                     
#     ( TRI SUR L'INDEX CLUSTER)                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAG
       ;;
(VA01EYAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FVA16GE
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.RELOAD.VA16RGE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /* Record Type = F  Record Length = 28 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYAH
       ;;
(VA01EYAH)
       m_CondExec 00,EQ,VA01EYAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTVA16                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAJ
       ;;
(VA01EYAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RTVA16   : NAME=RSVA16Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA16Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTVA16 /dev/null
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F45.RELOAD.VA16RGE
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA01EYAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA01EY_VA01EYAJ_RTVA16.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA01EYAK
       ;;
(VA01EYAK)
       m_CondExec 04,GE,VA01EYAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVA31AE SERVANT A LOADER LA TABLE RTVA31                     
#     ( TRI SUR L'INDEX CLUSTER)                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAM
       ;;
(VA01EYAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FVA31GE
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA01EYAM.VA31RGE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /* Record Type = F  Record Length = 19 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYAN
       ;;
(VA01EYAN)
       m_CondExec 00,EQ,VA01EYAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTVA31                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAQ
       ;;
(VA01EYAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RTVA31   : NAME=RSVA31Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA31Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RTVA31 /dev/null
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PTEM/VA01EYAM.VA31RGE
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA01EYAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA01EY_VA01EYAQ_RTVA31.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA01EYAR
       ;;
(VA01EYAR)
       m_CondExec 04,GE,VA01EYAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVA500AE POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAT
       ;;
(VA01EYAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FVA500GE
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA01EYAT.VA500BEY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_9_7 9 CH 7
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_BI_24_2 24 CH 2
 /FIELDS FLD_BI_16_8 16 CH 8
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_7 ASCENDING,
   FLD_BI_16_8 ASCENDING,
   FLD_BI_24_2 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYAU
       ;;
(VA01EYAU)
       m_CondExec 00,EQ,VA01EYAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYAX
       ;;
(VA01EYAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/VA01EYAT.VA500BEY
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA01EYAX.VA500CEY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA01EYAY
       ;;
(VA01EYAY)
       m_CondExec 04,GE,VA01EYAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBA
       ;;
(VA01EYBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/VA01EYAX.VA500CEY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA01EYBA.VA500DEY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYBB
       ;;
(VA01EYBB)
       m_CondExec 00,EQ,VA01EYBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA500 (ETAT DES ANOMALIES DE MVTS DE STOCK)             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBD
       ;;
(VA01EYBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/VA01EYAT.VA500BEY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FCUMULS ${DATA}/PTEM/VA01EYBA.VA500DEY
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA500 DES ANOMALIES DE MVTS DE STOCKS                          
#  FEDITION REPORT SYSOUT=(9,IVA500),RECFM=VA                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVAEY.IVA500GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA01EYBE
       ;;
(VA01EYBE)
       m_CondExec 04,GE,VA01EYBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FGG505AR POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBG
       ;;
(VA01EYBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FGG505GE
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA01EYBG.GG505BEY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_7 7 CH 7
 /FIELDS FLD_BI_21_8 21 CH 8
 /FIELDS FLD_BI_14_7 14 CH 7
 /FIELDS FLD_BI_29_2 29 CH 2
 /KEYS
   FLD_BI_7_7 ASCENDING,
   FLD_BI_14_7 ASCENDING,
   FLD_BI_21_8 ASCENDING,
   FLD_BI_29_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYBH
       ;;
(VA01EYBH)
       m_CondExec 00,EQ,VA01EYBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBJ
       ;;
(VA01EYBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/VA01EYBG.GG505BEY
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA01EYBJ.GG505CEY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA01EYBK
       ;;
(VA01EYBK)
       m_CondExec 04,GE,VA01EYBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBM
       ;;
(VA01EYBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/VA01EYBJ.GG505CEY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA01EYBM.GG505DEY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA01EYBN
       ;;
(VA01EYBN)
       m_CondExec 00,EQ,VA01EYBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IGG505 (ETAT DES MAJ DE PRA)                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBQ
       ;;
(VA01EYBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/VA01EYBG.GG505BEY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/VA01EYBM.GG505DEY
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IGG505 DES ANOMALIES DE MVTS DE STOCKS                          
#  FEDITION REPORT SYSOUT=(9,IGG505),RECFM=VA                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVAEY.IGG505GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA01EYBR
       ;;
(VA01EYBR)
       m_CondExec 04,GE,VA01EYBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IVA500 LISTE DES ANOMALIES DES MOUVEMENTS DE STOCK                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBT PGM=IEBGENER   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBT
       ;;
(VA01EYBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.IVA500GE
       m_OutputAssign -c 9 -w IVA500 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA01EYBU
       ;;
(VA01EYBU)
       m_CondExec 00,EQ,VA01EYBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IGG505 LISTE DES ANOMALIES DES MAJ DU PRA                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA01EYBX PGM=IEBGENER   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYBX
       ;;
(VA01EYBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.IGG505GE
       m_OutputAssign -c 9 -w IGG505 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA01EYBY
       ;;
(VA01EYBY)
       m_CondExec 00,EQ,VA01EYBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA01EYZA
       ;;
(VA01EYZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA01EYZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
