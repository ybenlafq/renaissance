#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF43L.ksh                       --- VERSION DU 08/10/2016 15:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLQMF43 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/01/27 AT 10.35.54 BY BURTECA                      
#    STANDARDS: P  JOBSET: QMF43L                                              
# --------------------------------------------------------------------         
# **--USER='DNPC'                                                              
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY Q043                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF43LA
       ;;
(QMF43LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       JUMP_LABEL=QMF43LAA
       ;;
(QMF43LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i SORTIN
RUN Q043 (&&DATEDEB='$DMOISJ_5_4$DMOISJ_3_2$DMOISJ_1_2' &&DATEFIN='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F043
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F61.QMF001AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF43LAB
       ;;
(QMF43LAB)
       m_CondExec 00,EQ,QMF43LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE QMF043                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF43LAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=QMF43LAD
       ;;
(QMF43LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF043 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F61.QMF001AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF43LAE
       ;;
(QMF43LAE)
       m_CondExec 04,GE,QMF43LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF043                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF43LAG PGM=IEFBR14    **                                          
# ***********************************                                          
       JUMP_LABEL=QMF43LAG
       ;;
(QMF43LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F61.QMF001AL
       m_ProgramExec IEFBR14 
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
