#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LW231P.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLW231 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/11/02 AT 12.36.49 BY BURTECA                      
#    STANDARDS: P  JOBSET: LW231P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   WAIT PAR PRECAUTION AFIN DE S'ASSURER QUE LE FICHIER ENVOYE PAR LA         
#   CHAINE PRECEDENTE LW2**P SOIT BIEN EXECUTE DANS CFT                        
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LW231PA
       ;;
(LW231PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+3'}
       G_A9=${G_A9:-'+3'}
       RUN=${RUN}
       JUMP_LABEL=LW231PAA
       ;;
(LW231PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_ProgramExec WAITSS "0060"
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#                                                                              
# **************************************                                       
# ********************************************************************         
#   BLW200 : INTERFACE DES OP ( MUTATIONS ) _A PREPARER                         
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAD
       ;;
(LW231PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES EN LECTURE                                                           
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGB02   : NAME=RSGB02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB02 /dev/null
#    RSGB03   : NAME=RSGB03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB03 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGB40   : NAME=RSGB40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB40 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#  TABLES EN MAJ                                                               
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSLW05   : NAME=RSLW05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW05 /dev/null
#    RSLW15   : NAME=RSLW15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW15 /dev/null
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSLW99   : NAME=RSLW99,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW99 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FJOUR ${DATA}/CORTEX4.P.MTXTFIX1/LW231P1
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1207 -t LSEQ -g +1 FMUT00 ${DATA}/PXX0/F07.BLW200AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW200 
       JUMP_LABEL=LW231PAE
       ;;
(LW231PAE)
       m_CondExec 04,GE,LW231PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAG
       ;;
(LW231PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BLW200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1207 -t LSEQ -g +1 SORT095 ${DATA}/PXX0/F07.BLW200RP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1207 -t LSEQ -g +1 SORT090 ${DATA}/PXX0/F07.BLW200SP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1207 -t LSEQ -g +1 SORT120 ${DATA}/PXX0/F07.BLW200TP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "095"
 /DERIVEDFIELD CST_1_9 "120"
 /DERIVEDFIELD CST_1_6 "090"
 /FIELDS FLD_CH_5_3 5 CH 3
 /CONDITION CND_2 FLD_CH_5_3 EQ CST_1_6 
 /CONDITION CND_3 FLD_CH_5_3 EQ CST_1_9 
 /CONDITION CND_1 FLD_CH_5_3 EQ CST_1_3 
 /COPY
 /MT_OUTFILE_ASG SORT095
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORT090
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORT120
 /INCLUDE CND_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LW231PAH
       ;;
(LW231PAH)
       m_CondExec 00,EQ,LW231PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   EASYTREAVE 907095                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAJ PGM=EZTPA00    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAJ
       ;;
(LW231PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FILEA ${DATA}/PXX0/F07.BLW200RP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1204 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW200BP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW231PAJ
       m_FileSort -s SYSIN
# ********************************************************************         
#   EASYTREAVE 907090                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAM PGM=EZTPA00    ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAM
       ;;
(LW231PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FILEA ${DATA}/PXX0/F07.BLW200SP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1204 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW200CP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW231PAM
       m_FileSort -s SYSIN
# ********************************************************************         
#   EASYTREAVE 907120                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAQ PGM=EZTPA00    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAQ
       ;;
(LW231PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FILEA ${DATA}/PXX0/F07.BLW200TP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1204 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW200DP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW231PAQ
       m_FileSort -s SYSIN
# ********************************************************************         
#  ENVOI CFT SUR LA GATEWAY 907095                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BLW200BP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI CFT SUR LA GATEWAY 907090                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BLW200CP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI CFT SUR LA GATEWAY 907120                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# *ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                      
# *CFTIN    DATA  *                                                            
# *SEND PART=XFBPRO,                                                           
# *     IDF=BLW200DP                                                           
# *         DATAEND                                                            
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW231P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAT
       ;;
(LW231PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/LW231PAT.FTLW231P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW231P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LW231PAX
       ;;
(LW231PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW231PAT.FTLW231P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP LW231PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LW231PBA
       ;;
(LW231PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW231P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LW231PBD
       ;;
(LW231PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A6} SYSOUT ${DATA}/PTEM/LW231PAT.FTLW231P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW231P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LW231PBG
       ;;
(LW231PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PBG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW231PAT.FTLW231P(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP LW231PBJ PGM=EZACFSM1   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=LW231PBJ
       ;;
(LW231PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PBJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW231P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PBM PGM=EZACFSM1   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=LW231PBM
       ;;
(LW231PBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PBM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A8} SYSOUT ${DATA}/PTEM/LW231PAT.FTLW231P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW231P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW231PBQ PGM=FTP        ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=LW231PBQ
       ;;
(LW231PBQ)
       m_CondExec ${EXACN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PBQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW231PAT.FTLW231P(+3),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
# ABY      STEP  PGM=EZACFSM1                                                  
# SYSOUT   FILE  NAME=FLOGFTP,MODE=N                                           
# SYSIN    DATA  *                                                             
# &LYYMMDD &LHR:&LMIN:&LSEC &JOBNAME OPSYMDHMS                                 
#         DATAEND                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LW231PZA
       ;;
(LW231PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW231PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
