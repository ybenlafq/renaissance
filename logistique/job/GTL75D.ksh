#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL75D.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL75 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/01/27 AT 10.53.27 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL75D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    IMPERATIF :  CETTE CHAINE DOIT ETRE EXECUTEE IMPERATIVEMENT               
#    -=-=-=-=-                                                                 
# ********************************************************************         
#  TRI DU FICHIER FTL076 (FICHIER DES VENTES DE LA VEILLE)                     
#  SUR : DATE LIVRAISON,CODE MAG,NUM DE VENTE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL75DA
       ;;
(GTL75DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL75DAA
       ;;
(GTL75DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DES NUMEROS DE FOLIOS        *                                        
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PTL991/F91.BTL076AD
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PTEM/GTL75DAA.BTL075AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_14_8 14 CH 8
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_14_8 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 40 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75DAB
       ;;
(GTL75DAB)
       m_CondExec 00,EQ,GTL75DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL075                                                                
# ********************************************************************         
#   GENERATION DU FICHIER DES VENTES EN ANOMALIE DE LIVRAISON PAR MAGA         
#   POUR UNE DATE DE LIVRAISON                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL75DAD
       ;;
(GTL75DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ANOMALIES                                                  
#    RTAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00D /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02D /dev/null
# ******  TABLE DES ENTETES LIGNE DE VENTE                                     
#    RTGV10D  : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10D /dev/null
# ******  TABLE DES LIGNES DE VENTE                                            
#    RTGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11D /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01D  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01D /dev/null
# ******  TABLE DES DETAILS TOURNEES DE LIVRAISON                              
#    RTTL02D  : NAME=RSTL02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02D /dev/null
# ******  FICHIER DES TOURNEES PREVISIONNELLES                                 
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL991/F91.BTL001AD
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A1} FTL075 ${DATA}/PTEM/GTL75DAA.BTL075AD
# ******  FICHIER VENTE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FTL076 ${DATA}/PTL991/F91.BTL076AD
# ******  FICHIER VENTE EN ANOMALIE DE LIVRAISON PAR MAGASIN                   
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FTL077 ${DATA}/PTEM/GTL75DAD.BTL077AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL075 
       JUMP_LABEL=GTL75DAE
       ;;
(GTL75DAE)
       m_CondExec 04,GE,GTL75DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTL077 (FICHIER DES VENTES EN ANOMALIE)                      
#  SUR : CODE PROFIL,CODE MAG,CODE TOURNEE,NUM DE VENTE                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL75DAG
       ;;
(GTL75DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GTL75DAD.BTL077AD
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/GTL75DAG.BTL078AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_43_6 43 CH 6
 /FIELDS FLD_CH_34_1 34 CH 1
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_43_6 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_34_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75DAH
       ;;
(GTL75DAH)
       m_CondExec 00,EQ,GTL75DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL080                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#   EDITION DES VENTES EN ANOMALIE DE LIVRAISON                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL75DAJ
       ;;
(GTL75DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02D /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01D  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01D /dev/null
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A3} FTL080 ${DATA}/PTEM/GTL75DAG.BTL078AD
# ******  FICHIER EDITION                                                      
       m_OutputAssign -c 9 -w ITL080 ITL080
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL080 
       JUMP_LABEL=GTL75DAK
       ;;
(GTL75DAK)
       m_CondExec 04,GE,GTL75DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL75DZA
       ;;
(GTL75DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL75DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
