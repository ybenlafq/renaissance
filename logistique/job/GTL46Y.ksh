#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL46Y.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGTL46 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.15.21 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL46Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# **--USER='LYON'                                                              
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000001 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000001 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL46YA
       ;;
(GTL46YA)
       DATE=${DATEJ}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       RUN=${RUN}
       UPSI=${UPSI:-''}
       USER=${USER:-''}
       JUMP_LABEL=GTL46YAA
       ;;
(GTL46YAA)
       m_CondExec ${EXAAA},NE,YES 
# AA      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    CHARGEMENT DANS DISPATCH DES      *                                       
#    LISTES DE DESTOCKAGES             *                                       
# **************************************                                       
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YR5
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y01
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL1 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000001
JTL013$VDATEJ__AAMMJJ000000001
JTL014$VDATEJ__AAMMJJ000000001
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000002 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000002 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAD
       ;;
(GTL46YAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YR6
       m_OutputAssign -c "*" DDOTV02
# AF      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y02
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL2 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000002
JTL013$VDATEJ__AAMMJJ000000002
JTL014$VDATEJ__AAMMJJ000000002
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000003 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000003 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAG
       ;;
(GTL46YAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YR7
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y03
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL3 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000003
JTL013$VDATEJ__AAMMJJ000000003
JTL014$VDATEJ__AAMMJJ000000003
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B.R DANS EOS                                        
# ********************************************************************         
#   ETAT JTL021 DESTINATION 000000000 B.R SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL022 DESTINATION 000000000 B.R AVEC CODE BARRE                      
# ********************************************************************         
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAJ PGM=DFSRRC00   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAJ
       ;;
(GTL46YAJ)
       m_CondExec ${EXAAP},NE,YES 
# AP      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YR8
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y04
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BR IMPRIM
#  ETAT DATE SOIT JTL021,AAMMJJ,000000000  000000000 = DEST DS IG50            
       m_FileAssign -i SYSIN
JTL021$VDATEJ__AAMMJJ000000000
JTL022$VDATEJ__AAMMJJ000000000
JTL023$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  B I G 0 0 2                                                                 
# ********************************************************************         
#  CHARGEMENT DU JTL301 DANS DISPATCH                                          
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAM PGM=DFSRRC00   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAM
       ;;
(GTL46YAM)
       m_CondExec ${EXAAU},NE,YES 
# AU      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,REGION=4096K,VSAMP=DFSVSAMP               
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YR9
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y05
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0023 IMPRIM
#  ETAT DATE  SOIT JTL301,AAMMJJ,000000000                                     
       m_FileAssign -i SYSIN
JTL301$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002  : EDITION DES APPAREILS A COMMUT�S : ETAT JGD751                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAQ PGM=DFSRRC00   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAQ
       ;;
(GTL46YAQ)
       m_CondExec ${EXAAZ},NE,YES 
# AZ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,REGION=4096K,VSAMP=DFSVSAMP               
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YRA
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y06
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0024 IMPRIM
#  ETAT DATE  SOIT JGD751,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD751$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002  : EDITION DES APPAREILS A COMMUT�S : ETAT JGD801                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAT PGM=DFSRRC00   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAT
       ;;
(GTL46YAT)
       m_CondExec ${EXABE},NE,YES 
# BE      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,REGION=4096K,VSAMP=DFSVSAMP               
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YRB
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y07
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0025 IMPRIM
#  ETAT DATE  SOIT JGD801,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD801$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE SYNTHESE DE DESTOCKAGE  : JGD921                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YAX
       ;;
(GTL46YAX)
       m_CondExec ${EXABJ},NE,YES 
# BJ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,REGION=4096K,VSAMP=DFSVSAMP               
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YRC
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y08
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0027 IMPRIM
#  ETAT DATE  SOIT JGD921,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD921$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE DESTOCKAGE FILIALE ETAT: JGD731                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YBA PGM=DFSRRC00   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YBA
       ;;
(GTL46YBA)
       m_CondExec ${EXABO},NE,YES 
# BO      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,REGION=4096K,VSAMP=DFSVSAMP               
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YRD
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y09
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0026 IMPRIM
#  ETAT DATE  SOIT JGD731,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD731$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#  BIG002  : EDITION LISTE ANOMALIE DE LIVRAISON ETAT: JTL401                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46YBD PGM=DFSRRC00   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL46YBD
       ;;
(GTL46YBD)
       m_CondExec ${EXABT},NE,YES 
# BT      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,REGION=4096K,VSAMP=DFSVSAMP               
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46YRE
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46Y10
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0027 IMPRIM
#  ETAT DATE  SOIT JTL401,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JTL401$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP COMREG   PGM=CZX3PEPI   **                                          
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
