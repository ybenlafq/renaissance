#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LV216P.ksh                       --- VERSION DU 08/10/2016 22:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLV216 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/01/19 AT 10.06.39 BY BURTEC3                      
#    STANDARDS: P  JOBSET: LV216P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BLV200                                                                
# ********************************************************************         
#  INTERFACE DES MUTATIONS                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LV216PA
       ;;
(LV216PA)
       EXAAA=${EXAAA:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LV216PAA
       ;;
(LV216PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FJOUR ${DATA}/CORTEX4.P.MTXTFIX1/LV216P1
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#  LECTURE                                                                     
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGB02   : NAME=RSGB02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB02 /dev/null
#    RTGB03   : NAME=RSGB03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB03 /dev/null
#    RTGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB15 /dev/null
#    RTGB40   : NAME=RSGB40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB40 /dev/null
#    RTGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV21 /dev/null
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTLI01   : NAME=RSLI01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI01 /dev/null
#    RTLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI20 /dev/null
#    RTTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH00 /dev/null
#    RTSU10   : NAME=RSSU10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTSU10 /dev/null
#    RTSU20   : NAME=RSSU20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTSU20 /dev/null
#  MAJ                                                                         
#    RTAN02   : NAME=RSAN02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN02 /dev/null
#    RTGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB05 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTLV05   : NAME=RSLV05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV05 /dev/null
#    RTLV15   : NAME=RSLV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV15 /dev/null
#    RTLV99   : NAME=RSLV99,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTLV99 /dev/null
#         FICHIER                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1-1204 -t LSEQ -g +1 FMUT00 ${DATA}/PXX0/F07.BLV200AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLV200 
       JUMP_LABEL=LV216PAB
       ;;
(LV216PAB)
       m_CondExec 04,GE,LV216PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FLV216AP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LV216PAD PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LV216PAD
       ;;
(LV216PAD)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LV216PAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/LV216PAD.FLV216AP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FLV216AP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LV216PAG PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LV216PAG
       ;;
(LV216PAG)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSTCPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LV216PAG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LV216PAD.FLV216AP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LV216PZA
       ;;
(LV216PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LV216PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
