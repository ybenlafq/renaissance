#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA800P.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVA800 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.10.06 BY BURTEC2                      
#    STANDARDS: P  JOBSET: VA800P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER BVA511ZP                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VA800PA
       ;;
(VA800PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VA800PAA
       ;;
(VA800PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *    FICHIER ISSU DE VA420P                                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BVA511ZP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA800PAA.FVA800AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_164_8 164 CH 8
 /FIELDS FLD_PD_164_8 164 PD 8
 /FIELDS FLD_CH_47_5 47 CH 5
 /FIELDS FLD_PD_156_8 156 PD 8
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_9_11 9 CH 11
 /FIELDS FLD_CH_156_8 156 CH 8
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_15_5 15 CH 5
 /KEYS
   FLD_CH_47_5 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_12_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_156_8,
    TOTAL FLD_PD_164_8
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_9_11,FLD_CH_47_5,FLD_CH_156_8,FLD_CH_164_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA800PAB
       ;;
(VA800PAB)
       m_CondExec 00,EQ,VA800PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BVA520AP                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA800PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA800PAD
       ;;
(VA800PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *    FICHIER ISSU DE VA420P                                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BVA520AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA800PAD.FVA800BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_122_5 122 CH 5
 /FIELDS FLD_CH_61_5 61 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_PD_147_8 147 PD 8
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_PD_139_8 139 PD 8
 /FIELDS FLD_CH_147_8 147 CH 8
 /FIELDS FLD_CH_139_8 139 CH 8
 /KEYS
   FLD_CH_61_5 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_122_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_8,
    TOTAL FLD_PD_147_8
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_6,FLD_CH_122_5,FLD_CH_61_5,FLD_CH_139_8,FLD_CH_147_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA800PAE
       ;;
(VA800PAE)
       m_CondExec 00,EQ,VA800PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FUSION DES FICHIERS FVA800AP ET FVA800BP                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA800PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA800PAG
       ;;
(VA800PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/VA800PAA.FVA800AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/VA800PAD.FVA800BP
       m_FileAssign -d NEW,CATLG,DELETE -r 32 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA800PAG.FVA800CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 32 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA800PAH
       ;;
(VA800PAH)
       m_CondExec 00,EQ,VA800PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA800                                                                
# ********************************************************************         
#  CREATION FICHIER MARGE A ENVOYER VERS SAP                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA800PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA800PAJ
       ;;
(VA800PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A3} FFT429 ${DATA}/PTEM/VA800PAG.FVA800CP
# ******  TABLE DB2 RTGA06                                                     
#    RTGA06   : NAME=RSGA06,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA06 /dev/null
# ******  FICHIER FVA800 ENVOI VERS SAP                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 FVA800 ${DATA}/PXX0/SAP.SAPREMP
# ******* PARAMETRE MOIS                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA800 
       JUMP_LABEL=VA800PAK
       ;;
(VA800PAK)
       m_CondExec 04,GE,VA800PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER VERS SAP VIA GATEWAY                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  CLASS=VAR,PARMS=VA800P                                        
#   PARMS                                                                      
#   LINE                                                                       
#    FLD 1,SOURCE='SEND PART=XFBPRO,'                                          
#   LINE                                                                       
#    FLD 6,SOURCE='IDF=SAPREMP,'                                               
#   LINE                                                                       
#    FLD  6,SOURCE='NFNAME=SAPREMP_'                                           
#    FLD 21,SOURCE=Y                                                           
#    FLD 23,SOURCE=M                                                           
#    FLD 25,SOURCE=D                                                           
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTVA800P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA800PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA800PAM
       ;;
(VA800PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA800PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/VA800PAM.FTVA800P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FRB040FF                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA800PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA800PAQ
       ;;
(VA800PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/VA800PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.VA800PAM.FTVA800P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP VA800PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA800PAT
       ;;
(VA800PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA800PAT.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA800PZA
       ;;
(VA800PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA800PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
