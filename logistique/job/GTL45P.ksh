#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL45P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGTL45 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/25 AT 11.02.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL45P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  B T L 0 4 5                                                                 
# ********************************************************************         
#   CREATION DES BONS DE LIVRAISON                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL45PA
       ;;
(GTL45PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL45PAA
       ;;
(GTL45PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ TABLE DES BONS LIVRAISON      *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES PROFILS ET DATE DE DELIVRANCES POUR LEQUELS LES B.L.            
#  SONT A EDITER                                                               
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL0/F07.BTL001BP
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE GENERALISEES (LPLGE CARSP)                                            
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES MARQUES                                                           
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#  TABLE DES VENTES/ADRESSES                                                   
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES VENTES/REGLEMENT                                                  
#    RTGV10   : NAME=RSGV10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV10 /dev/null
#  TABLE DES LIGNES DE VENTES ARTICLES                                         
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES LIGNES DE PRESTATION                                              
#    RTGV13   : NAME=RSGV13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV13 /dev/null
#  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                               
#    RTGV15   : NAME=RSGV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV15 /dev/null
#  TABLE DES VENTES/QUANTITES DISPONIBLES                                      
#    RTGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV21 /dev/null
# ******  LIBELLE DEVISE                                                       
#    RSFM04   : NAME=RSFM04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM04 /dev/null
#  FICHIER DES BONS DE LIVRAISON EXTRAIT                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FTL045 ${DATA}/PTL0/F07.TL0045AP
#  FICHIER DES BONS DE LIVRAISON EXTRAIT                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL065 ${DATA}/PTL0/F07.TL0065AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL045 
       JUMP_LABEL=GTL45PAB
       ;;
(GTL45PAB)
       m_CondExec 04,GE,GTL45PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 5 0                                                                 
# ********************************************************************         
#   CHARGEMENT DES BONS DE LIVRAISONS DANS LA D BASE IMPRESSION                
#   REPRISE: NON BACKOUT DU STEP LOGGING IMS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45PAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL45PAD
       ;;
(GTL45PAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BTL050,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL45PC1)                                             
#  FICHIER DES BONS DE LIVRAISON EXTRAIT                                       
       m_FileAssign -d SHR -g ${G_A1} FTL045 ${DATA}/PTL0/F07.TL0045AP
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL45PC1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#  DB IMPRESSION GENERALISEE                                                   
# DIGVP0   FILE NAME=DI0000DP,MODE=U,REST=(YES,GTL45PR1)                       
# DIGVIP   FILE NAME=DI0000IP,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX0.F07.DI0000DP
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX0.F07.DI0000IP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL45PAD
#  FDATE JJMMSSAA DATE D EDITION DES BL                                        
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGP 
# ********************************************************************         
#  B T L 0 4 6 :  NOUVEAU PGM . VIENT DOUBLER LE BTL45 POUR TEST               
#              :  PHIL LE 21061993                                             
# ********************************************************************         
#   CREATION DES BONS DE LIVRAISON                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL45PAG
       ;;
(GTL45PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES PROFILS ET DATE DE DELIVRANCES POUR LEQUELS LES B.L.            
#  SONT A EDITER :VENANT DU PGM BTL000                                         
       m_FileAssign -d SHR -g +0 FTL002 ${DATA}/PTL0/F07.BTL001AP
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE GENERALISEES (LPLGE CARSP)                                            
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE                                                                       
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE DES MARQUES                                                           
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#  TABLE                                                                       
#    RTGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA58 /dev/null
#  TABLE DES VENTES/ADRESSES                                                   
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES VENTES/REGLEMENT                                                  
#    RTGV10   : NAME=RSGV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#  TABLE DES LIGNES DE VENTES ARTICLES                                         
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES LIGNES DE PRESTATION                                              
#    RTGV13   : NAME=RSGV13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV13 /dev/null
#  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                               
#    RTGV15   : NAME=RSGV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV15 /dev/null
#  TABLE DES VENTES/QUANTITES DISPONIBLES                                      
#    RTGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV21 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RTAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
#  FICHIER DES BONS DE LIVRAISON EXTRAIT                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL065 ${DATA}/PTL0/F07.TL0066AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL046 
       JUMP_LABEL=GTL45PAH
       ;;
(GTL45PAH)
       m_CondExec 04,GE,GTL45PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                TRI DES LIVRAISONS NEW FICHIER                                
#               *******************************                                
#  (DDELIV/NLIEU/NVENTE)                                                       
#  SORT FIELDS=(1,8,A,9,3,A,12,7,A),FORMAT=CH                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL45PAJ
       ;;
(GTL45PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTL0/F07.TL0066AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F07.TL0066BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_7 12 CH 7
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_7 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL45PAK
       ;;
(GTL45PAK)
       m_CondExec 00,EQ,GTL45PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 6 5 PGM DOUBLE EN RAISON DU NEW PGM BTL046                          
# ********************************************************************         
#   CREATION DE LA LISTE DES LIVRAISONS PREVUES                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL45PAM
       ;;
(GTL45PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES LIVRAISONS PREVUES                                              
       m_FileAssign -d SHR -g ${G_A3} FTL065 ${DATA}/PTL0/F07.TL0066BP
#  LISTE DES LIVRAISONS PREVUES                                                
       m_OutputAssign -c 9 -w ITL065 ITL065
#  TABLE DES ANOMALIES                                                         
#    RTAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL065 
       JUMP_LABEL=GTL45PAN
       ;;
(GTL45PAN)
       m_CondExec 04,GE,GTL45PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BTL090  : LISTES DES ADRESSES FORC�ES EN MAGASIN                           
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL45PAQ
       ;;
(GTL45PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES ADRESSES                                                   
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ******  TABLE DES VENTES DETAILL�ES                                          
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  LISTE DES ADRESSES FORC�ES EN MAGASINS                               
       m_OutputAssign -c 9 -w ITL090 ITL090
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL090 
       JUMP_LABEL=GTL45PAR
       ;;
(GTL45PAR)
       m_CondExec 04,GE,GTL45PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
