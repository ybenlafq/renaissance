#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE51D.ksh                       --- VERSION DU 09/10/2016 00:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGIE51 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/28 AT 11.40.41 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GIE51D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   IMAGECOPY DU TABLESPACE RSIE10R DE LA D BASE PDDGI00                       
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE51DA
       ;;
(GIE51DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2009/03/28 AT 11.40.41 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GIE51D                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK/ECARTS'                      
# *                           APPL...: IMPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE51DAA
       ;;
(GIE51DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F91.IE10ID
#                                                                              
# ******  AVANCEMENT INVENTAIRE ENTREPOT                                       
#    TABLES   : NAME=RSIE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLES /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51DAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51DAB
       ;;
(GIE51DAB)
       m_CondExec 04,GE,GIE51DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS10  DE LA D BASE PPDGS00                       
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAD
       ;;
(GIE51DAD)
       m_CondExec ${EXAAF},NE,YES 
#    RSGS10   : NAME=RSGS10,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F07.GS10IP
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51DAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51DAE
       ;;
(GIE51DAE)
       m_CondExec 04,GE,GIE51DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS15   DE LA D BASE PPDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAG
       ;;
(GIE51DAG)
       m_CondExec ${EXAAK},NE,YES 
#    RSGS15   : NAME=RSGS15,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS15 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F07.GS15IP
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51DAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51DAH
       ;;
(GIE51DAH)
       m_CondExec 04,GE,GIE51DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS70R  DE LA D BASE PDDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAJ
       ;;
(GIE51DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F91.GS70ID
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51DAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51DAK
       ;;
(GIE51DAK)
       m_CondExec 04,GE,GIE51DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS60R  DE LA D BASE PDDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAM
       ;;
(GIE51DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F91.GS60ID
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51DAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51DAN
       ;;
(GIE51DAN)
       m_CondExec 04,GE,GIE51DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE510  : EPURATION DES TABLES RTGS60 ET RTGS70                             
#            A SAVOIR HS SORTI DU HS => REPARES ET                             
#            DONC PARTIS DANS UN AUTRE LIEU QUE HS                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAQ
       ;;
(GIE51DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGS60   : NAME=RSGS60D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
#    RSGS70   : NAME=RSGS70D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE510 
       JUMP_LABEL=GIE51DAR
       ;;
(GIE51DAR)
       m_CondExec 04,GE,GIE51DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE515   REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS               
#  REPRISE: SURTOUT PAS CAR COMMIT RESTAURE A FULL IMAGE COPY PRECEDEN         
#           DES TABLES RTGS10 RTGS15 RTGS60 RTGS70 RTIE10                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAT
       ;;
(GIE51DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK THEORIC AVANT INVENTAIRE                                       
#    RSIE05   : NAME=RSIE05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  STOCK HS                                                             
#    RSGS60   : NAME=RSGS60D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  HISTO DES HS                                                         
#    RSGS70   : NAME=RSGS70D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
# ******  PRET A L'ENTREPOT LGT                                                
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  STOCK SS LIEU ENTREPOT LGT                                           
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  AVANCEMENT INVENTAIRE ENTREPOT                                       
#    RSIE10   : NAME=RSIE10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS                                     
       m_FileAssign -d SHR FIIE515 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE515 
       JUMP_LABEL=GIE51DAU
       ;;
(GIE51DAU)
       m_CondExec 04,GE,GIE51DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE516   REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS               
#  REPRISE: SURTOUT PAS CAR COMMIT RESTAURE A FULL IMAGE COPY PRECEDEN         
#           DES TABLES RTGS10 RTGS15 RTGS60 RTGS70 RTIE10                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GIE51DAX
       ;;
(GIE51DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK THEORIC AVANT INVENTAIRE                                       
#    RSIE60   : NAME=RSIE60D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  STOCK HS                                                             
#    RSGS60   : NAME=RSGS60D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  HISTO DES HS                                                         
#    RSGS70   : NAME=RSGS70D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
# ******  PRET A L'ENTREPOT LGT                                                
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  STOCK SS LIEU ENTREPOT LGT                                           
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS                                     
       m_FileAssign -d SHR FIIE516 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE516 
       JUMP_LABEL=GIE51DAY
       ;;
(GIE51DAY)
       m_CondExec 04,GE,GIE51DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
