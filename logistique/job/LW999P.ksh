#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LW999P.ksh                       --- VERSION DU 08/10/2016 22:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLW999 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/12/26 AT 18.53.29 BY BURTECA                      
#    STANDARDS: P  JOBSET: LW999P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BMQLM7                                                               
#   ------------                                                               
#   RECOPIE DES MSG LM7 DANS UN FICHIER AVEC CRITERE DATE DE CREATION          
#    = FDATE - 7 JOURS (FDELAI) => MENAGE SUR Q1907095LW0006                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LW999PA
       ;;
(LW999PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       RUN=${RUN}
       JUMP_LABEL=LW999PAA
       ;;
(LW999PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_StepLibSet CORTEX4.DB2.MLNKMOD
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER QUI PERMET DE RECHARGER DANS UNE QUEUE                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FMQOUT ${DATA}/PXX0/F07.FLW99MQP
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P1
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P2
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/LW999P3
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P4
       m_FileAssign -d SHR FDELAI ${DATA}/CORTEX4.P.MTXTFIX1/LW999P5
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P6
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P7
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P8
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P9
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/LW999P10
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P11
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P12
       m_ProgramExec BMQLM7 
# ********************************************************************         
#   PGM : BMQLM7                                                               
#   ------------                                                               
#   RECOPIE DES MSG LM7 DANS UN FICHIER AVEC CRITERE DATE DE CREATION          
#    = FDATE - 7 JOURS (FDELAI) => MENAGE SUR Q1907090LW0006                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW999PAD PGM=BMQLM7     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LW999PAD
       ;;
(LW999PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_StepLibSet CORTEX4.DB2.MLNKMOD
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER QUI PERMET DE RECHARGER DANS UNE QUEUE                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FMQOUT ${DATA}/PXX0/F07.FLW99MQM
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P13
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P14
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/LW999P15
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P16
       m_FileAssign -d SHR FDELAI ${DATA}/CORTEX4.P.MTXTFIX1/LW999P17
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P18
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P19
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P20
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P21
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/LW999P22
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P23
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P24
       m_ProgramExec BMQLM7 
# ********************************************************************         
#   PGM : BMQLM7                                                               
#   ------------                                                               
#   RECOPIE DES MSG LM7 DANS UN FICHIER AVEC CRITERE DATE DE CREATION          
#    = FDATE - 7 JOURS (FDELAI) => MENAGE SUR                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW999PAG PGM=BMQLM7     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LW999PAG
       ;;
(LW999PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_StepLibSet CORTEX4.DB2.MLNKMOD
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER QUI PERMET DE RECHARGER DANS UNE QUEUE                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FMQOUT ${DATA}/PXX0/F07.FLW99MQR
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P25
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P26
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/LW999P27
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P28
       m_FileAssign -d SHR FDELAI ${DATA}/CORTEX4.P.MTXTFIX1/LW999P29
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P30
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P31
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P32
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P33
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/LW999P34
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P35
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P36
       m_ProgramExec BMQLM7 
# ********************************************************************         
#   PGM : BMQLM7                                                               
#   ------------                                                               
#   RECOPIE DES MSG LM7 DANS UN FICHIER AVEC CRITERE DATE DE CREATION          
#    = FDATE - 7 JOURS (FDELAI) => MENAGE SUR                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW999PAJ PGM=BMQLM7     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LW999PAJ
       ;;
(LW999PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_StepLibSet CORTEX4.DB2.MLNKMOD
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER QUI PERMET DE RECHARGER DANS UNE QUEUE                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FMQOUT ${DATA}/PXX0/F07.FLW99MQZ
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P37
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P38
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/LW999P39
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/LW999P40
       m_FileAssign -d SHR FDELAI ${DATA}/CORTEX4.P.MTXTFIX1/LW999P41
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P42
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/LW999P43
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P44
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P45
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/LW999P46
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/LW999P47
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/LW999P48
       m_ProgramExec BMQLM7 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
