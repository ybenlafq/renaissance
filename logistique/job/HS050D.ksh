#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HS050D.ksh                       --- VERSION DU 08/10/2016 13:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDHS050 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/07/23 AT 14.14.42 BY BURTECR                      
#    STANDARDS: P  JOBSET: HS050D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUERY : QHSMAG2  FORM : FHSMAG2  (REMPLACEMENT DE QHSMAG)                   
# ********************************************************************         
#  EDITION DES STOCKS HS MAG                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=HS050DA
       ;;
(HS050DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2001/07/23 AT 14.14.42 BY BURTECR                
# *    JOBSET INFORMATION:    NAME...: HS050D                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'REQUETE HS/MAG'                        
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=HS050DAA
       ;;
(HS050DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHSMAG DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/HS050D1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p HS050D1 -a QMFPARM DSQPRINT
       JUMP_LABEL=HS050DAB
       ;;
(HS050DAB)
       m_CondExec 04,GE,HS050DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUERY : QHSMAGT FORM : FHSMAGT                                              
# ********************************************************************         
#  EDITION DES STOCKS HS MAG                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HS050DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HS050DAD
       ;;
(HS050DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHSMAGT DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/HS050D2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p HS050D2 -a QMFPARM DSQPRINT
       JUMP_LABEL=HS050DAE
       ;;
(HS050DAE)
       m_CondExec 04,GE,HS050DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUERY : QHSMAGS FORM : FHSMAGS                                              
# ********************************************************************         
#  EDITION DES STOCKS HS MAG                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HS050DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HS050DAG
       ;;
(HS050DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QHSMAGS DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/HS050D3
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p HS050D3 -a QMFPARM DSQPRINT
       JUMP_LABEL=HS050DAH
       ;;
(HS050DAH)
       m_CondExec 04,GE,HS050DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
