#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL55P.ksh                       --- VERSION DU 09/10/2016 05:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGTL55 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/25 AT 11.03.53 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL55P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  B T L 0 5 5                                                                 
# ********************************************************************         
#   CREATION DES BONS DE REPRISES                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL55PA
       ;;
(GTL55PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL55PAA
       ;;
(GTL55PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES B.R. PLUS PROFILS LIV PROVENANT DE LA CHAINE DES TOURNE         
#  DE LIVRAISON                                                                
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL0/F07.BTL001BP
#  TABLE DES ARTICLES                                                          
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE GENERALISEES (CARSP)                                                  
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES MARQUES                                                           
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#  TABLE DES VENTES/ADRESSES                                                   
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES VENTES/REGLEMENT                                                  
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGV10   : NAME=RSGV10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV10 /dev/null
#  TABLE DES LIGNES DE VENTES ARTICLES                                         
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                               
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGV15   : NAME=RSGV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV15 /dev/null
#  TABLE DES VENTES/QUANTITES DISPONIBLES                                      
#  DD SERVANT POUR LES CROSS REF                                               
#    RTGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV21 /dev/null
#  FDATE JJMMSSAA                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES BONS DE LIVRAISON EXTRAIT                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FTL055 ${DATA}/PTEM/GTL55PAA.TL0055AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL055 
       JUMP_LABEL=GTL55PAB
       ;;
(GTL55PAB)
       m_CondExec 04,GE,GTL55PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 6 0                                                                 
# ********************************************************************         
#   CHARGEMENT DES BONS DE REPRISES   DANS LA D BASE IMPRESSION                
#   REPRISE: NON BACKOUT DU STEP LOGGING IMS                                   
# ********************************************************************         
# AF      IMSSTEP PGM=BTL060,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL55RC1)                                             
#                                                                              
# ***********************************                                          
# *   STEP GTL55PAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL55PAD
       ;;
(GTL55PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL55RC1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#                                                                              
#  FICHIER DES BONS DE REPRISES EXTRAIT                                        
       m_FileAssign -d SHR -g ${G_A1} FTL055 ${DATA}/PTEM/GTL55PAA.TL0055AP
#  DB IMPRESSION GENERALISEE                                                   
# IGVP0   FILE NAME=DI0000DP,MODE=U,REST=(YES,GTL55PR1)                        
# IGVIP   FILE NAME=DI0000IP,MODE=U,REST=NO                                    
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX0.F07.DI0000DP
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX0.F07.DI0000IP
#  FDATE JJMMSSAA DATE D EDITION DES BR                                        
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL55P01
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGP 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL55PZA
       ;;
(GTL55PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL55PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
