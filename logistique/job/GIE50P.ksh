#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE50P.ksh                       --- VERSION DU 09/10/2016 05:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGIE50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/18 AT 14.25.22 BY OPERAT1                      
#    STANDARDS: P  JOBSET: GIE50P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSIE05 ET RSIE10 AVANT MAJ                          
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE50PA
       ;;
(GIE50PA)
#
#GIE50PAA
#GIE50PAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GIE50PAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2014/03/18 AT 14.25.22 BY OPERAT1                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GIE50P                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'LOAD RTIE05 INVT'                      
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE50PAD
       ;;
(GIE50PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE CENTRALE DE L'INVENTAIRE ENTREPOT                              
#    RSIE00   : NAME=RSIE00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE00 /dev/null
# ******  STOCK SS LIEU ENTREPOT                                               
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#                                                                              
# ******  STOCK THEORIQUE  AVANT INVENTAIRE                                    
#    RSIE05   : NAME=RSIE05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  STOCK THEORIQUE  AVANT INVENTAIRE (HS)                               
#    RSIE60   : NAME=RSIE60,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  AVANCEMENT D'INVENTAIRE ENTREPOT                                     
#    RSIE10   : NAME=RSIE10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE10 /dev/null
# ******                                                                       
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE507 
       JUMP_LABEL=GIE50PAE
       ;;
(GIE50PAE)
       m_CondExec 04,GE,GIE50PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
