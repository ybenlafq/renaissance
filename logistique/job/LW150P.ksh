#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LW150P.ksh                       --- VERSION DU 17/10/2016 18:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLW150 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/10 AT 14.56.13 BY BURTEC2                      
#    STANDARDS: P  JOBSET: LW150P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BLW150 : INTERFACE DES LIBELLES                                            
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LW150PA
       ;;
(LW150PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+3'}
       G_A9=${G_A9:-'+3'}
       RUN=${RUN}
       JUMP_LABEL=LW150PAA
       ;;
(LW150PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#                                                                              
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES EN LECTURE                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA06   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-57 -t LSEQ -g +1 FRLI00 ${DATA}/PTEM/LW150PAA.BLW150AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW150 
       JUMP_LABEL=LW150PAB
       ;;
(LW150PAB)
       m_CondExec 04,GE,LW150PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI ECLATEMENT DES ENTREPOTS                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAD
       ;;
(LW150PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/LW150PAA.BLW150AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-57 -t LSEQ -g +1 SORT090 ${DATA}/PTEM/LW150PAD.BLW150SP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-57 -t LSEQ -g +1 SORT095 ${DATA}/PTEM/LW150PAD.BLW150RP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-57 -t LSEQ -g +1 SORT120 ${DATA}/PTEM/LW150PAD.BLW150TP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "120"
 /DERIVEDFIELD CST_1_3 "095"
 /DERIVEDFIELD CST_1_6 "090"
 /FIELDS FLD_CH_5_3 5 CH 3
 /CONDITION CND_2 FLD_CH_5_3 EQ CST_1_6 
 /CONDITION CND_3 FLD_CH_5_3 EQ CST_1_9 
 /CONDITION CND_1 FLD_CH_5_3 EQ CST_1_3 
 /COPY
 /MT_OUTFILE_ASG SORT095
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORT090
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORT120
 /INCLUDE CND_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LW150PAE
       ;;
(LW150PAE)
       m_CondExec 00,EQ,LW150PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   EASYTREAVE 907095                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAG PGM=EZTPA00    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAG
       ;;
(LW150PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FILEA ${DATA}/PTEM/LW150PAD.BLW150RP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-54 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW150BP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW150PAG
       m_FileSort -s SYSIN
# ********************************************************************         
#   EASYTREAVE 907090                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAJ PGM=EZTPA00    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAJ
       ;;
(LW150PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FILEA ${DATA}/PTEM/LW150PAD.BLW150SP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-54 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW150CP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW150PAJ
       m_FileSort -s SYSIN
# ********************************************************************         
#   EASYTREAVE 907120                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAM PGM=EZTPA00    ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAM
       ;;
(LW150PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FILEA ${DATA}/PTEM/LW150PAD.BLW150TP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-54 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW150DP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW150PAM
       m_FileSort -s SYSIN
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW150P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAQ
       ;;
(LW150PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/LW150PAQ.FTLW150P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW150P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAT
       ;;
(LW150PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW150PAQ.FTLW150P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW150P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LW150PAX
       ;;
(LW150PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A6} SYSOUT ${DATA}/PTEM/LW150PAQ.FTLW150P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW150P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LW150PBA
       ;;
(LW150PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PBA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW150PAQ.FTLW150P(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW150P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LW150PBD
       ;;
(LW150PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A8} SYSOUT ${DATA}/PTEM/LW150PAQ.FTLW150P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW150P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW150PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LW150PBG
       ;;
(LW150PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PBG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW150PAQ.FTLW150P(+3),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LW150PZA
       ;;
(LW150PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW150PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
