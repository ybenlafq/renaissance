#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL46L.ksh                       --- VERSION DU 08/10/2016 13:53
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGTL46 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.14.48 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL46L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='LILLE'                                                             
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000001 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000001 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL46LA
       ;;
(GTL46LA)
       DATE=${DATEJ}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       RUN=${RUN}
       UPSI=${UPSI:-''}
       USER=${USER:-''}
       JUMP_LABEL=GTL46LAA
       ;;
(GTL46LAA)
       m_CondExec ${EXAAA},NE,YES 
# AA      IMSSTEP PGM=BIG002B,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),              
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    CHARGEMENT DANS DISPATCH DES      *                                       
#    LISTES DE DESTOCKAGES             *                                       
# **************************************                                       
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR1
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L01
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL1 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000001
JTL013$VDATEJ__AAMMJJ000000001
JTL014$VDATEJ__AAMMJJ000000001
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000002 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000002 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAD
       ;;
(GTL46LAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR2
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L02
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL2 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000002
JTL013$VDATEJ__AAMMJJ000000002
JTL014$VDATEJ__AAMMJJ000000002
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000003 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000003 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAG
       ;;
(GTL46LAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR3
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L03
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL3 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000003
JTL013$VDATEJ__AAMMJJ000000003
JTL014$VDATEJ__AAMMJJ000000003
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B.R DANS EOS                                        
# ********************************************************************         
#   ETAT JTL021 DESTINATION 000000000 B.R SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL022 DESTINATION 000000000 B.R AVEC CODE BARRE                      
# ********************************************************************         
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAJ PGM=DFSRRC00   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAJ
       ;;
(GTL46LAJ)
       m_CondExec ${EXAAP},NE,YES 
# AP      IMSSTEP PGM=BIG002B,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),              
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR4
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L04
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BR IMPRIM
#  ETAT DATE SOIT JTL021,AAMMJJ,000000000  000000000 = DEST DS IG50            
       m_FileAssign -i SYSIN
JTL021$VDATEJ__AAMMJJ000000000
JTL022$VDATEJ__AAMMJJ000000000
JTL023$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  B I G 0 0 2                                                                 
# ********************************************************************         
#  CHARGEMENT DU JTL301 DANS DISPATCH                                          
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAM PGM=DFSRRC00   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAM
       ;;
(GTL46LAM)
       m_CondExec ${EXAAU},NE,YES 
# AU      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR5
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L05
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0023 IMPRIM
#  ETAT DATE  SOIT JTL301,AAMMJJ,000000000                                     
       m_FileAssign -i SYSIN
JTL301$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002  : EDITION DES APPAREILS A COMMUT�S : ETAT JGD751                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAQ PGM=DFSRRC00   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAQ
       ;;
(GTL46LAQ)
       m_CondExec ${EXAAZ},NE,YES 
# AZ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR6
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L06
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0024 IMPRIM
#  ETAT DATE  SOIT JGD751,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD751$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002  : EDITION DES APPAREILS A COMMUT�S : ETAT JGD801                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAT PGM=DFSRRC00   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAT
       ;;
(GTL46LAT)
       m_CondExec ${EXABE},NE,YES 
# BE      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR7
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L07
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0025 IMPRIM
#  ETAT DATE  SOIT JGD801,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD801$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE SYNTHESE DE DESTOCKAGE  : JGD921                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LAX
       ;;
(GTL46LAX)
       m_CondExec ${EXABJ},NE,YES 
# BJ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR8
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L08
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0027 IMPRIM
#  ETAT DATE  SOIT JGD921,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD921$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE DESTOCKAGE FILIALE ETAT: JGD731                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LBA PGM=DFSRRC00   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LBA
       ;;
(GTL46LBA)
       m_CondExec ${EXABO},NE,YES 
# BO      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LR9
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L09
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0026 IMPRIM
#  ETAT DATE  SOIT JGD731,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD731$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002  : EDITION LISTE ANOMALIE DE LIVRAISON ETAT: JTL401                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LBD PGM=DFSRRC00   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LBD
       ;;
(GTL46LBD)
       m_CondExec ${EXABT},NE,YES 
# BT      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LRA
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L10
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0029 IMPRIM
#  ETAT DATE  SOIT JTL401,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JTL401$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE DESTOCKAGE GLOBAL  ETAT: JGD791                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46LBG PGM=DFSRRC00   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL46LBG
       ;;
(GTL46LBG)
       m_CondExec ${EXABY},NE,YES 
# BY      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46LRB
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46L11
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0030 IMPRIM
#  ETAT DATE  SOIT JGD791,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD791$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGL 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP COMREG   PGM=CZX3PEPI   **                                          
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
