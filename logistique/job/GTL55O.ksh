#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL55O.ksh                       --- VERSION DU 08/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGTL55 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/25 AT 11.06.26 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL55O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#   BTL055 : CREATION DES BONS DE REPRISES                                     
#   REPRISE: OUI SI ABEND                                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL55OA
       ;;
(GTL55OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL55OAA
       ;;
(GTL55OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES B.R. PLUS PROFILS LIV                                    
#         PROVENANT DE LA CHAINE DES TOURNEES DE LIVRAISON                     
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL916/F16.BTL001BO
# ******  TABLE DES ARTICLES                                                   
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE GENERALISEES (CARSP)                                           
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE DES MARQUES                                                    
#    RSGA22O  : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22O /dev/null
# ******  TABLE DES VENTES/ADRESSES                                            
#    RSGV02O  : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02O /dev/null
# ******  TABLE DES VENTES/REGLEMENT                                           
#    RSGV10O  : NAME=RSGV10O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
# ******  TABLE DES LIGNES DE VENTES ARTICLES                                  
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                        
#    RSGV15O  : NAME=RSGV15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15O /dev/null
# ******  TABLE DES VENTES/QUANTITES DISPONIBLES                               
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
# ******  FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FTL055 ${DATA}/PTEM/GTL55OAA.TL0055AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL055 
       JUMP_LABEL=GTL55OAB
       ;;
(GTL55OAB)
       m_CondExec 04,GE,GTL55OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 6 0                                                                 
# ********************************************************************         
#   CHARGEMENT DES BONS DE REPRISES   DANS LA D BASE IMPRESSION                
#   REPRISE: OUI APRES AVOIR VERIFIER LE BACKOUT CORTEX RGTL55O                
#                POUR LA RESTORE DE LA BASE IMPRESSION GENERALISEE             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL55OAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL55OAD
       ;;
(GTL55OAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BTL060,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL55OR1)                                             
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL55OR1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
# ******  FICHIER DES BONS DE REPRISES EXTRAIT                                 
       m_FileAssign -d SHR -g ${G_A1} FTL055 ${DATA}/PTEM/GTL55OAA.TL0055AO
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DO,MODE=U,REST=(YES,GTL55OP1)                       
# DIGVIP   FILE NAME=DI0000IO,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PNCGO.F16.DI0000DO
       m_FileAssign -d SHR DIGVIP ${DATA}/PNCGO.F16.DI0000IO
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL55O01
#                                                                              
# ******  FDATE JJMMSSAA DATE D EDITION DES BE                                 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGO 
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL55OZA
       ;;
(GTL55OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL55OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
