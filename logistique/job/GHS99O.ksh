#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHS99O.ksh                       --- VERSION DU 09/10/2016 00:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGHS99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/28 AT 11.42.34 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GHS99O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GHS99OA
       ;;
(GHS99OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2009/03/28 AT 11.42.34 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GHS99O                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'BISEAU INVT'                           
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GHS99OAA
       ;;
(GHS99OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
#    RSIE10O  : NAME=RSIE10O,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIE10O /dev/null
#    RSIE80O  : NAME=RSIE80O,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIE80O /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 FIC1 ${DATA}/PFIC/F16.IE10IO
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 FIC2 ${DATA}/PFIC/F16.IE80IO
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHS99OAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHS99OAB
       ;;
(GHS99OAB)
       m_CondExec 04,GE,GHS99OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BIV520                                                                
#  ------------                                                                
#   RAPPROCHEMENT DU STOCK PRET ET DE L'ENCOURS G.E.F                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS99OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHS99OAD
       ;;
(GHS99OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSIE10O  : NAME=RSIE10O,MODE=(U,N) - DYNAM=YES                            
# -X-RSIE10O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSIE10O /dev/null
#    RSIE80O  : NAME=RSIE80O,MODE=(U,N) - DYNAM=YES                            
# -X-RSIE80O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSIE80O /dev/null
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV520 
       JUMP_LABEL=GHS99OAE
       ;;
(GHS99OAE)
       m_CondExec 04,GE,GHS99OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
