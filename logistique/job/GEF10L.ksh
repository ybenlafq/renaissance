#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEF10L.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGEF10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/23 AT 10.54.44 BY BURTECA                      
#    STANDARDS: P  JOBSET: GEF10L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BEF000                                                                
#  ------------                                                                
#  LISTE COMPLETE DE L'ENCOURS FOURNISSEUR                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEF10LA
       ;;
(GEF10LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GEF10LAA
       ;;
(GEF10LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSHE00   : NAME=RSHE00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ------  FICHIER D'EDITION                                                    
# IEF000   REPORT SYSOUT=(9,IEF000),RECFM=FBA,LRECL=133                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IEF000 ${DATA}/PXX0/F61.IEF000AL
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 209 -t LSEQ -g +1 FEFCSV ${DATA}/PXX0/F61.BEF000AL.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF000 
       JUMP_LABEL=GEF10LAB
       ;;
(GEF10LAB)
       m_CondExec 04,GE,GEF10LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF005                                                                
#  ------------                                                                
#  CREATION DES FICHIERS D'EXTRACTION POUR EDITION                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAD
       ;;
(GEF10LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#    RSHE00   : NAME=RSHE00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00 /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FEF010 ${DATA}/PTEM/GEF10LAD.FEF010AL
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 194 -t LSEQ -g +1 FDFCSV ${DATA}/PXX0/F61.BEF005AL.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF005 
       JUMP_LABEL=GEF10LAE
       ;;
(GEF10LAE)
       m_CondExec 04,GE,GEF10LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEF010AL ENTRANT DANS LE BEF010                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAG
       ;;
(GEF10LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GEF10LAD.FEF010AL
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GEF10LAG.FEF010BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_72_8 72 CH 8
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_80_5 80 CH 5
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_PD_98_5 98 PD 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_80_5 ASCENDING,
   FLD_CH_72_8 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10LAH
       ;;
(GEF10LAH)
       m_CondExec 00,EQ,GEF10LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF010                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF010 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PV TTC                                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAJ PGM=BEF010     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAJ
       ;;
(GEF10LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A2} FEF010 ${DATA}/PTEM/GEF10LAG.FEF010BL
# ------  EDITION DE L'ETAT IEF010                                             
# IEF010   REPORT SYSOUT=(9,IEF010),RECFM=FBA,LRECL=133                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IEF010 ${DATA}/PXX0/F61.IEF010AL
       m_ProgramExec BEF010 
# ********************************************************************         
#  TRI DU FICHIER FEF010AL ENTRANT DANS LES PGMS BEF011 ET BEF012              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAM
       ;;
(GEF10LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GEF10LAD.FEF010AL
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GEF10LAM.FEF010CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_CH_78_2 78 CH 2
 /FIELDS FLD_CH_72_6 72 CH 6
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_CH_51_5 51 CH 5
 /KEYS
   FLD_CH_51_5 ASCENDING,
   FLD_CH_72_6 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_78_2 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10LAN
       ;;
(GEF10LAN)
       m_CondExec 00,EQ,GEF10LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF011                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF011 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAQ PGM=BEF011     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAQ
       ;;
(GEF10LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A4} FEF010 ${DATA}/PTEM/GEF10LAM.FEF010CL
# ------  EDITION DE L'ETAT IEF010                                             
# IEF011   REPORT SYSOUT=(9,IEF011),RECFM=FBA,LRECL=133                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IEF011 ${DATA}/PXX0/F61.IEF011AL
       m_ProgramExec BEF011 
# ********************************************************************         
#  PGM : BEF012                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF012 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAT PGM=BEF012     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAT
       ;;
(GEF10LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BEF005                                          
       m_FileAssign -d SHR -g ${G_A5} FEF010 ${DATA}/PTEM/GEF10LAM.FEF010CL
# ------  EDITION DE L'ETAT IEF010                                             
# IEF012   REPORT SYSOUT=(9,IEF012),RECFM=FBA,LRECL=133                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IEF012 ${DATA}/PXX0/F61.IEF012AL
       m_ProgramExec BEF012 
# ********************************************************************         
#  PGM : BEF013                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF013 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PRMP                                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LAX PGM=BEF013     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LAX
       ;;
(GEF10LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A6} FEF010 ${DATA}/PTEM/GEF10LAG.FEF010BL
# ------  EDITION DE L'ETAT IEF013                                             
# IEF013   REPORT SYSOUT=(9,IEF013),RECFM=FBA,LRECL=133                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IEF013 ${DATA}/PXX0/F61.IEF013AL
       m_ProgramExec BEF013 
# ********************************************************************         
#    CREATION DE L'ETAT DANS EOS                                               
# ********************************************************************         
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LBA PGM=IEBGENER   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LBA
       ;;
(GEF10LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A7} SYSUT1 ${DATA}/PXX0/F61.IEF000AL
       m_OutputAssign -c 9 -w IEF000 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10LBB
       ;;
(GEF10LBB)
       m_CondExec 00,EQ,GEF10LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LBD PGM=IEBGENER   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LBD
       ;;
(GEF10LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A8} SYSUT1 ${DATA}/PXX0/F61.IEF010AL
       m_OutputAssign -c 9 -w IEF010 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10LBE
       ;;
(GEF10LBE)
       m_CondExec 00,EQ,GEF10LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LBG PGM=IEBGENER   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LBG
       ;;
(GEF10LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A9} SYSUT1 ${DATA}/PXX0/F61.IEF011AL
       m_OutputAssign -c 9 -w IEF011 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10LBH
       ;;
(GEF10LBH)
       m_CondExec 00,EQ,GEF10LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LBJ
       ;;
(GEF10LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A10} SYSUT1 ${DATA}/PXX0/F61.IEF012AL
       m_OutputAssign -c 9 -w IEF012 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10LBK
       ;;
(GEF10LBK)
       m_CondExec 00,EQ,GEF10LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10LBM PGM=IEBGENER   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LBM
       ;;
(GEF10LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A11} SYSUT1 ${DATA}/PXX0/F61.IEF013AL
       m_OutputAssign -c 9 -w IEF013 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10LBN
       ;;
(GEF10LBN)
       m_CondExec 00,EQ,GEF10LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GEF10LZA
       ;;
(GEF10LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
