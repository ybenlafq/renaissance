#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL75P.ksh                       --- VERSION DU 08/10/2016 13:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGTL75 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/16 AT 11.36.25 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL75P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FTL076 (FICHIER DES VENTES DE LA VEILLE)                     
#  SUR : DATE LIVRAISON,CODE MAG,NUM DE VENTE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL75PA
       ;;
(GTL75PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL75PAA
       ;;
(GTL75PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DU NUMERO DE FOLIOS DES BL ET BR DANS CES 2 CHAINES                   
# **************************************                                       
#  SORTIN: FIC CREE LA VEILLE AVEC LES TOURNEES DU LENDEMAIN DONC SI           
#    U34 DANS LA JOURNEE JE DOIS TROUVER LES TOURNEES DE DEMAIN MATIN          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PTL0/F07.BTL076AP
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL75PAA.BTL075AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_14_8 14 CH 8
 /KEYS
   FLD_CH_14_8 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 40 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75PAB
       ;;
(GTL75PAB)
       m_CondExec 00,EQ,GTL75PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL075                                                                
# ********************************************************************         
#   GENERATION DU FICHIER DES VENTES EN ANOMALIE DE LIVRAISON PAR MAGA         
#   POUR UNE DATE DE LIVRAISON                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL75PAD
       ;;
(GTL75PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ADRESSES DE VENTES                                                
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES ENTETES  LIGNE DE VENTE MODE=I,I POUR EVITER DEADLOCK             
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#  TABLE DES LIGNES DE VENTE                                                   
#    RTGV11   : NAME=RSGV11,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES GENERALITES TOURNEES DE LIVRAISON                                 
#    RTTL01   : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01 /dev/null
#  TABLE DES DETAILS TOURNEES DE LIVRAISON                                     
#    RTTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL02 /dev/null
#  FICHIER DES TOURNEES PREVISIONNELLES : ON DOIT TROUVER DANS CE FIC          
#  LIVRAISONS EXCEPTIONNELLES ET SURTOUT 907LD2 1BLIDF,1BRIDF,1REIMS           
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL0/F07.BTL001AP
#  FICHIER LIVRAISON AVANT SAISIE SOIT FIC CREE LA VEILLE                      
       m_FileAssign -d SHR -g ${G_A1} FTL075 ${DATA}/PTEM/GTL75PAA.BTL075AP
#  FICHIER DES VENTES AVEC DDELIV DE DEMAIN                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 FTL076 ${DATA}/PTL0/F07.BTL076AP
#  FICHIER VENTE EN ANOMALIE DE LIVRAISON PAR MAGASIN                          
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FTL077 ${DATA}/PTEM/GTL75PAD.BTL077AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL075 
       JUMP_LABEL=GTL75PAE
       ;;
(GTL75PAE)
       m_CondExec 04,GE,GTL75PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTL077 (FICHIER DES VENTES EN ANOMALIE)                      
#  SUR : CODE PROFIL,CODE MAG,CODE TOURNEE,NUM DE VENTE                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL75PAG
       ;;
(GTL75PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GTL75PAD.BTL077AP
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL75PAG.BTL078AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_34_1 34 CH 1
 /FIELDS FLD_CH_43_6 43 CH 6
 /KEYS
   FLD_CH_43_6 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_34_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75PAH
       ;;
(GTL75PAH)
       m_CondExec 00,EQ,GTL75PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL080                                                                
# ********************************************************************         
#   EDITION DES VENTES EN ANOMALIE DE LIVRAISON                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL75PAJ
       ;;
(GTL75PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ADRESSES DE VENTES                                                
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES GENERALITES TOURNEES DE LIVRAISON                                 
#    RTTL01   : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01 /dev/null
#  FICHIER LIVRAISON AVANT SAISIE                                              
       m_FileAssign -d SHR -g ${G_A3} FTL080 ${DATA}/PTEM/GTL75PAG.BTL078AP
#  FICHIER EDITION                                                             
       m_OutputAssign -c 9 -w ITL080 ITL080
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL080 
       JUMP_LABEL=GTL75PAK
       ;;
(GTL75PAK)
       m_CondExec 04,GE,GTL75PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL75PZA
       ;;
(GTL75PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL75PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
