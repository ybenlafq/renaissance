#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL75M.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGTL75 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/01/27 AT 10.58.51 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL75M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FTL076 (FICHIER DES VENTES DE LA VEILLE)                     
#  SUR : DATE LIVRAISON,CODE MAG,NUM DE VENTE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL75MA
       ;;
(GTL75MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL75MAA
       ;;
(GTL75MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DES NUMEROS DE FOLIOS        *                                        
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PTL989/F89.BTL076AM
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PTEM/GTL75MAA.BTL075AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_14_8 14 CH 8
 /KEYS
   FLD_CH_14_8 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 40 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75MAB
       ;;
(GTL75MAB)
       m_CondExec 00,EQ,GTL75MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL075                                                                
# ********************************************************************         
#   GENERATION DU FICHIER DES VENTES EN ANOMALIE DE LIVRAISON PAR MAGA         
#   POUR UNE DATE DE LIVRAISON                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL75MAD
       ;;
(GTL75MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
# FDATE    DATA CLASS=VAR,PARMS=FDATE,MBR=FDATE                                
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02M /dev/null
# ******  TABLE DES ENTETES LIGNE DE VENTE                                     
#    RTGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10M /dev/null
# ******  TABLE DES LIGNES DE VENTE                                            
#    RTGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11M /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01M  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01M /dev/null
# ******  TABLE DES DETAILS TOURNEES DE LIVRAISON                              
#    RTTL02M  : NAME=RSTL02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02M /dev/null
# ******  FICHIER DES TOURNEES PREVISIONNELLES                                 
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL989/F89.BTL001AM
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A1} FTL075 ${DATA}/PTEM/GTL75MAA.BTL075AM
# ******  FICHIER VENTE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FTL076 ${DATA}/PTL989/F89.BTL076AM
# ******  FICHIER VENTE EN ANOMALIE DE LIVRAISON PAR MAGASIN                   
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FTL077 ${DATA}/PTEM/GTL75MAD.BTL077AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL075 
       JUMP_LABEL=GTL75MAE
       ;;
(GTL75MAE)
       m_CondExec 04,GE,GTL75MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTL077 (FICHIER DES VENTES EN ANOMALIE)                      
#  SUR : CODE PROFIL,CODE MAG,CODE TOURNEE,NUM DE VENTE                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL75MAG
       ;;
(GTL75MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GTL75MAD.BTL077AM
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/GTL75MAG.BTL078AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_34_1 34 CH 1
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_43_6 43 CH 6
 /KEYS
   FLD_CH_43_6 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_34_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75MAH
       ;;
(GTL75MAH)
       m_CondExec 00,EQ,GTL75MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL080                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#   EDITION DES VENTES EN ANOMALIE DE LIVRAISON                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75MAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL75MAJ
       ;;
(GTL75MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02M  : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02M /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01M  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01M /dev/null
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A3} FTL080 ${DATA}/PTEM/GTL75MAG.BTL078AM
# ******  FICHIER EDITION                                                      
       m_OutputAssign -c 9 -w ITL080 ITL080
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL080 
       JUMP_LABEL=GTL75MAK
       ;;
(GTL75MAK)
       m_CondExec 04,GE,GTL75MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL75MZA
       ;;
(GTL75MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL75MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
