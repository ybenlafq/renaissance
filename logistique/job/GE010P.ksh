#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GE010P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGE010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/30 AT 11.59.37 BY BURTECW                      
#    STANDARDS: P  JOBSET: GE010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGE010                                                                
#  -------------                                                               
#   EXTRACTION DES INFORMATIONS CONCERNANT LES EMPLACEMENTS                    
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GE010PA
       ;;
(GE010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GE010PAA
       ;;
(GE010PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FICOUT ${DATA}/PTEM/GE010PAA.GE0010AP
# ******  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGE25   : NAME=RSGE25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE25 /dev/null
#    RTGE30   : NAME=RSGE30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE30 /dev/null
#    RTGE35   : NAME=RSGE35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE35 /dev/null
#    RTGE36   : NAME=RSGE36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE36 /dev/null
#    RTGE45   : NAME=RSGE45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE45 /dev/null
# ******  CARTE FDATE                                                          
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGE010 
       JUMP_LABEL=GE010PAB
       ;;
(GE010PAB)
       m_CondExec 04,GE,GE010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EXTRACTION DES EMPLACEMENTS POUR ETAT BGE015               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE010PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GE010PAD
       ;;
(GE010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GE010PAA.GE0010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GE010PAD.GE0010BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "907090"
 /FIELDS FLD_CH_70_1 70 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_45_4 45 CH 4
 /FIELDS FLD_CH_49_8 49 CH 8
 /FIELDS FLD_CH_39_6 39 CH 6
 /CONDITION CND_1 FLD_CH_39_6 EQ CST_1_8 
 /KEYS
   FLD_CH_39_6 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_45_4 ASCENDING,
   FLD_CH_70_1 ASCENDING,
   FLD_CH_49_8 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GE010PAE
       ;;
(GE010PAE)
       m_CondExec 00,EQ,GE010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DE LA LISTE PERMETTENT LE BACKUP DE LA GESTION                      
#  DES EMPLACEMENTS   ETAT ==> BGE015                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE010PAG PGM=BGE015     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GE010PAG
       ;;
(GE010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g ${G_A2} FICIN ${DATA}/PTEM/GE010PAD.GE0010BP
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w BGE015 IMPR
       m_ProgramExec BGE015 
# ********************************************************************         
#  TRI DU FICHIER D EXTRACTION DES EMPLACEMENTS POUR ETAT BGE016               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE010PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GE010PAJ
       ;;
(GE010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GE010PAA.GE0010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GE010PAJ.GE0016AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "SOL"
 /DERIVEDFIELD CST_3_12 "VRAC"
 /DERIVEDFIELD CST_1_15 "907090"
 /FIELDS FLD_CH_39_6 39 CH 6
 /FIELDS FLD_CH_49_8 49 CH 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_70_1 70 CH 1
 /FIELDS FLD_CH_45_4 45 CH 4
 /CONDITION CND_1 FLD_CH_45_4 EQ CST_1_8 OR FLD_CH_45_4 EQ CST_3_12 
 /CONDITION CND_2 FLD_CH_39_6 EQ CST_1_15 
 /KEYS
   FLD_CH_39_6 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_45_4 ASCENDING,
   FLD_CH_70_1 ASCENDING,
   FLD_CH_49_8 ASCENDING
 /INCLUDE CND_1
 /OMIT CND_2
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GE010PAK
       ;;
(GE010PAK)
       m_CondExec 00,EQ,GE010PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DE LA LISTE PERMETTENT LE BACKUP DE LA GESTION                      
#  DES EMPLACEMENTS   ETAT ==> BGE016 (SOL ET VRAC)                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE010PAM PGM=BGE016     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GE010PAM
       ;;
(GE010PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g ${G_A4} FICIN ${DATA}/PTEM/GE010PAJ.GE0016AP
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w BGE016 IMPR
       m_ProgramExec BGE016 
# ********************************************************************         
#  TRI DU FICHIER D EXTRACTION DES EMPLACEMENTS POUR ETAT BGE017               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE010PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GE010PAQ
       ;;
(GE010PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GE010PAA.GE0010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GE010PAQ.GE0017AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_12 "VRAC"
 /DERIVEDFIELD CST_1_8 "RACK"
 /DERIVEDFIELD CST_1_15 "907090"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_49_8 49 CH 8
 /FIELDS FLD_CH_45_4 45 CH 4
 /FIELDS FLD_CH_70_1 70 CH 1
 /FIELDS FLD_CH_39_6 39 CH 6
 /CONDITION CND_2 FLD_CH_39_6 EQ CST_1_15 
 /CONDITION CND_1 FLD_CH_45_4 EQ CST_1_8 OR FLD_CH_45_4 EQ CST_3_12 
 /KEYS
   FLD_CH_39_6 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_45_4 ASCENDING,
   FLD_CH_70_1 ASCENDING,
   FLD_CH_49_8 ASCENDING
 /INCLUDE CND_1
 /OMIT CND_2
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GE010PAR
       ;;
(GE010PAR)
       m_CondExec 00,EQ,GE010PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DE LA LISTE PERMETTENT LE BACKUP DE LA GESTION                      
#  DES EMPLACEMENTS   ETAT ==> BGE017 (SOL ET VRAC)                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE010PAT PGM=BGE017     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GE010PAT
       ;;
(GE010PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g ${G_A6} FICIN ${DATA}/PTEM/GE010PAQ.GE0017AP
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w BGE017 IMPR
       m_ProgramExec BGE017 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GE010PZA
       ;;
(GE010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GE010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
