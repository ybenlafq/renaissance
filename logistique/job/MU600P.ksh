#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU600P.ksh                       --- VERSION DU 09/10/2016 00:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU600 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/17 AT 08.49.05 BY BURTECA                      
#    STANDARDS: P  JOBSET: MU600P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  PGM BMU600 : FACTURATION INTER-FILIALES, G�N�RATION DES MOUVEMENTS          
#               DE STOCKS                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
#                                                                              
#  A T T E N T I O N :                                                         
#  -----------------                                                           
#  EN CAS DE PLANTAGE, IL EST IMPORTANT DE REPRENDRE CETTE CHAINE              
#             LA NUIT  EN RESPECTANT BIEN LE FDATE                             
#  LE 170615  SURTOUT NE JAMAIS LA REPRENDRE LA JOURN�E CICS OUVERT            
# ********************************************************************         
# ********************************************************************         
#                     A T T E N T I O N :                                      
#                     -----------------                                        
#        NE JAMAIS REPRENDRE LE MARDI SI LA GFI20P A DEJA                      
#                 TOURN�E ET LA METTRE TERMIN�E                                
# ********************************************************************         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU600PA
       ;;
(MU600PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=MU600PAA
       ;;
(MU600PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER PARAM�TRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#                                                                              
# *****   TABLES EN ECRITURE                                                   
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11X,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU600 
       JUMP_LABEL=MU600PAB
       ;;
(MU600PAB)
       m_CondExec 04,GE,MU600PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
