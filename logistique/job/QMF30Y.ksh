#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF30Y.ksh                       --- VERSION DU 08/10/2016 22:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYQMF30 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/05/03 AT 10.38.43 BY PREPA3                       
#    STANDARDS: P  JOBSET: QMF30Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='LYON'                                                              
# ********************************************************************         
#   LISTE STATS TOUS MAGASINS RUBRIQUE AGREGEE QMF111TA                        
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF30YA
       ;;
(QMF30YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       RUN=${RUN}
       JUMP_LABEL=QMF30YAA
       ;;
(QMF30YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF111TA DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN Q111TA (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F111TA
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAB
       ;;
(QMF30YAB)
       m_CondExec 04,GE,QMF30YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS TOUS MAGASINS DETAIL PAR MAGASIN QMF111TD                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAD
       ;;
(QMF30YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF111TD DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q111TD (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F111TD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAE
       ;;
(QMF30YAE)
       m_CondExec 04,GE,QMF30YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS SURFACE EGALE RUBRIQUE AGREGEE                                 
#            ETAT PLUS UTILISE CGA 061198                                      
# ********************************************************************         
# AAK  STEP PGM=IKJEFT01,PATTERN=EO4,PATKW=((L,QMF111EA),(M,QMF30Y3),-         
#                (S,RDAR))                                                     
# QMFPARM  DATA CLASS=VAR,PARMS=QMF111Y3,MBR=QMF30Y3                           
# ********************************************************************         
#   LISTE STATS SURFACE EGALE RUBRIQUE DETAILLEE QMF111ED                      
#            ETAT PLUS UTILISE CGA 061198                                      
# ********************************************************************         
# AAP  STEP PGM=IKJEFT01,PATTERN=EO4,PATKW=((L,QMF111ED),(M,QMF30Y4),-         
#                (S,RDAR))                                                     
# QMFPARM  DATA CLASS=VAR,PARMS=QMF111Y4,MBR=QMF30Y4                           
# ********************************************************************         
#   LISTE STATS GROUPE DE MAGASIN RUBRIQUE AGREGEE QMF111GA                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAG
       ;;
(QMF30YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF111GA DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q111GA (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&NSOC='$DRADEP_1_3' FORM=F111GA
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y5 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAH
       ;;
(QMF30YAH)
       m_CondExec 04,GE,QMF30YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS GROUPE DE MAGASIN RUBRIQUE DETAILLEE QMF111GD                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAJ
       ;;
(QMF30YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF111GD DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q111GD (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&NSOC='$DRADEP_1_3' FORM=F111GD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y6 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAK
       ;;
(QMF30YAK)
       m_CondExec 04,GE,QMF30YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAG RUBRIQUE AGREGEE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAM
       ;;
(QMF30YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF111DA DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q111DA (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F111DA
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAN
       ;;
(QMF30YAN)
       m_CondExec 04,GE,QMF30YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAG RUBRIQUE DETAILLEE                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAQ
       ;;
(QMF30YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF111DD DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q111DD (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F111DD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y8 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAR
       ;;
(QMF30YAR)
       m_CondExec 04,GE,QMF30YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS SURFACE EGALE RUBRIQUE AGREGEE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAT
       ;;
(QMF30YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF112EA DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q112EA (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F112EA
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y9 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAU
       ;;
(QMF30YAU)
       m_CondExec 04,GE,QMF30YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS SURFACE EGALE RUBRIQUE DETAILLEE                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YAX
       ;;
(QMF30YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF112ED DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q112ED (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F112ED
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y10 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YAY
       ;;
(QMF30YAY)
       m_CondExec 04,GE,QMF30YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAGS RUBRIQUE AGREGEE                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YBA
       ;;
(QMF30YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF112DA DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q112DA (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F112DA
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y11 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YBB
       ;;
(QMF30YBB)
       m_CondExec 04,GE,QMF30YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAGS RUBRIQUE DETAILLEE                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YBD
       ;;
(QMF30YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF112DD DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q112DD (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F112DD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y12 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YBE
       ;;
(QMF30YBE)
       m_CondExec 04,GE,QMF30YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAGS TOUTES RUBRIQUE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YBG
       ;;
(QMF30YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF113DT DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q113DT (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F113DT
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y13 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YBH
       ;;
(QMF30YBH)
       m_CondExec 04,GE,QMF30YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAGS RUBRIQUE DETAILLEE                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF30YBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF30YBJ
       ;;
(QMF30YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF113DD DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q113DD (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F113DD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF30Y14 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF30YBK
       ;;
(QMF30YBK)
       m_CondExec 04,GE,QMF30YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS GROUPE DE MAGS RUBRIQUE DETAILLEE  EXEC NO                     
#                     EXEC EN COMMENTAIRE LE 191297    CGA                     
# ********************************************************************         
# *ACS  STEP PGM=IKJEFT01,PATTERN=EO4,PARMS=((L,QMF113GD),                     
# *               (M,QMF30Y15),(S,RDAR))                                       
# *QMFPARM  DATA CLASS=VAR,PARMS=QMF113Y3,MBR=QMF30Y15                         
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
