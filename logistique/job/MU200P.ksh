#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU200P.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/09/16 AT 16.05.48 BY BURTECA                      
#    STANDARDS: P  JOBSET: MU200P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FOCNTION DE LA CARTE FFONC FIC STOCK ET STAT           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU200PA
       ;;
(MU200PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MU200PAA
       ;;
(MU200PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  PARAMETRAGE PGM                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MU200PAA
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/MU200PAA
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 59 STOCK AS400                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 FTS077 ${DATA}/PXX0/F07.BNM177AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=MU200PAB
       ;;
(MU200PAB)
       m_CondExec 04,GE,MU200PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DE REMONTEE STOCK 36                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAD
       ;;
(MU200PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BNM177AP
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200PAD.BNM177BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 59 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PAE
       ;;
(MU200PAE)
       m_CondExec 00,EQ,MU200PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BMU200 DE LA VEILLE                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAG
       ;;
(MU200PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DE LOAD DE LA VEILLE                                         
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200PAG.BMU200BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_6 8 CH 6
 /KEYS
   FLD_CH_8_6 ASCENDING,
   FLD_CH_1_7 ASCENDING
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PAH
       ;;
(MU200PAH)
       m_CondExec 00,EQ,MU200PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU200 : CREE FICHIER DE LOAD TABLE RTGS36 (STOCK ET TRANSIT MAG)           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAJ
       ;;
(MU200PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  TABLE RTGA10                                                         
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  FICHIER DE LA VEILLE TRIE                                            
       m_FileAssign -d SHR -g ${G_A2} RTGS36A ${DATA}/PTEM/MU200PAG.BMU200BP
# ******  FICHIER DES STOCK ET TRANSIT DES MAGS                                
       m_FileAssign -d SHR -g ${G_A3} FMU200J ${DATA}/PTEM/MU200PAD.BNM177BP
# ******  FICHIER DU JOUR POUR CHARGEMENT DE LA RTGS36                         
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 RTGS36J ${DATA}/PXX0/F07.BMU200AP
# ******  FICHIER DU JOUR POUR AIDE A LA VENTE                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 FMU200V ${DATA}/PXX0/F07.BGS907MP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU200 
       JUMP_LABEL=MU200PAK
       ;;
(MU200PAK)
       m_CondExec 04,GE,MU200PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LE LOAD RTGS36                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAM
       ;;
(MU200PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.BMU200AP
# ****    FICHIER ISSU DE MU200X                                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BMU200CY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BMU200CM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BMU200CD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BMU200CL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BMU200CO
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BMU200CX
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200PAM.BMU200CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_9 8 CH 9
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_17_7 17 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_9 ASCENDING,
   FLD_CH_17_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PAN
       ;;
(MU200PAN)
       m_CondExec 00,EQ,MU200PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUIVANT L INDEX CLUSTER POUR LOAD DE LA RTGS36                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAQ
       ;;
(MU200PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/MU200PAM.BMU200CP
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200PAQ.BMU200DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_20_7 20 CH 7
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_20_7 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PAR
       ;;
(MU200PAR)
       m_CondExec 00,EQ,MU200PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAT
       ;;
(MU200PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/MU200PAQ.BMU200DP
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200PAT.BMU200EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_3 8 CH 3
 /KEYS
   FLD_BI_8_3 ASCENDING
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PAU
       ;;
(MU200PAU)
       m_CondExec 00,EQ,MU200PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER UNLOAD SOUS TABLES MGNMD TTES FILIALES                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MU200PAX
       ;;
(MU200PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER EN PROVENANCE DE MU200F                                      
# *****   DE J OU J-1 SI PB SUR MU200F                                         
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.UNGA01F
       m_FileAssign -d NEW,CATLG,DELETE -r 95 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200PAX.BMU200FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 1 CH 3
 /KEYS
   FLD_BI_1_3 ASCENDING
 /* Record Type = F  Record Length = 95 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PAY
       ;;
(MU200PAY)
       m_CondExec 00,EQ,MU200PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU360 : CREATION DU FICHIER EN VUE DU LOAD DE LA RTGS36                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PBA PGM=BGS360     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MU200PBA
       ;;
(MU200PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES STOCK ET TRANSIT DES MAGS                                
       m_FileAssign -d SHR -g ${G_A7} FGS36 ${DATA}/PTEM/MU200PAT.BMU200EP
# ******  FICHIER DE L'UNLOAD TRI�                                             
       m_FileAssign -d SHR -g ${G_A8} FNMD ${DATA}/PTEM/MU200PAX.BMU200FP
# ******  FICHIER PREPARATOIRE POUR CHARGEMENT DE LA RTGS36                    
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 FGS36S ${DATA}/PTEM/MU200PBA.BGS360AP
       m_ProgramExec BGS360 
# ********************************************************************         
#  TRI POUR GENERATION DU FICHIER LOAD RTGS36                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=MU200PBD
       ;;
(MU200PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER PREPARATOIRE POUR CHARGEMENT DE LA RTGS36                    
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/MU200PBA.BGS360AP
# ******  FICHIER POUR CHARGEMENT DE LA RTGS36 TRIE                            
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.GS36RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_20_7 20 CH 7
 /FIELDS FLD_BI_14_3 14 CH 3
 /FIELDS FLD_BI_8_3 8 CH 3
 /FIELDS FLD_BI_1_7 1 CH 7
 /FIELDS FLD_BI_11_3 11 CH 3
 /KEYS
   FLD_BI_1_7 ASCENDING,
   FLD_BI_11_3 ASCENDING,
   FLD_BI_8_3 ASCENDING,
   FLD_BI_14_3 ASCENDING,
   FLD_BI_20_7 ASCENDING
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200PBE
       ;;
(MU200PBE)
       m_CondExec 00,EQ,MU200PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTGS36 (LGT)                                         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MU200PBG
       ;;
(MU200PBG)
       m_CondExec ${EXABY},NE,YES 
# ******  TABLE DES STOCKS MAGS                                                
#    RSGS36   : NAME=RSGS36,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  FICHIER DE LOAD  907                                                 
       m_FileAssign -d SHR -g ${G_A10} SYSREC ${DATA}/PXX0/F07.RELOAD.GS36RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS               
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU200PBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/MU200P_MU200PBG_RTGS36.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=MU200PBH
       ;;
(MU200PBH)
       m_CondExec 04,GE,MU200PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   AJOUT STEP POUR JALON ATOS                                                 
#   PERMET DE VERIFIER QUE LES JOBS NE SONT PAS EN RETARD                      
#   REPRISE : OUI SI PROBLEME DANS CE STEP METTRE TERMIN� SOUS PLAN _A          
#   CONDITION QUE CE SOIT LE DERNIER STEP DE LA CHAINE                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200PBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MU200PZA
       ;;
(MU200PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU200PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
