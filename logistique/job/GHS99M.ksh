#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHS99M.ksh                       --- VERSION DU 08/10/2016 12:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGHS99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/28 AT 11.42.25 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GHS99M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GHS99MA
       ;;
(GHS99MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2009/03/28 AT 11.42.25 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GHS99M                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'BISEAU INVT'                           
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GHS99MAA
       ;;
(GHS99MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
#    RSIE10   : NAME=RSIE10M,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIE10 /dev/null
#    RSIE80   : NAME=RSIE80M,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIE80 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 FIC1 ${DATA}/PFIC/F89.IE10IM
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 FIC2 ${DATA}/PFIC/F89.IE80IM
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHS99MAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHS99MAB
       ;;
(GHS99MAB)
       m_CondExec 04,GE,GHS99MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BIV520                                                                
#  ------------                                                                
#   RAPPROCHEMENT DU STOCK PRET ET DE L'ENCOURS G.E.F                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHS99MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHS99MAD
       ;;
(GHS99MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSIE10   : NAME=RSIE10M,MODE=(U,N) - DYNAM=YES                            
# -X-RSIE10M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSIE10 /dev/null
#    RSIE80   : NAME=RSIE80M,MODE=(U,N) - DYNAM=YES                            
# -X-RSIE80M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSIE80 /dev/null
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV520 
       JUMP_LABEL=GHS99MAE
       ;;
(GHS99MAE)
       m_CondExec 04,GE,GHS99MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
