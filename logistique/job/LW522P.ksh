#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LW522P.ksh                       --- VERSION DU 08/10/2016 22:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLW522 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/01 AT 11.07.53 BY BURTECA                      
#    STANDARDS: P  JOBSET: LW522P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BLW520 : INTEGRATION IMAGE DE STOCKS DANS LA TABLE RTLW12                  
#   REPRISE: OUI SI FIN ANORMALE                                               
#   ATTENTION LE FPARAM DOIT ETRE CREE POUR TOURNER LA SUITE                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=LW522PA
       ;;
(LW522PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2015/06/01 AT 11.07.53 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: LW522P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'COMPAR IMAG STO'                       
# *                           APPL...: REPPARIS                                
# *                           WAIT...: LW522P                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=LW522PAA
       ;;
(LW522PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPEND NCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *******                                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSIE10   : NAME=RSIE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE10 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSLW11   : NAME=RSLW11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW11 /dev/null
#    RSLW99   : NAME=RSLW99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW99 /dev/null
#                                                                              
#    RSLW12   : NAME=RSLW12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW12 /dev/null
#                                                                              
       m_FileAssign -d SHR -g +0 FLW520 ${DATA}/PXX0/F07.BLW520AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPARAM ${DATA}/PXX0/F07.FLW520DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW520 
       JUMP_LABEL=LW522PAB
       ;;
(LW522PAB)
       m_CondExec 04,GE,LW522PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BLW521 : EXTRAC STOCK DE LA GS10 ET LW12                                   
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW522PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LW522PAD
       ;;
(LW522PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FPARAM ${DATA}/PXX0/F07.FLW520DP
# *******                                                                      
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#    RSLW12   : NAME=RSLW12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW12 /dev/null
#    RSLW99   : NAME=RSLW99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW99 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FLW521 ${DATA}/PXX0/F07.FLW521DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW521 
       JUMP_LABEL=LW522PAE
       ;;
(LW522PAE)
       m_CondExec 04,GE,LW522PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW522PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LW522PAG
       ;;
(LW522PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.FLW521DP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LW522PAG.FLW521EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_106_6 106 PD 6
 /FIELDS FLD_CH_44_62 44 CH 62
 /FIELDS FLD_PD_118_6 118 PD 06
 /FIELDS FLD_PD_112_6 112 PD 06
 /FIELDS FLD_CH_14_10 14 CH 10
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 07 CH 07
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_44_62 ASCENDING,
   FLD_CH_14_10 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_106_6,
    TOTAL FLD_PD_112_6,
    TOTAL FLD_PD_118_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LW522PAH
       ;;
(LW522PAH)
       m_CondExec 00,EQ,LW522PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BLW522 : COMPARAISON DES STOCKS ET CREATION FICHIER XLS                    
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW522PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LW522PAJ
       ;;
(LW522PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FPARAM ${DATA}/PXX0/F07.FLW520DP
# *******                                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSLI20   : NAME=RSLI20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI20 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FLW522I ${DATA}/PTEM/LW522PAG.FLW521EP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FLW522O ${DATA}/PXX0/F07.FLW522AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW522 
       JUMP_LABEL=LW522PAK
       ;;
(LW522PAK)
       m_CondExec 04,GE,LW522PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# PUT 'PXX0.F07.FLW522AP(0)' +                                                 
# ETAT_INVENTAIRE_907120_&YR4.&LMON.&LDAY..CSV                                 
# QUIT                                                                         
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FTLW522P,MODE=O                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=FTP,PARM='(EXIT'                                          
# *                                                                            
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
#          DATAEND                                                             
#          FILE  NAME=FTLW522P,MODE=I                                          
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LW522PZA
       ;;
(LW522PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW522PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
