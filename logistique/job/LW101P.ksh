#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LW101P.ksh                       --- VERSION DU 17/10/2016 18:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLW101 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/11 AT 10.59.13 BY BURTEC2                      
#    STANDARDS: P  JOBSET: LW101P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BLW100 : CONSTITUTION DE L INTERFACE ARTICLES ET UNITES LOGISTIQUE         
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LW101PA
       ;;
(LW101PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+3'}
       G_A9=${G_A9:-'+3'}
       RUN=${RUN}
       JUMP_LABEL=LW101PAA
       ;;
(LW101PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES EN LECTURE                                                           
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#    RSGA55   : NAME=RSGA55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA55 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSLW10   : NAME=RSLW10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW10 /dev/null
#  TABLES EN MAJ                                                               
#    RSLW00   : NAME=RSLW00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW00 /dev/null
#    RSLW01   : NAME=RSLW01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW01 /dev/null
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#    RSLW99   : NAME=RSLW99,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW99 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-507 -t LSEQ -g +1 FART00 ${DATA}/PXX0/F07.BLW100AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW100 
       JUMP_LABEL=LW101PAB
       ;;
(LW101PAB)
       m_CondExec 04,GE,LW101PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAD
       ;;
(LW101PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BLW100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-507 -t LSEQ -g +1 SORT095 ${DATA}/PXX0/F07.BLW100RP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-507 -t LSEQ -g +1 SORT090 ${DATA}/PTEM/LW100PAD.BLW100SP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-507 -t LSEQ -g +1 SORT120 ${DATA}/PTEM/LW100PAD.BLW100TP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "095"
 /DERIVEDFIELD CST_1_6 "090"
 /DERIVEDFIELD CST_1_9 "120"
 /FIELDS FLD_CH_5_3 5 CH 3
 /CONDITION CND_1 FLD_CH_5_3 EQ CST_1_3 
 /CONDITION CND_2 FLD_CH_5_3 EQ CST_1_6 
 /CONDITION CND_3 FLD_CH_5_3 EQ CST_1_9 
 /COPY
 /MT_OUTFILE_ASG SORT095
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORT090
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORT120
 /INCLUDE CND_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LW101PAE
       ;;
(LW101PAE)
       m_CondExec 00,EQ,LW101PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   EASYTREAVE 907095                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAG PGM=EZTPA00    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAG
       ;;
(LW101PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FILEA ${DATA}/PXX0/F07.BLW100RP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW100BP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW101PAG
       m_FileSort -s SYSIN
# ********************************************************************         
#   EASYTREAVE 907090                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAJ PGM=EZTPA00    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAJ
       ;;
(LW101PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FILEA ${DATA}/PTEM/LW100PAD.BLW100SP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW100CP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW101PAJ
       m_FileSort -s SYSIN
# ********************************************************************         
#   EASYTREAVE 907120                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAM PGM=EZTPA00    ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAM
       ;;
(LW101PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FILEA ${DATA}/PTEM/LW100PAD.BLW100TP
       m_FileAssign -d NEW,CATLG,DELETE -r 1-504 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.BLW100DP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/LW101PAM
       m_FileSort -s SYSIN
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW101P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAQ
       ;;
(LW101PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/LW101PAQ.FTLW101P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW101P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAT
       ;;
(LW101PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW101PAQ.FTLW101P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW101P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LW101PAX
       ;;
(LW101PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A6} SYSOUT ${DATA}/PTEM/LW101PAQ.FTLW101P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW101P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LW101PBA
       ;;
(LW101PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PBA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW101PAQ.FTLW101P(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLW101P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LW101PBD
       ;;
(LW101PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A8} SYSOUT ${DATA}/PTEM/LW101PAQ.FTLW101P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLW101P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW101PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LW101PBG
       ;;
(LW101PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PBG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LW101PAQ.FTLW101P(+3),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LW101PZA
       ;;
(LW101PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW101PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
