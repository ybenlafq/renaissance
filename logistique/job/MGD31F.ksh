#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MGD31F.ksh                       --- VERSION DU 17/10/2016 18:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFMGD31 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/15 AT 10.11.21 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MGD31F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE :                                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MGD31FA
       ;;
(MGD31FA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2015/06/15 AT 10.11.21 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: MGD31F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'RECUP FIC MGD31'                       
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MGD31FAA
       ;;
(MGD31FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FQC26MAD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FQC26MAL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FQC26MAM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FQC26MAO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FQC26MAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FQC26MAY
       m_FileAssign -d NEW,CATLG,DELETE -r 1000 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FQC26MCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MGD31FAB
       ;;
(MGD31FAB)
       m_CondExec 00,EQ,MGD31FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGD31FAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MGD31FAD
       ;;
(MGD31FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGD31FAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/MGD31FAD.FTMGD31F
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGD31FAG PGM=JVMLDM76   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MGD31FAG
       ;;
(MGD31FAG)
       m_CondExec ${EXAAK},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F99.FQC26MCP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F99.MGD31ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGD31FAG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATTAGE SYSIN ENVOI FTP                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGD31FAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MGD31FAJ
       ;;
(MGD31FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/MGD31FAD.FTMGD31F
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MGD31FAJ.FTMGD32F
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_2_6  "PXX0.F99.MGD31ZIP(0)"
 /DERIVEDFIELD CST_0_4  "PUT "
 /DERIVEDFIELD CST_BLANK_3_7  X"27"
 /DERIVEDFIELD CST_4_8  " "
 /DERIVEDFIELD CST_BLANK_1_5  X"27"
 /DERIVEDFIELD CST_1_3 "VTEMGD"
 /FIELDS FLD_CH_13_6 13 CH 6
 /CONDITION CND_1 FLD_CH_13_6 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
 /* MT_ERROR (unknown field) ',X'7D',C' ' */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_4,CST_BLANK_1_5,CST_2_6,CST_BLANK_3_7,CST_4_8
 /* MT_ERROR (unknown field) ',X'7D',C' ' */
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MGD31FAK
       ;;
(MGD31FAK)
       m_CondExec 00,EQ,MGD31FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTMGD31F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGD31FAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MGD31FAM
       ;;
(MGD31FAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/MGD31FAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.MGD31FAJ.FTMGD32F(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MGD31FZA
       ;;
(MGD31FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGD31FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
