#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL46D.ksh                       --- VERSION DU 08/10/2016 23:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL46 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.14.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL46D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#    IMPERATIF :  CETTE CHAINE DOIT ETRE EXECUTEE IMPERATIVEMENT               
#    -=-=-=-=-    CAR LES BONS DE LIVRAISON SONT IMPERATIFS POUR               
#                 LE DEPOT.                                                    
#                                                                              
# ********************************************************************         
#                                                                              
# **--USER='DPM'                                                               
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B.R DANS EOS                                        
# ********************************************************************         
#   ETAT JTL021 DESTINATION 000000000 B.R SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL022 DESTINATION 000000000 B.R AVEC CODE BARRE                      
# ********************************************************************         
#   REPRISE:OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL46DA
       ;;
(GTL46DA)
       DATE=${DATEJ}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       RUN=${RUN}
       UPSI=${UPSI:-''}
       USER=${USER:-''}
       JUMP_LABEL=GTL46DAA
       ;;
(GTL46DAA)
       m_CondExec ${EXAAA},NE,YES 
# AA      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    CHARGEMENT DANS DISPATCH DES      *                                       
#    LISTES DE DESTOCKAGES             *                                       
# **************************************                                       
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DR5
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D01
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
#                                                                              
       m_OutputAssign -c 9 -w BR IMPRIM
#  ETAT DATE SOIT JTL021,AAMMJJ,000000000  000000000 = DEST DS IG50            
       m_FileAssign -i SYSIN
JTL021$VDATEJ__AAMMJJ000000000
JTL022$VDATEJ__AAMMJJ000000000
JTL023$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  B I G 0 0 2                                                                 
# ********************************************************************         
#  CHARGEMENT DU JTL301 DANS DISPATCH                                          
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAD
       ;;
(GTL46DAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DR6
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D02
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0023 IMPRIM
#  ETAT DATE  SOIT JTL301,AAMMJJ,000000000                                     
       m_FileAssign -i SYSIN
JTL301$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002  : EDITION DES APPAREILS A COMMUT�S : ETAT JGD751                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAG
       ;;
(GTL46DAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DR7
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D03
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0024 IMPRIM
#  ETAT DATE  SOIT JGD751,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD751$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002  : EDITION DES APPAREILS A COMMUT�S : ETAT JGD801                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAJ PGM=DFSRRC00   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAJ
       ;;
(GTL46DAJ)
       m_CondExec ${EXAAP},NE,YES 
# AP      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DR8
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D04
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_OutputAssign -c 9 -w BIG0025 IMPRIM
#  ETAT DATE  SOIT JGD801,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD801$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE SYNTHESE DE DESTOCKAGE  : JGD921                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAM PGM=DFSRRC00   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAM
       ;;
(GTL46DAM)
       m_CondExec ${EXAAU},NE,YES 
# AU      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DR9
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D05
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_OutputAssign -c 9 -w BIG0027 IMPRIM
#  ETAT DATE  SOIT JGD921,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD911$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002  : EDITION LISTE DE DESTOCKAGE FILIALE ETAT: JGD731                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAQ PGM=DFSRRC00   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAQ
       ;;
(GTL46DAQ)
       m_CondExec ${EXAAZ},NE,YES 
# AZ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DRA
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D06
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_OutputAssign -c 9 -w BIG0026 IMPRIM
#  ETAT DATE  SOIT JGD731,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD731$VDATEJ1__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002  : EDITION LISTE ANOMALIE DE LIVRAISON ETAT: JTL401                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAT PGM=DFSRRC00   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAT
       ;;
(GTL46DAT)
       m_CondExec ${EXABE},NE,YES 
# BE      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DRB
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D07
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_OutputAssign -c 9 -w BIG0029 IMPRIM
#  ETAT DATE  SOIT JTL401,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JTL401$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
#                                                                              
# ********************************************************************         
#  BIG002  : EDITION LISTE ANOMALIE DE LIVRAISON ETAT: JGD911                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46DAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46DAX
       ;;
(GTL46DAX)
       m_CondExec ${EXABJ},NE,YES 
# BJ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46DRC
       m_OutputAssign -c "*" DDOTV02
# ******  BASE EDITION                                                         
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46D08
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_OutputAssign -c 9 -w BIG0030 IMPRIM
#  ETAT DATE  SOIT JGD911,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JGD911$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP COMREG   PGM=CZX3PEPI   **                                          
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
