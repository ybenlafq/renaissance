#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE51Y.ksh                       --- VERSION DU 08/10/2016 12:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGIE51 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/28 AT 11.41.46 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GIE51Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   IMAGECOPY DU TABLESPACE RSIE10R DE LA D BASE PYDGI00                       
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE51YA
       ;;
(GIE51YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2009/03/28 AT 11.41.46 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GIE51Y                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK/ECARTS'                      
# *                           APPL...: IMPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE51YAA
       ;;
(GIE51YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F45.IE10IY
#                                                                              
# ******  AVANCEMENT INVENTAIRE ENTREPOT                                       
#    TABLES   : NAME=RSIE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR TABLES /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51YAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51YAB
       ;;
(GIE51YAB)
       m_CondExec 04,GE,GIE51YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS10  DE LA D BASE PPDGS00                       
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAD
       ;;
(GIE51YAD)
       m_CondExec ${EXAAF},NE,YES 
#    RSGS10   : NAME=RSGS10,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F07.GS10IP
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51YAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51YAE
       ;;
(GIE51YAE)
       m_CondExec 04,GE,GIE51YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS15   DE LA D BASE PPDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAG
       ;;
(GIE51YAG)
       m_CondExec ${EXAAK},NE,YES 
#    RSGS15   : NAME=RSGS15,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS15 /dev/null
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F07.GS15IP
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51YAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51YAH
       ;;
(GIE51YAH)
       m_CondExec 04,GE,GIE51YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS70R  DE LA D BASE PYDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAJ
       ;;
(GIE51YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F45.GS70IY
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51YAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51YAK
       ;;
(GIE51YAK)
       m_CondExec 04,GE,GIE51YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE RSGS60R  DE LA D BASE PYDGS00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAM
       ;;
(GIE51YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F45.GS60IY
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE51YAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE51YAN
       ;;
(GIE51YAN)
       m_CondExec 04,GE,GIE51YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE510  : EPURATION DES TABLES RTGS60 ET RTGS70                             
#            A SAVOIR HS SORTI DU HS => REPARES ET                             
#            DONC PARTIS DANS UN AUTRE LIEU QUE HS                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAQ
       ;;
(GIE51YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGS60   : NAME=RSGS60Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
#    RSGS70   : NAME=RSGS70Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE510 
       JUMP_LABEL=GIE51YAR
       ;;
(GIE51YAR)
       m_CondExec 04,GE,GIE51YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE515   REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS               
#  REPRISE: SURTOUT PAS CAR COMMIT RESTAURE A FULL IMAGE COPY PRECEDEN         
#           DES TABLES RTGS10 RTGS15 RTGS60 RTGS70 RTIE10                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAT
       ;;
(GIE51YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK THEORIC AVANT INVENTAIRE                                       
#    RSIE05   : NAME=RSIE05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  STOCK HS                                                             
#    RSGS60   : NAME=RSGS60Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  HISTO DES HS                                                         
#    RSGS70   : NAME=RSGS70Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
# ******  PRET A L'ENTREPOT LGT                                                
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  STOCK SS LIEU ENTREPOT LGT                                           
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  AVANCEMENT INVENTAIRE ENTREPOT                                       
#    RSIE10   : NAME=RSIE10Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS                                     
       m_FileAssign -d SHR FIIE515 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE515 
       JUMP_LABEL=GIE51YAU
       ;;
(GIE51YAU)
       m_CondExec 04,GE,GIE51YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE516   REPORT DES ECARTS D INVENTAIRE SUR LES TABLES STOCKS               
#  REPRISE: SURTOUT PAS CAR COMMIT RESTAURE A FULL IMAGE COPY PRECEDEN         
#           DES TABLES RTGS10 RTGS15 RTGS60 RTGS70 RTIE10                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE51YAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GIE51YAX
       ;;
(GIE51YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK THEORIC AVANT INVENTAIRE                                       
#    RSIE60   : NAME=RSIE60Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  STOCK HS                                                             
#    RSGS60   : NAME=RSGS60Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  HISTO DES HS                                                         
#    RSGS70   : NAME=RSGS70Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS70 /dev/null
# ******  PRET A L'ENTREPOT LGT                                                
#    RSGS15   : NAME=RSGS15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  STOCK SS LIEU ENTREPOT LGT                                           
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER EN DUMMY CAR NE SERT PAS                                     
       m_FileAssign -d SHR FIIE516 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE516 
       JUMP_LABEL=GIE51YAY
       ;;
(GIE51YAY)
       m_CondExec 04,GE,GIE51YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
