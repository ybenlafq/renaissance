#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA002M.ksh                       --- VERSION DU 09/10/2016 05:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMVA002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/23 AT 11.04.02 BY BURTECA                      
#    STANDARDS: P  JOBSET: VA002M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BVA510                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VA002MA
       ;;
(VA002MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VA002MAA
       ;;
(VA002MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTVA00   : NAME=RSVA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA15 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PTEM/VA002MAA.BVA510AM
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR FEXTRACB /dev/null
# *****   FICHIER D'EXTRACTION POUR LES PGMS D'EDITIONS                        
       m_FileAssign -d NEW,CATLG,DELETE -r 55 -t LSEQ -g +1 FVA510 ${DATA}/PTEM/VA002MAA.FVA510AM
       m_FileAssign -d SHR FVA515 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA510 
       JUMP_LABEL=VA002MAB
       ;;
(VA002MAB)
       m_CondExec 04,GE,VA002MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA510AP POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAD
       ;;
(VA002MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/VA002MAA.BVA510AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MAD.BVA510BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 " "
 /FIELDS FLD_BI_40_2 40 CH 2
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_BI_22_3 22 CH 3
 /FIELDS FLD_BI_33_7 33 CH 7
 /FIELDS FLD_BI_28_5 28 CH 5
 /FIELDS FLD_BI_15_7 15 CH 7
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_CH_52_1 52 CH 1
 /FIELDS FLD_PD_25_3 25 PD 3
 /CONDITION CND_1 FLD_CH_52_1 EQ CST_1_12 
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_7 ASCENDING,
   FLD_BI_22_3 ASCENDING,
   FLD_PD_25_3 ASCENDING,
   FLD_BI_28_5 ASCENDING,
   FLD_BI_33_7 ASCENDING,
   FLD_BI_40_2 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MAE
       ;;
(VA002MAE)
       m_CondExec 00,EQ,VA002MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAG
       ;;
(VA002MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/VA002MAD.BVA510BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MAG.BVA510CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MAH
       ;;
(VA002MAH)
       m_CondExec 04,GE,VA002MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAJ
       ;;
(VA002MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/VA002MAG.BVA510CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MAJ.BVA510DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MAK
       ;;
(VA002MAK)
       m_CondExec 00,EQ,VA002MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA500 (ETAT DES ANOMALIES DE MVTS DE STOCK)             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAM
       ;;
(VA002MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/VA002MAD.BVA510BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/VA002MAJ.BVA510DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA510                                                          
#  FEDITION DD SYSOUT=(9,IVA510),DCB=(RECFM=VA),SPIN=UNALLOC                   
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA510CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MAN
       ;;
(VA002MAN)
       m_CondExec 04,GE,VA002MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BVA510AM POUR LE PGM BVA511                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAQ
       ;;
(VA002MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/VA002MAA.BVA510AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MAQ.FVA511AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "AVO"
 /DERIVEDFIELD CST_3_8 "RCY"
 /FIELDS FLD_BI_7_35 7 CH 35
 /FIELDS FLD_BI_12_3 12 CH 3
 /CONDITION CND_1 FLD_BI_12_3 EQ CST_1_4 OR FLD_BI_12_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_7_35 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MAR
       ;;
(VA002MAR)
       m_CondExec 00,EQ,VA002MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA511                                                                
#  CREATION DES MVTS DU MOIS POUR LES LIEUX 'AVO' ET RCY'                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAT
       ;;
(VA002MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA06   : NAME=RSGA06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA06 /dev/null
#    RTGA55   : NAME=RSGA55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA55 /dev/null
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A7} FVA510 ${DATA}/PTEM/VA002MAQ.FVA511AM
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PNCGM/F89.BVA511AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA511 
       JUMP_LABEL=VA002MAU
       ;;
(VA002MAU)
       m_CondExec 04,GE,VA002MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BVA511AM POUR LE GENERATEUR D'ETAT                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MAX
       ;;
(VA002MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PNCGM/F89.BVA511AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MAX.BVA511BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_38_7 38 CH 7
 /FIELDS FLD_BI_20_7 20 CH 7
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_BI_27_3 27 CH 3
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_BI_45_2 45 CH 2
 /FIELDS FLD_PD_30_3 30 PD 3
 /FIELDS FLD_BI_33_5 33 CH 5
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_BI_20_7 ASCENDING,
   FLD_BI_27_3 ASCENDING,
   FLD_PD_30_3 ASCENDING,
   FLD_BI_33_5 ASCENDING,
   FLD_BI_38_7 ASCENDING,
   FLD_BI_45_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MAY
       ;;
(VA002MAY)
       m_CondExec 00,EQ,VA002MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBA
       ;;
(VA002MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/VA002MAX.BVA511BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MBA.BVA511CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MBB
       ;;
(VA002MBB)
       m_CondExec 04,GE,VA002MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBD
       ;;
(VA002MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/VA002MBA.BVA511CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MBD.BVA511DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MBE
       ;;
(VA002MBE)
       m_CondExec 00,EQ,VA002MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT IVA511 (ETAT DES MVTS DE STOCK LIEUX AVO ET RCY          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBG
       ;;
(VA002MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/VA002MAX.BVA511BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/VA002MBD.BVA511DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA511                                                          
#  FEDITION REPORT SYSOUT=(9,IVA511),RECFM=VA,FREE=CLOSE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA511BM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MBH
       ;;
(VA002MBH)
       m_CondExec 04,GE,VA002MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA520                                                                
# ********************************************************************         
#  CREATION DE LA LISTE DES ACHATS TRIES PAR FOURNISSEUR ET RAYON              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBJ
       ;;
(VA002MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTVA00   : NAME=RSVA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA15 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA06   : NAME=RSGA06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA06 /dev/null
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA20   : NAME=RSGA20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
#    RTGA55   : NAME=RSGA55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA55 /dev/null
#    RTGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGF10 /dev/null
#    RTGR10   : NAME=RSGA55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGR10 /dev/null
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PNCGM/F89.BVA520AM
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRACE ${DATA}/PTEM/VA002MBJ.BVA520EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA520 
       JUMP_LABEL=VA002MBK
       ;;
(VA002MBK)
       m_CondExec 04,GE,VA002MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA520AP POUR LE GENERATEUR D'ETAT                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBM
       ;;
(VA002MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PNCGM/F89.BVA520AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MBM.BVA520BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_20 15 CH 20
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_13_2 13 CH 02
 /FIELDS FLD_BI_10_3 10 CH 3
 /FIELDS FLD_BI_35_2 35 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_2 ASCENDING,
   FLD_BI_15_20 ASCENDING,
   FLD_BI_35_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MBN
       ;;
(VA002MBN)
       m_CondExec 00,EQ,VA002MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBQ
       ;;
(VA002MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/VA002MBM.BVA520BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MBQ.BVA520CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MBR
       ;;
(VA002MBR)
       m_CondExec 04,GE,VA002MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBT
       ;;
(VA002MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/VA002MBQ.BVA520CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MBT.BVA520DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MBU
       ;;
(VA002MBU)
       m_CondExec 00,EQ,VA002MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA500 (ETAT DES ANOMALIES DE MVTS DE STOCK)             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VA002MBX
       ;;
(VA002MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/VA002MBM.BVA520BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/VA002MBT.BVA520DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA520                                                          
#  FEDITION DD SYSOUT=(9,IVA520),DCB=(RECFM=VA),SPIN=UNALLOC                   
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA520BM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MBY
       ;;
(VA002MBY)
       m_CondExec 04,GE,VA002MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA525                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR LES ETATS DE POSITIONS ET DE MOUVEMENTS          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCA
       ;;
(VA002MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER D EXTRACTION ISSU DU BVA510                                         
       m_FileAssign -d SHR -g ${G_A18} FVA510 ${DATA}/PTEM/VA002MAA.FVA510AM
# *****   TABLES EN ENTREE                                                     
#    RTVA00   : NAME=RSVA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTVA05   : NAME=RSVA05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA15 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA20   : NAME=RSGA20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIERS D'EXTRACTION                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA530 ${DATA}/PTEM/VA002MCA.BVA530AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA540 ${DATA}/PTEM/VA002MCA.BVA540AM
       m_FileAssign -d SHR FVA541 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA525 
       JUMP_LABEL=VA002MCB
       ;;
(VA002MCB)
       m_CondExec 04,GE,VA002MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA540AP POUR ETAT IVA540D (ARTICLE DARTY)                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCD
       ;;
(VA002MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/VA002MCA.BVA540AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MCD.BVA540BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_187_8 187 PD 8
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_CH_171_24 171 CH 24
 /FIELDS FLD_CH_227_6 227 CH 6
 /FIELDS FLD_PD_251_6 251 PD 6
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_CH_251_18 251 CH 18
 /FIELDS FLD_PD_263_6 263 PD 6
 /FIELDS FLD_PD_131_8 131 PD 08
 /FIELDS FLD_CH_1_14 1 CH 14
 /FIELDS FLD_PD_179_8 179 PD 08
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_CH_131_8 131 CH 8
 /FIELDS FLD_PD_171_8 171 PD 08
 /FIELDS FLD_CH_32_2 32 CH 2
 /FIELDS FLD_PD_257_6 257 PD 6
 /FIELDS FLD_PD_227_6 227 PD 6
 /FIELDS FLD_CH_51_20 51 CH 20
 /FIELDS FLD_CH_39_8 39 CH 8
 /FIELDS FLD_CH_28_END 28 CH 
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_131_8,
    TOTAL FLD_PD_171_8,
    TOTAL FLD_PD_179_8,
    TOTAL FLD_PD_187_8,
    TOTAL FLD_PD_227_6,
    TOTAL FLD_PD_251_6,
    TOTAL FLD_PD_257_6,
    TOTAL FLD_PD_263_6
 /* MT_ERROR (unknown field) (1,14,32,2,39,8,51,20,131,8,171,24,227,6,251,18,28 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_14,FLD_CH_32_2,FLD_CH_39_8,FLD_CH_51_20,FLD_CH_131_8,FLD_CH_171_24,FLD_CH_227_6,FLD_CH_251_18,FLD_CH_28_END
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA002MCE
       ;;
(VA002MCE)
       m_CondExec 00,EQ,VA002MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP VA002MCG PGM=BVA526     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCG
       ;;
(VA002MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ISSU DU PGM BVA525                                           
       m_FileAssign -d SHR -g ${G_A20} FVA540I ${DATA}/PTEM/VA002MCD.BVA540BM
# *****   FICHIER RESULTANT                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA540O ${DATA}/PTEM/VA002MCG.BVA526AM
       m_ProgramExec BVA526 
# ********************************************************************         
#  TRI DU FICHIER BVA526AD POUR LE GENERATEUR D'ETAT (MICRO-FICHE)             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCJ
       ;;
(VA002MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/VA002MCG.BVA526AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MCJ.BVA526BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_2 18 CH 2
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_3 10 CH 3
 /FIELDS FLD_BI_13_3 13 CH 3
 /FIELDS FLD_BI_16_2 16 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_2 ASCENDING,
   FLD_BI_18_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MCK
       ;;
(VA002MCK)
       m_CondExec 00,EQ,VA002MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCM
       ;;
(VA002MCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A22} FEXTRAC ${DATA}/PTEM/VA002MCJ.BVA526BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MCM.BVA540CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MCN
       ;;
(VA002MCN)
       m_CondExec 04,GE,VA002MCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCQ
       ;;
(VA002MCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/VA002MCM.BVA540CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MCQ.BVA540DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MCR
       ;;
(VA002MCR)
       m_CondExec 00,EQ,VA002MCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA540M (ARTICLE DARTY)                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCT
       ;;
(VA002MCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A24} FEXTRAC ${DATA}/PTEM/VA002MCJ.BVA526BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A25} FCUMULS ${DATA}/PTEM/VA002MCQ.BVA540DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA540M                                                         
#  FEDITION DD SYSOUT=(9,IVA540D),DCB=(RECFM=VA),SPIN=UNALLOC                  
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA540AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MCU
       ;;
(VA002MCU)
       m_CondExec 04,GE,VA002MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA530AP POUR ETAT IVA530                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=VA002MCX
       ;;
(VA002MCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PTEM/VA002MCA.BVA530AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MCX.BVA531AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_6_3 6 CH 3
 /FIELDS FLD_BI_9_2 9 CH 2
 /FIELDS FLD_PD_11_3 11 PD 3
 /FIELDS FLD_BI_4_2 4 CH 2
 /FIELDS FLD_CH_1_294 1 CH 294
 /FIELDS FLD_BI_1_3 1 CH 3
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_2 ASCENDING,
   FLD_BI_6_3 ASCENDING,
   FLD_BI_9_2 ASCENDING,
   FLD_PD_11_3 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_294
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA002MCY
       ;;
(VA002MCY)
       m_CondExec 00,EQ,VA002MCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA530                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA530                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDA
       ;;
(VA002MDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A27} FVA530 ${DATA}/PTEM/VA002MCX.BVA531AM
# *****   ETAT IVA530                                                          
#  IVA530   DD SYSOUT=(9,IVA530),DCB=(RECFM=VA),SPIN=UNALLOC                   
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ IVA530 ${DATA}/PXX0.FICHEML.EXVA0M.IVA530BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA530 
       JUMP_LABEL=VA002MDB
       ;;
(VA002MDB)
       m_CondExec 04,GE,VA002MDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA530AP POUR ETAT IVA550D (VALO DARTY)                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDD
       ;;
(VA002MDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/VA002MCA.BVA530AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MDD.BVA550AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_79_220 79 CH 220
 /FIELDS FLD_BI_79_60 79 CH 60
 /FIELDS FLD_CH_1_5 01 CH 05
 /FIELDS FLD_BI_31_5 31 CH 5
 /FIELDS FLD_BI_9_22 9 CH 22
 /FIELDS FLD_PD_139_8 139 PD 08
 /FIELDS FLD_PD_147_8 147 PD 08
 /FIELDS FLD_BI_1_5 1 CH 5
 /FIELDS FLD_PD_163_8 163 PD 8
 /FIELDS FLD_PD_155_8 155 PD 08
 /FIELDS FLD_CH_9_30 09 CH 30
 /KEYS
   FLD_BI_1_5 ASCENDING,
   FLD_BI_9_22 ASCENDING,
   FLD_BI_31_5 ASCENDING,
   FLD_BI_79_60 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_8,
    TOTAL FLD_PD_147_8,
    TOTAL FLD_PD_155_8,
    TOTAL FLD_PD_163_8
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_5,FLD_CH_9_30,FLD_CH_79_220
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA002MDE
       ;;
(VA002MDE)
       m_CondExec 00,EQ,VA002MDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA550                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA550D (VALO DARTY)                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDG
       ;;
(VA002MDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE FDARDAC : 'DARTY'                                          
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/VA002MDG
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A29} FVA550 ${DATA}/PTEM/VA002MDD.BVA550AM
# *****   ETAT IVA550                                                          
#  IVA550   DD SYSOUT=(9,IVA550D),DCB=(RECFM=VA),SPIN=UNALLOC                  
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ IVA550 ${DATA}/PXX0.FICHEML.EXVA0M.IVA550AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA550 
       JUMP_LABEL=VA002MDH
       ;;
(VA002MDH)
       m_CondExec 04,GE,VA002MDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA530AP POUR ETAT IVA560                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDJ
       ;;
(VA002MDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/VA002MCA.BVA530AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MDJ.BVA560AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_227_8 227 PD 8
 /FIELDS FLD_PD_211_8 211 PD 8
 /FIELDS FLD_PD_219_8 219 PD 8
 /FIELDS FLD_PD_179_8 179 PD 8
 /FIELDS FLD_PD_289_6 289 PD 6
 /FIELDS FLD_CH_9_5 9 CH 5
 /FIELDS FLD_PD_283_6 283 PD 6
 /FIELDS FLD_PD_259_6 259 PD 6
 /FIELDS FLD_PD_155_8 155 PD 08
 /FIELDS FLD_BI_39_20 39 CH 20
 /FIELDS FLD_BI_9_5 9 CH 5
 /FIELDS FLD_PD_163_8 163 PD 8
 /FIELDS FLD_BI_79_40 79 CH 40
 /FIELDS FLD_PD_271_6 271 PD 6
 /FIELDS FLD_PD_235_6 235 PD 6
 /FIELDS FLD_PD_147_8 147 PD 08
 /FIELDS FLD_PD_139_8 139 PD 08
 /FIELDS FLD_PD_253_6 253 PD 6
 /FIELDS FLD_CH_39_20 39 CH 20
 /FIELDS FLD_PD_187_8 187 PD 8
 /FIELDS FLD_BI_31_5 31 CH 5
 /FIELDS FLD_PD_247_6 247 PD 6
 /FIELDS FLD_CH_31_5 31 CH 5
 /FIELDS FLD_PD_277_6 277 PD 6
 /FIELDS FLD_PD_265_6 265 PD 6
 /FIELDS FLD_PD_241_6 241 PD 6
 /FIELDS FLD_PD_171_8 171 PD 8
 /FIELDS FLD_CH_79_40 79 CH 40
 /FIELDS FLD_CH_1_5 01 CH 05
 /FIELDS FLD_BI_1_5 1 CH 5
 /FIELDS FLD_CH_139_156 139 CH 156
 /FIELDS FLD_PD_195_8 195 PD 8
 /FIELDS FLD_PD_203_8 203 PD 8
 /KEYS
   FLD_BI_1_5 ASCENDING,
   FLD_BI_9_5 ASCENDING,
   FLD_BI_31_5 ASCENDING,
   FLD_BI_39_20 ASCENDING,
   FLD_BI_79_40 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_8,
    TOTAL FLD_PD_147_8,
    TOTAL FLD_PD_155_8,
    TOTAL FLD_PD_163_8,
    TOTAL FLD_PD_171_8,
    TOTAL FLD_PD_179_8,
    TOTAL FLD_PD_187_8,
    TOTAL FLD_PD_195_8,
    TOTAL FLD_PD_203_8,
    TOTAL FLD_PD_211_8,
    TOTAL FLD_PD_219_8,
    TOTAL FLD_PD_227_8,
    TOTAL FLD_PD_235_6,
    TOTAL FLD_PD_241_6,
    TOTAL FLD_PD_247_6,
    TOTAL FLD_PD_253_6,
    TOTAL FLD_PD_259_6,
    TOTAL FLD_PD_265_6,
    TOTAL FLD_PD_271_6,
    TOTAL FLD_PD_277_6,
    TOTAL FLD_PD_283_6,
    TOTAL FLD_PD_289_6
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_5,FLD_CH_9_5,FLD_CH_31_5,FLD_CH_39_20,FLD_CH_79_40,FLD_CH_139_156
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA002MDK
       ;;
(VA002MDK)
       m_CondExec 00,EQ,VA002MDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA560                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA560                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDM
       ;;
(VA002MDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A31} FVA560 ${DATA}/PTEM/VA002MDJ.BVA560AM
# *****   ETAT IVA560                                                          
#  IVA560 DD SYSOUT=(9,IVA560),DCB=(RECFM=VA),SPIN=UNALLOC                     
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ IVA560 ${DATA}/PXX0.FICHEML.EXVA0M.IVA560BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA560 
       JUMP_LABEL=VA002MDN
       ;;
(VA002MDN)
       m_CondExec 04,GE,VA002MDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA565                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR LES ETATS IVA570 ET IVA580  (DARTY)              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDQ
       ;;
(VA002MDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER D EXTRACTION ISSU DU BVA510                                         
       m_FileAssign -d SHR -g ${G_A32} FVA510 ${DATA}/PTEM/VA002MAA.FVA510AM
# *****   TABLES EN ENTREE                                                     
#    RTVA00   : NAME=RSVA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTVA05   : NAME=RSVA05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA15 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIERS D'EXTRACTION DARTY                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FVA570 ${DATA}/PTEM/VA002MDQ.FVA570AM
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 FVA571 ${DATA}/PTEM/VA002MDQ.FVA571AM
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -t LSEQ -g +1 FVA580 ${DATA}/PTEM/VA002MDQ.FVA580AM
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 FVA581 ${DATA}/PXX0/F89.FVA581AM
# *****   FICHIERS D'EXTRACTION DACEM                                          
       m_FileAssign -d SHR FVA572 /dev/null
       m_FileAssign -d SHR FVA573 /dev/null
       m_FileAssign -d SHR FVA582 /dev/null
       m_FileAssign -d SHR FVA583 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA565 
       JUMP_LABEL=VA002MDR
       ;;
(VA002MDR)
       m_CondExec 04,GE,VA002MDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA570AP ISSU DU BVA565 POUR CREATION BVA570AP               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDT
       ;;
(VA002MDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/VA002MDQ.FVA570AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MDT.BVA570AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_49_8 49 PD 8
 /FIELDS FLD_PD_43_6 43 PD 6
 /FIELDS FLD_PD_65_8 65 PD 8
 /FIELDS FLD_PD_37_6 37 PD 6
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_PD_57_8 57 PD 8
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_73_8 73 PD 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING,
   FLD_CH_11_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_37_6,
    TOTAL FLD_PD_43_6,
    TOTAL FLD_PD_49_8,
    TOTAL FLD_PD_57_8,
    TOTAL FLD_PD_65_8,
    TOTAL FLD_PD_73_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MDU
       ;;
(VA002MDU)
       m_CondExec 00,EQ,VA002MDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA571AP ISSU DU BVA565 POUR CREATION BVA571AP               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MDX
       ;;
(VA002MDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/VA002MDQ.FVA571AM
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MDX.BVA571AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_PD_56_8 56 PD 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_17_1 17 CH 1
 /FIELDS FLD_PD_44_6 44 PD 6
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_PD_64_8 64 PD 8
 /FIELDS FLD_PD_50_6 50 PD 6
 /FIELDS FLD_CH_21_3 21 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING,
   FLD_CH_11_6 ASCENDING,
   FLD_CH_17_1 ASCENDING,
   FLD_CH_18_3 ASCENDING,
   FLD_CH_21_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_44_6,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_8,
    TOTAL FLD_PD_64_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MDY
       ;;
(VA002MDY)
       m_CondExec 00,EQ,VA002MDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA580AP ISSU DU BVA565 POUR CREATION BVA580AP               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=VA002MEA
       ;;
(VA002MEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/VA002MDQ.FVA580AM
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MEA.BVA580AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_6 20 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_42_8 42 PD 8
 /FIELDS FLD_PD_50_8 50 PD 8
 /FIELDS FLD_PD_14_6 14 PD 6
 /FIELDS FLD_PD_26_8 26 PD 8
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_PD_34_8 34 PD 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_14_6,
    TOTAL FLD_PD_20_6,
    TOTAL FLD_PD_26_8,
    TOTAL FLD_PD_34_8,
    TOTAL FLD_PD_42_8,
    TOTAL FLD_PD_50_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MEB
       ;;
(VA002MEB)
       m_CondExec 00,EQ,VA002MEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA581AP ISSU DU BVA565 POUR CREATION BVA581AP               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MED
       ;;
(VA002MED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PXX0/F89.FVA581AM
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MED.BVA581AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_16 "Z"
 /DERIVEDFIELD CST_1_12 "M"
 /FIELDS FLD_PD_47_6 47 PD 6
 /FIELDS FLD_PD_53_8 53 PD 8
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_PD_41_6 41 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_61_8 61 PD 8
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_12 OR FLD_CH_14_1 EQ CST_3_16 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING,
   FLD_CH_14_1 ASCENDING,
   FLD_CH_15_3 ASCENDING,
   FLD_CH_18_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_6,
    TOTAL FLD_PD_47_6,
    TOTAL FLD_PD_53_8,
    TOTAL FLD_PD_61_8
 /OMIT CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MEE
       ;;
(VA002MEE)
       m_CondExec 00,EQ,VA002MED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA570                                                                
# ********************************************************************         
#  CREATION DU FICHIER POUR ETAT IVA570 (ARTICLE DARTY)                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MEG PGM=BVA570     ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=VA002MEG
       ;;
(VA002MEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ISSU DU TRI DE FVA570AP                                      
       m_FileAssign -d SHR -g ${G_A37} FVA570 ${DATA}/PTEM/VA002MDT.BVA570AM
# *****   FICHIER ISSU DU TRI DE FVA571AP                                      
       m_FileAssign -d SHR -g ${G_A38} FVA571 ${DATA}/PTEM/VA002MDX.BVA571AM
# *****   FICHIER POUR ETAT IVA570 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IVA570 ${DATA}/PTEM/VA002MEG.BVA570BM
       m_ProgramExec BVA570 
# ********************************************************************         
#  TRI DU FICHIER BVA570BP POUR ETAT IVA570 (ARTICLE DARTY)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=VA002MEJ
       ;;
(VA002MEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/VA002MEG.BVA570BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MEJ.BVA570CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_22_3 22 CH 3
 /FIELDS FLD_BI_25_2 25 CH 2
 /FIELDS FLD_BI_18_1 18 CH 1
 /FIELDS FLD_BI_19_3 19 CH 3
 /FIELDS FLD_BI_12_6 12 CH 6
 /FIELDS FLD_BI_7_3 7 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_6 ASCENDING,
   FLD_BI_18_1 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_3 ASCENDING,
   FLD_BI_25_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MEK
       ;;
(VA002MEK)
       m_CondExec 00,EQ,VA002MEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=VA002MEM
       ;;
(VA002MEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A40} FEXTRAC ${DATA}/PTEM/VA002MEJ.BVA570CM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MEM.BVA570DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MEN
       ;;
(VA002MEN)
       m_CondExec 04,GE,VA002MEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=VA002MEQ
       ;;
(VA002MEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/VA002MEM.BVA570DM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MEQ.BVA570EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MER
       ;;
(VA002MER)
       m_CondExec 00,EQ,VA002MEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA570D (ARTICLE DARTY)                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=VA002MET
       ;;
(VA002MET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A42} FEXTRAC ${DATA}/PTEM/VA002MEJ.BVA570CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A43} FCUMULS ${DATA}/PTEM/VA002MEQ.BVA570EM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA570D                                                         
#  FEDITION DD SYSOUT=(9,IVA570D),DCB=(RECFM=VA),SPIN=UNALLOC                  
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA570AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MEU
       ;;
(VA002MEU)
       m_CondExec 04,GE,VA002MET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA580                                                                
# ********************************************************************         
#  CREATION DU FICHIER POUR ETAT IVA580 (ARTICLE DARTY)                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=VA002MEX
       ;;
(VA002MEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER ISSU DU TRI DE FVA580AP                                      
       m_FileAssign -d SHR -g ${G_A44} FVA580 ${DATA}/PTEM/VA002MEA.BVA580AM
# *****   FICHIER ISSU DU TRI DE FVA581AP                                      
       m_FileAssign -d SHR -g ${G_A45} FVA581 ${DATA}/PTEM/VA002MED.BVA581AM
# *****   FICHIER POUR ETAT IVA570 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IVA580 ${DATA}/PTEM/VA002MEX.BVA580BM
# ******  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTVA30   : NAME=RSVA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA30 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA580 
       JUMP_LABEL=VA002MEY
       ;;
(VA002MEY)
       m_CondExec 04,GE,VA002MEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BVA580BP POUR ETAT IVA580 (ARTICLE DARTY)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFA
       ;;
(VA002MFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PTEM/VA002MEX.BVA580BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MFA.BVA580CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_13_3 13 CH 3
 /FIELDS FLD_BI_12_1 12 CH 1
 /FIELDS FLD_BI_19_3 19 CH 3
 /FIELDS FLD_BI_22_2 22 CH 2
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_10_2 10 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_1 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_3 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MFB
       ;;
(VA002MFB)
       m_CondExec 00,EQ,VA002MFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFD
       ;;
(VA002MFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A47} FEXTRAC ${DATA}/PTEM/VA002MFA.BVA580CM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MFD.BVA580DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MFE
       ;;
(VA002MFE)
       m_CondExec 04,GE,VA002MFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFG
       ;;
(VA002MFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/VA002MFD.BVA580DM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MFG.BVA580EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MFH
       ;;
(VA002MFH)
       m_CondExec 00,EQ,VA002MFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA580D (ARTICLE DARTY)                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFJ
       ;;
(VA002MFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A49} FEXTRAC ${DATA}/PTEM/VA002MFA.BVA580CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A50} FCUMULS ${DATA}/PTEM/VA002MFG.BVA580EM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA580D                                                         
#  FEDITION DD SYSOUT=(9,IVA580D),DCB=(RECFM=VA),SPIN=UNALLOC                  
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA580AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MFK
       ;;
(VA002MFK)
       m_CondExec 04,GE,VA002MFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA590                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA590 (LIEU HS)                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFM
       ;;
(VA002MFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTVA00   : NAME=RSVA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTVA05   : NAME=RSVA05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA15 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIERS D'EXTRACTION DARTY                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PTEM/VA002MFM.BVA590AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA590 
       JUMP_LABEL=VA002MFN
       ;;
(VA002MFN)
       m_CondExec 04,GE,VA002MFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA590AP POUR ETAT IVA590                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFQ PGM=SORT       ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFQ
       ;;
(VA002MFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PTEM/VA002MFM.BVA590AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MFQ.BVA590BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_30_2 30 CH 2
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_PD_23_7 23 PD 7
 /FIELDS FLD_BI_20_3 20 CH 3
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_12_3 12 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_BI_20_3 ASCENDING,
   FLD_PD_23_7 ASCENDING,
   FLD_BI_30_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MFR
       ;;
(VA002MFR)
       m_CondExec 00,EQ,VA002MFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFT PGM=IKJEFT01   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFT
       ;;
(VA002MFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A52} FEXTRAC ${DATA}/PTEM/VA002MFQ.BVA590BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MFT.BVA590CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MFU
       ;;
(VA002MFU)
       m_CondExec 04,GE,VA002MFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MFX PGM=SORT       ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=VA002MFX
       ;;
(VA002MFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A53} SORTIN ${DATA}/PTEM/VA002MFT.BVA590CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MFX.BVA590DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MFY
       ;;
(VA002MFY)
       m_CondExec 00,EQ,VA002MFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA590                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGA PGM=IKJEFT01   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGA
       ;;
(VA002MGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A54} FEXTRAC ${DATA}/PTEM/VA002MFQ.BVA590BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A55} FCUMULS ${DATA}/PTEM/VA002MFX.BVA590DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA590                                                          
#  FEDITION DD SYSOUT=(9,IVA590),DCB=(RECFM=VA),SPIN=UNALLOC                   
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVA0M.IVA590BM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MGB
       ;;
(VA002MGB)
       m_CondExec 04,GE,VA002MGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA585                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA585 (SUIV. STOCKS CONSOMMABLE)         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGD
       ;;
(VA002MGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA05   : NAME=RSVA05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGA55   : NAME=RSGA55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA55 /dev/null
#    RTGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   EDITION ETAT IVA585N DARTY                                           
# * IVA585   REPORT SYSOUT=(9,IVA585),FREE=CLOSE,RECFM=FBA,LRECL=133,          
# *             BLKSIZE=0                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IVA585 ${DATA}/PXX0.FICHEML.EXVA0M.IVA585BM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA585 
       JUMP_LABEL=VA002MGE
       ;;
(VA002MGE)
       m_CondExec 04,GE,VA002MGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA595                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA595 (LIEU HS)                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGG
       ;;
(VA002MGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTVA00   : NAME=RSVA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTVA05   : NAME=RSVA05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA15 /dev/null
#    RTVA25   : NAME=RSVA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIERS D'EXTRACTION DARTY                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA595 ${DATA}/PTEM/VA002MGG.BVA595AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA595 
       JUMP_LABEL=VA002MGH
       ;;
(VA002MGH)
       m_CondExec 04,GE,VA002MGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BVA595AP POUR ETAT IVA595                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGJ PGM=SORT       ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGJ
       ;;
(VA002MGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A56} SORTIN ${DATA}/PTEM/VA002MGG.BVA595AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MGJ.BVA595BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_6 7 CH 6
 /KEYS
   FLD_BI_7_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MGK
       ;;
(VA002MGK)
       m_CondExec 00,EQ,VA002MGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGM PGM=IKJEFT01   ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGM
       ;;
(VA002MGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A57} FEXTRAC ${DATA}/PTEM/VA002MGJ.BVA595BM
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA002MGM.BVA595CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA002MGN
       ;;
(VA002MGN)
       m_CondExec 04,GE,VA002MGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGQ PGM=SORT       ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGQ
       ;;
(VA002MGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A58} SORTIN ${DATA}/PTEM/VA002MGM.BVA595CM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA002MGQ.BVA595DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA002MGR
       ;;
(VA002MGR)
       m_CondExec 00,EQ,VA002MGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA595                                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGT PGM=IKJEFT01   ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGT
       ;;
(VA002MGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A59} FEXTRAC ${DATA}/PTEM/VA002MGJ.BVA595BM
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A60} FCUMULS ${DATA}/PTEM/VA002MGQ.BVA595DM
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DAL                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA590 NON UTILISE CGA LE 081098                                
# //FEDITION DD    SYSOUT=(9,IVA595),DCB=(RECFM=VA),SPIN=UNALLOC               
       m_OutputAssign -c "*" FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA002MGU
       ;;
(VA002MGU)
       m_CondExec 04,GE,VA002MGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA510  (MOUVEMENTS DU MOIS)                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MGX PGM=IEBGENER   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=VA002MGX
       ;;
(VA002MGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA510CM
       m_OutputAssign -c 9 -w IVA510 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MGY
       ;;
(VA002MGY)
       m_CondExec 00,EQ,VA002MGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA511  (MOUVEMENTS DU MOIS (RET FRS)                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHA PGM=IEBGENER   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHA
       ;;
(VA002MHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA511BM
       m_OutputAssign -c 9 -w IVA511 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHB
       ;;
(VA002MHB)
       m_CondExec 00,EQ,VA002MHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA520  (MOUVEMENTS ACHATS PAR FRS)                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHD PGM=IEBGENER   ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHD
       ;;
(VA002MHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA520BM
       m_OutputAssign -c 9 -w IVA520 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHE
       ;;
(VA002MHE)
       m_CondExec 00,EQ,VA002MHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA530  (STOCK DETAIL ARTICLE / LIEU)                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHG PGM=IEBGENER   ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHG
       ;;
(VA002MHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA530BM
       m_OutputAssign -c 9 -w IVA530 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHH
       ;;
(VA002MHH)
       m_CondExec 00,EQ,VA002MHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA540D (RECAPITULATIF PAR LIEU)                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHJ PGM=IEBGENER   ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHJ
       ;;
(VA002MHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA540AM
       m_OutputAssign -c 9 -w IVA540D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHK
       ;;
(VA002MHK)
       m_CondExec 00,EQ,VA002MHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA550D (RECAPITULATIF PAR ARTICLE)                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHM PGM=IEBGENER   ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHM
       ;;
(VA002MHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA550AM
       m_OutputAssign -c 9 -w IVA550D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHN
       ;;
(VA002MHN)
       m_CondExec 00,EQ,VA002MHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA560  (RECAPITULATIF PAR ARTICLE)                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHQ PGM=IEBGENER   ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHQ
       ;;
(VA002MHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA560BM
       m_OutputAssign -c 9 -w IVA560 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHR
       ;;
(VA002MHR)
       m_CondExec 00,EQ,VA002MHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA570D (SYNTHESE DES CONTREPARTIES LIEU)                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHT PGM=IEBGENER   ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHT
       ;;
(VA002MHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA570AM
       m_OutputAssign -c 9 -w IVA570D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHU
       ;;
(VA002MHU)
       m_CondExec 00,EQ,VA002MHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA580D (SYNTHESE DES CONTREPARTIES STE)                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MHX PGM=IEBGENER   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=VA002MHX
       ;;
(VA002MHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA580AM
       m_OutputAssign -c 9 -w IVA580D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MHY
       ;;
(VA002MHY)
       m_CondExec 00,EQ,VA002MHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA585                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA002MIA PGM=IEBGENER   ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=VA002MIA
       ;;
(VA002MIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA585BM
       m_OutputAssign -c 9 -w IVA585 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#  EDITION : IVA585  (SUIVI STOCKS CONSOMAMABLE )                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MIB
       ;;
(VA002MIB)
       m_CondExec 00,EQ,VA002MIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP VA002MID PGM=IEBGENER   ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=VA002MID
       ;;
(VA002MID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVA0M.IVA590BM
#  SYSUT2   REPORT SYSOUT=(9,IVA590),FREE=CLOSE  PLUS BESOIN PAPIER            
#                        PLUS BESOIN SUR PAPIER CGA 080499                     
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA002MIE
       ;;
(VA002MIE)
       m_CondExec 00,EQ,VA002MID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA002MZA
       ;;
(VA002MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA002MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
