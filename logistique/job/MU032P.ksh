#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU032P.ksh                       --- VERSION DU 09/10/2016 00:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU032 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/01/28 AT 14.35.28 BY BURTECC                      
#    STANDARDS: P  JOBSET: MU032P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMU032 : EXTRACTION DES MUTATIONS NOMAD                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU032PA
       ;;
(MU032PA)
       EXAAA=${EXAAA:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MU032PAA
       ;;
(MU032PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGB05   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  FMOIS : MMSSAA                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
#  FICHIER CSV                                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 88 -t LSEQ -g +1 FMU032 ${DATA}/PXX0/F07.BMU032AP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU032 
       JUMP_LABEL=MU032PAB
       ;;
(MU032PAB)
       m_CondExec 04,GE,MU032PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTMU032P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU032PAD PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MU032PAD
       ;;
(MU032PAD)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU032PAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/MU032PAD.FTMU032P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTMU032P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU032PAG PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MU032PAG
       ;;
(MU032PAG)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/MU032PAG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.MU032PAD.FTMU032P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
#                                                                              
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MU032PZA
       ;;
(MU032PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU032PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
