#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL55D.ksh                       --- VERSION DU 09/10/2016 05:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL55 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.15.34 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL55D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#    IMPERATIF :  CETTE CHAINE DOIT ETRE EXECUTEE IMPERATIVEMENT               
#    -=-=-=-=-    CAR ELLE CONCERNE LES LIVRAISONS ET LE DEPOT,POUR            
#                 LES REPRISES CLIENTS.                                        
#                                                                              
# ********************************************************************         
#                                                                              
# ********************************************************************         
# ********************************************************************         
#   BTL055 : CREATION DES BONS DE REPRISES                                     
#   REPRISE: OUI SI ABEND                                                      
#          : NON SI FIN NORMALE FAIRE UN RECOVER TO RBA POUR LA                
#            TABLE RTGV10  A PARTIR DU QUIESCE DU STEP PRECEDENT               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL55DA
       ;;
(GTL55DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL55DAA
       ;;
(GTL55DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES B.R. PLUS PROFILS LIV                                    
# ******  PROVENANT DE LA CHAINE DES TOURNEES DE LIVRAISON                     
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL991/F91.BTL001BD
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  TABLE GENERALISEES (CARSP)                                           
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  TABLE DES MARQUES                                                    
#    RSGA22D  : NAME=RSGA22D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22D /dev/null
# ******  TABLE DES VENTES/ADRESSES                                            
#    RSGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02D /dev/null
# ******  TABLE DES VENTES/REGLEMENT                                           
#    RSGV10D  : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
# ******  TABLE DES LIGNES DE VENTES ARTICLES                                  
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
# ******  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                        
#    RSGV15D  : NAME=RSGV15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15D /dev/null
# ******  TABLE DES VENTES/QUANTITES DISPONIBLES                               
#    RSGV21D  : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21D /dev/null
# ******  FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FTL055 ${DATA}/PTEM/GTL55DAA.TL0055AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL055 
       JUMP_LABEL=GTL55DAB
       ;;
(GTL55DAB)
       m_CondExec 04,GE,GTL55DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 6 0                                                                 
# ********************************************************************         
#   CHARGEMENT DES BONS DE REPRISES   DANS LA D BASE IMPRESSION                
#   REPRISE: NON BACKOUT DU STEP LOGGING IMS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL55DAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL55DAD
       ;;
(GTL55DAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BTL060,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL55DR1)                                             
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL55DR1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#                                                                              
# ******  FICHIER DES BONS DE REPRISES EXTRAIT                                 
       m_FileAssign -d SHR -g ${G_A1} FTL055 ${DATA}/PTEM/GTL55DAA.TL0055AD
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DD,MODE=U,REST=(YES,GTL55DR1)                       
# DIGVIP   FILE NAME=DI0000ID,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL55D01
# ******  FDATE JJMMSSAA DATE D EDITION DES BD                                 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGD 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL55DZA
       ;;
(GTL55DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL55DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
