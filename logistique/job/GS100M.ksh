#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS100M.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGS100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/22 AT 09.42.11 BY BURTECA                      
#    STANDARDS: P  JOBSET: GS100M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGS107 :   CREATION FICHIER FGS100  :  MAJ DES STOCKS + HISTO STOC         
#                       FICHIER FEL030  :  ANOMALIES                           
#              RTGA01 - NCGFC : SI CODE GS107 = N => TRAITEMENT PARIS          
#                               SI CODE GS107 = O => TRAITEMENT FILIAL         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS100MA
       ;;
(GS100MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS100MAA
       ;;
(GS100MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  PARAMETRAGE PGM: SOUS TABLE NCGFC POUR 'GS107'                       
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  MARQUES                                                              
#    RSGA22   : NAME=RSGA22M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# ******  CODES VENDEURS                                                       
#    RSGV31   : NAME=RSGV31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31 /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER ENCAISSEMENT VENANT DE EL001R                                
       m_FileAssign -d SHR -g +0 FEMPORTE ${DATA}/PXX0/F89.BGS105AM
#                                                                              
# ******  FICHIER SERVANT A LA MAJ DES STOCKS                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FGS100 ${DATA}/PTEM/GS100MAA.BGS107AM
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FEL030 ${DATA}/PTEM/GS100MAA.BGS107BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS107 
       JUMP_LABEL=GS100MAB
       ;;
(GS100MAB)
       m_CondExec 04,GE,GS100MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FONCTION DE LA CARTE FCFONC FIC STOCK ET STAT          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAD
       ;;
(GS100MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****  PARAMETRAGE PGM                                                       
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GS100MAD
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GS100MAD
#                                                                              
# *****  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                          
#    RSMQ15   : NAME=RSMQ15M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FTS054 ${DATA}/PXX0/F89.BNM154AM
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FTS056 ${DATA}/PXX0/F89.BNM156AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GS100MAE
       ;;
(GS100MAE)
       m_CondExec 04,GE,GS100MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAG
       ;;
(GS100MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F89.BNM154AM
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100MAG.BNM154BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_17 1 CH 17
 /FIELDS FLD_BI_18_8 18 CH 8
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100MAH
       ;;
(GS100MAH)
       m_CondExec 00,EQ,GS100MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAJ
       ;;
(GS100MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F89.BNM156AM
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100MAJ.BNM156BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_17 1 CH 17
 /FIELDS FLD_BI_18_8 18 CH 8
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 116 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100MAK
       ;;
(GS100MAK)
       m_CondExec 00,EQ,GS100MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAM
       ;;
(GS100MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS100MAA.BGS107AM
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100MAM.BGS107CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4  "   "
 /FIELDS FLD_CH_89_24 89 CH 24
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_18_8 18 CH 8
 /FIELDS FLD_CH_1_88 01 CH 88
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = (113,,116) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_88,CST_1_4,FLD_CH_89_24
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS100MAN
       ;;
(GS100MAN)
       m_CondExec 00,EQ,GS100MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR                                           
#  DATE(VENTE)  SOCIETE   LIEU   ZONE(PRIX)  CAISSE  TERMINAL TRANSACT         
#           SORT FIELDS = (18,8,A,1,17,A)                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAQ
       ;;
(GS100MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS100MAM.BGS107CM
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GS100MAJ.BNM156BM
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100MAQ.BGS100BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_8 18 CH 8
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100MAR
       ;;
(GS100MAR)
       m_CondExec 00,EQ,GS100MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#  TRI DU FICHIER DES STOCKS DU JOUR                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAT PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAT
       ;;
(GS100MAT)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# SORTIN   FILE  NAME=BGS107CM,MODE=I                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GS100MAG.BNM154BM
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100MAT.BGS122AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_96_8 96 CH 8
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_96_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100MAU
       ;;
(GS100MAU)
       m_CondExec 00,EQ,GS100MAT ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS122 :                                                                    
#  REPRISE : OUI SI FIN ANORMALE .                                             
#           SINON  FAIRE 1 RECOVER AVEC LE RBA DU QUIESCE 1� STEP              
#            POUR : PMDMQ00.RSMQ15M ET                                         
#                 : PMDGS00.RSGS30M ET                                         
#                   PPDGS00.RSGS40  EST LOGISTIQUE                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MAX PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS100MAX
       ;;
(GS100MAX)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE (SOUS TABLES PARAMETRES)                           
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  RELATION ARTICLE ET ZONE DE PRIX                                     
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  TABLE DES STOCKS                                                     
#    RSGS30   : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER LG 113                                                       
       m_FileAssign -d SHR -g ${G_A7} FGS100 ${DATA}/PTEM/GS100MAT.BGS122AM
# ******  FICHIER DES ANOMALIES LG 110                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FGS103 ${DATA}/PTEM/GS100MAX.BGS103AM
#                                                                              
# ******  STOCK MAGASIN                                                        
#    RSGS30   : NAME=RSGS30M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  MOUVEMENT DE STOCK LGT                                               
#    RSGS40   : NAME=RSGS40,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGS40M  : NAME=RSGS40M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS40M /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSAN00   : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS122 
       JUMP_LABEL=GS100MAY
       ;;
(GS100MAY)
       m_CondExec 04,GE,GS100MAX ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU FICHIER DES ANOMALIES TRAITEMENT DU DES CAISSES                 
#            FEL032 = FICHIER ANOS DU PROG BGS100                              
#            FGS103 = FICHIER ANOS DU PROG BGS103                              
#  TRI     : SORT FIELDS=(1,6,A,19,3,A,22,5,A,7,7,A)                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MBA PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS100MBA
       ;;
(GS100MBA)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GS100MAA.BGS107BM
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GS100MAX.BGS103AM
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100MBA.BGS106AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_19_3 19 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_19_3 ASCENDING,
   FLD_CH_22_5 ASCENDING,
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100MBB
       ;;
(GS100MBB)
       m_CondExec 00,EQ,GS100MBA ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES ANOMALIES DU TRAITEMENT DES CAISSES (BGS100+BGS122)             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100MBD PGM=BGS106     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GS100MBD
       ;;
(GS100MBD)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  FICHIER D'EDITION                                                    
       m_FileAssign -d SHR -g ${G_A10} FEL032 ${DATA}/PTEM/GS100MBA.BGS106AM
# ******  EDITION DES ANOMALIES                                                
# ** ETAT SUPPRIME LE 290101 PLUS UTILISE A DAL CGA  *****************         
# IFEL031  REPORT SYSOUT=(9,IFEL031)                                           
       m_OutputAssign -c "*" IFEL031
       m_ProgramExec BGS106 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS100MZA
       ;;
(GS100MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS100MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
