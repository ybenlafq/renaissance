#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GD009P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGD009 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/03/22 AT 16.08.06 BY BURTECO                      
#    STANDARDS: P  JOBSET: GD009P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DE LA TABLE RTGD00                                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GD009PA
       ;;
(GD009PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GD009PAA
       ;;
(GD009PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGD009P
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GD009PAA
       m_ProgramExec IEFBR14 "RDAR,GD009P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PAD
       ;;
(GD009PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# ********************************************                                 
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGD009P
       m_OutputAssign -c X SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GD009PAE
       ;;
(GD009PAE)
       m_CondExec 00,EQ,GD009PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGD009 : EPURATION DES TABLES DE DESTOCKAGE                                 
#  REPRISE: OUI SI FIN ANORMALE                                                
#             A T T E N T I O N  A U  G D G           ********                 
#  REPRISE : NON SI FIN NORMALE ==> FAIRE UN RECOVER TO RBA POUR LES           
#       TABLE RTGDXX A PARTIR DU QUIESCE DU STEP PRECEDENT                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GD009PAG
       ;;
(GD009PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g +0 FGD09 ${DATA}/PGD0/F07.BGD009AP
# ******  VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLES EN LECTURE ET SUPPRESSION                                     
#    RSGD00   : NAME=RSGD00,MODE=(U,N) - DYNAM=YES                             
# -X-GD009PR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGD00 /dev/null
# ******  TABLES ACCEDEES                                                      
#    RSGD05   : NAME=RSGD05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD05 /dev/null
#    RSGD10   : NAME=RSGD10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD10 /dev/null
#    RSGD25   : NAME=RSGD25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD25 /dev/null
#    RSGD40   : NAME=RSGD40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD40 /dev/null
#    RSGD50   : NAME=RSGD50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD50 /dev/null
#    RSGD75   : NAME=RSGD75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGD75 /dev/null
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -g +1 RTGD05 ${DATA}/PTEM/GD009PAG.GD0005UP
       m_FileAssign -d NEW,CATLG,DELETE -r 183 -g +1 RTGD10 ${DATA}/PTEM/GD009PAG.GD0010UP
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 RTGD25 ${DATA}/PTEM/GD009PAG.GD0025UP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 RTGD40 ${DATA}/PTEM/GD009PAG.GD0040UP
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 RTGD50 ${DATA}/PTEM/GD009PAG.GD0050UP
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 RTGD75 ${DATA}/PTEM/GD009PAG.GD0075UP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -g +1 FGD11 ${DATA}/PGD0/F07.BGD009AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGD009 
       JUMP_LABEL=GD009PAH
       ;;
(GD009PAH)
       m_CondExec 04,GE,GD009PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SERVANT A LOADER LE RTGD10 (SUR L'INDEX CLUSTER)           
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GD009PAJ
       ;;
(GD009PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GD009PAG.GD0010UP
       m_FileAssign -d NEW,CATLG,DELETE -r 183 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD10RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_26 1 CH 26
 /KEYS
   FLD_CH_1_26 ASCENDING
 /* Record Type = F  Record Length = 183 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD009PAK
       ;;
(GD009PAK)
       m_CondExec 00,EQ,GD009PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD10                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GD009PAM
       ;;
(GD009PAM)
       m_CondExec ${EXAAU},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PGD0/F07.RELOAD.GD10RP
# ******  TABLE EN MISE A JOUR                                                 
#    RSGD10   : NAME=RSGD10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD10 /dev/null
# ******                                                                       
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD009P_GD009PAM_RTGD10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PAN
       ;;
(GD009PAN)
       m_CondExec 04,GE,GD009PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SERVANT A LOADER LE RTGD05 (SUR L'INDEX CLUSTER)           
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GD009PAQ
       ;;
(GD009PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GD009PAG.GD0005UP
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD05RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_39_9 39 CH 9
 /FIELDS FLD_CH_1_14 1 CH 14
 /FIELDS FLD_CH_23_13 23 CH 13
 /KEYS
   FLD_CH_1_14 ASCENDING,
   FLD_CH_23_13 ASCENDING,
   FLD_CH_39_9 ASCENDING
 /* Record Type = F  Record Length = 81 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD009PAR
       ;;
(GD009PAR)
       m_CondExec 00,EQ,GD009PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD05                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GD009PAT
       ;;
(GD009PAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A5} SYSREC ${DATA}/PGD0/F07.RELOAD.GD05RP
# ******  TABLE EN MISE A JOUR                                                 
#    RSGD05   : NAME=RSGD05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD05 /dev/null
# ******                                                                       
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD009P_GD009PAT_RTGD05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PAU
       ;;
(GD009PAU)
       m_CondExec 04,GE,GD009PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SERVANT A LOADER LE RTGD25 (SUR L'INDEX CLUSTER)           
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GD009PAX
       ;;
(GD009PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GD009PAG.GD0025UP
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD25RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD009PAY
       ;;
(GD009PAY)
       m_CondExec 00,EQ,GD009PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD25                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBA
       ;;
(GD009PBA)
       m_CondExec ${EXABO},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A7} SYSREC ${DATA}/PGD0/F07.RELOAD.GD25RP
# ******  TABLE EN MISE A JOUR                                                 
#    RSGD25   : NAME=RSGD25,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD25 /dev/null
# ******                                                                       
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD009P_GD009PBA_RTGD25.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PBB
       ;;
(GD009PBB)
       m_CondExec 04,GE,GD009PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SERVANT A LOADER LE RTGD40 (SUR L'INDEX CLUSTER)           
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBD
       ;;
(GD009PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GD009PAG.GD0040UP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD40RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_28 1 CH 28
 /KEYS
   FLD_CH_1_28 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD009PBE
       ;;
(GD009PBE)
       m_CondExec 00,EQ,GD009PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD40                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBG
       ;;
(GD009PBG)
       m_CondExec ${EXABY},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A9} SYSREC ${DATA}/PGD0/F07.RELOAD.GD40RP
# ******  TABLE EN MISE A JOUR                                                 
#    RSGD40   : NAME=RSGD40,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD40 /dev/null
# ******                                                                       
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD009P_GD009PBG_RTGD40.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PBH
       ;;
(GD009PBH)
       m_CondExec 04,GE,GD009PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SERVANT A LOADER LE RTGD50 (SUR L'INDEX CLUSTER)           
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBJ
       ;;
(GD009PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GD009PAG.GD0050UP
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD50RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_2 24 CH 2
 /FIELDS FLD_CH_23_1 23 CH 1
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_24_2 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_23_1 ASCENDING
 /* Record Type = F  Record Length = 40 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD009PBK
       ;;
(GD009PBK)
       m_CondExec 00,EQ,GD009PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD50                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBM
       ;;
(GD009PBM)
       m_CondExec ${EXACI},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A11} SYSREC ${DATA}/PGD0/F07.RELOAD.GD50RP
# ******  TABLE EN MISE A JOUR                                                 
#    RSGD50   : NAME=RSGD50,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD50 /dev/null
# ******                                                                       
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD009P_GD009PBM_RTGD50.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PBN
       ;;
(GD009PBN)
       m_CondExec 04,GE,GD009PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SERVANT A LOADER LE RTGD75 (SUR L'INDEX CLUSTER)           
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBQ
       ;;
(GD009PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GD009PAG.GD0075UP
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD75RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_32_3 32 CH 3
 /FIELDS FLD_CH_35_7 35 CH 7
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_18_7 18 CH 7
 /FIELDS FLD_CH_55_3 55 CH 3
 /KEYS
   FLD_CH_18_7 ASCENDING,
   FLD_CH_25_7 ASCENDING,
   FLD_CH_55_3 ASCENDING,
   FLD_CH_32_3 ASCENDING,
   FLD_CH_35_7 ASCENDING
 /* Record Type = F  Record Length = 63 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD009PBR
       ;;
(GD009PBR)
       m_CondExec 00,EQ,GD009PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD75                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD009PBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GD009PBT
       ;;
(GD009PBT)
       m_CondExec ${EXACS},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A13} SYSREC ${DATA}/PGD0/F07.RELOAD.GD75RP
# ******  TABLE EN MISE A JOUR                                                 
#    RSGD75   : NAME=RSGD75,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD75 /dev/null
# ******                                                                       
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD009P_GD009PBT_RTGD75.sysload
       m_UtilityExec
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD009PBU
       ;;
(GD009PBU)
       m_CondExec 04,GE,GD009PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GD009PZA
       ;;
(GD009PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD009PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
