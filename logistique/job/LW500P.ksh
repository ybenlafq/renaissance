#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LW500P.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLW500 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/10/10 AT 16.44.21 BY BURTEC2                      
#    STANDARDS: P  JOBSET: LW500P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DU REPRO                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=LW500PA
       ;;
(LW500PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2015/10/10 AT 16.44.21 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: LW500P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'INTERFA IMAG STO'                      
# *                           APPL...: REPPARIS                                
# *                           WAIT...: LW500P LW500P2 LW500P3                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=LW500PAA
       ;;
(LW500PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *************************************                                        
#  DEPEND NCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
# ***** FLUX EN PROVENANCE                                                     
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.BLW590AP
       m_FileAssign -d NEW,CATLG,DELETE -r 184 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LW500PAA.BLW590BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_1  "090"
 /FIELDS FLD_CH_1_181 1 CH 181
 /COPY
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_1,FLD_CH_1_181
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LW500PAB
       ;;
(LW500PAB)
       m_CondExec 00,EQ,LW500PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU REPRO                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW500PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LW500PAD
       ;;
(LW500PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***** FLUX EN PROVENANCE                                                     
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.BLW595AP
       m_FileAssign -d NEW,CATLG,DELETE -r 184 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LW500PAD.BLW595BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_1  "095"
 /FIELDS FLD_CH_1_181 1 CH 181
 /COPY
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_1,FLD_CH_1_181
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LW500PAE
       ;;
(LW500PAE)
       m_CondExec 00,EQ,LW500PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU REPRO                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW500PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LW500PAG
       ;;
(LW500PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***** FLUX EN PROVENANCE                                                     
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.BLW520AP
       m_FileAssign -d NEW,CATLG,DELETE -r 184 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LW500PAG.BLW520BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_1  "120"
 /FIELDS FLD_CH_1_181 1 CH 181
 /COPY
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_1,FLD_CH_1_181
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=LW500PAH
       ;;
(LW500PAH)
       m_CondExec 00,EQ,LW500PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW500PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LW500PAJ
       ;;
(LW500PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/LW500PAA.BLW590BP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/LW500PAD.BLW595BP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/LW500PAG.BLW520BP
       m_FileAssign -d NEW,CATLG,DELETE -r 184 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/LW500PAJ.BLW500DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_27_18 27 CH 18
 /FIELDS FLD_BI_4_6 4 CH 6
 /FIELDS FLD_BI_146_18 146 CH 18
 /FIELDS FLD_BI_164_20 164 CH 20
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_6 ASCENDING,
   FLD_BI_27_18 ASCENDING,
   FLD_BI_164_20 ASCENDING,
   FLD_BI_146_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LW500PAK
       ;;
(LW500PAK)
       m_CondExec 00,EQ,LW500PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BLW500 : INTERFACE IMAGE DE STOCK                                          
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW500PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LW500PAM
       ;;
(LW500PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *******                                                                      
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# *******                                                                      
#    RSLW99   : NAME=RSLW99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW99 /dev/null
# *******                                                                      
#    RSLW09   : NAME=RSLW09,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW09 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FIMS00 ${DATA}/PTEM/LW500PAJ.BLW500DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW500 
       JUMP_LABEL=LW500PAN
       ;;
(LW500PAN)
       m_CondExec 04,GE,LW500PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BLW015 : INTERFACE IMAGE DE STOCK                                          
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LW500PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LW500PAQ
       ;;
(LW500PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *******                                                                      
#    RSLW09   : NAME=RSLW09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW09 /dev/null
# *******                                                                      
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# *******                                                                      
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#                                                                              
       m_OutputAssign -c 9 -w ILW015 ILW015
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLW015 
       JUMP_LABEL=LW500PAR
       ;;
(LW500PAR)
       m_CondExec 04,GE,LW500PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LW500PZA
       ;;
(LW500PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LW500PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
