#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE15L.ksh                       --- VERSION DU 08/10/2016 22:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGIE15 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/15 AT 12.37.49 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GIE15L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIE015  : CREE FIC A PARTIR DE LA RTGS15                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GIE15LA
       ;;
(GIE15LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2001/02/15 AT 12.37.49 BY BURTEC5                
# *    JOBSET INFORMATION:    NAME...: GIE15L                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'EDITION INVT DEPOT'                    
# *                           APPL...: REPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GIE15LAA
       ;;
(GIE15LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES PRETS ENTREPOT                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
#    RSGS15L  : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15L /dev/null
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
#                                                                              
# ******  TABLE LIEUX                                                          
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
#                                                                              
# ******  FIC DE SORTIES                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 88 -g +1 FIE015 ${DATA}/PBI0/GIE15LAA.BIE015AL
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE015 
       JUMP_LABEL=GIE15LAB
       ;;
(GIE15LAB)
       m_CondExec 04,GE,GIE15LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        SORT 1,6,A 14,1,A 86,3,A,61,5,A,7,7,A                                 
#        REPRISE : OUI                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE15LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GIE15LAD
       ;;
(GIE15LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PBI0/GIE15LAA.BIE015AL
       m_FileAssign -d NEW,CATLG,DELETE -r 88 -g +1 SORTOUT ${DATA}/PBI0/GIE15LAD.BIE015BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_61_5 61 CH 5
 /FIELDS FLD_CH_86_3 86 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_14_1 ASCENDING,
   FLD_CH_86_3 ASCENDING,
   FLD_CH_61_5 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 88 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE15LAE
       ;;
(GIE15LAE)
       m_CondExec 00,EQ,GIE15LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE020  : LISTE DU DEPOT QUI DOIT BASCULER LE PRET RETOUR FOURN EN          
#            SOIT DE LA TABLE RTGS15 SSLIEU 'P' TYPE ' F' VERS RTGS60          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE15LAG PGM=BIE020     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE15LAG
       ;;
(GIE15LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  FIC DE SORTIES                                                       
       m_FileAssign -d SHR -g ${G_A2} FIE020 ${DATA}/PBI0/GIE15LAD.BIE015BL
# ******  FIC D IMPRESSION                                                     
       m_OutputAssign -c 9 -w IFI020 IFI020
       m_ProgramExec BIE020 
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GIE15LZA
       ;;
(GIE15LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE15LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
