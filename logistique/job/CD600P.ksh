#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CD600P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCD600 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/22 AT 09.03.21 BY BURTECA                      
#    STANDARDS: P  JOBSET: CD600P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  DELETE DU FICHIER CD6000AP ZIPPE                                            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CD600PA
       ;;
(CD600PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2015/06/22 AT 09.03.21 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CD600P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'EXTRAC DAC'                            
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CD600PAA
       ;;
(CD600PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CD600PAB
       ;;
(CD600PAB)
       m_CondExec 16,NE,CD600PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BCD600 : BATCH D'EXTRACTION                                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAD
       ;;
(CD600PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/CD600P01
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM41   : NAME=RSRM41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM41 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  TABLES EN MAJ                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -t LSEQ -g +1 FBCD60 ${DATA}/PTEM/CD600PAD.BCD600AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCD600 
       JUMP_LABEL=CD600PAE
       ;;
(CD600PAE)
       m_CondExec 04,GE,CD600PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EXTRACTION POUR PGM BCD610                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAG
       ;;
(CD600PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/CD600PAD.BCD600AP
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CD600PAG.BCD610AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_5_6 5 CH 6
 /FIELDS FLD_CH_41_7 41 CH 7
 /KEYS
   FLD_CH_41_7 ASCENDING,
   FLD_CH_5_6 ASCENDING
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CD600PAH
       ;;
(CD600PAH)
       m_CondExec 00,EQ,CD600PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BCD610 : BATCH D'EXTRACTION                                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAJ
       ;;
(CD600PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A2} FBCD60E ${DATA}/PTEM/CD600PAG.BCD610AP
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM41   : NAME=RSRM41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM41 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  FICHIER D'EXTRACION A ENVOYER VIA LA GATEWAY                         
       m_FileAssign -d NEW,CATLG,DELETE -r 455 -t LSEQ FBCD60S ${DATA}/PXX0.F07.CD6000BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCD610 
       JUMP_LABEL=CD600PAK
       ;;
(CD600PAK)
       m_CondExec 04,GE,CD600PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DU FICHER ACCESSOIRE / NRM DACEM                                        
# ********************************************************************         
# AAU      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# * ":CD6000BP""                            CD6000BP.TXT                       
# * ALLOCATION BIDON POUR CROSS-REF CORTEX                                     
# CD6000SP FILE  DYNAM=YES,NAME=CD6000SP,MODE=N                                
# SYSIN    DATA  *,MBR=CD600PN1                                                
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -TRAN(ASCII850)                                                             
#  -ARCHIVE(":CD6000SP"")                                                      
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":CD6000BP"",CD6000BP.TXT)                                      
#  ":CD6000BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ZIP DU FICHER ACCESSOIRE / NRM DACEM MIS EN COMMENTAIRE LE 220415           
# ********************************************************************         
# AAU      STEP  PGM=PKZIP                                                     
# //STEPLIB  DD DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                               
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# * ":CD6000BP""                            CD6000BP.TXT                       
# * ALLOCATION BIDON POUR CROSS-REF CORTEX                                     
# CD6000AP FILE  DYNAM=YES,NAME=CD6000AP,MODE=N                                
# SYSIN    DATA  *,MBR=CD600P                                                  
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -TEXT                                                                       
#  -TRAN(ASCII850)                                                             
#  -ARCHIVE(":CD6000AP"")                                                      
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":CD6000BP"",CD6000BP.TXT)                                      
#  ":CD6000BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAM PGM=JVMLDM76   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAM
       ;;
(CD600PAM)
       m_CondExec ${EXAAU},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 430 -t LSEQ FICZIP ${DATA}/PXX0.F07.CD6000AP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600P.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CD6000AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  DELETE DU FICHIER CD6000BP                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAQ
       ;;
(CD600PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CD600PAR
       ;;
(CD600PAR)
       m_CondExec 16,NE,CD600PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTCD600P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAT
       ;;
(CD600PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/CD600PAT.FCD6000P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  CHANGEMENT PCL => CD600P <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCD600P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=CD600PAX
       ;;
(CD600PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.CD600PAT.FCD6000P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#   PGM BCD600 : BATCH D'EXTRACTION  NEW   NEW   NEW  25.03.2014               
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBA
       ;;
(CD600PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/CD600P02
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM41   : NAME=RSRM41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM41 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  TABLES EN MAJ                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -t LSEQ -g +1 FBCD60 ${DATA}/PTEM/CD600PBA.FCD600AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCD600 
       JUMP_LABEL=CD600PBB
       ;;
(CD600PBB)
       m_CondExec 04,GE,CD600PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EXTRACTION POUR PGM BCD610                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBD
       ;;
(CD600PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/CD600PBA.FCD600AP
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CD600PBD.FCD610AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_41_7 41 CH 7
 /FIELDS FLD_CH_5_6 5 CH 6
 /KEYS
   FLD_CH_41_7 ASCENDING,
   FLD_CH_5_6 ASCENDING
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CD600PBE
       ;;
(CD600PBE)
       m_CondExec 00,EQ,CD600PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BCD610 : BATCH D'EXTRACTION                                            
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBG
       ;;
(CD600PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A5} FBCD60E ${DATA}/PTEM/CD600PBD.FCD610AP
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM41   : NAME=RSRM41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM41 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  FICHIER D'EXTRACION A ENVOYER VIA LA GATEWAY                         
       m_FileAssign -d NEW,CATLG,DELETE -r 455 -t LSEQ FBCD60S ${DATA}/PXX0.F07.FD6000BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCD610 
       JUMP_LABEL=CD600PBH
       ;;
(CD600PBH)
       m_CondExec 04,GE,CD600PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DU FICHER ACCESSOIRE / NRM DACEM mis en commentaire le 220415           
# ********************************************************************         
# ACD      STEP  PGM=PKZIP                                                     
# //STEPLIB  DD DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                               
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# * ":FD6000BP""                            FD6000BP.TXT                       
# * ALLOCATION BIDON POUR CROSS-REF CORTEX                                     
# FD6000AP FILE  DYNAM=YES,NAME=FD6000AP,MODE=N                                
# SYSIN    DATA  *,MBR=CD600PX1                                                
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -TEXT                                                                       
#  -TRAN(ASCII850)                                                             
#  -ARCHIVE(":FD6000AP"")                                                      
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":FD6000BP"",FD6000BP.TXT)                                      
#  ":FD6000BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBJ PGM=JVMLDM76   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBJ
       ;;
(CD600PBJ)
       m_CondExec ${EXACD},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ FICZIP ${DATA}/PXX0.F07.FD6000AP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PX1.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  DELETE DU FICHIER FD6000BP                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBM PGM=IDCAMS     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBM
       ;;
(CD600PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PBM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CD600PBN
       ;;
(CD600PBN)
       m_CondExec 16,NE,CD600PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FD6000AP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBQ PGM=EZACFSM1   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBQ
       ;;
(CD600PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PBQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/CD600PBQ.FCD600PP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FD6000AP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CD600PBT PGM=FTP        ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=CD600PBT
       ;;
(CD600PBT)
       m_CondExec ${EXACS},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PBT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.CD600PBQ.FCD600PP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CD600PZA
       ;;
(CD600PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CD600PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
