#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS140Y.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGS140 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/07/11 AT 14.59.56 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GS140Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  AUCUNE MISE A JOUR DANS CETTE CHAINE                                        
#          PEUT ETRE RELANCEE SI PROBLEME                                      
# ********************************************************************         
#  BSM140 : EXTRACTION A PARTIR DES TABLES DB2 POUR CREATION DU FICHIE         
#           FSM140 (VENTES ET STOCKS DE DRA)                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS140YA
       ;;
(GS140YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS140YAA
       ;;
(GS140YAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 945                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : VTK)                                
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  ZONE DE PRIX                                                         
#    RSGA59Y  : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59Y /dev/null
# ******  COMMISSIONS PAR ZONE DE PRIX                                         
#    RSGA62Y  : NAME=RSGA62Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62Y /dev/null
# ******  COMMISSIONS BBTE (PRIMES VENDEURS)                                   
#    RSGA75Y  : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75Y /dev/null
# ******  RESERVATION SUR CMDE FOURNISSEUR                                     
#    RSGF55Y  : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55Y /dev/null
# ******  STOCK SOUS-LIEU/ENTREPOT                                             
#    RSGS10Y  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10Y /dev/null
# ******  STOCK SOUS-LIEU/MAGASIN                                              
#    RSGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  VENTES MAGASINS                                                      
#    RSHV04Y  : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04Y /dev/null
# ******  FICHIER VENTES ET STOCKS DRA                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 FSM140 ${DATA}/PTEM/GS140YAA.BSM140AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM140 
       JUMP_LABEL=GS140YAB
       ;;
(GS140YAB)
       m_CondExec 04,GE,GS140YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR PGM BSM141                                            
#       TRIE PAR FAMILLE , CODIC ET GROUPE D'EDTION                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS140YAD
       ;;
(GS140YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS DRA                                         
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS140YAA.BSM140AY
# ******  FICHIER VENTES ET STOCKS DRA   ( TRIE )                              
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 SORTOUT ${DATA}/PTEM/GS140YAD.BSM140BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_234_7 234 CH 7
 /FIELDS FLD_CH_241_5 241 CH 5
 /FIELDS FLD_CH_4_2 4 CH 2
 /KEYS
   FLD_CH_241_5 ASCENDING,
   FLD_CH_234_7 ASCENDING,
   FLD_CH_4_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140YAE
       ;;
(GS140YAE)
       m_CondExec 00,EQ,GS140YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM141 :     PRE-FORMATAGE DU FICHIER D'EDITION                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS140YAG
       ;;
(GS140YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS DRA   ( TRIE )                              
       m_FileAssign -d SHR -g ${G_A2} FSM140 ${DATA}/PTEM/GS140YAD.BSM140BY
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : NCGFC)                              
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING IMBRICATION)             
#    RSGA09Y  : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09Y /dev/null
# ******  TABLE FAMILLE (EDITIONS DES ETATS)                                   
#    RSGA11Y  : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11Y /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING)                         
#    RSGA12Y  : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12Y /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******  TABLE ASSOCIATION CODES VALEURS MARKETING/CODES VALEUR DECRI         
#    RSGA25Y  : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25Y /dev/null
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26Y  : NAME=RSGA26Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26Y /dev/null
# ******  RESERVATIONS SUR COMMANDE FOURNISSEURS                               
#    RSGA53Y  : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53Y /dev/null
# ******  FORMATAGE DU FICHIER D'EDITION STOCK ET VENTES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 FSM141 ${DATA}/PTEM/GS140YAG.BSM141AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM141 
       JUMP_LABEL=GS140YAH
       ;;
(GS140YAH)
       m_CondExec 04,GE,GS140YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI AVANT EDITION                                                          
#        CRITERES GROUPE / WSEQED/(CMARKETING/CVMARKETING)                     
#                 TOTVTE DECROISSANT/CODIC                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS140YAJ
       ;;
(GS140YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE                            
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS140YAG.BSM141AY
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 SORTOUT ${DATA}/PTEM/GS140YAJ.BSM141BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_1_3 1 PD 3
 /FIELDS FLD_CH_353_10 353 CH 10
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_CH_338_10 338 CH 10
 /KEYS
   FLD_CH_7_2 ASCENDING,
   FLD_PD_1_3 ASCENDING,
   FLD_CH_338_10 ASCENDING,
   FLD_CH_353_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140YAK
       ;;
(GS140YAK)
       m_CondExec 00,EQ,GS140YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM142 : GENERATION DE L'EDITION                                            
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS140YAM
       ;;
(GS140YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26Y  : NAME=RSGA26Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26Y /dev/null
# ******  FICHIER D'EDITION STOCK ET VENTES TRIE                               
       m_FileAssign -d SHR -g ${G_A4} FSM141 ${DATA}/PTEM/GS140YAJ.BSM141BY
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 945                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ****** ETAT STOCK ET VENTES POUR LE SIEGE(DV ET ACHATS)                      
       m_OutputAssign -c 9 -w ISM140 ISM140
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM142 
       JUMP_LABEL=GS140YAN
       ;;
(GS140YAN)
       m_CondExec 04,GE,GS140YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS140YZA
       ;;
(GS140YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS140YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
