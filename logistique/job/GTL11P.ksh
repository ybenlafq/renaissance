#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL11P.ksh                       --- VERSION DU 20/10/2016 12:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGTL11 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 09.11.33 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GTL11P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
#                                                                              
# ********************************************************************         
#  PGM : BTL110                                                                
#  EXTRACTION PERMETTANT D'ALIMENTER LA TABLE RTTL09                           
#  REPRISE: OUI SI ABEND                                                       
#           NON SI FIN NORMALE FAIRE UN RECOVER A LA F.I.C DE RTTL09           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL11PA
       ;;
(GTL11PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL11PAA
       ;;
(GTL11PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES QUOTAS DE LIVRAISON         
#    RSGQ01   : NAME=RSGQ01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ01 /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23   : NAME=RSGV23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
# ************************************** TABLE DES TOUNEES (GENERALITE         
#    RSTL01   : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL01 /dev/null
# ************************************** TABLE DES TOURNEES (DETAILS)          
#    RSTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL02 /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04   : NAME=RSTL04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09   : NAME=RSTL09,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE HISTO VENTES                    
#    RSHV15   : NAME=RSHV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DIFDEP
# ************************************** PARAMETRE  H:HEBDO  M:MENSUEL         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11PAA
# ************************************** FTL110 ISSU DU DERNIER TRAIT          
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F07.FTL110AP
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL110N ${DATA}/PXX0/F07.FTL110AP
# ************************************** FIC D'EXTRACT FTL111                  
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111 ${DATA}/PXX0/F07.BTL111AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL11PAB
       ;;
(GTL11PAB)
       m_CondExec 04,GE,GTL11PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE : CUMUL DU FICHIER BTL111AP CREE PAR LE PROG BTL110 AVEC              
#          L'ANCIEN BTL111AP DU DERNIER TRAITEMENT                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAD
       ;;
(GTL11PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BTL111AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F07.BTL111AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A3} SORTOUT ${DATA}/PXX0/F07.BTL111AP
# File translated and concatened in SYSIN
       m_FileAssign -d SHR -C ${DATA}/CORTEX4.P.MTXTFIX1/GTL11PAD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 ""
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_3 FLD_CH_7_6 GE CST_1_9 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
 /* Record Type = F  Record Length = 120 */
 /INCLUDE CND_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PAE
       ;;
(GTL11PAE)
       m_CondExec 00,EQ,GTL11PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL111                                                                
#  INITIALISATION DES FICHIERS FTL111A  ET FTL111B                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAG
       ;;
(GTL11PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISE                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FIC D'EXTRACT ISSU DU BLT110          
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F07.FTL110AP
# ************************************** FIC D'EXTRACT ISSU DU TRI 1A          
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F07.BTL111AP
# ************************************** FICHIER FTL111A                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111A ${DATA}/PXX0/F07.FTL111AP
# ******  FICHIER FTL111B ALLANT VERS BTL122                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111B ${DATA}/PXX0/F07.FTL111BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL11PAH
       ;;
(GTL11PAH)
       m_CondExec 04,GE,GTL11PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2A : TRI DU FICHIER FTL111AP POUR CREATION DU MLT112AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAJ
       ;;
(GTL11PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F07.FTL111AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PAJ.MTL112AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PAK
       ;;
(GTL11PAK)
       m_CondExec 00,EQ,GTL11PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2B : TRI DU FICHIER FTL111AP POUR CREATION DU MTL112BP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAM
       ;;
(GTL11PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.FTL111AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PAM.MTL112BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PAN
       ;;
(GTL11PAN)
       m_CondExec 00,EQ,GTL11PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2C : TRI DU FICHIER FTL111BP POUR CREATION DU MTL112CP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAQ
       ;;
(GTL11PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PAQ.MTL112CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_89_2 89 CH 2
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PAR
       ;;
(GTL11PAR)
       m_CondExec 00,EQ,GTL11PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL112                                                                
#  CREATION DES ETATS ITL112A ET ITL112B                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAT
       ;;
(GTL11PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F07.FTL110AP
# ************************************** FICHIERS ISSU DU TRI 2A               
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL11PAJ.MTL112AP
# ************************************** FICHIERS ISSU DU TRI 2B               
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL11PAM.MTL112BP
# ************************************** FICHIERS ISSU DU TRI 2C               
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL11PAQ.MTL112CP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L                  
       m_OutputAssign -c 9 -w ITL112A ITL112A
# ************************************** ETAT DE STAT PAR PIECES               
       m_OutputAssign -c 9 -w ITL112B ITL112B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL11PAU
       ;;
(GTL11PAU)
       m_CondExec 04,GE,GTL11PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3A : TRI DU FICHIER FTL111AP POUR CREATION DU MTL113AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PAX
       ;;
(GTL11PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.FTL111AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PAX.MTL113AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PAY
       ;;
(GTL11PAY)
       m_CondExec 00,EQ,GTL11PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3B : TRI DU FICHIER FTL111AP POUR CREATION DU MTL113BP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBA
       ;;
(GTL11PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F07.FTL111AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PBA.MTL113BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PBB
       ;;
(GTL11PBB)
       m_CondExec 00,EQ,GTL11PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3C : TRI DU FICHIER FTL111BP POUR CREATION DU MTL113CP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBD
       ;;
(GTL11PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PBD.MTL113CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PBE
       ;;
(GTL11PBE)
       m_CondExec 00,EQ,GTL11PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3D : TRI DU FICHIER FTL111BP POUR CREATION DU MTL113CP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBG
       ;;
(GTL11PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PBG.MTL113DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PBH
       ;;
(GTL11PBH)
       m_CondExec 00,EQ,GTL11PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL113                                                                
#  CREATION DE L'ETAT ITL113                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBJ
       ;;
(GTL11PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F07.FTL110AP
# ************************************** FICHIERS ISSU DU TRI 3A               
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL11PAX.MTL113AP
# ************************************** FICHIERS ISSU DU TRI 3B               
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL11PBA.MTL113BP
# ************************************** FICHIERS ISSU DU TRI 3C               
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL11PBD.MTL113CP
# ************************************** FICHIERS ISSU DU TRI 3D               
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL11PBG.MTL113DP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR         
       m_OutputAssign -c 9 -w ITL113 ITL113
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL11PBK
       ;;
(GTL11PBK)
       m_CondExec 04,GE,GTL11PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4A : TRI DU FICHIER FTL111BP POUR CREATION DU FTL114AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBM
       ;;
(GTL11PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PBM.FTL114AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PBN
       ;;
(GTL11PBN)
       m_CondExec 00,EQ,GTL11PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4B : TRI DU FICHIER FTL111BP POUR CREATION DU FTL114BP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBQ
       ;;
(GTL11PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PBQ.FTL114BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PBR
       ;;
(GTL11PBR)
       m_CondExec 00,EQ,GTL11PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4C : TRI DU FICHIER FTL111BP POUR CREATION DU FTL114CP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBT
       ;;
(GTL11PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PBT.FTL114CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PBU
       ;;
(GTL11PBU)
       m_CondExec 00,EQ,GTL11PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL114                                                                
#  CREATION DES ETATS ITL114A ET ITL114B                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PBX
       ;;
(GTL11PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ************************************** TABLE DES ENTETES DE VENTES           
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23   : NAME=RSGV23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04   : NAME=RSTL04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A               
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL11PBM.FTL114AP
# ************************************** FICHIERS ISSU DU TRI 4B               
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL11PBQ.FTL114BP
# ************************************** FICHIERS ISSU DU TRI 4C               
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL11PBT.FTL114CP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L             
       m_OutputAssign -c 9 -w ITL114A ITL114A
# ************************************** STAT RETOURS MAGS PAR PIECES          
       m_OutputAssign -c 9 -w ITL114B ITL114B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL11PBY
       ;;
(GTL11PBY)
       m_CondExec 04,GE,GTL11PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 5A : TRI DU FICHIER FTL111BP POUR CREATION DU FTL115AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCA
       ;;
(GTL11PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PXX0/F07.FTL111BP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PCA.FTL115AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_106_2 106 CH 2
 /FIELDS FLD_CH_99_2 99 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_106_2 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_99_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PCB
       ;;
(GTL11PCB)
       m_CondExec 00,EQ,GTL11PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL115                                                                
#  CREATION DES ETATS ITL115A ET ITL115B                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCD
       ;;
(GTL11PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU TRI 5A               
       m_FileAssign -d SHR -g ${G_A29} FTL115 ${DATA}/PTEM/GTL11PCA.FTL115AP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** SYNTHESE RETOUR MAG PAR B.L           
       m_OutputAssign -c 9 -w ITL115A ITL115A
# ************************************** SYNTHESE RETOUR MAG PAR PIECE         
       m_OutputAssign -c 9 -w ITL115B ITL115B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL115 
       JUMP_LABEL=GTL11PCE
       ;;
(GTL11PCE)
       m_CondExec 04,GE,GTL11PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL116                                                                
#  CREATION DE L'ETAT ITL116                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCG
       ;;
(GTL11PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIVREURS                    
#    RSTL04   : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09   : NAME=RSTL09,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BTL110               
       m_FileAssign -d SHR -g ${G_A30} FTL110 ${DATA}/PXX0/F07.FTL110AP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE             
       m_OutputAssign -c 9 -w ITL116 ITL116
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL11PCH
       ;;
(GTL11PCH)
       m_CondExec 04,GE,GTL11PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL117                                                                
#  CREATION DU FICHIER FTL117AP                                                
#  REPRISE: OUI SI ABEND                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCJ
       ;;
(GTL11PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV         
#    RSGA13   : NAME=RSGA13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA13 /dev/null
# ************************************** TABLE DES FAMILLES                    
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES           
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT                       
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DIFDEP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11PCJ
# *****   FICHIER D'EXTRACT FTL117AP                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117 ${DATA}/PTEM/GTL11PCJ.FTL117AP
# *****   FICHIER A DESTINATION DE COPERNIC                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117B ${DATA}/PTEM/GTL11PCJ.FTL117BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL117 
       JUMP_LABEL=GTL11PCK
       ;;
(GTL11PCK)
       m_CondExec 04,GE,GTL11PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 6A : TRI DU FICHIER FTL117AP POUR CREATION DU FTL118AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCM
       ;;
(GTL11PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PCM.FTL118AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PCN
       ;;
(GTL11PCN)
       m_CondExec 00,EQ,GTL11PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL118                                                                
#  CREATION DE L'ETAT ITL118                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCQ
       ;;
(GTL11PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A               
       m_FileAssign -d SHR -g ${G_A32} FTL118 ${DATA}/PTEM/GTL11PCM.FTL118AP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11PCQ
# ************************************** MODE DE DELIV/MAG ET FAM              
       m_OutputAssign -c 9 -w ITL118 ITL118
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL11PCR
       ;;
(GTL11PCR)
       m_CondExec 04,GE,GTL11PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 7A : TRI DU FICHIER FTL117AP POUR CREATION DU FTL119AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCT
       ;;
(GTL11PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PCT.FTL119AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PCU
       ;;
(GTL11PCU)
       m_CondExec 00,EQ,GTL11PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL119                                                                
#  CREATION DE L'ETAT ITL119                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PCX
       ;;
(GTL11PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU TRI 7A               
       m_FileAssign -d SHR -g ${G_A34} FTL119 ${DATA}/PTEM/GTL11PCT.FTL119AP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ************************************** SYNTHESE DES MODES DE DELIV/F         
       m_OutputAssign -c 9 -w ITL119 ITL119
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL119 
       JUMP_LABEL=GTL11PCY
       ;;
(GTL11PCY)
       m_CondExec 04,GE,GTL11PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 8A : TRI DU FICHIER FTL117AP POUR CREATION DU FTL120AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDA
       ;;
(GTL11PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PDA.FTL120AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_38_2 38 CH 2
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_38_2 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PDB
       ;;
(GTL11PDB)
       m_CondExec 00,EQ,GTL11PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 8B : TRI DU FICHIER FTL117AP POUR CREATION DU FTL120BP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDD
       ;;
(GTL11PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PDD.FTL120BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_52_4 52 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PDE
       ;;
(GTL11PDE)
       m_CondExec 00,EQ,GTL11PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL120                                                                
#  CREATION DE L'ETAT ITL120                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDG
       ;;
(GTL11PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU TRI 8A               
       m_FileAssign -d SHR -g ${G_A37} FTL120 ${DATA}/PTEM/GTL11PDA.FTL120AP
# ************************************** FICHIERS ISSU DU TRI 8B               
       m_FileAssign -d SHR -g ${G_A38} FTL120C ${DATA}/PTEM/GTL11PDD.FTL120BP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11PDG
# ************************************** MODE DE DELIV/FAM ET MAGS             
       m_OutputAssign -c 9 -w ITL120 ITL120
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL120 
       JUMP_LABEL=GTL11PDH
       ;;
(GTL11PDH)
       m_CondExec 04,GE,GTL11PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 9A : TRI DU FICHIER FTL117AP POUR CREATION DU FTL121AP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDJ
       ;;
(GTL11PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PDJ.FTL121AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_38_2 38 CH 2
 /FIELDS FLD_PD_40_4 40 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_38_2 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PDK
       ;;
(GTL11PDK)
       m_CondExec 00,EQ,GTL11PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 9B : TRI DU FICHIER FTL117AP POUR CREATION DU FTL121BP                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDM
       ;;
(GTL11PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11PDM.FTL121BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11PDN
       ;;
(GTL11PDN)
       m_CondExec 00,EQ,GTL11PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL121                                                                
#  CREATION DE L'ETAT ITL121                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDQ
       ;;
(GTL11PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU TRI 9A               
       m_FileAssign -d SHR -g ${G_A41} FTL121 ${DATA}/PTEM/GTL11PDJ.FTL121AP
# ************************************** FICHIERS ISSU DU TRI 9B               
       m_FileAssign -d SHR -g ${G_A42} FTL121C ${DATA}/PTEM/GTL11PDM.FTL121BP
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11PDQ
# ************************************** SYNTHESE MODE DE DELIV/MAGS           
       m_OutputAssign -c 9 -w ITL121 ITL121
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL121 
       JUMP_LABEL=GTL11PDR
       ;;
(GTL11PDR)
       m_CondExec 04,GE,GTL11PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 10A : TRI DU FICHIER FTL117BP ISSU DU BTL117                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDT
       ;;
(GTL11PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PTEM/GTL11PCJ.FTL117BP
# ******  FICHIER A ENVOYER A COPERNIC VIA LA GATEWAY                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FTL117CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/REFORMAT LAYOUT rl_sumDBYTE x"20"
/FIELDS 
       FD_CH_1_3     1    3 CH,
       FD_sep_01     4    1 CH,
        
       FD_CH_5_4     5    4 CH,
       FD_sep_02     9    1 CH,
        
       FD_CH_10_3    10   3 CH,
       FD_sep_03     13   1 CH,
       FD_CH_14_3    14   3 CH,
       FD_sep_04     17   1 CH,
       FD_CH_18_7    18   7 CH,
       FD_sep_05     25   1 CH,
       FD_EN_26_10   26   EN 10,
       FD_sep_06     36   1 CH,
       FD_CH_37_3    37    3 CH,
       FD_sep_07     40    1 CH,
       FD_CH_41_3    41   3 CH,
       FD_sep_08     44   1 CH
/DERIVEDFIELD total_SFF   FD_EN_26_10   (9999999V99)
/KEYS FD_CH_1_3, FD_CH_5_4, FD_CH_10_3,   FD_CH_14_3, FD_CH_18_7, FD_CH_37_3, FD_CH_41_3
/SUMMARIZE TOTAL FD_EN_26_10
/MT_OUTFILE_ASG SORTOUT
/DYNAMICLAYOUT rl_sum POSITIONAL RETAINCOMPOSITE VALUE
        FD_CH_1_3    ALIAS BFD_CH_1_3,
        FD_sep_01       ALIAS BFD_sep_01,
        FD_CH_5_4    ALIAS BFD_CH_5_4,
        FD_sep_02       ALIAS BFD_sep_02,
        FD_CH_10_3  ALIAS BFD_CH_10_3,
        FD_sep_03       ALIAS BFD_sep_03,
        FD_CH_14_3  ALIAS BFD_CH_14_3,
        FD_sep_04       ALIAS BFD_sep_04,
        FD_CH_18_7  ALIAS BFD_CH_18_7,
        FD_sep_05       ALIAS BFD_sep_05,
        total_SFF   ALIAS total_OVERLAY,
        FD_sep_06       ALIAS BFD_sep_06,
        FD_CH_37_3  ALIAS AFTER_FD_CH_37_3,
        FD_sep_07       ALIAS BFD_sep_07,
        FD_CH_41_3  ALIAS AFTER_FD_CH_41_3   PRESERVENULLINFORMATION
        FD_sep_08       ALIAS BFD_sep_08
/REFORMAT LAYOUT rl_sum

 /FIELDS FLD_ZD_26_10 26 ZD 10
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_5_4 5 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_37_3 37 CH 3
 /FIELDS FLD_CH_18_7 18 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_5_4 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_18_7 ASCENDING,
   FLD_CH_37_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_26_10
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GTL11PDU
       ;;
(GTL11PDU)
       m_CondExec 00,EQ,GTL11PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL122                                                                
#  ------------                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PDX PGM=BTL122     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PDX
       ;;
(GTL11PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER FTL111B ISSU DU BTL111                                       
       m_FileAssign -d SHR -g ${G_A44} FTL111B ${DATA}/PXX0/F07.FTL111BP
# ******  FICHIER FTL122                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL122 ${DATA}/PTEM/GTL11PDX.FTL122AP
# ****    PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_ProgramExec BTL122 
# ********************************************************************         
#  TRI 11A : TRI DU FICHIER FTL122AP                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PEA
       ;;
(GTL11PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PTEM/GTL11PDX.FTL122AP
# ******  FICHIER A ENVOYER A COPERNIC VIA LA GATEWAY                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FTL122BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS
    FD_CH_1_3     1   3 CH,
    FD_sep_01     4   1 CH,
 
    FD_CH_5_4     5   4  CH,
    FD_sep_02     9   1 CH,
 
    FD_CH_10_3    10  3 CH,
    FD_sep_03     13  1 CH,
 
    FD_CH_14_3    14  3 CH,
    FD_sep_04     17  1 CH,
 
    FD_CH_18_5    18  5 CH,
    FD_sep_05     23  1 CH,
 
    FD_CH_24_5    24  5 CH,
    FD_sep_06     29  1 CH,
 
    FD_CH_30_1    30  1 CH,
    FD_sep_07     31  1 CH,
 
    FD_CH_32_2    32  2 CH,
    FD_sep_08     34  1 CH,
 
    FD_EN_35_10   35  EN 10,
    FD_sep_09     45  1 CH,
 
    FD_CH_46_3    46  3 CH,
    FD_sep_10     49  1 CH,
 
    FD_CH_50_3    50  3 CH,
    FD_sep_11     53  1 CH
 
/DERIVEDFIELD total_SFF   FD_EN_35_10  (9999999V99)
 
/KEYS FD_CH_1_3, FD_CH_5_4, FD_CH_10_3, FD_CH_14_3, FD_CH_18_5, FD_CH_24_5, FD_CH_30_1,
FD_CH_32_2, FD_CH_46_3, FD_CH_50_3
/SUMMARIZE TOTAL FD_EN_35_10
/MT_OUTFILE_ASG SORTOUT
/DYNAMICLAYOUT rl_sum POSITIONAL RETAINCOMPOSITE VALUE
  FD_CH_1_3    ALIAS BFD_CH_1_3,
  FD_sep_01       ALIAS BFD_sep_01,
  FD_CH_5_4    ALIAS BFD_CH_5_4,
  FD_sep_02       ALIAS BFD_sep_02,
  FD_CH_10_3  ALIAS BFD_CH_10_3,
  FD_sep_03       ALIAS BFD_sep_03,
  FD_CH_14_3  ALIAS BFD_CH_14_3,
  FD_sep_04       ALIAS BFD_sep_04,
  FD_CH_18_5  ALIAS BFD_CH_18_5,
  FD_sep_05       ALIAS BFD_sep_05,
  FD_CH_24_5  ALIAS BFD_CH_24_5,
  FD_sep_06       ALIAS BFD_sep_06,
  FD_CH_30_1  ALIAS BFD_CH_30_1,
  FD_sep_07       ALIAS BFD_sep_07,
  FD_CH_32_2  ALIAS BFD_CH_32_2,
  FD_sep_08       ALIAS BFD_sep_08,
  total_SFF   ALIAS total_OVERLAY,
  FD_sep_09       ALIAS AFD_sep_09,
  FD_CH_46_3  ALIAS AFTER_FD_CH_46_3,
  FD_sep_10       ALIAS AFD_sep_10,
  FD_CH_50_3  ALIAS AFTER_FD_CH_50_3,
  FD_sep_11       ALIAS AFD_sep_11
/REFORMAT LAYOUT rl_sum
 /FIELDS FLD_ZD_35_10 35 ZD 10
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_5_4 5 CH 4
 /FIELDS FLD_CH_24_5 24 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_5_4 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_24_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_35_10
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GTL11PEB
       ;;
(GTL11PEB)
       m_CondExec 00,EQ,GTL11PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AGJ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTL117XX,                                                           
#      FNAME=":FTL117CP""(0),                                                  
#      NFNAME=LIVPL_P_COP                                                      
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AGO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTL122XX,                                                           
#      FNAME=":FTL122BP""(0),                                                  
#      NFNAME=LIVACT_P_COP                                                     
#          DATAEND                                                             
# ********************************************************************         
#  CHANGEMENT PCL => GTL11P <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGTL11P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PED PGM=FTP        ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PED
       ;;
(GTL11PED)
       m_CondExec ${EXAGJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11PED.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GTL11PEG PGM=EZACFSM1   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PEG
       ;;
(GTL11PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11PEG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGTL11P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11PEJ PGM=FTP        ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PEJ
       ;;
(GTL11PEJ)
       m_CondExec ${EXAGT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11PEJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GTL11PEM PGM=EZACFSM1   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PEM
       ;;
(GTL11PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11PEM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL11PZA
       ;;
(GTL11PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
