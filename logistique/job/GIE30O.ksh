#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE30O.ksh                       --- VERSION DU 08/10/2016 13:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGIE30 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/19 AT 15.10.45 BY BURTECM                      
#    STANDARDS: P  JOBSET: GIE30O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DE LA TABLE RTIE05 DE DO AVANT MAJ                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GIE30OA
       ;;
(GIE30OA)
#
#GIE30OAA
#GIE30OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GIE30OAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GIE30OAD
       ;;
(GIE30OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  STOCK SS LIEU ENTREPOT                                               
#    RSGS10O  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10O /dev/null
# ******  STOCK HS                                                             
#    RSGS60O  : NAME=RSGS60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60O /dev/null
# ******  PRET A L'ENTREPOT                                                    
#    RSGS15O  : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15O /dev/null
# ******  ARTICLES                                                             
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  GENERALISEES                                                         
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  FAMILLES                                                             
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  STOCK THEORIQUE AVANT INVENTAIRE                                     
#    RSIE05O  : NAME=RSIE05O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05O /dev/null
# ******  STOCK THEORIQUE AVANT INVENTAIRE (H.S)                               
#    RSIE60O  : NAME=RSIE60O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60O /dev/null
#                                                                              
# *****************TABLE*DES*ANOMALIES********************************         
#  SUR LA RTGS60 , ON NE DOIT AVOIR QUE DES NLIEUTRT = E OU F    *             
#                  SOIT ENTREPOT OU FOURNISSEUR                  *             
#  SUR LA RTGS15 , ON NE DOIT PLUS AVOIR DE SSLIEU P AVEC TYPE F *             
#                  LE DEPOT DOIT LES BASCULER EN HS FOURNISSEURS *             
# *********************************************************************        
#                                                                              
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE030 
       JUMP_LABEL=GIE30OAE
       ;;
(GIE30OAE)
       m_CondExec 04,GE,GIE30OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#    LISTE DES ANOMALIES                                                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE30OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OAG
       ;;
(GIE30OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c 9 -w BIE030 SYSPRINT
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE30OAG.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE30OAH
       ;;
(GIE30OAH)
       m_CondExec 04,GE,GIE30OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BIE505                                                                
#  ------------                                                                
#              INITIALISATION DES TABLES RTIE10 ET RTIE90                      
#              ET CREATION D'UN ETAT DE CONTROLE POUR LA DO                    
#                                                                              
#              SUPP DU DELETE DE RTIE10 DANS PCL GIE00X                        
# ********************************************************************         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE30OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OAJ
       ;;
(GIE30OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ------  TABLE LIEU                                                           
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ------  CONTROLE DU TOP DANS RTIE10 RTIE90                                   
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GIE30OAJ
# ------  TABLE INVENTAIRE                                                     
#    RSIE10O  : NAME=RSIE10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10O /dev/null
# ------  TABLE INVENT AVANCEMENT INVHS PAR CODE REGROUP                       
#    RSIE90O  : NAME=RSIE90O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIE90O /dev/null
# ------  FICHIER EXTRAC POUR CREATION ETAT DE CONTROLE                        
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/GIE30OAJ.GIE505AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE505 
       JUMP_LABEL=GIE30OAK
       ;;
(GIE30OAK)
       m_CondExec 04,GE,GIE30OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI EN VUE DE LA CREATION D'UN ETAT DE CONTROLE POUR LA DO                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE30OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OAM
       ;;
(GIE30OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FEXTRAC                                                             
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GIE30OAJ.GIE505AO
#  FICHIER FEXTRAC TRIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GIE30OAM.GIE505BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE30OAN
       ;;
(GIE30OAN)
       m_CondExec 00,EQ,GIE30OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS GIE505CO                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE30OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OAQ
       ;;
(GIE30OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GIE30OAM.GIE505BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GIE30OAQ.GIE505CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GIE30OAR
       ;;
(GIE30OAR)
       m_CondExec 04,GE,GIE30OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE30OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OAT
       ;;
(GIE30OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GIE30OAQ.GIE505CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GIE30OAT.GIE505DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GIE30OAU
       ;;
(GIE30OAU)
       m_CondExec 00,EQ,GIE30OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIE505                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GIE30OAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OAX
       ;;
(GIE30OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GIE30OAM.GIE505BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GIE30OAT.GIE505DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIE505 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GIE30OAY
       ;;
(GIE30OAY)
       m_CondExec 04,GE,GIE30OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GIE30OZA
       ;;
(GIE30OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GIE30OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
