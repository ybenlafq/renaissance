#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM01P.ksh                       --- VERSION DU 08/10/2016 13:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGRM01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/13 AT 11.03.02 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GRM01P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BRM058 : PREPA FICHIER LOAD DE RTRM58                                 
#  ------------                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM01PA
       ;;
(GRM01PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRM01PAA
       ;;
(GRM01PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ** TABLES EN LECTURE                                                         
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSRM00   : NAME=RSRM00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM00 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
#                                                                              
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 38 -t LSEQ -g +1 FRM58 ${DATA}/PTEM/GRM01PAA.BRM058AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM058 
       JUMP_LABEL=GRM01PAB
       ;;
(GRM01PAB)
       m_CondExec 04,GE,GRM01PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BRM058 A LOADER                                              
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GRM01PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRM01PAD
       ;;
(GRM01PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GRM01PAA.BRM058AP
       m_FileAssign -d NEW,CATLG,DELETE -r 38 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM01PAD.BRM058BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 05
 /FIELDS FLD_CH_13_7 13 CH 07
 /FIELDS FLD_CH_6_7 6 CH 07
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_7 ASCENDING,
   FLD_CH_13_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM01PAE
       ;;
(GRM01PAE)
       m_CondExec 00,EQ,GRM01PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTRM58 A PARTIR DU FICHIER DE LOAD                        
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM01PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRM01PAG
       ;;
(GRM01PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* FIC DE LOAD DE RTRM58                                                
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PTEM/GRM01PAD.BRM058BP
# ******* RESSOURCE TABLE RTRM58                                               
#    RSRM58   : NAME=RSRM58,MODE=(U,N) - DYNAM=YES                             
# -X-RSRM58   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSRM58 /dev/null
# *******                                                                      
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM01PAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRM01P_GRM01PAG_RTRM58.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRM01PAH
       ;;
(GRM01PAH)
       m_CondExec 04,GE,GRM01PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM01PZA
       ;;
(GRM01PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM01PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
