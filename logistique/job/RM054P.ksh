#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RM054P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRM054 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/28 AT 16.46.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: RM054P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#   DECHARGEMENT DE RTGA54 SUR FIC SEQUENTIEL                                  
#   REPRISE:  OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RM054PA
       ;;
(RM054PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RM054PAA
       ;;
(RM054PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
#                                                                              
#                                                                              
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSPUNCH /dev/null
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -d NEW,CATLG,CATLG -r 41 -t LSEQ -g +1 SYSREC00 ${DATA}/PXX0/F07.UNGA54P
       m_DBHpuUnload -t RTGA54 -o SYSREC00
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RM054PAB
       ;;
(RM054PAB)
       m_CondExec 04,GE,RM054PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES DE LA TABLE RTGA54                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAD
       ;;
(RM054PAD)
       m_CondExec ${EXAAF},NE,YES 
# ******  TABLE EN LECTURE                                                     
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
# ******  FICHIER EN SORTIE D UNLOAD                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/RM054PAD.FRM054CP
# ******                                                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM054PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  PGM : BRM054 : INIT DE GAMME POUR PRODUITS DACEM ET NON DACEM               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAG
       ;;
(RM054PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL40 /dev/null
#    RSGQ05   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
#    RSRM41   : NAME=RSRM41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM41 /dev/null
#    RSRM43   : NAME=RSRM43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM43 /dev/null
#                                                                              
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} FGA54 ${DATA}/PTEM/RM054PAD.FRM054CP
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 FRM54 ${DATA}/PTEM/RM054PAG.FRM054DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM054 
       JUMP_LABEL=RM054PAH
       ;;
(RM054PAH)
       m_CondExec 04,GE,RM054PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DE LOAD ISSU DU BRM054                                       
#  SUPPRESSION DES DOUBLONS                                                    
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP RM054PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAJ
       ;;
(RM054PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RM054PAG.FRM054DP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FRM054EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_PD_15_3 15 PD 3
 /FIELDS FLD_PD_18_3 18 PD 3
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_15_3,
    TOTAL FLD_PD_18_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RM054PAK
       ;;
(RM054PAK)
       m_CondExec 00,EQ,RM054PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD REPLACE DE LA TABLE RTRM54 A PARTIR DU FICHIER TRIE                   
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAM
       ;;
(RM054PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******** FIC DE LOAD DE RTGA54                                               
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F07.FRM054EP
# ********* RESSOURCE TABLE RTGA54                                             
# ** ****** SI PLANTAGE BACKOUT AU STEP : APPEL RM054PR0 DANS MJCLFLER         
#    RSGA54   : NAME=RSGA54,MODE=(U,N) - DYNAM=YES                             
# -X-RM054PR0 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA54 /dev/null
# ********                                                                     
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM054PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/RM054P_RM054PAM_RTGA54.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RM054PAN
       ;;
(RM054PAN)
       m_CondExec 04,GE,RM054PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRM056                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAQ
       ;;
(RM054PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA00   : NAME=RSGA00X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA65   : NAME=RSGA65Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA65   : NAME=RSGA65M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA65   : NAME=RSGA65L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA65   : NAME=RSGA65O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA65   : NAME=RSGA65X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSRM13   : NAME=RSRM13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM13 /dev/null
#    RSRM14   : NAME=RSRM14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM14 /dev/null
#    RSRM43   : NAME=RSRM43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM43 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#    RSGA54   : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
#    RSRM43   : NAME=RSRM43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM43 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM056 
       JUMP_LABEL=RM054PAR
       ;;
(RM054PAR)
       m_CondExec 04,GE,RM054PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES                                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PAT PGM=PTLDRIVM   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAT
       ;;
(RM054PAT)
       m_CondExec ${EXABE},NE,YES 
# ******  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01,MODE=(U,N) - DYNAM=YES                             
#    RSFL50   : NAME=RSFL50,MODE=(U,N) - DYNAM=YES                             
#    RSFL50   : NAME=RSFL60,MODE=(U,N) - DYNAM=YES                             
# ******  FICHIER EN SORTIE D UNLOAD                                           
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC ${DATA}/PXX0/F07.BRM054HP
# ******                                                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM054PAT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC
# *******************************************************************          
#  ENVOI DES MUTATIONS VALORISÚS VERS GATEWAY                                  
#  REPRISE : OUI                                                    *          
# *******************************************************************          
#                                                                              
# ABJ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BRM054HP,                                                           
#      FNAME=":BRM054HP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTRM054P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RM054PAX
       ;;
(RM054PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM054PAX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/RM054PAX.FTRM054P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRM054P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RM054PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RM054PBA
       ;;
(RM054PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${DATA}/CORTEX4.P.MTXTFIX1/RM054PBA
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/RM054PAX.FTRM054P
       m_UtilityExec
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP RM054PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RM054PBD
       ;;
(RM054PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM054PBD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RM054PZA
       ;;
(RM054PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RM054PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
