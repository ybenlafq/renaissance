#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU901P.ksh                       --- VERSION DU 09/10/2016 00:59
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU901 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/07/15 AT 11.38.20 BY BURTEC6                      
#    STANDARDS: P  JOBSET: MU901P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTGB05,RTGB15,RTGB20                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU901PA
       ;;
(MU901PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MU901PAA
       ;;
(MU901PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QMU901P
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/MU901PAA
       m_ProgramExec IEFBR14 "RDAR,MU901P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=MU901PAD
       ;;
(MU901PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QMU901P
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=MU901PAE
       ;;
(MU901PAE)
       m_CondExec 00,EQ,MU901PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BMU988                                                                
#  EPURATION DES TABLES RTGB05 ,RTGB15,RTGB20                                  
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR         
#         : DU QUIESCE EN TETE DE CHAINE SUR TOUTES LES TABLES                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU901PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MU901PAG
       ;;
(MU901PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* NOMBRES DE JOURS D'EPURATION                                         
       m_FileAssign -d SHR FNJOUR ${DATA}/CORTEX4.P.MTXTFIX1/MU901PAG
# ******* TABLE DU CALENDRIER DE MUTS                                          
#    RSGB05   : NAME=RSGB05,MODE=(U,N) - DYNAM=YES                             
# -X-MU901PR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGB05 /dev/null
# ******* TABLE DES LIGNES DE MUTS                                             
#    RSGB15   : NAME=RSGB15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB15 /dev/null
# ******* TABLE DES MUTS                                                       
#    RSGB20   : NAME=RSGB20,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB20 /dev/null
# ******* TABLE HISTORIQUE MUTS                                                
#    RSGB55   : NAME=RSGB55,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB55 /dev/null
# ******* TABLE HISTORIQUE MUTS                                                
#    RSGB65   : NAME=RSGB65,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB65 /dev/null
# ******* TABLE HISTORIQUE MUTS                                                
#    RSGB70   : NAME=RSGB70,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB70 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU988 
       JUMP_LABEL=MU901PAH
       ;;
(MU901PAH)
       m_CondExec 04,GE,MU901PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BMU989                                                                
# ********************************************************************         
#  EPURATION DES TABLES RTGB55 ,RTGB65,RTGB70                                  
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR         
#         : DU QUIESCE EN TETE DE CHAINE SUR TOUTES LES TABLES                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU901PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MU901PAJ
       ;;
(MU901PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* NOMBRES DE JOURS D'EPURATION                                         
       m_FileAssign -d SHR FNJOUR ${DATA}/CORTEX4.P.MTXTFIX1/MU901PAJ
# ******* TABLE HISTORIQUE MUTS                                                
#    RSGB55   : NAME=RSGB55,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB55 /dev/null
# ******* TABLE HISTORIQUE MUTS                                                
#    RSGB65   : NAME=RSGB65,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB65 /dev/null
# ******* TABLE HISTO DE RTGB20                                                
#    RSGB70   : NAME=RSGB70,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB70 /dev/null
# ******* FIHIER HISTO DE RTGB55                                               
       m_FileAssign -d SHR FGB55 /dev/null
# ******* TABLE HISTORIQUE MUTS                                                
       m_FileAssign -d SHR FGB65 /dev/null
# ******* FIHIER HISTO DE RTGB20                                               
       m_FileAssign -d SHR FGB70 /dev/null
#                                                                              
# *********************                                                        
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU989 
       JUMP_LABEL=MU901PAK
       ;;
(MU901PAK)
       m_CondExec 04,GE,MU901PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
