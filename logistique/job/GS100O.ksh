#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS100O.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGS100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/22 AT 09.42.28 BY BURTECA                      
#    STANDARDS: P  JOBSET: GS100O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGS107 :   CREATION FICHIER FGS100  :  MAJ DES STOCKS + HISTO STOC         
#                       FICHIER FEL030  :  ANOMALIES                           
#              RTGA01 - NCGFC : SI CODE GS107 = N => TRAITEMENT PARIS          
#                               SI CODE GS107 = O => TRAITEMENT FILIAL         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS100OA
       ;;
(GS100OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS100OAA
       ;;
(GS100OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  ARTICLES                                                             
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  PARAMETRAGE PGM: SOUS TABLE NCGFC POUR 'GS107'                       
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  LIEUX                                                                
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  FAMILLES                                                             
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  MARQUES                                                              
#    RSGA22O  : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22O /dev/null
# ******  CODES VENDEURS                                                       
#    RSGV31O  : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31O /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER ENCAISSEMENT VENANT DE EL001R                                
       m_FileAssign -d SHR -g +0 FEMPORTE ${DATA}/PXX0/F16.BGS105AO
#                                                                              
# ******  FICHIER SERVANT A LA MAJ DES STOCKS                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FGS100 ${DATA}/PTEM/GS100OAA.BGS107AO
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FEL030 ${DATA}/PTEM/GS100OAA.BGS107BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS107 
       JUMP_LABEL=GS100OAB
       ;;
(GS100OAB)
       m_CondExec 04,GE,GS100OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FONCTION DE LA CARTE FCFONC FIC STOCK ET STAT          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAD
       ;;
(GS100OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****  PARAMETRAGE PGM                                                       
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GS100OAD
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GS100OAD
#                                                                              
# *****  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                          
#    RSMQ15O  : NAME=RSMQ15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FTS054 ${DATA}/PXX0/F16.BNM154AO
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FTS056 ${DATA}/PXX0/F16.BNM156AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GS100OAE
       ;;
(GS100OAE)
       m_CondExec 04,GE,GS100OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAG
       ;;
(GS100OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F16.BNM154AO
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100OAG.BNM154BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_8 18 CH 8
 /FIELDS FLD_BI_1_17 1 CH 17
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100OAH
       ;;
(GS100OAH)
       m_CondExec 00,EQ,GS100OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAJ
       ;;
(GS100OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F16.BNM156AO
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100OAJ.BNM156BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_8 18 CH 8
 /FIELDS FLD_BI_1_17 1 CH 17
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 116 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100OAK
       ;;
(GS100OAK)
       m_CondExec 00,EQ,GS100OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAM
       ;;
(GS100OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS100OAA.BGS107AO
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100OAM.BGS107CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4  "   "
 /FIELDS FLD_CH_18_8 18 CH 8
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_89_24 89 CH 24
 /FIELDS FLD_CH_1_88 01 CH 88
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = (113,,116) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_88,CST_1_4,FLD_CH_89_24
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS100OAN
       ;;
(GS100OAN)
       m_CondExec 00,EQ,GS100OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR                                           
#  DATE(VENTE)  SOCIETE   LIEU   ZONE(PRIX)  CAISSE  TERMINAL TRANSACT         
#           SORT FIELDS = (18,8,A,1,17,A)                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAQ
       ;;
(GS100OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS100OAM.BGS107CO
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GS100OAJ.BNM156BO
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100OAQ.BGS100BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_8 18 CH 8
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100OAR
       ;;
(GS100OAR)
       m_CondExec 00,EQ,GS100OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAT PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAT
       ;;
(GS100OAT)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# SORTIN   FILE  NAME=BGS107CO,MODE=I                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GS100OAG.BNM154BO
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100OAT.BGS122AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_96_8 96 CH 8
 /KEYS
   FLD_CH_96_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100OAU
       ;;
(GS100OAU)
       m_CondExec 00,EQ,GS100OAT ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS122 :                                                                    
#  REPRISE : OUI SI FIN ANORMALE .                                             
#           SINON  FAIRE 1 RECOVER AVEC LE RBA DU QUIESCE 1� STEP              
#            POUR : PODMQ00.RSMQ15O  ET                                        
#                 : PODGS00.RSGS30O  ET                                        
#                   PPDGS00.RSGS40  EST LOGISTIQUE                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OAX PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS100OAX
       ;;
(GS100OAX)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLES                                                       
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE GENERALISEE (SOUS TABLES PARAMETRES)                           
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58O  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58O /dev/null
# ******  RELATION ARTICLE ET ZONE DE PRIX                                     
#    RSGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59O /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50O  : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50O /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55O  : NAME=RSGG55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55O /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15O  : NAME=RSMQ15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  TABLE DES STOCKS                                                     
#    RSGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER LG 113                                                       
       m_FileAssign -d SHR -g ${G_A7} FGS100 ${DATA}/PTEM/GS100OAT.BGS122AO
# ******  FICHIER DES ANOMALIES LG 110                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FGS103 ${DATA}/PTEM/GS100OAX.BGS103AO
#                                                                              
# ******  STOCK MAGASIN                                                        
#    RSGS30O  : NAME=RSGS30O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  MOUVEMENT DE STOCK LGT                                               
#    RSGS40   : NAME=RSGS40,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSGS40O  : NAME=RSGS40O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS40O /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15O  : NAME=RSMQ15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS122 
       JUMP_LABEL=GS100OAY
       ;;
(GS100OAY)
       m_CondExec 04,GE,GS100OAX ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU FICHIER DES ANOMALIES TRAITEMENT DU DES CAISSES                 
#            FEL032 = FICHIER ANOS DU PROG BGS100                              
#            FGS103 = FICHIER ANOS DU PROG BGS103                              
#  TRI     : SORT FIELDS=(1,6,A,19,3,A,22,5,A,7,7,A)                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OBA PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS100OBA
       ;;
(GS100OBA)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GS100OAA.BGS107BO
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GS100OAX.BGS103AO
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100OBA.BGS106AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_19_3 19 CH 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_19_3 ASCENDING,
   FLD_CH_22_5 ASCENDING,
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100OBB
       ;;
(GS100OBB)
       m_CondExec 00,EQ,GS100OBA ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES ANOMALIES DU TRAITEMENT DES CAISSES (BGS100+BGS122)             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100OBD PGM=BGS106     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GS100OBD
       ;;
(GS100OBD)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  FICHIER D'EDITION                                                    
       m_FileAssign -d SHR -g ${G_A10} FEL032 ${DATA}/PTEM/GS100OBA.BGS106AO
# ******  EDITION DES ANOMALIES                                                
# ***ETAT*SUPPRIME*LE*290101*PLUS*UTILISE*A*MGIO*CGA******************         
# IFEL031  REPORT SYSOUT=(9,IFEL031)                                           
       m_OutputAssign -c "*" IFEL031
       m_ProgramExec BGS106 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS100OZA
       ;;
(GS100OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS100OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
