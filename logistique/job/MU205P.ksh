#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU205P.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU205 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/08/29 AT 08.39.53 BY BURTECA                      
#    STANDARDS: P  JOBSET: MU205P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *******************************************************************          
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU205PA
       ;;
(MU205PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MU205PAA
       ;;
(MU205PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.UNGA69P
       m_FileAssign -d NEW,CATLG,DELETE -r 32 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAA.BMU204OP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_11_2 11 CH 2
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_2 ASCENDING,
   FLD_CH_1_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAB
       ;;
(MU205PAB)
       m_CondExec 00,EQ,MU205PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAD
       ;;
(MU205PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204GP
       m_FileAssign -d NEW,CATLG,DELETE -r 31 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAD.BMU204PP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAE
       ;;
(MU205PAE)
       m_CondExec 00,EQ,MU205PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAG
       ;;
(MU205PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204IP
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAG.BMU204QP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAH
       ;;
(MU205PAH)
       m_CondExec 00,EQ,MU205PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAJ
       ;;
(MU205PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204MP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAJ.BMU204RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_6 8 CH 6
 /FIELDS FLD_BI_24_7 24 CH 7
 /FIELDS FLD_BI_1_7 01 CH 7
 /FIELDS FLD_PD_16_3 16 PD 3
 /KEYS
   FLD_BI_24_7 ASCENDING,
   FLD_BI_8_6 ASCENDING,
   FLD_BI_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_16_3
 /* Record Type = F  Record Length = 30 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAK
       ;;
(MU205PAK)
       m_CondExec 00,EQ,MU205PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAM
       ;;
(MU205PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204KP
       m_FileAssign -d NEW,CATLG,DELETE -r 38 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAM.BMU204SP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_32_7 32 CH 7
 /FIELDS FLD_BI_15_6 15 CH 6
 /FIELDS FLD_BI_1_7 01 CH 7
 /KEYS
   FLD_BI_32_7 ASCENDING,
   FLD_BI_15_6 ASCENDING,
   FLD_BI_1_7 ASCENDING
 /* Record Type = F  Record Length = 38 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAN
       ;;
(MU205PAN)
       m_CondExec 00,EQ,MU205PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAQ
       ;;
(MU205PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204NP
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAQ.BMU204UP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /* Record Type = F  Record Length = 12 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAR
       ;;
(MU205PAR)
       m_CondExec 00,EQ,MU205PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BMU204 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAT
       ;;
(MU205PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204LP
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAT.BMU204VP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_41_7 41 CH 7
 /FIELDS FLD_BI_1_7 01 CH 7
 /FIELDS FLD_BI_13_6 13 CH 6
 /KEYS
   FLD_BI_41_7 ASCENDING,
   FLD_BI_13_6 ASCENDING,
   FLD_BI_1_7 ASCENDING
 /* Record Type = F  Record Length = 47 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAU
       ;;
(MU205PAU)
       m_CondExec 00,EQ,MU205PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BRM050 DE LA CHAINE MU204P                       
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MU205PAX
       ;;
(MU205PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BRM050AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PAX.BRM050BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_3 16 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_6_7 6 CH 7
 /KEYS
   FLD_CH_6_7 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_16_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PAY
       ;;
(MU205PAY)
       m_CondExec 00,EQ,MU205PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTRM85 (REAPPRO MAG: RANG CODIC PAR GROUPE MARKE         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBA PGM=PTLDRIVM   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBA
       ;;
(MU205PBA)
       m_CondExec ${EXABO},NE,YES 
# ******  REAPPRO MAG: RANG CODIC PAR GROUPE DE SEGMENT                        
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/MU205PBA.RM85UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU205PBA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  SORT FICHIER UNLOAD RTRM85                                                  
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU205PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBD
       ;;
(MU205PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER RM85                                                         
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/MU205PBA.RM85UNP
# ******* FICHIER MUTS A TRAITER TRIE                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PBD.RM85TRIP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_31_7 31 CH 7
 /FIELDS FLD_CH_6_5 6 CH 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_6_5 ASCENDING,
   FLD_CH_31_7 ASCENDING,
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PBE
       ;;
(MU205PBE)
       m_CondExec 00,EQ,MU205PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU205 :PREPARE LA PROPO DE MUT                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBG
       ;;
(MU205PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE A TRAITER                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  TABLE DES ARTICLES                                                   
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE (LSELA)                                            
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE EDITION DES ETATS                                              
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE ARTICLES ASSORTISSEMENT MAGASINS                               
#    RTGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA19 /dev/null
# *****                                                                        
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE DES LIENS ENTRE ARTICLES                                       
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
# *****                                                                        
#    RTGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA58 /dev/null
# *****                                                                        
#    RTGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA59 /dev/null
# *****                                                                        
#    RTGA64   : NAME=RSGA64,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA64 /dev/null
# ******  TABLE CALENDRIER DE MUTATION                                         
#    RTGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB05 /dev/null
# *****                                                                        
#    RTGG20   : NAME=RSGG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG20 /dev/null
# *****                                                                        
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10 /dev/null
# *****                                                                        
#    RTGS25   : NAME=RSGS25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS25 /dev/null
# *****                                                                        
#    RTMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTMU06 /dev/null
# *****                                                                        
#    RTFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL05 /dev/null
# *****                                                                        
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
# *****                                                                        
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
# ** FICHIERS EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A2} FRM85E ${DATA}/PTEM/MU205PBD.RM85TRIP
       m_FileAssign -d SHR -g +0 FRM58 ${DATA}/PXX0/F07.UNRM58P
       m_FileAssign -d SHR -g +0 FRM58B ${DATA}/PXX0/F07.UNRM58AP
# ******  FICHIER PRIX STANDARS                                                
       m_FileAssign -d SHR -g ${G_A3} FGA69E ${DATA}/PTEM/MU205PAA.BMU204OP
# ******  FICHIER STOCKS MAGASINS                                              
       m_FileAssign -d SHR -g ${G_A4} FGS36E ${DATA}/PTEM/MU205PAM.BMU204SP
# ******  FICHIER OFFRES ACTIVES                                               
       m_FileAssign -d SHR -g ${G_A5} FGA66E ${DATA}/PTEM/MU205PAD.BMU204PP
# ******  FICHIER DATE PROCHAINE COMMANDE                                      
       m_FileAssign -d SHR -g ${G_A6} FGF55E ${DATA}/PTEM/MU205PAG.BMU204QP
# ******  FICHIER MUTATIONS ATTENDUES                                          
       m_FileAssign -d SHR -g ${G_A7} FGB15E ${DATA}/PTEM/MU205PAJ.BMU204RP
# ******  FICHIER ELEMENT DE GROUPE                                            
       m_FileAssign -d SHR -g ${G_A8} FGA58E ${DATA}/PTEM/MU205PAQ.BMU204UP
# ******  FICHIER VENTES                                                       
       m_FileAssign -d SHR -g ${G_A9} FHV04E ${DATA}/PTEM/MU205PAT.BMU204VP
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A10} FMUMUT ${DATA}/PTEM/MU205PAX.BRM050BP
# ******  FICHIER DE SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FMU205 ${DATA}/PTEM/MU205PBG.BMU205AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU205 
       JUMP_LABEL=MU205PBH
       ;;
(MU205PBH)
       m_CondExec 04,GE,MU205PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBJ
       ;;
(MU205PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/MU205PBG.BMU205AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PBJ.BMU205BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_209_8 209 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_209_8 DESCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PBK
       ;;
(MU205PBK)
       m_CondExec 00,EQ,MU205PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU220                                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBM
       ;;
(MU205PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISE (TXTVA)                                             
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE                                                                
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE                                                                
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE                                                                
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA26 /dev/null
# ******  FICHIER                                                              
       m_FileAssign -d SHR -g ${G_A12} FMU220 ${DATA}/PTEM/MU205PBJ.BMU205BP
# ******  FICHIER DES PROPOS DE MUTS 512                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IMU220 ${DATA}/PTEM/MU205PBM.BMU220AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU220 
       JUMP_LABEL=MU205PBN
       ;;
(MU205PBN)
       m_CondExec 04,GE,MU205PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBQ
       ;;
(MU205PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/MU205PBM.BMU220AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PBQ.BMU220BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_36_3 36 PD 3
 /FIELDS FLD_PD_59_5 59 PD 5
 /FIELDS FLD_CH_1_20 1 CH 20
 /FIELDS FLD_CH_39_20 39 CH 20
 /FIELDS FLD_PD_64_1 64 PD 1
 /KEYS
   FLD_CH_1_20 ASCENDING,
   FLD_PD_36_3 ASCENDING,
   FLD_CH_39_20 ASCENDING,
   FLD_PD_59_5 ASCENDING,
   FLD_PD_64_1 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PBR
       ;;
(MU205PBR)
       m_CondExec 00,EQ,MU205PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU230 :                                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBT PGM=BMU230     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBT
       ;;
(MU205PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER POUR EDITION                                                 
       m_FileAssign -d SHR -g ${G_A14} FMU230 ${DATA}/PTEM/MU205PBQ.BMU220BP
# ******  FICHIER POUR EDITION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FMU231 ${DATA}/PTEM/MU205PBT.BMU230AP
       m_ProgramExec BMU230 
# ********************************************************************         
#  TRI DU FICHIER D EDITION                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=MU205PBX
       ;;
(MU205PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/MU205PBT.BMU230AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU205PBX.BMU230BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_20 01 CH 20
 /FIELDS FLD_PD_64_1 64 PD 1
 /FIELDS FLD_PD_59_5 59 PD 5
 /FIELDS FLD_PD_36_3 36 PD 3
 /FIELDS FLD_CH_39_20 39 CH 20
 /KEYS
   FLD_CH_1_20 ASCENDING,
   FLD_PD_36_3 ASCENDING,
   FLD_CH_39_20 ASCENDING,
   FLD_PD_59_5 ASCENDING,
   FLD_PD_64_1 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU205PBY
       ;;
(MU205PBY)
       m_CondExec 00,EQ,MU205PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BMU221                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PCA PGM=BMU221     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=MU205PCA
       ;;
(MU205PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FMU220 ${DATA}/PTEM/MU205PBX.BMU230BP
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IMU220A IMU220A
       m_OutputAssign -c 9 -w IMU220B IMU220B
       m_OutputAssign -c 9 -w IMU220C IMU220C
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 IMU220D ${DATA}/PXX0/F07.IMU220AP
       m_OutputAssign -c 9 -w IMU220E IMU220E
       m_OutputAssign -c 9 -w IMU220F IMU220F
       m_ProgramExec BMU221 
# ********************************************************************         
#  CREATION DE L'ETAT DANS EOS                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PCD PGM=IEBGENER   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=MU205PCD
       ;;
(MU205PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A17} SYSUT1 ${DATA}/PXX0/F07.IMU220AP
       m_OutputAssign -c 9 -w IMU220D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=MU205PCE
       ;;
(MU205PCE)
       m_CondExec 00,EQ,MU205PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# ADM      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=IMU220AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTMU205P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU205PCG PGM=FTP        ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=MU205PCG
       ;;
(MU205PCG)
       m_CondExec ${EXADM},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/MU205PCG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MU205PCJ PGM=EZACFSM1   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=MU205PCJ
       ;;
(MU205PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU205PCJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MU205PZA
       ;;
(MU205PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU205PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
