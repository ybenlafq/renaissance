#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF14P.ksh                       --- VERSION DU 08/10/2016 22:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQMF14 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 97/12/12 AT 15.57.17 BY BURTECM                      
#    STANDARDS: P  JOBSET: QMF14P                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#   PGM : BUR0043                                                              
# ********************************************************************         
#   PREPARATION DU FICHIER DE SYSIN POUR LA REQUETE Q014                       
#   LIVRAISON DE REIMS                                                         
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF14PA
       ;;
(QMF14PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       QMF14PA=${VENDRED}
       RUN=${RUN}
       JUMP_LABEL=QMF14PAA
       ;;
(QMF14PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER SYSIN QMF JOURNALIER (REQUETE Q014)                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 QMF001A ${DATA}/PXX0/F07.QMF014AP
#  FICHIER SYSIN QMF CAS DU VENDREDI (REQUETE Q014)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 QMF001B ${DATA}/PXX0/F07.QMF014BP
#  FICHIER SYSIN QMF JOURNALIER (REQUETE Q177)                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 QMF001C ${DATA}/PXX0/F07.QMF177AP
#  FICHIER SYSIN QMF CAS DU VENDREDI (REQUETE Q177)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 QMF001D ${DATA}/PXX0/F07.QMF177BP
       m_ProgramExec BUR0043 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF014                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF14PAD PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=QMF14PAD
       ;;
(QMF14PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.QMF014AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF14PAE
       ;;
(QMF14PAE)
       m_CondExec 00,EQ,QMF14PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   LISTE DES LIVRAISONS DE REIMS                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF14PAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=QMF14PAG
       ;;
(QMF14PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF014 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF14PAH
       ;;
(QMF14PAH)
       m_CondExec 04,GE,QMF14PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF014                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF14PAJ PGM=IEFBR14    **                                          
# ***********************************                                          
       JUMP_LABEL=QMF14PAJ
       ;;
(QMF14PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF014 (VENDREDI)                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF14PAM PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=QMF14PAM
       ;;
(QMF14PAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[QMF14PA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.QMF014BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF14PAN
       ;;
(QMF14PAN)
       m_CondExec 00,EQ,QMF14PAM ${EXAAU},NE,YES 1,EQ,$[QMF14PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   LISTE DES LIVRAISONS DE REIMS                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF14PAQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=QMF14PAQ
       ;;
(QMF14PAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[QMF14PA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF014 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF14PAR
       ;;
(QMF14PAR)
       m_CondExec 04,GE,QMF14PAQ ${EXAAZ},NE,YES 1,EQ,$[QMF14PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF014                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF14PAT PGM=IEFBR14    **                                          
# ***********************************                                          
       JUMP_LABEL=QMF14PAT
       ;;
(QMF14PAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[QMF14PA] 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
