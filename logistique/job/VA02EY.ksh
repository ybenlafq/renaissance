#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA02EY.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYVA02E -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/23 AT 11.40.00 BY BURTECA                      
#    STANDARDS: P  JOBSET: VA02EY                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BVA410                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VA02EYA
       ;;
(VA02EYA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VA02EYAA
       ;;
(VA02EYAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTVA01   : NAME=RSVA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTVA16   : NAME=RSVA16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA16 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PTEM/VA02EYAA.BVA51AY
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR FEXTRACB /dev/null
# *****   FICHIER D'EXTRACTION POUR LES PGMS D'EDITIONS                        
       m_FileAssign -d NEW,CATLG,DELETE -r 55 -t LSEQ -g +1 FVA510 ${DATA}/PTEM/VA02EYAA.FVA510GE
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
       m_FileAssign -d SHR FVA515 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA410 
       JUMP_LABEL=VA02EYAB
       ;;
(VA02EYAB)
       m_CondExec 04,GE,VA02EYAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA425                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR LES ETATS DE POSITIONS ET DE MOUVEMENTS          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAD
       ;;
(VA02EYAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER D EXTRACTION ISSU DU BVA510                                         
       m_FileAssign -d SHR -g ${G_A1} FVA510 ${DATA}/PTEM/VA02EYAA.FVA510GE
# *****   TABLES EN ENTREE                                                     
#    RTVA01   : NAME=RSVA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTVA06   : NAME=RSVA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTVA16   : NAME=RSVA16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA16 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA20   : NAME=RSGA20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIERS D'EXTRACTION                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA530 ${DATA}/PTEM/VA02EYAD.VA530AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA540 ${DATA}/PTEM/VA02EYAD.VA540AY
       m_FileAssign -d SHR FVA541 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA425 
       JUMP_LABEL=VA02EYAE
       ;;
(VA02EYAE)
       m_CondExec 04,GE,VA02EYAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER VA540AY POUR ETAT IVA540D (ARTICLE DARTY)          *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAG
       ;;
(VA02EYAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VA02EYAD.VA540AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYAG.VA540BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_CH_1_14 1 CH 14
 /FIELDS FLD_CH_28_END 28 CH 
 /FIELDS FLD_CH_227_6 227 CH 6
 /FIELDS FLD_PD_171_8 171 PD 08
 /FIELDS FLD_PD_227_6 227 PD 6
 /FIELDS FLD_CH_51_20 51 CH 20
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_CH_251_18 251 CH 18
 /FIELDS FLD_PD_251_6 251 PD 6
 /FIELDS FLD_CH_171_24 171 CH 24
 /FIELDS FLD_CH_39_8 39 CH 8
 /FIELDS FLD_PD_131_8 131 PD 08
 /FIELDS FLD_PD_187_8 187 PD 8
 /FIELDS FLD_PD_263_6 263 PD 6
 /FIELDS FLD_PD_257_6 257 PD 6
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_CH_32_2 32 CH 2
 /FIELDS FLD_PD_179_8 179 PD 08
 /FIELDS FLD_CH_131_8 131 CH 8
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_131_8,
    TOTAL FLD_PD_171_8,
    TOTAL FLD_PD_179_8,
    TOTAL FLD_PD_187_8,
    TOTAL FLD_PD_227_6,
    TOTAL FLD_PD_251_6,
    TOTAL FLD_PD_257_6,
    TOTAL FLD_PD_263_6
 /* MT_ERROR (unknown field) (1,14,32,2,39,8,51,20,131,8,171,24,227,6,251,18,28 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_14,FLD_CH_32_2,FLD_CH_39_8,FLD_CH_51_20,FLD_CH_131_8,FLD_CH_171_24,FLD_CH_227_6,FLD_CH_251_18,FLD_CH_28_END
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=VA02EYAH
       ;;
(VA02EYAH)
       m_CondExec 00,EQ,VA02EYAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAJ PGM=BVA526     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAJ
       ;;
(VA02EYAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ISSU DU PGM BVA525                                           
       m_FileAssign -d SHR -g ${G_A3} FVA540I ${DATA}/PTEM/VA02EYAG.VA540BY
# *****   FICHIER RESULTANT                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA540O ${DATA}/PTEM/VA02EYAJ.VA526AY
       m_ProgramExec BVA526 
# ********************************************************************         
#  TRI DU FICHIER BVA526AL POUR LE GENERATEUR D'ETAT (MICRO-FICHE)             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAM
       ;;
(VA02EYAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/VA02EYAJ.VA526AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYAM.VA526BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_18_2 18 CH 2
 /FIELDS FLD_BI_16_2 16 CH 2
 /FIELDS FLD_BI_13_3 13 CH 3
 /FIELDS FLD_BI_10_3 10 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_2 ASCENDING,
   FLD_BI_18_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYAN
       ;;
(VA02EYAN)
       m_CondExec 00,EQ,VA02EYAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAQ
       ;;
(VA02EYAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/VA02EYAM.VA526BY
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA02EYAQ.VA540CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA02EYAR
       ;;
(VA02EYAR)
       m_CondExec 04,GE,VA02EYAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAT
       ;;
(VA02EYAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/VA02EYAQ.VA540CY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYAT.VA540DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYAU
       ;;
(VA02EYAU)
       m_CondExec 00,EQ,VA02EYAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA540D (ARTICLE DARTY)                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYAX
       ;;
(VA02EYAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/VA02EYAM.VA526BY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A8} FCUMULS ${DATA}/PTEM/VA02EYAT.VA540DY
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA540D SUR FIC POUR MICROFICHAGE ******                        
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVAEY.IVA540GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA02EYAY
       ;;
(VA02EYAY)
       m_CondExec 04,GE,VA02EYAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA465                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR LES ETATS IVA570 ET IVA580  (DARTY)              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBA
       ;;
(VA02EYBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER D EXTRACTION ISSU DU BVA510                                         
       m_FileAssign -d SHR -g ${G_A9} FVA510 ${DATA}/PTEM/VA02EYAA.FVA510GE
# *****   TABLES EN ENTREE                                                     
#    RTVA01   : NAME=RSVA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTVA06   : NAME=RSVA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTVA16   : NAME=RSVA16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA16 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIERS D'EXTRACTION DARTY                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FVA570 ${DATA}/PTEM/VA02EYBA.FVA570GE
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 FVA571 ${DATA}/PTEM/VA02EYBA.FVA571GE
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -t LSEQ -g +1 FVA580 ${DATA}/PTEM/VA02EYBA.FVA580GE
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 FVA581 ${DATA}/PXX0/F45.FVA581GE
# *****   FICHIERS D'EXTRACTION DACEM                                          
       m_FileAssign -d SHR FVA572 /dev/null
       m_FileAssign -d SHR FVA573 /dev/null
       m_FileAssign -d SHR FVA582 /dev/null
       m_FileAssign -d SHR FVA583 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA465 
       JUMP_LABEL=VA02EYBB
       ;;
(VA02EYBB)
       m_CondExec 04,GE,VA02EYBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA570GE ISSU DU BVA565 POUR CREATION BVA570AE               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBD
       ;;
(VA02EYBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/VA02EYBA.FVA570GE
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYBD.VA570AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_PD_37_6 37 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_73_8 73 PD 8
 /FIELDS FLD_PD_49_8 49 PD 8
 /FIELDS FLD_PD_57_8 57 PD 8
 /FIELDS FLD_PD_65_8 65 PD 8
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_PD_43_6 43 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING,
   FLD_CH_11_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_37_6,
    TOTAL FLD_PD_43_6,
    TOTAL FLD_PD_49_8,
    TOTAL FLD_PD_57_8,
    TOTAL FLD_PD_65_8,
    TOTAL FLD_PD_73_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYBE
       ;;
(VA02EYBE)
       m_CondExec 00,EQ,VA02EYBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA571GE ISSU DU BVA565 POUR CREATION BVA571AE               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBG
       ;;
(VA02EYBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/VA02EYBA.FVA571GE
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYBG.VA571AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_64_8 64 PD 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_56_8 56 PD 8
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_PD_50_6 50 PD 6
 /FIELDS FLD_PD_44_6 44 PD 6
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_11_6 11 CH 6
 /FIELDS FLD_CH_17_1 17 CH 1
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING,
   FLD_CH_11_6 ASCENDING,
   FLD_CH_17_1 ASCENDING,
   FLD_CH_18_3 ASCENDING,
   FLD_CH_21_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_44_6,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_8,
    TOTAL FLD_PD_64_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYBH
       ;;
(VA02EYBH)
       m_CondExec 00,EQ,VA02EYBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA580GE ISSU DU BVA565 POUR CREATION BVA58AE                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBJ
       ;;
(VA02EYBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/VA02EYBA.FVA580GE
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYBJ.BVA58AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_42_8 42 PD 8
 /FIELDS FLD_PD_20_6 20 PD 6
 /FIELDS FLD_PD_34_8 34 PD 8
 /FIELDS FLD_PD_26_8 26 PD 8
 /FIELDS FLD_PD_14_6 14 PD 6
 /FIELDS FLD_PD_50_8 50 PD 8
 /FIELDS FLD_CH_4_2 4 CH 2
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_14_6,
    TOTAL FLD_PD_20_6,
    TOTAL FLD_PD_26_8,
    TOTAL FLD_PD_34_8,
    TOTAL FLD_PD_42_8,
    TOTAL FLD_PD_50_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYBK
       ;;
(VA02EYBK)
       m_CondExec 00,EQ,VA02EYBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA581GE ISSU DU BVA565 POUR CREATION VA581AY                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBM
       ;;
(VA02EYBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F45.FVA581GE
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYBM.VA581AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "M"
 /DERIVEDFIELD CST_3_16 "Z"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_47_6 47 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_15_3 15 CH 3
 /FIELDS FLD_PD_41_6 41 PD 6
 /FIELDS FLD_PD_53_8 53 PD 8
 /FIELDS FLD_PD_61_8 61 PD 8
 /FIELDS FLD_CH_18_3 18 CH 3
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_12 OR FLD_CH_14_1 EQ CST_3_16 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_2 ASCENDING,
   FLD_CH_14_1 ASCENDING,
   FLD_CH_15_3 ASCENDING,
   FLD_CH_18_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_6,
    TOTAL FLD_PD_47_6,
    TOTAL FLD_PD_53_8,
    TOTAL FLD_PD_61_8
 /OMIT CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYBN
       ;;
(VA02EYBN)
       m_CondExec 00,EQ,VA02EYBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  PGM : BVA570                                                                
# ********************************************************************         
#  CREATION DU FICHIER POUR ETAT IVA570 (ARTICLE DARTY)                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBQ PGM=BVA570     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBQ
       ;;
(VA02EYBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ISSU DU TRI DE FVA570AL                                      
       m_FileAssign -d SHR -g ${G_A14} FVA570 ${DATA}/PTEM/VA02EYBD.VA570AY
# *****   FICHIER ISSU DU TRI DE FVA571AL                                      
       m_FileAssign -d SHR -g ${G_A15} FVA571 ${DATA}/PTEM/VA02EYBG.VA571AY
# *****   FICHIER POUR ETAT IVA570 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IVA570 ${DATA}/PTEM/VA02EYBQ.VA570BY
       m_ProgramExec BVA570 
# ********************************************************************         
#  TRI DU FICHIER BVA570BE POUR ETAT IVA570 (ARTICLE DARTY)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBT
       ;;
(VA02EYBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/VA02EYBQ.VA570BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYBT.VA570CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_19_3 19 CH 3
 /FIELDS FLD_BI_12_6 12 CH 6
 /FIELDS FLD_BI_18_1 18 CH 1
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_22_3 22 CH 3
 /FIELDS FLD_BI_25_2 25 CH 2
 /FIELDS FLD_BI_10_2 10 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_6 ASCENDING,
   FLD_BI_18_1 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_3 ASCENDING,
   FLD_BI_25_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYBU
       ;;
(VA02EYBU)
       m_CondExec 00,EQ,VA02EYBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYBX
       ;;
(VA02EYBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/VA02EYBT.VA570CY
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA02EYBX.VA570DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA02EYBY
       ;;
(VA02EYBY)
       m_CondExec 04,GE,VA02EYBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCA
       ;;
(VA02EYCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/VA02EYBX.VA570DY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYCA.VA570EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYCB
       ;;
(VA02EYCB)
       m_CondExec 00,EQ,VA02EYCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA570D (ARTICLE DARTY)                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCD
       ;;
(VA02EYCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/VA02EYBT.VA570CY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A20} FCUMULS ${DATA}/PTEM/VA02EYCA.VA570EY
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA570D SUR FIC POUR MICROFICHAGE ******                        
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVAEY.IVA571GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA02EYCE
       ;;
(VA02EYCE)
       m_CondExec 04,GE,VA02EYCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA480                                                                
# ********************************************************************         
#  CREATION DU FICHIER POUR ETAT IVA580 (ARTICLE DARTY)                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCG
       ;;
(VA02EYCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER ISSU DU TRI DE FVA580GE                                      
       m_FileAssign -d SHR -g ${G_A21} FVA580 ${DATA}/PTEM/VA02EYBJ.BVA58AY
# *****   FICHIER ISSU DU TRI DE FVA581GE                                      
       m_FileAssign -d SHR -g ${G_A22} FVA581 ${DATA}/PTEM/VA02EYBM.VA581AY
# *****   FICHIER POUR ETAT IVA570 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IVA580 ${DATA}/PTEM/VA02EYCG.BVA58BY
# ******  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTVA31   : NAME=RSVA31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA31 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA480 
       JUMP_LABEL=VA02EYCH
       ;;
(VA02EYCH)
       m_CondExec 04,GE,VA02EYCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BVA58BP POUR ETAT IVA580 (ARTICLE DARTY)                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCJ
       ;;
(VA02EYCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/VA02EYCG.BVA58BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYCJ.BVA58CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_22_2 22 CH 2
 /FIELDS FLD_BI_12_1 12 CH 1
 /FIELDS FLD_BI_19_3 19 CH 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_13_3 13 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_1 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_3 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYCK
       ;;
(VA02EYCK)
       m_CondExec 00,EQ,VA02EYCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCM
       ;;
(VA02EYCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A24} FEXTRAC ${DATA}/PTEM/VA02EYCJ.BVA58CY
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/VA02EYCM.BVA58DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=VA02EYCN
       ;;
(VA02EYCN)
       m_CondExec 04,GE,VA02EYCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCQ
       ;;
(VA02EYCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/VA02EYCM.BVA58DY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VA02EYCQ.BVA58EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA02EYCR
       ;;
(VA02EYCR)
       m_CondExec 00,EQ,VA02EYCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT IVA580D (ARTICLE DARTY)                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCT
       ;;
(VA02EYCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A26} FEXTRAC ${DATA}/PTEM/VA02EYCJ.BVA58CY
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A27} FCUMULS ${DATA}/PTEM/VA02EYCQ.BVA58EY
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DN                            
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA580D SUR FIC POUR MICROFICHAGE ******                        
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ FEDITION ${DATA}/PXX0.FICHEML.EXVAEY.IVA580GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=VA02EYCU
       ;;
(VA02EYCU)
       m_CondExec 04,GE,VA02EYCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA481                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA480 (ARTICLE DARTY)                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYCX
       ;;
(VA02EYCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   PARAMETRE SOCIETE DN = 969                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   PARAMETRE DACEM/PIECE MAJEURE O/N                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/VA02EYCX
# *****   FICHIER POUR ETAT IVA480 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IVA480 ${DATA}/PXX0.FICHEML.EXVAEY.VA480GE1
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA01   : NAME=RSVA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTVA06   : NAME=RSVA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTVA31   : NAME=RSVA31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA31 /dev/null
#    RTVA16   : NAME=RSVA16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA16 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA481 
       JUMP_LABEL=VA02EYCY
       ;;
(VA02EYCY)
       m_CondExec 04,GE,VA02EYCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA440                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA440 (ARTICLE DARTY)                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDA
       ;;
(VA02EYDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   PARAMETRE SOCIETE DN = 969                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   PARAMETRE DACEM/PIECE MAJEURE O/N                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/VA02EYDA
# *****   FICHIER POUR ETAT IVA440 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IVA440 ${DATA}/PXX0.FICHEML.EXVAEY.VA440GE1
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA01   : NAME=RSVA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTVA06   : NAME=RSVA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTVA31   : NAME=RSVA31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA31 /dev/null
#    RTVA16   : NAME=RSVA16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA16 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA440 
       JUMP_LABEL=VA02EYDB
       ;;
(VA02EYDB)
       m_CondExec 04,GE,VA02EYDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA470                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA470 (ARTICLE DARTY)                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDD
       ;;
(VA02EYDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   PARAMETRE SOCIETE DN = 969                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   PARAMETRE DACEM/PIECE MAJEURE O/N                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/VA02EYDD
# *****   FICHIER POUR ETAT IVA470 (DARTY)                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IVA470 ${DATA}/PXX0.FICHEML.EXVAEY.VA470GE2
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA01   : NAME=RSVA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTVA06   : NAME=RSVA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTVA31   : NAME=RSVA31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA31 /dev/null
#    RTVA16   : NAME=RSVA16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA16 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA470 
       JUMP_LABEL=VA02EYDE
       ;;
(VA02EYDE)
       m_CondExec 04,GE,VA02EYDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA485                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR L'ETAT IVA585 (SUIV. STOCKS CONSOMMABLE)         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDG
       ;;
(VA02EYDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTVA25   : NAME=RSVA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA25 /dev/null
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTVA06   : NAME=RSVA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTGA55   : NAME=RSGA55Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA55 /dev/null
#    RTGA30   : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
# *****   FICHIERS D'EXTRACTION DARTY                                          
# IVA585  REPORT SYSOUT=(9,IVA585),FREE=CLOSE,RECFM=FBA,LRECL=133,             
#               BLKSIZE=0                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IVA585 ${DATA}/PXX0.FICHEML.EXVAEY.IVA585GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA485 
       JUMP_LABEL=VA02EYDH
       ;;
(VA02EYDH)
       m_CondExec 04,GE,VA02EYDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : STOCK DETAIL RECAPITULATIF PAR LIEU (ARCTICLE DARTY)              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDJ PGM=IEBGENER   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDJ
       ;;
(VA02EYDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.IVA540GE
       m_OutputAssign -c 9 -w IVA540D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYDK
       ;;
(VA02EYDK)
       m_CondExec 00,EQ,VA02EYDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : SYNTHESE DES CONTREPARTIES CONSO/LIEU (ARTICLE DARTY)             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDM PGM=IEBGENER   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDM
       ;;
(VA02EYDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.IVA571GE
       m_OutputAssign -c 9 -w IVA570D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYDN
       ;;
(VA02EYDN)
       m_CondExec 00,EQ,VA02EYDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE : OUI                                                               
#  IVA580D : SYNTHESE DES CONTREPARTIES TOTAL SOCIETE (ARTICLE DARTY)          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDQ PGM=IEBGENER   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDQ
       ;;
(VA02EYDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.IVA580GE
       m_OutputAssign -c 9 -w IVA580D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYDR
       ;;
(VA02EYDR)
       m_CondExec 00,EQ,VA02EYDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE : OUI                                                               
#  IVA480D : SYNTHESE DES CONTREPARTIES TOTAL SOCIETE (ARTICLE DARTY)          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDT PGM=IEBGENER   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDT
       ;;
(VA02EYDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.VA480GE1
       m_OutputAssign -c 9 -w IVA480D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYDU
       ;;
(VA02EYDU)
       m_CondExec 00,EQ,VA02EYDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE : OUI                                                               
#  IVA440D : SYNTHESE DES STOCKS TOTAL SOCIETE (ARTICLE DARTY)                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYDX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYDX
       ;;
(VA02EYDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.VA440GE1
       m_OutputAssign -c 9 -w IVA440D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYDY
       ;;
(VA02EYDY)
       m_CondExec 00,EQ,VA02EYDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE : OUI                                                               
#  VA470GE2 : SYNTHESE DES STOCKS TOTAL SOCIETE (ARTICLE DARTY)                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYEA
       ;;
(VA02EYEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.VA470GE2
       m_OutputAssign -c 9 -w IVA470D SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYEB
       ;;
(VA02EYEB)
       m_CondExec 00,EQ,VA02EYEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION : IVA585                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA02EYED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYED
       ;;
(VA02EYED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.EXVAEY.IVA585GE
       m_OutputAssign -c 9 -w IVA585 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VA02EYEE
       ;;
(VA02EYEE)
       m_CondExec 00,EQ,VA02EYED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA02EYZA
       ;;
(VA02EYZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA02EYZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
