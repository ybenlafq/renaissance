#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS003P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS003 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/31 AT 17.35.49 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GS003P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BGS003 :  ALIMENTATION DU FICHIER DES INFOS                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS003PA
       ;;
(GS003PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS003PAA
       ;;
(GS003PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FMOIS : MMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *** TABLE EN LECTURE                                                         
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSHV40   : NAME=RSHV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV40 /dev/null
# ******                                                                       
# *** TABLE EN ECRITURE                                                        
#    RSHV15   : NAME=RSHV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
#                                                                              
# ******  FICHIER J-1                                                          
       m_FileAssign -d SHR -g +0 FGS03 ${DATA}/PXX0/F07.FGS003DP
# ******  FICHIER EN TETE                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 EGS003 ${DATA}/PTEM/GS003PAA.FGS003AP
# ******  FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 FGS003 ${DATA}/PTEM/GS003PAA.FGS003BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS003 
       JUMP_LABEL=GS003PAB
       ;;
(GS003PAB)
       m_CondExec 04,GE,GS003PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES DONN�ES                                                  
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAD PGM=MERGE      ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAD
       ;;
(GS003PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS003PAA.FGS003BP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS003PAD.FGS003TP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_13_7 13 CH 7
 /FIELDS FLD_CH_164_3 164 CH 3
 /FIELDS FLD_CH_96_8 96 CH 8
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_164_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_13_7 ASCENDING,
   FLD_CH_96_8 ASCENDING
 /* Record Type = F  Record Length = 166 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PAE
       ;;
(GS003PAE)
       m_CondExec 00,EQ,GS003PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES EN TETE ET MVTS                                                  
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAG PGM=MERGE      ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAG
       ;;
(GS003PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GS003PAA.FGS003AP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GS003PAD.FGS003TP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS003CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 166 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PAH
       ;;
(GS003PAH)
       m_CondExec 00,EQ,GS003PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS003CP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAJ
       ;;
(GS003PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS003PAJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ECLATEMENT DES FICHIERS                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAM
       ;;
(GS003PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS003PAA.FGS003BP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF1 ${DATA}/PTEM/GS003PAM.FGS090CP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF2 ${DATA}/PTEM/GS003PAM.FGS095CP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF3 ${DATA}/PTEM/GS003PAM.FGS120CP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOF4 ${DATA}/PTEM/GS003PAM.FGS125CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_28 "120")
 /DERIVEDFIELD CST_1_16 "907"
 /DERIVEDFIELD CST_3_36 "125")
 /DERIVEDFIELD CST_1_32 "907"
 /DERIVEDFIELD CST_3_12 "090")
 /DERIVEDFIELD CST_3_20 "095")
 /DERIVEDFIELD CST_1_8 "907"
 /DERIVEDFIELD CST_1_24 "907"
 /FIELDS FLD_CH_86_7 86 CH 7
 /FIELDS FLD_CH_46_3 46 CH 3
 /FIELDS FLD_CH_50_3 50 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_13_7 13 CH 7
 /FIELDS FLD_CH_96_8 96 CH 8
 /CONDITION CND_4 FLD_CH_46_3 EQ CST_1_32 AND FLD_CH_50_3 EQ CST_3_36 OR 
 /CONDITION CND_2 FLD_CH_46_3 EQ CST_1_16 AND FLD_CH_50_3 EQ CST_3_20 OR 
 /CONDITION CND_1 FLD_CH_46_3 EQ CST_1_8 AND FLD_CH_50_3 EQ CST_3_12 OR 
 /CONDITION CND_3 FLD_CH_46_3 EQ CST_1_24 AND FLD_CH_50_3 EQ CST_3_28 OR 
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_13_7 ASCENDING,
   FLD_CH_96_8 ASCENDING,
   FLD_CH_86_7 ASCENDING
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /INCLUDE CND_3
 /MT_OUTFILE_SUF 4
 /INCLUDE CND_4
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS003PAN
       ;;
(GS003PAN)
       m_CondExec 00,EQ,GS003PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES EN TETE ET MVTS POUR 907090                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAQ PGM=MERGE      ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAQ
       ;;
(GS003PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GS003PAA.FGS003AP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GS003PAM.FGS090CP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS090DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PAR
       ;;
(GS003PAR)
       m_CondExec 00,EQ,GS003PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS090DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAT
       ;;
(GS003PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS003PAT.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FUSION DES EN TETE ET MVTS POUR 907095                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PAX PGM=MERGE      ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS003PAX
       ;;
(GS003PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GS003PAA.FGS003AP
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GS003PAM.FGS095CP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS095DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PAY
       ;;
(GS003PAY)
       m_CondExec 00,EQ,GS003PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS095DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS003PBA
       ;;
(GS003PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS003PBA.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FUSION DES EN TETE ET MVTS POUR 907120                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PBD PGM=MERGE      ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS003PBD
       ;;
(GS003PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GS003PAA.FGS003AP
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GS003PAM.FGS120CP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS120DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PBE
       ;;
(GS003PBE)
       m_CondExec 00,EQ,GS003PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS120DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PBG PGM=FTP        ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GS003PBG
       ;;
(GS003PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS003PBG.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  FUSION DES EN TETE ET MVTS POUR 907125                                      
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PBJ PGM=MERGE      ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GS003PBJ
       ;;
(GS003PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GS003PAA.FGS003AP
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GS003PAM.FGS125CP
       m_FileAssign -d NEW,CATLG,DELETE -r 163 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS125DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 163 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PBK
       ;;
(GS003PBK)
       m_CondExec 00,EQ,GS003PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FGS125DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GS003PBM
       ;;
(GS003PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GS003PBM.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  TRI DU FICHIER DES DONN�ES                                                  
# ********************************************************************         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS003PBQ PGM=MERGE      ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GS003PBQ
       ;;
(GS003PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GS003PAA.FGS003BP
       m_FileAssign -d NEW,CATLG,DELETE -r 166 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGS003DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_164_3 164 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_13_7 13 CH 7
 /FIELDS FLD_CH_96_8 96 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_164_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_13_7 ASCENDING,
   FLD_CH_96_8 ASCENDING
 /* Record Type = F  Record Length = 166 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS003PBR
       ;;
(GS003PBR)
       m_CondExec 00,EQ,GS003PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS003PZA
       ;;
(GS003PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS003PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
