#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FL003P.ksh                       --- VERSION DU 09/10/2016 05:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFL003 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/10/20 AT 10.23.53 BY BURTECA                      
#    STANDARDS: P  JOBSET: FL003P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS ISSUS DES CHAINES FL001X                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FL003PA
       ;;
(FL003PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2004/10/20 AT 10.23.53 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: FL003P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'MAJ FL50'                              
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FL003PAA
       ;;
(FL003PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BFL001AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BFL001AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BFL001AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BFL001AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BFL001AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BFL001AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BFL001AX
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 SORTOUT ${DATA}/PTEM/FL003PAA.BFL003AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_5 1 CH 5
 /KEYS
   FLD_BI_1_5 ASCENDING
 /* Record Type = F  Record Length = 60 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FL003PAB
       ;;
(FL003PAB)
       m_CondExec 00,EQ,FL003PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL003 : MAJ DES DONNEES LOGISTIQUE DE LA RTFL40 ET LA RTGA65               
#  A PARTIR DES FICHIERS DE FILIALES                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FL003PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FL003PAD
       ;;
(FL003PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FL001 ${DATA}/PTEM/FL003PAA.BFL003AP
# ******  TABLES MISES _A JOUR                                                  
#    RTGA54   : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA54 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL003 
       JUMP_LABEL=FL003PAE
       ;;
(FL003PAE)
       m_CondExec 04,GE,FL003PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FL003PZA
       ;;
(FL003PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FL003PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
