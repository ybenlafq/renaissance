#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GIE21P.ksh                       --- VERSION DU 08/10/2016 23:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGIE21 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/09/13 AT 12.05.44 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GIE21P                                              
# --------------------------------------------------------------------         
# **--USER='DARTY'                                                             
# ********************************************************************         
#  LISTE DES VENTES MODIFIEES PENDANT L INVENTAIRE                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GIE21PA
       ;;
(GIE21PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GIE21PAA
       ;;
(GIE21PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c 9 -w VENTEMAJ SYSPRINT
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV23   : NAME=RSGV23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -i SYSIN
 SELECT NSOCIETE, NLIEU, NVENTE, DMODIFVTE 
 FROM RTGV10 WHERE DMODIFVTE BETWEEN '$DATEDEB' AND '$DATEFIN'
 ORDER BY DMODIFVTE, NSOCIETE, NLIEU, NVENTE;
 SELECT NSOCIETE, NLIEU, NVENTE, NCODIC, 
DDELIV, DANNULATION, CMODDEL, QVENDUE, CENREG
 FROM RTGV23 WHERE DANNULATION BETWEEN '$DATEDEB' AND '$DATEFIN'
 ORDER BY DANNULATION, NSOCIETE, NLIEU,
  NVENTE ; 
_end
       m_ExecSQL -f SYSIN
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GIE21PAB
       ;;
(GIE21PAB)
       m_CondExec 04,GE,GIE21PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
