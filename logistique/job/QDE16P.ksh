#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QDE16P.ksh                       --- VERSION DU 08/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQDE16 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 97/12/11 AT 17.00.32 BY BURTECM                      
#    STANDARDS: P  JOBSET: QDE16P                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#    REQUETE QDE016P                                                           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QDE16PA
       ;;
(QDE16PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=QDE16PAA
       ;;
(QDE16PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMFDE016 DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN QDE016 (&&QMFFIN8='$QMFFIN8_ANNMMJJ' &&QMFFIN='$QMFFIN_ANNMMJJ' FORM=FDE016
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QDE16P1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QDE16PAB
       ;;
(QDE16PAB)
       m_CondExec 04,GE,QDE16PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REQUETE QDE016P                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAA    STEP  PGM=IKJEFT01,PATTERN=EO3,PARMS=(L=QMFDE016,R=RDAR,F='',         
#              P=QDE016)                                                       
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# PRED     LINK NAME=$MU005P,MODE=I                                            
# PRED1    LINK NAME=$GV903P,MODE=I                                            
# PRED2    LINK NAME=$SA202P,MODE=I                                            
# **************************************                                       
# HOSTIN   DATA  CLASS=VAR,PARMS=QQFFINP,MBR=QDE16P1                           
#          DATA  CLASS=VAR,PARMS=QQFFINS,MBR=QDE16P2                           
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
