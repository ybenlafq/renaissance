#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS140M.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGS140 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/07/11 AT 14.57.29 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GS140M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  AUCUNE MISE A JOUR DANS CETTE CHAINE                                        
#          PEUT ETRE RELANCEE SI PROBLEME                                      
# ********************************************************************         
#  BSM140 : EXTRACTION A PARTIR DES TABLES DB2 POUR CREATION DU FICHIE         
#           FSM140 (VENTES ET STOCKS DE DAL)                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS140MA
       ;;
(GS140MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS140MAA
       ;;
(GS140MAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# * FAYCAL==> AJOUT DE SA202M POUR QUE LE JOB TOURNE APRES LES SAUVES          
# * CAR QUAND PLANTAGE DE LA SAUVE SA55TP OU SA55MP CE JOB DEMARRE             
# * ET ON SE PLANTE EN -904                                                    
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 989                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  ARTICLES                                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : VTK)                                
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  ZONE DE PRIX                                                         
#    RSGA59M  : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59M /dev/null
# ******  COMMISSIONS PAR ZONE DE PRIX                                         
#    RSGA62M  : NAME=RSGA62M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62M /dev/null
# ******  COMMISSIONS BBTE (PRIMES VENDEURS)                                   
#    RSGA75M  : NAME=RSGA75M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75M /dev/null
# ******  RESERVATION SUR CMDE FOURNISSEUR                                     
#    RSGF55M  : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55M /dev/null
# ******  STOCK SOUS-LIEU/ENTREPOT                                             
#    RSGS10M  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10M /dev/null
# ******  STOCK SOUS-LIEU/MAGASIN                                              
#    RSGS30M  : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30M /dev/null
# ******  VENTES MAGASINS                                                      
#    RSHV04M  : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04M /dev/null
# ******  FICHIER VENTES ET STOCKS DAL                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 FSM140 ${DATA}/PTEM/GS140MAA.BSM140AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM140 
       JUMP_LABEL=GS140MAB
       ;;
(GS140MAB)
       m_CondExec 04,GE,GS140MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR PGM BSM141                                            
#       TRIE PAR FAMILLE , CODIC ET GROUPE D'EDTION                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS140MAD
       ;;
(GS140MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS DAL                                         
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS140MAA.BSM140AM
# ******  FICHIER VENTES ET STOCKS DAL   ( TRIE )                              
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -g +1 SORTOUT ${DATA}/PTEM/GS140MAD.BSM140BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_2 4 CH 2
 /FIELDS FLD_CH_234_7 234 CH 7
 /FIELDS FLD_CH_241_5 241 CH 5
 /KEYS
   FLD_CH_241_5 ASCENDING,
   FLD_CH_234_7 ASCENDING,
   FLD_CH_4_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140MAE
       ;;
(GS140MAE)
       m_CondExec 00,EQ,GS140MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM141 :     PRE-FORMATAGE DU FICHIER D'EDITION                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS140MAG
       ;;
(GS140MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER VENTES ET STOCKS DAL   ( TRIE )                              
       m_FileAssign -d SHR -g ${G_A2} FSM140 ${DATA}/PTEM/GS140MAD.BSM140BM
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00M  : NAME=RSAN00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ******  TABLE GENERALISEE (SOUS-TABLES : NCGFC)                              
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING IMBRICATION)             
#    RSGA09M  : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09M /dev/null
# ******  TABLE FAMILLE (EDITIONS DES ETATS)                                   
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******  TABLE D'EDITION DES ETATS (TABLES MARKETING)                         
#    RSGA12M  : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12M /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ******  TABLE ASSOCIATION CODES VALEURS MARKETING/CODES VALEUR DECRI         
#    RSGA25M  : NAME=RSGA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25M /dev/null
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26M  : NAME=RSGA26M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26M /dev/null
# ******  RESERVATIONS SUR COMMANDE FOURNISSEURS                               
#    RSGA53M  : NAME=RSGA53M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53M /dev/null
# ******  FORMATAGE DU FICHIER D'EDITION STOCK ET VENTES                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 FSM141 ${DATA}/PTEM/GS140MAG.BSM141AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM141 
       JUMP_LABEL=GS140MAH
       ;;
(GS140MAH)
       m_CondExec 04,GE,GS140MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI AVANT EDITION                                                          
#        CRITERES GROUPE / WSEQED/(CMARKETING/CVMARKETING)                     
#                 TOTVTE DECROISSANT/CODIC                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS140MAJ
       ;;
(GS140MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE                            
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS140MAG.BSM141AM
# ******  FICHIER D'EDITION STOCK ET VENTES FORMATE TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 382 -g +1 SORTOUT ${DATA}/PTEM/GS140MAJ.BSM141BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_PD_1_3 1 PD 3
 /FIELDS FLD_CH_338_10 338 CH 10
 /FIELDS FLD_CH_353_10 353 CH 10
 /KEYS
   FLD_CH_7_2 ASCENDING,
   FLD_PD_1_3 ASCENDING,
   FLD_CH_338_10 ASCENDING,
   FLD_CH_353_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS140MAK
       ;;
(GS140MAK)
       m_CondExec 00,EQ,GS140MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM142 : GENERATION DE L'EDITION                                            
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS140MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS140MAM
       ;;
(GS140MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE VALEURS DES CODES MARKETING                                    
#    RSGA26M  : NAME=RSGA26M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26M /dev/null
# ******  FICHIER D'EDITION STOCK ET VENTES TRIE                               
       m_FileAssign -d SHR -g ${G_A4} FSM141 ${DATA}/PTEM/GS140MAJ.BSM141BM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ****** SOCIETE : 989                                                         
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ****** ETAT STOCK ET VENTES POUR LE SIEGE(DV ET ACHATS)                      
       m_OutputAssign -c 9 -w ISM140 ISM140
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM142 
       JUMP_LABEL=GS140MAN
       ;;
(GS140MAN)
       m_CondExec 04,GE,GS140MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS140MZA
       ;;
(GS140MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS140MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
