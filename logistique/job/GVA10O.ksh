#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GVA10O.ksh                       --- VERSION DU 08/10/2016 12:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGVA10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/09/26 AT 12.13.31 BY BURTECR                      
#    STANDARDS: P  JOBSET: GVA10O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BSP905 : MAJ HISTO SEUIL REVENTE A PERTE RTSP10                            
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GVA10OA
       ;;
(GVA10OA)
       EXABE=${EXABE:-0}
       RUN=${RUN}
       JUMP_LABEL=GVA10OAA
       ;;
(GVA10OAA)
       m_CondExec ${EXABE},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  DEPENDANCE POUR OPTIMISATION DE PLAN                                        
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** HISTO DU SRP SEUIL DE REVENTE A PERTE                                 
#    RSSP10O  : NAME=RSSP10O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP10O /dev/null
# ****** PRM                                                                   
#    RSGG50O  : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50O /dev/null
# ****** TABLE COMMANDES FOURNISSEURS                                          
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#                                                                              
# ****** PARAMETRE : DACEM                                                     
       m_FileAssign -d SHR FDARDAC ${DATA}/CORTEX4.P.MTXTFIX1/GVA10OAA
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ****** JJMMSSAA                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSP905 
       JUMP_LABEL=GVA10OAB
       ;;
(GVA10OAB)
       m_CondExec 04,GE,GVA10OAA ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
