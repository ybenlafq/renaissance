#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF02P.ksh                       --- VERSION DU 08/10/2016 13:46
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQMF02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/06/14 AT 09.37.55 BY PREPA3                       
#    STANDARDS: P  JOBSET: QMF02P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DARTY'                                                             
# ********************************************************************         
#   LISTE DES STATISTIQUES DE MUTATION                                         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF02PA
       ;;
(QMF02PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXABE=${EXABE:-0}
       RUN=${RUN}
       JUMP_LABEL=QMF02PAA
       ;;
(QMF02PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF02 DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN Q002 (&&JOURDEBUT='$DMOISJ_5_4$DMOISJ_3_2$DMOISJ_1_2' &&JOURFIN='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=F002
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF02P1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF02PAB
       ;;
(QMF02PAB)
       m_CondExec 04,GE,QMF02PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENTREES HS                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF02PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF02PAD
       ;;
(QMF02PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF18 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q018 (&&DEBUT='$DMOISQ' &&FIN='$FMOISQ' FORM=F018
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF02P2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF02PAE
       ;;
(QMF02PAE)
       m_CondExec 04,GE,QMF02PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENCOURS DE HS                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF02PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF02PAG
       ;;
(QMF02PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF20 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF02P3
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF02P3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF02PAH
       ;;
(QMF02PAH)
       m_CondExec 04,GE,QMF02PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES RECEPTIONS EN ATENTES QMF023                                     
#   MVTS DE STOCK                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF02PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF02PAJ
       ;;
(QMF02PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF23 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q023 (&&DEBUT='$DMOISJ_5_4$DMOISJ_3_2$DMOISJ_1_2' &&FIN='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' &&SOC='$DIFDEP_1_3' FORM=F023
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF02P4 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF02PAK
       ;;
(QMF02PAK)
       m_CondExec 04,GE,QMF02PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES ENTREES                                                          
#   LISTE DES ENTREES PLUS UTILISE  COMMENT LE 081098 CGA                      
# ********************************************************************         
# AAU  STEP PGM=IKJEFT01,PATTERN=EO4,PARMS=((L,QMF25),(S,RDAR),      -         
#                (M,QMF02P5))                                                  
# QMFPARM  DATA CLASS=VAR,PARMS=QMF25P2,MBR=QMF02P5                            
# ********************************************************************         
#   LISTE DES SORTIES                                                          
# ********************************************************************         
# AAZ      STEP PGM=IKJEFT01,PATTERN=EO4,PARMS=((L,QMF26),(S,RDAR),            
#                (M,QMF02P6))                                                  
# QMFPARM  DATA CLASS=VAR,PARMS=QMF26P2,MBR=QMF02P6                            
# ********************************************************************         
#   LISTE DES STOCKS                                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF02PAM PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF02PAM
       ;;
(QMF02PAM)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF27 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF02P7
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF02P7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF02PAN
       ;;
(QMF02PAN)
       m_CondExec 04,GE,QMF02PAM ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
