#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL46M.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGTL46 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.15.03 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL46M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000001 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000001 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL46MA
       ;;
(GTL46MA)
       DATE=${DATEJ}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       RUN=${RUN}
       UPSI=${UPSI:-''}
       USER=${USER:-''}
       JUMP_LABEL=GTL46MAA
       ;;
(GTL46MAA)
       m_CondExec ${EXAAA},NE,YES 
# AA      IMSSTEP PGM=BIG002B,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),              
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    CHARGEMENT DANS DISPATCH DES      *                                       
#    LISTES DE DESTOCKAGES             *                                       
# **************************************                                       
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46MR5
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
# DIGVP0   FILE  NAME=DI0000DM,MODE=I,REST=(YES,GTL46MP5)                      
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46M01
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL1 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000001
JTL013$VDATEJ__AAMMJJ000000001
JTL014$VDATEJ__AAMMJJ000000001
_end
       m_ProgramExec -b TPIGM 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000002 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000002 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46MAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL46MAD
       ;;
(GTL46MAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BIG002B,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),              
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46MR6
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
# DIGVP0   FILE  NAME=DI0000DM,MODE=I,REST=(YES,GTL46MP6)                      
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46M02
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL2 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000002
JTL013$VDATEJ__AAMMJJ000000002
JTL014$VDATEJ__AAMMJJ000000002
_end
       m_ProgramExec -b TPIGM 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000003 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000003 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46MAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL46MAG
       ;;
(GTL46MAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BIG002B,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),              
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46MR7
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
# DIGVP0   FILE  NAME=DI0000DM,MODE=I,REST=(YES,GTL46MP7)                      
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46M03
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL3 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000003
JTL013$VDATEJ__AAMMJJ000000003
JTL014$VDATEJ__AAMMJJ000000003
_end
       m_ProgramExec -b TPIGM 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B.R DANS EOS                                        
# ********************************************************************         
#   ETAT JTL021 DESTINATION 000000000 B.R SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL022 DESTINATION 000000000 B.R AVEC CODE BARRE                      
# ********************************************************************         
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46MAJ PGM=DFSRRC00   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL46MAJ
       ;;
(GTL46MAJ)
       m_CondExec ${EXAAP},NE,YES 
# AP      IMSSTEP PGM=BIG002B,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),              
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46MR8
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
# DIGVP0   FILE  NAME=DI0000DM,MODE=I,REST=(YES,GTL46MP8)                      
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46M04
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BR IMPRIM
#  ETAT DATE SOIT JTL021,AAMMJJ,000000000  000000000 = DEST DS IG50            
       m_FileAssign -i SYSIN
JTL021$VDATEJ__AAMMJJ000000000
JTL022$VDATEJ__AAMMJJ000000000
JTL023$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGM 
# ********************************************************************         
#  B I G 0 0 2                                                                 
# ********************************************************************         
#  CHARGEMENT DU JTL301 DANS DISPATCH                                          
#   REPRISE : OUI APRES AVOIR VERIFIER LE BACKOUT IMS RGTL46M                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46MAM PGM=DFSRRC00   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL46MAM
       ;;
(GTL46MAM)
       m_CondExec ${EXAAU},NE,YES 
# AU      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=(YES,GTL46MR3),VSAMP=DFSVSAMP                
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46MR3
       m_OutputAssign -c "*" DDOTV02
#                                                                              
# ******* BASE EDITION                                                         
# DIGVP0   FILE  NAME=DI0000DM,MODE=U,REST=(YES,GTL46MP3)                      
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46M05
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0023 IMPRIM
#  ETAT DATE  SOIT JTL301,AAMMJJ,000000000                                     
       m_FileAssign -i SYSIN
JTL301$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGM 
#                                                                              
# ********************************************************************         
#  BIG002  : EDITION LISTE ANOMALIE DE LIVRAISON ETAT: JTL401                  
#   REPRISE : OUI APRES AVOIR VERIFIER LE BACKOUT IMS RGTL46M                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL46MAQ PGM=DFSRRC00   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL46MAQ
       ;;
(GTL46MAQ)
       m_CondExec ${EXAAZ},NE,YES 
# AZ      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=(YES,GTL46MR4),VSAMP=DFSVSAMP                
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL46MR4
       m_OutputAssign -c "*" DDOTV02
#                                                                              
# ******* BASE EDITION                                                         
# DIGVP0   FILE  NAME=DI0000DM,MODE=U,REST=(YES,GTL46MP4)                      
# DIGVIP   FILE  NAME=DI0000IM,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX989.F89.DI0000DM
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX989.F89.DI0000IM
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL46M06
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BIG0027 IMPRIM
#  ETAT DATE  SOIT JTL401,AAMMJJ+1,000000000                                   
       m_FileAssign -i SYSIN
JTL401$VDATEJ__AAMMJJ000000000
_end
       m_ProgramExec -b TPIGM 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP COMREG   PGM=CZX3PEPI   **                                          
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
