#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CLIE0P.ksh                       --- VERSION DU 07/10/2016 22:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCLIE0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/05/11 AT 16.27.19 BY PREPA2                       
#    STANDARDS: P  JOBSET: CLIE0P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# *******************************************************************          
#  TRI DES FICHIERS EXTRACTION DES VENTES                                      
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CLIE0PA
       ;;
(CLIE0PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CLIE0PAA
       ;;
(CLIE0PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****================================**                                       
#  DEPENDANCE POUR PLAN                                                        
# ****================================**                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F91.BBC101AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F61.BBC101AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F89.BBC101AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F16.BBC101AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F07.BBC101AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F45.BBC101AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FBC102AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -g +1 SORTOUT ${DATA}/PTEM/CLIE0PAA.BBC048AP
# *************************************************************                
#   BBC048                                                                     
#   DETERMINE SI IL FAUT REDRESSER OU DEDOUBLONNER                             
# **************************************************************               
#                                                                              
# ***********************************                                          
# *   STEP CLIE0PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CLIE0PAD
       ;;
(CLIE0PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE                                                                       
#    RSBC12   : NAME=RSBC12P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC12 /dev/null
       m_FileAssign -d SHR -g ${G_A1} FBC401 ${DATA}/PTEM/CLIE0PAA.BBC048AP
#  FICHIER POUR HISTORIQUE DARTY ALLANT VERS LA CLIE0P                         
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FBC409 ${DATA}/PEX0/F07.BBC048BP
#  FICHIER ALLANT VERS CLIREP EN ENTREE DU BBC030                              
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -g +1 FBCUCM ${DATA}/PEX0/F07.BBC048CP
       m_FileAssign -i FDATE
$FDATE
_end
# ******************************************************                       
#   BBC049                                                                     
#   ENVOI LES MESSAGES MQ A UCM                                                
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP CLIE0PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BBC048 
       JUMP_LABEL=CLIE0PAG
       ;;
(CLIE0PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} FBC048 ${DATA}/PEX0/F07.BBC048CP
       m_FileAssign -i FDATE
$FDATE
_end
# *************************************************************                
#  DEPENDANCE POUR PLAN                                                        
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC049 
       JUMP_LABEL=CLIE0PAH
       ;;
(CLIE0PAH)
       m_CondExec 04,GE,CLIE0PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CLIE0PZA
       ;;
(CLIE0PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CLIE0PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
