#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GD900P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGD900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/12/05 AT 14.28.17 BY BURTECW                      
#    STANDARDS: P  JOBSET: GD900P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGD005  EVALUATION DE LA CHARGE JOURNALIERE SUR 14 JOURS                    
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GD900PA
       ;;
(GD900PA)
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       EXAAA=${EXAAA:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GD900PAA
       ;;
(GD900PAA)
       m_CondExec ${EXAAA},NE,YES 
# ****************************************                                     
#  DEPENDANCES POUR PLAN :               *                                     
#    OBLIGATOIRE POUR LOGIQUE APPL       *                                     
#    POUR AVOIR LES APPAREILS A COMMUTER *                                     
# ****************************************                                     
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGB10   : NAME=RSGB10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB10 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -g +1 FGD05 ${DATA}/PTEM/GD900PAA.BGD005AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGD005 
       JUMP_LABEL=GD900PAB
       ;;
(GD900PAB)
       m_CondExec 04,GE,GD900PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   31,7,A,1,6,A  CODIC + SOCDEPOT                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAD PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAD
       ;;
(GD900PAD)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GD900PAA.BGD005AP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -g +1 SORTOUT ${DATA}/PTEM/GD900PAD.BGD005BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_31_7 31 CH 7
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_31_7 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 070 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD900PAE
       ;;
(GD900PAE)
       m_CondExec 00,EQ,GD900PAD ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGD006  PREP AU CHARGEMENT DES TABLES DE SYNTHESE                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAG PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAG
       ;;
(GD900PAG)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g ${G_A2} FGD06 ${DATA}/PTEM/GD900PAD.BGD005BP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 RTGD15 ${DATA}/PTEM/GD900PAG.BGD006BP
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 RTGD20 ${DATA}/PTEM/GD900PAG.BGD006CP
#  POUR TABLE RTGD30 = AUX ARTICLES PRESENTS DANS LES MUTS                     
       m_FileAssign -d NEW,CATLG,DELETE -r 162 -g +1 RTGD30 ${DATA}/PTEM/GD900PAG.GD0030UP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGD006 
       JUMP_LABEL=GD900PAH
       ;;
(GD900PAH)
       m_CondExec 04,GE,GD900PAG ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER SERVANT A LOADER LA RTGD30                                  
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAJ PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAJ
       ;;
(GD900PAJ)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GD900PAG.GD0030UP
       m_FileAssign -d NEW,CATLG,DELETE -r 162 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD30RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 162 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD900PAK
       ;;
(GD900PAK)
       m_CondExec 00,EQ,GD900PAJ ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD30                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAM PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAM
       ;;
(GD900PAM)
       m_CondExec ${EXABJ},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSGD30   : NAME=RSGD30,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD30 /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PGD0/F07.RELOAD.GD30RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD900PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD900P_GD900PAM_RTGD30.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD900PAN
       ;;
(GD900PAN)
       m_CondExec 04,GE,GD900PAM ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,15,A  POUR TABLE RTGD15                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAQ PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAQ
       ;;
(GD900PAQ)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GD900PAG.BGD006BP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 SORTOUT ${DATA}/PGD0/F07.GD0015UP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 1 CH 15
 /FIELDS FLD_PD_29_4 29 PD 4
 /FIELDS FLD_PD_36_4 36 PD 4
 /FIELDS FLD_PD_26_3 26 PD 3
 /FIELDS FLD_PD_33_3 33 PD 3
 /FIELDS FLD_PD_19_3 19 PD 3
 /FIELDS FLD_PD_22_4 22 PD 4
 /FIELDS FLD_PD_16_3 16 PD 3
 /KEYS
   FLD_CH_1_15 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_16_3,
    TOTAL FLD_PD_19_3,
    TOTAL FLD_PD_22_4,
    TOTAL FLD_PD_26_3,
    TOTAL FLD_PD_29_4,
    TOTAL FLD_PD_33_3,
    TOTAL FLD_PD_36_4
 /* Record Type = F  Record Length = 46 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD900PAR
       ;;
(GD900PAR)
       m_CondExec 00,EQ,GD900PAQ ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER SERVANT A LOADER LA RTGD15                                  
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAT PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAT
       ;;
(GD900PAT)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PGD0/F07.GD0015UP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD15RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
 /* Record Type = F  Record Length = 46 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD900PAU
       ;;
(GD900PAU)
       m_CondExec 00,EQ,GD900PAT ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD15                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PAX PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GD900PAX
       ;;
(GD900PAX)
       m_CondExec ${EXABY},NE,YES 
#  FICHIERS DE TRAVAIL  AU MOINS = AU SYSREC ,ATTENTION SYSWK = 3350           
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSGD15   : NAME=RSGD15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD15 /dev/null
       m_FileAssign -d SHR -g ${G_A7} SYSREC ${DATA}/PGD0/F07.RELOAD.GD15RP
#  SYSUT1 AU MOINS = A LA TAILLE DE L INDEX ATTENTION SYSWK=3350               
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD900PAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD900P_GD900PAX_RTGD15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD900PAY
       ;;
(GD900PAY)
       m_CondExec 04,GE,GD900PAX ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,38,A  POUR TABLE RTGD20                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PBA PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GD900PBA
       ;;
(GD900PBA)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GD900PAG.BGD006CP
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PGD0/F07.GD0020UP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_39_3 39 PD 3
 /FIELDS FLD_PD_46_3 46 PD 3
 /FIELDS FLD_PD_49_4 49 PD 4
 /FIELDS FLD_PD_42_4 42 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_1_38 1 CH 38
 /FIELDS FLD_PD_53_3 53 PD 3
 /KEYS
   FLD_CH_1_38 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_39_3,
    TOTAL FLD_PD_42_4,
    TOTAL FLD_PD_46_3,
    TOTAL FLD_PD_49_4,
    TOTAL FLD_PD_53_3,
    TOTAL FLD_PD_56_4
 /* Record Type = F  Record Length = 73 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD900PBB
       ;;
(GD900PBB)
       m_CondExec 00,EQ,GD900PBA ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER SERVANT A LOADER LA RTGD20                                  
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PBD PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GD900PBD
       ;;
(GD900PBD)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PGD0/F07.GD0020UP
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PGD0/F07.RELOAD.GD20RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_38 1 CH 38
 /KEYS
   FLD_CH_1_38 ASCENDING
 /* Record Type = F  Record Length = 73 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GD900PBE
       ;;
(GD900PBE)
       m_CondExec 00,EQ,GD900PBD ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGD20                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GD900PBG PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GD900PBG
       ;;
(GD900PBG)
       m_CondExec ${EXACN},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSGD20   : NAME=RSGD20,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD20 /dev/null
       m_FileAssign -d SHR -g ${G_A10} SYSREC ${DATA}/PGD0/F07.RELOAD.GD20RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD900PBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GD900P_GD900PBG_RTGD20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GD900PBH
       ;;
(GD900PBH)
       m_CondExec 04,GE,GD900PBG ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GD900PZA
       ;;
(GD900PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GD900PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
