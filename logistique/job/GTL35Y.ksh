#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL35Y.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGTL35 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.12.01 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL35Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BTL035 : VENTES LIVREES PAR EQUIPE POUBELLE OU                             
#            VENTES NON TOPEES LIVREES                                         
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL35YA
       ;;
(GTL35YA)
#
#GTL35YAQ
#GTL35YAQ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GTL35YAQ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL35YAA
       ;;
(GTL35YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DES TABLES DE TOURNEES        *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  TABLE DES VENTES/ADRESSES                                            
#    RSGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
# ******  TABLE DES LIGNES DE VENTES ARTICLES                                  
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  TOURNEES/DETAILS                                                     
#    RSTL02Y  : NAME=RSTL02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02Y /dev/null
#                                                                              
# ******  FICHIER VENTES LIVREES PAR EQUIPE POUBELLE                           
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FTL035 ${DATA}/PTEM/GTL35YAA.BTL035AY
# ******  FICHIER VENTES NON TOPEES LIVREES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 FTL036 ${DATA}/PTEM/GTL35YAA.BTL036AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL035 
       JUMP_LABEL=GTL35YAB
       ;;
(GTL35YAB)
       m_CondExec 04,GE,GTL35YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER VENTES LIVREES  PAR EQUIPE POUBELLE                    
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL35YAD
       ;;
(GTL35YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GTL35YAA.BTL035AY
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL35YAD.BTL035BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_10 12 CH 10
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_3 9 CH 3
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_10 ASCENDING
 /* Record Type = F  Record Length = 113 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL35YAE
       ;;
(GTL35YAE)
       m_CondExec 00,EQ,GTL35YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTL040 : EDITION DES VENTES AVEC EQUIPE POUBELLE (ETAT JTL401)              
#  REPRISE: NON (UTILISER LE BACKOUT)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35YAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL35YAG
       ;;
(GTL35YAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BTL040,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               REGION=4096K,LOG=(YES,GTL35YR1)                                
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL35YR1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#                                                                              
# ******  DATE : JJMMSSAA                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FTL035 ${DATA}/PTEM/GTL35YAD.BTL035BY
# ******  IMPRESSION GENERALISEE EN UPDATE                                     
# DIGVP0   FILE  NAME=DI0000DY,MODE=U,REST=(YES,GTL35YP1)                      
# DIGVIP   FILE  NAME=DI0000IY,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL35YAG
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#    CREATION DU FICHIER  POUR LE LOAD DE P945.RTTL08                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL35YAJ
       ;;
(GTL35YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GTL35YAA.BTL036AY
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 SORTOUT ${DATA}/PTL945/F45.RELOAD.TL08RY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_30 1 CH 30
 /KEYS
   FLD_CH_1_30 ASCENDING
 /* Record Type = F  Record Length = 37 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL35YAK
       ;;
(GTL35YAK)
       m_CondExec 00,EQ,GTL35YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTTL08                                                   
#    REPRISE :OUI.ATTENTION UN CODE 8 EST ANORMAL                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35YAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL35YAM
       ;;
(GTL35YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PTL945/F45.RELOAD.TL08RY
#    RSTL08Y  : NAME=RSTL08Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSTL08Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSTL08Y /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL35YAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GTL35Y_GTL35YAM_RTTL08.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GTL35YAN
       ;;
(GTL35YAN)
       m_CondExec 04,GE,GTL35YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLESPACE RSTL08Y DE LA PYDTL00                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35YAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL35YZA
       ;;
(GTL35YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL35YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
