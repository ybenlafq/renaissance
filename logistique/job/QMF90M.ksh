#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF90M.ksh                       --- VERSION DU 08/10/2016 15:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMQMF90 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/03/02 AT 09.33.18 BY BURTECR                      
#    STANDARDS: P  JOBSET: QMF90M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='DARTY'                                                             
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF111TADM (DACEM)                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF90MA
       ;;
(QMF90MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       RUN=${RUN}
       JUMP_LABEL=QMF90MAA
       ;;
(QMF90MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i SORTIN
RUN ADMFIL.Q111TADM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111TADM
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF90MAB
       ;;
(QMF90MAB)
       m_CondExec 00,EQ,QMF90MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS TOUS MAGASINS RUBRIQUE AGREGEE QMF111TADM (DACEM)              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAD
       ;;
(QMF90MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w Q111TADM DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF90MAE
       ;;
(QMF90MAE)
       m_CondExec 04,GE,QMF90MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF111TADM                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAG PGM=IEFBR14    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAG
       ;;
(QMF90MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF111TDDM (DACEM)                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAJ
       ;;
(QMF90MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q111TDDM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111TDDM
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF90MAK
       ;;
(QMF90MAK)
       m_CondExec 00,EQ,QMF90MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS TOUS MAGASINS DETAIL PAR MAGASIN QMF111TDDM (DACEM)            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAM
       ;;
(QMF90MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w Q111TDDM DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF90MAN
       ;;
(QMF90MAN)
       m_CondExec 04,GE,QMF90MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF111TDDM                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAQ PGM=IEFBR14    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAQ
       ;;
(QMF90MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF111DADM                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAT
       ;;
(QMF90MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q111DADM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111DADM
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF90MAU
       ;;
(QMF90MAU)
       m_CondExec 00,EQ,QMF90MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAG RUBRIQUE AGREGEE                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MAX
       ;;
(QMF90MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w Q111DADM DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF90MAY
       ;;
(QMF90MAY)
       m_CondExec 04,GE,QMF90MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF111DADM                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBA PGM=IEFBR14    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBA
       ;;
(QMF90MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF111DDDM                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBD
       ;;
(QMF90MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q111DDDM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111DDDM
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 SORTOUT ${DATA}/PEX0.F89.QMF001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF90MBE
       ;;
(QMF90MBE)
       m_CondExec 00,EQ,QMF90MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAG RUBRIQUE DETAILLEE                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBG
       ;;
(QMF90MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w Q111DDDM DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F89.QMF001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF90MBH
       ;;
(QMF90MBH)
       m_CondExec 04,GE,QMF90MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF111DDDM                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBJ PGM=IEFBR14    ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBJ
       ;;
(QMF90MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F89.QMF001AM
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   LISTE STATS TOUS MAGASINS RUBRIQUE AGREGEE QMF111TADM (DACEM)              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBM
       ;;
(QMF90MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF11TAD
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q111TADM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111TADM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF90M1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF90MBN
       ;;
(QMF90MBN)
       m_CondExec 04,GE,QMF90MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS TOUS MAGASINS DETAIL PAR MAGASIN QMF111TDDM (DACEM)            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBQ
       ;;
(QMF90MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF11TDD
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q111TDDM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111TDDM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF90M2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF90MBR
       ;;
(QMF90MBR)
       m_CondExec 04,GE,QMF90MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS SURFACE EGALE RUBRIQUE AGREGEE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBT
       ;;
(QMF90MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF11EAD
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q111EADM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111EADM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF90M3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF90MBU
       ;;
(QMF90MBU)
       m_CondExec 04,GE,QMF90MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LA QUERY QMF111EDDM (DACEM)                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MBX
       ;;
(QMF90MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF11EDD
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q111EDDM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111EDDM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF90M4 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF90MBY
       ;;
(QMF90MBY)
       m_CondExec 04,GE,QMF90MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE STATS DETAIL PAR MAG RUBRIQUE AGREGEE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF90MCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF90MCA
       ;;
(QMF90MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 DSQPRINT ${DATA}/PXX0/F89.QMF11DAD
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q111DADM (&&JOUR='$FMOISJ_5_4$FMOISJ_3_2$FMOISJ_1_2' FORM=ADMFIL.F111DADM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF90M7 -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF90MCB
       ;;
(QMF90MCB)
       m_CondExec 04,GE,QMF90MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
