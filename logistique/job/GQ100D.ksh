#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GQ100D.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGQ100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 19.05.02 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GQ100D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG : BGQ100                                                              
#   EXTRACTION DES DONN�ES                                                     
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GQ100DA
       ;;
(GQ100DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GQ100DAA
       ;;
(GQ100DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE -- VUE RVGA01QA -- S/TABLE QCGAR                   
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGQ01   : NAME=RSGQ01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ01 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL02 /dev/null
#    RSTL06   : NAME=RSTL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL06 /dev/null
#                                                                              
# ******* FICHIER DES LIBELLES                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGQ100 ${DATA}/PXX0/F91.FGQ100AD
#                                                                              
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FMOIS                                                                
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGQ100 
       JUMP_LABEL=GQ100DAB
       ;;
(GQ100DAB)
       m_CondExec 04,GE,GQ100DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# **             TRI DU FICHIER ISSU DU BGQ100                       *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQ100DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GQ100DAD
       ;;
(GQ100DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ** FICHIER EN ENTREE                                                         
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F91.FGQ100AD
# ** FICHIER EN SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GQ100DAD.FGQ100BD
# **                                                                           
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_1_6 1 CH 6
 /FIELDS FLD_BI_52_5 52 CH 5
 /FIELDS FLD_BI_77_5 77 CH 5
 /KEYS
   FLD_BI_1_6 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_52_5 ASCENDING,
   FLD_BI_77_5 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GQ100DAE
       ;;
(GQ100DAE)
       m_CondExec 00,EQ,GQ100DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * PROG : BGQ101                                                    *         
# * FORMATTAGE DES DONN�ES                                           *         
# * REPRISE: OUI SI FIN ANORMALE                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQ100DAG PGM=BGQ101     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GQ100DAG
       ;;
(GQ100DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER DES LIBELLES                                                 
       m_FileAssign -d SHR -g ${G_A2} FGQ100 ${DATA}/PTEM/GQ100DAD.FGQ100BD
#                                                                              
# ******* FICHIER DES LIBELLES                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FGQ101 ${DATA}/PXX0/F91.FGQ100CD
       m_ProgramExec BGQ101 
#                                                                              
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  CLASS=VAR,PARMS=GQ100D                                        
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGQ100D                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQ100DAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GQ100DAJ
       ;;
(GQ100DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQ100DAJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GQ100DAJ.FTGQ100D
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGQ100D                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GQ100DAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GQ100DAM
       ;;
(GQ100DAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GQ100DAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GQ100DAJ.FTGQ100D(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GQ100DAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GQ100DAQ
       ;;
(GQ100DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQ100DAQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GQ100DZA
       ;;
(GQ100DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GQ100DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
