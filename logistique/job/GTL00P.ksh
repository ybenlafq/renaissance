#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL00P.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGTL00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/25 AT 11.01.25 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#   TRAITEMENT LE VENDREDI ON TRAITE  LES B L. DU DIMANCHE ET DU LUNDI         
#   BTL100 : AFFECTE UN PROFIL DE TOURNEES AUX LIVRAISONS DU JOUR              
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL00PA
       ;;
(GTL00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL00PAA
       ;;
(GTL00PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LES TABLES DE VENTES   *                                       
#    MAJ                               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSAN00   : NAME=RSAN00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSAN00 /dev/null
#   CALENDRIER DES LIVRAISONS                                                  
#    RSTL10   : NAME=RSTL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL10 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL100 
       JUMP_LABEL=GTL00PAB
       ;;
(GTL00PAB)
       m_CondExec 04,GE,GTL00PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREE UNE GENERATION  A VIDE DE FACON A LES PRENDRE EN DISP=MOD LORS         
#  DES REPRISES DE BTL000                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAD
       ;;
(GTL00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT1 ${DATA}/PTL0/F07.BTL001CP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT2 ${DATA}/PTL0/F07.BTL002DP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GTL00PAE
       ;;
(GTL00PAE)
       m_CondExec 16,NE,GTL00PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRAITEMENT LE VENDREDI  ON TRAITE  LES B L. DU DIMANCHE ET DU LUND         
#   BTL000 : EXTRACTION DES VENTES A LIVRER                                    
#   FIC FTL000 : FIC DES VENTES POUR LOGICIEL DES LIVRAISONS                   
#   FIC FTL001 : FIC DES ANOMALIES,CUMUL DE PROFIL(1 LIGNE PAR PROFIL)         
#   PGM = BTL000                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAG
       ;;
(GTL00PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
# ******  TABLES EN LECTURE ET EN MISE A JOUR                                  
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGD05   : NAME=RSGD05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD05 /dev/null
#    RSTL05   : NAME=RSTL05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05 /dev/null
# ******  CALENDRIER DES LIVRAISONS                                            
#    RSTL10   : NAME=RSTL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL10 /dev/null
       m_FileAssign -d SHR FTL000 /dev/null
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A1} FTL001 ${DATA}/PTL0/F07.BTL001CP
# ******  FICHIER DES PROFILS A EDITER EN DIFFERE :POUR PGM BTL046             
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A2} FTL002 ${DATA}/PTL0/F07.BTL002DP
# ******  FICHIER DES APPAREILS A COMMUTER                                     
       m_FileAssign -d SHR FTL003 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
# ***     PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL000 
       JUMP_LABEL=GTL00PAH
       ;;
(GTL00PAH)
       m_CondExec 04,GE,GTL00PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL001CP POUR ELIMINER LES DOUBLONS                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAJ
       ;;
(GTL00PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTL0/F07.BTL001CP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F07.BTL001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00PAK
       ;;
(GTL00PAK)
       m_CondExec 00,EQ,GTL00PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL002DP POUR ELIMINER LES DOUBLONS CREES LORS DES           
#  REPRISES (CREATION ENTRE 2 COMMITS)                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAM
       ;;
(GTL00PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTL0/F07.BTL002DP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F07.BTL002AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00PAN
       ;;
(GTL00PAN)
       m_CondExec 00,EQ,GTL00PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LES GTL45P GTL55P                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAQ
       ;;
(GTL00PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTL0/F07.BTL001BP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTL0/F07.BTL002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F07.BTL001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00PAR
       ;;
(GTL00PAR)
       m_CondExec 00,EQ,GTL00PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   FUSION 9,3,A,1,8,A,12,23,A                                                 
#   SOC,JOUR,PLAN,TYPE D ENR,ZONE TRI                                          
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAT
       ;;
(GTL00PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTL0/F07.BTL001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL00PAT.BTL030AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_23 12 CH 23
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_23 ASCENDING
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00PAU
       ;;
(GTL00PAU)
       m_CondExec 00,EQ,GTL00PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    ECRITURE   DANS LA BASE IMPRESSION GENERALISEE                            
#    1 LISTE NOMBRE DE PROFILS TRAITES                                         
#    1 LISTE ANOMALIES                                                         
#    1 LISTE DES TRANSITAIRES (POUR L ETRANGER)                                
#    REPRISE: NON  BACKOUT DU STEP PAR LOGGING IMS                  *          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PAX
       ;;
(GTL00PAX)
       m_CondExec ${EXABJ},NE,YES 
# ABJ     IMSSTEP PGM=BTL030,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#                LOG=(YES,GTL00RC1)                                            
#  DB IMPRESSION GENERALISEE                                                   
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL00RC1
       m_OutputAssign -c "*" DDOTV02
# DIGVP0   FILE  NAME=DI0000DP,MODE=U,REST=(YES,GTL00PR1)                      
# DIGVIP   FILE  NAME=DI0000IP,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX0.F07.DI0000DP
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX0.F07.DI0000IP
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL00PAX
       m_FileAssign -d SHR -g ${G_A8} FTL030 ${DATA}/PTEM/GTL00PAT.BTL030AP
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGP 
# ********************************************************************         
#  MISE A JOUR DES RETOURS DE LIVRAISON RECYCLEES                              
#  PGM = BTL085                                                                
#  REPRISE: OUI APRES ABEND                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PBA
       ;;
(GTL00PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSTL04   : NAME=RSTL04,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04 /dev/null
# ******  DEPENDANCE POUR PLAN                                                 
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL085 
       JUMP_LABEL=GTL00PBB
       ;;
(GTL00PBB)
       m_CondExec 04,GE,GTL00PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL00PZA
       ;;
(GTL00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
