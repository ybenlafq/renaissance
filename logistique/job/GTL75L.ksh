#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL75L.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGTL75 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/01/27 AT 10.55.14 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL75L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER FTL076 (FICHIER DES VENTES DE LA VEILLE)                     
#  SUR : DATE LIVRAISON,CODE MAG,NUM DE VENTE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL75LA
       ;;
(GTL75LA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL75LAA
       ;;
(GTL75LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DES NUMEROS DE FOLIOS        *                                        
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PTL961/F61.BTL076AL
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PTEM/GTL75LAA.BTL075AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_8 14 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_14_8 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 40 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75LAB
       ;;
(GTL75LAB)
       m_CondExec 00,EQ,GTL75LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL075                                                                
# ********************************************************************         
#   GENERATION DU FICHIER DES VENTES EN ANOMALIE DE LIVRAISON PAR MAGA         
#   POUR UNE DATE DE LIVRAISON                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL75LAD
       ;;
(GTL75LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02L  : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02L /dev/null
# ******  TABLE DES ENTETES LIGNE DE VENTE                                     
#    RTGV10L  : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10L /dev/null
# ******  TABLE DES LIGNES DE VENTE                                            
#    RTGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11L /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01L  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01L /dev/null
# ******  TABLE DES DETAILS TOURNEES DE LIVRAISON                              
#    RTTL02L  : NAME=RSTL02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02L /dev/null
# ******  FICHIER DES TOURNEES PREVISIONNELLES                                 
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL961/F61.BTL001AL
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A1} FTL075 ${DATA}/PTEM/GTL75LAA.BTL075AL
# ******  FICHIER VENTE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FTL076 ${DATA}/PTL961/F61.BTL076AL
# ******  FICHIER VENTE EN ANOMALIE DE LIVRAISON PAR MAGASIN                   
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FTL077 ${DATA}/PTEM/GTL75LAD.BTL077AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL075 
       JUMP_LABEL=GTL75LAE
       ;;
(GTL75LAE)
       m_CondExec 04,GE,GTL75LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FTL077 (FICHIER DES VENTES EN ANOMALIE)                      
#  SUR : CODE PROFIL,CODE MAG,CODE TOURNEE,NUM DE VENTE                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL75LAG
       ;;
(GTL75LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GTL75LAD.BTL077AL
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/GTL75LAG.BTL078AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_43_6 43 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_34_1 34 CH 1
 /KEYS
   FLD_CH_43_6 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_34_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL75LAH
       ;;
(GTL75LAH)
       m_CondExec 00,EQ,GTL75LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL080                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#   EDITION DES VENTES EN ANOMALIE DE LIVRAISON                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL75LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL75LAJ
       ;;
(GTL75LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RTGV02L  : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV02L /dev/null
# ******  TABLE DES GENERALITES TOURNEES DE LIVRAISON                          
#    RTTL01L  : NAME=RSTL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL01L /dev/null
# ******  FICHIER LIVRAISON AVANT SAISIE                                       
       m_FileAssign -d SHR -g ${G_A3} FTL080 ${DATA}/PTEM/GTL75LAG.BTL078AL
# ******  FICHIER EDITION                                                      
       m_OutputAssign -c 9 -w ITL080 ITL080
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL080 
       JUMP_LABEL=GTL75LAK
       ;;
(GTL75LAK)
       m_CondExec 04,GE,GTL75LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL75LZA
       ;;
(GTL75LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL75LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
