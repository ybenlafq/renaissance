#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL47D.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL47 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/26 AT 23.30.53 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL47D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000001 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000001 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL47DA
       ;;
(GTL47DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=GTL47DAA
       ;;
(GTL47DAA)
       m_CondExec ${EXAAA},NE,YES 
# AA      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL47DRA
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL47D01
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL1 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000001  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000001
JTL013$VDATEJ__AAMMJJ000000001
JTL014$VDATEJ__AAMMJJ000000001
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000002 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000002 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL47DAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL47DAD
       ;;
(GTL47DAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL47DRB
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL47D02
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL2 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000002  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000002
JTL013$VDATEJ__AAMMJJ000000002
JTL014$VDATEJ__AAMMJJ000000002
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#  BIG002 : CHARGEMENT DES B L DANS EOS                                        
# ********************************************************************         
#   ETAT JTL012 DESTINATION 000000003 B.L SANS CODE BARRE                      
#              OU                                                              
#   ETAT JTL013 DESTINATION 000000003 B.L AVEC CODE BARRE                      
# ********************************************************************         
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL47DAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL47DAG
       ;;
(GTL47DAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BIG002,TYPE=DBB,PSB=GIG002,LANG=(ASM,PLI),               
#               UPSI=00000010,LOG=NO,VSAMP=DFSVSAMP                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL47DRC
       m_OutputAssign -c "*" DDOTV02
#  D BASE EDITION                                                              
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL47D03
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
#                                                                              
       m_OutputAssign -c 9 -w BL3 IMPRIM
#  ETAT DATE SOIT JTL012,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
#     OU                                                                       
#  ETAT DATE SOIT JTL013,AAMMJJ,000000003  000000001 = LA DEST DS IG50         
       m_FileAssign -i SYSIN
JTL012$VDATEJ__AAMMJJ000000003
JTL013$VDATEJ__AAMMJJ000000003
JTL014$VDATEJ__AAMMJJ000000003
_end
       m_ProgramExec -b TPIGD 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
