#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRM23P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGRM23 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 19.17.44 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GRM23P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRM23PA
       ;;
(GRM23PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRM23PAA
       ;;
(GRM23PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# *********************************                                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204KP
       m_FileAssign -d NEW,CATLG,DELETE -r 38 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAA.BRM230AP
# *********************************                                            
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_14 1 CH 14
 /FIELDS FLD_BI_15_6 15 CH 6
 /FIELDS FLD_PD_21_3 21 PD 3
 /KEYS
   FLD_BI_15_6 ASCENDING,
   FLD_BI_1_14 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PAD
       ;;
(GRM23PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************                                            
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BMU204MP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAD.BRM230BP
# ********************************************************                     
#  BRM230 : PGM EXTRACTION REAPRO MAG PAR GROUPE DE GROUPE                     
# ********************************************************                     
#  REPRISE: OUI                                                                
# ********************************************************                     
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_16_3 16 PD 3
 /FIELDS FLD_BI_8_8 8 CH 8
 /FIELDS FLD_BI_1_7 1 CH 7
 /KEYS
   FLD_BI_8_8 ASCENDING,
   FLD_BI_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_16_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PAG
       ;;
(GRM23PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM75   : NAME=RSRM75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM75 /dev/null
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA66 /dev/null
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGB20   : NAME=RSGB20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB20 /dev/null
#    RSGS43   : NAME=RSGS43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
#    RSFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL40 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
# ******* FICHIERS PROVENANT DE PPMU204                                        
       m_FileAssign -d SHR -g ${G_A1} FGS36E ${DATA}/PTEM/GRM23PAA.BRM230AP
       m_FileAssign -d SHR -g ${G_A2} FGB15E ${DATA}/PTEM/GRM23PAD.BRM230BP
# ******* FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FGS36S ${DATA}/PTEM/GRM23PAG.BRM230CP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FGB15S ${DATA}/PTEM/GRM23PAG.BRM230DP
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 FRM230 ${DATA}/PTEM/GRM23PAG.BRM230EP
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 FRM231 ${DATA}/PTEM/GRM23PAG.BRM230FP
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 FRM232 ${DATA}/PTEM/GRM23PAG.BRM230GP
# ***********************************                                          
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          

       m_ProgramExec -b BRM230 
       JUMP_LABEL=GRM23PAJ
       ;;
(GRM23PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************************                                          
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GRM23PAG.BRM230CP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAJ.BRM230HP
# **********************************                                           
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_17 1 CH 17
 /FIELDS FLD_PD_18_3 18 PD 3
 /KEYS
   FLD_BI_1_17 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_18_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PAM
       ;;
(GRM23PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **********************************                                           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GRM23PAG.BRM230DP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAM.BRM230IP
# ********************************                                             
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_17 1 CH 17
 /FIELDS FLD_PD_18_3 18 PD 3
 /KEYS
   FLD_BI_1_17 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_18_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PAQ
       ;;
(GRM23PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GRM23PAG.BRM230EP
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAQ.BRM230JP
# **********************************                                           
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_20 1 CH 20
 /KEYS
   FLD_BI_1_20 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PAT
       ;;
(GRM23PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **********************************                                           
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GRM23PAG.BRM230FP
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAT.BRM230KP
# *********************************                                            
#                                                                              
# ***********************************                                          
# *   STEP GRM23PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_16 1 CH 16
 /KEYS
   FLD_BI_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PAX
       ;;
(GRM23PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************                                            
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GRM23PAG.BRM230GP
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PAX.BRM230LP
# ********************************************************                     
#  BRM231 : PGM EXTRACTION REAPRO MAG PAR GROUPE DE GROUPE                     
# ********************************************************                     
#  REPRISE: OUI                                                                
# ********************************************************                     
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_16 1 CH 16
 /KEYS
   FLD_BI_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PBA
       ;;
(GRM23PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM75   : NAME=RSRM75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM75 /dev/null
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA66 /dev/null
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGB20   : NAME=RSGB20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB20 /dev/null
#    RSGS43   : NAME=RSGS43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
#    RSFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL40 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL60 /dev/null
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
# ******* TABLE EN MAJ                                                         
#    RSRM80   : NAME=RSRM80,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM80 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS PROVENANT DU PGM BRM230                                     
       m_FileAssign -d SHR -g ${G_A8} FGS36S ${DATA}/PTEM/GRM23PAJ.BRM230HP
       m_FileAssign -d SHR -g ${G_A9} FGB15S ${DATA}/PTEM/GRM23PAM.BRM230IP
       m_FileAssign -d SHR -g ${G_A10} FRM230 ${DATA}/PTEM/GRM23PAQ.BRM230JP
       m_FileAssign -d SHR -g ${G_A11} FRM231 ${DATA}/PTEM/GRM23PAT.BRM230KP
       m_FileAssign -d SHR -g ${G_A12} FRM232 ${DATA}/PTEM/GRM23PAX.BRM230LP
# ******* VENTES PREVISIONNELLES                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 JRM231 ${DATA}/PXX0/F07.BRM231AP
# ********************************                                             
#  TRI DU FICHIER BRM231AP                                                     
# ********************************                                             
#  REPRISE: OUI                                                                
# ********************************                                             
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          

       m_ProgramExec -b BRM231 
       JUMP_LABEL=GRM23PBD
       ;;
(GRM23PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.BRM231AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PBD.BRM631GP
# ********************************************************                     
#  BRM631 : PGM EXTRACTION REAPRO MAG PAR GROUPE DE GROUPE                     
# ********************************************************                     
#  REPRISE: OUI                                                                
# ********************************************************                     
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_32_3 32 PD 03
 /FIELDS FLD_BI_57_1 57 CH 1
 /FIELDS FLD_BI_153_20 153 CH 20
 /FIELDS FLD_BI_35_20 35 CH 20
 /KEYS
   FLD_PD_32_3 ASCENDING,
   FLD_BI_35_20 ASCENDING,
   FLD_BI_57_1 ASCENDING,
   FLD_BI_153_20 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PBG
       ;;
(GRM23PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE                                                     
#    RSRM06   : NAME=RSRM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM06 /dev/null
#    RSRM81   : NAME=RSRM81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM81 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* VENTES PREVISIONNELLES                                               
       m_FileAssign -d SHR -g ${G_A14} JRM231 ${DATA}/PTEM/GRM23PBD.BRM631GP
# ******* VENTES PREVISIONNELLES PAR GROUPE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 JRM631 ${DATA}/PTEM/GRM23PBG.BRM631AP
# **********************************************************                   
#  TRI DU FICHIER FEXTRAC (BRM631BP)                                           
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          

       m_ProgramExec -b BRM631 
       JUMP_LABEL=GRM23PBJ
       ;;
(GRM23PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GRM23PBG.BRM631AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PBJ.BRM631BP
# ***********************************************************                  
#  PGM : BEG050 CREATION D'UN FICHIER FCUMULS BRM631CP                         
# ***********************************************************                  
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_PD_40_4 40 PD 04
 /FIELDS FLD_BI_20_20 20 CH 20
 /FIELDS FLD_BI_44_7 44 CH 7
 /FIELDS FLD_BI_202_1 202 CH 1
 /FIELDS FLD_BI_51_1 51 CH 1
 /FIELDS FLD_BI_7_5 07 CH 5
 /FIELDS FLD_BI_12_5 12 CH 05
 /FIELDS FLD_PD_52_2 52 PD 02
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_BI_20_20 ASCENDING,
   FLD_PD_40_4 DESCENDING,
   FLD_BI_202_1 ASCENDING,
   FLD_BI_44_7 ASCENDING,
   FLD_BI_51_1 ASCENDING,
   FLD_PD_52_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PBM
       ;;
(GRM23PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/GRM23PBJ.BRM631BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRM23PBM.BRM631CP
# **********************************************************                   
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRM23PBQ
       ;;
(GRM23PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GRM23PBM.BRM631CP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRM23PBQ.BRM631DP
# ***********************************************************                  
#  PGM : BEG060 CREATION DE L'ETAT : JRM631                                    
# ***********************************************************                  
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRM23PBT
       ;;
(GRM23PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *****   FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A18} FEXTRAC ${DATA}/PTEM/GRM23PBJ.BRM631BP
# ******* FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A19} FCUMULS ${DATA}/PTEM/GRM23PBQ.BRM631DP
# ******* PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* FICHIER D'IMPRESSION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ -g +1 FEDITION ${DATA}/PXX0/F07.JRM631AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRM23PBU
       ;;
(GRM23PBU)
       m_CondExec 04,GE,GRM23PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT DANS EOS                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM23PBX PGM=IEBGENER   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRM23PBX
       ;;
(GRM23PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A20} SYSUT1 ${DATA}/PXX0/F07.JRM631AP
       m_OutputAssign -c 9 -w JRM631 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRM23PBY
       ;;
(GRM23PBY)
       m_CondExec 00,EQ,GRM23PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# ADC      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=JRM631AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGRM23P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRM23PCA PGM=FTP        ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRM23PCA
       ;;
(GRM23PCA)
       m_CondExec ${EXADC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM23PCA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GRM23PCD PGM=EZACFSM1   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRM23PCD
       ;;
(GRM23PCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM23PCD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRM23PZA
       ;;
(GRM23PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRM23PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
