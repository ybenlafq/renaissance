#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU200D.ksh                       --- VERSION DU 08/10/2016 17:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDMU200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/21 AT 10.17.15 BY BURTECA                      
#    STANDARDS: P  JOBSET: MU200D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FOCNTION DE LA CARTE FFONC FIC STOCK ET STAT           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU200DA
       ;;
(MU200DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MU200DAA
       ;;
(MU200DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  PARAMETRAGE PGM                                                      
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MU200DAA
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/MU200DAA
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 34 STOCK AS400                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 FTS077 ${DATA}/PXX0/F91.BNM177AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=MU200DAB
       ;;
(MU200DAB)
       m_CondExec 04,GE,MU200DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DE REMONTEE STOCK 36                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MU200DAD
       ;;
(MU200DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F91.BNM177AD
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200DAD.BNM177BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200DAE
       ;;
(MU200DAE)
       m_CondExec 00,EQ,MU200DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DU LOAD DE LA RTGS36 (FICHIER DE LA VEILLE)                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MU200DAG
       ;;
(MU200DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DE LOAD DE LA VEILLE                                         
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BMU200CD
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU200DAG.BMU200BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_6 8 CH 6
 /KEYS
   FLD_CH_8_6 ASCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU200DAH
       ;;
(MU200DAH)
       m_CondExec 00,EQ,MU200DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREE LE FIC POUR CHARGEMENT DE LA TABLE RTGS36 (STOCK ET TRANSIT MA         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU200DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MU200DAJ
       ;;
(MU200DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  TABLE RTGA10                                                         
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  FICHIER DE LA VEILLE TRIE                                            
       m_FileAssign -d SHR -g ${G_A2} RTGS36A ${DATA}/PTEM/MU200DAG.BMU200BD
# ******  FICHIER DES STOCK ET TRANSIT DES MAGS                                
       m_FileAssign -d SHR -g ${G_A3} FMU200J ${DATA}/PTEM/MU200DAD.BNM177BD
# ******  FICHIER DU JOUR POUR CHARGEMENT DE LA RTGS36                         
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 RTGS36J ${DATA}/PXX0/F91.BMU200CD
# ******  FICHIER DU JOUR POUR AIDE A LA VENTE                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 FMU200V ${DATA}/PXX0/F91.BGS991MD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU200 
       JUMP_LABEL=MU200DAK
       ;;
(MU200DAK)
       m_CondExec 04,GE,MU200DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MU200DZA
       ;;
(MU200DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU200DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
