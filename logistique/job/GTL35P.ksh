#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL35P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGTL35 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/25 AT 11.02.08 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL35P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTL035                                                         *            
# ********************************************************************         
#   VENTES LIVREES PAR EQUIPE POUBELLE OU                                      
#   VENTES NON TOPEES LIVREES                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL35PA
       ;;
(GTL35PA)
#
#GTL35PAQ
#GTL35PAQ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GTL35PAQ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL35PAA
       ;;
(GTL35PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ TABLES DE TOURNEES            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEES (LPLGE CARSP)                                            
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE DES VENTES/ADRESSES                                                   
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES LIGNES DE VENTES ARTICLES                                         
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#  TOURNEES/DETAILS                                                            
#    RTTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL02 /dev/null
#  FICHIER VENTES LIVREES PAR EQUIPE POUBELLE                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FTL035 ${DATA}/PTEM/GTL35PAA.BTL035AP
#  FICHIER VENTES NON TOPEES LIVREES                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 FTL036 ${DATA}/PTEM/GTL35PAA.BTL036AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL035 
       JUMP_LABEL=GTL35PAB
       ;;
(GTL35PAB)
       m_CondExec 04,GE,GTL35PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER VENTES LIVREES  PAR EQUIPE POUBELLE                    
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL35PAD
       ;;
(GTL35PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GTL35PAA.BTL035AP
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL35PAD.BTL035BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_10 12 CH 10
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_10 ASCENDING
 /* Record Type = F  Record Length = 113 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL35PAE
       ;;
(GTL35PAE)
       m_CondExec 00,EQ,GTL35PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BTL040                                                                
# ********************************************************************         
#  EDITION DES VENTES AVEC EQUIPE POUBELLE (ETAT JTL401)                       
#  REPRISE: NON (UTILISER LE BACKOUT)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35PAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL35PAG
       ;;
(GTL35PAG)
       m_CondExec ${EXAAK},NE,YES 
# AAK     IMSSTEP PGM=BTL040,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#                LOG=(YES,GTL35RC1)                                            
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  CARTE DATE                                                                  
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER EN ENTREE                                                           
       m_FileAssign -d SHR -g ${G_A2} FTL035 ${DATA}/PTEM/GTL35PAD.BTL035BP
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL35RC1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#  DB IMPRESSION GENERALISEE EN UPDATE                                         
# DIGVP0   FILE  NAME=DI0000DP,MODE=U,REST=(YES,GTL35PR1)                      
# DIGVIP   FILE  NAME=DI0000IP,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX0.F07.DI0000DP
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX0.F07.DI0000IP
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL35PAG
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGP 
# ********************************************************************         
#    CREE FIC POUR LOAD DE RTTL08                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL35PAJ
       ;;
(GTL35PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GTL35PAA.BTL036AP
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F07.RELOAD.TL08RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_30 1 CH 30
 /KEYS
   FLD_CH_1_30 ASCENDING
 /* Record Type = F  Record Length = 37 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL35PAK
       ;;
(GTL35PAK)
       m_CondExec 00,EQ,GTL35PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTTL08                                                    
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL35PAM
       ;;
(GTL35PAM)
       m_CondExec ${EXAAU},NE,YES 
#  FICHIERS DE TRAVAIL  AU MOINS = AU SYSREC ,ATTENTION SYSWK = 3350           
#    RSTL08   : NAME=RSTL08,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL08 /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PTL0/F07.RELOAD.TL08RP
#  SYSUT1 AU MOINS = A LA TAILLE DE L INDEX ATTENTION SYSWK=3350               
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SYSMAP ERREUR CLE EN DOUBLE LAISSER MINIMUM CYL (1,1)                       
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL35PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GTL35P_GTL35PAM_RTTL08.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GTL35PAN
       ;;
(GTL35PAN)
       m_CondExec 04,GE,GTL35PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLESPACE RSTL08  DE LA PPDTL00                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL35PZA
       ;;
(GTL35PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL35PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
