#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA004P.ksh                       --- VERSION DU 08/10/2016 13:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVA004 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/07/09 AT 15.42.54 BY PREPA2                       
#    STANDARDS: P  JOBSET: VA004P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  DELETE DES FICHIERS DU DERNIER PASSAGE                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA004PA
       ;;
(VA004PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2008/07/09 AT 15.42.54 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: VA004P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'BUDGET-VALO'                           
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA004PAA
       ;;
(VA004PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA004PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VA004PAB
       ;;
(VA004PAB)
       m_CondExec 16,NE,VA004PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPACKAGE DES ZONES PACKEES DU FICHIER PXX0.F07.BVA600AP                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAD PGM=SYSTPC1    ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAD
       ;;
(VA004PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ******* FICHIER ISSU DE VA003P                                               
       m_FileAssign -d SHR -g +0 ENTREE ${DATA}/PXX0/F07.BVA600AP
# ******* FICHIER POUR LE CONTROLE DE GESTION                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 539 -g +1 SORTIE ${DATA}/PTEM/VA004PAD.BVA600UP
#  SELECT US51/4/7                                                             
#         IIII I +-----> LONGUEUR EN SORTIE                                    
#         IIII +-------> LONGUEUR EN ENTREE                                    
#         II++---------> POSITION DEBUT (FIC D'ENTREE)                         
#         I+-----------> SIGNE                                                 
#         +------------> UNPACK                                                
#   P ---> PACK                                                                
#                                                                              
       m_ProgramExec VA004PAD
# ********************************************************************         
#  MERGE DU FICHIER DEPACKE BVA600AP POUR REDUIRE LA LONGUEUR ET               
#  CREATION DU FICHIER PXX0.F07.IVA600 POUR LE CONTROLE DE GESTION             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAG PGM=MERGE      ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAG
       ;;
(VA004PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/VA004PAD.BVA600UP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 SORTOUT ${DATA}/PAS0.F07.IVA600
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA004PAH
       ;;
(VA004PAH)
       m_CondExec 00,EQ,VA004PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPACKAGE DES ZONES PACKEES DU FICHIER PXX0.F07.BVA610AP                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAJ PGM=SYSTPC1    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAJ
       ;;
(VA004PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ******* FICHIER ISSU DE VA003P                                               
       m_FileAssign -d SHR -g +0 ENTREE ${DATA}/PXX0/F07.BVA610AP
# ******* FICHIER POUR LE CONTROLE DE GESTION                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 539 -g +1 SORTIE ${DATA}/PTEM/VA004PAJ.BVA610UP
#  SELECT US51/4/7                                                             
#         IIII I +-----> LONGUEUR EN SORTIE                                    
#         IIII +-------> LONGUEUR EN ENTREE                                    
#         II++---------> POSITION DEBUT (FIC D'ENTREE)                         
#         I+-----------> SIGNE                                                 
#         +------------> UNPACK                                                
#   P ---> PACK                                                                
#                                                                              
       m_ProgramExec VA004PAJ
# ********************************************************************         
#  MERGE DU FICHIER DEPACKE BVA610AP POUR REDUIRE LA LONGUEUR ET               
#  CREATION DU FICHIER PXX0.F07.IVA610 POUR LE CONTROLE DE GESTION             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAM PGM=MERGE      ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAM
       ;;
(VA004PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VA004PAJ.BVA610UP
       m_FileAssign -d NEW,CATLG,DELETE -r 130 SORTOUT ${DATA}/PAS0.F07.IVA610
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA004PAN
       ;;
(VA004PAN)
       m_CondExec 00,EQ,VA004PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPACKAGE DES ZONES PACKEES DU FICHIER PXX0.F07.BVA630AP                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAQ PGM=SYSTPC1    ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAQ
       ;;
(VA004PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ******* FICHIER ISSU DE VA003P                                               
       m_FileAssign -d SHR -g +0 ENTREE ${DATA}/PXX0/F07.BVA630AP
# ******* FICHIER POUR LE CONTROLE DE GESTION                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 539 -g +1 SORTIE ${DATA}/PTEM/VA004PAQ.BVA630UP
#  SELECT US51/4/7                                                             
#         IIII I +-----> LONGUEUR EN SORTIE                                    
#         IIII +-------> LONGUEUR EN ENTREE                                    
#         II++---------> POSITION DEBUT (FIC D'ENTREE)                         
#         I+-----------> SIGNE                                                 
#         +------------> UNPACK                                                
#   P ---> PACK                                                                
#                                                                              
       m_ProgramExec VA004PAQ
# ********************************************************************         
#  MERGE DU FICHIER DEPACKE BVA630AP POUR REDUIRE LA LONGUEUR ET               
#  CREATION DU FICHIER PXX0.F07.IVA630 POUR LE CONTROLE DE GESTION             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAT PGM=MERGE      ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAT
       ;;
(VA004PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/VA004PAQ.BVA630UP
       m_FileAssign -d NEW,CATLG,DELETE -r 114 SORTOUT ${DATA}/PAS0.F07.IVA630
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 114 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA004PAU
       ;;
(VA004PAU)
       m_CondExec 00,EQ,VA004PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPACKAGE DES ZONES PACKEES DU FICHIER PXX0.F07.BVA640AP                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PAX PGM=SYSTPC1    ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VA004PAX
       ;;
(VA004PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ******* FICHIER ISSU DE VA003P                                               
       m_FileAssign -d SHR -g +0 ENTREE ${DATA}/PXX0/F07.BVA640AP
# ****** FI CHIER POUR LE CONTROLE DE GESTION                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 569 -g +1 SORTIE ${DATA}/PTEM/VA004PAX.BVA640UP
#  SELECT US51/4/7                                                             
#         IIII I +-----> LONGUEUR EN SORTIE                                    
#         IIII +-------> LONGUEUR EN ENTREE                                    
#         II++---------> POSITION DEBUT (FIC D'ENTREE)                         
#         I+-----------> SIGNE                                                 
#         +------------> UNPACK                                                
#   P ---> PACK                                                                
#                                                                              
       m_ProgramExec VA004PAX
# ********************************************************************         
#  MERGE DU FICHIER DEPACKE BVA640AP POUR REDUIRE LA LONGUEUR ET               
#  CREATION DU FICHIER PXX0.F07.IVA640 POUR LE CONTROLE DE GESTION             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA004PBA PGM=MERGE      ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VA004PBA
       ;;
(VA004PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/VA004PAX.BVA640UP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 SORTOUT ${DATA}/PAS0.F07.IVA640
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 194 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VA004PBB
       ;;
(VA004PBB)
       m_CondExec 00,EQ,VA004PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA004PZA
       ;;
(VA004PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA004PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
