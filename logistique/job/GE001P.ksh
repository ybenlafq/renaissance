#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GE001P.ksh                       --- VERSION DU 08/10/2016 13:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGE001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/02 AT 10.03.59 BY BURTECA                      
#    STANDARDS: P  JOBSET: GE001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  B G E 0 0 1                                                                 
# ********************************************************************         
#   EXTRACTION DES INFORMATIONS CONCERNANT LES CAPACITES DE STOCKAGE           
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GE001PA
       ;;
(GE001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/09/02 AT 10.03.59 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GE001P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ED GEST� EMPLACEMENT'                  
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GE001PAA
       ;;
(GE001PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGE20   : NAME=RSGE20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE20 /dev/null
#    RTGE25   : NAME=RSGE25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE25 /dev/null
#    RTGE30   : NAME=RSGE30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE30 /dev/null
#    RTGE36   : NAME=RSGE36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE36 /dev/null
#    RTGE40   : NAME=RSGE40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE40 /dev/null
#    RTGE45   : NAME=RSGE45,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGE45 /dev/null
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FSORTIE ${DATA}/PTEM/GE001PAA.GE0001AP
# ******  APPEL DU PGM BGE001                                                  
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGE001 
       JUMP_LABEL=GE001PAB
       ;;
(GE001PAB)
       m_CondExec 04,GE,GE001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER D EXTRACTION DES EMPLACEMNTS                                
#   1,6 CODE ENTREPOT; 7,1 TYPE DE STOCKAGE; 8,1 TYPE ENREGISTREMENT           
#   9,4 ZONE DE RATIO                                                          
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE001PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GE001PAD
       ;;
(GE001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EXTRACTION DES EMPLACEMENTS                                  
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GE001PAA.GE0001AP
# ******  FICHIER EXTRACTION DES EMPLACEMENTS TRIE                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GE001PAD.GE0001BP
# ******  TRI                                                                  
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "907090"
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_8_1 8 CH 1
 /FIELDS FLD_CH_9_4 9 CH 4
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_7 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_8_1 ASCENDING,
   FLD_CH_9_4 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GE001PAE
       ;;
(GE001PAE)
       m_CondExec 00,EQ,GE001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B G E 0 0 2                                                                 
#  EDITION DU TABLEAU DE BORD CAPACITE DE STOCKAGE                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GE001PAG PGM=BGE002     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GE001PAG
       ;;
(GE001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER D EXTRACTION TRIE                                            
       m_FileAssign -d SHR -g ${G_A2} FENTREE ${DATA}/PTEM/GE001PAD.GE0001BP
# ******  FICHIER EDITION                                                      
       m_OutputAssign -c 9 -w BGE002 IMPR
       m_ProgramExec BGE002 
# ******  DEPENDANCE POUR PLAN                                                 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GE001PZA
       ;;
(GE001PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GE001PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
