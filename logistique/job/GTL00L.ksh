#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL00L.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGTL00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 10.55.14 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL00L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#    IMPERATIF :  CETTE CHAINE DOIT ETRE EXECUTEE IMPERATIVEMENT CAR :         
#    -=-=-=-=-    AFFECTATION D'UN PROFIL AUX TOURNEES DE LIVRAISON            
#                 ET EXTRACTION DES VENTES A LIVRER.                           
#                                                                              
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
#   BTL100 : AFFECTE UN PROFIL DE TOURNEES AUX LIVRAISONS DU JOUR              
#  REPRISE : OUI LE PROG GERE LES REPRISES                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL00LA
       ;;
(GTL00LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL00LAA
       ;;
(GTL00LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    POUR AVOIR LES TABLES             *                                       
#    DE VENTES A JOUR                 *                                        
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  DETAILS DES VENTES                                                   
#    RSGV11L  : NAME=RSGV11L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11L /dev/null
# ******  ANOMALIES                                                            
#    RSAN00L  : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00L /dev/null
# ******  CALANDRIER DES TOURNEES                                              
#    RSTL10L  : NAME=RSTL10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL10L /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *******                                                                      
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL100 
       JUMP_LABEL=GTL00LAB
       ;;
(GTL00LAB)
       m_CondExec 04,GE,GTL00LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREE UNE GENERATION  A VIDE DE FACON A LES PRENDRE EN DISP=MOD LORS         
#  DES REPRISES  DE BTL000                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAD
       ;;
(GTL00LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT1 ${DATA}/PTL961/F61.BTL001CL
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 OUT2 ${DATA}/PTL961/F61.BTL002DL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00LAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GTL00LAE
       ;;
(GTL00LAE)
       m_CondExec 16,NE,GTL00LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BTL000 : EXTRACTION DES VENTES A LIVRER                                    
#   FIC FTL000 : FIC DES VENTES POUR LOGICIEL DES LIVRAISONS                   
#   FIC FTL001 : FIC DES ANOMALIES,CUMUL DE PROFIL(1 LOGNE PAR PROFIL)         
#   PGM = BTL000                                                               
#  REPRISE : OUI LE PROG GERE LES REPRISES                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAG
       ;;
(GTL00LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  LIEUX                                                                
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ******  ADRESSES                                                             
#    RSGV02L  : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02L /dev/null
# ******  RESERVATION STOCK ENTREPOT                                           
#    RSGV21L  : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21L /dev/null
# ******  DETAILS DES VENTES                                                   
#    RSGV11L  : NAME=RSGV11L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11L /dev/null
# ******  LIVRAISON CLIENT A DESTOCKER                                         
#    RSGD05L  : NAME=RSGD05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGD05L /dev/null
# ******  PROFIL DE TOURNEES DEMANDEES                                         
#    RSTL05L  : NAME=RSTL05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05L /dev/null
# ******  CALANDRIER DES TOURNEES                                              
#    RSTL10L  : NAME=RSTL10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL10L /dev/null
       m_FileAssign -d SHR FTL000 /dev/null
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A1} FTL001 ${DATA}/PTL961/F61.BTL001CL
       m_FileAssign -d MOD,KEEP,KEEP -g ${G_A2} FTL002 ${DATA}/PTL961/F61.BTL002DL
# ******  FICHIER EN DUMMY                                                     
       m_FileAssign -d SHR FTL003 /dev/null
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL000 
       JUMP_LABEL=GTL00LAH
       ;;
(GTL00LAH)
       m_CondExec 04,GE,GTL00LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL001CL POUR ELIMINER LES DOUBLONS                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAJ
       ;;
(GTL00LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTL961/F61.BTL001CL
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL961/F61.BTL001BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00LAK
       ;;
(GTL00LAK)
       m_CondExec 00,EQ,GTL00LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BTL002DL POUR ELIMINER LES DOUBLONS                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAM
       ;;
(GTL00LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTL961/F61.BTL002DL
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL961/F61.BTL002AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_130 1 CH 130
 /KEYS
   FLD_CH_1_130 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00LAN
       ;;
(GTL00LAN)
       m_CondExec 00,EQ,GTL00LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LA CHAINE : GTL75L                                
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAQ
       ;;
(GTL00LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTL961/F61.BTL001BL
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTL961/F61.BTL002AL
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTL961/F61.BTL001AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00LAR
       ;;
(GTL00LAR)
       m_CondExec 00,EQ,GTL00LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   FUSION 9,3,A,1,8,A,12,23,A                                                 
#   SOC,JOUR,PLAN,TYPE D ENR,ZONE TRI                                          
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAT
       ;;
(GTL00LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTL961/F61.BTL001AL
       m_FileAssign -d NEW,CATLG,DELETE -r 130 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL00LAT.BTL030AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_23 12 CH 23
 /FIELDS FLD_CH_1_8 1 CH 8
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_23 ASCENDING
 /* Record Type = F  Record Length = 130 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL00LAU
       ;;
(GTL00LAU)
       m_CondExec 00,EQ,GTL00LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    ECRITURE   DANS LA BASE IMPRESSION GENERALISEE                            
#    1 LISTE NOMBRE DE PROFILS TRAITES                                         
#    1 LISTE ANOMALIES                                                         
#    1 LISTE DES TRANSITAIRES (POUR L ETRANGER)                                
#    REPRISE: NON  BACKOUT DU STEP PAR LOGGING IMS                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LAX PGM=DFSRRC00   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LAX
       ;;
(GTL00LAX)
       m_CondExec ${EXABJ},NE,YES 
# BJ      IMSSTEP PGM=BTL030,LANG=CBL,TYPE=DLI,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL00LR1)                                             
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL00LR1
       m_OutputAssign -c "*" DDOTV02
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DL,MODE=U,REST=(YES,GTL00LR1)                       
# DIGVIP   FILE NAME=DI0000IL,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX961.F61.DI0000DL
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX961.F61.DI0000IL
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL00LAX
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FTL030 ${DATA}/PTEM/GTL00LAT.BTL030AL
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGL 
# ********************************************************************         
#  MISE A JOUR DES RETOURS DE LIVRAISON RECYCLEES                              
#  PGM = BTL085                                                                
#  REPRISE: OUI APRES ABEND                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL00LBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LBA
       ;;
(GTL00LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
# ******  RETOURS DES TOURNEES                                                 
#    RSTL04L  : NAME=RSTL04L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04L /dev/null
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL085 
       JUMP_LABEL=GTL00LBB
       ;;
(GTL00LBB)
       m_CondExec 04,GE,GTL00LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL00LZA
       ;;
(GTL00LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL00LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
