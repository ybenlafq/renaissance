#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BQHS1M.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMBQHS1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.14.51 BY BURTEC6                      
#    STANDARDS: P  JOBSET: BQHS1M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  EXTRACTION DU FICHIER QHS98901                                              
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BQHS1MA
       ;;
(BQHS1MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       RUN=${RUN}
       JUMP_LABEL=BQHS1MAA
       ;;
(BQHS1MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#    RSGA67   : NAME=RSGA67M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSHE10   : NAME=RSHE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#                                                                              
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ****** FICHIER EN  SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -t LSEQ -g +1 FQHS01 ${DATA}/PXX0/F89.BQHS01AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQHS01 
       JUMP_LABEL=BQHS1MAB
       ;;
(BQHS1MAB)
       m_CondExec 04,GE,BQHS1MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION DU FICHIER QHS98902                                              
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BQHS1MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BQHS1MAD
       ;;
(BQHS1MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#    RSGA67   : NAME=RSGA67M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSHE10   : NAME=RSHE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE10 /dev/null
#    RSHE11   : NAME=RSHE11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE11 /dev/null
#    RSHE20   : NAME=RSHE20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE20 /dev/null
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ****** FICHIER EN  SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 FQHS02 ${DATA}/PXX0/F89.BQHS02AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BQHS02 
       JUMP_LABEL=BQHS1MAE
       ;;
(BQHS1MAE)
       m_CondExec 04,GE,BQHS1MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS (GATEWAY) XFBPRO FICHIER BQHS01BM                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BQHS01AM,                                                           
#      FNAME=":BQHS01AM""(0),                                                  
#      NFNAME=QHS98901                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT VERS (GATEWAY) XFBPRO FICHIER BQHS02BM                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BQHS02AM,                                                           
#      FNAME=":BQHS02AM""(0),                                                  
#      NFNAME=QHS98902                                                         
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBQHS1M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BQHS1MAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BQHS1MAG
       ;;
(BQHS1MAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BQHS1MAG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BQHS1MAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BQHS1MAJ
       ;;
(BQHS1MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BQHS1MAJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBQHS1M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BQHS1MAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BQHS1MAM
       ;;
(BQHS1MAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BQHS1MAM.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BQHS1MAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BQHS1MAQ
       ;;
(BQHS1MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BQHS1MAQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
