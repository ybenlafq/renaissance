#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL11O.ksh                       --- VERSION DU 20/10/2016 12:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGTL11 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 09.10.53 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GTL11O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BTL110                                                                
#  EXTRACTION PERMETTANT D'ALIMENTER LA TABLE RTTL09                           
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
#           NON SI FIN NORMALE FAIRE UN RECOVER A LA F.I.C DE RTTL09           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL11OA
       ;;
(GTL11OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL11OAA
       ;;
(GTL11OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ************************************** TABLE DES QUOTAS DE LIVRAISON         
#    RSGQ01O  : NAME=RSGQ01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01O /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40O  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40O /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02O  : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02O /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23O  : NAME=RSGV23O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23O /dev/null
# ************************************** TABLE DES TOUNEES (GENERALITE         
#    RSTL01O  : NAME=RSTL01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL01O /dev/null
# ************************************** TABLE DES TOURNEES (DETAILS)          
#    RSTL02O  : NAME=RSTL02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02O /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04O  : NAME=RSTL04O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04O /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09O  : NAME=RSTL09O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09O /dev/null
# ************************************** TABLE HISTO VENTES                    
#    RSHV15O  : NAME=RSHV15O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15O /dev/null
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DODEP
# ************************************** PARAMETRE  H:HEBDO  M:MENSUEL         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11OAA
# ************************************** FTL110 ISSU DU DERNIER TRAIT          
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** FIC D'EXTRACT FTL110                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL110N ${DATA}/PXX0/F16.FTL110AO
# ************************************** FIC D'EXTRACT FTL111                  
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111 ${DATA}/PXX0/F16.BTL111AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL11OAB
       ;;
(GTL11OAB)
       m_CondExec 04,GE,GTL11OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MERGE : CUMUL DU FICHIER BTL111AO CREE PAR LE PROG BTL110 AVEC              
#          L'ANCIEN BTL111AY DU DERNIER TRAITEMENT                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAD
       ;;
(GTL11OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F16.BTL111AO
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F16.BTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A3} SORTOUT ${DATA}/PXX0/F16.BTL111AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OAE
       ;;
(GTL11OAE)
       m_CondExec 00,EQ,GTL11OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL111                                                                
#  INITIALISATION DES FICHIERS FTL111A  ET FTL111B                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAG
       ;;
(GTL11OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISE                      
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ************************************** FIC D'EXTRACT ISSU DU BLT110          
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** FIC D'EXTRACT ISSU DU TRI 1A          
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F16.BTL111AO
# ************************************** FICHIER FTL111A                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111A ${DATA}/PXX0/F16.FTL111AO
# ************************************** FICHIER  TL111B                       
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111B ${DATA}/PXX0/F16.FTL111BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL11OAH
       ;;
(GTL11OAH)
       m_CondExec 04,GE,GTL11OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2A : TRI DU FICHIER FTL111AO POUR CREATION DU MLT112AO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAJ
       ;;
(GTL11OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OAJ.MTL112AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OAK
       ;;
(GTL11OAK)
       m_CondExec 00,EQ,GTL11OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2B : TRI DU FICHIER FTL111AO POUR CREATION DU MTL112BO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAM
       ;;
(GTL11OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OAM.MTL112BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OAN
       ;;
(GTL11OAN)
       m_CondExec 00,EQ,GTL11OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 2C : TRI DU FICHIER FTL111BM POUR CREATION DU MTL112CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAQ
       ;;
(GTL11OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OAQ.MTL112CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_62_3 62 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OAR
       ;;
(GTL11OAR)
       m_CondExec 00,EQ,GTL11OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL112                                                                
#  CREATION DES ETATS ITL112A ET ITL112B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAT
       ;;
(GTL11OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** FICHIERS ISSU DU TRI 2A               
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL11OAJ.MTL112AO
# ************************************** FICHIERS ISSU DU TRI 2B               
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL11OAM.MTL112BO
# ************************************** FICHIERS ISSU DU TRI 2C               
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL11OAQ.MTL112CO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L                  
#  ITL112A  REPORT SYSOUT=(9,ITL112A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL112A ${DATA}/PXX0/F16.ITL112CO.MICROFIC
# ************************************** ETAT DE STAT PAR PIECES               
#  ITL112B  REPORT SYSOUT=(9,ITL112B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL112B ${DATA}/PXX0/F16.ITL112DO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL11OAU
       ;;
(GTL11OAU)
       m_CondExec 04,GE,GTL11OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3A : TRI DU FICHIER FTL111AO POUR CREATION DU MTL113AO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OAX
       ;;
(GTL11OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OAX.MTL113AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OAY
       ;;
(GTL11OAY)
       m_CondExec 00,EQ,GTL11OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3B : TRI DU FICHIER FTL111AO POUR CREATION DU MTL113BO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBA
       ;;
(GTL11OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OBA.MTL113BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OBB
       ;;
(GTL11OBB)
       m_CondExec 00,EQ,GTL11OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3C : TRI DU FICHIER FTL111BM POUR CREATION DU MTL113CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBD
       ;;
(GTL11OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OBD.MTL113CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OBE
       ;;
(GTL11OBE)
       m_CondExec 00,EQ,GTL11OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 3D : TRI DU FICHIER FTL111BO POUR CREATION DU MTL113CO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBG
       ;;
(GTL11OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OBG.MTL113DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OBH
       ;;
(GTL11OBH)
       m_CondExec 00,EQ,GTL11OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL113                                                                
#  CREATION DE L'ETAT ITL113                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBJ
       ;;
(GTL11OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ************************************** FICHIERS ISSU DU BLT110               
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** FICHIERS ISSU DU TRI 3A               
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL11OAX.MTL113AO
# ************************************** FICHIERS ISSU DU TRI 3B               
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL11OBA.MTL113BO
# ************************************** FICHIERS ISSU DU TRI 3C               
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL11OBD.MTL113CO
# ************************************** FICHIERS ISSU DU TRI 3D               
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL11OBG.MTL113DO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR         
#  ITL113   REPORT SYSOUT=(9,ITL113),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL113 ${DATA}/PXX0/F16.ITL113CO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL11OBK
       ;;
(GTL11OBK)
       m_CondExec 04,GE,GTL11OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4A : TRI DU FICHIER FTL111BO POUR CREATION DU FTL114AO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBM
       ;;
(GTL11OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OBM.FTL114AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OBN
       ;;
(GTL11OBN)
       m_CondExec 00,EQ,GTL11OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4B : TRI DU FICHIER FTL111BO POUR CREATION DU FTL114BO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBQ
       ;;
(GTL11OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OBQ.FTL114BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_97_2 97 CH 2
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OBR
       ;;
(GTL11OBR)
       m_CondExec 00,EQ,GTL11OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 4C : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114CM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBT
       ;;
(GTL11OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OBT.FTL114CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OBU
       ;;
(GTL11OBU)
       m_CondExec 00,EQ,GTL11OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL114                                                                
#  CREATION DES ETATS ITL114A ET ITL114B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OBX
       ;;
(GTL11OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00M  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES VENTES/ADRESSE              
#    RSGV02M  : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02M /dev/null
# ************************************** TABLE DES ENTETES DE VENTES           
#    RSGV10M  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
# ************************************** TABLE DES VENTES                      
#    RSGV11M  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL         
#    RSGV23M  : NAME=RSGV23O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23M /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)          
#    RSTL04M  : NAME=RSTL04O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A               
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL11OBM.FTL114AO
# ************************************** FICHIERS ISSU DU TRI 4B               
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL11OBQ.FTL114BO
# ************************************** FICHIERS ISSU DU TRI 4C               
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL11OBT.FTL114CO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L             
#  ITL114A  REPORT SYSOUT=(9,ITL114A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL114A ${DATA}/PXX0/F16.ITL114CO.MICROFIC
# ************************************** STAT RETOURS MAGS PAR PIECES          
#  ITL114B  REPORT SYSOUT=(9,ITL114B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL114B ${DATA}/PXX0/F16.ITL114DO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL11OBY
       ;;
(GTL11OBY)
       m_CondExec 04,GE,GTL11OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 5A : TRI DU FICHIER FTL111BM POUR CREATION DU FTL115AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCA
       ;;
(GTL11OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11OCA.FTL115AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_99_2 99 CH 2
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_106_2 106 CH 2
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_106_2 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_99_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OCB
       ;;
(GTL11OCB)
       m_CondExec 00,EQ,GTL11OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL115                                                                
#  CREATION DES ETATS ITL115A ET ITL115B                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCD
       ;;
(GTL11OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 5A               
       m_FileAssign -d SHR -g ${G_A29} FTL115 ${DATA}/PTEM/GTL11OCA.FTL115AO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** SYNTHESE RETOUR MAG PAR B.L           
#  ITL115A  REPORT SYSOUT=(9,ITL115A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL115A ${DATA}/PXX0/F16.ITL115CO.MICROFIC
# ************************************** SYNTHESE RETOUR MAG PAR PIECE         
#  ITL115B  REPORT SYSOUT=(9,ITL115B),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL115B ${DATA}/PXX0/F16.ITL115DO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL115 
       JUMP_LABEL=GTL11OCE
       ;;
(GTL11OCE)
       m_CondExec 04,GE,GTL11OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL116                                                                
#  CREATION DE L'ETAT ITL116                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCG
       ;;
(GTL11OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIVREURS                    
#    RSTL04M  : NAME=RSTL03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)           
#    RSTL09M  : NAME=RSTL09O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTL09M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU BTL110               
       m_FileAssign -d SHR -g ${G_A30} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE             
#  ITL116   REPORT SYSOUT=(9,ITL116),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 ITL116 ${DATA}/PXX0/F16.ITL116BO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL11OCH
       ;;
(GTL11OCH)
       m_CondExec 04,GE,GTL11OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL117                                                                
#  CREATION DU FICHIER FTL117AY                                                
# ********************************************************************         
#  REPRISE: OUI SI ABEND                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCJ
       ;;
(GTL11OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES                    
#    RSGA00M  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV         
#    RSGA13M  : NAME=RSGA13O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13M /dev/null
# ************************************** TABLE DES FAMILLES                    
#    RSGA14M  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES           
#    RSGA30M  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30M /dev/null
# ************************************** TABLE DES MVTS DE STOCK               
#    RSGS40M  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT                       
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DODEP
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11OCJ
# ************************************** FIC D'EXTRACT FTL117AY                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117 ${DATA}/PTEM/GTL10OCG.FTL117AO
# *****   FICHIER A DESTINATION DE COPERNIC                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117B ${DATA}/PTEM/GTL10OCG.FTL117BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       #Untranslated IKJEFT01 with unknown command
# DATA UNTRANSLATED: 
# SYSTSIN  DATA  *,CLASS=FIX2                                              
#  DSN SYSTEM(RDAR)                                                        
#  RUN PROGRAM(BTL117) PLAN(BTDARO)                                        
#  END                                                                     

       JUMP_LABEL=GTL11OCK
       ;;
(GTL11OCK)
       m_CondExec 04,GE,GTL11OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 6A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL118AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCM
       ;;
(GTL11OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OCJ.FTL118AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OCN
       ;;
(GTL11OCN)
       m_CondExec 00,EQ,GTL11OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL118                                                                
#  CREATION DE L'ETAT ITL118                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCQ
       ;;
(GTL11OCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A               
       m_FileAssign -d SHR -g ${G_A32} FTL118 ${DATA}/PTEM/GTL10OCJ.FTL118AO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11OCQ
# ************************************** MODE DE DELIV/MAG ET FAM              
#  ITL118   REPORT SYSOUT=(9,ITL118),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL118 ${DATA}/PXX0/F16.ITL118BO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL11OCR
       ;;
(GTL11OCR)
       m_CondExec 04,GE,GTL11OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 7A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL119AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCT
       ;;
(GTL11OCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OCQ.FTL119AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11OCU
       ;;
(GTL11OCU)
       m_CondExec 00,EQ,GTL11OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL119                                                                
#  CREATION DE L'ETAT ITL119                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OCX
       ;;
(GTL11OCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 7A               
       m_FileAssign -d SHR -g ${G_A34} FTL119 ${DATA}/PTEM/GTL10OCQ.FTL119AO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# ************************************** SYNTHESE DES MODES DE DELIV/F         
#  ITL119   REPORT SYSOUT=(9,ITL119),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL119 ${DATA}/PXX0/F16.ITL119BO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL119 
       JUMP_LABEL=GTL11OCY
       ;;
(GTL11OCY)
       m_CondExec 04,GE,GTL11OCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 8A : TRI DU FICHIER FTL117AO POUR CREATION DU FTL120AO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODA
       ;;
(GTL11ODA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11ODA.FTL120AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_38_2 38 CH 2
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_56_4 56 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_38_2 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11ODB
       ;;
(GTL11ODB)
       m_CondExec 00,EQ,GTL11ODA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 8B : TRI DU FICHIER FTL117AO POUR CREATION DU FTL120BO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODD
       ;;
(GTL11ODD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11ODD.FTL120BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11ODE
       ;;
(GTL11ODE)
       m_CondExec 00,EQ,GTL11ODD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL120                                                                
#  CREATION DE L'ETAT ITL120                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODG
       ;;
(GTL11ODG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 8A               
       m_FileAssign -d SHR -g ${G_A37} FTL120 ${DATA}/PTEM/GTL11ODA.FTL120AO
# ************************************** FICHIERS ISSU DU TRI 8B               
       m_FileAssign -d SHR -g ${G_A38} FTL120C ${DATA}/PTEM/GTL11ODD.FTL120BO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11ODG
# ************************************** MODE DE DELIV/FAM ET MAGS             
#  ITL120   REPORT SYSOUT=(9,ITL120),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL120 ${DATA}/PXX0/F16.ITL120BO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL120 
       JUMP_LABEL=GTL11ODH
       ;;
(GTL11ODH)
       m_CondExec 04,GE,GTL11ODG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 9A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL121AM                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODJ
       ;;
(GTL11ODJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11ODJ.FTL121AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_CH_38_2 38 CH 2
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_38_2 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11ODK
       ;;
(GTL11ODK)
       m_CondExec 00,EQ,GTL11ODJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 9B : TRI DU FICHIER FTL117AO POUR CREATION DU FTL121BO                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODM
       ;;
(GTL11ODM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL11ODM.FTL121BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL11ODN
       ;;
(GTL11ODN)
       m_CondExec 00,EQ,GTL11ODM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL121                                                                
#  CREATION DE L'ETAT ITL121                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODQ
       ;;
(GTL11ODQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE                     
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX                       
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES                   
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 9A               
       m_FileAssign -d SHR -g ${G_A41} FTL121 ${DATA}/PTEM/GTL11ODJ.FTL121AO
# ************************************** FICHIERS ISSU DU TRI 9B               
       m_FileAssign -d SHR -g ${G_A42} FTL121C ${DATA}/PTEM/GTL11ODM.FTL121BO
# ************************************** PARAMETRE DATE                        
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# ******  MENSUEL OU HEBDO                                                     
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL11ODQ
# ************************************** SYNTHESE MODE DE DELIV/MAGS           
#  ITL121   REPORT SYSOUT=(9,ITL121),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 ITL121 ${DATA}/PXX0/F16.ITL121BO.MICROFIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL121 
       JUMP_LABEL=GTL11ODR
       ;;
(GTL11ODR)
       m_CondExec 04,GE,GTL11ODQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL112A                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODT PGM=IEBGENER   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODT
       ;;
(GTL11ODT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A43} SYSUT1 ${DATA}/PXX0/F16.ITL112CO.MICROFIC
       m_OutputAssign -c 9 -w ITL112A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11ODU
       ;;
(GTL11ODU)
       m_CondExec 00,EQ,GTL11ODT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL112B                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11ODX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11ODX
       ;;
(GTL11ODX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A44} SYSUT1 ${DATA}/PXX0/F16.ITL112DO.MICROFIC
       m_OutputAssign -c 9 -w ITL112B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11ODY
       ;;
(GTL11ODY)
       m_CondExec 00,EQ,GTL11ODX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL113                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OEA
       ;;
(GTL11OEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A45} SYSUT1 ${DATA}/PXX0/F16.ITL113CO.MICROFIC
       m_OutputAssign -c 9 -w ITL113 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEB
       ;;
(GTL11OEB)
       m_CondExec 00,EQ,GTL11OEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL114A                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OED
       ;;
(GTL11OED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A46} SYSUT1 ${DATA}/PXX0/F16.ITL114CO.MICROFIC
       m_OutputAssign -c 9 -w ITL114A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEE
       ;;
(GTL11OEE)
       m_CondExec 00,EQ,GTL11OED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL114B                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OEG PGM=IEBGENER   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OEG
       ;;
(GTL11OEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A47} SYSUT1 ${DATA}/PXX0/F16.ITL114DO.MICROFIC
       m_OutputAssign -c 9 -w ITL114B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEH
       ;;
(GTL11OEH)
       m_CondExec 00,EQ,GTL11OEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL115A                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OEJ PGM=IEBGENER   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OEJ
       ;;
(GTL11OEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A48} SYSUT1 ${DATA}/PXX0/F16.ITL115CO.MICROFIC
       m_OutputAssign -c 9 -w ITL115A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEK
       ;;
(GTL11OEK)
       m_CondExec 00,EQ,GTL11OEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL115B                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OEM PGM=IEBGENER   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OEM
       ;;
(GTL11OEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A49} SYSUT1 ${DATA}/PXX0/F16.ITL115DO.MICROFIC
       m_OutputAssign -c 9 -w ITL115B SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEN
       ;;
(GTL11OEN)
       m_CondExec 00,EQ,GTL11OEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL116                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OEQ PGM=IEBGENER   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OEQ
       ;;
(GTL11OEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A50} SYSUT1 ${DATA}/PXX0/F16.ITL116BO.MICROFIC
       m_OutputAssign -c 9 -w ITL116 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OER
       ;;
(GTL11OER)
       m_CondExec 00,EQ,GTL11OEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL118                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OET PGM=IEBGENER   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OET
       ;;
(GTL11OET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A51} SYSUT1 ${DATA}/PXX0/F16.ITL118BO.MICROFIC
       m_OutputAssign -c 9 -w ITL118 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEU
       ;;
(GTL11OEU)
       m_CondExec 00,EQ,GTL11OET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL119                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OEX PGM=IEBGENER   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OEX
       ;;
(GTL11OEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A52} SYSUT1 ${DATA}/PXX0/F16.ITL119BO.MICROFIC
       m_OutputAssign -c 9 -w ITL119 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OEY
       ;;
(GTL11OEY)
       m_CondExec 00,EQ,GTL11OEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL120                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFA PGM=IEBGENER   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFA
       ;;
(GTL11OFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A53} SYSUT1 ${DATA}/PXX0/F16.ITL120BO.MICROFIC
       m_OutputAssign -c 9 -w ITL120 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OFB
       ;;
(GTL11OFB)
       m_CondExec 00,EQ,GTL11OFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION ITL121                                                              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFD PGM=IEBGENER   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFD
       ;;
(GTL11OFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A54} SYSUT1 ${DATA}/PXX0/F16.ITL121BO.MICROFIC
       m_OutputAssign -c 9 -w ITL121 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GTL11OFE
       ;;
(GTL11OFE)
       m_CondExec 00,EQ,GTL11OFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI 10A : TRI DU FICHIER FTL117BO ISSU DU BTL117                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFG
       ;;
(GTL11OFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A55} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117BO
# ******  FICHIER A ENVOYER A COPERNIC VIA LA GATEWAY                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FTL117CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/REFORMAT LAYOUT rl_sumDBYTE x"20"
/FIELDS 
       FD_CH_1_3     1    3 CH,
       FD_sep_01     4    1 CH,
        
       FD_CH_5_4     5    4 CH,
       FD_sep_02     9    1 CH,
        
       FD_CH_10_3    10   3 CH,
       FD_sep_03     13   1 CH,
       FD_CH_14_3    14   3 CH,
       FD_sep_04     17   1 CH,
       FD_CH_18_7    18   7 CH,
       FD_sep_05     25   1 CH,
       FD_EN_26_10   26   EN 10,
       FD_sep_06     36   1 CH,
       FD_CH_37_3    37    3 CH,
       FD_sep_07     40    1 CH,
       FD_CH_41_3    41   3 CH,
       FD_sep_08     44   1 CH
/DERIVEDFIELD total_SFF   FD_EN_26_10   (9999999V99)
/KEYS FD_CH_1_3, FD_CH_5_4, FD_CH_10_3,   FD_CH_14_3, FD_CH_18_7, FD_CH_37_3, FD_CH_41_3
/SUMMARIZE TOTAL FD_EN_26_10
/MT_OUTFILE_ASG SORTOUT
/DYNAMICLAYOUT rl_sum POSITIONAL RETAINCOMPOSITE VALUE
        FD_CH_1_3    ALIAS BFD_CH_1_3,
        FD_sep_01       ALIAS BFD_sep_01,
        FD_CH_5_4    ALIAS BFD_CH_5_4,
        FD_sep_02       ALIAS BFD_sep_02,
        FD_CH_10_3  ALIAS BFD_CH_10_3,
        FD_sep_03       ALIAS BFD_sep_03,
        FD_CH_14_3  ALIAS BFD_CH_14_3,
        FD_sep_04       ALIAS BFD_sep_04,
        FD_CH_18_7  ALIAS BFD_CH_18_7,
        FD_sep_05       ALIAS BFD_sep_05,
        total_SFF   ALIAS total_OVERLAY,
        FD_sep_06       ALIAS BFD_sep_06,
        FD_CH_37_3  ALIAS AFTER_FD_CH_37_3,
        FD_sep_07       ALIAS BFD_sep_07,
        FD_CH_41_3  ALIAS AFTER_FD_CH_41_3   PRESERVENULLINFORMATION
        FD_sep_08       ALIAS BFD_sep_08
/REFORMAT LAYOUT rl_sum

 /FIELDS FLD_ZD_26_10 26 ZD 10
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_37_3 37 CH 3
 /FIELDS FLD_CH_5_4 5 CH 4
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_CH_18_7 18 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_5_4 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_18_7 ASCENDING,
   FLD_CH_37_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_26_10
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GTL11OFH
       ;;
(GTL11OFH)
       m_CondExec 00,EQ,GTL11OFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL122                                                                
#  ------------                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFJ PGM=BTL122     ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFJ
       ;;
(GTL11OFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER FTL111B ISSU DU BTL111                                       
       m_FileAssign -d SHR -g ${G_A56} FTL111B ${DATA}/PXX0/F16.FTL111BO
# ******  FICHIER FTL122                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL122 ${DATA}/PTEM/GTL11OFJ.FTL122AO
# ****    PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_ProgramExec BTL122 
# ********************************************************************         
#  TRI 11A : TRI DU FICHIER FTL122AO                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFM PGM=SORT       ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFM
       ;;
(GTL11OFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A57} SORTIN ${DATA}/PTEM/GTL11OFJ.FTL122AO
# ******  FICHIER A ENVOYER A COPERNIC VIA LA GATEWAY                          
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FTL122BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS
    FD_CH_1_3     1   3 CH,
    FD_sep_01     4   1 CH,
 
    FD_CH_5_4     5   4  CH,
    FD_sep_02     9   1 CH,
 
    FD_CH_10_3    10  3 CH,
    FD_sep_03     13  1 CH,
 
    FD_CH_14_3    14  3 CH,
    FD_sep_04     17  1 CH,
 
    FD_CH_18_5    18  5 CH,
    FD_sep_05     23  1 CH,
 
    FD_CH_24_5    24  5 CH,
    FD_sep_06     29  1 CH,
 
    FD_CH_30_1    30  1 CH,
    FD_sep_07     31  1 CH,
 
    FD_CH_32_2    32  2 CH,
    FD_sep_08     34  1 CH,
 
    FD_EN_35_10   35  EN 10,
    FD_sep_09     45  1 CH,
 
    FD_CH_46_3    46  3 CH,
    FD_sep_10     49  1 CH,
 
    FD_CH_50_3    50  3 CH,
    FD_sep_11     53  1 CH
 
/DERIVEDFIELD total_SFF   FD_EN_35_10  (9999999V99)
 
/KEYS FD_CH_1_3, FD_CH_5_4, FD_CH_10_3, FD_CH_14_3, FD_CH_18_5, FD_CH_24_5, FD_CH_30_1,
FD_CH_32_2, FD_CH_46_3, FD_CH_50_3
/SUMMARIZE TOTAL FD_EN_35_10
/MT_OUTFILE_ASG SORTOUT
/DYNAMICLAYOUT rl_sum POSITIONAL RETAINCOMPOSITE VALUE
  FD_CH_1_3    ALIAS BFD_CH_1_3,
  FD_sep_01       ALIAS BFD_sep_01,
  FD_CH_5_4    ALIAS BFD_CH_5_4,
  FD_sep_02       ALIAS BFD_sep_02,
  FD_CH_10_3  ALIAS BFD_CH_10_3,
  FD_sep_03       ALIAS BFD_sep_03,
  FD_CH_14_3  ALIAS BFD_CH_14_3,
  FD_sep_04       ALIAS BFD_sep_04,
  FD_CH_18_5  ALIAS BFD_CH_18_5,
  FD_sep_05       ALIAS BFD_sep_05,
  FD_CH_24_5  ALIAS BFD_CH_24_5,
  FD_sep_06       ALIAS BFD_sep_06,
  FD_CH_30_1  ALIAS BFD_CH_30_1,
  FD_sep_07       ALIAS BFD_sep_07,
  FD_CH_32_2  ALIAS BFD_CH_32_2,
  FD_sep_08       ALIAS BFD_sep_08,
  total_SFF   ALIAS total_OVERLAY,
  FD_sep_09       ALIAS AFD_sep_09,
  FD_CH_46_3  ALIAS AFTER_FD_CH_46_3,
  FD_sep_10       ALIAS AFD_sep_10,
  FD_CH_50_3  ALIAS AFTER_FD_CH_50_3,
  FD_sep_11       ALIAS AFD_sep_11
/REFORMAT LAYOUT rl_sum
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_5_4 5 CH 4
 /FIELDS FLD_CH_24_5 24 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_ZD_35_10 35 ZD 10
 /MT_INFILE_SORT SORTIN
 /REFORMAT 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_5_4 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_24_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_35_10
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GTL11OFN
       ;;
(GTL11OFN)
       m_CondExec 00,EQ,GTL11OFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AIR      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTL117XX,                                                           
#      FNAME=":FTL117CO""(0),                                                  
#      NFNAME=LIVPL_O_COP                                                      
#          DATAEND                                                             
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AIW      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTL122XX,                                                           
#      FNAME=":FTL122BO""(0),                                                  
#      NFNAME=LIVACT_O_COP                                                     
#          DATAEND                                                             
# ********************************************************************         
#  DEPENDANCE POUR PLAN                                                        
#  CHANGEMENT PCL => GTL11O <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGTL11O                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFQ PGM=FTP        ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFQ
       ;;
(GTL11OFQ)
       m_CondExec ${EXAIR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11OFQ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFT PGM=EZACFSM1   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFT
       ;;
(GTL11OFT)
       m_CondExec ${EXAIW},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11OFT.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGTL11O                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL11OFX PGM=FTP        ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OFX
       ;;
(GTL11OFX)
       m_CondExec ${EXAJB},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11OFX.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GTL11OGA PGM=EZACFSM1   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OGA
       ;;
(GTL11OGA)
       m_CondExec ${EXAJG},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11OGA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GTL11OZA
       ;;
(GTL11OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL11OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
