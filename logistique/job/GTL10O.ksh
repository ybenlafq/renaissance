#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL10O.ksh                       --- VERSION DU 19/10/2016 11:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGTL10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/11/25 AT 11.17.25 BY PREPA2                       
#    STANDARDS: P  JOBSET: GTL10O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSTL09O                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL10OA
       ;;
(GTL10OA)
#
#GTL10OAA
#GTL10OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GTL10OAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'0'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL10OAD
       ;;
(GTL10OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLE GENERALISEE
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01O /dev/null
# ******** TABLE DES QUOTAS DE LIVRAISON
#    RSGQ01O  : NAME=RSGQ01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGQ01O /dev/null
# ******** TABLE DES MVTS DE STOCK
#    RSGS40O  : NAME=RSGS40O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGS40O /dev/null
# ******** TABLE DES VENTES/ADRESSE
#    RSGV02O  : NAME=RSGV02O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV02O /dev/null
# ******** TABLE DES VENTES
#    RSGV11O  : NAME=RSGV11O,MODE=(I,N) - DYNAM=YES
       m_FileAssign -d SHR RSGV11O /dev/null
# ******** TABLE DES VENTES/MODE DE DDELIV
#    RSGV23O  : NAME=RSGV23O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV23O /dev/null
# ******** TABLE DES TOUNEES (GENERALITES)
#    RSTL01O  : NAME=RSTL01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL01O /dev/null
# ******** TABLE DES TOURNEES (DETAILS)
#    RSTL02O  : NAME=RSTL02O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL02O /dev/null
# ******** TABLE DES TOURNEES (RETOURS)
#    RSTL04O  : NAME=RSTL04O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL04O /dev/null
# 
# ******** TABLE DES TOURNESS (HISTOS)
#    RSTL09O  : NAME=RSTL09O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSTL09O /dev/null
# ************************************** TABLE HISTO VENTES
#    RSHV15M  : NAME=RSHV15O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSHV15M /dev/null
# 
# ******** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ******** PARAMETRE SOCIETE
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DODEP
# ******** PARAMETRE  H:HEBDO  M:MENSUEL
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10OAD
# ******** FIC D'EXTRACT FTL110
       m_FileAssign -d SHR -g +0 FTL110 ${DATA}/PXX0/F16.FTL110AO
# ******** FIC D'EXTRACT FTL110
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL110N ${DATA}/PXX0/F16.FTL110AO
# ******** FIC D'EXTRACT FTL111
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111 ${DATA}/PXX0/F16.BTL111AO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL110 
       JUMP_LABEL=GTL10OAE
       ;;
(GTL10OAE)
       m_CondExec 04,GE,GTL10OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  MERGE : CUMUL DU FICHIER BTL111AO CREE PAR LE PROG BTL110 AVEC
#          L'ANCIEN BTL111AR DU DERNIER TRAITEMENT
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OAG PGM=SORT       ** ID=AAK
# ***********************************
       JUMP_LABEL=GTL10OAG
       ;;
(GTL10OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F16.BTL111AO
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F16.BTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g ${G_A3} SORTOUT ${DATA}/PXX0/F16.BTL111AO
# **************************POGTL1************************************
#  BTL111 : INITIALISATION DES FICHIERS FTL111A  ET FTL111B
#  REPRISE: OUI
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_8 7 CH 8
 /FIELDS FLD_CH_20_5 20 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_8 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_20_5 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OAH
       ;;
(GTL10OAH)
       m_CondExec 00,EQ,GTL10OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# 
# ***********************************
# *   STEP GTL10OAJ PGM=IKJEFT01   ** ID=AAP
# ***********************************
       JUMP_LABEL=GTL10OAJ
       ;;
(GTL10OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLE GENERALISE
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01O /dev/null
# ******** TABLE DES LIEUX
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10O /dev/null
# ******** TABLE DES ANOMALIES
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00O /dev/null
# ******** FIC D'EXTRACT ISSU DU BLT110
       m_FileAssign -d SHR -g ${G_A4} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ******** FIC D'EXTRACT ISSU DU TRI 1A
       m_FileAssign -d SHR -g ${G_A5} FTL111 ${DATA}/PXX0/F16.BTL111AO
# ******** FICHIER FTL111A
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111A ${DATA}/PXX0/F16.FTL111AO
# ******** FICHIER FTL111B
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL111B ${DATA}/PXX0/F16.FTL111BO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL111 
       JUMP_LABEL=GTL10OAK
       ;;
(GTL10OAK)
       m_CondExec 04,GE,GTL10OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 2A : TRI DU FICHIER FTL111AM POUR CREATION DU FLT112AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OAM PGM=SORT       ** ID=AAU
# ***********************************
       JUMP_LABEL=GTL10OAM
       ;;
(GTL10OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OAM.FTL112AO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_51_8 51 CH 8
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_51_8 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OAN
       ;;
(GTL10OAN)
       m_CondExec 00,EQ,GTL10OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 2B : TRI DU FICHIER FTL111AO POUR CREATION DU FTL112BO
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OAQ PGM=SORT       ** ID=AAZ
# ***********************************
       JUMP_LABEL=GTL10OAQ
       ;;
(GTL10OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OAQ.FTL112BO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_CH_108_5 108 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OAR
       ;;
(GTL10OAR)
       m_CondExec 00,EQ,GTL10OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 2C : TRI DU FICHIER FTL111BO POUR CREATION FU FTL112CO
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OAT PGM=SORT       ** ID=ABE
# ***********************************
       JUMP_LABEL=GTL10OAT
       ;;
(GTL10OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OAT.FTL112CO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_91_2 91 CH 2
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_89_2 89 CH 2
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_40_1 40 CH 1
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_108_5 108 CH 5
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_108_5 ASCENDING,
   FLD_CH_89_2 ASCENDING,
   FLD_CH_91_2 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_40_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OAU
       ;;
(GTL10OAU)
       m_CondExec 00,EQ,GTL10OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL112
#  CREATION DES ETATS ITL112A ET ITL112B
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OAX PGM=IKJEFT01   ** ID=ABJ
# ***********************************
       JUMP_LABEL=GTL10OAX
       ;;
(GTL10OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01O /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00O /dev/null
# ************************************** FICHIERS ISSU DU BLT110
       m_FileAssign -d SHR -g ${G_A9} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** FICHIERS ISSU DU TRI 2A
       m_FileAssign -d SHR -g ${G_A10} FTL112A ${DATA}/PTEM/GTL10OAM.FTL112AO
# ************************************** FICHIERS ISSU DU TRI 2B
       m_FileAssign -d SHR -g ${G_A11} FTL112B ${DATA}/PTEM/GTL10OAQ.FTL112BO
# ************************************** FICHIERS ISSU DU TRI 2C
       m_FileAssign -d SHR -g ${G_A12} FTL112C ${DATA}/PTEM/GTL10OAT.FTL112CO
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT DE STAT PAR B.L
       m_OutputAssign -c 9 -w ITL112A ITL112A
# ************************************** ETAT DE STAT PAR PIECES
       m_OutputAssign -c 9 -w ITL112B ITL112B
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL112 
       JUMP_LABEL=GTL10OAY
       ;;
(GTL10OAY)
       m_CondExec 04,GE,GTL10OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3A : TRI DU FICHIER FTL111AM POUR CREATION DU FTL113AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBA PGM=SORT       ** ID=ABO
# ***********************************
       JUMP_LABEL=GTL10OBA
       ;;
(GTL10OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBA.FTL113AO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_93_2 93 CH 2
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBB
       ;;
(GTL10OBB)
       m_CondExec 00,EQ,GTL10OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3B : TRI DU FICHIER FTL111AM POUR CREATION DU FTL113BM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBD PGM=SORT       ** ID=ABT
# ***********************************
       JUMP_LABEL=GTL10OBD
       ;;
(GTL10OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F16.FTL111AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBD.FTL113BO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_PD_59_3 59 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBE
       ;;
(GTL10OBE)
       m_CondExec 00,EQ,GTL10OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3C : TRI DES FICHIERS FTL111AM,FTL111BM POUR CREATION DU FTL113
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBG PGM=SORT       ** ID=ABY
# ***********************************
       JUMP_LABEL=GTL10OBG
       ;;
(GTL10OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBG.FTL113CO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_15 "     "
 /FIELDS FLD_CH_95_2 95 CH 2
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_25_5 25 CH 5
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_CH_93_2 93 CH 2
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_15 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING,
   FLD_CH_93_2 ASCENDING,
   FLD_CH_95_2 ASCENDING,
   FLD_CH_65_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBH
       ;;
(GTL10OBH)
       m_CondExec 00,EQ,GTL10OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 3D : TRI DES FICHIERS FTL111AM,FTL111BM POUR CREATION FU FTL113
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBJ PGM=SORT       ** ID=ACD
# ***********************************
       JUMP_LABEL=GTL10OBJ
       ;;
(GTL10OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBJ.FTL113DO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_59_3 59 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_25_5 25 CH 5
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_25_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_59_3,
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBK
       ;;
(GTL10OBK)
       m_CondExec 00,EQ,GTL10OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL113
#  CREATION DE L'ETAT ITL113
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBM PGM=IKJEFT01   ** ID=ACI
# ***********************************
       JUMP_LABEL=GTL10OBM
       ;;
(GTL10OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01O /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00O /dev/null
# ************************************** FICHIERS ISSU DU BLT110
       m_FileAssign -d SHR -g ${G_A17} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** FICHIERS ISSU DU TRI 3A
       m_FileAssign -d SHR -g ${G_A18} FTL113H ${DATA}/PTEM/GTL10OBA.FTL113AO
# ************************************** FICHIERS ISSU DU TRI 3B
       m_FileAssign -d SHR -g ${G_A19} FTL113HT ${DATA}/PTEM/GTL10OBD.FTL113BO
# ************************************** FICHIERS ISSU DU TRI 3C
       m_FileAssign -d SHR -g ${G_A20} FTL113M ${DATA}/PTEM/GTL10OBG.FTL113CO
# ************************************** FICHIERS ISSU DU TRI 3D
       m_FileAssign -d SHR -g ${G_A21} FTL113MT ${DATA}/PTEM/GTL10OBJ.FTL113DO
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** ETAT STAT DES RETOURS DE LIVR
       m_OutputAssign -c 9 -w ITL113 ITL113
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL113 
       JUMP_LABEL=GTL10OBN
       ;;
(GTL10OBN)
       m_CondExec 04,GE,GTL10OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 4A : TRI DU FICHIER FTL111BO POUR CREATION DU FTL114AO
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBQ PGM=SORT       ** ID=ACN
# ***********************************
       JUMP_LABEL=GTL10OBQ
       ;;
(GTL10OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBQ.FTL114EO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_12 "     "
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /CONDITION CND_2 FLD_CH_65_5 EQ CST_1_12 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /OMIT CND_2
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBR
       ;;
(GTL10OBR)
       m_CondExec 00,EQ,GTL10OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 4B : TRI DU FICHIER FTL111BO POUR CREATION DU FTL114BO
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBT PGM=SORT       ** ID=ACS
# ***********************************
       JUMP_LABEL=GTL10OBT
       ;;
(GTL10OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBT.FTL114FO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "     "
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_44_7 44 CH 7
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_97_2 97 CH 2
 /FIELDS FLD_CH_41_3 41 CH 3
 /CONDITION CND_1 FLD_CH_65_5 EQ CST_1_8 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING,
   FLD_CH_97_2 ASCENDING,
   FLD_CH_65_5 ASCENDING,
   FLD_CH_44_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBU
       ;;
(GTL10OBU)
       m_CondExec 00,EQ,GTL10OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 4C : TRI DU FICHIER FTL111BM POUR CREATION DU FTL114CM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OBX PGM=SORT       ** ID=ACX
# ***********************************
       JUMP_LABEL=GTL10OBX
       ;;
(GTL10OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F16.FTL111BO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OBX.FTL114GO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_77_4 77 PD 4
 /FIELDS FLD_PD_81_4 81 PD 4
 /FIELDS FLD_CH_41_3 41 CH 3
 /FIELDS FLD_PD_71_3 71 PD 3
 /FIELDS FLD_PD_62_3 62 PD 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_85_4 85 PD 4
 /FIELDS FLD_PD_74_3 74 PD 3
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_41_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_62_3,
    TOTAL FLD_PD_71_3,
    TOTAL FLD_PD_74_3,
    TOTAL FLD_PD_77_4,
    TOTAL FLD_PD_81_4,
    TOTAL FLD_PD_85_4
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OBY
       ;;
(GTL10OBY)
       m_CondExec 00,EQ,GTL10OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL114
#  CREATION DES ETATS ITL114A ET ITL114B
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCA PGM=IKJEFT01   ** ID=ADC
# ***********************************
       JUMP_LABEL=GTL10OCA
       ;;
(GTL10OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES
#    RSGA00M  : NAME=RSGA00O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES VENTES/ADRESSE
#    RSGV02M  : NAME=RSGV02O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV02M /dev/null
# ************************************** TABLE DES ENTETES DE VENTES
#    RSGV10M  : NAME=RSGV10O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV10M /dev/null
# ************************************** TABLE DES VENTES
#    RSGV11M  : NAME=RSGV11O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV11M /dev/null
# ************************************** TABLE DES VENTES/MODE DE DDEL
#    RSGV23M  : NAME=RSGV23O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGV23M /dev/null
# ************************************** TABLE DES TOURNEES (RETOURS)
#    RSTL04M  : NAME=RSTL04O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL04M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 4A
       m_FileAssign -d SHR -g ${G_A25} FTL114M ${DATA}/PTEM/GTL10OBQ.FTL114EO
# ************************************** FICHIERS ISSU DU TRI 4B
       m_FileAssign -d SHR -g ${G_A26} FTL114D ${DATA}/PTEM/GTL10OBT.FTL114FO
# ************************************** FICHIERS ISSU DU TRI 4C
       m_FileAssign -d SHR -g ${G_A27} FTL114MT ${DATA}/PTEM/GTL10OBX.FTL114GO
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT RETOURS MAGS PAR B.L
       m_OutputAssign -c 9 -w ITL114A ITL114A
# ************************************** STAT RETOURS MAGS PAR PIECES
#  ITL114B  REPORT SYSOUT=(9,ITL114B),RECFM=FBA,LRECL=133,BLKSIZE=1330
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL114 
       JUMP_LABEL=GTL10OCB
       ;;
(GTL10OCB)
       m_CondExec 04,GE,GTL10OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL116
#  CREATION DE L'ETAT ITL116
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCD PGM=IKJEFT01   ** ID=ADH
# ***********************************
       JUMP_LABEL=GTL10OCD
       ;;
(GTL10OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01 /dev/null
# ************************************** TABLE DES LIVREURS
#    RSTL04   : NAME=RSTL03O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSTL04 /dev/null
# ************************************** TABLE DES TOURNESS (HISTOS)
#    RSTL09   : NAME=RSTL09O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSTL09 /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00   : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00 /dev/null
# ************************************** FICHIERS ISSU DU BTL110
       m_FileAssign -d SHR -g ${G_A28} FTL110 ${DATA}/PXX0/F16.FTL110AO
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** STAT DE LIVRAISON GLOBALE
       m_OutputAssign -c 9 -w ITL116 ITL116
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL116 
       JUMP_LABEL=GTL10OCE
       ;;
(GTL10OCE)
       m_CondExec 04,GE,GTL10OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL117
#  CREATION DU FICHIER FTL117AY
# ********************************************************************
#  REPRISE: OUI SI ABEND
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCG PGM=IKJEFT01   ** ID=ADM
# ***********************************
       JUMP_LABEL=GTL10OCG
       ;;
(GTL10OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE DES ARTICLES
#    RSGA00M  : NAME=RSGA00O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA00M /dev/null
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE FAMILLES/MODE DE DDELIV
#    RSGA13M  : NAME=RSGA13O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA13M /dev/null
# ************************************** TABLE DES FAMILLES
#    RSGA14M  : NAME=RSGA14O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA14M /dev/null
# ************************************** PARAM ASSOCIEE AUX FAMILLES
#    RSGA30M  : NAME=RSGA30O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA30M /dev/null
# ************************************** TABLE DES MVTS DE STOCK
#    RSGS40M  : NAME=RSGS40,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGS40M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** PARAMETRE FMOIS
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE DEPOT
       m_FileAssign -d SHR FDEP ${DATA}/CORTEX4.P.MTXTFIX1/DODEP
# ******  DATE JJMMSSAA
       m_FileAssign -i FDATE
$FDATE
_end
# ******  MENSUEL OU HEBDO
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10OCG
# ************************************** FIC D'EXTRACT FTL117AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117 ${DATA}/PTEM/GTL10OCG.FTL117AO
# *****   FICHIER A DESTINATION DE COPERNIC
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTL117B ${DATA}/PTEM/GTL10OCG.FTL117BO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       #Untranslated IKJEFT01 with unknown command
# DATA UNTRANSLATED: 
# SYSTSIN  DATA  *,CLASS=FIX2                                              
#  DSN SYSTEM(RDAR)                                                        
#  RUN PROGRAM(BTL117) PLAN(BTDARO)                                        
#  END                                                                     

       JUMP_LABEL=GTL10OCH
       ;;
(GTL10OCH)
       m_CondExec 04,GE,GTL10OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 6A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL118AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCJ PGM=SORT       ** ID=ADR
# ***********************************
       JUMP_LABEL=GTL10OCJ
       ;;
(GTL10OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OCJ.FTL118AO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_64_4 64 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OCK
       ;;
(GTL10OCK)
       m_CondExec 00,EQ,GTL10OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL118
#  CREATION DE L'ETAT ITL118
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCM PGM=IKJEFT01   ** ID=ADW
# ***********************************
       JUMP_LABEL=GTL10OCM
       ;;
(GTL10OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES LIEUX
#    RSGA10M  : NAME=RSGA10O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA10M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 6A
       m_FileAssign -d SHR -g ${G_A30} FTL118 ${DATA}/PTEM/GTL10OCJ.FTL118AO
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# ******  MENSUEL OU HEBDO
       m_FileAssign -d SHR CPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GTL10OCM
# ************************************** MODE DE DELIV/MAG ET FAM
       m_OutputAssign -c 9 -w ITL118 ITL118
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL118 
       JUMP_LABEL=GTL10OCN
       ;;
(GTL10OCN)
       m_CondExec 04,GE,GTL10OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  TRI 7A : TRI DU FICHIER FTL117AM POUR CREATION DU FTL119AM
# ********************************************************************
#  REPRISE : OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCQ PGM=SORT       ** ID=AEB
# ***********************************
       JUMP_LABEL=GTL10OCQ
       ;;
(GTL10OCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GTL10OCG.FTL117AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL10OCQ.FTL119AO
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_60_4 60 PD 4
 /FIELDS FLD_PD_48_4 48 PD 4
 /FIELDS FLD_PD_52_4 52 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_CH_12_5 12 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_37_1 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_4,
    TOTAL FLD_PD_52_4,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_4,
    TOTAL FLD_PD_64_4
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL10OCR
       ;;
(GTL10OCR)
       m_CondExec 00,EQ,GTL10OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BTL119
#  CREATION DE L'ETAT ITL119
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GTL10OCT PGM=IKJEFT01   ** ID=AEG
# ***********************************
       JUMP_LABEL=GTL10OCT
       ;;
(GTL10OCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************************************** TABLE GENERALISEE
#    RSGA01M  : NAME=RSGA01O,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RSGA01M /dev/null
# ************************************** TABLE DES ANOMALIES
#    RSAN00M  : NAME=RSAN00O,MODE=U - DYNAM=YES
       m_FileAssign -d SHR RSAN00M /dev/null
# ************************************** FICHIERS ISSU DU TRI 7A
       m_FileAssign -d SHR -g ${G_A32} FTL119 ${DATA}/PTEM/GTL10OCQ.FTL119AO
# ************************************** PARAMETRE DATE
       m_FileAssign -i FDATE
$FDATE
_end
# ************************************** PARAMETRE FMOIS
       m_FileAssign -i FMOIS
$FMOIS
_end
# ************************************** PARAMETRE SOCIETE
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# ************************************** SYNTHESE DES MODES DE DELIV/F
       m_OutputAssign -c 9 -w ITL119 ITL119
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BTL119 
       JUMP_LABEL=GTL10OCU
       ;;
(GTL10OCU)
       m_CondExec 04,GE,GTL10OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  DEPENDANCE POUR PLAN
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=GTL10OZA
       ;;
(GTL10OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL10OZA.sysin
       m_UtilityExec
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
