#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS400O.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGS400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/03 AT 18.27.17 BY BURTECN                      
#    STANDARDS: P  JOBSET: GS400O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGS400 : EXTRACTION A PARTIR DE LA RTGS40 DES MOUVEMENTS DE REGUL           
#           SPECIALE                                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS400OA
       ;;
(GS400OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS400OAA
       ;;
(GS400OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLES                                                       
#    RSGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  TABLE DES EDITIONS ETAT/FAMILLE                                      
#    RSGA11O  : NAME=RSGA11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA11O /dev/null
# ******  TABLE DES MVTS DE STOCKS                                             
#    RSGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  TABLE DES PRMP                                                       
#    RSGG50O  : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50O /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00O  : NAME=RSAN00O,MODE=(U,U) - DYNAM=YES                            
# -X-RSAN00O  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSAN00O /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 122 -g +1 FGS400 ${DATA}/PTEM/GS400OAA.BGS400AO
# ******  NUMERO DE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  CARTE PARAMETRE (DERNIER JOUR DU MOIS)                               
       m_FileAssign -i FPARAM
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS400 
       JUMP_LABEL=GS400OAB
       ;;
(GS400OAB)
       m_CondExec 04,GE,GS400OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION                                                 
#  CRITERE DE TRI : (1,34,CH,A)                                                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS400OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GS400OAD
       ;;
(GS400OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GS400OAA.BGS400AO
       m_FileAssign -d NEW,CATLG,DELETE -r 122 -g +1 SORTOUT ${DATA}/PTEM/GS400OAD.BGS401AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_34 1 CH 34
 /KEYS
   FLD_CH_1_34 ASCENDING
 /* Record Type = F  Record Length = 122 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS400OAE
       ;;
(GS400OAE)
       m_CondExec 00,EQ,GS400OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS401 : EDITION DE LA LISTE DE REGULARISATION SPECIALE (JGS401)            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS400OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS400OAG
       ;;
(GS400OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00O  : NAME=RSAN00O,MODE=(U,U) - DYNAM=YES                            
# -X-RSAN00O  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSAN00O /dev/null
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION TRIE                                            
       m_FileAssign -d SHR -g ${G_A2} FGS400 ${DATA}/PTEM/GS400OAD.BGS401AO
# ******  NUMERO DE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  CARTE PARAMETRE (DERNIER JOUR DU MOIS)                               
       m_FileAssign -i FPARAM
$FMOISJ
_end
# ******  EDITION DE L'ETAT DES REGULS SPECIALES                               
#  JGS401   REPORT SYSOUT=(9,IGS401)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 126 -g +1 JGS401 ${DATA}/PXX0/F16.IGS401AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS401 
       JUMP_LABEL=GS400OAH
       ;;
(GS400OAH)
       m_CondExec 04,GE,GS400OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    EDITION  DE L'ETAT REGULARISATION SPECIALES SOUS DISPATCH                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS400OAJ PGM=IEBGENER   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS400OAJ
       ;;
(GS400OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A3} SYSUT1 ${DATA}/PXX0/F16.IGS401AO
       m_OutputAssign -c 9 -w IGS401 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GS400OAK
       ;;
(GS400OAK)
       m_CondExec 00,EQ,GS400OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GS400OZA
       ;;
(GS400OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS400OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
