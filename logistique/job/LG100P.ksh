#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LG100P.ksh                       --- VERSION DU 13/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPLG100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 10.25.48 BY BURTEC6                      
#    STANDARDS: P  JOBSET: LG100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   DELETE DES FICHIERS SEQUENTIEL                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LG100PA
       ;;
(LG100PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXAQO=${EXAQO:-0}
       EXAQT=${EXAQT:-0}
       EXAQY=${EXAQY:-0}
       EXARD=${EXARD:-0}
       EXARI=${EXARI:-0}
       EXARN=${EXARN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A71=${G_A71:-'+1'}
       G_A72=${G_A72:-'+1'}
       G_A73=${G_A73:-'+1'}
       G_A74=${G_A74:-'+1'}
       G_A75=${G_A75:-'+1'}
       G_A76=${G_A76:-'+1'}
       G_A77=${G_A77:-'+1'}
       G_A78=${G_A78:-'+1'}
       G_A79=${G_A79:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A80=${G_A80:-'+1'}
       G_A81=${G_A81:-'+1'}
       G_A82=${G_A82:-'+1'}
       G_A83=${G_A83:-'+1'}
       G_A84=${G_A84:-'+1'}
       G_A85=${G_A85:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LG100PAA
       ;;
(LG100PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=LG100PAB
       ;;
(LG100PAB)
       m_CondExec 16,NE,LG100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   UNLOAD CODE PARAM�TRE LGTPS DE CHAQUE FILIALE                              
#   UNLOAD DES SOUS TABLE LGTPS ET LGTPM + RTGA30                              
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAD
       ;;
(LG100PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
#    RSGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
#    RSGA30L  : NAME=RSGA30L,MODE=I - DYNAM=YES                                
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
#    RSGA30M  : NAME=RSGA30M,MODE=I - DYNAM=YES                                
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
#    RSGA30D  : NAME=RSGA30D,MODE=I - DYNAM=YES                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/LG100PAD.LG100UAP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   PROG    BLG100 : PGM  D EXTRACTION                                         
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAG
       ;;
(LG100PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE ARTICLES                                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA04   : NAME=RSGA04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA04 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA80   : NAME=RSGA83,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA80 /dev/null
#  TABLE DES VENTES                                                            
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV10   : NAME=RSGV10Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV10   : NAME=RSGV10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#  TABLE DES TOURNEES                                                          
#    RTTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL02 /dev/null
#  INTERROGATION DES TABLES FILIALES VIA SYNONYMES                             
#    RTTL02   : NAME=RSTL02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#    RTTL02   : NAME=RSTL02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTL02 /dev/null
#                                                                              
#    RTTL11   : NAME=RSTL11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTL11 /dev/null
#  TABLE EN MAJ                                                                
#    RTGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB05 /dev/null
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/LG100PAG
       m_FileAssign -d SHR -g ${G_A1} FRTGA01 ${DATA}/PTEM/LG100PAD.LG100UAP
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F07.BLG100CP
#  FICHIER EN SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FLG360 ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FSYSIN ${DATA}/PXX0/F07.BLG100BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLG100 
       JUMP_LABEL=LG100PAH
       ;;
(LG100PAH)
       m_CondExec 04,GE,LG100PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907130                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAJ
       ;;
(LG100PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907130
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907130"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PAK
       ;;
(LG100PAK)
       m_CondExec 00,EQ,LG100PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907131                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAM
       ;;
(LG100PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907131
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907131"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PAN
       ;;
(LG100PAN)
       m_CondExec 00,EQ,LG100PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907132                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAQ
       ;;
(LG100PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907132
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907132"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PAR
       ;;
(LG100PAR)
       m_CondExec 00,EQ,LG100PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907133                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAT
       ;;
(LG100PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907133
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907133"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PAU
       ;;
(LG100PAU)
       m_CondExec 00,EQ,LG100PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 907134                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PAX
       ;;
(LG100PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F907134
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907134"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PAY
       ;;
(LG100PAY)
       m_CondExec 00,EQ,LG100PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916001                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBA
       ;;
(LG100PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916001
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916001"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBB
       ;;
(LG100PBB)
       m_CondExec 00,EQ,LG100PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916015                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBD
       ;;
(LG100PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916015
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916015"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBE
       ;;
(LG100PBE)
       m_CondExec 00,EQ,LG100PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916018                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBG
       ;;
(LG100PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916018
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916018"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBH
       ;;
(LG100PBH)
       m_CondExec 00,EQ,LG100PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916019                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBJ
       ;;
(LG100PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916019
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916019"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBK
       ;;
(LG100PBK)
       m_CondExec 00,EQ,LG100PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916020                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBM
       ;;
(LG100PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916020
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916020"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBN
       ;;
(LG100PBN)
       m_CondExec 00,EQ,LG100PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916024                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBQ
       ;;
(LG100PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916024
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916024"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBR
       ;;
(LG100PBR)
       m_CondExec 00,EQ,LG100PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916025                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBT
       ;;
(LG100PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916025
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916025"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBU
       ;;
(LG100PBU)
       m_CondExec 00,EQ,LG100PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916026                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=LG100PBX
       ;;
(LG100PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916026
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916026"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PBY
       ;;
(LG100PBY)
       m_CondExec 00,EQ,LG100PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916031                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCA
       ;;
(LG100PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916031
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916031"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCB
       ;;
(LG100PCB)
       m_CondExec 00,EQ,LG100PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916036                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCD
       ;;
(LG100PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916036
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916036"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCE
       ;;
(LG100PCE)
       m_CondExec 00,EQ,LG100PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916037                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCG
       ;;
(LG100PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916037
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916037"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCH
       ;;
(LG100PCH)
       m_CondExec 00,EQ,LG100PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916039                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCJ
       ;;
(LG100PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916039
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916039"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCK
       ;;
(LG100PCK)
       m_CondExec 00,EQ,LG100PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916045                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCM
       ;;
(LG100PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916045
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916045"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCN
       ;;
(LG100PCN)
       m_CondExec 00,EQ,LG100PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916047                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCQ
       ;;
(LG100PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916047
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916047"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCR
       ;;
(LG100PCR)
       m_CondExec 00,EQ,LG100PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916052                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCT
       ;;
(LG100PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916052
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916052"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCU
       ;;
(LG100PCU)
       m_CondExec 00,EQ,LG100PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916058                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=LG100PCX
       ;;
(LG100PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916058
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916058"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PCY
       ;;
(LG100PCY)
       m_CondExec 00,EQ,LG100PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916160                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDA
       ;;
(LG100PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916160
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916160"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDB
       ;;
(LG100PDB)
       m_CondExec 00,EQ,LG100PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916161                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDD
       ;;
(LG100PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916161
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916161"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDE
       ;;
(LG100PDE)
       m_CondExec 00,EQ,LG100PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916162                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDG
       ;;
(LG100PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916162
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916162"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDH
       ;;
(LG100PDH)
       m_CondExec 00,EQ,LG100PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916163                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDJ
       ;;
(LG100PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916163
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916163"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDK
       ;;
(LG100PDK)
       m_CondExec 00,EQ,LG100PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916164                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDM
       ;;
(LG100PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916164
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916164"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDN
       ;;
(LG100PDN)
       m_CondExec 00,EQ,LG100PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916165                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDQ
       ;;
(LG100PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916165
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916165"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDR
       ;;
(LG100PDR)
       m_CondExec 00,EQ,LG100PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916166                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDT
       ;;
(LG100PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916166
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916166"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDU
       ;;
(LG100PDU)
       m_CondExec 00,EQ,LG100PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916167                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PDX
       ;;
(LG100PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916167
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916167"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PDY
       ;;
(LG100PDY)
       m_CondExec 00,EQ,LG100PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916168                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=LG100PEA
       ;;
(LG100PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916168
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916168"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEB
       ;;
(LG100PEB)
       m_CondExec 00,EQ,LG100PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916169                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PED
       ;;
(LG100PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916169
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916169"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEE
       ;;
(LG100PEE)
       m_CondExec 00,EQ,LG100PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916170                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=LG100PEG
       ;;
(LG100PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916170
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916170"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEH
       ;;
(LG100PEH)
       m_CondExec 00,EQ,LG100PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916171                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=LG100PEJ
       ;;
(LG100PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916171
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916171"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEK
       ;;
(LG100PEK)
       m_CondExec 00,EQ,LG100PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916172                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=LG100PEM
       ;;
(LG100PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916172
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916172"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEN
       ;;
(LG100PEN)
       m_CondExec 00,EQ,LG100PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916173                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=LG100PEQ
       ;;
(LG100PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916173
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916173"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PER
       ;;
(LG100PER)
       m_CondExec 00,EQ,LG100PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916174                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=LG100PET
       ;;
(LG100PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916174
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916174"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEU
       ;;
(LG100PEU)
       m_CondExec 00,EQ,LG100PET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916175                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PEX PGM=SORT       ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=LG100PEX
       ;;
(LG100PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916175
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916175"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PEY
       ;;
(LG100PEY)
       m_CondExec 00,EQ,LG100PEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916176                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFA
       ;;
(LG100PFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916176
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916176"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFB
       ;;
(LG100PFB)
       m_CondExec 00,EQ,LG100PFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916177                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFD PGM=SORT       ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFD
       ;;
(LG100PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916177
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916177"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFE
       ;;
(LG100PFE)
       m_CondExec 00,EQ,LG100PFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916178                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFG
       ;;
(LG100PFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916178
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916178"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFH
       ;;
(LG100PFH)
       m_CondExec 00,EQ,LG100PFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916179                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFJ PGM=SORT       ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFJ
       ;;
(LG100PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A42} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916179
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916179"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFK
       ;;
(LG100PFK)
       m_CondExec 00,EQ,LG100PFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 916210                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFM PGM=SORT       ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFM
       ;;
(LG100PFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F916210
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "916210"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFN
       ;;
(LG100PFN)
       m_CondExec 00,EQ,LG100PFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945069                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFQ PGM=SORT       ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFQ
       ;;
(LG100PFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A44} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945069
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945069"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFR
       ;;
(LG100PFR)
       m_CondExec 00,EQ,LG100PFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945070                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFT PGM=SORT       ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFT
       ;;
(LG100PFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945070
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945070"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFU
       ;;
(LG100PFU)
       m_CondExec 00,EQ,LG100PFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945071                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PFX PGM=SORT       ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=LG100PFX
       ;;
(LG100PFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945071
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945071"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PFY
       ;;
(LG100PFY)
       m_CondExec 00,EQ,LG100PFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945072                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGA PGM=SORT       ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGA
       ;;
(LG100PGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A47} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945072
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945072"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGB
       ;;
(LG100PGB)
       m_CondExec 00,EQ,LG100PGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945073                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGD PGM=SORT       ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGD
       ;;
(LG100PGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945073
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945073"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGE
       ;;
(LG100PGE)
       m_CondExec 00,EQ,LG100PGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945074                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGG PGM=SORT       ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGG
       ;;
(LG100PGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A49} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945074
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945074"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGH
       ;;
(LG100PGH)
       m_CondExec 00,EQ,LG100PGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945075                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGJ PGM=SORT       ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGJ
       ;;
(LG100PGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A50} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945075
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945075"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGK
       ;;
(LG100PGK)
       m_CondExec 00,EQ,LG100PGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945076                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGM PGM=SORT       ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGM
       ;;
(LG100PGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945076
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945076"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGN
       ;;
(LG100PGN)
       m_CondExec 00,EQ,LG100PGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945077                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGQ PGM=SORT       ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGQ
       ;;
(LG100PGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A52} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945077
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945077"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGR
       ;;
(LG100PGR)
       m_CondExec 00,EQ,LG100PGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945078                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGT PGM=SORT       ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGT
       ;;
(LG100PGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A53} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945078
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945078"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGU
       ;;
(LG100PGU)
       m_CondExec 00,EQ,LG100PGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945080                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PGX PGM=SORT       ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=LG100PGX
       ;;
(LG100PGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A54} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945080
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945080"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PGY
       ;;
(LG100PGY)
       m_CondExec 00,EQ,LG100PGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945081                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHA PGM=SORT       ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHA
       ;;
(LG100PHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A55} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945081
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945081"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHB
       ;;
(LG100PHB)
       m_CondExec 00,EQ,LG100PHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945082                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHD PGM=SORT       ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHD
       ;;
(LG100PHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A56} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945082
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945082"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHE
       ;;
(LG100PHE)
       m_CondExec 00,EQ,LG100PHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945083                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHG PGM=SORT       ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHG
       ;;
(LG100PHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A57} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945083
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945083"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHH
       ;;
(LG100PHH)
       m_CondExec 00,EQ,LG100PHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945084                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHJ PGM=SORT       ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHJ
       ;;
(LG100PHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A58} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945084
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945084"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHK
       ;;
(LG100PHK)
       m_CondExec 00,EQ,LG100PHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 945086                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHM PGM=SORT       ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHM
       ;;
(LG100PHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A59} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F945086
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945086"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHN
       ;;
(LG100PHN)
       m_CondExec 00,EQ,LG100PHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961071                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHQ PGM=SORT       ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHQ
       ;;
(LG100PHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A60} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961071
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961071"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHR
       ;;
(LG100PHR)
       m_CondExec 00,EQ,LG100PHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961072                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHT PGM=SORT       ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHT
       ;;
(LG100PHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A61} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961072
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961072"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHU
       ;;
(LG100PHU)
       m_CondExec 00,EQ,LG100PHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961073                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PHX PGM=SORT       ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=LG100PHX
       ;;
(LG100PHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A62} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961073
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961073"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PHY
       ;;
(LG100PHY)
       m_CondExec 00,EQ,LG100PHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961074                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIA PGM=SORT       ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIA
       ;;
(LG100PIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A63} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961074
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961074"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIB
       ;;
(LG100PIB)
       m_CondExec 00,EQ,LG100PIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961075                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PID PGM=SORT       ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=LG100PID
       ;;
(LG100PID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A64} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961075
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961075"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIE
       ;;
(LG100PIE)
       m_CondExec 00,EQ,LG100PID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961076                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIG PGM=SORT       ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIG
       ;;
(LG100PIG)
       m_CondExec ${EXAMS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A65} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961076
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961076"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIH
       ;;
(LG100PIH)
       m_CondExec 00,EQ,LG100PIG ${EXAMS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961077                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIJ PGM=SORT       ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIJ
       ;;
(LG100PIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A66} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961077
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961077"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIK
       ;;
(LG100PIK)
       m_CondExec 00,EQ,LG100PIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961079                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIM PGM=SORT       ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIM
       ;;
(LG100PIM)
       m_CondExec ${EXANC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A67} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961079
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961079"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIN
       ;;
(LG100PIN)
       m_CondExec 00,EQ,LG100PIM ${EXANC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 961080                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIQ PGM=SORT       ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIQ
       ;;
(LG100PIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A68} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F961080
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "961080"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIR
       ;;
(LG100PIR)
       m_CondExec 00,EQ,LG100PIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989072                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIT PGM=SORT       ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIT
       ;;
(LG100PIT)
       m_CondExec ${EXANM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A69} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989072
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989072"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIU
       ;;
(LG100PIU)
       m_CondExec 00,EQ,LG100PIT ${EXANM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989075                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PIX PGM=SORT       ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=LG100PIX
       ;;
(LG100PIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A70} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989075
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989075"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PIY
       ;;
(LG100PIY)
       m_CondExec 00,EQ,LG100PIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989076                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJA PGM=SORT       ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJA
       ;;
(LG100PJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A71} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989076
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989076"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJB
       ;;
(LG100PJB)
       m_CondExec 00,EQ,LG100PJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 989078                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJD PGM=SORT       ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJD
       ;;
(LG100PJD)
       m_CondExec ${EXAOB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A72} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F989078
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989078"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJE
       ;;
(LG100PJE)
       m_CondExec 00,EQ,LG100PJD ${EXAOB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991007                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJG PGM=SORT       ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJG
       ;;
(LG100PJG)
       m_CondExec ${EXAOG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A73} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991007
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991007"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJH
       ;;
(LG100PJH)
       m_CondExec 00,EQ,LG100PJG ${EXAOG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991010                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJJ PGM=SORT       ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJJ
       ;;
(LG100PJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A74} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991010
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991010"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJK
       ;;
(LG100PJK)
       m_CondExec 00,EQ,LG100PJJ ${EXAOL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991014                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJM PGM=SORT       ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJM
       ;;
(LG100PJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A75} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991014
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991014"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJN
       ;;
(LG100PJN)
       m_CondExec 00,EQ,LG100PJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991018                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJQ PGM=SORT       ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJQ
       ;;
(LG100PJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A76} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991018
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991018"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJR
       ;;
(LG100PJR)
       m_CondExec 00,EQ,LG100PJQ ${EXAOV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991077                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJT
       ;;
(LG100PJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A77} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991077
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991077"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJU
       ;;
(LG100PJU)
       m_CondExec 00,EQ,LG100PJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991078                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PJX PGM=SORT       ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=LG100PJX
       ;;
(LG100PJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A78} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991078
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991078"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PJY
       ;;
(LG100PJY)
       m_CondExec 00,EQ,LG100PJX ${EXAPF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991079                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKA
       ;;
(LG100PKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A79} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991079
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991079"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PKB
       ;;
(LG100PKB)
       m_CondExec 00,EQ,LG100PKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991080                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKD PGM=SORT       ** ID=APP                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKD
       ;;
(LG100PKD)
       m_CondExec ${EXAPP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A80} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991080
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991080"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PKE
       ;;
(LG100PKE)
       m_CondExec 00,EQ,LG100PKD ${EXAPP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991081                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKG PGM=SORT       ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKG
       ;;
(LG100PKG)
       m_CondExec ${EXAPU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A81} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991081
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991081"
 /FIELDS FLD_CH_222_8 222 CH 8
 /FIELDS FLD_CH_211_6 211 CH 06
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PKH
       ;;
(LG100PKH)
       m_CondExec 00,EQ,LG100PKG ${EXAPU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991084                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKJ PGM=SORT       ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKJ
       ;;
(LG100PKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A82} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991084
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991084"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PKK
       ;;
(LG100PKK)
       m_CondExec 00,EQ,LG100PKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER POUR LA PTF 991088                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKM PGM=SORT       ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKM
       ;;
(LG100PKM)
       m_CondExec ${EXAQE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A83} SORTIN ${DATA}/PXX0/F07.BLG100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F99.F991088
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991088"
 /FIELDS FLD_CH_211_6 211 CH 06
 /FIELDS FLD_CH_222_8 222 CH 8
 /CONDITION CND_1 FLD_CH_211_6 EQ CST_1_5 
 /KEYS
   FLD_CH_211_6 ASCENDING,
   FLD_CH_222_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 500 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LG100PKN
       ;;
(LG100PKN)
       m_CondExec 00,EQ,LG100PKM ${EXAQE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES DIFF�RENTS FICHIERS PLATEFORME                                      
#  REPRISE : OUI                                                               
# ***********************************                                          
# AQJ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    FILE  NAME=BLG100BP,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKQ PGM=JVMLDM76   ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKQ
       ;;
(LG100PKQ)
       m_CondExec ${EXAQJ},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 256 -t LSEQ FICZIP ${DATA}/PXX0.F99.F360ZIP
# *                                                                            
       m_FileAssign -d SHR -g ${G_A84} SYSIN ${DATA}/PXX0/F07.BLG100BP
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI VERS SERVEUR 360 DE PROD                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AOL      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=F360ZIP,                                                            
#      FNAME=":F360ZIP"",                                                      
#      SAPPL=F360                                                              
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTLG100P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKT PGM=EZACFSM1   ** ID=AQO                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKT
       ;;
(LG100PKT)
       m_CondExec ${EXAQO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PKT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/LG100PKT.FTLG100P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTLG100P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PKX PGM=FTP        ** ID=AQT                                   
# ***********************************                                          
       JUMP_LABEL=LG100PKX
       ;;
(LG100PKX)
       m_CondExec ${EXAQT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PKX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.LG100PKT.FTLG100P(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP LG100PLA PGM=EZACFSM1   ** ID=AQY                                   
# ***********************************                                          
       JUMP_LABEL=LG100PLA
       ;;
(LG100PLA)
       m_CondExec ${EXAQY},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PLA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#   UPDATE DE LA TABLE RTLI00 POUR REMETTRE LE PARAM D'ORIGINE                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PLD PGM=IKJEFT01   ** ID=ARD                                   
# ***********************************                                          
       JUMP_LABEL=LG100PLD
       ;;
(LG100PLD)
       m_CondExec ${EXARD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# *******  MAJ TABLE ARTICLE INFOCENTRE                                        
#    RSLI00   : NAME=RSLI00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PLD.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=LG100PLE
       ;;
(LG100PLE)
       m_CondExec 04,GE,LG100PLD ${EXARD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IDCAMS CREATION A VIDE DES FICHIERS FPARAM :                               
#   - FIC1 : BLG100CP                                                          
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PLG PGM=IDCAMS     ** ID=ARI                                   
# ***********************************                                          
       JUMP_LABEL=LG100PLG
       ;;
(LG100PLG)
       m_CondExec ${EXARI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.BLG100CP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PLG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=LG100PLH
       ;;
(LG100PLH)
       m_CondExec 16,NE,LG100PLG ${EXARI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   AJOUT STEP POUR JALON ATOS                                                 
#   PERMET DE VERIFIER QUE LES JOBS NE SONT PAS EN RETARD                      
#   REPRISE : OUI SI PROBLEME DANS CE STEP METTRE TERMIN� SOUS PLAN _A          
#   CONDITION QUE CE SOIT LE DERNIER STEP DE LA CHAINE                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LG100PLJ PGM=IEBGENER   ** ID=ARN                                   
# ***********************************                                          
       JUMP_LABEL=LG100PZA
       ;;
(LG100PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LG100PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
