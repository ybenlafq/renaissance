#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MU204P.ksh                       --- VERSION DU 13/10/2016 19:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMU204 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/13 AT 11.00.52 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MU204P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BRM258 : CE PGM SELECTIONNE LES LIENS DE RTRM58 DATEREP=DATE DU JOU         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MU204PA
       ;;
(MU204PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MU204PAA
       ;;
(MU204PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM25   : NAME=RSRM25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM25 /dev/null
#    RSRM75   : NAME=RSRM75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM75 /dev/null
#    RSFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL40 /dev/null
#    RSRM00   : NAME=RSRM00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM00 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
#    RSRM30   : NAME=RSRM30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM30 /dev/null
#    RSRM80   : NAME=RSRM80,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM80 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM95 /dev/null
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
#    RSRM70   : NAME=RSRM70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM70 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
# ******  TABLES EN MISE A JOUR                                                
#    RSRM85   : NAME=RSRM85,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#    RSRM90   : NAME=RSRM90,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM90 /dev/null
#    RSRM58   : NAME=RSRM58,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
#    RSRM60   : NAME=RSRM60,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM60 /dev/null
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FRM005 ${DATA}/PTEM/MU204PAA.BRM258AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM258 
       JUMP_LABEL=MU204PAB
       ;;
(MU204PAB)
       m_CondExec 04,GE,MU204PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EDITION                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAD
       ;;
(MU204PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/MU204PAA.BRM258AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BRM258BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_19_3 19 CH 03
 /FIELDS FLD_BI_12_7 12 CH 07
 /FIELDS FLD_BI_7_5 07 CH 05
 /FIELDS FLD_BI_22_2 22 CH 02
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_7 ASCENDING,
   FLD_BI_19_3 ASCENDING,
   FLD_BI_22_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PAE
       ;;
(MU204PAE)
       m_CondExec 00,EQ,MU204PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAG
       ;;
(MU204PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/F07.BRM258BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/MU204PAG.BRM258CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MU204PAH
       ;;
(MU204PAH)
       m_CondExec 04,GE,MU204PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAJ
       ;;
(MU204PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/MU204PAG.BRM258CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PAJ.BRM258DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PAK
       ;;
(MU204PAK)
       m_CondExec 00,EQ,MU204PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IRM005                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAM
       ;;
(MU204PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/F07.BRM258BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/MU204PAJ.BRM258DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRM005 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MU204PAN
       ;;
(MU204PAN)
       m_CondExec 04,GE,MU204PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES DES TABLES RTGA69 , RTGA68 , RTGF55 , RTGS36             
#  AJOUT D UNE SYSREC09 POUR UNLOAD DE LA RTGS36(MARS 2005)                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAQ
       ;;
(MU204PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
# **************************************                                       
#    RTGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
#    RTGA55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
#    RTGA36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 32 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA69P
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/MU204PAQ.UNGN68P
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/MU204PAQ.UNGF55P
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SYSREC04 ${DATA}/PTEM/MU204PAQ.UNGS36P
       m_FileAssign -d NEW,CATLG,DELETE -r 62 -t LSEQ -g +1 SYSREC05 ${DATA}/PTEM/MU204PAQ.UNFL50P
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 SYSREC06 ${DATA}/PTEM/MU204PAQ.UNGA01P
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 SYSREC07 ${DATA}/PXX0/F07.UNRM58P
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SYSREC08 ${DATA}/PTEM/MU204PAQ.UNGS36BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PAQ.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#   LOAD DE LA TABLE RTFL51 _A PARTIR DE LA FUSION SYSREC07 + SYSREC05          
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAT
       ;;
(MU204PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* FIC DE LOAD DE RTFL51                                                
       m_FileAssign -d SHR -g ${G_A6} SYSREC ${DATA}/PTEM/MU204PAQ.UNFL50P
#    RSFL51   : NAME=RSFL51,MODE=(U,N) - DYNAM=YES                             
# -X-RSFL51   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFL51 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/MU204P_MU204PAT_RTFL51.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=MU204PAU
       ;;
(MU204PAU)
       m_CondExec 04,GE,MU204PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DE L'UNLOAD SYSREC08                                                    
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MU204PAX
       ;;
(MU204PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.UNRM58P
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNRM58AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_7 8 CH 7
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_15_3 15 CH 3
 /KEYS
   FLD_CH_8_7 ASCENDING,
   FLD_CH_15_3 ASCENDING,
   FLD_CH_18_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PAY
       ;;
(MU204PAY)
       m_CondExec 00,EQ,MU204PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTFL53 _A PARTIR DU SYSREC06 PRECEDENT                     
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBA
       ;;
(MU204PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* FIC DE LOAD DE RTFL51                                                
       m_FileAssign -d SHR -g ${G_A8} SYSREC ${DATA}/PTEM/MU204PAQ.UNGA01P
#    RSFL53   : NAME=RSFL53,MODE=(U,N) - DYNAM=YES                             
# -X-RSFL53   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFL53 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/MU204P_MU204PBA_RTFL53.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=MU204PBB
       ;;
(MU204PBB)
       m_CondExec 04,GE,MU204PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU202 : DELETE DE LA RTGB06 DE LA VEILLE ET CREATION _A PARTIR DES          
#  MUTS DU JOUR DE LA RTGB05                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBD
       ;;
(MU204PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES MUTS DU JOUR                                               
#    RSGB06   : NAME=RSGB06,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB06 /dev/null
# ******  TABLE DETAIL DE MUT                                                  
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU202 
       JUMP_LABEL=MU204PBE
       ;;
(MU204PBE)
       m_CondExec 04,GE,MU204PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES POUR RECHARGEMENT DE LA TABLE RTFL52                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PBG PGM=PTLDRIVM   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBG
       ;;
(MU204PBG)
       m_CondExec ${EXABY},NE,YES 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 138 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/MU204PBG.UNFL52P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PBG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  TRI DE L'UNLOAD SUR L'INDEX CLUSTER                                         
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBJ
       ;;
(MU204PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/MU204PBG.UNFL52P
       m_FileAssign -d NEW,CATLG,DELETE -r 138 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PBJ.UNFL52AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_102_5 102 CH 5
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_56_6 56 CH 6
 /KEYS
   FLD_CH_56_6 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_102_5 ASCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PBK
       ;;
(MU204PBK)
       m_CondExec 00,EQ,MU204PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTFL52 _A PARTIR DU TRI PRECEDENT                          
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBM
       ;;
(MU204PBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* FIC DE LOAD DE RTFL52                                                
       m_FileAssign -d SHR -g ${G_A10} SYSREC ${DATA}/PTEM/MU204PBJ.UNFL52AP
#    RSFL52   : NAME=RSFL52,MODE=(U,N) - DYNAM=YES                             
# -X-RSFL52   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFL52 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/MU204P_MU204PBM_RTFL52.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=MU204PBN
       ;;
(MU204PBN)
       m_CondExec 04,GE,MU204PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RAJOUT CODE FILIALE DANS FIC UNGA68AP                                       
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBQ
       ;;
(MU204PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/MU204PAQ.UNGN68P
       m_FileAssign -d NEW,CATLG,DELETE -r 31 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PBQ.UNGN68AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_3  "907"
 /DERIVEDFIELD CST_2_5  "EFFIC"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_8 8 CH 8
 /FIELDS FLD_CH_16_1 16 CH 1
 /FIELDS FLD_CH_17_7 17 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_8 DESCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_3,FLD_CH_1_7,CST_2_5,FLD_CH_16_1,FLD_CH_8_8,FLD_CH_17_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MU204PBR
       ;;
(MU204PBR)
       m_CondExec 00,EQ,MU204PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER POUR PASSER LE FICHIER D'UNLOAD SUR 41                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PBT PGM=IEBGENER   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBT
       ;;
(MU204PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A12} SYSUT1 ${DATA}/PTEM/MU204PAQ.UNGS36P
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SYSUT2 ${DATA}/PTEM/MU204PBT.UNGS36AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_41 1 CH 41
 /COPY
 /MT_OUTFILE_ASG SYSUT2
 /REFORMAT FLD_CH_1_41
_end
       m_FileSort -s SYSIN -i SYSUT1
       JUMP_LABEL=MU204PBU
       ;;
(MU204PBU)
       m_CondExec 00,EQ,MU204PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DE L'UNLOAD SYSREC09 (SORTIE EN 41 DE LONG )                            
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=MU204PBX
       ;;
(MU204PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/MU204PAQ.UNGS36BP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PBX.UNGS36CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_40_7 40 CH 7
 /FIELDS FLD_CH_17_3 17 CH 3
 /FIELDS FLD_CH_8_3 08 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_32_8 32 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_7,FLD_CH_8_3,FLD_CH_11_3,FLD_CH_14_3,FLD_CH_17_3,FLD_CH_25_7,FLD_CH_32_8,FLD_CH_40_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MU204PBY
       ;;
(MU204PBY)
       m_CondExec 00,EQ,MU204PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DE L'UNLOAD SYSREC09 (SORTIE EN 41 DE LONG )                            
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCA
       ;;
(MU204PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/MU204PAQ.UNGS36BP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PCA.UNGS36DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_5  "DSP"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_8_3 08 CH 3
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_32_8 32 CH 8
 /FIELDS FLD_CH_40_7 40 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_7,FLD_CH_8_3,FLD_CH_11_3,CST_3_5,FLD_CH_21_3,FLD_CH_25_7,FLD_CH_32_8,FLD_CH_40_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MU204PCB
       ;;
(MU204PCB)
       m_CondExec 00,EQ,MU204PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER UNGS36BP (AJOUT) NOUVELLE ENTREE DU BMU204 FGS36I            
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCD
       ;;
(MU204PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/MU204PBT.UNGS36AP
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/MU204PCA.UNGS36DP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PCD.UNGS36EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_PD_17_3 17 PD 03
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_17_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PCE
       ;;
(MU204PCE)
       m_CondExec 00,EQ,MU204PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMU204 : CE PGM GENERE LES FICHIERS SEQUENTIELS UTILES AU BMU205            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCG
       ;;
(MU204PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE EN LECTURE                                                     
#    RTFL51   : NAME=RSFL51,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL51 /dev/null
#    RTFL52   : NAME=RSFL52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL52 /dev/null
#    RTFL53   : NAME=RSFL53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL53 /dev/null
#    RTGB06   : NAME=RSGB06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGB06 /dev/null
#    RTRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRM05 /dev/null
#    RTRM50   : NAME=RSRM50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRM50 /dev/null
#    RTRM95   : NAME=RSRM95,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRM95 /dev/null
#    RTGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA54 /dev/null
# ******  FICHIER EN ENTREE VENANT DE GM065P                                   
       m_FileAssign -d SHR -g +0 FHV04E ${DATA}/PEX0/F07.BGM065EP
# ******  FICHIER UNLOAD RTGN68                                                
       m_FileAssign -d SHR -g ${G_A17} FGA66E ${DATA}/PTEM/MU204PBQ.UNGN68AP
# ******  FICHIER UNLOAD RTGF55                                                
       m_FileAssign -d SHR -g ${G_A18} FGF55E ${DATA}/PTEM/MU204PAQ.UNGF55P
# ******  FICHIER UNLOAD RTGS36 (NOUVEAU FICHIER ISSU DU TRI)                  
       m_FileAssign -d SHR -g ${G_A19} FGS36E ${DATA}/PTEM/MU204PCD.UNGS36EP
# ******  FICHIER DE TRI RTGS36 (SYSREC09)                                     
       m_FileAssign -d SHR -g ${G_A20} FGS36I ${DATA}/PTEM/MU204PBX.UNGS36CP
# ******  FICHIER UNLOAD RTRM58                                                
       m_FileAssign -d SHR -g ${G_A21} FRM58 ${DATA}/PXX0/F07.UNRM58P
# ******  FICHIER RTRM58 APRES TRI                                             
       m_FileAssign -d SHR -g ${G_A22} FRM58B ${DATA}/PXX0/F07.UNRM58AP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 31 -t LSEQ -g +1 FGA66S ${DATA}/PXX0/F07.BMU204GP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 FGF55S ${DATA}/PXX0/F07.BMU204IP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 38 -t LSEQ -g +1 FGS36S ${DATA}/PXX0/F07.BMU204KP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -t LSEQ -g +1 FHV04S ${DATA}/PXX0/F07.BMU204LP
# ******  FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FGB15S ${DATA}/PXX0/F07.BMU204MP
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -t LSEQ -g +1 FGA58S ${DATA}/PXX0/F07.BMU204NP
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 FMUTIN ${DATA}/PTEM/MU204PCG.BMU204XP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU204 
       JUMP_LABEL=MU204PCH
       ;;
(MU204PCH)
       m_CondExec 04,GE,MU204PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LE BRM045 DU FICHIER BMU204XP                                      
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCJ
       ;;
(MU204PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/MU204PCG.BMU204XP
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PCJ.BMU204YP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_5_7 5 CH 7
 /FIELDS FLD_CH_12_6 12 CH 6
 /KEYS
   FLD_CH_5_7 ASCENDING,
   FLD_CH_12_6 ASCENDING
 /* Record Type = F  Record Length = 134 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PCK
       ;;
(MU204PCK)
       m_CondExec 00,EQ,MU204PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LE BRM045                                                          
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCM
       ;;
(MU204PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F07.BMU204KP
       m_FileAssign -d NEW,CATLG,DELETE -r 38 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PCM.BMU204CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_21_3 21 PD 3
 /FIELDS FLD_CH_32_7 32 CH 7
 /FIELDS FLD_CH_15_6 15 CH 6
 /KEYS
   FLD_CH_32_7 ASCENDING,
   FLD_CH_15_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_3
 /* Record Type = F  Record Length = 38 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PCN
       ;;
(MU204PCN)
       m_CondExec 00,EQ,MU204PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LE BRM045                                                          
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCQ
       ;;
(MU204PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PXX0/F07.BMU204MP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PCQ.BMU204EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_PD_16_3 16 PD 3
 /FIELDS FLD_CH_24_7 24 CH 7
 /KEYS
   FLD_CH_24_7 ASCENDING,
   FLD_CH_8_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_16_3
 /* Record Type = F  Record Length = 30 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PCR
       ;;
(MU204PCR)
       m_CondExec 00,EQ,MU204PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRM045                                                                
#  ------------                                                                
#  EXTRACTION DES MUTATIONS DONT LA DATE DE DEBUT DE SAISIE EST                
#  AU LENDEMAIN.EXTRACT RTRM50 DES STOCKS OBJECTIFS ET AVANCES POUR LE         
#  MUTS EN GESTION DE PENURIE                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCT
       ;;
(MU204PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c 9 -w PPMU204 SYSOUT
# ** TABLES EN LECTURE                                                         
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSGB06   : NAME=RSGB06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB06 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#    RSFL64   : NAME=RSFL64,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL64 /dev/null
# ****** TABLES AJOUTEES LE 22/04/03                                           
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
#                                                                              
# ** TABLES EN MAJ                                                             
#                                                                              
# ******  TABLE DETAIL DE MUT                                                  
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#                                                                              
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A26} FGS36E ${DATA}/PTEM/MU204PCM.BMU204CP
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A27} FGB15E ${DATA}/PTEM/MU204PCQ.BMU204EP
       m_FileAssign -d SHR -g ${G_A28} FMUTIN ${DATA}/PTEM/MU204PCJ.BMU204YP
# ******  FICHIER DES MUTATIONS _A TRAITER                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FMUMUT ${DATA}/PXX0/F07.BRM045AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM045 
       JUMP_LABEL=MU204PCU
       ;;
(MU204PCU)
       m_CondExec 04,GE,MU204PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT CODIC , TRI DES VENTES 4 DERNIERES SEMAINES                            
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MU204PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=MU204PCX
       ;;
(MU204PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMUMUT                                                              
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PXX0/F07.BRM045AP
#  FICHIER FMUMUT TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PCX.BRM045BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_100_6 100 CH 6
 /FIELDS FLD_PD_67_4 67 PD 4
 /KEYS
   FLD_CH_100_6 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_PD_67_4 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PCY
       ;;
(MU204PCY)
       m_CondExec 00,EQ,MU204PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM050 : CREATION DES MUTATIONS POUR LES ARTICLES EN GESTION DE             
#  PENURIE EN FONCTION DES VENTES DES 4 SEMAINES.STOCK MAGS,MUT EN             
#  COURS ET STOCK DEPOT POUR LES MAGASINS CONCERNES                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDA
       ;;
(MU204PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c 9 -w PPMU204 SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE CODICS GROUPE                                                  
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  TABLE DETAIL DE MUT                                                  
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  TABLE CALENDRIER MUT                                                 
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# ******  TABLE MUT                                                            
#    RSMU10   : NAME=RSMU10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU10 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ****** TABLES AJOUTEES LE 22/04/03                                           
#    RSRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM58 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES MUTATIONS A TRAITER TRIE                                 
       m_FileAssign -d SHR -g ${G_A30} FMUMUT ${DATA}/PTEM/MU204PCX.BRM045BP
# ******  FICHIER DES MUTATIONS MGI  L=28                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 FMUMGI ${DATA}/PTEM/MU204PDA.BRM050CP
# ******  FICHIER DES MUTATIONS A TRAITER                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FEDMUT ${DATA}/PXX0/F07.BRM050AP
# ******  FICHIER POUR ETAT IRM055                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IRM055 ${DATA}/PTEM/MU204PDA.IRM055AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM050 
       JUMP_LABEL=MU204PDB
       ;;
(MU204PDB)
       m_CondExec 04,GE,MU204PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES MUTS A ENVOYER AUX MGI  ( DXREBP )                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDD
       ;;
(MU204PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/MU204PDA.BRM050CP
# ******  SEND FIC MUTS A ENVOYER AUX MGI  ( DXREBP )                          
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BRM050DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_27_2 27 PD 2
 /FIELDS FLD_CH_1_26 01 CH 26
 /KEYS
   FLD_CH_1_26 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_27_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PDE
       ;;
(MU204PDE)
       m_CondExec 00,EQ,MU204PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EDITION                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDG
       ;;
(MU204PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/MU204PDA.IRM055AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PDG.IRM055BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_27 01 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PDH
       ;;
(MU204PDH)
       m_CondExec 00,EQ,MU204PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDJ
       ;;
(MU204PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A33} FEXTRAC ${DATA}/PTEM/MU204PDG.IRM055BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/MU204PDJ.IRM055CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MU204PDK
       ;;
(MU204PDK)
       m_CondExec 04,GE,MU204PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDM
       ;;
(MU204PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/MU204PDJ.IRM055CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PDM.IRM055DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PDN
       ;;
(MU204PDN)
       m_CondExec 00,EQ,MU204PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IMU220                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDQ
       ;;
(MU204PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A35} FEXTRAC ${DATA}/PTEM/MU204PDG.IRM055BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A36} FCUMULS ${DATA}/PTEM/MU204PDM.IRM055DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_FileAssign -d NEW,CATLG,DELETE -r 1-260 -t LSEQ -g +1 FEDITION ${DATA}/PXX0/F07.IRM055EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MU204PDR
       ;;
(MU204PDR)
       m_CondExec 04,GE,MU204PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDT
       ;;
(MU204PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PXX0/F07.BRM045AP
# ******  FIC SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MU204PDT.BRM045CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_14 "X"
 /DERIVEDFIELD CST_3_10 "C"
 /DERIVEDFIELD CST_1_6 "996090"
 /FIELDS FLD_CH_54_6 54 CH 6
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_90_1 90 CH 1
 /FIELDS FLD_CH_17_7 17 CH 7
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_48_6 48 CH 6
 /CONDITION CND_1 FLD_CH_54_6 EQ CST_1_6 AND FLD_CH_66_1 NE CST_3_10 AND FLD_CH_90_1 EQ CST_5_14 
 /KEYS
   FLD_CH_48_6 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_17_7 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MU204PDU
       ;;
(MU204PDU)
       m_CondExec 00,EQ,MU204PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * BRM096 : PREPARATION DES MUTATIONS DACEM A ENVOYER VERS BPCS               
# * REPRISE: OUI                                                               
# ********************************************************************         
# AGO      STEP  PGM=IKJEFT01                                                  
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *******  TABLES EN LECTURE                                                   
# RSGB05   FILE  DYNAM=YES,NAME=RSGB05,MODE=I                                  
# RSGB15   FILE  DYNAM=YES,NAME=RSGB15,MODE=I                                  
# RSGA10   FILE  DYNAM=YES,NAME=RSGA10,MODE=I                                  
# RSPT01   FILE  DYNAM=YES,NAME=RSPT01,MODE=I                                  
# *******  FICHIER FDATE                                                       
# FDATE    DATA  CLASS=VAR,PARMS=FDATE                                         
# *******  FICHIER ENTRE                                                       
# FMUMUT   FILE  NAME=BRM045CP,MODE=I                                          
# *******  FICHIER VERS BPCS                                                   
# FRM96    FILE  NAME=BRM096AP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BRM096) PLAN(BRM096)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DU FICHIER ISSU DE BRM096                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# AGT      SORT                                                                
# **************************************                                       
# SORTIN   FILE  NAME=BRM096AP,MODE=I                                          
# SORTOUT  FILE  NAME=FCDECLIP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,3,A,30,6,A,9,11,A),FORMAT=CH                                 
#  INCLUDE COND=(16,4,CH,GE,C'0000',AND,16,4,CH,LE,C'9999')                    
#  RECORD TYPE=F,LENGTH=38                                                     
#          DATAEND                                                             
# ********************************************************************         
#  EASYTREAVE POUR METTRE LE FICHIER                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# AGY      STEP  PGM=EZTPA00                                                   
# SYSPRINT REPORT SYSOUT=X                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# ******   FICHIER A DESTINATION DE L'AS400                                    
# FILEA    FILE  NAME=FCDECLIP,MODE=I                                          
# ******   FICHIER A DESTINATION DE KESAGIE SANS LES ZONES PACK�ES             
# SORTIE   FILE  NAME=FCDECLOP,MODE=O                                          
# IMPRIM   REPORT SYSOUT=T                                                     
# SYSOUT   REPORT SYSOUT=T                                                     
# SYSIN    DATA  *                                                             
# FILE FILEA                                                                   
#   AFIL1         1  19   A                                                    
#   ACODIC       20   7   A                                                    
#   AQTE         27   3   P                                                    
#   AFIL2        30   6   A                                                    
#   AQTEI        36   3   P                                                    
# FILE SORTIE                                                                  
#   BFIL1         1  19   A                                                    
#   BCODIC       20   7   A                                                    
#   BQTE         27   5   N                                                    
#   BFIL2        32   6   A                                                    
#   BCODIC2      38  15   A                                                    
#   BQTEI        53   5   N                                                    
# *                                                                            
# JOB INPUT FILEA                                                              
#    BFIL1    = AFIL1                                                          
#    BCODIC   = ACODIC                                                         
#    BQTE     = AQTE                                                           
#    BQTEI    = AQTEI                                                          
#    BFIL2    = AFIL2                                                          
#    BCODIC2  = ACODIC                                                         
#    PUT SORTIE                                                                
#           DATAEND                                                            
# ********************************************************************         
#  ENVOI VERS GATEWAY DE PROD FICHIER DES MUTATIONS                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# AHD      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FCDECLOP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI VERS GATEWAY DE RECETTE FICHIER DES MUTATIONS                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AHI      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPPRO,                                                           
#      IDF=GENERIX,                                                            
#      FNAME=":FCDECLOP""(0),                                                  
#      NFNAME=DAYORD                                                           
#          DATAEND                                                             
# ********************************************************************         
#  CREATION DE L'ETAT DANS EOS                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PDX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=MU204PDX
       ;;
(MU204PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A38} SYSUT1 ${DATA}/PXX0/F07.IRM055EP
       m_OutputAssign -c 9 -w IRM055 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=MU204PDY
       ;;
(MU204PDY)
       m_CondExec 00,EQ,MU204PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ********************************************************************         
# AHS      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=IRM055EP                                                            
#          DATAEND                                                             
# ********************************************************************         
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   AJOUT STEP POUR JALON ATOS                                                 
#   PERMET DE VERIFIER QUE LES JOBS NE SONT PAS EN RETARD                      
#   REPRISE : OUI SI PROBLEME DANS CE STEP METTRE TERMIN� SOUS PLAN _A          
#   CONDITION QUE CE SOIT LE DERNIER STEP DE LA CHAINE                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MU204PEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=MU204PED
       ;;
(MU204PED)
       m_CondExec ${EXAGJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PED.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MU204PEG PGM=EZACFSM1   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=MU204PEG
       ;;
(MU204PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PEG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MU204PZA
       ;;
(MU204PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MU204PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
