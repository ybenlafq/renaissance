#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VA599D.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDVA599 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/10/04 AT 16.56.42 BY PREPA2                       
#    STANDARDS: P  JOBSET: VA599D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  UNLOAD DES TABLES DE VALOS RTVA00/VA05/VA10/VA15/VA20/VA25/VA30             
#  S = S/SYSTEME                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VA599DA
       ;;
(VA599DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    1999/10/04 AT 16.56.42 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: VA599D                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'SAVE VALOS'                            
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VA599DAA
       ;;
(VA599DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 111 -g +1 SYSREC01 ${DATA}/PVA0/VA599DAA.VA00UNLD
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -g +1 SYSREC02 ${DATA}/PVA0/VA599DAA.VA05UNLD
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SYSREC03 ${DATA}/PVA0/VA599DAA.VA10UNLD
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 SYSREC04 ${DATA}/PVA0/VA599DAA.VA15UNLD
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SYSREC05 ${DATA}/PVA0/VA599DAA.VA20UNLD
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 SYSREC06 ${DATA}/PVA0/VA599DAA.VA25UNLD
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SYSREC07 ${DATA}/PVA0/VA599DAA.VA30UNLD
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  LOAD DE LA TABLE STVA00 SAUVE DE LA TABLE RTVA00                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAD
       ;;
(VA599DAD)
       m_CondExec ${EXAAF},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A1} SYSREC ${DATA}/PVA0/VA599DAA.VA00UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAD_STVA00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAE
       ;;
(VA599DAE)
       m_CondExec 04,GE,VA599DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE STVA05 SAUVE DE LA TABLE RTVA05                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAG
       ;;
(VA599DAG)
       m_CondExec ${EXAAK},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PVA0/VA599DAA.VA05UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAG_STVA05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAH
       ;;
(VA599DAH)
       m_CondExec 04,GE,VA599DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE STVA10 SAUVE DE LA TABLE RTVA10                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAJ
       ;;
(VA599DAJ)
       m_CondExec ${EXAAP},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PVA0/VA599DAA.VA10UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAJ_STVA10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAK
       ;;
(VA599DAK)
       m_CondExec 04,GE,VA599DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE STVA15 SAUVE DE LA TABLE RTVA15                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAM
       ;;
(VA599DAM)
       m_CondExec ${EXAAU},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PVA0/VA599DAA.VA15UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAM_STVA15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAN
       ;;
(VA599DAN)
       m_CondExec 04,GE,VA599DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE STVA20 SAUVE DE LA TABLE RTVA20                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAQ
       ;;
(VA599DAQ)
       m_CondExec ${EXAAZ},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A5} SYSREC ${DATA}/PVA0/VA599DAA.VA20UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAQ_STVA20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAR
       ;;
(VA599DAR)
       m_CondExec 04,GE,VA599DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE STVA25 SAUVE DE LA TABLE RTVA25                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAT
       ;;
(VA599DAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A6} SYSREC ${DATA}/PVA0/VA599DAA.VA25UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAT_STVA25.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAU
       ;;
(VA599DAU)
       m_CondExec 04,GE,VA599DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE STVA30 SAUVE DE LA TABLE RTVA30                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VA599DAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VA599DAX
       ;;
(VA599DAX)
       m_CondExec ${EXABJ},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A7} SYSREC ${DATA}/PVA0/VA599DAA.VA30UNLD
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VA599D_VA599DAX_STVA30.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VA599DAY
       ;;
(VA599DAY)
       m_CondExec 04,GE,VA599DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VA599DZA
       ;;
(VA599DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VA599DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
