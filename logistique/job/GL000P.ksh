#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GL000P.ksh                       --- VERSION DU 08/10/2016 13:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGL000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/19 AT 11.41.57 BY BURTECA                      
#    STANDARDS: P  JOBSET: GL000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  BGL000                                                         *            
#   EXTRACTION DES LIVRAISONS EN ATTENTE DEPUIS S-5                            
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GL000PA
       ;;
(GL000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GL000PAA
       ;;
(GL000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE DES LIVRAISONS FOURNISSEURS                                           
#    RSGL00   : NAME=RSGL00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGL00 /dev/null
#  TABLE DES COMMANDES FOURNISSEURS                                            
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#  TABLE DES LIGNES DE COMMANDES ECHEANCEES                                    
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#  FICHIER DES LIVRAISONS EN ATTENTE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FGL000 ${DATA}/PTEM/GL000PAA.GL0000AP
#  DATE A PARTIR DE LAQUELLE ON FAIT L EXTRACTION                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGL000 
       JUMP_LABEL=GL000PAB
       ;;
(GL000PAB)
       m_CondExec 04,GE,GL000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CRITERES DE TRI 1,55                                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GL000PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GL000PAD
       ;;
(GL000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES LIVRAISONS EN ATTENTE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GL000PAA.GL0000AP
#  FICHIER DES LIVRAISONS EN ATTENTE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GL000PAD.GL0000BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_41 15 CH 41
 /FIELDS FLD_BI_1_6 1 CH 6
 /FIELDS FLD_BI_7_8 7 CH 8
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_1_6 ASCENDING,
   FLD_BI_15_41 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GL000PAE
       ;;
(GL000PAE)
       m_CondExec 00,EQ,GL000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGL005                                                         *            
#   LISTE DES LIVRAISONS EN ATTENTE PAR ENTREPOT                               
#   TRAITEMENT DU DIMANCHE                                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GL000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GL000PAG
       ;;
(GL000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES LIVRAISONS EN ATTENTES TRIEES                                   
       m_FileAssign -d SHR -g ${G_A2} FGL005 ${DATA}/PTEM/GL000PAD.GL0000BP
#  TABLE DU PLANNING LIVRAISON ENTREPOT                                        
#    TABLEA   : NAME=RSGF35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR TABLEA /dev/null
#  LISTE DES LIVRAISON EN ATTENTE                                              
       m_OutputAssign -c 9 -w BGL005 IGL005
       m_FileAssign -i FDATE
$FDATE
_end
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGL005 
       JUMP_LABEL=GL000PAH
       ;;
(GL000PAH)
       m_CondExec 04,GE,GL000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GL000PZA
       ;;
(GL000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GL000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
