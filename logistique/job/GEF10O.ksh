#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEF10O.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGEF10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/07/04 AT 16.27.29 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GEF10O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='BURTEC'                                                            
# ********************************************************************         
#  PGM : BEF000                                                                
#  ------------                                                                
#  LISTE COMPLETE DE L'ENCOURS FOURNISSEUR                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEF10OA
       ;;
(GEF10OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GEF10OAA
       ;;
(GEF10OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
#    RSHE00O  : NAME=RSHE00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00O /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ------  FICHIER D'EDITION                                                    
#  IEF000   REPORT SYSOUT=(9,IEF000),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF000 ${DATA}/PXX0/F16.IEF000AO
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 209 -g +1 FEFCSV ${DATA}/PXX0/F96.BEF000AO.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF000 
       JUMP_LABEL=GEF10OAB
       ;;
(GEF10OAB)
       m_CondExec 04,GE,GEF10OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF005                                                                
#  ------------                                                                
#  CREATION DES FICHIERS D'EXTRACTION POUR EDITION                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAD
       ;;
(GEF10OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSEF00   : NAME=RSEF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEF00 /dev/null
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGG50O  : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50O /dev/null
#    RSHE00O  : NAME=RSHE00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHE00O /dev/null
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 FEF010 ${DATA}/PTEM/GEF10OAD.FEF010AO
#                                                                              
# ****** FICHIER D EXTRACT EDITION                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 194 -g +1 FDFCSV ${DATA}/PXX0/F96.BEF005AO.ETAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEF005 
       JUMP_LABEL=GEF10OAE
       ;;
(GEF10OAE)
       m_CondExec 04,GE,GEF10OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEF010AO ENTRANT DANS LE BEF010                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAG
       ;;
(GEF10OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GEF10OAD.FEF010AO
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GEF10OAG.FEF010BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_CH_80_5 80 CH 5
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_72_8 72 CH 8
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_80_5 ASCENDING,
   FLD_CH_72_8 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10OAH
       ;;
(GEF10OAH)
       m_CondExec 00,EQ,GEF10OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF010                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF010 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PV TTC                                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAJ PGM=BEF010     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAJ
       ;;
(GEF10OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A2} FEF010 ${DATA}/PTEM/GEF10OAG.FEF010BO
# ------  EDITION DE L'ETAT IEF010                                             
#  IEF010   REPORT SYSOUT=(9,IEF010),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF010 ${DATA}/PXX0/F16.IEF010AO
       m_ProgramExec BEF010 
# ********************************************************************         
#  TRI DU FICHIER FEF010AO ENTRANT DANS LES PGMS BEF011 ET BEF012              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAM
       ;;
(GEF10OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER ISSU DU BEF005                                               
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GEF10OAD.FEF010AO
# ------  FICHIER TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -g +1 SORTOUT ${DATA}/PTEM/GEF10OAM.FEF010CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_7 65 CH 7
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_PD_98_5 98 PD 5
 /FIELDS FLD_CH_72_6 72 CH 6
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_78_2 78 CH 2
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_CH_51_5 51 CH 5
 /KEYS
   FLD_CH_51_5 ASCENDING,
   FLD_CH_72_6 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_78_2 ASCENDING,
   FLD_CH_65_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5,
    TOTAL FLD_PD_98_5
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GEF10OAN
       ;;
(GEF10OAN)
       m_CondExec 00,EQ,GEF10OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEF011                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF011 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAQ PGM=BEF011     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAQ
       ;;
(GEF10OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A4} FEF010 ${DATA}/PTEM/GEF10OAM.FEF010CO
# ------  EDITION DE L'ETAT IEF010                                             
#  IEF011   REPORT SYSOUT=(9,IEF011),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF011 ${DATA}/PXX0/F16.IEF011AO
       m_ProgramExec BEF011 
# ********************************************************************         
#  PGM : BEF012                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF012 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU MONTANT PROVISIONNE                                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAT PGM=BEF012     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAT
       ;;
(GEF10OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BEF005                                          
       m_FileAssign -d SHR -g ${G_A5} FEF010 ${DATA}/PTEM/GEF10OAM.FEF010CO
# ------  EDITION DE L'ETAT IEF010                                             
#  IEF012   REPORT SYSOUT=(9,IEF012),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF012 ${DATA}/PXX0/F16.IEF012AO
       m_ProgramExec BEF012 
# ********************************************************************         
#  PGM : BEF013                                                                
#  ------------                                                                
#  EDITION DE L'ETAT IEF013 VALORISATION DE L' ENCOURS FOURNISSEUR             
#  AU PRMP                                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OAX PGM=BEF013     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OAX
       ;;
(GEF10OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ------  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER TRIE ISSU DU BFEF005                                         
       m_FileAssign -d SHR -g ${G_A6} FEF010 ${DATA}/PTEM/GEF10OAG.FEF010BO
# ------  EDITION DE L'ETAT IEF013                                             
#  IEF013   REPORT SYSOUT=(9,IEF013),RECFM=FBA,LRECL=133                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IEF013 ${DATA}/PXX0/F16.IEF013AO
       m_ProgramExec BEF013 
# ********************************************************************         
#    CREATION DE L'ETAT DANS EOS                                               
# ********************************************************************         
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OBA PGM=IEBGENER   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OBA
       ;;
(GEF10OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A7} SYSUT1 ${DATA}/PXX0/F16.IEF000AO
       m_OutputAssign -c 9 -w IEF000 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10OBB
       ;;
(GEF10OBB)
       m_CondExec 00,EQ,GEF10OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OBD PGM=IEBGENER   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OBD
       ;;
(GEF10OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A8} SYSUT1 ${DATA}/PXX0/F16.IEF010AO
       m_OutputAssign -c 9 -w IEF010 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10OBE
       ;;
(GEF10OBE)
       m_CondExec 00,EQ,GEF10OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OBG PGM=IEBGENER   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OBG
       ;;
(GEF10OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A9} SYSUT1 ${DATA}/PXX0/F16.IEF011AO
       m_OutputAssign -c 9 -w IEF011 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10OBH
       ;;
(GEF10OBH)
       m_CondExec 00,EQ,GEF10OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OBJ
       ;;
(GEF10OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A10} SYSUT1 ${DATA}/PXX0/F16.IEF012AO
       m_OutputAssign -c 9 -w IEF012 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10OBK
       ;;
(GEF10OBK)
       m_CondExec 00,EQ,GEF10OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEF10OBM PGM=IEBGENER   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OBM
       ;;
(GEF10OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A11} SYSUT1 ${DATA}/PXX0/F16.IEF013AO
       m_OutputAssign -c 9 -w IEF013 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GEF10OBN
       ;;
(GEF10OBN)
       m_CondExec 00,EQ,GEF10OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GEF10OZA
       ;;
(GEF10OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GEF10OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
