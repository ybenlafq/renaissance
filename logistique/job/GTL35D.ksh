#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL35D.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGTL35 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 10.56.52 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL35D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BTL035 : VENTES LIVREES PAR EQUIPE POUBELLE OU                             
#            VENTES NON TOPEES LIVREES                                         
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL35DA
       ;;
(GTL35DA)
#
#GTL35DAQ
#GTL35DAQ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GTL35DAQ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL35DAA
       ;;
(GTL35DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ DES TABLES DE TOURNEES        *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******  TABLE DES VENTES/ADRESSES                                            
#    RSGV02D  : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02D /dev/null
# ******  TABLE DES LIGNES DE VENTES ARTICLES                                  
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
# ******  TOURNEES/DETAILS                                                     
#    RSTL02D  : NAME=RSTL02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02D /dev/null
#                                                                              
# ******  FICHIER VENTES LIVREES PAR EQUIPE POUBELLE                           
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FTL035 ${DATA}/PTEM/GTL35DAA.BTL035AD
# ******  FICHIER VENTES NON TOPEES LIVREES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 FTL036 ${DATA}/PTEM/GTL35DAA.BTL036AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL035 
       JUMP_LABEL=GTL35DAB
       ;;
(GTL35DAB)
       m_CondExec 04,GE,GTL35DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER VENTES LIVREES  PAR EQUIPE POUBELLE                    
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL35DAD
       ;;
(GTL35DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GTL35DAA.BTL035AD
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GTL35DAD.BTL035BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_10 12 CH 10
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_1_8 ASCENDING,
   FLD_CH_12_10 ASCENDING
 /* Record Type = F  Record Length = 113 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL35DAE
       ;;
(GTL35DAE)
       m_CondExec 00,EQ,GTL35DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTL040 : EDITION DES VENTES AVEC EQUIPE POUBELLE (ETAT JTL401)              
#  REPRISE: NON (UTILISER LE BACKOUT)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35DAG PGM=DFSRRC00   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL35DAG
       ;;
(GTL35DAG)
       m_CondExec ${EXAAK},NE,YES 
# AK      IMSSTEP PGM=BTL040,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               LOG=(YES,GTL35DR1)                                             
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL35DR1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
# ******  DATE : JJMMSSAA                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FTL035 ${DATA}/PTEM/GTL35DAD.BTL035BD
# ******  IMPRESSION GENERALISEE EN UPDATE                                     
# DIGVP0   FILE  NAME=DI0000DD,MODE=U,REST=(YES,GTL35DP1)                      
# DIGVIP   FILE  NAME=DI0000ID,MODE=U,REST=NO                                  
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX991.F91.DI0000DD
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX991.F91.DI0000ID
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
# TRPDRVR  DD  DSN=ALSDL2.LOAD.PDSE,DISP=SHR                                   
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL35DAG
#                                                                              
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGD 
# ********************************************************************         
#    CREATION DU FICHIER  POUR LE LOAD DE P991.RTTL08                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL35DAJ
       ;;
(GTL35DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GTL35DAA.BTL036AD
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 SORTOUT ${DATA}/PTL991/F91.RELOAD.TL08RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_30 1 CH 30
 /KEYS
   FLD_CH_1_30 ASCENDING
 /* Record Type = F  Record Length = 37 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL35DAK
       ;;
(GTL35DAK)
       m_CondExec 00,EQ,GTL35DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTTL08                                                   
#    REPRISE :OUI.ATTENTION UN CODE 8 EST ANORMAL                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35DAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GTL35DAM
       ;;
(GTL35DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PTL991/F91.RELOAD.TL08RD
#    RSTL08D  : NAME=RSTL08D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL08D /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL35DAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GTL35D_GTL35DAM_RTTL08.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GTL35DAN
       ;;
(GTL35DAN)
       m_CondExec 04,GE,GTL35DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLESPACE RSTL08D DE LA PDDTL00                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL35DAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GTL35DZA
       ;;
(GTL35DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GTL35DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
