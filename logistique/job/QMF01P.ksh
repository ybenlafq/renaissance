#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QMF01P.ksh                       --- VERSION DU 08/10/2016 13:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQMF01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/29 AT 16.30.26 BY BURTEC2                      
#    STANDARDS: P  JOBSET: QMF01P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BUR0043                                                              
# ********************************************************************         
#   PREPARATION DU FICHIER DE SYSIN POUR LA REQUETE Q014                       
#   LIVRAISON DE REIMS                                                         
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QMF01PA
       ;;
(QMF01PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       QMF01PA=${VENDRED}
       QMF01PC=${LUNDI}
       RUN=${RUN}
       JUMP_LABEL=QMF01PAA
       ;;
(QMF01PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVE MATIN (SA202P)        *                                       
# **************************************                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER SYSIN QMF JOURNALIER (REQUETE Q014)                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 QMF001A ${DATA}/PXX0/F07.QMF014AP
#  FICHIER SYSIN QMF CAS DU VENDREDI (REQUETE Q014)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 QMF001B ${DATA}/PXX0/F07.QMF014BP
#  FICHIER SYSIN QMF JOURNALIER (REQUETE Q177)                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 QMF001C ${DATA}/PXX0/F07.QMF177AP
#  FICHIER SYSIN QMF CAS DU VENDREDI (REQUETE Q177)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 QMF001D ${DATA}/PXX0/F07.QMF177BP
       m_ProgramExec BUR0043 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF177                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAD
       ;;
(QMF01PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.QMF177AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PAE
       ;;
(QMF01PAE)
       m_CondExec 00,EQ,QMF01PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   LISTE DES TOURNEES DE LIVRAISON                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAG
       ;;
(QMF01PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF177 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PAH
       ;;
(QMF01PAH)
       m_CondExec 04,GE,QMF01PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF177                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAJ PGM=IEFBR14    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAJ
       ;;
(QMF01PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF177 (VENDREDI)                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAM
       ;;
(QMF01PAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[QMF01PA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.QMF177BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PAN
       ;;
(QMF01PAN)
       m_CondExec 00,EQ,QMF01PAM ${EXAAU},NE,YES 1,EQ,$[QMF01PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   LISTE DES LIVRAISONS DE REIMS                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAQ
       ;;
(QMF01PAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[QMF01PA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF177 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PAR
       ;;
(QMF01PAR)
       m_CondExec 04,GE,QMF01PAQ ${EXAAZ},NE,YES 1,EQ,$[QMF01PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF177                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAT PGM=IEFBR14    ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAT
       ;;
(QMF01PAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[QMF01PA] 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA REQUETE Q034                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PAX
       ;;
(QMF01PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN Q034 (&&DEBUT='$VDATEJ_ANNMM01' &&FIN='$VDATEJ_ANNMMJJ' FORM=F034
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PAY
       ;;
(QMF01PAY)
       m_CondExec 00,EQ,QMF01PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE Q034                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBA
       ;;
(QMF01PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF034 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PBB
       ;;
(QMF01PBB)
       m_CondExec 04,GE,QMF01PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBD PGM=IEFBR14    ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBD
       ;;
(QMF01PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA REQUETE Q021                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBG
       ;;
(QMF01PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN Q021 (&&JOUR='$VDATEJ1_ANNMMDD' FORM=F021
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PBH
       ;;
(QMF01PBH)
       m_CondExec 00,EQ,QMF01PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE Q021                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBJ
       ;;
(QMF01PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF021 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PBK
       ;;
(QMF01PBK)
       m_CondExec 04,GE,QMF01PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBM PGM=IEFBR14    ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBM
       ;;
(QMF01PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA REQUETE Q006                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBQ
       ;;
(QMF01PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.Q006 (&&JOUR='$VDATEJ_ANNMMDD' &&NSOC='$DIFDEP_1_3' FORM=ADMFIL.F006
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PBR
       ;;
(QMF01PBR)
       m_CondExec 00,EQ,QMF01PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE Q006                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBT
       ;;
(QMF01PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF006 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PBU
       ;;
(QMF01PBU)
       m_CondExec 04,GE,QMF01PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PBX PGM=IEFBR14    ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PBX
       ;;
(QMF01PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA REQUETE Q007                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCA
       ;;
(QMF01PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN Q007 (&&JOUR='$VDATEJ_ANNMMDD' FORM=F007
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PCB
       ;;
(QMF01PCB)
       m_CondExec 00,EQ,QMF01PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE Q007                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCD
       ;;
(QMF01PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF007 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PCE
       ;;
(QMF01PCE)
       m_CondExec 04,GE,QMF01PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCG PGM=IEFBR14    ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCG
       ;;
(QMF01PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA REQUETE Q008                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCJ
       ;;
(QMF01PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN Q008 (&&JOUR='$VDATEJ_ANNMMDD' FORM=F008
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PCK
       ;;
(QMF01PCK)
       m_CondExec 00,EQ,QMF01PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE Q008                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCM
       ;;
(QMF01PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF008 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PCN
       ;;
(QMF01PCN)
       m_CondExec 04,GE,QMF01PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCQ PGM=IEFBR14    ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCQ
       ;;
(QMF01PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA REQUETE Q008P                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCT
       ;;
(QMF01PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN Q008P (&&JOUR='$VDATEJ_ANNMMDD' FORM=F008P
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PCU
       ;;
(QMF01PCU)
       m_CondExec 00,EQ,QMF01PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE Q008P                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PCX
       ;;
(QMF01PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QMF008P DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PCY
       ;;
(QMF01PCY)
       m_CondExec 04,GE,QMF01PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDA PGM=IEFBR14    ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDA
       ;;
(QMF01PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  QMFBATCH : REQUETE Q019                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDD
       ;;
(QMF01PDD)
       m_CondExec ${EXAEV},NE,YES 1,EQ,$[QMF01PC] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF019 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q019 (&&JOUR='$VDATEJ1_ANNMMDD' &&SOC='$DIFDEP_1_3' FORM=F019
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF01PX -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01PDE
       ;;
(QMF01PDE)
       m_CondExec 04,GE,QMF01PDD ${EXAEV},NE,YES 1,EQ,$[QMF01PC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LA QUERY CONTROLE DE CAISSE                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDG
       ;;
(QMF01PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN CON01 (&&JOUR='$VDATEJ_ANNMMDD' FORM=FCON01
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PDH
       ;;
(QMF01PDH)
       m_CondExec 00,EQ,QMF01PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE CONTROLE DE RECUPERATION DE CAISSE                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDJ
       ;;
(QMF01PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w CONTCAIS DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PDK
       ;;
(QMF01PDK)
       m_CondExec 04,GE,QMF01PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY                                        
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDM PGM=IEFBR14    ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDM
       ;;
(QMF01PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF (QHSDEP)                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDQ
       ;;
(QMF01PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN PDARTY.QHSDEP (&&NSOC='$DIFDEP_1_3' FORM=PDARTY.FHSDEP
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PDR
       ;;
(QMF01PDR)
       m_CondExec 00,EQ,QMF01PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DE DEPHASAGE STOCK HS GS10/GS60                                      
#                                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDT
       ;;
(QMF01PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QHSDEP DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PDU
       ;;
(QMF01PDU)
       m_CondExec 04,GE,QMF01PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PDX PGM=IEFBR14    ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PDX
       ;;
(QMF01PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   CREATION DU FICHIER POUR LA QUERY QMF (Q039)                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PEA
       ;;
(QMF01PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN PDARTY.Q039  (&&DJOUR='$VDATEJ_ANNMMDD' FORM=PDARTY.F039
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PEB
       ;;
(QMF01PEB)
       m_CondExec 00,EQ,QMF01PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE DES VENTES DE GSM                                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PED
       ;;
(QMF01PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w Q039 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PEE
       ;;
(QMF01PEE)
       m_CondExec 04,GE,QMF01PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PEG PGM=IEFBR14    ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PEG
       ;;
(QMF01PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
#   LISTE QQF QMUTPEN                                                          
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PEJ
       ;;
(QMF01PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMUTPEN DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QMUTPEN (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.FMUTPEN
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMUTPEN -a QMFPARM DSQPRINT
       JUMP_LABEL=QMF01PEK
       ;;
(QMF01PEK)
       m_CondExec 04,GE,QMF01PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CREATION DU FICHIER POUR LA REQUETE QPRE1                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PEM
       ;;
(QMF01PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i SORTIN
RUN ADMFIL.QPRE1 (&&JOUR='$VDATEJ_ANNMMDD' FORM=ADMFIL.FPRE1
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ SORTOUT ${DATA}/PEX0.F07.QMF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QMF01PEN
       ;;
(QMF01PEN)
       m_CondExec 00,EQ,QMF01PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
# ********************************************************************         
#   REQUETE QPRE1                                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PEQ
       ;;
(QMF01PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c 9 -w QPRE1 DSQPRINT
#                                                                              
# ******  FICHIER DE LA QUERY                                                  
       m_FileAssign -d SHR ENTRE ${DATA}/PEX0.F07.QMF001AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -d "ENTRE" DSQPRINT
       JUMP_LABEL=QMF01PER
       ;;
(QMF01PER)
       m_CondExec 04,GE,QMF01PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEFBR14                                                                     
# ********************************************************************         
#   DELETE DU FICHIER SYSIN DE LA QUERY QMF                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QMF01PET PGM=IEFBR14    ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=QMF01PET
       ;;
(QMF01PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d MOD,DELETE,DELETE DD1 ${DATA}/PEX0.F07.QMF001AP
       m_ProgramExec IEFBR14 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
