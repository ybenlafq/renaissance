#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GTL45Y.ksh                       --- VERSION DU 07/10/2016 22:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGTL45 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/24 AT 11.13.42 BY BURTECA                      
#    STANDARDS: P  JOBSET: GTL45Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  B T L 0 4 5                                                                 
#   CREATION DES BONS DE LIVRAISON                                             
#   REPRISE: OUI SI ABEND                                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GTL45YA
       ;;
(GTL45YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GTL45YAA
       ;;
(GTL45YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    MAJ TABLES DES BONS DE LIVRAISON  *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES PROFILS ET DATE DE DELIVRANCES                           
# ******  POUR LEQUELS LES B.L. SONT A EDITER                                  
       m_FileAssign -d SHR -g +0 FTL001 ${DATA}/PTL945/F45.BTL001BY
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES ARTICLES                                                   
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE GENERALISEES (LPLGE CARSP)                                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE DES MARQUES                                                    
#    RSGA22Y  : NAME=RSGA22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22Y /dev/null
# ******  TABLE DES VENTES/ADRESSES                                            
#    RSGV02Y  : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
# ******  TABLE DES VENTES/REGLEMENT                                           
#    RSGV10Y  : NAME=RSGV10Y,MODE=(U,U) - DYNAM=YES                            
# -X-RSGV10Y  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSGV10Y /dev/null
# ******  TABLE DES LIGNES DE VENTES ARTICLES                                  
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  TABLE DES LIGNES DE PRESTATION                                       
#    RSGV13Y  : NAME=RSGV13Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV13Y /dev/null
# ******  TABLE DES VENTES/CARACTERISTIQUES SPECIFIQUES                        
#    RSGV15Y  : NAME=RSGV15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15Y /dev/null
# ******  TABLE DES VENTES/QUANTITES DISPONIBLES                               
#    RSGV21Y  : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21Y /dev/null
# ******  LIBELLE DEVISE                                                       
#    RSFM04Y  : NAME=RSFM04Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM04Y /dev/null
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 FTL045 ${DATA}/PTL945/F45.TL0045AY
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FTL065 ${DATA}/PTL945/F45.TL0065AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL045 
       JUMP_LABEL=GTL45YAB
       ;;
(GTL45YAB)
       m_CondExec 04,GE,GTL45YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 5 0                                                                 
#   CHARGEMENT DES BONS DE LIVRAISONS DANS LA D BASE IMPRESSION                
#   REPRISE: NON BACKOUT DU STEP LOGGING IMS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45YAD PGM=DFSRRC00   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GTL45YAD
       ;;
(GTL45YAD)
       m_CondExec ${EXAAF},NE,YES 
# AF      IMSSTEP PGM=BTL050,LANG=CBL,TYPE=DBB,PSB=GGD070,UPSI=0000001         
#               REGION=4096K,LOG=(YES,GTL45YR1)                                
#                                                                              
       m_StepLibSet SYS2.DL2420.LOAD.PDSE.ELIM:SYS2.DL2.MLNKMOD.RELINK:CORTEX4.MVS.MLNKMOD:IMSVS.RESLIB
       m_FileAssign -d SHR DFSRESLB ${DATA}/IMSVS.RESLIB
       m_FileAssign -d SHR IMSACB ${DATA}/SYS3.EXPLOIT.ACBS
       m_OutputAssign -c "*" DL2DMPA
       m_OutputAssign -c "*" DL2DMPD
       m_FileAssign -d NEW,CATLG,CATLG -r 1-1916 -g +1 IEFRDER ${DATA}/RECIMS/F99.GTL45YR1
       m_OutputAssign -c "*" DDOTV02
       m_OutputAssign -c "*" TRCEOUT
       m_FileAssign -d SHR TRCEPARM ${DATA}/CORTEX4.P.MTXTFIX5/TRACEDL2
#                                                                              
# ******  FICHIER DES BONS DE LIVRAISON EXTRAIT                                
       m_FileAssign -d SHR -g ${G_A1} FTL045 ${DATA}/PTL945/F45.TL0045AY
# ******  DB IMPRESSION GENERALISEE                                            
# DIGVP0   FILE NAME=DI0000DY,MODE=U,REST=(YES,GTL45YR1)                       
# DIGVIP   FILE NAME=DI0000IY,MODE=U,REST=NO                                   
       m_FileAssign -d SHR DIGVP0 ${DATA}/PEX945.F45.DI0000DY
       m_FileAssign -d SHR DIGVIP ${DATA}/PEX945.F45.DI0000IY
#                                                                              
       m_FileAssign -d SHR TRPDRVR ${DATA}/SYS2.DL2420.LOAD.PDSE.ELIM
       m_FileAssign -d SHR DDITV02 ${DATA}/CORTEX4.P.MTXTFIX1/GTL45YAD
#                                                                              
# ******  FDATE JJMMSSAA DATE D EDITION DES BL                                 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR DFSVSAMP ${DATA}/CORTEX4.P.MTXTFIX2/DFSVSAMP
       m_ProgramExec -b TPIGY 
# ********************************************************************         
#                TRI DES LIVRAISONS DU JOUR                                    
#               ****************************                                   
#  (DDELIV/NLIEU/NVENTE)                                                       
#  SORT FIELDS=(1,8,A,9,3,A,12,7,A),FORMAT=CH                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GTL45YAG
       ;;
(GTL45YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTL945/F45.TL0065AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 SORTOUT ${DATA}/PTL0/F45.TL0065BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_7 12 CH 7
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_9_3 9 CH 3
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_7 ASCENDING
 /* Record Type = F  Record Length = 120 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GTL45YAH
       ;;
(GTL45YAH)
       m_CondExec 00,EQ,GTL45YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  B T L 0 6 5                                                                 
#   CREATION DE LA LISTE DES LIVRAISONS PREVUES                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GTL45YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GTL45YAJ
       ;;
(GTL45YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES LIVRAISONS PREVUES                                       
       m_FileAssign -d SHR -g ${G_A3} FTL065 ${DATA}/PTL0/F45.TL0065BY
# ******  LISTE DES LIVRAISONS PREVUES                                         
       m_OutputAssign -c 9 -w ITL065 ITL065
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL065 
       JUMP_LABEL=GTL45YAK
       ;;
(GTL45YAK)
       m_CondExec 04,GE,GTL45YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
