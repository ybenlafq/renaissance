#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GS100P.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGS100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/22 AT 09.42.47 BY BURTECA                      
#    STANDARDS: P  JOBSET: GS100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTGS10 RTGS30 RTGS60 RTGS70 RTGS40 RTHV12 RTMQ15         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GS100PA
       ;;
(GS100PA)
#
#GS100PAA
#GS100PAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GS100PAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GS100PAD
       ;;
(GS100PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  PARAMETRAGE PGM: SOUS TABLE NCGFC POUR 'GS107'                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  MARQUES                                                              
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
# ******  CODES VENDEURS                                                       
#    RSGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV31 /dev/null
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER ENCAISSEMENT VENANT DE EL001P                                
       m_FileAssign -d SHR -g +0 FEMPORTE ${DATA}/PXX0/F07.BGS105AP
#                                                                              
# ******  FICHIER SERVANT A LA MAJ DES STOCKS                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ -g +1 FGS100 ${DATA}/PTEM/GS100PAD.BGS107AP
# ******  FICHIER DES ANOMALIES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FEL030 ${DATA}/PTEM/GS100PAD.BGS107BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS107 
       JUMP_LABEL=GS100PAE
       ;;
(GS100PAE)
       m_CondExec 04,GE,GS100PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ100 : RECUPERATION DES MESSAGES MQ SERIES POUR GENERER DES               
#           FICHIERS EN FOCNTION DE LA CARTE FCFONC FIC STOCK ET STAT          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GS100PAG
       ;;
(GS100PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRAGE PGM                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GS100PAG
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GS100PAG
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FTS054 ${DATA}/PXX0/F07.BNM154AP
# ******  FICHIER LG 116                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FTS056 ${DATA}/PXX0/F07.BNM156AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GS100PAH
       ;;
(GS100PAH)
       m_CondExec 04,GE,GS100PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GS100PAJ
       ;;
(GS100PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.BNM154AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100PAJ.BNM154BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_17 1 CH 17
 /FIELDS FLD_BI_18_8 18 CH 8
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100PAK
       ;;
(GS100PAK)
       m_CondExec 00,EQ,GS100PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE MQSERIES QUI PASSE A LG 113                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GS100PAM
       ;;
(GS100PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.BNM156AP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100PAM.BNM156BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_17 1 CH 17
 /FIELDS FLD_BI_18_8 18 CH 8
 /KEYS
   FLD_BI_18_8 ASCENDING,
   FLD_BI_1_17 ASCENDING
 /* Record Type = F  Record Length = 116 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100PAN
       ;;
(GS100PAN)
       m_CondExec 00,EQ,GS100PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GS100PAQ
       ;;
(GS100PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GS100PAD.BGS107AP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100PAQ.BGS107CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4  "   "
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_1_88 01 CH 88
 /FIELDS FLD_CH_89_24 89 CH 24
 /FIELDS FLD_CH_18_8 18 CH 8
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = (113,,116) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_88,CST_1_4,FLD_CH_89_24
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GS100PAR
       ;;
(GS100PAR)
       m_CondExec 00,EQ,GS100PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR SUR DATE DE VENTE,SOCIETE,LIEU,           
#  ZONE DE PRIX,CAISSE,TERMINAL,TRANSACT�                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GS100PAT
       ;;
(GS100PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GS100PAQ.BGS107CP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GS100PAM.BNM156BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100PAT.BGS100BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_8 18 CH 8
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_18_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = 116 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100PAU
       ;;
(GS100PAU)
       m_CondExec 00,EQ,GS100PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES STOCKS DU JOUR SUR DATE DE VENTE,SOCIETE,LIEU,           
#  ZONE DE PRIX,CAISSE,TERMINAL,TRANSACT�                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GS100PAX
       ;;
(GS100PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# SORTIN   FILE  NAME=BGS107CP,MODE=I                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GS100PAJ.BNM154BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100PAX.BGS122AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_96_8 96 CH 8
 /KEYS
   FLD_CH_96_8 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100PAY
       ;;
(GS100PAY)
       m_CondExec 00,EQ,GS100PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGS122 :                                                                    
#  REPRISE : OUI SI FIN ANORMALE .                                             
#           SINON  FAIRE 1 RECOVER AVEC LE RBA DU QUIESCE 1� STEP              
#            POUR : PRDMQ00.RSMQ15R ET                                         
#                 : PRDGS00.RSGS30R ET PRDGS00.RSGS40R                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GS100PBA
       ;;
(GS100PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE (SOUS TABLES PARAMETRES)                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  RELATION ARTICLE ET ZONE DE PRIX                                     
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  TABLE DES STOCKS                                                     
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER LG 113                                                       
       m_FileAssign -d SHR -g ${G_A7} FGS100 ${DATA}/PTEM/GS100PAX.BGS122AP
# ******  FICHIER DES ANOMALIES LG 110                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 FGS103 ${DATA}/PTEM/GS100PBA.BGS103AP
#                                                                              
# ******  STOCK MAGASIN                                                        
#    RSGS30   : NAME=RSGS30,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  MOUVEMENT DE STOCK                                                   
#    RSGS40   : NAME=RSGS40,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGS122 
       JUMP_LABEL=GS100PBB
       ;;
(GS100PBB)
       m_CondExec 04,GE,GS100PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU FICHIER DES ANOMALIES TRAITEMENT DU DES CAISSES                 
#  FEL030 = FICHIER ANOS DU PROG BGS107                                        
#  FGS103 = FICHIER ANOS DU PROG BGS103                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GS100PBD
       ;;
(GS100PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GS100PAD.BGS107BP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GS100PBA.BGS103AP
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GS100PBD.BGS106AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_19_3 19 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_19_3 ASCENDING,
   FLD_CH_22_5 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 110 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GS100PBE
       ;;
(GS100PBE)
       m_CondExec 00,EQ,GS100PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGS106                                                                
# ********************************************************************         
#  EDITION DES ANOMALIES DU TRAITEMENT DES CAISSES (BGS100+BGS103)             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PBG PGM=BGS106     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GS100PBG
       ;;
(GS100PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER D'EDITION                                                    
       m_FileAssign -d SHR -g ${G_A10} FEL032 ${DATA}/PTEM/GS100PBD.BGS106AP
# ******  EDITION DES ANOMALIES                                                
       m_OutputAssign -c "*" IFEL031
       m_ProgramExec BGS106 
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   AJOUT STEP POUR JALON ATOS                                                 
#   PERMET DE VERIFIER QUE LES JOBS NE SONT PAS EN RETARD                      
#   REPRISE : OUI SI PROBLEME DANS CE STEP METTRE TERMIN� SOUS PLAN _A          
#   CONDITION QUE CE SOIT LE DERNIER STEP DE LA CHAINE                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GS100PBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GS100PZA
       ;;
(GS100PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GS100PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
