      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BMU230.                                                      
      *===============================================================*         
      *                                                                         
      *   REAPPROVISIONNEMENT DES STOCKS MAGASINS                               
      *                                                                         
      *===============================================================*         
      *                                                                         
      * CONSTITUTION D UN FICHIER CONTENANT LA NOTION DE 80/20                  
      * UNIQUEMENT SUR LE 1ER ENRGT REPRESENTANT 20% DES VENTES                 
      * AFIN DE POUVOIR TRAITER CORRECTEMENT LE FICHIER PAR LE                  
      * GENERATEUR                                                              
      *===============================================================*         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FMU230   ASSIGN TO FMU230.                                   
      *--                                                                       
            SELECT FMU230   ASSIGN TO FMU230                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FMU231   ASSIGN TO FMU231.                                   
      *--                                                                       
            SELECT FMU231   ASSIGN TO FMU231                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *--- FICHIER ENTREE                                                       
       FD  FMU230 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENRGT-FMU230 PIC X(512).                                             
      *--- FICHIER CUMUL                                                        
       FD  FMU231 RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.            
       01  ENRGT-FMU231 PIC X(512).                                             
       WORKING-STORAGE SECTION.                                                 
      ****************************************************************          
      *    COMPTEURS                                                            
      ****************************************************************          
       77  CPT-FMU230-LUS         PIC S9(7) COMP-3 VALUE ZERO.                  
       77  CPT-FMU231-ECRITS      PIC S9(7) COMP-3 VALUE ZERO.                  
       77  W-8020-ADD             PIC S9(7) COMP-3 VALUE ZERO.                  
       77  PIC-EDIT               PIC Z(6)9.                                    
       01  W-FMU230               PIC X VALUE '0'.                              
           88 OK-FMU230           VALUE '1'.                                    
           88 FIN-FMU230          VALUE '3'.                                    
      ***************************************************************           
      * ZONE DE RUPTURE                                                         
      ***************************************************************           
       01  W-RUPTURE.                                                           
           05  W-NSOCIETE            PIC X(3).                                  
           05  W-SEQFAM              PIC S9(5) COMP-3.                          
      *    05  W-CVMARKETING         PIC X(5) OCCURS 3.                         
           05  W-LAGREGATED          PIC X(20).                                 
           05  W-RUP-8020            PIC X.                                     
       01  W-OLD-RUPTURE.                                                       
           05  W-OLD-NSOCIETE        PIC X(3).                                  
           05  W-OLD-SEQFAM          PIC S9(5) COMP-3.                          
      *    05  W-OLD-CVMARKETING     PIC X(5) OCCURS 3.                         
           05  W-OLD-LAGREGATED      PIC X(20).                                 
           05  W-OLD-RUP-8020            PIC X.                                 
      ***************************************************************           
      * ZONE D APPEL AU MODULE D ABEND                                          
      ***************************************************************           
           COPY  ABENDCOP.                                                      
           COPY SYBWDIV0.                                                       
           COPY SYKWSQ10.                                                       
      *                                                               * 00001610
      *****************************************************************         
      *  DESCRIPTION DES ZONES D'ABEND                                *         
      *****************************************************************         
       01  FILLER    PIC X(16)    VALUE  '*** Z-INOUT ****'.                    
       01  Z-INOUT   PIC X(4096)  VALUE  SPACE.                                 
      *****************************************************************         
           COPY SYBWERRO.                                                       
           COPY IMU220 REPLACING ==:IMU220:== BY ==IMU220==.                    
      *===============================================================*         
       PROCEDURE DIVISION.                                                      
      *===============================================================*         
       MODULE-BMU230                  SECTION.                                  
           PERFORM ENTREE.                                                      
           PERFORM TRAITEMENT.                                                  
           PERFORM SORTIE.                                                      
       FIN-MODULE-BMU230.   EXIT.                                               
      *===============================================================*         
      *    - LECTURE DES PARAMETRES                                             
      *===============================================================*         
       ENTREE SECTION.                                                          
           OPEN INPUT  FMU230.                                                  
           OPEN OUTPUT FMU231.                                                  
       FIN-ENTREE. EXIT.                                                        
      *****************************************************************         
      *    TRAITEMENT                                                           
      *****************************************************************         
       TRAITEMENT SECTION.                                                      
      *****************************************************************         
      *    BOUCLE DE LECTURE DU FICHIER FMU230                                  
      *****************************************************************         
           PERFORM LECTURE-FMU230.                                              
           PERFORM UNTIL FIN-FMU230                                             
              PERFORM TRAITEMENT-ARTICLE                                        
              PERFORM LECTURE-FMU230                                            
           END-PERFORM.                                                         
       FIN-TRAITEMENT. EXIT.                                                    
      *****************************************************************         
      *   LECTURE DE FMU230                                                     
      *****************************************************************         
       LECTURE-FMU230             SECTION.                                      
           READ FMU230                                                          
                AT END DISPLAY 'FIN FMU230'                                     
                SET FIN-FMU230 TO TRUE.                                         
           IF NOT FIN-FMU230                                                    
              ADD 1 TO CPT-FMU230-LUS                                           
              SET OK-FMU230 TO TRUE                                             
           END-IF.                                                              
       FIN-LECTURE-FMU230.  EXIT.                                               
      *****************************************************************         
      *    POUR CHAQUE ENREGISTREMENT LU:                                       
      *    - ON DETERMINE SI ON A UNE RUPTURE SUR UN DES CRTITERES :            
      *      - SOCIETE                                                          
      *      - WSEQFAM                                                          
      *      - CVMARKETING                                                      
      *      AFIN D AFFECTER LE RANG PAR SEGMENT DE MARCH�                      
      *****************************************************************         
       TRAITEMENT-ARTICLE             SECTION.                                  
           INITIALIZE ENRGT-FMU231.                                             
           MOVE ENRGT-FMU230 TO DSECT-IMU220.                                   
           MOVE IMU220-NSOCIETE  TO W-NSOCIETE.                                 
           MOVE IMU220-WSEQFAM   TO W-SEQFAM.                                   
      *    MOVE IMU220-LVMARKET1 TO W-CVMARKETING(1).                           
      *    MOVE IMU220-LVMARKET2 TO W-CVMARKETING(2).                           
      *    MOVE IMU220-LVMARKET3 TO W-CVMARKETING(3).                           
           MOVE IMU220-LAGREGATED TO W-LAGREGATED.                              
           MOVE IMU220-W8020-RUP TO W-RUP-8020.                                 
           IF W-OLD-RUPTURE NOT = W-RUPTURE                                     
              MOVE W-RUPTURE TO W-OLD-RUPTURE                                   
           ELSE                                                                 
              MOVE SPACES TO IMU220-W8020-RUP                                   
           END-IF.                                                              
           IF IMU220-LAGREGATED = IMU220-LFAM                                   
              MOVE SPACE TO IMU220-WFAM                                         
           ELSE                                                                 
              MOVE 1 TO     IMU220-WFAM                                         
           END-IF                                                               
           PERFORM ECRITURE-FMU231.                                             
       FIN-TRAITEMENT-ARTICLE. EXIT.                                            
      ****************************************************************          
      *    ECRITURE DE FMU231                                                   
      ****************************************************************          
       ECRITURE-FMU231  SECTION.                                                
           MOVE +512 TO DSECT-IMU220-LONG.                                      
           WRITE ENRGT-FMU231 FROM DSECT-IMU220.                                
           ADD 1 TO CPT-FMU231-ECRITS.                                          
       FIN-ECRITURE-FMU231.  EXIT.                                              
      *===============================================================*         
       SORTIE      SECTION.                                                     
      *===============================================================*         
           CLOSE FMU230 FMU231.                                                 
           MOVE CPT-FMU230-LUS TO PIC-EDIT.                                     
           DISPLAY '        ENRGTS LUS    SUR FMU230 : ' PIC-EDIT.              
           MOVE CPT-FMU231-ECRITS TO PIC-EDIT.                                  
           DISPLAY '        ENRGTS ECRITS SUR FMU231 : ' PIC-EDIT.              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-SORTIE. EXIT.                                                        
      *****************************************************************         
      *       A B A N D O N    B A T C H                              *         
      *****************************************************************         
           COPY SYBCERRO.                                                       
