      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.    BMU221                                            00020000
       DATE-COMPILED.                                                   00030000
       AUTHOR. DSA013.                                                  00040000
      *------------------------------------------------------------*    00050000
      *                PROGRAMME D'EDITION DE L'ETAT               *    00060000
      *                      REAPPRO NRM IMU220                    *    00070000
      *                                                            *    00080000
      * DATE CREATION : 24/09/03                                   *    00090000
      *------------------------------------------------------------*    00100000
LM0906*  DATE       : 09/2006           - AUTEUR : DSA031          *    00110001
      *  OBJET      : DECOUPER L'ETAT PAR FAMILLE (A,B,C,D,E,F)    *    00120001
      *               UN REPORT PAR FICHIER                        *    00130001
      *------------------------------------------------------------*    00140001
PM0105*  DATE       : 01/2005           - AUTEUR : DSA054          *    00150001
      *  OBJET      : ALIMENTER LE STOCK DEPOT POUR LES CODIC      *    00160001
      *               REMPLACÚS                                    *    00170001
      *------------------------------------------------------------*    00180000
MH0606*  DATE       : 06/2006           - AUTEUR : DSA035-MH       *    00190000
"     *  OBJET      : MODIFICATION CODE ETAT 'IMU220' EN DUR PAR   *    00200000
"     *               CODE SEGMENT DEFINI POUR CHAQUE FAMILLE      *    00210000
MH0606**************************************************************    00220000
       ENVIRONMENT DIVISION.                                            00230000
       CONFIGURATION SECTION.                                           00240000
       SPECIAL-NAMES.                                                   00250000
           DECIMAL-POINT IS COMMA.                                      00260000
       INPUT-OUTPUT SECTION.                                            00270000
       FILE-CONTROL.                                                    00280000
      *                                                                 00290000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FMU220  ASSIGN TO FMU220                              00300001
LM0906*            FILE STATUS IS STA-FMU220.                           00310001
      *--                                                                       
           SELECT FMU220  ASSIGN TO FMU220                                      
                   FILE STATUS IS STA-FMU220                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE                               00320001
LM0906*            FILE STATUS IS STA-FDATE.                            00330001
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
                   FILE STATUS IS STA-FDATE                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
LM0906*    SELECT IMU220A ASSIGN TO IMU220A.                            00340001
      *--                                                                       
           SELECT IMU220A ASSIGN TO IMU220A                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
LM0906*    SELECT IMU220B ASSIGN TO IMU220B.                            00350001
      *--                                                                       
           SELECT IMU220B ASSIGN TO IMU220B                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
LM0906*    SELECT IMU220C ASSIGN TO IMU220C.                            00360001
      *--                                                                       
           SELECT IMU220C ASSIGN TO IMU220C                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
LM0906*    SELECT IMU220D ASSIGN TO IMU220D.                            00370001
      *--                                                                       
           SELECT IMU220D ASSIGN TO IMU220D                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
LM0906*    SELECT IMU220E ASSIGN TO IMU220E.                            00380001
      *--                                                                       
           SELECT IMU220E ASSIGN TO IMU220E                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
LM0906*    SELECT IMU220F ASSIGN TO IMU220F.                            00390001
      *--                                                                       
           SELECT IMU220F ASSIGN TO IMU220F                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00400000
       DATA DIVISION.                                                   00410000
       FILE SECTION.                                                    00420000
      *                                                                 00430000
      *---------------------------------------------------------------* 00440000
      *  D E F I N I T I O N    F I C H I E R    F D A T E            * 00450000
      *---------------------------------------------------------------* 00460000
      *                                                                 00470000
       FD  FDATE                                                        00480000
           RECORDING F                                                  00490000
           BLOCK 0 RECORDS                                              00500000
           LABEL RECORD STANDARD.                                       00510000
       01  FDATE-ENREG.                                                 00520000
           02 FDATE-JJ              PIC X(02).                          00530000
           02 FDATE-MM              PIC X(02).                          00540000
           02 FDATE-SS              PIC X(02).                          00550000
           02 FDATE-AA              PIC X(02).                          00560000
           02 FILLER                PIC X(72).                          00570000
      *                                                                 00580000
      *---------------------------------------------------------------* 00590000
      *  D E F I N I T I O N    F I C H I E R    F G P 8 8 9          * 00600000
      *---------------------------------------------------------------* 00610000
      *                                                                 00620000
       FD  FMU220                                                       00630000
           RECORDING F                                                  00640000
           BLOCK 0 RECORDS                                              00650000
           LABEL RECORD STANDARD.                                       00660000
       01  ENREG-FMU220.                                                00670001
           05  FILLER         PIC X(006).                               00680001
           05  FMU220-CODSEGM PIC X(001).                               00690001
           05  FILLER         PIC X(505).                               00700001
      *                                                                 00710000
      *---------------------------------------------------------------* 00720000
      *  D E F I N I T I O N    F I C H I E R    I G P 8 7 0          * 00730000
      *---------------------------------------------------------------* 00740000
      *                                                                 00750000
       FD  IMU220A                                                      00760001
           RECORDING F                                                  00770000
           BLOCK 0 RECORDS                                              00780000
           LABEL RECORD STANDARD.                                       00790000
       01  LIGNE-IMU220A.                                               00800001
           02 SAUT-A     PIC X.                                         00810001
           02 LIGNE-A    PIC X(132).                                    00820001
      *                                                                 00830001
       FD  IMU220B                                                      00840001
           RECORDING F                                                  00850001
           BLOCK 0 RECORDS                                              00860001
           LABEL RECORD STANDARD.                                       00870001
       01  LIGNE-IMU220B.                                               00880001
           02 SAUT-B     PIC X.                                         00890001
           02 LIGNE-B    PIC X(132).                                    00900001
      *                                                                 00910001
       FD  IMU220C                                                      00920001
           RECORDING F                                                  00930001
           BLOCK 0 RECORDS                                              00940001
           LABEL RECORD STANDARD.                                       00950001
       01  LIGNE-IMU220C.                                               00960001
           02 SAUT-C     PIC X.                                         00970001
           02 LIGNE-C    PIC X(132).                                    00980001
      *                                                                 00990001
       FD  IMU220D                                                      01000001
           RECORDING F                                                  01010001
           BLOCK 0 RECORDS                                              01020001
           LABEL RECORD STANDARD.                                       01030001
       01  LIGNE-IMU220D.                                               01040001
           02 SAUT-D     PIC X.                                         01050001
           02 LIGNE-D    PIC X(132).                                    01060001
      *                                                                 01070001
       FD  IMU220E                                                      01080001
           RECORDING F                                                  01090001
           BLOCK 0 RECORDS                                              01100001
           LABEL RECORD STANDARD.                                       01110001
       01  LIGNE-IMU220E.                                               01120001
           02 SAUT-E     PIC X.                                         01130001
           02 LIGNE-E    PIC X(132).                                    01140001
      *                                                                 01150001
       FD  IMU220F                                                      01160001
           RECORDING F                                                  01170001
           BLOCK 0 RECORDS                                              01180001
           LABEL RECORD STANDARD.                                       01190001
       01  LIGNE-IMU220F.                                               01200001
           02 SAUT-F     PIC X.                                         01210001
           02 LIGNE-F    PIC X(132).                                    01220001
      *                                                                 01230001
      *---------------------------------------------------------------* 01240001
      *                 W O R K I N G  -  S T O R A G E               * 01250000
      *---------------------------------------------------------------* 01260000
      *                                                                 01270000
       WORKING-STORAGE SECTION.                                         01280000
       77  STA-FMU220       PIC 99.                                     01290001
       77  STA-FDATE        PIC 99.                                     01300001
      *                                                                 01310001
       77  WS-CODSEGM-PREV  PIC X.                                      01320001
       77  WS-CODSEGM-TEST  PIC X.                                      01330001
                                                                        01340001
                                                                        01350001
       01  LIGNE-IMU220.                                                01360001
           02 SAUT           PIC X.                                     01370001
           02 LIGNE          PIC X(132).                                01380001
                                                                        01390001
        COPY ABENDCOP.                                                  01400000
      *                                                                 01410000
        COPY WORKDATC.                                                  01420000
        COPY  SYBWDIV0.                                                 01430000
      *                                                                 01440000
      *---------------------------------------------------------------* 01450000
      *           D E S C R I P T I O N    D E S    Z O N E S         * 01460000
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      * 01470000
      *---------------------------------------------------------------* 01480000
      *                                                                 01490000
                                                                        01500000
       01  W-DATE.                                                      01510000
           03  DATJMSA.                                                 01520000
               05 JJ                  PIC  XX.                          01530000
               05 MM                  PIC  XX.                          01540000
               05 SS                  PIC  XX.                          01550000
               05 AA                  PIC  XX.                          01560000
           03  W-TIME.                                                  01570000
               05 HH                  PIC  XX.                          01580000
               05 MN                  PIC  XX.                          01590000
               05 FILLER              PIC  X(14).                       01600000
       01  JOUR-SEM PIC X.                                              01610000
                                                                        01620000
       01  W-DATE-TAMP.                                                 01630000
               05 SS                  PIC  XX.                          01640000
               05 AA                  PIC  XX.                          01650000
               05 MM                  PIC  XX.                          01660000
               05 JJ                  PIC  XX.                          01670000
      *                                                                 01680000
       01  CTR-FMU220  PIC 9(9) VALUE 0.                                01690000
      *                                                                 01700000
       01  FLAG-FICHIER PIC X VALUE SPACE.                              01710000
           88  EOF      VALUE 'F'.                                      01720000
                                                                        01730000
       01  WEDITIOX                  PIC X(8)            VALUE SPACE.   01740000
                                                                        01750000
      *                                                                 01760000
      *ENREGISTREMENT COURANT                                           01770000
              COPY IMU220 REPLACING ==:IMU220:== BY ==FMU220==.         01780000
                                                                        01790000
      *                                                                 01800000
      *ENREGISTREMENT PRECEDENT                                         01810000
              COPY IMU220 REPLACING ==:IMU220:== BY ==W-FMU220==.       01820000
                                                                        01830000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01 W-4S                                   PIC  S9(5) COMP.      01840000
      *--                                                                       
        01 W-4S                                   PIC  S9(5) COMP-5.            
      *}                                                                        
        01 W-PRIX                                 PIC  9(6).            01850000
      *--------------------------- CUMUL T10  --------------------------01860000
        01 W-T10.                                                       01870000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOT4S                    PIC S9(5) COMP.       01880000
      *--                                                                       
               10  W-T10-TOT4S                    PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTS-4                   PIC S9(5) COMP.       01890000
      *--                                                                       
               10  W-T10-TOTS-4                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTS-3                   PIC S9(5) COMP.       01900000
      *--                                                                       
               10  W-T10-TOTS-3                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTS-2                   PIC S9(5) COMP.       01910000
      *--                                                                       
               10  W-T10-TOTS-2                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTS-1                   PIC S9(5) COMP.       01920000
      *--                                                                       
               10  W-T10-TOTS-1                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTPRO                   PIC S9(5) COMP.       01930000
      *--                                                                       
               10  W-T10-TOTPRO                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTMAX                   PIC S9(5) COMP.       01940000
      *--                                                                       
               10  W-T10-TOTMAX                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTMAG                   PIC S9(5) COMP.       01950000
      *--                                                                       
               10  W-T10-TOTMAG                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTATT                   PIC S9(5) COMP.       01960000
      *--                                                                       
               10  W-T10-TOTATT                   PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TOTMUT-ATT               PIC S9(5) COMP.       01970000
      *--                                                                       
               10  W-T10-TOTMUT-ATT               PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-DIFF                     PIC S9(5) COMP.       01980000
      *--                                                                       
               10  W-T10-DIFF                     PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-QSTOCK-PRO2              PIC S9(5) COMP.       01990000
      *--                                                                       
               10  W-T10-QSTOCK-PRO2              PIC S9(5) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-QSTOCK-MAX2              PIC S9(5) COMP.       02000000
      *--                                                                       
               10  W-T10-QSTOCK-MAX2              PIC S9(5) COMP-5.             
      *}                                                                        
      *                                                                 02010000
       01  CTR-PAGE     PIC 9(4) VALUE 0.                               02020000
       01  CTR-LIGNE    PIC 99   VALUE 0.                               02030000
       01  MAX-LIGNE    PIC 99   VALUE 58.                              02040000
       01  MAX-LIGNE1   PIC 99   VALUE 53.                              02050000
       01  MAX-LIGNE2   PIC 99   VALUE 56.                              02060000
           EJECT                                                        02070000
      *                                                                 02080000
       01  IMU220-LIGNES.                                               02090000
      *--------------------------- LIGNE E00  --------------------------02100000
           05  IMU220-E00.                                              02110000
MH             10  IMU220-SEGMENT                 PIC X(7).             02120000
MH             10  FILLER                         PIC X(51) VALUE       02130000
           '                                       P R E P A R '.       02140000
               10  FILLER                         PIC X(47) VALUE       02150000
           'A T I O N   D E   M U T A T I O N     EDITE LE '.           02160000
               10  IMU220-E00-DATEDITE.                                 02170000
                   15 JJ                          PIC XX.               02180000
                   15 FILLER                      PIC X VALUE '/'.      02190000
                   15 MM                          PIC XX.               02200000
                   15 FILLER                      PIC X VALUE '/'.      02210000
                   15 SS                          PIC XX.               02220000
                   15 AA                          PIC XX.               02230000
               10  FILLER                         PIC X(03) VALUE       02240000
           ' A '.                                                       02250000
               10  IMU220-E00-HEUREDITE           PIC 99.               02260000
               10  FILLER                         PIC X VALUE 'H'.      02270000
               10  IMU220-E00-MINUEDITE           PIC 99.               02280000
               10  FILLER                         PIC X(06) VALUE       02290000
           ' PAGE '.                                                    02300000
               10  IMU220-E00-NOPAGE              PIC ZZZ.              02310000
      *--------------------------- LIGNE E05  --------------------------02320000
           05  IMU220-E05.                                              02330000
               10  FILLER                         PIC X(10) VALUE       02340000
           'MAGASIN : '.                                                02350000
               10  IMU220-E05-NSOCIETE            PIC X(03).            02360000
               10  FILLER                         PIC X(01) VALUE       02370000
           ' '.                                                         02380000
               10  IMU220-E05-NLIEU               PIC X(03).            02390000
               10  FILLER                         PIC X(01) VALUE       02400000
           ' '.                                                         02410000
               10  IMU220-E05-LLIEU               PIC X(20).            02420000
               10  FILLER                         PIC X(19) VALUE       02430000
           '        ENTREPOT : '.                                       02440000
               10  IMU220-E05-NSOCENTR            PIC X(03).            02450000
               10  FILLER                         PIC X(01) VALUE       02460000
           ' '.                                                         02470000
               10  IMU220-E05-NDEPOT              PIC X(03).            02480000
               10  FILLER                         PIC X(01) VALUE       02490000
           ' '.                                                         02500000
               10  IMU220-E05-LIEU-DEPOT          PIC X(20).            02510000
               10  FILLER                         PIC X(36) VALUE       02520000
           '                 DATE STOCKS MAG. : '.                      02530000
               10  IMU220-E05-DEXTRACTION.                              02540000
                   15 JJ                          PIC XX.               02550000
                   15 FILLER                      PIC X VALUE '/'.      02560000
                   15 MM                          PIC XX.               02570000
                   15 FILLER                      PIC X VALUE '/'.      02580000
                   15 SS                          PIC XX.               02590000
                   15 AA                          PIC XX.               02600000
               10  FILLER                         PIC X(01) VALUE       02610000
           ' '.                                                         02620000
      *--------------------------- LIGNE E08  --------------------------02630000
           05  IMU220-E08.                                              02640000
               10  FILLER                         PIC X(58) VALUE       02650000
           '                                                          '.02660000
               10  FILLER                         PIC X(58) VALUE       02670000
           '                                            DATE STOCKS PR'.02680000
               10  FILLER                         PIC X(05) VALUE       02690000
           'O. : '.                                                     02700000
               10  IMU220-E08-DATE-STO-PRO.                             02710000
                   15 JJ                          PIC XX.               02720000
                   15 FILLER                      PIC X VALUE '/'.      02730000
                   15 MM                          PIC XX.               02740000
                   15 FILLER                      PIC X VALUE '/'.      02750000
                   15 SS                          PIC XX.               02760000
                   15 AA                          PIC XX.               02770000
               10  FILLER                         PIC X(01) VALUE       02780000
           ' '.                                                         02790000
      *--------------------------- LIGNE E10  --------------------------02800000
           05  IMU220-E10.                                              02810000
               10  FILLER                         PIC X(21) VALUE       02820000
           'NUMERO DE MUTATION : '.                                     02830000
               10  IMU220-E10-NMUTATION           PIC X(07).            02840000
               10  FILLER                         PIC X(42) VALUE       02850000
           '                  DATE DE FIN DE SAISIE : '.                02860000
               10  IMU220-E10-DFINSAIS.                                 02870000
                   15 JJ                          PIC XX.               02880000
                   15 FILLER                      PIC X VALUE '/'.      02890000
                   15 MM                          PIC XX.               02900000
                   15 FILLER                      PIC X VALUE '/'.      02910000
                   15 SS                          PIC XX.               02920000
                   15 AA                          PIC XX.               02930000
               10  FILLER                         PIC X(41) VALUE       02940000
           '                      DATE DE MUTATION : '.                 02950000
               10  IMU220-E10-DMUTATION.                                02960000
                   15 JJ                          PIC XX.               02970000
                   15 FILLER                      PIC X VALUE '/'.      02980000
                   15 MM                          PIC XX.               02990000
                   15 FILLER                      PIC X VALUE '/'.      03000000
                   15 SS                          PIC XX.               03010000
                   15 AA                          PIC XX.               03020000
               10  FILLER                         PIC X(01) VALUE       03030000
           ' '.                                                         03040000
      *--------------------------- LIGNE E15  --------------------------03050000
           05  IMU220-E15.                                              03060000
               10  FILLER                         PIC X(58) VALUE       03070000
           '----------------------------------------------------------'.03080000
               10  FILLER                         PIC X(58) VALUE       03090000
           '----------------------------------------------------------'.03100000
               10  FILLER                         PIC X(16) VALUE       03110000
           '----------------'.                                          03120000
      *--------------------------- LIGNE E20  --------------------------03130000
           05  IMU220-E20.                                              03140000
               10  FILLER                         PIC X(58) VALUE       03150000
           'FAMILLE / CARACTERISTIQUES                                '.03160000
               10  FILLER                         PIC X(58) VALUE       03170000
           '                                                          '.03180000
               10  FILLER                         PIC X(16) VALUE       03190000
           '                '.                                          03200000
      *--------------------------- LIGNE E25  --------------------------03210000
           05  IMU220-E25.                                              03220000
               10  FILLER                         PIC X(58) VALUE       03230000
           '                                             STA        RG'.03240000
               10  FILLER                         PIC X(58) VALUE       03250000
           '    VENTES EMPORTEES       STOCKS    MUT                  '.03260000
               10  FILLER                         PIC X(16) VALUE       03270000
           ' STO STA        '.                                          03280000
      *--------------------------- LIGNE E30  --------------------------03290000
           05  IMU220-E30.                                              03300000
               10  FILLER                         PIC X(58) VALUE       03310000
           ' MQE       REFERENCE       CODES DESCRIPTIFS EXP  PVTTC ST'.03320000
               10  FILLER                         PIC X(58) VALUE       03330000
           'E  4S  S-4 S-3 S-2 S-1  OBJ  MAX MAG ATT  CODIC    QTE COL'.03340000
               10  FILLER                         PIC X(16) VALUE       03350000
           '  EN PRD COMMENT'.                                          03360000
      *--------------------------- LIGNE E35  --------------------------03370000
           05  IMU220-E35.                                              03380000
               10  FILLER                         PIC X(58) VALUE       03390000
           '----------------------------------------------------------'.03400000
               10  FILLER                         PIC X(58) VALUE       03410000
           '----------------------------------------------------------'.03420000
               10  FILLER                         PIC X(16) VALUE       03430000
           '----------------'.                                          03440000
      *--------------------------- LIGNE E40  --------------------------03450000
           05  IMU220-E40.                                              03460000
               10  IMU220-E40-LFAM                PIC X(20).            03470000
               10  FILLER                         PIC X(58) VALUE       03480000
           '                                                          '.03490000
               10  FILLER                         PIC X(54) VALUE       03500000
           '                                                      '.    03510000
      *--------------------------- LIGNE E45  --------------------------03520000
           05  IMU220-E45.                                              03530000
               10  FILLER                         PIC X(58) VALUE       03540000
           '********************************************              '.03550000
               10  FILLER                         PIC X(58) VALUE       03560000
           '                                                          '.03570000
               10  FILLER                         PIC X(16) VALUE       03580000
           '                '.                                          03590000
      *--------------------------- LIGNE E50  --------------------------03600000
           05  IMU220-E50.                                              03610000
               10  IMU220-E50-LAGREGATED          PIC X(20).            03620000
               10  FILLER                         PIC X(58) VALUE       03630000
           '                                                          '.03640000
               10  FILLER                         PIC X(54) VALUE       03650000
           '                                                      '.    03660000
      *--------------------------- LIGNE E65  --------------------------03670000
           05  IMU220-E65.                                              03680000
               10  FILLER                         PIC X(58) VALUE       03690000
           '********************************************              '.03700000
               10  FILLER                         PIC X(58) VALUE       03710000
           '                                                          '.03720000
               10  FILLER                         PIC X(16) VALUE       03730000
           '                '.                                          03740000
      *--------------------------- LIGNE D04  --------------------------03750000
           05  IMU220-D04.                                              03760000
               10  FILLER                         PIC X(58) VALUE       03770000
           '--------------------------------------------------------- '.03780000
               10  FILLER                         PIC X(58) VALUE       03790000
           ' 80 / 20  ------------------------------------------------'.03800000
               10  FILLER                         PIC X(16) VALUE       03810000
           '----------------'.                                          03820000
      *--------------------------- LIGNE D05  --------------------------03830000
           05  IMU220-D05.                                              03840000
               10  IMU220-D05-CMARQ               PIC X(05).            03850000
               10  FILLER                         PIC X(01) VALUE       03860000
           ' '.                                                         03870000
               10  IMU220-D05-LREFFOURN           PIC X(20).            03880000
               10  FILLER                         PIC X(01) VALUE       03890000
           ' '.                                                         03900000
               10  IMU220-D05-CVDESCRIPT1         PIC X(05).            03910000
               10  FILLER                         PIC X(01) VALUE       03920000
           ' '.                                                         03930000
               10  IMU220-D05-CVDESCRIPT2         PIC X(05).            03940000
               10  FILLER                         PIC X(01) VALUE       03950000
           ' '.                                                         03960000
               10  IMU220-D05-CVDESCRIPT3         PIC X(05).            03970000
               10  FILLER                         PIC X(01) VALUE       03980000
           ' '.                                                         03990000
               10  IMU220-D05-LSTATCOMP           PIC X(03).            04000000
               10  FILLER                         PIC X(01) VALUE       04010000
           ' '.                                                         04020000
               10  IMU220-D05-PRIX-VENTE          PIC ZZZZZZ.           04030000
               10  FILLER                         PIC X(01) VALUE       04040000
           ' '.                                                         04050000
               10  IMU220-D05-RANG                PIC ZZZ.              04060000
               10  FILLER                         PIC X(01) VALUE       04070000
           ' '.                                                         04080000
               10  IMU220-D05-4S                  PIC ZZZ.              04090000
               10  FILLER                         PIC X(02) VALUE       04100000
           ' !'.                                                        04110000
               10  IMU220-D05-S-4                 PIC ZZZ.              04120000
               10  FILLER                         PIC X(01) VALUE       04130000
           ' '.                                                         04140000
               10  IMU220-D05-S-3                 PIC ZZZ.              04150000
               10  FILLER                         PIC X(01) VALUE       04160000
           ' '.                                                         04170000
               10  IMU220-D05-S-2                 PIC ZZZ.              04180000
               10  FILLER                         PIC X(01) VALUE       04190000
           ' '.                                                         04200000
               10  IMU220-D05-S-1                 PIC ZZZ.              04210000
               10  FILLER                         PIC X(02) VALUE       04220000
           ' !'.                                                        04230000
               10  IMU220-D05-QSTOCK-PRO2         PIC ZZ9.              04240000
               10  IMU220-REDEFINES1 REDEFINES IMU220-D05-QSTOCK-PRO2.  04250000
                   15  IMU220-D05-QSTOCK-PRO3         PIC ZZZ.          04260000
               10  IMU220-D05-FLAG-CALCUL         PIC X(01).            04270000
               10  FILLER                         PIC X(01) VALUE       04280000
           ' '.                                                         04290000
               10  IMU220-D05-QSTOCK-MAX2         PIC ZZ9.              04300000
               10  IMU220-REDEFINES2 REDEFINES IMU220-D05-QSTOCK-MAX2.  04310000
                   15  IMU220-D05-QSTOCK-MAX3         PIC ZZZ.          04320000
               10  FILLER                         PIC X(01) VALUE       04330000
           '!'.                                                         04340000
               10  IMU220-D05-QSTOCK-MAG          PIC ZZZ.              04350000
               10  FILLER                         PIC X(01) VALUE       04360000
           ' '.                                                         04370000
               10  IMU220-D05-QMUT-ATT            PIC ZZZ.              04380000
               10  FILLER                         PIC X(01) VALUE       04390000
           ' '.                                                         04400000
               10  IMU220-D05-NCODIC9             PIC X(09).            04410000
               10  FILLER                         PIC X(01) VALUE       04420000
           ' '.                                                         04430000
               10  IMU220-D05-QSTOCK-MUT          PIC ZZZ.              04440000
               10  FILLER                         PIC X(01) VALUE       04450000
           ' '.                                                         04460000
               10  IMU220-D05-QCOLIDSTOCK         PIC ZZZ.              04470000
               10  IMU220-D05-COLACTIF            PIC X(01).            04480000
               10  FILLER                         PIC X(01) VALUE       04490000
           ' '.                                                         04500000
               10  IMU220-D05-QSTOCK-DEP          PIC ZZ.               04510000
               10  FILLER                         PIC X(01) VALUE       04520000
           ' '.                                                         04530000
               10  IMU220-D05-STATUT-PRODUIT      PIC X(01).            04540000
               10  FILLER                         PIC X(01) VALUE       04550000
           ' '.                                                         04560000
               10  IMU220-D05-COMMENTAIRE         PIC X(09).            04570000
      *--------------------------- LIGNE T05  --------------------------04580000
           05  IMU220-T05.                                              04590000
               10  FILLER                         PIC X(58) VALUE       04600000
           '                                                          '.04610000
               10  FILLER                         PIC X(58) VALUE       04620000
           '                                                          '.04630000
               10  FILLER                         PIC X(16) VALUE       04640000
           '                '.                                          04650000
      *--------------------------- LIGNE T10  --------------------------04660000
           05  IMU220-T10.                                              04670000
               10  FILLER                         PIC X(58) VALUE       04680000
           '                                                 TOTAUX : '.04690000
               10  IMU220-T10-TOT4S               PIC ZZZZZ.            04700000
               10  FILLER                         PIC X(01) VALUE       04710000
           ' '.                                                         04720000
               10  IMU220-T10-TOTS-4              PIC ZZZZ.             04730000
               10  IMU220-T10-TOTS-3              PIC ZZZZ.             04740000
               10  IMU220-T10-TOTS-2              PIC ZZZZ.             04750000
               10  IMU220-T10-TOTS-1              PIC ZZZZ.             04760000
               10  FILLER                         PIC X(01) VALUE       04770000
           ' '.                                                         04780000
               10  IMU220-T10-TOTPRO              PIC ZZZZ.             04790000
               10  FILLER                         PIC X(01) VALUE       04800000
           ' '.                                                         04810000
               10  IMU220-T10-TOTMAX              PIC ZZZZ.             04820000
               10  IMU220-T10-TOTMAG              PIC ZZZZ.             04830000
               10  IMU220-T10-TOTATT              PIC ZZZZ.             04840000
               10  FILLER                         PIC X(10) VALUE       04850000
           '          '.                                                04860000
               10  IMU220-T10-TOTMUT-ATT          PIC ZZZZ.             04870000
               10  FILLER                         PIC X(20) VALUE       04880000
           '                    '.                                      04890000
           EJECT                                                        04900000
      *                                                                 04910000
      *---------------------------------------------------------------* 04920000
      *           D E S C R I P T I O N    D E S    T A B L E S       * 04930000
      *---------------------------------------------------------------* 04940000
      *                                                                 04950000
      *                                                                 04960000
      *                                                                 04970000
       PROCEDURE DIVISION.                                              04980000
      ***************************************************************** 04990000
      *              T R A M E   DU   P R O G R A M M E               * 05000000
      ***************************************************************** 05010000
      *                                                               * 05020000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05030000
      *                                                               * 05040000
      *           ------------------------------------------          * 05050000
      *           --  MODULE DE BASE DU PROGRAMME  BMU221 --          * 05060000
      *           ------------------------------------------          * 05070000
      *                               I                               * 05080000
      *           -----------------------------------------           * 05090000
      *           I                   I                   I           * 05100000
      *   -----------------   -----------------   -----------------   * 05110000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 05120000
      *   -----------------   -----------------   -----------------   * 05130000
      *                                                               * 05140000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05150000
      *                                                                 05160000
       MODULE-BMU221 SECTION.                                           05170000
      *                                                                 05180000
           PERFORM  DEBUT-BMU221.                                       05190000
           PERFORM  TRAIT-BMU221.                                       05200000
           PERFORM  FIN-BMU221.                                         05210000
      *                                                                 05220000
       F-MODULE-BMU221. EXIT.                                           05230000
      *                                                                 05240000
           EJECT                                                        05250000
      *                                                                 05260000
      *---------------------------------------------------------------* 05270000
      *                    D E B U T        B G P 8 9 0               * 05280000
      *---------------------------------------------------------------* 05290000
      *                                                                 05300000
       DEBUT-BMU221 SECTION.                                            05310000
      *                                                                 05320000
           DISPLAY '*** DEBUT BMU221 ***'.                              05330000
           MOVE WHEN-COMPILED   TO Z-WHEN-COMPILED.                     05340000
           STRING Z-WHEN-COMPILED(4:2) '/' Z-WHEN-COMPILED(1:2) '/'     05350000
                  Z-WHEN-COMPILED(7:2) DELIMITED BY SIZE INTO WEDITIOX  05360000
           DISPLAY                                                      05370000
           '**      DATE DE COMPILE (JJ/MM/AA): '   WEDITIOX    '  **'. 05380000
           DISPLAY '** VERSION DU 24/09/03 **'.                         05390000
           PERFORM OUVERTURE-FICHIERS.                                  05400000
           PERFORM TRAIT-DATE.                                          05410000
      *                                                                 05420000
       F-DEBUT-BMU221.                                                  05430000
           EXIT.                                                        05440000
      *                                                                 05450000
           EJECT                                                        05460000
      *                                                                 05470000
      *---------------------------------------------------------------* 05480000
      *                    T R A I T        B G P 8 9 0               * 05490000
      *---------------------------------------------------------------* 05500000
      *                                                                 05510000
       TRAIT-BMU221 SECTION.                                            05520000
      *                                                                 05530000
           INITIALIZE W-T10                                             05540000
                                                                        05550000
           PERFORM LECTURE-FMU220.                                      05560000
LM0907     MOVE FMU220-CODSEGM TO WS-CODSEGM-PREV                       05570001
                                                                        05580000
EOF        PERFORM UNTIL EOF                                            05590000
             MOVE ZERO TO CTR-PAGE                                      05600001
                          CTR-LIGNE                                     05610001
LM0907       MOVE FMU220-CODSEGM TO WS-CODSEGM-PREV                     05620001
LM0907       PERFORM UNTIL FMU220-CODSEGM NOT = WS-CODSEGM-PREV         05630001
                       OR EOF                                           05640001
               PERFORM RUPTURE-ENTETE                                   05650001
               PERFORM ECRITURE-ENTETE                                  05660001
                                                                        05670000
ENTETE         PERFORM UNTIL                                            05680001
                     FMU220-NSOCIETE  NOT = W-FMU220-NSOCIETE           05690001
                  OR FMU220-NLIEU     NOT = W-FMU220-NLIEU              05700001
                  OR FMU220-NMUTATION NOT = W-FMU220-NMUTATION          05710001
                  OR FMU220-SAUT      NOT = W-FMU220-SAUT               05720001
LM0907            OR FMU220-CODSEGM   NOT = WS-CODSEGM-PREV             05730001
                  OR EOF                                                05740001
                  PERFORM ECRITURE-LAGREGATED                           05750001
                  PERFORM RUPTURE-LAGREGATED                            05760001
                                                                        05770000
LAGREG            PERFORM UNTIL                                         05780001
                       FMU220-NSOCIETE   NOT = W-FMU220-NSOCIETE        05790000
                    OR FMU220-NLIEU      NOT = W-FMU220-NLIEU           05800000
                    OR FMU220-NMUTATION  NOT = W-FMU220-NMUTATION       05810000
                    OR FMU220-SAUT       NOT = W-FMU220-SAUT            05820000
                    OR FMU220-WSEQFAM    NOT = W-FMU220-WSEQFAM         05830000
                    OR FMU220-LAGREGATED NOT = W-FMU220-LAGREGATED      05840000
LM0907              OR FMU220-CODSEGM   NOT = WS-CODSEGM-PREV           05850001
                    OR EOF                                              05860000
                    PERFORM RUPTURE-LIGNE                               05870000
                                                                        05880000
LIGNE               PERFORM UNTIL                                       05890000
                          FMU220-NSOCIETE   NOT = W-FMU220-NSOCIETE     05900000
                       OR FMU220-NLIEU      NOT = W-FMU220-NLIEU        05910000
                       OR FMU220-NMUTATION  NOT = W-FMU220-NMUTATION    05920000
                       OR FMU220-SAUT       NOT = W-FMU220-SAUT         05930000
                       OR FMU220-WSEQFAM    NOT = W-FMU220-WSEQFAM      05940000
                       OR FMU220-LAGREGATED NOT = W-FMU220-LAGREGATED   05950000
                       OR FMU220-ZTRI       NOT = W-FMU220-ZTRI         05960000
                       OR FMU220-NSEQ       NOT = W-FMU220-NSEQ         05970000
LM0907                 OR FMU220-CODSEGM    NOT = WS-CODSEGM-PREV       05980001
                       OR EOF                                           05990000
                       PERFORM CUMUL-TOTAUX                             06000000
                       PERFORM ECRITURE-DETAIL                          06010000
                       PERFORM LECTURE-FMU220                           06020000
LIGNE               END-PERFORM                                         06030000
LAGREG           END-PERFORM                                            06040000
                 PERFORM ECRITURE-TOTAUX                                06050000
ENTETE         END-PERFORM                                              06060001
CODSEG       END-PERFORM                                                06070001
EOF        END-PERFORM.                                                 06080001
                                                                        06090000
       FIN-TRAIT-BMU221. EXIT.                                          06100000
      *                                                                 06110000
       FIN-BMU221 SECTION.                                              06120000
      *                                                                 06130000
           CLOSE FMU220.                                                06140001
           CLOSE FDATE.                                                 06150001
           CLOSE IMU220A IMU220B IMU220C IMU220D IMU220E IMU220F.       06160001
           DISPLAY '**********************************************'.    06170000
           DISPLAY '*    PROGRAMME BMU221  TERMINE NORMALEMENT   *'.    06180000
           DISPLAY '*                                            *'.    06190000
           DISPLAY '*  NOMBRE D''ENREG TRAITES SUR FMU220 = ' CTR-FMU22006200000
                   ' *'.                                                06210000
           DISPLAY '*                                            *'.    06220000
           DISPLAY '**********************************************'.    06230000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    06240000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 06250000
       FIN-FIN-BMU221. EXIT.                                            06260000
           EJECT                                                        06270000
      *                                                                 06280000
      *---------------------------------------------------------------* 06290000
      *         M O D U L E S    D E B U T  B G P 8 9 0               * 06300000
      *---------------------------------------------------------------* 06310000
      *                                                                 06320000
       OUVERTURE-FICHIERS SECTION.                                      06330000
      *-------------------                                              06340000
      *                                                                 06350000
           OPEN INPUT FMU220.                                           06360001
           IF STA-FMU220 NOT = ZERO                                     06370001
              STRING 'ERREUR OUVERTURE FMU220 :' STA-FMU220 ':'         06380001
                DELIMITED BY SIZE                                       06390001
                INTO  ABEND-MESS                                        06400001
                PERFORM FIN-ANORMALE                                    06410001
           END-IF.                                                      06420001
           OPEN INPUT FDATE.                                            06430001
           IF STA-FDATE  NOT = ZERO                                     06440001
              STRING 'ERREUR OUVERTURE FDATE  :' STA-FDATE  ':'         06450001
                DELIMITED BY SIZE                                       06460001
                INTO  ABEND-MESS                                        06470001
                PERFORM FIN-ANORMALE                                    06480001
           END-IF.                                                      06490001
           OPEN OUTPUT IMU220A.                                         06500001
           OPEN OUTPUT IMU220B.                                         06510001
           OPEN OUTPUT IMU220C.                                         06520001
           OPEN OUTPUT IMU220D.                                         06530001
           OPEN OUTPUT IMU220E.                                         06540001
           OPEN OUTPUT IMU220F.                                         06550001
       FIN-OUVERTURE-FICHIERS. EXIT.                                    06560000
      *                                                                 06570000
       TRAIT-DATE SECTION.                                              06580000
      *-----------                                                      06590000
           READ FDATE                                                   06600000
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS              06610000
                  PERFORM FIN-ANORMALE.                                 06620000
                                                                        06630000
           MOVE '1'                 TO GFDATA.                          06640000
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                      06650000
           CALL 'BETDATC' USING WORK-BETDATC.                           06660000
           IF GFVDAT NOT = '1'                                          06670000
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                    06680000
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS    06690000
              PERFORM FIN-ANORMALE                                      06700000
           ELSE                                                         06710000
              DISPLAY '** FDATE  ** ' GFJMSA-5                          06720000
           END-IF.                                                      06730000
           COMPUTE GFQNT0 = GFQNT0 + 1 .                                06740000
           MOVE '3' TO GFDATA.                                          06750000
                                                                        06760000
           CALL 'BETDATC' USING WORK-BETDATC.                           06770000
           IF GFVDAT NOT = '1'                                          06780000
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS        06790000
              PERFORM FIN-ANORMALE                                      06800000
           END-IF.                                                      06810000
           MOVE GFJJMMSSAA       TO DATJMSA.                            06820000
           MOVE GFSMN            TO JOUR-SEM.                           06830000
           DISPLAY 'DATE RECEPTION ETAT DANS BOITE EOS : ' DATJMSA.     06840000
           ACCEPT W-TIME FROM TIME.                                     06850000
       FIN-TRAIT-DATE. EXIT.                                            06860000
      *                                                                 06870000
      *---------------------------------------------------------------* 06880000
      *         M O D U L E S    T R A I T  B G P 8 7 0               * 06890000
      *---------------------------------------------------------------* 06900000
      *                                                                 06910000
       LECTURE-FMU220 SECTION.                                          06920000
      *----------------                                                 06930000
           MOVE DSECT-FMU220 TO DSECT-W-FMU220.                         06940000
           INITIALIZE DSECT-FMU220.                                     06950000
           READ FMU220                                                  06960000
                INTO DSECT-FMU220                                       06970000
                AT END SET EOF TO TRUE                                  06980000
           END-READ                                                     06990000
           IF NOT EOF                                                   07000000
              IF CTR-FMU220 = 0                                         07010001
                 MOVE DSECT-FMU220 TO DSECT-W-FMU220                    07020001
              END-IF                                                    07030001
              ADD 1 TO CTR-FMU220                                       07040001
      *       DISPLAY 'CTR-FMU220' CTR-FMU220                           07041002
           END-IF.                                                      07050000
       FIN-LECTURE-FMU220. EXIT.                                        07060000
      *                                                                 07070000
       RUPTURE-ENTETE   SECTION.                                        07080000
                                                                        07090000
MH         MOVE NOMETAT-FMU220   TO NOMETAT-W-FMU220                    07100000
           MOVE FMU220-NSOCIETE  TO W-FMU220-NSOCIETE                   07110000
           MOVE FMU220-NLIEU     TO W-FMU220-NLIEU                      07120000
           MOVE FMU220-NMUTATION TO W-FMU220-NMUTATION                  07130000
           MOVE FMU220-SAUT      TO W-FMU220-SAUT.                      07140000
LM0907     MOVE FMU220-CODSEGM   TO WS-CODSEGM-PREV.                    07150001
       FIN-RUPTURE-ENTETE.    EXIT.                                     07160000
      *                                                                 07170000
       RUPTURE-LAGREGATED SECTION.                                      07180000
                                                                        07190000
           MOVE FMU220-NSOCIETE   TO W-FMU220-NSOCIETE                  07200000
           MOVE FMU220-NLIEU      TO W-FMU220-NLIEU                     07210000
           MOVE FMU220-NMUTATION  TO W-FMU220-NMUTATION                 07220000
           MOVE FMU220-SAUT       TO W-FMU220-SAUT                      07230000
           MOVE FMU220-WSEQFAM    TO W-FMU220-WSEQFAM                   07240000
           MOVE FMU220-LAGREGATED TO W-FMU220-LAGREGATED.               07250000
LM0907     MOVE FMU220-CODSEGM    TO WS-CODSEGM-PREV.                   07260001
       FIN-RUPTURE-LAGREGATED. EXIT.                                    07270000
      *                                                                 07280000
       RUPTURE-LIGNE SECTION.                                           07290000
                                                                        07300000
           MOVE FMU220-NSOCIETE   TO W-FMU220-NSOCIETE                  07310000
           MOVE FMU220-NLIEU      TO W-FMU220-NLIEU                     07320000
           MOVE FMU220-NMUTATION  TO W-FMU220-NMUTATION                 07330000
           MOVE FMU220-SAUT       TO W-FMU220-SAUT                      07340000
           MOVE FMU220-WSEQFAM    TO W-FMU220-WSEQFAM                   07350000
           MOVE FMU220-LAGREGATED TO W-FMU220-LAGREGATED                07360000
           MOVE FMU220-ZTRI       TO W-FMU220-ZTRI                      07370000
           MOVE FMU220-NSEQ       TO W-FMU220-NSEQ.                     07380000
LM0907     MOVE FMU220-CODSEGM    TO WS-CODSEGM-PREV.                   07390001
                                                                        07400000
       FIN-RUPTURE-LIGNE. EXIT.                                         07410000
      *                                                                 07420000
       ECRITURE-ENTETE SECTION.                                         07430000
      *-----------------                                                07440000
LM0906     MOVE FMU220-CODSEGM  TO WS-CODSEGM-TEST.                     07450001
           ADD 1              TO CTR-PAGE.                              07460000
           MOVE 0             TO CTR-LIGNE.                             07470000
MH         MOVE NOMETAT-FMU220        TO IMU220-SEGMENT.                07480000
           MOVE CORRESPONDING DATJMSA TO IMU220-E00-DATEDITE.           07490000
           MOVE HH OF W-TIME          TO IMU220-E00-HEUREDITE.          07500000
           MOVE MN OF W-TIME          TO IMU220-E00-MINUEDITE.          07510000
           MOVE CTR-PAGE              TO IMU220-E00-NOPAGE.             07520000
           MOVE '1'                   TO SAUT.                          07530000
           MOVE IMU220-E00            TO LIGNE.                         07540000
           PERFORM ECRITURE-VENTILATION                                 07550001
           ADD 1 TO CTR-LIGNE.                                          07560000
      *                                                                 07570000
           MOVE FMU220-NSOCIETE    TO IMU220-E05-NSOCIETE.              07580000
           MOVE FMU220-NLIEU       TO IMU220-E05-NLIEU.                 07590000
           MOVE FMU220-LLIEU       TO IMU220-E05-LLIEU.                 07600000
           MOVE FMU220-NSOCENTR    TO IMU220-E05-NSOCENTR.              07610000
           MOVE FMU220-NDEPOT      TO IMU220-E05-NDEPOT.                07620000
           MOVE FMU220-LIEU-DEPOT  TO IMU220-E05-LIEU-DEPOT.            07630000
           IF FMU220-DEXTRACTION > SPACES                               07640000
              MOVE FMU220-DEXTRACTION TO W-DATE-TAMP                    07650000
              MOVE CORRESPONDING W-DATE-TAMP TO IMU220-E05-DEXTRACTION  07660000
           ELSE                                                         07670000
              MOVE SPACES                    TO IMU220-E05-DEXTRACTION  07680000
           END-IF                                                       07690000
                                                                        07700000
           MOVE '0'                TO SAUT.                             07710000
           MOVE IMU220-E05         TO LIGNE.                            07720000
           PERFORM ECRITURE-VENTILATION                                 07730001
           ADD 2 TO CTR-LIGNE.                                          07740000
      *                                                                 07750000
           IF FMU220-DATE-STO-PRO > SPACES                              07760000
              MOVE FMU220-DATE-STO-PRO       TO W-DATE-TAMP             07770000
              MOVE CORRESPONDING W-DATE-TAMP TO IMU220-E08-DATE-STO-PRO 07780000
           ELSE                                                         07790000
              MOVE SPACES                    TO IMU220-E08-DATE-STO-PRO 07800000
           END-IF                                                       07810000
                                                                        07820000
           MOVE '0'                 TO SAUT.                            07830000
           MOVE IMU220-E08          TO LIGNE.                           07840000
           PERFORM ECRITURE-VENTILATION                                 07850001
           ADD 2 TO CTR-LIGNE.                                          07860000
      *                                                                 07870000
           MOVE FMU220-NMUTATION          TO IMU220-E10-NMUTATION.      07880000
           MOVE FMU220-DFINSAIS           TO W-DATE-TAMP                07890000
           MOVE CORRESPONDING W-DATE-TAMP TO IMU220-E10-DFINSAIS.       07900000
           MOVE FMU220-DMUTATION          TO W-DATE-TAMP                07910000
           MOVE CORRESPONDING W-DATE-TAMP TO IMU220-E10-DMUTATION.      07920000
           MOVE '0'               TO SAUT.                              07930000
           MOVE IMU220-E10        TO LIGNE.                             07940001
           PERFORM ECRITURE-VENTILATION                                 07950001
           ADD 2 TO CTR-LIGNE.                                          07960000
      *                                                                 07970000
           MOVE '0'               TO SAUT.                              07980000
           MOVE IMU220-E15        TO LIGNE.                             07990000
           PERFORM ECRITURE-VENTILATION                                 08000001
           ADD 2 TO CTR-LIGNE.                                          08010000
      *                                                                 08020000
           MOVE ' '               TO SAUT.                              08030000
           MOVE IMU220-E20        TO LIGNE.                             08040000
           PERFORM ECRITURE-VENTILATION                                 08050001
           ADD 1 TO CTR-LIGNE.                                          08060000
      *                                                                 08070000
           MOVE ' '               TO SAUT.                              08080000
           MOVE IMU220-E25        TO LIGNE.                             08090000
           PERFORM ECRITURE-VENTILATION                                 08100001
           ADD 1 TO CTR-LIGNE.                                          08110000
      *                                                                 08120000
           MOVE ' '               TO SAUT.                              08130000
           MOVE IMU220-E30        TO LIGNE.                             08140000
           PERFORM ECRITURE-VENTILATION                                 08150001
           ADD 1 TO CTR-LIGNE.                                          08160000
       FIN-ECRITURE-ENTETE. EXIT.                                       08170000
      *                                                                 08180000
       ECRITURE-LAGREGATED SECTION.                                     08190000
      *-----------------                                                08200000
           IF CTR-LIGNE >  MAX-LIGNE1                                   08210000
              PERFORM ECRITURE-ENTETE                                   08220000
           END-IF                                                       08230000
           MOVE ' '               TO SAUT.                              08240000
           MOVE IMU220-E35        TO LIGNE.                             08250000
           PERFORM ECRITURE-VENTILATION                                 08260001
           ADD 1 TO CTR-LIGNE.                                          08270000
      *                                                                 08280000
           MOVE ' '               TO SAUT.                              08290000
           MOVE FMU220-LFAM       TO IMU220-E40-LFAM.                   08300000
           MOVE IMU220-E40        TO LIGNE.                             08310000
           PERFORM ECRITURE-VENTILATION                                 08320001
           ADD 1 TO CTR-LIGNE.                                          08330000
      *                                                                 08340000
           MOVE ' '               TO SAUT.                              08350000
           MOVE IMU220-E45        TO LIGNE.                             08360000
           PERFORM ECRITURE-VENTILATION                                 08370001
           ADD 1 TO CTR-LIGNE.                                          08380000
      *                                                                 08390000
           IF FMU220-WFAM > ' '                                         08400000
              MOVE ' '               TO SAUT                            08410000
              MOVE FMU220-LAGREGATED TO IMU220-E50-LAGREGATED           08420000
              MOVE IMU220-E50        TO LIGNE                           08430000
              PERFORM ECRITURE-VENTILATION                              08440001
              ADD 1 TO CTR-LIGNE                                        08450000
      *                                                                 08460000
              MOVE ' '               TO SAUT                            08470000
              MOVE IMU220-E65        TO LIGNE                           08480000
              PERFORM ECRITURE-VENTILATION                              08490001
              ADD 1 TO CTR-LIGNE                                        08500000
           END-IF.                                                      08510000
       FIN-ECRITURE-LAGREGATED. EXIT.                                   08520000
                                                                        08530000
       ECRITURE-DETAIL SECTION.                                         08540000
      *-----------------                                                08550000
LM0906     MOVE FMU220-CODSEGM  TO WS-CODSEGM-TEST.                     08560001
           IF CTR-LIGNE >= MAX-LIGNE                                    08570000
              PERFORM ECRITURE-ENTETE                                   08580000
              PERFORM ECRITURE-LAGREGATED                               08590000
           END-IF                                                       08600000
                                                                        08610000
           IF FMU220-W8020-RUP > ' '                                    08620000
              MOVE ' '               TO SAUT                            08630000
              MOVE IMU220-D04        TO LIGNE                           08640001
              PERFORM ECRITURE-VENTILATION                              08650001
              ADD 1 TO CTR-LIGNE                                        08660000
           END-IF                                                       08670000
                                                                        08680000
           IF CTR-LIGNE >= MAX-LIGNE                                    08690000
              PERFORM ECRITURE-ENTETE                                   08700000
              PERFORM ECRITURE-LAGREGATED                               08710000
           END-IF                                                       08720000
                                                                        08730000
           IF FMU220-NSEQ =  0                                          08740000
              MOVE FMU220-CMARQ       TO IMU220-D05-CMARQ               08750000
              MOVE FMU220-CVDESCRIPT1 TO IMU220-D05-CVDESCRIPT1         08760000
              MOVE FMU220-CVDESCRIPT2 TO IMU220-D05-CVDESCRIPT2         08770000
              MOVE FMU220-CVDESCRIPT3 TO IMU220-D05-CVDESCRIPT3         08780000
              IF FMU220-LSTATCOMP > SPACES                              08790000
                 MOVE FMU220-LSTATCOMP   TO IMU220-D05-LSTATCOMP        08800000
              ELSE                                                      08810000
                 MOVE SPACES             TO IMU220-D05-LSTATCOMP        08820000
              END-IF                                                    08830000
              COMPUTE W-PRIX ROUNDED =  FMU220-PRIX-VENTE               08840000
              MOVE W-PRIX             TO IMU220-D05-PRIX-VENTE          08850000
              MOVE FMU220-RANG        TO IMU220-D05-RANG                08860000
              MOVE FMU220-FLAG-CALCUL TO IMU220-D05-FLAG-CALCUL         08870000
              MOVE FMU220-QSTOCK-MUT  TO IMU220-D05-QSTOCK-MUT          08880000
              MOVE FMU220-QCOLIDSTOCK TO IMU220-D05-QCOLIDSTOCK         08890000
              MOVE FMU220-COLACTIF    TO IMU220-D05-COLACTIF            08900000
PM0105*       MOVE FMU220-QSTOCK-DEP  TO IMU220-D05-QSTOCK-DEP          08910000
              MOVE FMU220-STATUT-PRODUIT TO IMU220-D05-STATUT-PRODUIT   08920000
              IF FMU220-COMMENTAIRE > SPACES                            08930000
                 MOVE FMU220-COMMENTAIRE TO IMU220-D05-COMMENTAIRE      08940000
              ELSE                                                      08950000
                 MOVE SPACES             TO IMU220-D05-COMMENTAIRE      08960000
              END-IF                                                    08970000
           ELSE                                                         08980000
              MOVE SPACES             TO IMU220-D05-CMARQ               08990000
              MOVE SPACES             TO IMU220-D05-CVDESCRIPT1         09000000
              MOVE SPACES             TO IMU220-D05-CVDESCRIPT2         09010000
              MOVE SPACES             TO IMU220-D05-CVDESCRIPT3         09020000
              MOVE SPACES             TO IMU220-D05-LSTATCOMP           09030000
              MOVE 0                  TO IMU220-D05-PRIX-VENTE          09040000
              MOVE 0                  TO IMU220-D05-RANG                09050000
              MOVE SPACES             TO IMU220-D05-FLAG-CALCUL         09060000
              MOVE 0                  TO IMU220-D05-QSTOCK-MUT          09070000
              MOVE 0                  TO IMU220-D05-QCOLIDSTOCK         09080000
              MOVE SPACES             TO IMU220-D05-COLACTIF            09090000
              MOVE 0                  TO IMU220-D05-QSTOCK-DEP          09100000
              MOVE SPACES             TO IMU220-D05-STATUT-PRODUIT      09110000
              MOVE SPACES             TO IMU220-D05-COMMENTAIRE         09120000
           END-IF                                                       09130000
                                                                        09140000
           MOVE FMU220-NCODIC9     TO IMU220-D05-NCODIC9                09150000
           MOVE FMU220-LREFFOURN   TO IMU220-D05-LREFFOURN              09160000
           MOVE FMU220-S-1         TO IMU220-D05-S-1                    09170000
           MOVE FMU220-S-2         TO IMU220-D05-S-2                    09180000
           MOVE FMU220-S-3         TO IMU220-D05-S-3                    09190000
           MOVE FMU220-S-4         TO IMU220-D05-S-4                    09200000
           COMPUTE W-4S = FMU220-S-1 + FMU220-S-2 + FMU220-S-3 +        09210000
                          FMU220-S-4                                    09220000
                                                                        09230000
           MOVE W-4S               TO IMU220-D05-4S                     09240000
           MOVE FMU220-QSTOCK-MAG  TO IMU220-D05-QSTOCK-MAG             09250000
           MOVE FMU220-QMUT-ATT    TO IMU220-D05-QMUT-ATT               09260000
PM0105     MOVE FMU220-QSTOCK-DEP  TO IMU220-D05-QSTOCK-DEP             09270000
                                                                        09280000
           IF FMU220-TOP0 > ' '                                         09290000
              MOVE W-T10-QSTOCK-PRO2  TO IMU220-D05-QSTOCK-PRO2         09300000
           ELSE                                                         09310000
              MOVE 0                  TO IMU220-D05-QSTOCK-PRO3         09320000
           END-IF.                                                      09330000
                                                                        09340000
           IF W-T10-DIFF  > 0                                           09350000
              MOVE W-T10-QSTOCK-MAX2  TO IMU220-D05-QSTOCK-MAX2         09360000
           ELSE                                                         09370000
              MOVE 0                  TO IMU220-D05-QSTOCK-MAX3         09380000
           END-IF.                                                      09390000
                                                                        09400000
           MOVE ' '                TO SAUT.                             09410000
           MOVE IMU220-D05        TO LIGNE                              09420000
           PERFORM ECRITURE-VENTILATION                                 09430001
           ADD 1 TO CTR-LIGNE.                                          09440000
       FIN-ECRITURE-DETAIL. EXIT.                                       09450000
                                                                        09460000
       CUMUL-TOTAUX SECTION.                                            09470000
      *-----------------                                                09480000
           ADD FMU220-S-4          TO W-T10-TOTS-4                      09490000
           ADD FMU220-S-3          TO W-T10-TOTS-3                      09500000
           ADD FMU220-S-2          TO W-T10-TOTS-2                      09510000
           ADD FMU220-S-1          TO W-T10-TOTS-1                      09520000
           COMPUTE W-T10-TOT4S = W-T10-TOTS-4 + W-T10-TOTS-3 +          09530000
                                 W-T10-TOTS-2 + W-T10-TOTS-1            09540000
                                                                        09550000
           COMPUTE W-T10-DIFF  = FMU220-QSTOCK-MAX - FMU220-QSTOCK-PRO  09560000
PM0105*--- ON NE TOTALISE PLUS LES STOCK OBJECTIFS DU REMPLACE          09570000
PM0105     IF FMU220-NSEQ =  0                                          09580000
  "           ADD FMU220-QSTOCK-PRO   TO W-T10-TOTPRO                   09590000
PM0105     END-IF                                                       09600000
           ADD FMU220-QSTOCK-MAX   TO W-T10-TOTMAX                      09610000
           ADD FMU220-QSTOCK-MAG   TO W-T10-TOTMAG                      09620000
           ADD FMU220-QMUT-ATT     TO W-T10-TOTATT                      09630000
           ADD FMU220-QSTOCK-MUT   TO W-T10-TOTMUT-ATT                  09640000
                                                                        09650001
           MOVE FMU220-QSTOCK-PRO  TO W-T10-QSTOCK-PRO2                 09660000
           MOVE FMU220-QSTOCK-MAX  TO W-T10-QSTOCK-MAX2.                09670000
       FIN-CUMUL-TOTAUX. EXIT.                                          09680000
      *                                                                 09690000
       ECRITURE-TOTAUX SECTION.                                         09700000
      *-----------------                                                09710000
LM0906     MOVE WS-CODSEGM-PREV   TO WS-CODSEGM-TEST.                   09720001
                                                                        09730001
           IF CTR-LIGNE >  MAX-LIGNE2                                   09740000
              PERFORM ECRITURE-ENTETE                                   09750000
              PERFORM ECRITURE-LAGREGATED                               09760000
           END-IF                                                       09770000
                                                                        09780000
           MOVE ' '               TO SAUT.                              09790000
           MOVE IMU220-T05        TO LIGNE.                             09800000
           PERFORM ECRITURE-VENTILATION                                 09810001
           ADD 1 TO CTR-LIGNE.                                          09820000
                                                                        09830000
           MOVE ' ' TO SAUT.                                            09840000
           MOVE W-T10-TOTS-4       TO IMU220-T10-TOTS-4                 09850000
           MOVE W-T10-TOTS-3       TO IMU220-T10-TOTS-3                 09860000
           MOVE W-T10-TOTS-2       TO IMU220-T10-TOTS-2                 09870000
           MOVE W-T10-TOTS-1       TO IMU220-T10-TOTS-1                 09880000
           MOVE W-T10-TOT4S        TO IMU220-T10-TOT4S                  09890000
                                                                        09900000
           MOVE W-T10-TOTPRO       TO IMU220-T10-TOTPRO                 09910000
           MOVE W-T10-TOTMAX       TO IMU220-T10-TOTMAX                 09920000
           MOVE W-T10-TOTMAG       TO IMU220-T10-TOTMAG                 09930000
           MOVE W-T10-TOTATT       TO IMU220-T10-TOTATT                 09940000
           MOVE W-T10-TOTMUT-ATT   TO IMU220-T10-TOTMUT-ATT             09950000
           MOVE IMU220-T10         TO LIGNE.                            09960000
           PERFORM ECRITURE-VENTILATION                                 09970001
           ADD 1 TO CTR-LIGNE.                                          09980000
           INITIALIZE W-T10.                                            09990000
       FIN-ECRITURE-TOTAUX. EXIT.                                       10000000
      *                                                                 10010000
       ECRITURE-VENTILATION  SECTION.                                   10020001
           EVALUATE WS-CODSEGM-TEST                                     10030001
              WHEN 'A' PERFORM ECRITURE-IMU220A                         10040001
              WHEN 'B' PERFORM ECRITURE-IMU220B                         10050001
              WHEN 'C' PERFORM ECRITURE-IMU220C                         10060001
              WHEN 'D' PERFORM ECRITURE-IMU220D                         10070001
              WHEN 'E' PERFORM ECRITURE-IMU220E                         10080001
              WHEN 'F' PERFORM ECRITURE-IMU220F                         10090001
           END-EVALUATE.                                                10100001
       FIN-ECRITURE-VENTILATION.  EXIT.                                 10110001
      *                                                                 10120001
       ECRITURE-IMU220A   SECTION.                                      10130001
           MOVE  SAUT  TO  SAUT-A.                                      10140001
           MOVE  LIGNE TO  LIGNE-A.                                     10150001
           WRITE LIGNE-IMU220A.                                         10160001
       F-ECRITURE-IMU220A.   EXIT.                                      10170001
      *                                                                 10180001
       ECRITURE-IMU220B   SECTION.                                      10190001
           MOVE  SAUT  TO  SAUT-B.                                      10200001
           MOVE  LIGNE TO  LIGNE-B.                                     10210001
           WRITE LIGNE-IMU220B.                                         10220001
       F-ECRITURE-IMU220B.   EXIT.                                      10230001
      *                                                                 10240001
       ECRITURE-IMU220C   SECTION.                                      10250001
           MOVE  SAUT  TO  SAUT-C.                                      10260001
           MOVE  LIGNE TO  LIGNE-C.                                     10270001
           WRITE LIGNE-IMU220C.                                         10280001
       F-ECRITURE-IMU220C.   EXIT.                                      10290001
      *                                                                 10300001
       ECRITURE-IMU220D   SECTION.                                      10310001
           MOVE  SAUT  TO  SAUT-D.                                      10320001
           MOVE  LIGNE TO  LIGNE-D.                                     10330001
           WRITE LIGNE-IMU220D.                                         10340001
       F-ECRITURE-IMU220D.   EXIT.                                      10350001
      *                                                                 10360001
       ECRITURE-IMU220E   SECTION.                                      10370001
           MOVE  SAUT  TO  SAUT-E.                                      10380001
           MOVE  LIGNE TO  LIGNE-E.                                     10390001
           WRITE LIGNE-IMU220E.                                         10400001
       F-ECRITURE-IMU220E.   EXIT.                                      10410001
      *                                                                 10420001
       ECRITURE-IMU220F   SECTION.                                      10430001
           MOVE  SAUT  TO  SAUT-F.                                      10440001
           MOVE  LIGNE TO  LIGNE-F.                                     10450001
           WRITE LIGNE-IMU220F.                                         10460001
       F-ECRITURE-IMU220F.   EXIT.                                      10470001
      *                                                                 10480001
      *---------------------------------------------------------------* 10490001
      *         P A R A G R A P H E   F I N  A N O R M A L E          * 10500000
      *---------------------------------------------------------------* 10510000
      *                                                                 10520000
       FIN-ANORMALE SECTION.                                            10530000
      *-------------                                                    10540000
           DISPLAY FMU220-NCODIC9                                       10550000
           MOVE 'BMU221' TO ABEND-PROG.                                 10560000
           CALL 'ABEND' USING                                           10570000
                        ABEND-PROG                                      10580000
                        ABEND-MESS.                                     10590000
      *                                                                 10600000
       F-FIN-ANORMALE.                                                  10610000
           EXIT.                                                        10620000
      *                                                                 10630000
           EJECT                                                        10640000
