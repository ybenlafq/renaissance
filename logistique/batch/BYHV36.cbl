      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.    BYHV36.                                           00020001
       AUTHOR.        DSA022.                                           00030000
      *------------------------------------------------------------*    00040000
      *   PROGRAMME D'EXCLUSION DE MVTS SUITE A LA FUSION DE 2 FIL *    00050000
      *------------------------------------------------------------*    00060000
      * DATE CREATION : 10/08/04  DSA022 NT                        *    00070000
      * PROJET        : VALO DES STOCKS                            *    00080000
      * PERIODICITE   : MENSUEL ET RQ                              *    00090000
      *-------------------------------------------------------------    00530000
       ENVIRONMENT DIVISION.                                            00540000
       CONFIGURATION SECTION.                                           00550000
       SPECIAL-NAMES.                                                   00560000
           DECIMAL-POINT IS COMMA.                                      00570000
       INPUT-OUTPUT SECTION.                                            00580000
       FILE-CONTROL.                                                    00590000
                                                                        00600000
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FHV030 ASSIGN TO FHV030.                              00630000
      *                                                                         
      *--                                                                       
           SELECT FHV030 ASSIGN TO FHV030                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00640000
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FHV031 ASSIGN TO FHV031.                              00640100
      *                                                                         
      *--                                                                       
           SELECT FHV031 ASSIGN TO FHV031                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00641000
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGS40  ASSIGN TO FGS40.                               00650000
      *                                                                         
      *--                                                                       
           SELECT FGS40  ASSIGN TO FGS40                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00660000
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNSOC  ASSIGN TO FNSOC.                               00670001
      *                                                                         
      *--                                                                       
           SELECT FNSOC  ASSIGN TO FNSOC                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00671001
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPARAM ASSIGN TO FPARAM.                              00672001
      *                                                                         
      *--                                                                       
           SELECT FPARAM ASSIGN TO FPARAM                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00673001
      *}                                                                        
       DATA DIVISION.                                                   00690000
       FILE SECTION.                                                    00700000
                                                                        00710000
      *                                                                 00720000
      * FICHIER DES MVTS ISSU DE LA STAT0P DE LA FILIALE RESTANTE       00870000
      * EX :FICHIER BHV030AL DNPC                                       00871000
       FD  FHV030                                                       00880000
           LABEL RECORD STANDARD                                        00890000
           BLOCK  CONTAINS 0   RECORDS                                  00900000
           DATA RECORD FHV030-RECORD.                                   00910000
       01  FHV030-RECORD       PIC X(146).                              00920000
                                                                        00930000
      * FICHIER DES MVTS ISSU DE LA STAT0P DE LA FILIALE DETRUITE       00930200
      * EX : FICHIER BHV030AR DN                                        00930300
       FD  FHV031                                                       00930400
           LABEL RECORD STANDARD                                        00930500
           BLOCK  CONTAINS 0   RECORDS                                  00930600
           DATA RECORD FHV031-RECORD.                                   00930700
       01  FHV031-RECORD       PIC X(179).                              00930801
                                                                        00930900
      * FICHIER DES MOUVEMENTS QUE L'ON GARDE                           00931100
      * EX : FICHIER BHV030AL DNPC                                      00931200
       FD  FGS40                                                        00932000
           LABEL RECORD STANDARD                                        00933000
           BLOCK  CONTAINS 0   RECORDS                                  00934000
           DATA RECORD FGS40-RECORD.                                    00935000
       01  FGS40-RECORD       PIC X(146).                               00936000
                                                                        00937000
      *                                                                 00938001
       FD  FNSOC                                                        00939001
           LABEL RECORD STANDARD                                        00939101
           BLOCK  CONTAINS 0   RECORDS.                                 00939201
       01  MW-FILLER             PIC X(80).                                     
      *                                                                 00939401
       FD  FPARAM                                                       00939501
           LABEL RECORD STANDARD                                        00939601
           BLOCK  CONTAINS 0   RECORDS.                                 00939701
      *MW DAR-2                                                                 
       01  MW-FILLER             PIC X(80).                                     
      *                                                                 00940001
                                                                        01070000
      *---------------------------------------------------------------* 01080000
      *                 W O R K I N G  -  S T O R A G E               * 01090000
      *---------------------------------------------------------------* 01100000
      *                                                                 01110000
       WORKING-STORAGE SECTION.                                         01120000
      *                                                                 01130000
      *                                                                 01139201
       01  TOP-LECTURE-FHV030     PIC 9    VALUE 1.                     01139301
           88  DEBUT-FHV030                VALUE 1.                     01139401
           88  FIN-FHV030                  VALUE 2.                     01139501
       01  W-FHV030.                                                    01139700
           05 FHV030-RVGS4001.                                          01139801
              10  FHV030-NSOCORIG                                       01139901
                  PIC X(0003).                                          01140001
              10  FHV030-NLIEUORIG                                      01140101
                  PIC X(0003).                                          01140201
              10  FHV030-NSSLIEUORIG                                    01140301
                  PIC X(0003).                                          01140401
              10  FHV030-NSOCDEST                                       01140501
                  PIC X(0003).                                          01140601
              10  FHV030-NLIEUDEST                                      01140701
                  PIC X(0003).                                          01140801
              10  FHV030-NSSLIEUDEST                                    01140901
                  PIC X(0003).                                          01141001
              10  FHV030-NCODIC                                         01141101
                  PIC X(0007).                                          01141201
              10  FHV030-WSTOCKMASQ                                     01141301
                  PIC X(0001).                                          01141401
              10  FHV030-NSOCVTE                                        01141501
                  PIC X(0003).                                          01141601
              10  FHV030-NLIEUVTE                                       01141701
                  PIC X(0003).                                          01141801
              10  FHV030-NORIGINE                                       01141901
                  PIC X(0007).                                          01142001
              10  FHV030-CMODDEL                                        01142101
                  PIC X(0003).                                          01142201
              10  FHV030-DMVT                                           01142301
                  PIC X(0008).                                          01142401
              10  FHV030-DHMVT                                          01142501
                  PIC X(0002).                                          01142601
              10  FHV030-DMMVT                                          01142701
                  PIC X(0002).                                          01142801
              10  FHV030-CPROG                                          01142901
                  PIC X(0005).                                          01143001
              10  FHV030-COPER                                          01143101
                  PIC X(0010).                                          01143201
              10  FHV030-DOPER                                          01143301
                  PIC X(0008).                                          01143401
              10  FHV030-QMVT                                           01143501
                  PIC S9(5) COMP-3.                                     01143601
              10  FHV030-DSYST                                          01143701
                  PIC S9(13) COMP-3.                                    01143801
              10  FHV030-CINSEE                                         01143901
                  PIC X(0005).                                          01144001
              10  FHV030-CVENDEUR                                       01144101
                  PIC X(0006).                                          01144201
              10  FHV030-PVTOTAL                                        01144301
                  PIC S9(7)V9(0002) COMP-3.                             01144401
              10  FHV030-DCREATION                                      01144501
                  PIC X(0008).                                          01144601
              10  FHV030-CLIEUTRTORIG                                   01144701
                  PIC X(0005).                                          01144801
              10  FHV030-CLIEUTRTDEST                                   01144901
                  PIC X(0005).                                          01145001
              10  FHV030-DSTAT                                          01145101
                  PIC X(0006).                                          01145201
              10  FHV030-PVTOTALSR                                      01145301
                  PIC S9(7)V9(0002) COMP-3.                             01145401
              10  FHV030-NCODICGRP                                      01145501
                  PIC X(0007).                                          01145601
              10  FHV030-NSEQ                                           01145701
                  PIC X(0002).                                          01145801
           05 W-PVTOTAL1        PIC S9(7)V99 COMP-3.                    01146401
      *                                                                 01146501
       01  TOP-LECTURE-FHV031     PIC 9    VALUE 1.                     01146601
           88  DEBUT-FHV031                VALUE 1.                     01146701
           88  FIN-FHV031                  VALUE 2.                     01146801
       01  W-FHV031.                                                    01146901
           05 FHV031-RVGS4001.                                          01147001
              10 FHV031-NSOCORIG PIC X(003).                            01147101
              10 FILLER          PIC X(006).                            01147201
              10 FHV031-NSOCDEST PIC X(003).                            01147301
              10 FILLER          PIC X(068).                            01147401
              10 FHV031-DSYST    PIC S9(13) COMP-3.                     01147501
              10 FILLER          PIC X(054).                            01147601
           05 W-PVTOTAL2         PIC S9(7)V99 COMP-3.                   01147701
           05 FHV031-NSOCO       PIC X(003).                            01147801
           05 FHV031-NLIEUO      PIC X(003).                            01147901
           05 FHV031-NSSLIEUO    PIC X(001).                            01148001
           05 FHV031-CTRTO       PIC X(005).                            01148101
           05 FHV031-NSOCD       PIC X(003).                            01148201
           05 FHV031-NLIEUD      PIC X(003).                            01148301
           05 FHV031-NSSLIEUD    PIC X(001).                            01148401
           05 FHV031-CTRTD       PIC X(005).                            01148501
           05 FHV031-NSOCV       PIC X(003).                            01148601
           05 FHV031-NLIEUV      PIC X(003).                            01148701
      *                                                                 01148801
       01  W-RTGS40.                                                    01148901
           02 W-VUE-RVGS4001 PIC X(141).                                01149001
           02 W-PVTOTAL      PIC S9(7)V99 COMP-3.                       01149101
      *                                                                 01149201
       01  W-FNSOC.                                                     01149301
           02 W-NSOC          PIC X(03).                                01149401
           02 FILLER          PIC X(77).                                01149501
      *                                                                 01149601
       01  W-FPARAM.                                                    01149701
           02 W-COPER         PIC X(10).                                01149801
           02 FILLER          PIC X(80).                                01149901
      *                                                                 01150001
       01  W-COMPTEURS.                                                 01150101
           05 CPT-FHV030   PIC 9(7) VALUE 0.                            01150201
           05 CPT-FHV031   PIC 9(7) VALUE 0.                            01150301
           05 CPT-ECRIT    PIC 9(7) VALUE 0.                            01150401
      *                                                                 01151001
       01  W-DSYST         PIC S9(13) COMP-3.                           01160001
      *------------------------------------------------                 02190000
      *                 COPIE                                           02200000
      *------------------------------------------------                 02210000
        COPY ABENDCOP.                                                  02220000
      * COPY WORKDATC.                                                  02230001
      *                                                                 02240000
       PROCEDURE DIVISION.                                              02250000
      *                                                                 02260000
      ***************************************************************** 02270000
      *              T R A M E   DU   P R O G R A M M E               * 02280000
      ***************************************************************** 02290000
      *                                                               * 02300000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 02310000
      *                                                               * 02320000
      *           ------------------------------------------          * 02330000
      *           --  MODULE DE BASE DU PROGRAMME  BYHV36 --          * 02340001
      *           ------------------------------------------          * 02350000
      *                               I                               * 02360000
      *           -----------------------------------------           * 02370000
      *           I                   I                   I           * 02380000
      *   -----------------   -----------------   -----------------   * 02390000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 02400000
      *   -----------------   -----------------   -----------------   * 02410000
      *                                                               * 02420000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 02430000
      *                                                                 02440000
       MODULE-BYHV36.                                                   02450001
      *                                                                 02460000
           PERFORM  DEBUT-BYHV36.                                       02470001
           PERFORM  TRAIT-BYHV36 UNTIL FIN-FHV030.                      02480001
           PERFORM  FIN-BYHV36.                                         02490001
      *                                                                 02500000
      *---------------------------------------------------------------* 02510000
      *                    D E B U T        B H V 0 3 8               * 02520001
      *---------------------------------------------------------------* 02530000
      *                                                                 02540000
       DEBUT-BYHV36 SECTION.                                            02550001
      *---------------------                                            02560001
           OPEN INPUT  FHV030 FHV031 FNSOC FPARAM                       02570001
           OPEN OUTPUT FGS40 .                                          02580000
      *                                                                 02580101
           READ FNSOC INTO W-FNSOC AT END                               02580201
              DISPLAY 'MANQUE FICHIER FNSOC EN ENTREE'                  02580301
              PERFORM FIN-ANORMALE                                      02580401
           END-READ                                                     02580501
           DISPLAY 'SOCIETE : ' W-NSOC                                  02580601
      *                                                                 02580701
           READ FPARAM INTO W-FPARAM AT END                             02580801
              DISPLAY 'MANQUE FICHIER FPARAM EN ENTREE'                 02580901
              PERFORM FIN-ANORMALE                                      02581001
           END-READ                                                     02581101
           DISPLAY 'CODE OPERATION A EXCLURE : ' W-COPER                02581201
      *                                                                 02581301
           INITIALIZE W-FHV030 W-FHV031 W-RTGS40                        02581401
           PERFORM LECTURE-FHV030.                                      02581501
      *    DISPLAY ' PREMIERE LECTURE FHV030'                           02581601
           PERFORM LECTURE-FHV031.                                      02582000
      *    DISPLAY ' PREMIERE LECTURE FHV031'                           02582101
           IF FIN-FHV031                                                02583000
              DISPLAY 'FICHIER DES MVTS FHV031 VIDE'                    02583100
           END-IF.                                                      02584000
                                                                        02590000
      *                                                                 02880000
      *---------------------------------------------------------------* 02890000
      *                    T R A I T        B H V 0 3 8               * 02900000
      *---------------------------------------------------------------* 02910000
      *                                                                 02920000
       TRAIT-BYHV36 SECTION.                                            02930001
      *---------------------                                            02940001
      *    DISPLAY 'LECTURE FHV030 ' W-FHV030.                          02950001
      *    DISPLAY 'LECTURE FHV031 ' W-FHV031.                          02951001
           IF FHV030-RVGS4001 = FHV031-RVGS4001                         02960001
      *  ON NE GARDE QUE LES MOUVEMENTS AYANT EU LIEU AVANT LE BASCULE  02960101
      *  VERS LA SOCIETE QUE L'ON FUSIONNE                              02960201
      *       DISPLAY 'EGALITE BOUCLE'                                  02960301
              IF FHV031-NSOCO = W-NSOC                                  02960401
              OR FHV031-NSOCD = W-NSOC                                  02960501
      *          TRANSCO INVERSE                                        02960601
                 MOVE FHV031-NSOCO    TO FHV030-NSOCORIG                02960701
                 MOVE FHV031-NLIEUO   TO FHV030-NLIEUORIG               02960801
                 MOVE FHV031-NSSLIEUO TO FHV030-NSSLIEUORIG             02960901
                 MOVE FHV031-CTRTO    TO FHV030-CLIEUTRTORIG            02961001
                 MOVE FHV031-NSOCD    TO FHV030-NSOCDEST                02961101
                 MOVE FHV031-NLIEUD   TO FHV030-NLIEUDEST               02961201
                 MOVE FHV031-NSSLIEUD TO FHV030-NSSLIEUDEST             02961301
                 MOVE FHV031-CTRTD    TO FHV030-CLIEUTRTDEST            02961401
                 MOVE FHV031-NSOCV    TO FHV030-NSOCVTE                 02961501
                 MOVE FHV031-NLIEUV   TO FHV030-NLIEUVTE                02961601
                 PERFORM ECRITURE-FGS40                                 02961701
      *          DISPLAY 'ECRITURE 1'                                   02961801
              END-IF                                                    02961901
              PERFORM LECTURE-FHV030                                    02962001
              PERFORM LECTURE-FHV031                                    02962100
           ELSE                                                         02963000
              IF FHV030-RVGS4001 < FHV031-RVGS4001                      02964001
      *  ON EXCLUT LA PREMIERE REGULARISATION DE STOCK DEF PAR LE       02964101
      *  COPER DE LA CARTE FPARAM ET TRANSCODIFICATION SUPERMUT         02964201
      *          DISPLAY '< BOUCLE'                                     02964301
                 IF FHV030-COPER NOT = W-COPER                          02964401
NT                  IF  FHV030-NSOCORIG  = '961'                        02964501
NT                  AND FHV030-NLIEUORIG = '994'                        02964601
NT                      MOVE '994' TO FHV030-NSOCORIG                   02964701
NT                      MOVE '961' TO FHV030-NLIEUORIG                  02964801
NT                  END-IF                                              02964901
                    PERFORM ECRITURE-FGS40                              02965001
      *             DISPLAY 'ECRITURE 2'                                02965101
                 END-IF                                                 02965201
                 PERFORM LECTURE-FHV030                                 02965301
              ELSE                                                      02966000
      *          DISPLAY '> BOUCLE'                                     02966101
                 PERFORM LECTURE-FHV031                                 02967000
              END-IF                                                    02968000
           END-IF.                                                      02969001
                                                                        03630000
      *---------------------------------------------------------------* 03640000
      *                   F I N        B H V 0 3 0                    * 03650000
      *---------------------------------------------------------------* 03660000
                                                                        03670000
       FIN-BYHV36 SECTION.                                              03680001
      *-------------------                                              03690001
           CLOSE FHV030 FHV031 FGS40 FNSOC FPARAM.                      03700001
           DISPLAY '**********************************************'.    03790000
           DISPLAY '*    PROGRAMME BYHV36  TERMINE NORMALEMENT   *'.    03800001
           DISPLAY '*                                            *'.    03810000
           DISPLAY '**********************************************'.    03820001
           DISPLAY '*                                            *'.    03830001
           DISPLAY '*  ENREGISTREMENTS LUS SUR FHV030    = ' CPT-FHV030 03840001
           DISPLAY '*  ENREGISTREMENTS LUS SUR FHV031    = ' CPT-FHV031 03850001
           DISPLAY '*  ENREGISTREMENTS ECRITS SUR FGS40  = ' CPT-ECRIT  03860001
           DISPLAY '*                                            *'.    03890001
           DISPLAY '**********************************************'.    03891001
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    03900000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 03910000
      *                                                                 03950000
      *                                                                 03950101
      *------------------------                                         03951000
       LECTURE-FHV030  SECTION.                                         03952000
      *------------------------                                         03953000
           READ FHV030 INTO W-FHV030                                    03954000
                AT END SET FIN-FHV030 TO TRUE.                          03955000
           IF NOT FIN-FHV030                                            03956000
              ADD  1             TO CPT-FHV030                          03957001
              MOVE FHV030-DSYST  TO W-DSYST                             03957101
              MOVE 0             TO FHV030-DSYST                        03957201
           ELSE                                                         03957301
              MOVE HIGH-VALUE TO W-FHV030                               03957401
           END-IF.                                                      03958000
      *    DISPLAY 'LECTURE FHV030 ' W-FHV030.                          03958101
      *                                                                 03959000
      *------------------------                                         03959100
       LECTURE-FHV031  SECTION.                                         03959200
      *------------------------                                         03959300
           READ FHV031 INTO W-FHV031                                    03959400
                AT END SET FIN-FHV031 TO TRUE.                          03959500
           IF NOT FIN-FHV031                                            03959600
              ADD  1  TO CPT-FHV031                                     03959700
              MOVE 0  TO FHV031-DSYST                                   03959801
           ELSE                                                         03959900
              MOVE HIGH-VALUE TO W-FHV031                               03960000
           END-IF.                                                      03960100
      *    DISPLAY 'LECTURE FHV031 ' W-FHV031.                          03960201
      *                                                                 03960300
      *------------------------                                         03960400
       ECRITURE-FGS40  SECTION.                                         03960500
      *------------------------                                         03960600
           MOVE W-DSYST       TO FHV030-DSYST                           03960701
           MOVE W-FHV030      TO W-RTGS40                               03960800
           WRITE FGS40-RECORD FROM W-RTGS40                             03960900
           ADD 1 TO CPT-ECRIT.                                          03961001
      *    DISPLAY 'ECRITURE FGS40 ' W-RTGS40.                          03961101
      *                                                                 03962000
      *---------------------------------------------------------------* 05880000
      *         FIN ANORMALE                                          * 05890000
      *---------------------------------------------------------------* 05900000
       FIN-ANORMALE SECTION.                                            05910001
      *---------------------                                            05920001
           CLOSE FHV030 FHV031 FGS40 FNSOC FPARAM.                      05921001
           MOVE 'BYHV36' TO ABEND-PROG.                                 05930001
           CALL 'ABEND' USING                                           05940000
                        ABEND-PROG                                      05950000
                        ABEND-MESS.                                     05960000
      *                                                                 05970000
