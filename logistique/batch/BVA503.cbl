      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.   BVA503.                                            00000020
       AUTHOR.       DSA015.                                            00000030
                                                                        00000040
      * ------------------------------------------------------------ *  00000050
      *                                                              *  00000060
      *   PROJET           :                                         *  00000070
      *   PROGRAMME        : BVA503.                                 *  00000080
      *   PERIODICITE      :                                         *  00000090
      *   FONCTION         : CREATION FICHIER POUR LOAD RTVA20       *  00000100
      *                     NCODIC / NSEQPRO / LSEQPRO / DSYST                  
      *   DATE DE CREATION : 08/02/1993.                             *  00000110
      *                                                              *  00000120
      * ------------------------------------------------------------ *  00000130
                                                                        00000140
       ENVIRONMENT DIVISION.                                            00000150
       CONFIGURATION SECTION.                                           00000160
       INPUT-OUTPUT SECTION.                                            00000170
       FILE-CONTROL.                                                    00000180
                                                                        00000190
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FEX001T  ASSIGN TO  FEX001T.                         00000200
      *--                                                                       
           SELECT  FEX001T  ASSIGN TO  FEX001T                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  RTVA20   ASSIGN TO  RTVA20.                          00000210
      *                                                                         
      *--                                                                       
           SELECT  RTVA20   ASSIGN TO  RTVA20                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000220
      *}                                                                        
       DATA DIVISION.                                                   00000230
       FILE SECTION.                                                    00000240
                                                                        00000250
      * ------------------------------------------------------------ *  00000260
      *               D E S C R I P T I O N   F I C H I E R          *  00000270
      * ------------------------------------------------------------ *  00000280
                                                                        00000290
       FD  FEX001T                                                      00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-FEX001T PIC X(44).                                       00000340
                                                                        00000360
       FD  RTVA20                                                       00000300
           RECORDING F                                                  00000310
           BLOCK 0 RECORDS                                              00000320
           LABEL RECORD STANDARD.                                       00000330
       01  ENR-RTVA20  PIC X(44).                                       00000340
                                                                        00000360
                                                                        00000450
      * ------------------------------------------------------------ *  00000460
      *                W O R K I N G - S T O R A G E                 *  00000470
      * ------------------------------------------------------------ *  00000480
                                                                        00000490
       WORKING-STORAGE SECTION.                                         00000500
                                                                        00000510
      *   LONGUEUR 047                                                  00000520
                                                                        00000530
           COPY RVVA2000.                                               00000550
                                                                        00000600
                                                                        00000600
       77  CPT-RTVA20                PIC S9(05)   COMP-3  VALUE 0.      00000810
       77  CPT-FEX001T               PIC S9(05)   COMP-3  VALUE 0.      00000810
                                                                        00000980
       01  DATHEUR                   PIC S9(13) COMP-3.                 21730300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WRET                      PIC S9(04) COMP.                   01020000
      *--                                                                       
       01  WRET                      PIC S9(04) COMP-5.                         
      *}                                                                        
       01  CODE-FEX001T              PIC X(01) VALUE '0'.               00000990
           88 PAS-FIN-FEX001T        VALUE '0'.                         00001000
           88 FIN-FEX001T            VALUE '1'.                         00001010
                                                                        00001020
                                                                        00001210
      * ------------------------------------------------------------ *  00001220
      *         DESCRIPTION DES ZONES D'APPEL DU MODULE ABEND        *  00001230
      * ------------------------------------------------------------ *  00001240
                                                                        00001250
           COPY ABENDCOP.                                               00001260
                                                                        00001270
                                                                        00001400
      * ------------------------------------------------------------ *  00001370
      *              P R O C E D U R E     D I V I S I O N           *  00001380
      * ------------------------------------------------------------ *  00001390
       PROCEDURE DIVISION.                                              00001410
                                                                        00001420
      * ------------------------------------------------------------ *  00001430
      *             T R A M E     D U     P R O G R A M M E          *  00001440
      * ------------------------------------------------------------ *  00001450
      *                                                              *  00001460
      *            -----------------------------------------         *  00001470
      *            -  MODULE DE BASE DU PROGRAMME  BVA503  -         *  00001480
      *            -----------------------------------------         *  00001490
      *                               I                              *  00001500
      *            -----------------------------------------         *  00001510
      *            I                  I                    I         *  00001520
      *       -----------      ----------------        ---------     *  00001530
      *       -  DEBUT  -      -  TRAITEMENT  -        -  FIN  -     *  00001540
      *       -----------      ----------------        ---------     *  00001550
      *                                                              *  00001560
      * ------------------------------------------------------------ *  00001570
                                                                        00001580
           PERFORM DEBUT-BVA503.                                        00001590
           PERFORM LECTURE-FEX001T.                                     00001970
           PERFORM TRAIT-BVA503 UNTIL FIN-FEX001T.                      00001600
           PERFORM FIN-BVA503.                                          00001610
                                                                        00001620
      * ------------------------------------------------------------ *  00001630
      *                     D E B U T    B V A 5 0 3                 *  00001640
      * ------------------------------------------------------------ *  00001650
                                                                        00001660
       DEBUT-BVA503 SECTION.                                            00001670
                                                                        00001680
                                                                        00001730
           OPEN INPUT  FEX001T.                                                 
           OPEN OUTPUT RTVA20.                                                  
           CALL 'SPDATDB2' USING WRET DATHEUR.                          21724000
           IF WRET NOT = ZERO                                           21725000
              MOVE 'ERREUR APPEL MODULE SPDATDB2' TO ABEND-MESS         21726000
              GO TO FIN-ANORMALE                                        21727000
           END-IF.                                                      21728000
           INITIALIZE RVVA2000.                                                 
                                                                        00001750
       FIN-DEBUT-BVA503. EXIT.                                          00001930
                                                                        00001940
      * -------------------- LECTURE FICHIER FEX001T --------------- *  00001950
                                                                        00001960
       LECTURE-FEX001T  SECTION.                                        00001970
                                                                        00001980
           READ FEX001T INTO RVVA2000 AT END                            00001990
                SET FIN-FEX001T TO TRUE                                 00002000
           END-READ.                                                    00002010
           IF NOT FIN-FEX001T                                           00002000
              ADD 1            TO CPT-FEX001T                           00002020
           END-IF.                                                              
                                                                        00002030
       FIN-LECTURE-FEX001T. EXIT.                                       00002040
                                                                        00002050
                                                                        00002150
      * ------------------------------------------------------------ *  00002160
      *                     T R A I T    B R E 0 0 0                 *  00002170
      * ------------------------------------------------------------ *  00002180
                                                                        00002190
       TRAIT-BVA503 SECTION.                                            00002200
                                                                        00002210
           PERFORM ECRITURE-RTVA20.                                     02390   
           PERFORM LECTURE-FEX001T.                                     00002490
      *                                                                 00002520
                                                                        00002530
       FIN-TRAIT-BVA503. EXIT.                                          00002540
                                                                        00002550
                                                                        00003350
      * ------------------------------------------------------------ *  00003360
      *               CHARGEMENT LIGNE FICHIER EXTRACTION            *  00003370
      * ------------------------------------------------------------ *  00003380
                                                                        00003390
       ECRITURE-RTVA20 SECTION.                                                 
                                                                        00003410
           MOVE DATHEUR     TO VA20-DSYST                               00003740
           WRITE ENR-RTVA20 FROM RVVA2000.                              00003750
                                                                        00003760
           ADD 1 TO CPT-RTVA20.                                         00003770
           INITIALIZE RVVA2000.                                                 
                                                                        00003780
                                                                        00003790
       FIN-ECRITURE-RTVA20. EXIT.                                               
      * ------------------------ FIN-ANORMALE ---------------------- *  00004110
                                                                        00004120
       FIN-ANORMALE SECTION.                                            00004130
                                                                        00004140
           DISPLAY '*****************************************'.         00004150
           DISPLAY '***** FIN ANORMALE DU PROGRAMME *********'.         00004160
           DISPLAY '*****************************************'.         00004170
           CLOSE RTVA20 FEX001T.                                        00004180
           MOVE 'BVA503' TO ABEND-PROG.                                 00004190
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    00004200
                                                                        00004210
       FIN-FIN-ANORMALE. EXIT.                                          00004220
                                                                        00004230
      * ------------------------------------------------------------ *  00003820
      *                      F I N     B V A 5 0 3                   *  00003830
      * ------------------------------------------------------------ *  00003840
                                                                        00003850
       FIN-BVA503 SECTION.                                              00003860
                                                                        00003870
           CLOSE FEX001T RTVA20.                                        00003920
           PERFORM COMPTE-RENDU.                                        00003930
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003940
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        00003950
       FIN-FIN-BVA503. EXIT.                                            00003960
                                                                        00003970
      * ------------------------ COMPTE-RENDU ---------------------- *  00003980
                                                                        00003990
       COMPTE-RENDU SECTION.                                            00004000
                                                                        00004010
           DISPLAY '**'.                                                00004020
           DISPLAY '**          B V A 5 0 3 '.                          00004030
           DISPLAY '**'.                                                00004040
           DISPLAY '** NOMBRE DE LECTURES FEX001T: '  CPT-FEX001T.      00004050
           DISPLAY '** NOMBRE D''ECRITURES RTVA20 : ' CPT-RTVA20.       00004060
           DISPLAY '**'.                                                00004070
                                                                        00004080
       FIN-COMPTE-RENDU. EXIT.                                          00004090
