      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
      *                                                                         
       PROGRAM-ID.  BIE020.                                                     
       AUTHOR.  DSA014.                                                         
       DATE-WRITTEN.  18/07/90.                                                 
      *                                                                         
      *************************************************************             
      *                                                           *             
      *   EDITION DU STOCK THEORIQUE DU SOUS-LIEU PRET            *             
      *                                                           *             
      *************************************************************             
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           C01           IS SAUT-PAGE.                                          
      *                                                                         
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIE020  ASSIGN TO FIE020.                                     
      *--                                                                       
           SELECT FIE020  ASSIGN TO FIE020                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IFI020  ASSIGN TO IFI020.                                     
      *--                                                                       
           SELECT IFI020  ASSIGN TO IFI020                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
      *                                                                         
       FILE SECTION.                                                            
      *                                                                         
      *                                                                 00097055
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE FIE020                         * 00099058
      *---------------------------------------------------------------* 00100055
       FD   FIE020                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FIE020.                                                         
            03  FILLER      PIC X(088).                                         
      *                                                                 00097055
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE IFI020                         * 00099058
      *---------------------------------------------------------------* 00100055
       FD   IFI020                                                              
            LABEL RECORD OMITTED.                                               
       01   ENR-IFI020.                                                         
            03  FILLER      PIC X(133).                                         
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                 W O R K I N G  -  S T O R A G E               *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                                 
      *------------------------                                                 
      *                                                                         
      *===============================================================*         
      *                 DESCRIPTION DE FIE020                         *         
      *===============================================================*         
       01  FIE020-ENREG.                                                        
           05  FIE020-NSOCDEPOT              PIC X(3).                          
           05  FIE020-NDEPOT                 PIC X(3).                          
           05  FIE020-NCODIC                 PIC X(7).                          
           05  FIE020-WTYPPRET               PIC X(1).                          
           05  FIE020-QPRET                  PIC S9(5) COMP-3.                  
           05  FIE020-DPRET                  PIC X(8).                          
           05  FIE020-LCOMMENTPRET           PIC X(30).                         
           05  FIE020-CFAM                   PIC X(5).                          
           05  FIE020-CMARQ                  PIC X(5).                          
           05  FIE020-LREFFOURN              PIC X(20).                         
           05  FIE020-WSEQFAM                PIC S9(5)      COMP-3.             
      *                                                                         
      *                                                                         
      *                                                                         
       01  ETAT-FIE020                   PIC X.                                 
           88  FIE020-FINI               VALUE '0'.                             
           88  FIE020-NON-FINI           VALUE '1'.                             
      *                                                                         
       01  W-DATE.                                                              
           02 W-DATE-SS                  PIC X(02).                             
           02 W-DATE-AA                  PIC X(02).                             
           02 W-DATE-MM                  PIC X(02).                             
           02 W-DATE-JJ                  PIC X(02).                             
       01  W-DATE-EDIT.                                                         
           02 W-DATE-EDIT-JJ             PIC X(02).                             
           02 W-SLAH1                    PIC X       VALUE '/'.                 
           02 W-DATE-EDIT-MM             PIC X(02).                             
           02 W-SLAH2                    PIC X       VALUE '/'.                 
           02 W-DATE-EDIT-AA             PIC X(02).                             
      *                                                                         
       01  W-LIGNES-FIE020-LUES          PIC S9(9) COMP-3  VALUE +0.            
       01  W-LIGNES-IFI020-ECRITES       PIC S9(9) COMP-3  VALUE +0.            
      *                                                                         
      *---------------------------------------------------------------*         
      *              LIGNES DE L'ETAT IFI020                          *         
      *---------------------------------------------------------------*         
      *                                                                         
      *01  SAUT-PAGE                 PIC X VALUE '1'.                           
      *01  SAUT-LIGNE                PIC X VALUE '-'.                           
      *                                                                         
       01  LIGNE-TIRETS.                                                        
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(105) VALUE ALL '-'.                 
           02  FILLER                 PIC X(1) VALUE '!'.                       
           02  FILLER                 PIC X(25) VALUE SPACES.                   
      *                                                                         
      *                                                                         
       01  LIGNE-RESULTAT.                                                      
           02  FILLER                 PIC X(4) VALUE ' !  '.                    
           02  ETAT-CFAM              PIC X(5).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-CMARQ             PIC X(5).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-REFERENCE         PIC X(20).                                
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-NCODIC            PIC X(7).                                 
           02  FILLER                 PIC X(5) VALUE '  !  '.                   
           02  ETATX-QPRET.                                                     
               03  ETAT-QPRET         PIC Z(4)9.                                
           02  FILLER                 PIC X(5) VALUE '  !  '.                   
           02  ETAT-DPRET             PIC X(8).                                 
           02  FILLER                 PIC X(5) VALUE '  !  '.                   
           02  ETAT-LCOMMENTPRET      PIC X(30).                                
           02  FILLER                 PIC X(3) VALUE '  !'.                     
           02  FILLER                 PIC X(25) VALUE SPACES.                   
      *                                                                         
       01  LIGNE-ENTETE1.                                                       
           02  FILLER                 PIC X VALUE SPACE.                        
           02  FILLER                 PIC X(6) VALUE 'JIE020'.                  
           02  FILLER                 PIC X(33) VALUE SPACES.                   
           02  FILLER                 PIC X(36)                                 
                VALUE 'STOCK THEORIQUE DU SOUS-LIEU "PRETS"'.                   
           02  FILLER                 PIC X(17)                                 
                VALUE ' PAR TYPE DE PRET'.                                      
           02  FILLER                 PIC X(26) VALUE SPACES.                   
           02  FILLER                 PIC X(07) VALUE 'PAGE : '.                
           02  ENTETE-PAGE            PIC X(3).                                 
           02  FILLER                 PIC X(4) VALUE SPACES.                    
      *                                                                         
       01  LIGNE-ENTETE2.                                                       
           02  FILLER                 PIC X VALUE ' '.                          
           02  FILLER                 PIC X(7) VALUE 'DATE : '.                 
           02  ENTETE-DATE            PIC X(8).                                 
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(7) VALUE 'TIME : '.                 
           02  ENTETE-TIME            PIC X(5).                                 
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(10) VALUE 'SOCIETE : '.             
           02  ENTETE-SOCIETE         PIC X(3).                                 
           02  FILLER                 PIC X(86) VALUE SPACES.                   
      *                                                                         
       01  LIGNE-ENTETE3.                                                       
           02  FILLER                 PIC X VALUE ' '.                          
           02  FILLER                 PIC X(11) VALUE 'ENTREPOT : '.            
           02  ENTETE-ENTREPOT        PIC X(3).                                 
           02  FILLER                 PIC X(118) VALUE SPACES.                  
      *                                                                         
       01  LIGNE-ENTETE4.                                                       
           02  FILLER                 PIC X VALUE ' '.                          
           02  FILLER                 PIC X(15) VALUE 'TYPE DE PRET : '.        
           02  ENTETE-TYPPRET         PIC X(3).                                 
           02  FILLER                 PIC X(114) VALUE SPACES.                  
      *                                                                         
       01  LIGNE-VIDE.                                                          
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(41) VALUE SPACES.                   
           02  FILLER                 PIC X(6) VALUE '   !  '.                  
           02  FILLER                 PIC X(4) VALUE  SPACES.                   
           02  FILLER                 PIC X(7) VALUE '   !   '.                 
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(8) VALUE '     !  '.                
           02  FILLER                 PIC X(30) VALUE SPACES.                   
           02  FILLER                 PIC X(3) VALUE '  !'.                     
           02  FILLER                 PIC X(25) VALUE SPACES.                   
      *                                                                         
       01  LIGNE1.                                                              
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(41) VALUE SPACES.                   
           02  FILLER                 PIC X(6) VALUE '   !  '.                  
           02  FILLER                 PIC X(4) VALUE ' QTE'.                    
           02  FILLER                 PIC X(7) VALUE '   !   '.                 
           02  FILLER                 PIC X(4) VALUE 'DATE'.                    
           02  FILLER                 PIC X(8) VALUE '     !  '.                
           02  FILLER                 PIC X(11) VALUE 'COMMENTAIRE'.            
           02  FILLER                 PIC X(19) VALUE SPACES.                   
           02  FILLER                 PIC X(3) VALUE '  !'.                     
           02  FILLER                 PIC X(25) VALUE SPACES.                   
      *                                                                         
      *                                                                         
       01  LIGNE2.                                                              
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(3) VALUE 'FAM'.                     
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(6) VALUE 'MARQUE'.                  
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(9) VALUE 'REFERENCE'.               
           02  FILLER                 PIC X(11) VALUE SPACES.                   
           02  FILLER                 PIC X(5) VALUE 'CODIC'.                   
           02  FILLER                 PIC X(6) VALUE '   !  '.                  
           02  FILLER                 PIC X(4) VALUE 'PRET'.                    
           02  FILLER                 PIC X(7) VALUE '   !   '.                 
           02  FILLER                 PIC X(4) VALUE 'PRET'.                    
           02  FILLER                 PIC X(8) VALUE '     !  '.                
           02  FILLER                 PIC X(4) VALUE 'PRET'.                    
           02  FILLER                 PIC X(26) VALUE SPACES.                   
           02  FILLER                 PIC X(3) VALUE '  !'.                     
           02  FILLER                 PIC X(25) VALUE SPACES.                   
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
       01  NLIGNE                    PIC 9(2).                                  
       01  NLIGNE-INIT               PIC 9(2) VALUE 15.                         
       01  NLIGNE-MAX                PIC 9(2) VALUE 50.                         
       01  W-PAGE                    PIC 9(3).                                  
       01  NSOCDEPOT-COURANT         PIC X(3).                                  
       01  NDEPOT-COURANT            PIC X(3).                                  
       01  WTYPPRET-COURANT          PIC X(3).                                  
      *                                                                         
       01  W1-DATE.                                                             
           02  W1-AA                     PIC XX.                                
           02  W1-MM                     PIC XX.                                
           02  W1-JJ                     PIC XX.                                
       01  W2-DATE.                                                             
           02  W2-JJ                     PIC XX.                                
           02  FILLER                    PIC X VALUE '/'.                       
           02  W2-MM                     PIC XX.                                
           02  FILLER                    PIC X VALUE '/'.                       
           02  W2-AA                     PIC XX.                                
      *                                                                         
       01  W-TIME                        PIC 9(8).                              
       01  W1-TIME       REDEFINES W-TIME.                                      
           02  W1-TIME-HH                PIC 99.                                
           02  W1-TIME-MM                PIC 99.                                
           02  W1-TIME-SS                PIC 99.                                
           02  W1-TIME-CC                PIC 99.                                
       01  W2-TIME.                                                             
           02  W2-TIME-HH                PIC 99.                                
           02  FILLER                    PIC X VALUE 'H'.                       
           02  W2-TIME-MM                PIC 99.                                
      *                                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E   D I V I S I O N               *         
      *****************************************************************         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *---------------------------------------------------------------*         
      *              D E B U T   B I E 0 2 0                          *         
      *---------------------------------------------------------------*         
      *                                                                         
      *==============================================================*          
      *                                                              *          
      *==============================================================*          
      *                                                                         
      *                                                                         
       MODULE-BIE020              SECTION.                                      
      *---------------                                                          
      *                                                                         
           PERFORM  DEBUT.                                                      
           PERFORM  TRAITEMENT.                                                 
           PERFORM  FIN.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-BIE020.  EXIT.                                                
              EJECT                                                             
      *                                                                         
      *===============================================================*         
      *                     D E B U T                                 *         
      *===============================================================*         
      *                                                                         
       DEBUT                 SECTION.                                           
      *------------------------------                                           
      *                                                                         
           DISPLAY  '************************************************'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '**   PROGRAMME  :  BIE020                     **'.         
           DISPLAY  '**   EDITION DU STOCK THEORIQUE DU SOUS-LIEU  **'.         
           DISPLAY  '**   PRET                                     **'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '************************************************'.         
           DISPLAY  '***     PROGRAMME DEBUTE NORMALEMENT         ***'.         
      *                                                                         
           OPEN  INPUT  FIE020                                                  
                 OUTPUT IFI020.                                                 
      *                                                                         
      *                                                                         
           ACCEPT W1-DATE FROM DATE.                                            
           MOVE W1-JJ TO W2-JJ.                                                 
           MOVE W1-MM TO W2-MM.                                                 
           MOVE W1-AA TO W2-AA.                                                 
           MOVE W2-DATE TO ENTETE-DATE.                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *            RECHERCHE DE L'HEURE                               *         
      *---------------------------------------------------------------*         
           ACCEPT W-TIME FROM TIME.                                             
           MOVE W1-TIME-HH TO W2-TIME-HH.                                       
           MOVE W1-TIME-MM TO W2-TIME-MM.                                       
           MOVE W2-TIME TO ENTETE-TIME.                                         
      *                                                                         
      *                                                                         
       FIN-DEBUT.                 EXIT.                                         
      *                                                                         
      *===============================================================*         
      *       F I N   N O R M A L E   D U   P R O G R A M M E         *         
      *===============================================================*         
      *                                                                         
       FIN                        SECTION.                                      
      *-----------------------------------                                      
      *                                                                         
           DISPLAY  '************************************************'.         
           DISPLAY  '***       FIN  NORMALE  DU PROGRAMME         ***'.         
           DISPLAY  '***                 BIE555                   ***'.         
           DISPLAY  '************************************************'.         
           DISPLAY  '** NBR DE LIGNES FIE020 LUES          : '          07480000
                    W-LIGNES-FIE020-LUES.                               07490000
           DISPLAY  '** NBR DE LIGNES IFI020 ECRITES       : '          07500000
                    W-LIGNES-IFI020-ECRITES                             07510000
      *                                                                         
           CLOSE  FIE020                                                        
                  IFI020.                                                       
      *                                                                         
       FIN-FIN.        EXIT.                                                    
      *                                                                         
      *==============================================================*          
      *                  T R A I T E M E N T                         *          
      *==============================================================*          
       TRAITEMENT    SECTION.                                                   
      *                                                                         
           MOVE ZERO TO  W-PAGE.                                                
           SET FIE020-NON-FINI TO TRUE.                                         
           PERFORM LIRE-FIE020.                                                 
           PERFORM TRAITER-PAGE UNTIL FIE020-FINI.                              
      *                                                                         
       FIN-TRAITEMENT.             EXIT.                                        
      *                                                                         
      *****************************************************************         
      *     PROCEDURE : TRAITER-PAGE                                  *         
      *                                                               *         
      *     FONCTION  : ECRITURE D'UNE PAGE D'EDITION                 *         
      *                                                               *         
      *****************************************************************         
       TRAITER-PAGE               SECTION.                                      
      *                                                                         
           ADD 1 TO W-PAGE.                                                     
      *                                                                         
           MOVE W-PAGE           TO ENTETE-PAGE.                                
           MOVE FIE020-NSOCDEPOT TO ENTETE-SOCIETE.                             
           MOVE FIE020-NDEPOT    TO ENTETE-ENTREPOT.                            
           MOVE FIE020-WTYPPRET  TO ENTETE-TYPPRET.                             
           MOVE FIE020-NSOCDEPOT TO NSOCDEPOT-COURANT.                          
           MOVE FIE020-NDEPOT    TO NDEPOT-COURANT.                             
           MOVE FIE020-WTYPPRET  TO WTYPPRET-COURANT.                           
      *                                                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *          ECRITURE DE L'ENTETE POUR UNE PAGE                   *         
      *---------------------------------------------------------------*         
           MOVE SPACES TO ENR-IFI020.                                           
           WRITE ENR-IFI020 AFTER SAUT-PAGE.                                    
           WRITE ENR-IFI020 FROM LIGNE-ENTETE1 AFTER 2.                         
           WRITE ENR-IFI020 FROM LIGNE-ENTETE2 AFTER 3.                         
           WRITE ENR-IFI020 FROM LIGNE-ENTETE3 AFTER 2.                         
           WRITE ENR-IFI020 FROM LIGNE-ENTETE4 AFTER 2.                         
           WRITE ENR-IFI020 FROM LIGNE-TIRETS  AFTER 3.                         
           WRITE ENR-IFI020 FROM LIGNE1.                                        
           WRITE ENR-IFI020 FROM LIGNE2.                                        
           WRITE ENR-IFI020 FROM LIGNE-VIDE.                                    
           WRITE ENR-IFI020 FROM LIGNE-TIRETS.                                  
           WRITE ENR-IFI020 FROM LIGNE-VIDE.                                    
      *                                                                         
           MOVE NLIGNE-INIT TO NLIGNE.                                          
      *---------------------------------------------------------------*         
      *        APPEL DE LA BOUCLE DE TRAITEMENT POUR UNE LIGNE        *         
      *---------------------------------------------------------------*         
           PERFORM TRAITER-LIGNE UNTIL FIE020-FINI                              
                        OR NLIGNE > NLIGNE-MAX                                  
                        OR FIE020-NSOCDEPOT NOT = NSOCDEPOT-COURANT             
                        OR FIE020-NDEPOT    NOT = NDEPOT-COURANT                
                        OR FIE020-WTYPPRET  NOT = WTYPPRET-COURANT.             
      *                                                                         
           WRITE ENR-IFI020 FROM LIGNE-TIRETS.                                  
      *                                                                         
       FIN-TRAITER-PAGE.           EXIT.                                        
      *                                                                         
      *****************************************************************         
      *        PROCEDURE : TRAITER-LIGNE                              *         
      *                                                               *         
      *        FONCTION  : ECRITURE D'UNE LIGNE RESULTAT              *         
      *                                                               *         
      *****************************************************************         
       TRAITER-LIGNE              SECTION.                                      
      *                                                                         
      *                                                                         
           MOVE FIE020-NCODIC         TO   ETAT-NCODIC.                         
           MOVE FIE020-QPRET          TO   ETAT-QPRET.                          
           MOVE FIE020-DPRET          TO   W-DATE.                              
           MOVE W-DATE-JJ             TO W-DATE-EDIT-JJ.                        
           MOVE W-DATE-MM             TO W-DATE-EDIT-MM.                        
           MOVE W-DATE-AA             TO W-DATE-EDIT-AA.                        
           MOVE W-DATE-EDIT           TO   ETAT-DPRET.                          
           MOVE FIE020-LCOMMENTPRET   TO   ETAT-LCOMMENTPRET.                   
           MOVE FIE020-CFAM           TO   ETAT-CFAM.                           
           MOVE FIE020-CMARQ          TO   ETAT-CMARQ.                          
           MOVE FIE020-LREFFOURN      TO   ETAT-REFERENCE.                      
      *                                                                         
      *---------------------------------------------------------------*         
      *        ECRITURE DU FICHIER IFI020                             *         
      *---------------------------------------------------------------*         
           WRITE ENR-IFI020 FROM LIGNE-RESULTAT.                                
      *                                                                         
      *                                                                         
           WRITE ENR-IFI020 FROM LIGNE-VIDE.                                    
           ADD 2 TO NLIGNE.                                                     
      *                                                                         
      *---------------------------------------------------------------*         
      *        LECTURE DE L'ENREGISTREMENT SUIVANT DE FIE020          *         
      *---------------------------------------------------------------*         
           PERFORM LIRE-FIE020.                                                 
      *                                                                         
      *                                                                         
       FIN-TRAITER-LIGNE.            EXIT.                                      
      *                                                                         
      *****************************************************************         
      *        PROCEDURE : LIRE-FIE020                                *         
      *                                                               *         
      *        FONCTION  : LECTURE D'UN ENREGISTREMENT DE FIE020      *         
      *                                                               *         
      *****************************************************************         
       LIRE-FIE020                  SECTION.                                    
      *                                                                         
           READ   FIE020 INTO   FIE020-ENREG   AT END                           
                SET FIE020-FINI TO TRUE.                                        
           IF FIE020-NON-FINI                                                   
                ADD 1 TO W-LIGNES-FIE020-LUES                                   
           END-IF.                                                              
      *                                                                         
       FIN-LIRE-FIE020.             EXIT.                                       
      *                                                                         
      *                                                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *             A B A N D O N - P R O G R A M M E                 *         
      *---------------------------------------------------------------*         
      *                                                                         
       ABANDON-PROGRAMME        SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           CLOSE  FIE020.                                                       
           CLOSE  IFI020.                                                       
      *                                                                         
           DISPLAY '***************************************'.                   
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.                   
           DISPLAY '***************************************'.                   
      *                                                                         
           MOVE  'BIE020'  TO    ABEND-PROG.                                    
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
      *                                                                         
       FIN-ABANDON-PROGRAMME. EXIT.                                             
          EJECT                                                                 
      *                                                                         
