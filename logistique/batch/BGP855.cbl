      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BGP855                                                    
       AUTHOR. DSA079.                                                          
      *------------------------------------------------------------*            
      *                PROGRAMME D'EDITION DE L'ETAT               *            
      *                      REAPPRO FOURNISSEUR DACEM             *            
      *                                                            *            
      * DATE CREATION : 07/08/02                                   *            
      *------------------------------------------------------------*            
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGP849  ASSIGN TO FGP849.                                     
      *--                                                                       
           SELECT FGP849  ASSIGN TO FGP849                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IGP855 ASSIGN TO IGP855.                                      
      *--                                                                       
           SELECT IGP855 ASSIGN TO IGP855                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F D A T E            *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENREG.                                                 00000030
           02 FDATE-JJ              PIC X(02).                                  
           02 FDATE-MM              PIC X(02).                                  
           02 FDATE-SS              PIC X(02).                                  
           02 FDATE-AA              PIC X(02).                                  
           02 FILLER                PIC X(72).                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F I V 1 8 0          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FGP849                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENREG-FGP849 PIC X(512).                                             
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    I I V 1 8 0          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  IGP855                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-IGP855 .                                                       
           02 SAUT       PIC X.                                                 
           02 LIGNE      PIC X(189).                                            
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
       77  W-ZONE    PIC XX.                                                    
      *                                                                         
        COPY ABENDCOP.                                                          
      *                                                                         
        COPY WORKDATC.                                                          
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
       01  W-DATE.                                                              
           03  DATJMSA.                                                         
               05 JJ                  PIC  XX.                                  
               05 MM                  PIC  XX.                                  
               05 SS                  PIC  XX.                                  
               05 AA                  PIC  XX.                                  
           03  W-TIME.                                                          
               05 HH                  PIC  XX.                                  
               05 MN                  PIC  XX.                                  
               05 FILLER              PIC  X(14).                               
       01  JOUR-SEM PIC X.                                                      
      *                                                                         
       01  RUPTURE        PIC 99 VALUE 15.                                      
      *                                                                         
       01  CTR-FGP849  PIC 9(9) VALUE 0.                                        
      *                                                                         
       01  FLAG-FICHIER PIC X VALUE SPACE.                                      
           88  EOF      VALUE 'F'.                                              
       01  FLAG-ENTETE       PIC X VALUE SPACE.                                 
           88  RUPTURE-ENTETE    VALUE ' '.                                     
           88  ENTETE-ECRITE     VALUE 'O'.                                     
       01  FLAG-EDITION      PIC X VALUE SPACE.                                 
           88  CODIC-NON-EDITE   VALUE ' '.                                     
           88  CODIC-EDITE       VALUE 'O'.                                     
       01  W-FLAG-EDITION      PIC X VALUE SPACE.                               
           88  W-CODIC-NON-EDITE   VALUE ' '.                                   
           88  W-CODIC-EDITE       VALUE 'O'.                                   
       01  W-ELMT-EDITION      PIC X VALUE SPACE.                               
           88  CODIC-ELEMENT     VALUE 'O'.                                     
           88  CODIC-IMPRIME     VALUE ' '.                                     
      *                                                                         
       01 W-CAPPRO2.                                                            
          05 W-SEQ-CAPPRO PIC X     VALUE SPACES.                               
          05 W-NUMSTCOMP  PIC X(03) VALUE SPACES.                               
          05 FILLER       PIC X     VALUE SPACES.                               
       01 W-LSTATCOMP  PIC X(03).                                               
       01 W-WSENSAPPRO PIC X(01).                                               
       01 W-CEXPO      PIC X(05).                                               
       01 W-WOA        PIC X(01).                                               
       01 W-PSTDTTC    PIC S9(07)V9(02) COMP-3.                                 
      *                                                                         
      *ENREGISTREMENT COURANT                                                   
       01 FI-CPT          PIC S9(02) COMP-3.                                    
       01 FI-MAX          PIC S9(02) COMP-3 VALUE 7.                            
       01 W-WENTNAT       PIC X(06) VALUE SPACES.                               
       01 W-NCODIC        PIC X(07) VALUE SPACES.                               
       01 FGP849-ENTREPOT PIC X(06).                                            
       01 COPY-FGP849.                                                          
              COPY FGP849 REPLACING ==:FGPXXX:== BY ==FGP849==.                 
      *                                                                         
      *ENREGISTREMENT PRECEDENT                                                 
       01 COPY-W-FGP849.                                                        
              COPY FGP849 REPLACING ==:FGPXXX:== BY ==W-FGP849==.               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01 W-Q4S                              PIC  S9(5) COMP.                  
      *--                                                                       
        01 W-Q4S                              PIC  S9(5) COMP-5.                
      *}                                                                        
      *--------------------------- CUMUL T10  --------------------------        
        01 W-T10.                                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQ4S                   PIC S9(7) COMP.                
      *--                                                                       
               10  W-T10-TCQ4S                   PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQS-1                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T10-TCQS-1                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQS-2                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T10-TCQS-2                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQS-3                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T10-TCQS-3                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQS-4                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T10-TCQS-4                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQSTOCKMAG             PIC S9(8) COMP.                
      *--                                                                       
               10  W-T10-TCQSTOCKMAG             PIC S9(8) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQSTOCKBOU             PIC S9(8) COMP.                
      *--                                                                       
               10  W-T10-TCQSTOCKBOU             PIC S9(8) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCNBMAG                 PIC 9(4) COMP.                 
      *--                                                                       
               10  W-T10-TCNBMAG                 PIC 9(4) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCNBBOU                 PIC 9(4) COMP.                 
      *--                                                                       
               10  W-T10-TCNBBOU                 PIC 9(4) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQSTOCKDIS             PIC S9(8) COMP.                
      *--                                                                       
               10  W-T10-TCQSTOCKDIS             PIC S9(8) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQSTOCKRES             PIC S9(7) COMP.                
      *--                                                                       
               10  W-T10-TCQSTOCKRES             PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQCDE                  PIC 9(6) COMP.                 
      *--                                                                       
               10  W-T10-TCQCDE                  PIC 9(6) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T10-TCQCDERES               PIC 9(6) COMP.                 
      *--                                                                       
               10  W-T10-TCQCDERES               PIC 9(6) COMP-5.               
      *}                                                                        
      *---------------------- VENTILATION 25  --------------------------        
       01 I-T25                    PIC S9(4) COMP-3 VALUE 0.                    
       01 MAX-T25                  PIC S9(5) COMP-3 VALUE 1500.                 
       01 NB-T25                   PIC S9(5) COMP-3 VALUE 0.                    
       01 W-T25.                                                                
          02 TABLE-CUMUL-AGREDATED OCCURS 1500.                                 
               10  W-T25-LMARQ                   PIC X(20).                     
               10  W-T25-QPOIDSAGR               PIC 999,9.                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQ4S                   PIC S9(7) COMP.                
      *--                                                                       
               10  W-T25-TLQ4S                   PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQS-1                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T25-TLQS-1                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQS-2                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T25-TLQS-2                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQS-3                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T25-TLQS-3                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQS-4                  PIC S9(7) COMP.                
      *--                                                                       
               10  W-T25-TLQS-4                  PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQSTOCKMAG             PIC S9(8) COMP.                
      *--                                                                       
               10  W-T25-TLQSTOCKMAG             PIC S9(8) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQSTOCKBOU             PIC S9(8) COMP.                
      *--                                                                       
               10  W-T25-TLQSTOCKBOU             PIC S9(8) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQSTOCKDIS             PIC S9(8) COMP.                
      *--                                                                       
               10  W-T25-TLQSTOCKDIS             PIC S9(8) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQSTOCKRES             PIC S9(7) COMP.                
      *--                                                                       
               10  W-T25-TLQSTOCKRES             PIC S9(7) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQCDE                  PIC 9(6) COMP.                 
      *--                                                                       
               10  W-T25-TLQCDE                  PIC 9(6) COMP-5.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T25-TLQCDERES               PIC 9(6) COMP.                 
      *--                                                                       
               10  W-T25-TLQCDERES               PIC 9(6) COMP-5.               
      *}                                                                        
      *                                                                         
       01 SEGSOMME.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQ4S         PIC S9(7) COMP.                                   
      *--                                                                       
          02 SEGTLQ4S         PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQS-1        PIC S9(7) COMP.                                   
      *--                                                                       
          02 SEGTLQS-1        PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQS-2        PIC S9(7) COMP.                                   
      *--                                                                       
          02 SEGTLQS-2        PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQS-3        PIC S9(7) COMP.                                   
      *--                                                                       
          02 SEGTLQS-3        PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQS-4        PIC S9(7) COMP.                                   
      *--                                                                       
          02 SEGTLQS-4        PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQSTOCKMAG   PIC S9(8) COMP.                                   
      *--                                                                       
          02 SEGTLQSTOCKMAG   PIC S9(8) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQSTOCKBOU   PIC S9(8) COMP.                                   
      *--                                                                       
          02 SEGTLQSTOCKBOU   PIC S9(8) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQSTOCKDIS   PIC S9(8) COMP.                                   
      *--                                                                       
          02 SEGTLQSTOCKDIS   PIC S9(8) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQSTOCKRES   PIC S9(7) COMP.                                   
      *--                                                                       
          02 SEGTLQSTOCKRES   PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQCDE        PIC 9(6)  COMP.                                   
      *--                                                                       
          02 SEGTLQCDE        PIC 9(6) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 SEGTLQCDERES     PIC 9(6)  COMP.                                   
      *--                                                                       
          02 SEGTLQCDERES     PIC 9(6) COMP-5.                                  
      *}                                                                        
      *---------------------- VENTILATION 45  --------------------------        
       01 F-TAMPON.                                                             
          05 F-TAMPON1 PIC X(03).                                               
          05 F-TAMPON2 PIC X(03).                                               
       01 I-T45                    PIC S9(4) COMP-3 VALUE 0.                    
       01 MAX-T45                  PIC S9(5) COMP-3 VALUE 1500.                 
       01 NB-T45                   PIC S9(5) COMP-3 VALUE 0.                    
       01 F-T45                    PIC S9(2) COMP-3 VALUE 0.                    
       01 FAX-T45                  PIC S9(2) COMP-3 VALUE 10.                   
       01 FB-T45                   PIC S9(2) COMP-3 VALUE 0.                    
       01 I-STA                    PIC S9(1) COMP-3 VALUE 0.                    
       01 W-T45.                                                                
          02 TABLE-CUMUL-FAMILLE  OCCURS 1500.                                  
               10  W-T45-LMARQ                    PIC X(20).                    
               10  W-T45-QPOIDSFAM                PIC 999,9.                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQ4S                    PIC S9(7) COMP.               
      *--                                                                       
               10  W-T45-TFQ4S                    PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQS-1                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T45-TFQS-1                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQS-2                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T45-TFQS-2                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQS-3                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T45-TFQS-3                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQS-4                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T45-TFQS-4                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQSTOCKMAG              PIC S9(8) COMP.               
      *--                                                                       
               10  W-T45-TFQSTOCKMAG              PIC S9(8) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQSTOCKBOU              PIC S9(8) COMP.               
      *--                                                                       
               10  W-T45-TFQSTOCKBOU              PIC S9(8) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQSTOCKDIS              PIC S9(8) COMP.               
      *--                                                                       
               10  W-T45-TFQSTOCKDIS              PIC S9(8) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQSTOCKRES              PIC S9(7) COMP.               
      *--                                                                       
               10  W-T45-TFQSTOCKRES              PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQCDE                   PIC 9(6) COMP.                
      *--                                                                       
               10  W-T45-TFQCDE                   PIC 9(6) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T45-TFQCDERES                PIC 9(6) COMP.                
      *--                                                                       
               10  W-T45-TFQCDERES                PIC 9(6) COMP-5.              
      *}                                                                        
          02 TABLE-CUMUL-ENTREPOT OCCURS 10.                                    
               10  W-T45-FENTREPOT.                                             
                   15  W-T45-FSOCDEPOT            PIC X(03).                    
                   15  W-T45-FDEPOT               PIC X(03).                    
               10  W-T45-FILIALE.                                               
                   15  W-T45-FILIALE1                 PIC X(03).                
                   15  W-T45-FILIALE2                 PIC X(03).                
                   15  W-T45-FILIALE3                 PIC X(03).                
                   15  W-T45-FILIALE4                 PIC X(03).                
                   15  W-T45-FILIALE5                 PIC X(03).                
                   15  W-T45-FILIALE6                 PIC X(03).                
               10  W-T45-CAPPRO OCCURS 3.                                       
      * 1 POUR LES APP, 2 POUR EPD-EPF, 3 POUR LES CQE                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-NBRE-CODIC               PIC S9(04) COMP.          
      *--                                                                       
                   15  W-T45-NBRE-CODIC               PIC S9(04) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQ4S                  PIC S9(07) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQ4S                  PIC S9(07) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQS-1                 PIC S9(07) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQS-1                 PIC S9(07) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQS-2                 PIC S9(07) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQS-2                 PIC S9(07) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQS-3                 PIC S9(07) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQS-3                 PIC S9(07) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQS-4                 PIC S9(07) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQS-4                 PIC S9(07) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQSTOCKMAG            PIC S9(08) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQSTOCKMAG            PIC S9(08) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQSTOCKBOU            PIC S9(08) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQSTOCKBOU            PIC S9(08) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQSTOCKDIS            PIC S9(08) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQSTOCKDIS            PIC S9(08) COMP-5.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-T45-TSTAQSTOCKRES            PIC S9(07) COMP.          
      *--                                                                       
                   15  W-T45-TSTAQSTOCKRES            PIC S9(07) COMP-5.        
      *}                                                                        
       01 SOMME-STATUT.                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTANBRECODIC            PIC S9(04) COMP.               
      *--                                                                       
             05  W-T45-TCSTANBRECODIC            PIC S9(04) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQ4S                  PIC S9(07) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQ4S                  PIC S9(07) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQS-1                 PIC S9(07) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQS-1                 PIC S9(07) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQS-2                 PIC S9(07) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQS-2                 PIC S9(07) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQS-3                 PIC S9(07) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQS-3                 PIC S9(07) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQS-4                 PIC S9(07) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQS-4                 PIC S9(07) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQSTOCKMAG            PIC S9(08) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQSTOCKMAG            PIC S9(08) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQSTOCKDIS            PIC S9(08) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQSTOCKDIS            PIC S9(08) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      05  W-T45-TCSTAQSTOCKRES            PIC S9(07) COMP.               
      *--                                                                       
             05  W-T45-TCSTAQSTOCKRES            PIC S9(07) COMP-5.             
      *}                                                                        
      *                                                                         
       01 FSOMME.                                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMNBRECODIC    PIC S9(4) COMP.                                   
      *--                                                                       
          02 FSOMNBRECODIC    PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQ4S        PIC S9(7) COMP.                                   
      *--                                                                       
          02 FSOMTFQ4S        PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQS-1       PIC S9(7) COMP.                                   
      *--                                                                       
          02 FSOMTFQS-1       PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQS-2       PIC S9(7) COMP.                                   
      *--                                                                       
          02 FSOMTFQS-2       PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQS-3       PIC S9(7) COMP.                                   
      *--                                                                       
          02 FSOMTFQS-3       PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQS-4       PIC S9(7) COMP.                                   
      *--                                                                       
          02 FSOMTFQS-4       PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQSTOCKMAG  PIC S9(8) COMP.                                   
      *--                                                                       
          02 FSOMTFQSTOCKMAG  PIC S9(8) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQSTOCKBOU  PIC S9(8) COMP.                                   
      *--                                                                       
          02 FSOMTFQSTOCKBOU  PIC S9(8) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQSTOCKDIS  PIC S9(8) COMP.                                   
      *--                                                                       
          02 FSOMTFQSTOCKDIS  PIC S9(8) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQSTOCKRES  PIC S9(7) COMP.                                   
      *--                                                                       
          02 FSOMTFQSTOCKRES  PIC S9(7) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQCDE       PIC 9(6)  COMP.                                   
      *--                                                                       
          02 FSOMTFQCDE       PIC 9(6) COMP-5.                                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 FSOMTFQCDERES    PIC 9(6)  COMP.                                   
      *--                                                                       
          02 FSOMTFQCDERES    PIC 9(6) COMP-5.                                  
      *}                                                                        
      *---------------------- VENTILATION 65  --------------------------        
       01 I-T65                    PIC S9(4) COMP-3 VALUE 0.                    
       01 MAX-T65                  PIC S9(5) COMP-3 VALUE 1500.                 
       01 NB-T65                   PIC S9(5) COMP-3 VALUE 0.                    
       01 W-T65.                                                                
          02 TABLE-CUMUL-CHEFPROD OCCURS 1500.                                  
               10  W-T65-LMARQ                    PIC X(20).                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQ4S                    PIC S9(7) COMP.               
      *--                                                                       
               10  W-T65-TPQ4S                    PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQS-1                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T65-TPQS-1                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQS-2                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T65-TPQS-2                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQS-3                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T65-TPQS-3                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQS-4                   PIC S9(7) COMP.               
      *--                                                                       
               10  W-T65-TPQS-4                   PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQSTOCKMAG              PIC S9(8) COMP.               
      *--                                                                       
               10  W-T65-TPQSTOCKMAG              PIC S9(8) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQSTOCKBOU              PIC S9(8) COMP.               
      *--                                                                       
               10  W-T65-TPQSTOCKBOU              PIC S9(8) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQSTOCKDIS              PIC S9(8) COMP.               
      *--                                                                       
               10  W-T65-TPQSTOCKDIS              PIC S9(8) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQSTOCKRES              PIC S9(7) COMP.               
      *--                                                                       
               10  W-T65-TPQSTOCKRES              PIC S9(7) COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQCDE                   PIC 9(6) COMP.                
      *--                                                                       
               10  W-T65-TPQCDE                   PIC 9(6) COMP-5.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T65-TPQCDERES                PIC 9(6) COMP.                
      *--                                                                       
               10  W-T65-TPQCDERES                PIC 9(6) COMP-5.              
      *}                                                                        
      *                                                                         
       01 CHEFPSOMME.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQ4S        PIC S9(7) COMP.                                  
      *--                                                                       
          02 CHEFPTPQ4S        PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQS-1       PIC S9(7) COMP.                                  
      *--                                                                       
          02 CHEFPTPQS-1       PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQS-2       PIC S9(7) COMP.                                  
      *--                                                                       
          02 CHEFPTPQS-2       PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQS-3       PIC S9(7) COMP.                                  
      *--                                                                       
          02 CHEFPTPQS-3       PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQS-4       PIC S9(7) COMP.                                  
      *--                                                                       
          02 CHEFPTPQS-4       PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQSTOCKMAG  PIC S9(8) COMP.                                  
      *--                                                                       
          02 CHEFPTPQSTOCKMAG  PIC S9(8) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQSTOCKBOU  PIC S9(8) COMP.                                  
      *--                                                                       
          02 CHEFPTPQSTOCKBOU  PIC S9(8) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQSTOCKDIS  PIC S9(8) COMP.                                  
      *--                                                                       
          02 CHEFPTPQSTOCKDIS  PIC S9(8) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQSTOCKRES  PIC S9(7) COMP.                                  
      *--                                                                       
          02 CHEFPTPQSTOCKRES  PIC S9(7) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQCDE       PIC 9(6)  COMP.                                  
      *--                                                                       
          02 CHEFPTPQCDE       PIC 9(6) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 CHEFPTPQCDERES    PIC 9(6)  COMP.                                  
      *--                                                                       
          02 CHEFPTPQCDERES    PIC 9(6) COMP-5.                                 
      *}                                                                        
      *                                                                         
       01  CTR-PAGE     PIC 9(4) VALUE 0.                                       
      *MASQUE D EDITION                                                         
       01  W-MASKED5NS     PIC ZZZZ9.                                           
       01  W-MASKED3NS     PIC ZZ9.                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL5      PIC S9(4)  COMP.                                     
      *--                                                                       
       01  W-MASKCAL5      PIC S9(4) COMP-5.                                    
      *}                                                                        
       01  W-MASKED5       PIC -ZZZ9.                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL6      PIC S9(5)  COMP.                                     
      *--                                                                       
       01  W-MASKCAL6      PIC S9(5) COMP-5.                                    
      *}                                                                        
       01  W-MASKED6       PIC -ZZZZ9.                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL7      PIC S9(6)  COMP.                                     
      *--                                                                       
       01  W-MASKCAL7      PIC S9(6) COMP-5.                                    
      *}                                                                        
       01  W-MASKED7       PIC -ZZZZZ9.                                         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MAX         PIC S9(7) COMP.                                        
      *--                                                                       
       01  W-MAX         PIC S9(7) COMP-5.                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-NBRE        PIC S9(7) COMP.                                        
      *--                                                                       
       01  W-NBRE        PIC S9(7) COMP-5.                                      
      *}                                                                        
       01  W-CODE.                                                              
           02 W-LCODE    PIC X.                                                 
           02 W-SIGNE    PIC X.                                                 
      *                                                                         
       01  W-NUMED2        PIC Z9.                                              
       01  W-NUMED3        PIC ZZ9.                                             
       01  W-NUMED4        PIC ZZZ9.                                            
       01  W-NUMED5        PIC ZZZZ9.                                           
       01  W-NUMED5S       PIC -ZZZ9.                                           
       01  W-NUMED6        PIC ZZZZZ9.                                          
       01  W-NUMED6S       PIC -ZZZZ9.                                          
       01  W-NUMED7        PIC ZZZZZZ9.                                         
       01  W-NUMED7S       PIC -ZZZZZ9.                                         
       01  W-NUMED8        PIC ZZZZZZZ9.                                        
       01  W-NUMED8S       PIC -ZZZZZZ9.                                        
      *                                                                         
       01  EDITX.                                                               
           05 EDIT3.                                                            
              10 W-EDIT3     PIC XX.                                            
              10 W-MILLIERS3 PIC X.                                             
           05 EDIT4.                                                            
              10 W-EDIT4     PIC XXX.                                           
              10 W-MILLIERS4 PIC X.                                             
           05 EDIT5.                                                            
              10 W-EDIT5     PIC XXXX.                                          
              10 W-MILLIERS5 PIC X.                                             
           05 EDIT6.                                                            
              10 W-EDIT6     PIC XXXXX.                                         
              10 W-MILLIERS6 PIC X.                                             
           05 EDIT7.                                                            
              10 W-EDIT7     PIC XXXXXX.                                        
              10 W-MILLIERS7 PIC X.                                             
           05 EDIT8.                                                            
              10 W-EDIT8     PIC XXXXXXX.                                       
              10 W-MILLIERS8 PIC X.                                             
      *                                                                         
      * VARIABLE PERMETTANT UNE EDITION DES NOMBRES EN MILLIERS                 
      * PAR EXEMPLE  104000 --> 104K                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  QUOTIENT   PIC S9(4) COMP.                                           
      *--                                                                       
       01  QUOTIENT   PIC S9(4) COMP-5.                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RESTE      PIC 9(2) COMP.                                            
      *--                                                                       
       01  RESTE      PIC 9(2) COMP-5.                                          
      *}                                                                        
      *                                                                         
       01  CTR-LIGNE    PIC 99   VALUE 0.                                       
       01  MAX-LIGNE    PIC 99   VALUE 60.                                      
           EJECT                                                                
      *-------------------------- BLOC ECRITURE DU CODIC2 -------------         
       01 I-BLOC                   PIC S9(4) COMP-3 VALUE 0.                    
       01 MAX-BLOC                 PIC S9(5) COMP-3 VALUE 500.                  
       01 NB-BLOC                  PIC S9(5) COMP-3 VALUE 0.                    
       01 NB-LIGNE-BLOC            PIC S9(5) COMP-3 VALUE 0.                    
       01 NB-LIGNE-CODIC1          PIC S9(5) COMP-3 VALUE 0.                    
       01 W-BLOC.                                                               
          02 BLOC-LIGNE OCCURS 500.                                             
               10  LIGNE-CODIC2                   PIC X(190).                   
      *                                                                         
       01  IGP855-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  IGP855-E00.                                                      
               10  IGP855-E00-CETAT               PIC X(06).                    
               10  FILLER                         PIC X(09) VALUE               
           ' BOITE : '.                                                         
               10  IGP855-E00-CBOITE              PIC X(07).                    
               10  FILLER                         PIC X(36) VALUE               
           '                                    '.                              
               10  FILLER                         PIC X(58) VALUE               
           '                   R E A P P R O V I S I O N N E M E N T  '.        
               10  FILLER                         PIC X(46) VALUE               
           '                                     EDITE LE '.                    
               10  IGP855-E00-DATEDITE.                                         
                   15 JJ                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 MM                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 SS                          PIC XX.                       
                   15 AA                          PIC XX.                       
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  IGP855-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  IGP855-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  IGP855-E00-NOPAGE              PIC ZZZ.                      
      *--------------------------- LIGNE E05  --------------------------        
           05  IGP855-E05.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'SOCIETE      : '.                                                   
               10  IGP855-E05-NSOCIETE            PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IGP855-E05-LLIEU               PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                           F O U R N I S S'.        
               10  FILLER                         PIC X(58) VALUE               
           ' E U R                                                    '.        
               10  FILLER                         PIC X(32) VALUE               
           '                                '.                                  
      *--------------------------- LIGNE E10  --------------------------        
           05  IGP855-E10.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'CHEF PRODUIT : '.                                                   
               10  IGP855-E10-CHEFPROD            PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-E10-LCHEFPROD           PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                       V E R S I O N     D'.        
               10  FILLER                         PIC X(58) VALUE               
           ' A C E M                                                  '.        
               10  FILLER                         PIC X(32) VALUE               
           '                                '.                                  
      *--------------------------- LIGNE E15  --------------------------        
           05  IGP855-E15.                                                      
               10  FILLER                         PIC X(15) VALUE               
           '     FAMILLE : '.                                                   
               10  IGP855-E15-CFAM                PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-E15-LFAM                PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(32) VALUE               
           '                                '.                                  
      *--------------------------- LIGNE E20  --------------------------        
           05  IGP855-E20.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'SEGMENT MARK.: '.                                                   
               10  IGP855-E20-LAGREGATED          PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(38) VALUE               
           '                                      '.                            
      *--------------------------- LIGNE E25  --------------------------        
           05  IGP855-E25.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(15) VALUE               
           '               '.                                                   
      *--------------------------- LIGNE E30  --------------------------        
           05  IGP855-E30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE E35  --------------------------        
           05  IGP855-E35.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '! MARQUE      REFERENCE      STA E O !   PRIX    !      VE'.        
               10  FILLER                         PIC X(58) VALUE               
           'NTES MGC ET MGI CUMULEES      ! NB  DISPO           !    E'.        
               10  FILLER                         PIC X(58) VALUE               
           'NTREPOT    !  STOCK  ! SOC     ! PTFE COMMANDES    !  CODI'.        
               10  FILLER                         PIC X(15) VALUE               
           'C  ! CDES      '.                                                   
      *--------------------------- LIGNE E40  --------------------------        
           05  IGP855-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                            TUT X A !    ZP     !  S-4   '.        
               10  FILLER                         PIC X(58) VALUE               
           ' S-3    S-2    S-1   !   4S   ! MAG  MAG            !  DIS'.        
               10  FILLER                         PIC X(58) VALUE               
           'PO   RES.  !  TOTAL  !     ENT ! DATE    QTE   RES !      '.        
               10  FILLER                         PIC X(15) VALUE               
           '   !A PASSER   '.                                                   
      *--------------------------- LIGNE E45  --------------------------        
           05  IGP855-E45.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE D05  --------------------------        
           05  IGP855-D05.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IGP855-D05-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-LREFFOURN           PIC X(20).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-LSTATCOMP           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-CEXPO               PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-WOA                 PIC X(01).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-PSTDTTC                                           
                   PIC -ZZZZ9,99                      BLANK WHEN ZERO.          
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-QS-4                PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QS-3                PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QS-2                PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QS-1                PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-Q4S                 PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-NBMAG               PIC XXX.                      
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QSTOCKMAG           PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-NBBOU               PIC XXX.                      
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QSTOCKBOU           PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-QSTOCKDIS           PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QSTOCKRES           PIC XXXXXX.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-D05-QSTOCKTOT           PIC X(08).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-NSOCDEPOT           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-NDEPOT              PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-DCDE4               PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGP855-D05-QCDE                PIC ZZZZ9.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D05-QCDERES             PIC ZZZZ9.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D05-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(13) VALUE               
           ' !      !    '.                                                     
      *--------------------------- LIGNE D10  --------------------------        
           05  IGP855-D10.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IGP855-D10-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D10-LREFFOURN           PIC X(20).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D10-LSTATCOMP           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D10-CEXPO               PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D10-WOA                 PIC X(01).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-D10-PSTDTTC                                           
                   PIC -ZZZZ9,99                      BLANK WHEN ZERO.          
               10  FILLER                         PIC X(58) VALUE               
           ' !                             !        !                 '.        
               10  FILLER                         PIC X(58) VALUE               
           '    !                !         !         !                '.        
               10  FILLER                         PIC X(05) VALUE               
           '   ! '.                                                             
               10  IGP855-D10-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(13) VALUE               
           ' !      !    '.                                                     
      *--------------------------- LIGNE D15  --------------------------        
           05  IGP855-D15.                                                      
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGP855-D15-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D15-LREFFOURN           PIC X(20).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D15-LSTATCOMP           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D15-CEXPO               PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-D15-WOA                 PIC X(01).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IGP855-D15-PSTDTTC                                           
                   PIC -ZZZZ9,99                      BLANK WHEN ZERO.          
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
               10  IGP855-D15-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(13) VALUE               
           '             '.                                                     
      *--------------------------- LIGNE T04  --------------------------        
           05  IGP855-T00.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                                    !           !        '.        
               10  FILLER                         PIC X(58) VALUE               
           '                     !        !                     !     '.        
               10  FILLER                         PIC X(58) VALUE               
           '           !         !         !                   !      '.        
               10  FILLER                         PIC X(15) VALUE               
           '   !      !    '.                                                   
      *--------------------------- LIGNE T05  --------------------------        
           05  IGP855-T05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                                    !           ! ------ '.        
               10  FILLER                         PIC X(58) VALUE               
           '------ ------ ------ ! ------ ! --- -----           ! ----'.        
               10  FILLER                         PIC X(58) VALUE               
           '--- ------ ! ------- !         !       ----- ----- !      '.        
               10  FILLER                         PIC X(15) VALUE               
           '   !      !    '.                                                   
      *--------------------------- LIGNE T10  --------------------------        
           05  IGP855-T10.                                                      
               10  FILLER                         PIC X(51) VALUE               
           '!                                    !           ! '.               
               10  IGP855-T10-TCQS-4              PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T10-TCQS-3              PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T10-TCQS-2              PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T10-TCQS-1              PIC -ZZZZ9.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T10-TCQ4S               PIC -ZZZZ9.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T10-TCNBMAG             PIC ZZ9.                      
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T10-TCQSTOCKMAG         PIC ZZZZ9.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
MPSUP *        10  IGP855-T10-TCNBBOU             PIC ZZ9.                      
MPADD          10  IGP855-T10-TCNBBOU             PIC XXX VALUE SPACES.         
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
MPSUP *        10  IGP855-T10-TCQSTOCKBOU         PIC -ZZZ9.                    
MPADD          10  IGP855-T10-TCQSTOCKBOU      PIC XXXXX VALUE SPACES.          
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T10-TCQSTOCKDIS         PIC -ZZZZZ9.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T10-TCQSTOCKRES         PIC -ZZZZ9.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T10-TCQSTOCKTOT         PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T10-TCQCDE              PIC ZZZZ9.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T10-TCQCDERES           PIC ZZZZ9.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE T20  --------------------------        
           05  IGP855-T20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE T25  --------------------------        
           05  IGP855-T25.                                                      
               10  FILLER                         PIC X(14) VALUE               
           '! TOT SEG.MRK '.                                                    
               10  IGP855-T25-LAGREGATED          PIC X(20).                    
               10  FILLER                         PIC X(17) VALUE               
           '   !           ! '.                                                 
               10  IGP855-T25-TLQS-4              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T25-TLQS-3              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T25-TLQS-2              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T25-TLQS-1              PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T25-TLQ4S               PIC XXXXXX.                   
               10  FILLER                         PIC X(07) VALUE               
           ' !     '.                                                           
               10  IGP855-T25-TLQSTOCKMAG         PIC XXXXX.                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
MP             10  IGP855-T25-TLQSTOCKBOU      PIC XXXXX VALUE SPACES.          
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T25-TLQSTOCKDIS         PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T25-TLQSTOCKRES         PIC XXXXXX.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T25-TLQSTOCKTOT         PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T25-TLQCDE              PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T25-TLQCDERES           PIC XXXXX.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE T30  --------------------------        
           05  IGP855-T30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE T35  --------------------------        
           05  IGP855-T35.                                                      
               10  FILLER                         PIC X(06) VALUE               
           '!     '.                                                            
               10  IGP855-T35-LMARQ               PIC X(20).                    
               10  FILLER                         PIC X(14) VALUE               
           '           !  '.                                                    
               10  IGP855-T35-QPOIDSCMARQ                                       
                   PIC -Z9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(06) VALUE               
           ' %  ! '.                                                            
               10  IGP855-T35-TLCQS-4             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T35-TLCQS-3             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T35-TLCQS-2             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T35-TLCQS-1             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T35-TLCQ4S              PIC -ZZZZ9.                   
               10  FILLER                         PIC X(07) VALUE               
           ' !     '.                                                           
               10  IGP855-T35-TLCQSTOCKMAG        PIC ZZZZ9.                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
MPSUP *        10  IGP855-T35-TLCQSTOCKBOU        PIC ZZZZ9.                    
MPADD          10  IGP855-T35-TLCQSTOCKBOU   PIC XXXXX VALUE SPACES.            
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T35-TLCQSTOCKDIS        PIC -ZZZZZ9.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T35-TLCQSTOCKRES        PIC -ZZZZ9.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T35-TLCQSTOCKTOT        PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T35-TLCQCDE             PIC ZZZZ9.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T35-TLCQCDERES          PIC ZZZZ9.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE T40  --------------------------        
           05  IGP855-T40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE T45  --------------------------        
           05  IGP855-T45.                                                      
               10  FILLER                         PIC X(14) VALUE               
           '! TOT FAMILLE '.                                                    
               10  IGP855-T45-LFAM                PIC X(20).                    
               10  FILLER                         PIC X(17) VALUE               
           '   !           ! '.                                                 
               10  IGP855-T45-TFQS-4              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T45-TFQS-3              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T45-TFQS-2              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T45-TFQS-1              PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T45-TFQ4S               PIC XXXXXX.                   
               10  FILLER                         PIC X(07) VALUE               
           ' !     '.                                                           
               10  IGP855-T45-TFQSTOCKMAG         PIC XXXXX.                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
MP           10  IGP855-T45-TFQSTOCKBOU         PIC XXXXX VALUE SPACES.         
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T45-TFQSTOCKDIS         PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T45-TFQSTOCKRES         PIC XXXXXX.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T45-TFQSTOCKTOT         PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T45-TFQCDE              PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T45-TFQCDERES           PIC XXXXX.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE T50  --------------------------        
           05  IGP855-T50.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE T55  --------------------------        
           05  IGP855-T55.                                                      
               10  FILLER                         PIC X(06) VALUE               
           '!     '.                                                            
               10  IGP855-T55-LMARQ               PIC X(20).                    
               10  FILLER                         PIC X(14) VALUE               
           '           !  '.                                                    
               10  IGP855-T55-QPOIDSCFAM                                        
                   PIC -Z9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(06) VALUE               
           ' %  ! '.                                                            
               10  IGP855-T55-TFCQS-4             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T55-TFCQS-3             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T55-TFCQS-2             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T55-TFCQS-1             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T55-TFCQ4S              PIC -ZZZZ9.                   
               10  FILLER                         PIC X(07) VALUE               
           ' !     '.                                                           
               10  IGP855-T55-TFCQSTOCKMAG        PIC ZZZZ9.                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
MPSUP *        10  IGP855-T55-TFCQSTOCKBOU        PIC -ZZZ9.                    
MPADD          10  IGP855-T55-TFCQSTOCKBOU   PIC XXXXX VALUE SPACES.            
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T55-TFCQSTOCKDIS        PIC -ZZZZZ9.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T55-TFCQSTOCKRES        PIC -ZZZZ9.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T55-TFCQSTOCKTOT        PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T55-TFCQCDE             PIC ZZZZ9.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T55-TFCQCDERES          PIC ZZZZ9.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE T60  --------------------------        
           05  IGP855-T60.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE T65  --------------------------        
           05  IGP855-T65.                                                      
               10  FILLER                         PIC X(14) VALUE               
           '! TOT CHEF PR '.                                                    
               10  IGP855-T65-LCHEFPROD           PIC X(20).                    
               10  FILLER                         PIC X(17) VALUE               
           '   !           ! '.                                                 
               10  IGP855-T65-TPQS-4              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T65-TPQS-3              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T65-TPQS-2              PIC XXXXXX.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T65-TPQS-1              PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T65-TPQ4S               PIC XXXXXX.                   
               10  FILLER                         PIC X(07) VALUE               
           ' !     '.                                                           
               10  IGP855-T65-TPQSTOCKMAG         PIC XXXXX.                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
MP             10  IGP855-T65-TPQSTOCKBOU      PIC XXXXX VALUE SPACES.          
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T65-TPQSTOCKDIS         PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T65-TPQSTOCKRES         PIC XXXXXX.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T65-TPQSTOCKTOT         PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T65-TPQCDE              PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T65-TPQCDERES           PIC XXXXX.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE T70  --------------------------        
           05  IGP855-T70.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+---------+-------------------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
      *--------------------------- LIGNE T75  --------------------------        
           05  IGP855-T75.                                                      
               10  FILLER                         PIC X(06) VALUE               
           '!     '.                                                            
               10  IGP855-T75-LMARQ               PIC X(20).                    
               10  FILLER                         PIC X(25) VALUE               
           '           !           ! '.                                         
               10  IGP855-T75-TPCQS-4             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T75-TPCQS-3             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T75-TPCQS-2             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T75-TPCQS-1             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T75-TPOCQ4S             PIC -ZZZZ9.                   
               10  FILLER                         PIC X(07) VALUE               
           ' !     '.                                                           
               10  IGP855-T75-TPCQSTOCKMAG        PIC XXXXX.                    
               10  FILLER                         PIC X(05) VALUE               
           '     '.                                                             
MP             10  IGP855-T75-TPCQSTOCKBOU    PIC XXXXX VALUE SPACES.           
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP855-T75-TPCQSTOCKDIS        PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T75-TPCQSTOCKRES        PIC XXXXXX.                   
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP855-T75-TPCQSTOCKTOT        PIC X(08).                    
               10  FILLER                         PIC X(19) VALUE               
           ' !         !       '.                                               
               10  IGP855-T75-TPCQCDE             PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP855-T75-TPCQCDERES          PIC XXXXX.                    
               10  FILLER                         PIC X(23) VALUE               
           ' !         !      !    '.                                           
      *--------------------------- LIGNE B05  --------------------------        
           05  IGP855-B05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+------------------------------------+-----------+--------'.        
               10  FILLER                         PIC X(58) VALUE               
           '------------------------------+---------------------+-----'.        
               10  FILLER                         PIC X(58) VALUE               
           '-----------+---------+-------------------+---------+------'.        
               10  FILLER                         PIC X(15) VALUE               
           '---+------+----'.                                                   
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    T A B L E S       *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BGP855 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BGP855 SECTION.                                                   
      *                                                                         
           PERFORM  DEBUT-BGP855.                                               
           PERFORM  TRAIT-BGP855.                                               
           PERFORM  FIN-BGP855.                                                 
      *                                                                         
       F-MODULE-BGP855. EXIT.                                                   
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    D E B U T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       DEBUT-BGP855 SECTION.                                                    
      *                                                                         
           DISPLAY '*** DEBUT BGP855 ***'.                                      
           PERFORM OUVERTURE-FICHIERS.                                          
           PERFORM TRAIT-DATE.                                                  
      *                                                                         
       F-DEBUT-BGP855.                                                          
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    T R A I T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       TRAIT-BGP855 SECTION.                                                    
      *                                                                         
           INITIALIZE W-T10 W-T25 W-T45 W-T65 W-BLOC NB-BLOC                    
                      SEGSOMME                                                  
                      FSOMME                                                    
                      CHEFPSOMME.                                               
           PERFORM LECTURE-FGP849.                                              
EOF        PERFORM UNTIL EOF                                                    
            PERFORM RUPTURE-BOITE                                               
BOITE       PERFORM UNTIL                                                       
               FGP849-CBOITE   NOT = W-FGP849-CBOITE                            
            OR FGP849-CETAT    NOT = W-FGP849-CETAT                             
            OR EOF                                                              
            PERFORM RUPTURE-NSOCIETE                                            
NSOC        PERFORM UNTIL                                                       
               FGP849-CBOITE   NOT = W-FGP849-CBOITE                            
            OR FGP849-NSOCIETE NOT = W-FGP849-NSOCIETE                          
            OR FGP849-CETAT    NOT = W-FGP849-CETAT                             
            OR EOF                                                              
             PERFORM RUPTURE-CHEFPROD                                           
CHEF         PERFORM UNTIL                                                      
                FGP849-CBOITE   NOT = W-FGP849-CBOITE                           
             OR FGP849-NSOCIETE NOT = W-FGP849-NSOCIETE                         
             OR FGP849-CETAT    NOT = W-FGP849-CETAT                            
             OR FGP849-CHEFPROD NOT = W-FGP849-CHEFPROD                         
             OR EOF                                                             
               PERFORM RUPTURE-FAMILLE                                          
FAM            PERFORM UNTIL                                                    
                  FGP849-CBOITE   NOT = W-FGP849-CBOITE                         
               OR FGP849-CETAT    NOT = W-FGP849-CETAT                          
               OR FGP849-NSOCIETE NOT = W-FGP849-NSOCIETE                       
               OR FGP849-CHEFPROD NOT = W-FGP849-CHEFPROD                       
               OR FGP849-WSEQFAM  NOT = W-FGP849-WSEQFAM                        
               OR EOF                                                           
                 PERFORM RUPTURE-LAGREGATED                                     
                 PERFORM ECRITURE-ENTETE                                        
AGRE             PERFORM UNTIL                                                  
                    FGP849-CBOITE     NOT = W-FGP849-CBOITE                     
                 OR FGP849-CETAT      NOT = W-FGP849-CETAT                      
                 OR FGP849-NSOCIETE   NOT = W-FGP849-NSOCIETE                   
                 OR FGP849-CHEFPROD   NOT = W-FGP849-CHEFPROD                   
                 OR FGP849-WSEQFAM    NOT = W-FGP849-WSEQFAM                    
                 OR FGP849-LAGREGATED NOT = W-FGP849-LAGREGATED                 
                 OR EOF                                                         
COD2               PERFORM RUPTURE-NCODIC2                                      
                   PERFORM UNTIL                                                
                      FGP849-CBOITE     NOT = W-FGP849-CBOITE                   
                   OR FGP849-CETAT      NOT = W-FGP849-CETAT                    
                   OR FGP849-NSOCIETE   NOT = W-FGP849-NSOCIETE                 
                   OR FGP849-CHEFPROD   NOT = W-FGP849-CHEFPROD                 
                   OR FGP849-WSEQFAM    NOT = W-FGP849-WSEQFAM                  
                   OR FGP849-LAGREGATED NOT = W-FGP849-LAGREGATED               
                   OR FGP849-WCMARQ     NOT = W-FGP849-WCMARQ                   
                   OR FGP849-CAPPRO2    NOT = W-FGP849-CAPPRO2                  
                   OR FGP849-NCODIC2    NOT = W-FGP849-NCODIC2                  
                   OR EOF                                                       
                     PERFORM RUPTURE-NCODIC                                     
COD1                 PERFORM UNTIL                                              
                        FGP849-CBOITE     NOT = W-FGP849-CBOITE                 
                     OR FGP849-CETAT      NOT = W-FGP849-CETAT                  
                     OR FGP849-NSOCIETE   NOT = W-FGP849-NSOCIETE               
                     OR FGP849-CHEFPROD   NOT = W-FGP849-CHEFPROD               
                     OR FGP849-WSEQFAM    NOT = W-FGP849-WSEQFAM                
                     OR FGP849-LAGREGATED NOT = W-FGP849-LAGREGATED             
                     OR FGP849-WCMARQ     NOT = W-FGP849-WCMARQ                 
                     OR FGP849-CAPPRO2    NOT = W-FGP849-CAPPRO2                
                     OR FGP849-NCODIC2    NOT = W-FGP849-NCODIC2                
                     OR FGP849-WDEB       NOT = W-FGP849-WDEB                   
                     OR FGP849-NCODIC     NOT = W-FGP849-NCODIC                 
                     OR EOF                                                     
                       PERFORM RUPTURE-WENTNAT                                  
ENTNAT                 PERFORM UNTIL                                            
                          FGP849-CBOITE     NOT = W-FGP849-CBOITE               
                       OR FGP849-CETAT      NOT = W-FGP849-CETAT                
                       OR FGP849-NSOCIETE   NOT = W-FGP849-NSOCIETE             
                       OR FGP849-CHEFPROD   NOT = W-FGP849-CHEFPROD             
                       OR FGP849-WSEQFAM    NOT = W-FGP849-WSEQFAM              
                       OR FGP849-LAGREGATED NOT = W-FGP849-LAGREGATED           
                       OR FGP849-WCMARQ     NOT = W-FGP849-WCMARQ               
                       OR FGP849-CAPPRO2    NOT = W-FGP849-CAPPRO2              
                       OR FGP849-NCODIC2    NOT = W-FGP849-NCODIC2              
                       OR FGP849-WDEB       NOT = W-FGP849-WDEB                 
                       OR FGP849-NCODIC     NOT = W-FGP849-NCODIC               
                       OR FGP849-WENTNAT    NOT = W-FGP849-WENTNAT              
                       OR EOF                                                   
                         PERFORM RUPTURE-DCDE8                                  
DCDE8                    PERFORM UNTIL                                          
                            FGP849-CBOITE     NOT = W-FGP849-CBOITE             
                         OR FGP849-CETAT      NOT = W-FGP849-CETAT              
                         OR FGP849-NSOCIETE   NOT = W-FGP849-NSOCIETE           
                         OR FGP849-CHEFPROD   NOT = W-FGP849-CHEFPROD           
                         OR FGP849-WSEQFAM    NOT = W-FGP849-WSEQFAM            
                         OR FGP849-LAGREGATED NOT = W-FGP849-LAGREGATED         
                         OR FGP849-WCMARQ     NOT = W-FGP849-WCMARQ             
                         OR FGP849-CAPPRO2    NOT = W-FGP849-CAPPRO2            
                         OR FGP849-NCODIC2    NOT = W-FGP849-NCODIC2            
                         OR FGP849-WDEB       NOT = W-FGP849-WDEB               
                         OR FGP849-NCODIC     NOT = W-FGP849-NCODIC             
                         OR FGP849-WENTNAT    NOT = W-FGP849-WENTNAT            
                         OR FGP849-WTYPENT    NOT = W-FGP849-WTYPENT            
                         OR FGP849-NSOCDEPOT  NOT = W-FGP849-NSOCDEPOT          
                         OR FGP849-NDEPOT     NOT = W-FGP849-NDEPOT             
                         OR FGP849-DCDE8      NOT = W-FGP849-DCDE8              
                         OR EOF                                                 
                            IF FGP849-WGROUPE NOT = 'E'                         
                               PERFORM CUMUL-TOTAUX                             
                            ELSE                                                
                               PERFORM CUMUL-CODIC                              
                            END-IF                                              
                            PERFORM ECRITURE-DETAIL                             
                            PERFORM LECTURE-FGP849                              
DCDE8                    END-PERFORM                                            
ENTNAT                 END-PERFORM                                              
                       PERFORM ECRITURE-T0                                      
COD1                 END-PERFORM                                                
                     PERFORM ECRITURE-T10-T15                                   
COD2               END-PERFORM                                                  
                   PERFORM LIGNE-T20                                            
AGRE             END-PERFORM                                                    
                 PERFORM ECRITURE-T25                                           
FAM            END-PERFORM                                                      
               PERFORM ECRITURE-T45                                             
CHEF         END-PERFORM                                                        
             PERFORM ECRITURE-T65                                               
NSOC        END-PERFORM                                                         
BOITE       END-PERFORM                                                         
EOF        END-PERFORM.                                                         
       FIN-TRAIT-BGP855. EXIT.                                                  
      *                                                                         
       FIN-BGP855 SECTION.                                                      
      *                                                                         
           CLOSE FDATE IGP855 FGP849.                                           
           DISPLAY '**********************************************'.            
           DISPLAY '*    PROGRAMME BGP855  TERMINE NORMALEMENT   *'.            
           DISPLAY '*                                            *'.            
           DISPLAY '*  NOMBRE D''ENREG LUS SUR FGP849    = ' CTR-FGP849         
                   '  *'.                                                       
           DISPLAY '*                                            *'.            
           DISPLAY '**********************************************'.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-BGP855. EXIT.                                                    
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    D E B U T  B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       OUVERTURE-FICHIERS SECTION.                                              
      *-------------------                                                      
      *                                                                         
           OPEN INPUT FDATE FGP849.                                             
           OPEN OUTPUT IGP855.                                                  
       FIN-OUVERTURE-FICHIERS. EXIT.                                            
      *                                                                         
       TRAIT-DATE SECTION.                                                      
      *-----------                                                              
           READ FDATE                                                           
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                      
                  PERFORM FIN-ANORMALE.                                         
           MOVE '1'                 TO GFDATA.                                  
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                              
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                            
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS            
              PERFORM FIN-ANORMALE                                              
           ELSE                                                                 
              DISPLAY '** FDATE  ** ' GFJMSA-5                                  
           END-IF.                                                              
           COMPUTE GFQNT0 = GFQNT0 + 1 .                                        
           MOVE '3' TO GFDATA.                                                  
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS                
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFJJMMSSAA       TO DATJMSA.                                    
           MOVE GFSMN            TO JOUR-SEM.                                   
           DISPLAY 'DATE RECEPTION ETAT DANS BOITE EOS : ' DATJMSA.             
      *    MOVE 'FICHIER DATE VIDE' TO ABEND-MESS.                              
           ACCEPT W-TIME FROM TIME.                                             
       FIN-TRAIT-DATE. EXIT.                                                    
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    T R A I T  B G P 8 6 5               *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FGP849 SECTION.                                                  
      *----------------                                                         
           MOVE FGP849-ENREG TO W-FGP849-ENREG.                                 
           MOVE FLAG-EDITION TO W-FLAG-EDITION.                                 
           INITIALIZE FGP849-ENREG.                                             
      * DANS CES ETATS N APPARAISSENT QUE LES CODICS APP                        
           PERFORM UNTIL EOF                                                    
           OR (FGP849-CETAT = 'IGP855' AND FGP849-CAPPRO2 = 'APP')              
           OR (FGP849-CETAT = 'IGP856' AND FGP849-CAPPRO2 = 'APP')              
             READ FGP849                                                        
                  INTO FGP849-ENREG                                             
                  AT END SET EOF TO TRUE                                        
             END-READ                                                           
             SET CODIC-EDITE      TO TRUE                                       
           END-PERFORM.                                                         
           STRING FGP849-NSOCDEPOT FGP849-NDEPOT                                
                  DELIMITED BY SIZE INTO FGP849-ENTREPOT                        
           IF NOT EOF                                                           
              IF JOUR-SEM = 1                                                   
                 MOVE FGP849-QS-3 TO FGP849-QS-4                                
                 MOVE FGP849-QS-2 TO FGP849-QS-3                                
                 MOVE FGP849-QS-1 TO FGP849-QS-2                                
                 MOVE FGP849-QS-0 TO FGP849-QS-1                                
              END-IF                                                            
              IF CTR-FGP849 = 0                                                 
                 MOVE FGP849-ENREG TO W-FGP849-ENREG                            
                 MOVE SPACE        TO W-FLAG-EDITION                            
              END-IF                                                            
              ADD 1 TO CTR-FGP849                                               
              PERFORM NUMERO-RUPTURE                                            
              IF RUPTURE > 6                                                    
              AND CODIC-EDITE                                                   
                 MOVE HIGH-VALUE TO  W-LSTATCOMP                                
                                     W-WSENSAPPRO                               
                                     W-CEXPO                                    
                                     W-WOA                                      
                 MOVE 9999999,99 TO  W-PSTDTTC                                  
              END-IF                                                            
           END-IF.                                                              
       FIN-LECTURE-FGP849. EXIT.                                                
      *---------------- RUPTURE DE L ETAT ----------------------------          
       NUMERO-RUPTURE SECTION.                                                  
      *-----------------                                                        
           IF FGP849-NSOCIETE = W-FGP849-NSOCIETE                               
            MOVE 14 TO RUPTURE                                                  
            IF FGP849-CHEFPROD = W-FGP849-CHEFPROD                              
             MOVE 13 TO RUPTURE                                                 
             IF FGP849-WSEQFAM = W-FGP849-WSEQFAM                               
              MOVE 12 TO RUPTURE                                                
              IF FGP849-LAGREGATED = W-FGP849-LAGREGATED                        
               MOVE 11 TO RUPTURE                                               
               IF FGP849-WCMARQ = W-FGP849-WCMARQ                               
                MOVE 10 TO RUPTURE                                              
                IF FGP849-CAPPRO2 = W-FGP849-CAPPRO2                            
                 MOVE 9  TO RUPTURE                                             
                 IF FGP849-NCODIC2 = W-FGP849-NCODIC2                           
                  MOVE 8  TO RUPTURE                                            
                  IF FGP849-WDEB = W-FGP849-WDEB                                
                   MOVE 7  TO RUPTURE                                           
                   IF FGP849-NCODIC = W-FGP849-NCODIC                           
                    MOVE 6  TO RUPTURE                                          
                    IF FGP849-WENTNAT = W-FGP849-WENTNAT                        
                     MOVE 5  TO RUPTURE                                         
                     IF FGP849-WTYPENT = W-FGP849-WTYPENT                       
                      MOVE 4  TO RUPTURE                                        
                      IF FGP849-NSOCDEPOT = W-FGP849-NSOCDEPOT                  
                       MOVE 3  TO RUPTURE                                       
                       IF FGP849-NDEPOT = W-FGP849-NDEPOT                       
                        MOVE 2  TO RUPTURE                                      
                        IF FGP849-DCDE8 = W-FGP849-DCDE8                        
                         MOVE 1  TO RUPTURE                                     
                        END-IF                                                  
                       END-IF                                                   
                      END-IF                                                    
                     END-IF                                                     
                    END-IF                                                      
                   END-IF                                                       
                  END-IF                                                        
                 END-IF                                                         
                END-IF                                                          
               END-IF                                                           
              END-IF                                                            
             END-IF                                                             
            END-IF                                                              
           ELSE                                                                 
            MOVE 15 TO RUPTURE                                                  
           END-IF.                                                              
           IF CTR-FGP849 = 1 MOVE 15 TO RUPTURE.                                
       FIN-NUMERO-RUPTURE. EXIT.                                                
      *                                                                         
       RUPTURE-BOITE    SECTION.                                                
           INITIALIZE CTR-PAGE.                                                 
           MOVE FGP849-CBOITE   TO W-FGP849-CBOITE.                             
           MOVE FGP849-CETAT    TO W-FGP849-CETAT.                              
       FIN-RUPTURE-BOITE.    EXIT.                                              
      *                                                                         
       RUPTURE-NSOCIETE SECTION.                                                
           MOVE FGP849-CBOITE   TO W-FGP849-CBOITE.                             
           MOVE FGP849-CETAT    TO W-FGP849-CETAT.                              
           MOVE FGP849-NSOCIETE TO W-FGP849-NSOCIETE.                           
       FIN-RUPTURE-NSOCIETE. EXIT.                                              
      *                                                                         
       RUPTURE-CHEFPROD SECTION.                                                
           MOVE FGP849-CBOITE   TO W-FGP849-CBOITE.                             
           MOVE FGP849-CETAT    TO W-FGP849-CETAT.                              
           MOVE FGP849-NSOCIETE TO W-FGP849-NSOCIETE.                           
           MOVE FGP849-CHEFPROD TO W-FGP849-CHEFPROD.                           
       FIN-RUPTURE-CHEFPROD. EXIT.                                              
      *                                                                         
       RUPTURE-FAMILLE SECTION.                                                 
           MOVE FGP849-CBOITE   TO W-FGP849-CBOITE.                             
           MOVE FGP849-CETAT    TO W-FGP849-CETAT.                              
           MOVE FGP849-NSOCIETE TO W-FGP849-NSOCIETE.                           
           MOVE FGP849-CHEFPROD TO W-FGP849-CHEFPROD.                           
           MOVE FGP849-WSEQFAM  TO W-FGP849-WSEQFAM.                            
       FIN-RUPTURE-FAMILLE. EXIT.                                               
      *                                                                         
       RUPTURE-LAGREGATED SECTION.                                              
           MOVE FGP849-CBOITE        TO W-FGP849-CBOITE.                        
           MOVE FGP849-CETAT         TO W-FGP849-CETAT.                         
           MOVE FGP849-NSOCIETE      TO W-FGP849-NSOCIETE.                      
           MOVE FGP849-CHEFPROD      TO W-FGP849-CHEFPROD.                      
           MOVE FGP849-WSEQFAM       TO W-FGP849-WSEQFAM.                       
           MOVE FGP849-LAGREGATED    TO W-FGP849-LAGREGATED.                    
       FIN-RUPTURE-LAGREGATED. EXIT.                                            
      *                                                                         
       RUPTURE-NCODIC2 SECTION.                                                 
           MOVE FGP849-CBOITE        TO W-FGP849-CBOITE.                        
           MOVE FGP849-CETAT         TO W-FGP849-CETAT.                         
           MOVE FGP849-NSOCIETE      TO W-FGP849-NSOCIETE.                      
           MOVE FGP849-CHEFPROD      TO W-FGP849-CHEFPROD.                      
           MOVE FGP849-WSEQFAM       TO W-FGP849-WSEQFAM.                       
           MOVE FGP849-LAGREGATED    TO W-FGP849-LAGREGATED.                    
           MOVE FGP849-WCMARQ        TO W-FGP849-WCMARQ.                        
           MOVE FGP849-CAPPRO2       TO W-FGP849-CAPPRO2.                       
           MOVE FGP849-NCODIC2       TO W-FGP849-NCODIC2.                       
       FIN-RUPTURE-NCODIC2. EXIT.                                               
      *                                                                         
       RUPTURE-NCODIC SECTION.                                                  
           MOVE FGP849-CBOITE        TO W-FGP849-CBOITE.                        
           MOVE FGP849-CETAT         TO W-FGP849-CETAT.                         
           MOVE FGP849-NSOCIETE      TO W-FGP849-NSOCIETE.                      
           MOVE FGP849-CHEFPROD      TO W-FGP849-CHEFPROD.                      
           MOVE FGP849-WSEQFAM       TO W-FGP849-WSEQFAM.                       
           MOVE FGP849-LAGREGATED    TO W-FGP849-LAGREGATED.                    
           MOVE FGP849-WCMARQ        TO W-FGP849-WCMARQ.                        
           MOVE FGP849-CAPPRO2       TO W-FGP849-CAPPRO2.                       
           MOVE FGP849-NCODIC2       TO W-FGP849-NCODIC2.                       
           MOVE FGP849-WDEB          TO W-FGP849-WDEB.                          
           MOVE FGP849-NCODIC        TO W-FGP849-NCODIC.                        
       FIN-RUPTURE-NCODIC. EXIT.                                                
      *                                                                         
       RUPTURE-WENTNAT SECTION.                                                 
           MOVE FGP849-CBOITE        TO W-FGP849-CBOITE.                        
           MOVE FGP849-CETAT         TO W-FGP849-CETAT.                         
           MOVE FGP849-NSOCIETE      TO W-FGP849-NSOCIETE.                      
           MOVE FGP849-CHEFPROD      TO W-FGP849-CHEFPROD.                      
           MOVE FGP849-WSEQFAM       TO W-FGP849-WSEQFAM.                       
           MOVE FGP849-LAGREGATED    TO W-FGP849-LAGREGATED.                    
           MOVE FGP849-WCMARQ        TO W-FGP849-WCMARQ.                        
           MOVE FGP849-CAPPRO2       TO W-FGP849-CAPPRO2.                       
           MOVE FGP849-NCODIC2       TO W-FGP849-NCODIC2.                       
           MOVE FGP849-WDEB          TO W-FGP849-WDEB.                          
           MOVE FGP849-NCODIC        TO W-FGP849-NCODIC.                        
           MOVE FGP849-WENTNAT       TO W-FGP849-WENTNAT.                       
       FIN-RUPTURE-WENTNAT. EXIT.                                               
      *                                                                         
       RUPTURE-DCDE8 SECTION.                                                   
           MOVE FGP849-CBOITE        TO W-FGP849-CBOITE.                        
           MOVE FGP849-CETAT         TO W-FGP849-CETAT.                         
           MOVE FGP849-NSOCIETE      TO W-FGP849-NSOCIETE.                      
           MOVE FGP849-CHEFPROD      TO W-FGP849-CHEFPROD.                      
           MOVE FGP849-WSEQFAM       TO W-FGP849-WSEQFAM.                       
           MOVE FGP849-LAGREGATED    TO W-FGP849-LAGREGATED.                    
           MOVE FGP849-WCMARQ        TO W-FGP849-WCMARQ.                        
           MOVE FGP849-CAPPRO2       TO W-FGP849-CAPPRO2.                       
           MOVE FGP849-NCODIC2       TO W-FGP849-NCODIC2.                       
           MOVE FGP849-WDEB          TO W-FGP849-WDEB.                          
           MOVE FGP849-NCODIC        TO W-FGP849-NCODIC.                        
           MOVE FGP849-WENTNAT       TO W-FGP849-WENTNAT.                       
           MOVE FGP849-WTYPENT       TO W-FGP849-WTYPENT.                       
           MOVE FGP849-NSOCDEPOT     TO W-FGP849-NSOCDEPOT.                     
           MOVE FGP849-NDEPOT        TO W-FGP849-NDEPOT.                        
           MOVE FGP849-DCDE8         TO W-FGP849-DCDE8.                         
       FIN-RUPTURE-DCDE8. EXIT.                                                 
      *                                                                         
      *-------------------- ECRITURE DES LIGNES ETAT -----------------          
      *                                                                         
       ECRITURE-BLOC SECTION.                                                   
      *---------------                                                          
           IF CODIC-EDITE                                                       
              ADD 1                   TO NB-BLOC                                
              EVALUATE SAUT                                                     
                WHEN ' '  ADD 1  TO NB-LIGNE-BLOC NB-LIGNE-CODIC1               
                WHEN '0'  ADD 2  TO NB-LIGNE-BLOC NB-LIGNE-CODIC1               
                WHEN OTHER CONTINUE                                             
              END-EVALUATE                                                      
              IF NB-BLOC > MAX-BLOC                                             
                 MOVE 'TAB BLOC TROP PETIT' TO ABEND-MESS                       
                 PERFORM FIN-ANORMALE                                           
              END-IF                                                            
              MOVE LIGNE-IGP855       TO LIGNE-CODIC2(NB-BLOC)                  
              IF RUPTURE-ENTETE                                                 
                 PERFORM ECRITURE-ENTETE                                        
              END-IF                                                            
           END-IF.                                                              
       FIN-ECRITURE-BLOC. EXIT.                                                 
      *                                                                         
       ECRITURE-BLOC-2 SECTION.                                                 
      *---------------                                                          
           ADD 1                   TO NB-BLOC.                                  
           EVALUATE SAUT                                                        
             WHEN ' '  ADD 1  TO NB-LIGNE-BLOC NB-LIGNE-CODIC1                  
             WHEN '0'  ADD 2  TO NB-LIGNE-BLOC NB-LIGNE-CODIC1                  
             WHEN OTHER CONTINUE                                                
           END-EVALUATE.                                                        
           IF NB-BLOC > MAX-BLOC                                                
              MOVE 'TAB BLOC TROP PETIT' TO ABEND-MESS                          
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE LIGNE-IGP855       TO LIGNE-CODIC2(NB-BLOC).                    
       FIN-ECRITURE-BLOC-2. EXIT.                                               
      *                                                                         
       ECRITURE-ENTETE SECTION.                                                 
      *-----------------                                                        
           IF CODIC-EDITE                                                       
             ADD 1              TO CTR-PAGE                                     
             MOVE 0             TO CTR-LIGNE                                    
             MOVE FGP849-CETAT  TO IGP855-E00-CETAT                             
             MOVE FGP849-CBOITE TO IGP855-E00-CBOITE                            
             MOVE CORRESPONDING DATJMSA TO IGP855-E00-DATEDITE                  
             MOVE HH OF W-TIME          TO IGP855-E00-HEUREDITE                 
             MOVE MN OF W-TIME          TO IGP855-E00-MINUEDITE                 
             MOVE CTR-PAGE              TO IGP855-E00-NOPAGE                    
             MOVE '1'                   TO SAUT                                 
             MOVE IGP855-E00            TO LIGNE                                
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP849-NSOCIETE    TO IGP855-E05-NSOCIETE                     
             MOVE FGP849-LLIEU       TO IGP855-E05-LLIEU                        
             MOVE ' '                TO SAUT                                    
             MOVE IGP855-E05         TO LIGNE                                   
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP849-CHEFPROD   TO IGP855-E10-CHEFPROD                      
             MOVE FGP849-LCHEFPROD  TO IGP855-E10-LCHEFPROD                     
             MOVE ' '               TO SAUT                                     
             MOVE IGP855-E10        TO LIGNE                                    
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP849-CFAM       TO IGP855-E15-CFAM                          
             MOVE FGP849-LFAM       TO IGP855-E15-LFAM                          
             MOVE '0'               TO SAUT                                     
             MOVE IGP855-E15        TO LIGNE                                    
             WRITE LIGNE-IGP855                                                 
             ADD 2 TO CTR-LIGNE                                                 
      *                                                                         
             IF FGP849-LAGREGATED > SPACES                                      
                MOVE FGP849-LAGREGATED TO IGP855-E20-LAGREGATED                 
                MOVE '0'               TO SAUT                                  
                MOVE IGP855-E20        TO LIGNE                                 
                WRITE LIGNE-IGP855                                              
                ADD 2 TO CTR-LIGNE                                              
             END-IF                                                             
      *                                                                         
             MOVE ' '               TO SAUT                                     
             MOVE IGP855-E30        TO LIGNE                                    
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP855-E35        TO LIGNE                                    
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP855-E40        TO LIGNE                                    
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP855-E45        TO LIGNE                                    
             WRITE LIGNE-IGP855                                                 
             ADD 1 TO CTR-LIGNE                                                 
             SET ENTETE-ECRITE  TO TRUE                                         
           ELSE                                                                 
             SET RUPTURE-ENTETE TO TRUE                                         
           END-IF.                                                              
       FIN-ECRITURE-ENTETE. EXIT.                                               
      *                                                                         
       ECRITURE-ENTETE-PRE SECTION.                                             
      *-----------------                                                        
           ADD 1         TO CTR-PAGE.                                           
           MOVE 0        TO CTR-LIGNE.                                          
           MOVE W-FGP849-CETAT  TO IGP855-E00-CETAT.                            
           MOVE W-FGP849-CBOITE TO IGP855-E00-CBOITE.                           
           MOVE CORRESPONDING DATJMSA TO IGP855-E00-DATEDITE.                   
           MOVE HH OF W-TIME          TO IGP855-E00-HEUREDITE.                  
           MOVE MN OF W-TIME          TO IGP855-E00-MINUEDITE.                  
           MOVE CTR-PAGE              TO IGP855-E00-NOPAGE.                     
           MOVE '1'                   TO SAUT.                                  
           MOVE IGP855-E00            TO LIGNE.                                 
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP849-NSOCIETE    TO IGP855-E05-NSOCIETE.                    
           MOVE W-FGP849-LLIEU       TO IGP855-E05-LLIEU.                       
           MOVE ' '                TO SAUT.                                     
           MOVE IGP855-E05         TO LIGNE.                                    
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP849-CHEFPROD   TO IGP855-E10-CHEFPROD.                     
           MOVE W-FGP849-LCHEFPROD  TO IGP855-E10-LCHEFPROD.                    
           MOVE ' '               TO SAUT.                                      
           MOVE IGP855-E10        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP849-CFAM       TO IGP855-E15-CFAM.                         
           MOVE W-FGP849-LFAM       TO IGP855-E15-LFAM.                         
           MOVE '0'               TO SAUT.                                      
           MOVE IGP855-E15        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 2 TO CTR-LIGNE.                                                  
      *                                                                         
           IF W-FGP849-LAGREGATED > SPACES                                      
              MOVE W-FGP849-LAGREGATED TO IGP855-E20-LAGREGATED                 
              MOVE '0'               TO SAUT                                    
              MOVE IGP855-E20        TO LIGNE                                   
              WRITE LIGNE-IGP855                                                
              ADD 2 TO CTR-LIGNE                                                
           END-IF.                                                              
      *                                                                         
           MOVE ' '               TO SAUT.                                      
           MOVE IGP855-E25        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP855-E30        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP855-E35        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP855-E40        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP855-E45        TO LIGNE.                                     
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
       FIN-ECRITURE-ENTETE-PRE. EXIT.                                           
       ECRITURE-DETAIL SECTION.                                                 
      *-----------------                                                        
           IF FGP849-WGROUPE NOT = 'E'                                          
              PERFORM ECRITURE-DETAIL05                                         
              SET CODIC-IMPRIME TO TRUE                                         
           ELSE                                                                 
              PERFORM ECRITURE-DETAIL10                                         
           END-IF.                                                              
           IF FGP849-WSENSAPPRO > SPACES                                        
           AND CODIC-IMPRIME                                                    
              PERFORM ECRITURE-DETAIL15                                         
           END-IF.                                                              
           IF CODIC-EDITE                                                       
              MOVE FGP849-LSTATCOMP  TO W-LSTATCOMP                             
              MOVE FGP849-WSENSAPPRO TO W-WSENSAPPRO                            
              MOVE FGP849-CEXPO      TO W-CEXPO                                 
              MOVE FGP849-WOA        TO W-WOA                                   
              MOVE FGP849-PSTDTTC    TO W-PSTDTTC                               
           END-IF.                                                              
           MOVE FGP849-WENTNAT TO W-WENTNAT.                                    
           MOVE FGP849-NCODIC  TO W-NCODIC .                                    
       FIN-ECRITURE-DETAIL. EXIT.                                               
       ECRITURE-T0     SECTION.                                                 
      *-----------------                                                        
           IF W-FGP849-WENTNAT NOT = FGP849-WENTNAT                             
           AND W-FGP849-NCODIC = FGP849-NCODIC                                  
           AND CODIC-IMPRIME                                                    
              MOVE ' ' TO SAUT                                                  
              MOVE IGP855-T00         TO LIGNE                                  
              PERFORM ECRITURE-BLOC                                             
           END-IF.                                                              
       FIN-ECRITURE-T0. EXIT.                                                   
       ECRITURE-DETAIL05 SECTION.                                               
      *-----------------                                                        
           IF RUPTURE > 6                                                       
              MOVE FGP849-CMARQ       TO IGP855-D05-CMARQ                       
              MOVE FGP849-LREFFOURN   TO IGP855-D05-LREFFOURN                   
           ELSE                                                                 
              INITIALIZE IGP855-D05-CMARQ                                       
                         IGP855-D05-LREFFOURN                                   
           END-IF.                                                              
           IF RUPTURE > 5                                                       
      *    OR (RUPTURE > 2 AND FGP849-NSOCIETE = 'XXX')                         
           OR (FGP849-WTYPENT = 'F' AND FGP849-NSOCIETE = 'XXX')                
              IF FGP849-WTYPENT NOT = 'F'                                       
                 MOVE FGP849-NCODIC   TO IGP855-D05-NCODIC                      
              ELSE                                                              
                 MOVE SPACES          TO IGP855-D05-NCODIC                      
              END-IF                                                            
              MOVE FGP849-LSTATCOMP   TO IGP855-D05-LSTATCOMP                   
              MOVE FGP849-CEXPO       TO IGP855-D05-CEXPO                       
              MOVE FGP849-WOA         TO IGP855-D05-WOA                         
              MOVE FGP849-PSTDTTC     TO IGP855-D05-PSTDTTC                     
              COMPUTE W-MASKCAL6                                                
              = FGP849-QS-1 + FGP849-QS-2 + FGP849-QS-3 + FGP849-QS-4           
              MOVE W-MASKCAL6        TO W-MASKED6                               
              MOVE W-MASKED6         TO IGP855-D05-Q4S                          
              MOVE FGP849-QS-1       TO W-MASKED6                               
              MOVE W-MASKED6         TO IGP855-D05-QS-1                         
              MOVE FGP849-QS-2       TO W-MASKED6                               
              MOVE W-MASKED6         TO IGP855-D05-QS-2                         
              MOVE FGP849-QS-3       TO W-MASKED6                               
              MOVE W-MASKED6         TO IGP855-D05-QS-3                         
              MOVE FGP849-QS-4       TO W-MASKED6                               
              MOVE W-MASKED6         TO IGP855-D05-QS-4                         
              MOVE FGP849-QSTOCKMAG  TO W-MASKED5                               
              MOVE W-MASKED5         TO IGP855-D05-QSTOCKMAG                    
MPSUP *       MOVE FGP849-QSTOCKBOU  TO W-MASKED5                               
MPSUP *       MOVE W-MASKED5         TO IGP855-D05-QSTOCKBOU                    
MPADD         MOVE SPACES            TO IGP855-D05-QSTOCKBOU                    
              MOVE FGP849-NBMAG      TO W-MASKED3NS                             
              MOVE W-MASKED3NS       TO IGP855-D05-NBMAG                        
MPSUP *       MOVE FGP849-NBBOU      TO W-MASKED3NS                             
MPSUP *       MOVE W-MASKED3NS       TO IGP855-D05-NBBOU                        
MPADD         MOVE SPACES            TO IGP855-D05-NBBOU                        
           ELSE                                                                 
              INITIALIZE IGP855-D05-LSTATCOMP IGP855-D05-Q4S                    
                         IGP855-D05-QS-1      IGP855-D05-QSTOCKMAG              
                         IGP855-D05-QSTOCKBOU IGP855-D05-NBBOU                  
                         IGP855-D05-QS-2      IGP855-D05-QS-3                   
                         IGP855-D05-QS-4      IGP855-D05-CEXPO                  
                         IGP855-D05-NBMAG     IGP855-D05-NCODIC                 
                         IGP855-D05-PSTDTTC   IGP855-D05-WOA                    
           END-IF.                                                              
      * LE LSTATCOMP EST AFFICHE POUR LA PREMIERE LIGNE DEPOT DU CODIC          
      * ET NE SERA RAFFICHE QUE SI IL EST DIFFERENT POUR LES DEPOTS SUIV        
           IF FGP849-LSTATCOMP = W-LSTATCOMP                                    
           AND FGP849-WSENSAPPRO = W-WSENSAPPRO                                 
              INITIALIZE IGP855-D05-LSTATCOMP FGP849-WSENSAPPRO                 
           END-IF.                                                              
           IF FGP849-PSTDTTC   = W-PSTDTTC                                      
              INITIALIZE IGP855-D05-PSTDTTC                                     
           END-IF.                                                              
           IF FGP849-CEXPO     = W-CEXPO                                        
              INITIALIZE IGP855-D05-CEXPO                                       
           END-IF.                                                              
           IF FGP849-WOA       = W-WOA                                          
              INITIALIZE IGP855-D05-WOA                                         
           END-IF.                                                              
           IF RUPTURE > 2                                                       
              MOVE FGP849-QSTOCKDIS   TO W-MASKED7                              
              MOVE W-MASKED7          TO IGP855-D05-QSTOCKDIS                   
              MOVE FGP849-QSTOCKRES   TO W-MASKED6                              
              MOVE W-MASKED6          TO IGP855-D05-QSTOCKRES                   
              MOVE FGP849-NSOCDEPOT   TO IGP855-D05-NSOCDEPOT                   
              MOVE FGP849-NDEPOT      TO IGP855-D05-NDEPOT                      
              MOVE '8S'               TO W-CODE                                 
              INITIALIZE W-NBRE                                                 
              IF RUPTURE > 2                                                    
                 ADD FGP849-QSTOCKDIS     TO W-NBRE                             
                 ADD FGP849-QSTOCKRES     TO W-NBRE                             
                 IF RUPTURE > 5                                                 
                 OR (FGP849-WTYPENT = 'F' AND FGP849-NSOCIETE = 'XXX')          
                    ADD FGP849-QSTOCKMAG TO W-NBRE                              
                    ADD FGP849-QSTOCKBOU  TO W-NBRE                             
                 END-IF                                                         
              END-IF                                                            
              PERFORM TEST-CADRAGE                                              
              MOVE EDIT8            TO IGP855-D05-QSTOCKTOT                     
           ELSE                                                                 
              INITIALIZE IGP855-D05-QSTOCKDIS IGP855-D05-QSTOCKRES              
                         IGP855-D05-NSOCDEPOT IGP855-D05-NDEPOT                 
                         IGP855-D05-QSTOCKTOT                                   
           END-IF.                                                              
           IF FGP849-NSOCIETE = 'YYY'                                           
           AND FGP849-WTYPENT > '1' AND FGP849-WTYPENT < '7'                    
              MOVE FGP849-NFILIALE (1) TO IGP855-D05-NSOCDEPOT                  
              PERFORM VARYING FI-CPT FROM 1 BY 1                                
                UNTIL FI-CPT > FI-MAX                                           
                OR FGP849-NFILIALE (FI-CPT) = SPACES OR LOW-VALUE               
                  IF FGP849-NFILIALE (FI-CPT) < IGP855-D05-NSOCDEPOT            
                    MOVE FGP849-NFILIALE(FI-CPT) TO IGP855-D05-NSOCDEPOT        
                  END-IF                                                        
              END-PERFORM                                                       
              MOVE '...' TO IGP855-D05-NDEPOT                                   
              INITIALIZE IGP855-D05-QSTOCKDIS IGP855-D05-QSTOCKRES              
              IF  FGP849-WENTNAT = W-WENTNAT                                    
              AND FGP849-NCODIC  = W-NCODIC                                     
                  INITIALIZE IGP855-D05-NCODIC                                  
              END-IF                                                            
           END-IF                                                               
           IF RUPTURE > 1                                                       
              MOVE FGP849-DCDE4       TO IGP855-D05-DCDE4                       
              MOVE FGP849-QCDE        TO IGP855-D05-QCDE                        
              MOVE FGP849-QCDERES     TO IGP855-D05-QCDERES                     
           ELSE                                                                 
              INITIALIZE IGP855-D05-DCDE4 IGP855-D05-QCDE                       
                         IGP855-D05-QCDERES                                     
           END-IF.                                                              
           MOVE ' '                TO SAUT.                                     
           MOVE IGP855-D05         TO LIGNE.                                    
           PERFORM ECRITURE-BLOC.                                               
       FIN-ECRITURE-DETAIL05. EXIT.                                             
       ECRITURE-DETAIL10 SECTION.                                               
      *-----------------                                                        
           IF RUPTURE > 6                                                       
              MOVE FGP849-CMARQ       TO IGP855-D10-CMARQ                       
              MOVE FGP849-LREFFOURN   TO IGP855-D10-LREFFOURN                   
              MOVE FGP849-WOA         TO IGP855-D10-WOA                         
              MOVE FGP849-PSTDTTC     TO IGP855-D10-PSTDTTC                     
           ELSE                                                                 
           INITIALIZE IGP855-D10-CMARQ                                          
                      IGP855-D10-LREFFOURN IGP855-D10-WOA                       
                      IGP855-D10-PSTDTTC                                        
           END-IF.                                                              
           IF RUPTURE > 5                                                       
              MOVE FGP849-NCODIC      TO IGP855-D10-NCODIC                      
              MOVE FGP849-LSTATCOMP   TO IGP855-D10-LSTATCOMP                   
              MOVE FGP849-CEXPO       TO IGP855-D10-CEXPO                       
           ELSE                                                                 
              INITIALIZE IGP855-D10-LSTATCOMP IGP855-D10-NCODIC                 
                         IGP855-D10-CEXPO                                       
           END-IF.                                                              
           IF FGP849-LSTATCOMP = W-LSTATCOMP                                    
           AND FGP849-WSENSAPPRO = W-WSENSAPPRO                                 
      *    AND RUPTURE <= 6                                                     
              INITIALIZE IGP855-D10-LSTATCOMP FGP849-WSENSAPPRO                 
           END-IF.                                                              
           IF W-NCODIC NOT = FGP849-NCODIC                                      
              MOVE ' '                TO SAUT                                   
              MOVE IGP855-D10         TO LIGNE                                  
              PERFORM ECRITURE-BLOC                                             
              SET CODIC-IMPRIME TO TRUE                                         
           ELSE                                                                 
              SET CODIC-ELEMENT TO TRUE                                         
           END-IF.                                                              
       FIN-ECRITURE-DETAIL10. EXIT.                                             
       ECRITURE-DETAIL15 SECTION.                                               
      *-----------------                                                        
           IF RUPTURE > 6                                                       
              MOVE FGP849-CMARQ       TO IGP855-D15-CMARQ                       
              MOVE FGP849-LREFFOURN   TO IGP855-D15-LREFFOURN                   
              MOVE FGP849-WOA         TO IGP855-D15-WOA                         
              MOVE FGP849-PSTDTTC     TO IGP855-D15-PSTDTTC                     
            ELSE                                                                
              INITIALIZE IGP855-D15-CMARQ   IGP855-D15-PSTDTTC                  
                         IGP855-D15-LREFFOURN IGP855-D15-WOA                    
            END-IF.                                                             
            IF RUPTURE > 5                                                      
               MOVE FGP849-NCODIC      TO IGP855-D15-NCODIC                     
               MOVE FGP849-LSTATCOMP   TO IGP855-D15-LSTATCOMP                  
            ELSE                                                                
               INITIALIZE IGP855-D15-LSTATCOMP IGP855-D15-NCODIC                
            END-IF.                                                             
            MOVE '+'                TO SAUT.                                    
            MOVE IGP855-D15         TO LIGNE.                                   
            PERFORM ECRITURE-BLOC.                                              
            PERFORM ECRITURE-BLOC.                                              
       FIN-ECRITURE-DETAIL15. EXIT.                                             
       BAS-PAGE SECTION.                                                        
      *---------                                                                
           MOVE ' '                TO SAUT.                                     
           MOVE IGP855-B05         TO LIGNE.                                    
           PERFORM ECRITURE-LIGNE-SUI.                                          
       FIN-BAS-PAGE. EXIT.                                                      
       CUMUL-TOTAUX SECTION.                                                    
      *-----------------                                                        
           IF CODIC-EDITE                                                       
              PERFORM CUMUL-CODIC                                               
              IF FGP849-LAGREGATED > SPACES                                     
                 PERFORM REMPLISSAGE-TAB-T25                                    
              END-IF                                                            
              PERFORM REMPLISSAGE-TAB-T45                                       
              PERFORM REMPLISSAGE-TAB-T65                                       
           END-IF.                                                              
           PERFORM COMPTEUR-FAMILLE.                                            
       FIN-CUMUL-TOTAUX. EXIT.                                                  
      *                                                                         
       CUMUL-CODIC SECTION.                                                     
      *-------------                                                            
           IF FGP849-WGROUPE NOT = 'E'                                          
             IF RUPTURE > 5                                                     
             OR (FGP849-NSOCIETE = 'YYY'                                        
             AND FGP849-WTYPENT NOT = '9')                                      
             OR (FGP849-NSOCIETE = 'XXX' AND FGP849-WTYPENT = 'F')              
                 COMPUTE W-Q4S                                                  
              = FGP849-QS-1 + FGP849-QS-2 + FGP849-QS-3 + FGP849-QS-4           
                 ADD W-Q4S              TO W-T10-TCQ4S                          
                 ADD FGP849-QS-1        TO W-T10-TCQS-1                         
                 ADD FGP849-QS-2        TO W-T10-TCQS-2                         
                 ADD FGP849-QS-3        TO W-T10-TCQS-3                         
                 ADD FGP849-QS-4        TO W-T10-TCQS-4                         
                 ADD FGP849-QSTOCKMAG   TO W-T10-TCQSTOCKMAG                    
                 ADD FGP849-QSTOCKBOU   TO W-T10-TCQSTOCKBOU                    
                 ADD FGP849-NBMAG       TO W-T10-TCNBMAG                        
                 ADD FGP849-NBBOU       TO W-T10-TCNBBOU                        
             END-IF                                                             
             IF RUPTURE > 2                                                     
                 ADD FGP849-QSTOCKDIS   TO W-T10-TCQSTOCKDIS                    
                 ADD FGP849-QSTOCKRES   TO W-T10-TCQSTOCKRES                    
             END-IF                                                             
             IF RUPTURE > 1                                                     
                 ADD FGP849-QCDE        TO W-T10-TCQCDE                         
                 ADD FGP849-QCDERES     TO W-T10-TCQCDERES                      
             END-IF                                                             
           END-IF.                                                              
       FIN-CUMUL-CODIC. EXIT.                                                   
      *                                                                         
       ECRITURE-T10-T15 SECTION.                                                
      *-----------------                                                        
           IF NB-LIGNE-CODIC1 > 1                                               
           IF W-FGP849-WGROUPE NOT = 'E'                                        
              MOVE ' '               TO SAUT                                    
              MOVE IGP855-T05        TO LIGNE                                   
              PERFORM ECRITURE-BLOC-2                                           
              MOVE W-T10-TCQ4S          TO IGP855-T10-TCQ4S                     
              MOVE W-T10-TCQS-1         TO IGP855-T10-TCQS-1                    
              MOVE W-T10-TCQS-2         TO IGP855-T10-TCQS-2                    
              MOVE W-T10-TCQS-3         TO IGP855-T10-TCQS-3                    
              MOVE W-T10-TCQS-4         TO IGP855-T10-TCQS-4                    
              MOVE W-T10-TCQSTOCKMAG    TO IGP855-T10-TCQSTOCKMAG               
MPSUP *       MOVE W-T10-TCQSTOCKBOU    TO IGP855-T10-TCQSTOCKBOU               
              MOVE W-T10-TCNBMAG        TO IGP855-T10-TCNBMAG                   
MPSUP *       MOVE W-T10-TCNBBOU        TO IGP855-T10-TCNBBOU                   
              MOVE W-T10-TCQSTOCKDIS    TO IGP855-T10-TCQSTOCKDIS               
              MOVE W-T10-TCQSTOCKRES    TO IGP855-T10-TCQSTOCKRES               
              MOVE '8S'             TO W-CODE                                   
              COMPUTE W-NBRE                                                    
              = W-T10-TCQSTOCKMAG + W-T10-TCQSTOCKBOU                           
              + W-T10-TCQSTOCKDIS + W-T10-TCQSTOCKRES                           
              PERFORM TEST-CADRAGE                                              
              MOVE EDIT8            TO IGP855-T10-TCQSTOCKTOT                   
              MOVE W-T10-TCQCDE         TO IGP855-T10-TCQCDE                    
              MOVE W-T10-TCQCDERES      TO IGP855-T10-TCQCDERES                 
              MOVE IGP855-T10           TO LIGNE                                
              PERFORM ECRITURE-BLOC-2                                           
              INITIALIZE W-T10                                                  
           END-IF                                                               
           END-IF.                                                              
           INITIALIZE W-T10 NB-LIGNE-CODIC1.                                    
       FIN-ECRITURE-T10-T15. EXIT.                                              
      *                                                                         
       LIGNE-T20 SECTION.                                                       
      *----------                                                               
           IF NB-LIGNE-BLOC > 0                                                 
           IF W-FGP849-NCODIC2 NOT = FGP849-NCODIC2                             
              IF ((NB-LIGNE-BLOC + CTR-LIGNE) > (MAX-LIGNE))                    
                  MOVE ' ' TO SAUT                                              
                  INITIALIZE LIGNE                                              
                  MOVE '** SUITE **'               TO LIGNE                     
                  PERFORM ECRITURE-LIGNE-SUI                                    
                  IF (((CTR-LIGNE NOT = 10)                                     
                    AND (W-FGP849-LAGREGATED = SPACES))                         
                    OR ((CTR-LIGNE NOT = 12)                                    
                    AND (W-FGP849-LAGREGATED NOT = SPACES)))                    
                      PERFORM ECRITURE-ENTETE-PRE                               
                  END-IF                                                        
              END-IF                                                            
              PERFORM VARYING I-BLOC FROM 1 BY 1                                
                      UNTIL I-BLOC > NB-BLOC                                    
                      MOVE LIGNE-CODIC2(I-BLOC) TO LIGNE-IGP855                 
                      IF SAUT = '+'                                             
                         SUBTRACT 1 FROM CTR-LIGNE                              
                      END-IF                                                    
                      PERFORM ECRITURE-LIGNE-SUI                                
              END-PERFORM                                                       
              INITIALIZE W-BLOC NB-BLOC NB-LIGNE-BLOC                           
              MOVE ' '                TO SAUT                                   
              MOVE IGP855-T20         TO LIGNE                                  
              PERFORM ECRITURE-LIGNE-SUI                                        
           END-IF                                                               
           END-IF.                                                              
       FIN-LIGNE-T20. EXIT.                                                     
      *----------------- VENTILATION SEG. MRK -----------------------           
      *                                                                         
       REMPLISSAGE-TAB-T25 SECTION.                                             
      *---------------------                                                    
           PERFORM VARYING I-T25 FROM 1 BY 1                                    
                   UNTIL ( I-T25 > NB-T25 )                                     
                   OR    ( W-T25-LMARQ(I-T25)  = FGP849-LMARQ )                 
           END-PERFORM.                                                         
           IF (I-T25 > NB-T25)                                                  
                 ADD 1 TO NB-T25                                                
                 IF NB-T25 > MAX-T25                                            
                    MOVE 'TAB T25 TROP PETIT' TO ABEND-MESS                     
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP849-LMARQ      TO W-T25-LMARQ(NB-T25)                  
                 MOVE NB-T25            TO I-T25                                
           END-IF.                                                              
      *    IF  FGP849-LSTATCOMP NOT = 'EPD'                                     
      *    AND FGP849-LSTATCOMP NOT = 'EPF'                                     
      *    AND FGP849-LSTATCOMP NOT = 'CQE'                                     
             IF RUPTURE > 5                                                     
             OR (FGP849-NSOCIETE = 'YYY'                                        
             AND FGP849-WTYPENT NOT = '9')                                      
             OR (FGP849-NSOCIETE = 'XXX' AND FGP849-WTYPENT = 'F')              
                ADD W-Q4S              TO W-T25-TLQ4S      (I-T25)              
                ADD FGP849-QS-1        TO W-T25-TLQS-1     (I-T25)              
                ADD FGP849-QS-2        TO W-T25-TLQS-2     (I-T25)              
                ADD FGP849-QS-3        TO W-T25-TLQS-3      (I-T25)             
                ADD FGP849-QS-4        TO W-T25-TLQS-4      (I-T25)             
                ADD FGP849-QSTOCKMAG   TO W-T25-TLQSTOCKMAG (I-T25)             
                ADD FGP849-QSTOCKBOU   TO W-T25-TLQSTOCKBOU (I-T25)             
             END-IF.                                                            
             IF RUPTURE > 2                                                     
                ADD FGP849-QSTOCKDIS   TO W-T25-TLQSTOCKDIS(I-T25)              
                ADD FGP849-QSTOCKRES   TO W-T25-TLQSTOCKRES(I-T25)              
             END-IF.                                                            
             IF RUPTURE > 1                                                     
                ADD FGP849-QCDE        TO W-T25-TLQCDE(I-T25)                   
                ADD FGP849-QCDERES     TO W-T25-TLQCDERES(I-T25)                
             END-IF.                                                            
      *    END-IF.                                                              
       FIN-REMPLISSAGE-TAB-T25. EXIT.                                           
      *                                                                         
       ECRITURE-T25 SECTION.                                                    
      *--------------                                                           
           IF W-FGP849-LAGREGATED > SPACES                                      
           AND NB-T25 > 0                                                       
              MOVE ' '  TO SAUT                                                 
              MOVE W-FGP849-LAGREGATED TO IGP855-T25-LAGREGATED                 
              PERFORM SEGSOMME-T25                                              
              MOVE IGP855-T25        TO LIGNE                                   
              PERFORM ECRITURE-LIGNE-SUI                                        
              MOVE IGP855-T30        TO LIGNE                                   
              PERFORM ECRITURE-LIGNE-SUI                                        
              PERFORM ECRITURE-LIGNE-T35                                        
      *       IF RUPTURE < 13                                                   
                 PERFORM BAS-PAGE                                               
      *       END-IF                                                            
              INITIALIZE W-T25 NB-T25 SEGSOMME                                  
           END-IF.                                                              
       FIN-ECRITURE-T25. EXIT.                                                  
      *                                                                         
       SEGSOMME-T25 SECTION.                                                    
           PERFORM VARYING I-T25 FROM 1 BY 1                                    
                   UNTIL I-T25 > NB-T25                                         
                   ADD W-T25-TLQ4S       (I-T25) TO SEGTLQ4S                    
                   ADD W-T25-TLQS-1      (I-T25) TO SEGTLQS-1                   
                   ADD W-T25-TLQS-2      (I-T25) TO SEGTLQS-2                   
                   ADD W-T25-TLQS-3      (I-T25) TO SEGTLQS-3                   
                   ADD W-T25-TLQS-4      (I-T25) TO SEGTLQS-4                   
                   ADD W-T25-TLQSTOCKMAG (I-T25) TO SEGTLQSTOCKMAG              
                   ADD W-T25-TLQSTOCKBOU (I-T25) TO SEGTLQSTOCKBOU              
                   ADD W-T25-TLQSTOCKDIS (I-T25) TO SEGTLQSTOCKDIS              
                   ADD W-T25-TLQSTOCKRES (I-T25) TO SEGTLQSTOCKRES              
                   ADD W-T25-TLQCDE      (I-T25) TO SEGTLQCDE                   
                   ADD W-T25-TLQCDERES   (I-T25) TO SEGTLQCDERES                
           END-PERFORM.                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE SEGTLQ4S    TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T25-TLQ4S.                                
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE SEGTLQS-1   TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T25-TLQS-1.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE SEGTLQS-2   TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T25-TLQS-2.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE SEGTLQS-3   TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T25-TLQS-3.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE SEGTLQS-4   TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T25-TLQS-4.                               
      *                                                                         
           MOVE '5S'             TO W-CODE.                                     
           MOVE SEGTLQSTOCKMAG   TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T25-TLQSTOCKMAG.                     
      *                                                                         
MPSUP *    MOVE '5S'             TO W-CODE.                                     
MPSUP *    MOVE SEGTLQSTOCKBOU   TO W-NBRE.                                     
MPSUP *    PERFORM TEST-CADRAGE.                                                
MPSUP *    MOVE EDIT5            TO IGP855-T25-TLQSTOCKBOU.                     
      *                                                                         
           MOVE '7S'             TO W-CODE.                                     
           MOVE SEGTLQSTOCKDIS   TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7            TO IGP855-T25-TLQSTOCKDIS.                     
      *                                                                         
           MOVE '6S'             TO W-CODE.                                     
           MOVE SEGTLQSTOCKRES   TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6            TO IGP855-T25-TLQSTOCKRES.                     
      *                                                                         
           MOVE '6'              TO W-CODE.                                     
           MOVE SEGTLQCDE        TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6            TO IGP855-T25-TLQCDE.                          
      *                                                                         
           MOVE '5'              TO W-CODE.                                     
           MOVE SEGTLQCDERES     TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T25-TLQCDERES.                       
       FIN-SEGSOMME-T25. EXIT.                                                  
      *                                                                         
      * CETTE PROCEDURE EVITE LA TRONCATURE A GAUCHE DES NBRES                  
      *                                                                         
      * EN ENTREE : W-LCODE -> NBRE DE CARACTERES MAXIMUM EDITES                
      *             W-SIGNE -> S POUR UN NOMBRE SIGNE (AUTREMENT SPACE)         
      *             W-NBRE  -> LE NOMBRE QUE L ON VEUT EDITER                   
      * EN SORTIE : EDIT?   -> LE NOMBRE SOUS LA FORME PIC X                    
      *             SI LE NOMBRE EST PLUS GRAND QUE LA ZONE D EDITION           
      *             ON SUPPRIME LES CHIFFRES DES MILLIERS QUI SONT              
      *             REMPLACES PAR UN 'K'                                        
      *                                                                         
       TEST-CADRAGE       SECTION.                                              
           EVALUATE W-LCODE                                                     
             WHEN '3'                                                           
                  MOVE 999     TO W-MAX                                         
             WHEN '4'                                                           
                  MOVE 9999    TO W-MAX                                         
             WHEN '5'                                                           
                  MOVE 99999   TO W-MAX                                         
             WHEN '6'                                                           
                  MOVE 999999  TO W-MAX                                         
             WHEN '7'                                                           
                  MOVE 9999999 TO W-MAX                                         
             WHEN '8'                                                           
                  MOVE 99999999 TO W-MAX                                        
             WHEN OTHER                                                         
                  MOVE 99999   TO W-MAX                                         
           END-EVALUATE                                                         
           IF W-NBRE > W-MAX                                                    
              DIVIDE W-NBRE BY 1000 GIVING QUOTIENT REMAINDER RESTE             
              EVALUATE W-LCODE                                                  
                WHEN '3'                                                        
                     MOVE QUOTIENT  TO W-NUMED2                                 
                     MOVE W-NUMED2  TO W-EDIT3                                  
                     MOVE 'K'       TO W-MILLIERS3                              
                WHEN '4'                                                        
                     MOVE QUOTIENT  TO W-NUMED3                                 
                     MOVE W-NUMED3  TO W-EDIT4                                  
                     MOVE 'K'       TO W-MILLIERS4                              
                WHEN '5'                                                        
                     MOVE QUOTIENT  TO W-NUMED4                                 
                     MOVE W-NUMED4  TO W-EDIT5                                  
                     MOVE 'K'       TO W-MILLIERS5                              
                WHEN '6'                                                        
                     MOVE QUOTIENT  TO W-NUMED5                                 
                     MOVE W-NUMED5  TO W-EDIT6                                  
                     MOVE 'K'       TO W-MILLIERS6                              
                WHEN '7'                                                        
                     MOVE QUOTIENT  TO W-NUMED6                                 
                     MOVE W-NUMED6  TO W-EDIT7                                  
                     MOVE 'K'       TO W-MILLIERS7                              
                WHEN '8'                                                        
                     MOVE QUOTIENT  TO W-NUMED7                                 
                     MOVE W-NUMED7  TO W-EDIT8                                  
                     MOVE 'K'       TO W-MILLIERS8                              
                WHEN OTHER                                                      
                     MOVE '*******' TO EDITX                                    
              END-EVALUATE                                                      
           ELSE                                                                 
              IF W-NBRE < 0                                                     
                 EVALUATE W-CODE                                                
                   WHEN '4'                                                     
                        MOVE W-NBRE    TO W-NUMED4                              
                        MOVE W-NUMED4  TO EDIT4                                 
                   WHEN '5'                                                     
                        MOVE W-NBRE    TO W-NUMED5                              
                        MOVE W-NUMED5  TO EDIT5                                 
                   WHEN '5S'                                                    
                        MOVE W-NBRE    TO W-NUMED5S                             
                        MOVE W-NUMED5S TO EDIT5                                 
                   WHEN '6S'                                                    
                        MOVE W-NBRE    TO W-NUMED6S                             
                        MOVE W-NUMED6S TO EDIT6                                 
                   WHEN '7S'                                                    
                        MOVE W-NBRE    TO W-NUMED7S                             
                        MOVE W-NUMED7S TO EDIT7                                 
                   WHEN '8S'                                                    
                        MOVE W-NBRE    TO W-NUMED8S                             
                        MOVE W-NUMED8S TO EDIT8                                 
                   WHEN OTHER                                                   
                        MOVE '*******'  TO EDITX                                
                 END-EVALUATE                                                   
              ELSE                                                              
                 EVALUATE W-LCODE                                               
                   WHEN '3'                                                     
                        MOVE W-NBRE    TO W-NUMED3                              
                        MOVE W-NUMED3  TO EDIT3                                 
                   WHEN '4'                                                     
                        MOVE W-NBRE    TO W-NUMED4                              
                        MOVE W-NUMED4  TO EDIT4                                 
                   WHEN '5'                                                     
                        MOVE W-NBRE    TO W-NUMED5                              
                        MOVE W-NUMED5  TO EDIT5                                 
                   WHEN '6'                                                     
                        MOVE W-NBRE    TO W-NUMED6                              
                        MOVE W-NUMED6  TO EDIT6                                 
                   WHEN '7'                                                     
                        MOVE W-NBRE    TO W-NUMED7                              
                        MOVE W-NUMED7  TO EDIT7                                 
                   WHEN '8'                                                     
                        MOVE W-NBRE    TO W-NUMED8                              
                        MOVE W-NUMED8  TO EDIT8                                 
                   WHEN OTHER                                                   
                        MOVE '*******'  TO EDITX                                
                 END-EVALUATE                                                   
              END-IF                                                            
           END-IF.                                                              
           IF EDITX = SPACES                                                    
              MOVE '0' TO EDITX                                                 
           END-IF.                                                              
       FIN-TEST-CADRAGE.       EXIT.                                            
      *                                                                         
       ECRITURE-LIGNE-T35 SECTION.                                              
           MOVE ' ' TO SAUT.                                                    
           PERFORM VARYING I-T25 FROM 1 BY 1                                    
                         UNTIL I-T25 > NB-T25                                   
             MOVE W-T25-LMARQ       (I-T25) TO IGP855-T35-LMARQ                 
             MOVE W-T25-TLQ4S       (I-T25) TO IGP855-T35-TLCQ4S                
             MOVE W-T25-TLQS-1      (I-T25) TO IGP855-T35-TLCQS-1               
             MOVE W-T25-TLQS-2      (I-T25) TO IGP855-T35-TLCQS-2               
             MOVE W-T25-TLQS-3      (I-T25) TO IGP855-T35-TLCQS-3               
             MOVE W-T25-TLQS-4      (I-T25) TO IGP855-T35-TLCQS-4               
             MOVE W-T25-TLQSTOCKMAG (I-T25) TO IGP855-T35-TLCQSTOCKMAG          
MP    *      MOVE W-T25-TLQSTOCKBOU (I-T25) TO IGP855-T35-TLCQSTOCKBOU          
             MOVE W-T25-TLQSTOCKDIS (I-T25) TO IGP855-T35-TLCQSTOCKDIS          
             MOVE W-T25-TLQSTOCKRES (I-T25) TO IGP855-T35-TLCQSTOCKRES          
             MOVE W-T25-TLQCDE      (I-T25) TO IGP855-T35-TLCQCDE               
             MOVE W-T25-TLQCDERES   (I-T25) TO IGP855-T35-TLCQCDERES            
             IF SEGTLQ4S NOT = 0                                                
                COMPUTE W-T25-QPOIDSAGR(I-T25) ROUNDED                          
                = (W-T25-TLQ4S(I-T25) * 100) / SEGTLQ4S                         
             ELSE                                                               
                MOVE 0 TO W-T25-QPOIDSAGR(I-T25)                                
             END-IF                                                             
             MOVE W-T25-QPOIDSAGR(I-T25)    TO IGP855-T35-QPOIDSCMARQ           
             MOVE IGP855-T35               TO LIGNE                             
             PERFORM ECRITURE-LIGNE-SUI                                         
           END-PERFORM.                                                         
       FIN-ECRITURE-LIGNE-T35. EXIT.                                            
      *------------------- VENTILATION FAMILLE -----------------------          
      *                                                                         
       REMPLISSAGE-TAB-T45 SECTION.                                             
      *---------------------                                                    
           PERFORM VARYING I-T45 FROM 1 BY 1                                    
                   UNTIL ( I-T45 > NB-T45 )                                     
                   OR    ( W-T45-LMARQ(I-T45)  = FGP849-LMARQ )                 
           END-PERFORM.                                                         
           IF (I-T45 > NB-T45)                                                  
                 ADD 1 TO NB-T45                                                
                 IF NB-T45 > MAX-T45                                            
                    MOVE 'TAB T45 TROP PETIT' TO ABEND-MESS                     
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP849-LMARQ      TO W-T45-LMARQ(NB-T45)                  
                 MOVE NB-T45            TO I-T45                                
           END-IF.                                                              
           IF RUPTURE > 6                                                       
           AND FGP849-WGROUPE NOT = 'C'                                         
           AND FGP849-WGROUPE NOT = 'E'                                         
              ADD 1 TO FSOMNBRECODIC                                            
           END-IF.                                                              
           IF RUPTURE > 5                                                       
           OR (FGP849-NSOCIETE = 'YYY'                                          
           AND FGP849-WTYPENT NOT = '9')                                        
           OR (FGP849-NSOCIETE = 'XXX' AND FGP849-WTYPENT = 'F')                
              ADD W-Q4S              TO W-T45-TFQ4S       (I-T45)               
              ADD FGP849-QS-1        TO W-T45-TFQS-1      (I-T45)               
              ADD FGP849-QS-2        TO W-T45-TFQS-2      (I-T45)               
              ADD FGP849-QS-3        TO W-T45-TFQS-3      (I-T45)               
              ADD FGP849-QS-4        TO W-T45-TFQS-4      (I-T45)               
              ADD FGP849-QSTOCKMAG   TO W-T45-TFQSTOCKMAG (I-T45)               
              ADD FGP849-QSTOCKBOU   TO W-T45-TFQSTOCKBOU (I-T45)               
           END-IF.                                                              
           IF RUPTURE > 2                                                       
              ADD FGP849-QSTOCKDIS   TO W-T45-TFQSTOCKDIS(I-T45)                
              ADD FGP849-QSTOCKRES   TO W-T45-TFQSTOCKRES(I-T45)                
           END-IF.                                                              
           IF RUPTURE > 1                                                       
              ADD FGP849-QCDE        TO W-T45-TFQCDE(I-T45)                     
              ADD FGP849-QCDERES     TO W-T45-TFQCDERES(I-T45)                  
           END-IF.                                                              
       FIN-REMPLISSAGE-TAB-T45. EXIT.                                           
      *                                                                         
       COMPTEUR-FAMILLE SECTION.                                                
      *-------------------------                                                
           IF FGP849-ENTREPOT = FGP849-WENTNAT                                  
              EVALUATE FGP849-WTYPENT                                           
                WHEN ' '   MOVE FGP849-ENTREPOT     TO F-TAMPON                 
                WHEN '9'   MOVE FGP849-ENTREPOT     TO F-TAMPON                 
                WHEN OTHER MOVE FGP849-NFILIALE (1) TO F-TAMPON1                
                  PERFORM VARYING FI-CPT FROM 1 BY 1                            
                    UNTIL FI-CPT > FI-MAX                                       
                    OR FGP849-NFILIALE (FI-CPT) = SPACES OR LOW-VALUE           
                       IF FGP849-NFILIALE (FI-CPT) < F-TAMPON1                  
                          MOVE FGP849-NFILIALE(FI-CPT) TO F-TAMPON1             
                       END-IF                                                   
                  END-PERFORM                                                   
                  MOVE '...'               TO F-TAMPON2                         
              END-EVALUATE                                                      
              PERFORM VARYING F-T45 FROM 1 BY 1                                 
                   UNTIL ( F-T45 > FB-T45 )                                     
                   OR    ( W-T45-FENTREPOT(F-T45) = F-TAMPON )                  
              END-PERFORM                                                       
              IF (F-T45 > FB-T45)                                               
                 ADD 1 TO FB-T45                                                
                 IF FB-T45 > FAX-T45                                            
                    MOVE 'TAB T45 ENTREPOT > FAX-T45' TO ABEND-MESS             
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE F-TAMPON           TO W-T45-FENTREPOT(FB-T45)             
                 MOVE FGP849-TAB-FILIALE TO W-T45-FILIALE  (FB-T45)             
              END-IF                                                            
              PERFORM VENTILE-STATUT                                            
           ELSE                                                                 
              PERFORM VARYING F-T45 FROM 1 BY 1                                 
                   UNTIL ( F-T45 > FB-T45 )                                     
                   OR    ( W-T45-FENTREPOT(F-T45) = FGP849-WENTNAT )            
              END-PERFORM                                                       
              IF (F-T45 > FB-T45)                                               
                 ADD 1 TO FB-T45                                                
                 IF FB-T45 > FAX-T45                                            
                    MOVE 'TAB T45 ENTREPOT > FAX-T45' TO ABEND-MESS             
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP849-WENTNAT     TO W-T45-FENTREPOT(FB-T45)             
                 MOVE FGP849-TAB-FILIALE TO W-T45-FILIALE  (FB-T45)             
              END-IF                                                            
              PERFORM VENTILE-STATUT                                            
           END-IF.                                                              
       FIN-COMPTEUR-FAMILLE. EXIT.                                              
      *                                                                         
       VENTILE-STATUT SECTION.                                                  
      *----------------------                                                   
           EVALUATE FGP849-LSTATCOMP                                            
                WHEN 'EPD'                                                      
                   IF RUPTURE > 5                                               
                   OR (FGP849-NSOCIETE = 'YYY'                                  
                   AND FGP849-WTYPENT NOT = '9')                                
                           IF  FGP849-WGROUPE NOT = 'E'                         
                           AND FGP849-WGROUPE NOT = 'C'                         
                           AND FGP849-QSTOCKDIS > 0                             
                              ADD 1 TO W-T45-NBRE-CODIC (F-T45 , 2)             
                           END-IF                                               
                           ADD FGP849-QS-4                                      
                               TO W-T45-TSTAQS-4      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QS-3                                      
                               TO W-T45-TSTAQS-3      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QS-2                                      
                               TO W-T45-TSTAQS-2      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QS-1                                      
                               TO W-T45-TSTAQS-1      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QSTOCKMAG                                 
                               TO W-T45-TSTAQSTOCKMAG (F-T45 , 2)               
                           ADD FGP849-QSTOCKBOU                                 
                               TO W-T45-TSTAQSTOCKBOU (F-T45 , 2)               
                   END-IF                                                       
                   IF RUPTURE > 2                                               
                           ADD FGP849-QSTOCKDIS                                 
                               TO W-T45-TSTAQSTOCKDIS (F-T45 , 2)               
                           ADD FGP849-QSTOCKRES                                 
                               TO W-T45-TSTAQSTOCKRES (F-T45 , 2)               
                   END-IF                                                       
                WHEN 'EPF'                                                      
                   IF RUPTURE > 5                                               
                   OR (FGP849-NSOCIETE = 'YYY'                                  
                   AND FGP849-WTYPENT NOT = '9')                                
                           IF  FGP849-WGROUPE NOT = 'E'                         
                           AND FGP849-WGROUPE NOT = 'C'                         
                           AND FGP849-QSTOCKDIS > 0                             
                              ADD 1 TO W-T45-NBRE-CODIC (F-T45 , 2)             
                           END-IF                                               
                           ADD FGP849-QS-4                                      
                               TO W-T45-TSTAQS-4      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QS-3                                      
                               TO W-T45-TSTAQS-3      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QS-2                                      
                               TO W-T45-TSTAQS-2      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QS-1                                      
                               TO W-T45-TSTAQS-1      (F-T45 , 2)               
                                  W-T45-TSTAQ4S       (F-T45 , 2)               
                           ADD FGP849-QSTOCKMAG                                 
                               TO W-T45-TSTAQSTOCKMAG (F-T45 , 2)               
                           ADD FGP849-QSTOCKBOU                                 
                               TO W-T45-TSTAQSTOCKBOU (F-T45 , 2)               
                   END-IF                                                       
                   IF RUPTURE > 2                                               
                           ADD FGP849-QSTOCKDIS                                 
                               TO W-T45-TSTAQSTOCKDIS (F-T45 , 2)               
                           ADD FGP849-QSTOCKRES                                 
                               TO W-T45-TSTAQSTOCKRES (F-T45 , 2)               
                   END-IF                                                       
                WHEN 'NG'  CONTINUE                                             
                WHEN 'CQE'                                                      
                   IF RUPTURE > 5                                               
                   OR (FGP849-NSOCIETE = 'YYY'                                  
                   AND FGP849-WTYPENT NOT = '9')                                
                           IF  FGP849-WGROUPE NOT = 'E'                         
                           AND FGP849-WGROUPE NOT = 'C'                         
                           AND FGP849-QSTOCKDIS > 0                             
                              ADD 1 TO W-T45-NBRE-CODIC (F-T45 , 3)             
                           END-IF                                               
                           ADD FGP849-QS-4                                      
                               TO W-T45-TSTAQS-4      (F-T45 , 3)               
                                  W-T45-TSTAQ4S       (F-T45 , 3)               
                           ADD FGP849-QS-3                                      
                               TO W-T45-TSTAQS-3      (F-T45 , 3)               
                                  W-T45-TSTAQ4S       (F-T45 , 3)               
                           ADD FGP849-QS-2                                      
                               TO W-T45-TSTAQS-2      (F-T45 , 3)               
                                  W-T45-TSTAQ4S       (F-T45 , 3)               
                           ADD FGP849-QS-1                                      
                               TO W-T45-TSTAQS-1      (F-T45 , 3)               
                                  W-T45-TSTAQ4S       (F-T45 , 3)               
                           ADD FGP849-QSTOCKMAG                                 
                               TO W-T45-TSTAQSTOCKMAG (F-T45 , 3)               
                           ADD FGP849-QSTOCKBOU                                 
                               TO W-T45-TSTAQSTOCKBOU (F-T45 , 3)               
                   END-IF                                                       
                   IF RUPTURE > 2                                               
                           ADD FGP849-QSTOCKDIS                                 
                               TO W-T45-TSTAQSTOCKDIS (F-T45 , 3)               
                           ADD FGP849-QSTOCKRES                                 
                               TO W-T45-TSTAQSTOCKRES (F-T45 , 3)               
                   END-IF                                                       
                WHEN OTHER                                                      
                 IF CODIC-EDITE                                                 
                   IF RUPTURE > 5                                               
                   OR (FGP849-NSOCIETE = 'YYY'                                  
                   AND FGP849-WTYPENT NOT = '9')                                
                           IF  FGP849-WGROUPE NOT = 'E'                         
                           AND FGP849-WGROUPE NOT = 'C'                         
                           AND FGP849-QSTOCKDIS > 0                             
                              ADD 1 TO W-T45-NBRE-CODIC (F-T45 , 1)             
                           END-IF                                               
                           ADD FGP849-QS-4                                      
                               TO W-T45-TSTAQS-4      (F-T45 , 1)               
                                  W-T45-TSTAQ4S       (F-T45 , 1)               
                           ADD FGP849-QS-3                                      
                               TO W-T45-TSTAQS-3      (F-T45 , 1)               
                                  W-T45-TSTAQ4S       (F-T45 , 1)               
                           ADD FGP849-QS-2                                      
                               TO W-T45-TSTAQS-2      (F-T45 , 1)               
                                  W-T45-TSTAQ4S       (F-T45 , 1)               
                           ADD FGP849-QS-1                                      
                               TO W-T45-TSTAQS-1      (F-T45 , 1)               
                                  W-T45-TSTAQ4S       (F-T45 , 1)               
                           ADD FGP849-QSTOCKMAG                                 
                               TO W-T45-TSTAQSTOCKMAG (F-T45 , 1)               
                           ADD FGP849-QSTOCKBOU                                 
                               TO W-T45-TSTAQSTOCKBOU (F-T45 , 1)               
                   END-IF                                                       
                   IF RUPTURE > 2                                               
                           ADD FGP849-QSTOCKDIS                                 
                               TO W-T45-TSTAQSTOCKDIS (F-T45 , 1)               
                           ADD FGP849-QSTOCKRES                                 
                               TO W-T45-TSTAQSTOCKRES (F-T45 , 1)               
                   END-IF                                                       
                 END-IF                                                         
           END-EVALUATE.                                                        
       FIN-VENTILE-STATUT. EXIT.                                                
      *                                                                         
       ECRITURE-T45 SECTION.                                                    
      *-------------                                                            
           IF NB-T45 > 0                                                        
            MOVE ' '               TO SAUT                                      
            MOVE W-FGP849-LFAM          TO IGP855-T45-LFAM                      
            PERFORM FAMSOMME-T45                                                
            MOVE IGP855-T45           TO LIGNE                                  
            PERFORM ECRITURE-LIGNE-SUI                                          
      *                                                                         
      *     IF W-FGP849-FICHE = 'N'                                             
      *      MOVE IGP855-T46           TO LIGNE                                 
      *      PERFORM ECRITURE-LIGNE-SUI                                         
      *      PERFORM VARYING F-T45 FROM 1 BY 1                                  
      *              UNTIL W-T45-FENTREPOT (F-T45) = SPACES OR LOW-VALUE        
      *       MOVE W-T45-FSOCDEPOT (F-T45) TO IGP855-T47-FSOCDEPOT              
      *       MOVE W-T45-FDEPOT    (F-T45) TO IGP855-T47-FDEPOT                 
      *       MOVE ':'                     TO IGP855-T47-POINT                  
      *       MOVE W-T45-FILIALE1  (F-T45) TO IGP855-T47-FILIALE1               
      *       MOVE W-T45-FILIALE2  (F-T45) TO IGP855-T47-FILIALE2               
      *       MOVE W-T45-FILIALE3  (F-T45) TO IGP855-T47-FILIALE3               
      *       MOVE W-T45-FILIALE4  (F-T45) TO IGP855-T47-FILIALE4               
      *       MOVE W-T45-FILIALE5  (F-T45) TO IGP855-T47-FILIALE5               
      *       MOVE W-T45-FILIALE6  (F-T45) TO IGP855-T47-FILIALE6               
      *       INITIALIZE SOMME-STATUT                                           
      *       PERFORM VARYING I-STA FROM 1 BY 1                                 
      *                UNTIL I-STA > 3                                          
      *        EVALUATE I-STA                                                   
      *          WHEN 1 MOVE 'APP      ' TO IGP855-T47-STAT-LIBELLE             
      *          WHEN 2 MOVE 'EPF - EPD' TO IGP855-T47-STAT-LIBELLE             
      *                 INITIALIZE IGP855-T47-FSOCDEPOT                         
      *                            IGP855-T47-FDEPOT                            
      *                            IGP855-T47-POINT                             
      *                            IGP855-T47-FILIALE1                          
      *                            IGP855-T47-FILIALE2                          
      *                            IGP855-T47-FILIALE3                          
      *                            IGP855-T47-FILIALE4                          
      *                            IGP855-T47-FILIALE5                          
      *                            IGP855-T47-FILIALE6                          
      *          WHEN 3 MOVE 'CQE      ' TO IGP855-T47-STAT-LIBELLE             
      *                 INITIALIZE IGP855-T47-FSOCDEPOT                         
      *                            IGP855-T47-FDEPOT                            
      *                            IGP855-T47-POINT                             
      *                            IGP855-T47-FILIALE1                          
      *                            IGP855-T47-FILIALE2                          
      *                            IGP855-T47-FILIALE3                          
      *                            IGP855-T47-FILIALE4                          
      *                            IGP855-T47-FILIALE5                          
      *                            IGP855-T47-FILIALE6                          
      *        END-EVALUATE                                                     
      *                                                                         
      *        MOVE '4'   TO W-CODE                                             
      *        MOVE W-T45-NBRE-CODIC (F-T45 , I-STA) TO W-NBRE                  
      *        ADD W-T45-NBRE-CODIC (F-T45 , I-STA)                             
      *            TO W-T45-TCSTANBRECODIC                                      
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT4 TO IGP855-T47-TFSNBRECODIC                            
      *                                                                         
      *        MOVE '6S'  TO W-CODE                                             
      *        MOVE W-T45-TSTAQ4S (F-T45 , I-STA) TO W-NBRE                     
      *        ADD  W-T45-TSTAQ4S (F-T45 , I-STA) TO W-T45-TCSTAQ4S             
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T47-TFSQS4                                  
      *        MOVE W-T45-TSTAQS-4 (F-T45 , I-STA) TO W-NBRE                    
      *        ADD  W-T45-TSTAQS-4 (F-T45 , I-STA) TO W-T45-TCSTAQS-4           
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T47-TFSQS-4                                 
      *        MOVE W-T45-TSTAQS-3 (F-T45 , I-STA) TO W-NBRE                    
      *        ADD  W-T45-TSTAQS-3 (F-T45 , I-STA) TO W-T45-TCSTAQS-3           
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T47-TFSQS-3                                 
      *        MOVE W-T45-TSTAQS-2 (F-T45 , I-STA) TO W-NBRE                    
      *        ADD  W-T45-TSTAQS-2 (F-T45 , I-STA) TO W-T45-TCSTAQS-2           
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T47-TFSQS-2                                 
      *        MOVE W-T45-TSTAQS-1 (F-T45 , I-STA) TO W-NBRE                    
      *        ADD  W-T45-TSTAQS-1 (F-T45 , I-STA) TO W-T45-TCSTAQS-1           
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T47-TFSQS-1                                 
      *                                                                         
      *        MOVE '5S'  TO W-CODE                                             
      *        MOVE W-T45-TSTAQSTOCKMAG (F-T45 , I-STA) TO W-NBRE               
      *        ADD  W-T45-TSTAQSTOCKMAG (F-T45 , I-STA)                         
      *             TO W-T45-TCSTAQSTOCKMAG                                     
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT5 TO IGP855-T47-TFSSTOCKMAG                             
      *                                                                         
      *        MOVE '7S'  TO W-CODE                                             
      *        MOVE W-T45-TSTAQSTOCKDIS (F-T45 , I-STA) TO W-NBRE               
      *        ADD  W-T45-TSTAQSTOCKDIS (F-T45 , I-STA)                         
      *             TO W-T45-TCSTAQSTOCKDIS                                     
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT7 TO IGP855-T47-TFSSTOCKDIS                             
      *                                                                         
      *        MOVE '6S'  TO W-CODE                                             
      *        MOVE W-T45-TSTAQSTOCKRES (F-T45 , I-STA) TO W-NBRE               
      *        ADD  W-T45-TSTAQSTOCKRES (F-T45 , I-STA)                         
      *             TO W-T45-TCSTAQSTOCKRES                                     
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T47-TFSSTOCKRES                             
      *                                                                         
      *        MOVE IGP855-T47           TO LIGNE                               
      *        PERFORM ECRITURE-LIGNE-SUI                                       
      *       END-PERFORM                                                       
      *                                                                         
      *       MOVE IGP855-T48           TO LIGNE                                
      *       PERFORM ECRITURE-LIGNE-SUI                                        
      *                                                                         
      *       MOVE 'TOTAL :  '    TO IGP855-T49-STAT-TOTAL                      
      *       MOVE '6S'  TO W-CODE                                              
      *       MOVE W-T45-TCSTAQ4S TO W-NBRE                                     
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT6 TO IGP855-T49-TCFSQS4                                  
      *       MOVE W-T45-TCSTAQS-4 TO W-NBRE                                    
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT6 TO IGP855-T49-TCFSQS-4                                 
      *       MOVE W-T45-TCSTAQS-3 TO W-NBRE                                    
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT6 TO IGP855-T49-TCFSQS-3                                 
      *       MOVE W-T45-TCSTAQS-2 TO W-NBRE                                    
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT6 TO IGP855-T49-TCFSQS-2                                 
      *       MOVE W-T45-TCSTAQS-1 TO W-NBRE                                    
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT6 TO IGP855-T49-TCFSQS-1                                 
      *                                                                         
      *       MOVE '5S'  TO W-CODE                                              
      *       MOVE W-T45-TCSTAQSTOCKMAG TO W-NBRE                               
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT5 TO IGP855-T49-TCFSSTOCKMAG                             
      *                                                                         
      *       MOVE '7S'  TO W-CODE                                              
      *       MOVE W-T45-TCSTAQSTOCKDIS TO W-NBRE                               
      *       PERFORM TEST-CADRAGE                                              
      *       MOVE EDIT7 TO IGP855-T49-TCFSSTOCKDIS                             
      *                                                                         
      *        MOVE '6S'  TO W-CODE                                             
      *        MOVE W-T45-TCSTAQSTOCKRES TO W-NBRE                              
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT6 TO IGP855-T49-TCFSSTOCKRES                            
      *                                                                         
      *        MOVE '4'  TO W-CODE                                              
      *        MOVE W-T45-TCSTANBRECODIC TO W-NBRE                              
      *        PERFORM TEST-CADRAGE                                             
      *        MOVE EDIT4 TO IGP855-T49-TCFSNBRECODIC                           
      *                                                                         
      *        MOVE IGP855-T49           TO LIGNE                               
      *        PERFORM ECRITURE-LIGNE-SUI                                       
      *      END-PERFORM                                                        
      *     END-IF                                                              
            MOVE IGP855-T50           TO LIGNE                                  
            PERFORM ECRITURE-LIGNE-SUI                                          
            PERFORM ECRITURE-LIGNE-T55                                          
            PERFORM BAS-PAGE                                                    
           END-IF.                                                              
           INITIALIZE W-T45 NB-T45 FB-T45 FSOMME.                               
       FIN-ECRITURE-T45. EXIT.                                                  
      *                                                                         
       ECRITURE-LIGNE-T55 SECTION.                                              
      *-------------------                                                      
           MOVE ' ' TO SAUT.                                                    
           PERFORM VARYING I-T45 FROM 1 BY 1                                    
                         UNTIL I-T45 > NB-T45                                   
             MOVE W-T45-LMARQ(I-T45)        TO IGP855-T55-LMARQ                 
             MOVE W-T45-TFQ4S(I-T45)        TO IGP855-T55-TFCQ4S                
             MOVE W-T45-TFQS-1(I-T45)       TO IGP855-T55-TFCQS-1               
             MOVE W-T45-TFQS-2(I-T45)       TO IGP855-T55-TFCQS-2               
             MOVE W-T45-TFQS-3(I-T45)       TO IGP855-T55-TFCQS-3               
             MOVE W-T45-TFQS-4(I-T45)       TO IGP855-T55-TFCQS-4               
             MOVE W-T45-TFQSTOCKMAG(I-T45)  TO IGP855-T55-TFCQSTOCKMAG          
MPSUP *      MOVE W-T45-TFQSTOCKBOU(I-T45)  TO IGP855-T55-TFCQSTOCKBOU          
             MOVE W-T45-TFQSTOCKDIS(I-T45)  TO IGP855-T55-TFCQSTOCKDIS          
             MOVE W-T45-TFQSTOCKRES(I-T45)  TO IGP855-T55-TFCQSTOCKRES          
             MOVE '8S'             TO W-CODE                                    
             COMPUTE W-NBRE                                                     
             = W-T45-TFQSTOCKDIS(I-T45) + W-T45-TFQSTOCKMAG(I-T45)              
             + W-T45-TFQSTOCKRES(I-T45) + W-T45-TFQSTOCKBOU(I-T45)              
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT8            TO IGP855-T55-TFCQSTOCKTOT                   
             MOVE '5' TO W-CODE                                                 
             MOVE W-T45-TFQCDE(I-T45)       TO W-NBRE                           
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT5                     TO IGP855-T55-TFCQCDE               
             MOVE W-T45-TFQCDERES(I-T45)    TO IGP855-T55-TFCQCDERES            
             IF FSOMTFQ4S NOT = 0                                               
                COMPUTE W-T45-QPOIDSFAM(I-T45) ROUNDED                          
                = (W-T45-TFQ4S(I-T45) * 100) / FSOMTFQ4S                        
             ELSE                                                               
                MOVE 0 TO W-T45-QPOIDSFAM(I-T45)                                
             END-IF                                                             
             MOVE W-T45-QPOIDSFAM(I-T45)    TO IGP855-T55-QPOIDSCFAM            
             MOVE IGP855-T55                TO LIGNE                            
             PERFORM ECRITURE-LIGNE-SUI                                         
           END-PERFORM.                                                         
       FIN-ECRITURE-LIGNE-T55. EXIT.                                            
      *                                                                         
       FAMSOMME-T45 SECTION.                                                    
      *-------------                                                            
           PERFORM VARYING I-T45 FROM 1 BY 1                                    
                   UNTIL I-T45 > NB-T45                                         
                   ADD W-T45-TFQ4S       (I-T45) TO FSOMTFQ4S                   
                   ADD W-T45-TFQS-1      (I-T45) TO FSOMTFQS-1                  
                   ADD W-T45-TFQS-2      (I-T45) TO FSOMTFQS-2                  
                   ADD W-T45-TFQS-3      (I-T45) TO FSOMTFQS-3                  
                   ADD W-T45-TFQS-4      (I-T45) TO FSOMTFQS-4                  
                   ADD W-T45-TFQSTOCKMAG (I-T45) TO FSOMTFQSTOCKMAG             
                   ADD W-T45-TFQSTOCKBOU (I-T45) TO FSOMTFQSTOCKBOU             
                   ADD W-T45-TFQSTOCKDIS (I-T45) TO FSOMTFQSTOCKDIS             
                   ADD W-T45-TFQSTOCKRES (I-T45) TO FSOMTFQSTOCKRES             
                   ADD W-T45-TFQCDE      (I-T45) TO FSOMTFQCDE                  
                   ADD W-T45-TFQCDERES   (I-T45) TO FSOMTFQCDERES               
           END-PERFORM.                                                         
      *    MOVE '4'           TO W-CODE.                                        
      *    MOVE FSOMNBRECODIC TO W-NBRE.                                        
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT4         TO IGP855-T45-TFNBRECODIC.                        
           MOVE '6S'        TO W-CODE.                                          
           MOVE FSOMTFQ4S   TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T45-TFQ4S.                                
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE FSOMTFQS-1  TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T45-TFQS-1.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE FSOMTFQS-2  TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T45-TFQS-2.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE FSOMTFQS-3  TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T45-TFQS-3.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE FSOMTFQS-4  TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T45-TFQS-4.                               
      *                                                                         
           MOVE '5S'             TO W-CODE.                                     
           MOVE FSOMTFQSTOCKMAG  TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T45-TFQSTOCKMAG.                     
      *                                                                         
MPSUP *    MOVE '5S'             TO W-CODE.                                     
MPSUP *    MOVE FSOMTFQSTOCKBOU  TO W-NBRE.                                     
MPSUP *    PERFORM TEST-CADRAGE.                                                
MPSUP *    MOVE EDIT5            TO IGP855-T45-TFQSTOCKBOU.                     
      *                                                                         
           MOVE '7S'             TO W-CODE.                                     
           MOVE FSOMTFQSTOCKDIS  TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7            TO IGP855-T45-TFQSTOCKDIS.                     
      *                                                                         
           MOVE '6S'             TO W-CODE.                                     
           MOVE FSOMTFQSTOCKRES  TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6            TO IGP855-T45-TFQSTOCKRES.                     
      *                                                                         
           MOVE '8S'             TO W-CODE.                                     
           COMPUTE W-NBRE                                                       
           = FSOMTFQSTOCKRES + FSOMTFQSTOCKDIS + FSOMTFQSTOCKBOU                
           + FSOMTFQSTOCKMAG .                                                  
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8            TO IGP855-T45-TFQSTOCKTOT.                     
      *                                                                         
           MOVE '5'              TO W-CODE.                                     
           MOVE FSOMTFQCDE       TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T45-TFQCDE.                          
      *                                                                         
           MOVE '5'              TO W-CODE.                                     
           MOVE FSOMTFQCDERES    TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T45-TFQCDERES.                       
       FIN-FAMSOMME-T45. EXIT.                                                  
      *                                                                         
      *------------------ VENTILATION CHEF PR ------------------------          
      *                                                                         
       REMPLISSAGE-TAB-T65 SECTION.                                             
      *---------------------                                                    
           PERFORM VARYING I-T65 FROM 1 BY 1                                    
                   UNTIL ( I-T65 > NB-T65 )                                     
                   OR    ( W-T65-LMARQ(I-T65)  = FGP849-LMARQ )                 
           END-PERFORM.                                                         
           IF (I-T65 > NB-T65)                                                  
                 ADD 1 TO NB-T65                                                
                 IF NB-T65 > MAX-T65                                            
                    MOVE 'TAB TROP PETIT' TO ABEND-MESS                         
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP849-LMARQ      TO W-T65-LMARQ(NB-T65)                  
                 MOVE NB-T65            TO I-T65                                
           END-IF.                                                              
           IF RUPTURE > 5                                                       
           OR (FGP849-NSOCIETE = 'YYY'                                          
           AND FGP849-WTYPENT NOT = '9')                                        
           OR (FGP849-NSOCIETE = 'XXX' AND FGP849-WTYPENT = 'F')                
              ADD W-Q4S              TO W-T65-TPQ4S       (I-T65)               
              ADD FGP849-QS-1        TO W-T65-TPQS-1      (I-T65)               
              ADD FGP849-QS-2        TO W-T65-TPQS-2      (I-T65)               
              ADD FGP849-QS-3        TO W-T65-TPQS-3      (I-T65)               
              ADD FGP849-QS-4        TO W-T65-TPQS-4      (I-T65)               
              ADD FGP849-QSTOCKMAG   TO W-T65-TPQSTOCKMAG (I-T65)               
              ADD FGP849-QSTOCKBOU   TO W-T65-TPQSTOCKBOU (I-T65)               
           END-IF.                                                              
           IF RUPTURE > 2                                                       
              ADD FGP849-QSTOCKDIS   TO W-T65-TPQSTOCKDIS(I-T65)                
              ADD FGP849-QSTOCKRES   TO W-T65-TPQSTOCKRES(I-T65)                
           END-IF.                                                              
           IF RUPTURE > 1                                                       
              ADD FGP849-QCDE        TO W-T65-TPQCDE(I-T65)                     
              ADD FGP849-QCDERES     TO W-T65-TPQCDERES(I-T65)                  
           END-IF.                                                              
       FIN-REMPLISSAGE-TAB-T65. EXIT.                                           
      *                                                                         
       ECRITURE-T65 SECTION.                                                    
           IF NB-T65 > 0                                                        
             MOVE ' '               TO SAUT                                     
             MOVE W-FGP849-LCHEFPROD     TO IGP855-T65-LCHEFPROD                
             PERFORM CHEFPSOMME-T65                                             
             MOVE IGP855-T65           TO LIGNE                                 
             PERFORM ECRITURE-LIGNE-SUI                                         
             MOVE IGP855-T70           TO LIGNE                                 
             PERFORM ECRITURE-LIGNE-SUI                                         
             PERFORM ECRITURE-LIGNE-T75                                         
             INITIALIZE W-T65 NB-T65 CHEFPSOMME                                 
           END-IF.                                                              
       FIN-ECRITURE-T65. EXIT.                                                  
      *                                                                         
       CHEFPSOMME-T65 SECTION.                                                  
           PERFORM VARYING I-T65 FROM 1 BY 1                                    
                   UNTIL I-T65 > NB-T65                                         
                   ADD W-T65-TPQ4S       (I-T65) TO CHEFPTPQ4S                  
                   ADD W-T65-TPQS-1      (I-T65) TO CHEFPTPQS-1                 
                   ADD W-T65-TPQS-2      (I-T65) TO CHEFPTPQS-2                 
                   ADD W-T65-TPQS-3      (I-T65) TO CHEFPTPQS-3                 
                   ADD W-T65-TPQS-4      (I-T65) TO CHEFPTPQS-4                 
                   ADD W-T65-TPQSTOCKMAG (I-T65) TO CHEFPTPQSTOCKMAG            
                   ADD W-T65-TPQSTOCKBOU (I-T65) TO CHEFPTPQSTOCKBOU            
                   ADD W-T65-TPQSTOCKDIS (I-T65) TO CHEFPTPQSTOCKDIS            
                   ADD W-T65-TPQSTOCKRES (I-T65) TO CHEFPTPQSTOCKRES            
                   ADD W-T65-TPQCDE      (I-T65) TO CHEFPTPQCDE                 
                   ADD W-T65-TPQCDERES   (I-T65) TO CHEFPTPQCDERES              
           END-PERFORM.                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE CHEFPTPQ4S TO W-NBRE.                                           
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T65-TPQ4S.                                
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE CHEFPTPQS-1 TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T65-TPQS-1.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE CHEFPTPQS-2 TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T65-TPQS-2.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE CHEFPTPQS-3 TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T65-TPQS-3.                               
      *                                                                         
           MOVE '6S'        TO W-CODE.                                          
           MOVE CHEFPTPQS-4 TO W-NBRE.                                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6       TO IGP855-T65-TPQS-4.                               
      *                                                                         
           MOVE '5S'             TO W-CODE.                                     
           MOVE CHEFPTPQSTOCKMAG TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T65-TPQSTOCKMAG.                     
      *                                                                         
MPSUP *    MOVE '5S'             TO W-CODE.                                     
MPSUP *    MOVE CHEFPTPQSTOCKBOU TO W-NBRE.                                     
MPSUP *    PERFORM TEST-CADRAGE.                                                
MPSUP *    MOVE EDIT5            TO IGP855-T65-TPQSTOCKBOU.                     
      *                                                                         
           MOVE '7S'             TO W-CODE.                                     
           MOVE CHEFPTPQSTOCKDIS TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7            TO IGP855-T65-TPQSTOCKDIS.                     
      *                                                                         
           MOVE '6S'             TO W-CODE.                                     
           MOVE CHEFPTPQSTOCKRES TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT6            TO IGP855-T65-TPQSTOCKRES.                     
      *                                                                         
           MOVE '5'              TO W-CODE.                                     
           MOVE CHEFPTPQCDE      TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T65-TPQCDE.                          
      *                                                                         
           MOVE '5'              TO W-CODE.                                     
           MOVE CHEFPTPQCDERES   TO W-NBRE.                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5            TO IGP855-T65-TPQCDERES.                       
       FIN-CHEFPSOMME-T65. EXIT.                                                
      *                                                                         
       ECRITURE-LIGNE-T75 SECTION.                                              
           MOVE ' ' TO SAUT.                                                    
           PERFORM VARYING I-T65 FROM 1 BY 1                                    
                         UNTIL I-T65 > NB-T65                                   
             MOVE W-T65-LMARQ       (I-T65) TO IGP855-T75-LMARQ                 
             MOVE W-T65-TPQ4S       (I-T65) TO IGP855-T75-TPOCQ4S               
             MOVE W-T65-TPQS-1      (I-T65) TO IGP855-T75-TPCQS-1               
             MOVE W-T65-TPQS-2      (I-T65) TO IGP855-T75-TPCQS-2               
             MOVE W-T65-TPQS-3      (I-T65) TO IGP855-T75-TPCQS-3               
             MOVE W-T65-TPQS-4      (I-T65) TO IGP855-T75-TPCQS-4               
             MOVE '5S' TO W-CODE                                                
             MOVE W-T65-TPQSTOCKMAG (I-T65) TO W-NBRE                           
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT5                     TO IGP855-T75-TPCQSTOCKMAG          
MPSUP *      MOVE W-T65-TPQSTOCKBOU (I-T65) TO W-NBRE                           
MPSUP *      PERFORM TEST-CADRAGE                                               
MPSUP *      MOVE EDIT5                     TO IGP855-T75-TPCQSTOCKBOU          
             MOVE W-T65-TPQSTOCKDIS (I-T65) TO W-NBRE                           
             MOVE '7S'                      TO W-CODE                           
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT7                     TO IGP855-T75-TPCQSTOCKDIS          
             MOVE W-T65-TPQSTOCKRES (I-T65) TO W-NBRE                           
             MOVE '6'                       TO W-CODE                           
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT6                     TO IGP855-T75-TPCQSTOCKRES          
             MOVE '5' TO W-CODE                                                 
             MOVE W-T65-TPQCDE      (I-T65) TO W-NBRE                           
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT5                     TO IGP855-T75-TPCQCDE               
             MOVE '5' TO W-CODE                                                 
             MOVE W-T65-TPQCDERES   (I-T65) TO W-NBRE                           
             PERFORM TEST-CADRAGE                                               
             MOVE EDIT5                     TO IGP855-T75-TPCQCDERES            
             MOVE IGP855-T75                TO LIGNE                            
             PERFORM ECRITURE-LIGNE-SUI                                         
           END-PERFORM.                                                         
           PERFORM BAS-PAGE.                                                    
       FIN-ECRITURE-LIGNE-T75. EXIT.                                            
      *                                                                         
       ECRITURE-LIGNE-SUI SECTION.                                              
      *-------------------                                                      
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
           IF CTR-LIGNE > MAX-LIGNE                                             
              PERFORM ECRITURE-ENTETE-PRE                                       
           END-IF.                                                              
       FIN-ECRITURE-LIGNE-SUI. EXIT.                                            
      *                                                                         
       ECRITURE-1-LIGNE SECTION.                                                
      *-----------------                                                        
           WRITE LIGNE-IGP855.                                                  
           ADD 1 TO CTR-LIGNE.                                                  
           IF CTR-LIGNE > MAX-LIGNE                                             
              PERFORM ECRITURE-ENTETE                                           
           END-IF.                                                              
       FIN-ECRITURE-1-LIGNE. EXIT.                                              
      *---------------------------------------------------------------*         
      *         P A R A G R A P H E   F I N  A N O R M A L E          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FIN-ANORMALE SECTION.                                                    
      *-------------                                                            
           MOVE 'BGP855' TO ABEND-PROG.                                         
           CALL 'ABEND' USING                                                   
                        ABEND-PROG                                              
                        ABEND-MESS.                                             
      *                                                                         
       F-FIN-ANORMALE.                                                          
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
