      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.            BGS106.                                           
       AUTHOR.                FLORENCE.                                         
      *                                                                         
      * ============================================================= *         
      *                                                               *         
      *       PROGRAMME     :  BGS106.                                *         
      *       FONCTION      :  EDITION STOCKS DES ARTICLES VENDUS EN  *         
      *                        MAGASIN DONT LA VENTE A FAIT JUSTE     *         
      *                        L'OBJET D'UN PASSAGE EN CAISSE  E.L.A  *         
      *                                                               *         
      *       FICHIERS      :  FEL032                                 *         
      *                     :  IFEL031                                *         
      *                                                               *         
      *       DATE CREATION :  03/07/1991.                            *         
      *                                                               *         
      * ============================================================= *         
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
      *                                                                         
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FEL032        ASSIGN  TO  FEL032.                           
      *--                                                                       
            SELECT  FEL032        ASSIGN  TO  FEL032                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  IFEL031       ASSIGN  TO  IFEL031.                          
      *--                                                                       
            SELECT  IFEL031       ASSIGN  TO  IFEL031                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      * ============================================================= *         
      *  D E F I N I T I O N    F I C H I E R                         *         
      *                     F E L 0 3 2                               *         
      * ============================================================= *         
      *                                                                         
       FD  FEL032                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FEL032-ENREG.                                                        
           02 FEL032-NSOC  PIC X(03).                                           
           02 FEL032-NLIEU PIC X(03).                                           
           02 FEL032-CODIC PIC X(07).                                           
           02 FEL032-CFAM  PIC X(05).                                           
           02 FEL032-WSEQFAM  PIC S9(05) COMP-3.                                
           02 FEL032-CMARQ PIC X(05).                                           
           02 FEL032-LREFFOURN PIC X(20).                                       
           02 FEL032-QTE   PIC S9(5) COMP-3.                                    
           02 FEL032-TYPEL PIC X(07).                                           
           02 FEL032-MESS  PIC X(52).                                           
           02 FILLER       PIC X(02).                                           
      *                                                                         
      * ============================================================= *         
      *  D E F I N I T I O N    F I C H I E R   I M P R E S S I O N   *         
      *                   I F E L 0 3 1                               *         
      * ============================================================= *         
      *                                                                         
       FD  IFEL031                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-IFEL031.                                                       
           03  F-SAUT    PIC X(01).                                             
           03  F-LIGNE   PIC  X(132).                                           
      *                                                                         
      * ============================================================= *         
      *                 W O R K I N G  -  S T O R A G E               *         
      * ============================================================= *         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  I                       PIC 9(02)  COMP VALUE 0.                     
      *--                                                                       
       77  I                       PIC 9(02) COMP-5 VALUE 0.                    
      *}                                                                        
       77  W-PAGE                  PIC 9(03)  VALUE 0.                          
       77  CTR-FEL032-LUS          PIC S9(07) COMP-3.                           
       77  ZZZ                     PIC ZZZZZZ9.                                 
       01  ETAT-FICHIER            PIC  X(01)  VALUE  '0'.                      
           88  END-OF-FILE         VALUE  '1'.                                  
       01  Z-QTE  PIC ZZZZ9.                                                    
      *                                                                         
      *                                                                         
      *-----------------------------------------------------------------        
      *               DESCRIPTION DES LIGNES D'EDITION                          
      *-----------------------------------------------------------------        
      *                                                                         
       01  E1.                                                                  
           02 FILLER PIC X(50)  VALUE SPACES.                                   
           02 FILLER PIC X(20)  VALUE 'ETABLISSEMENTS DARTY'.                   
           02 FILLER PIC X(48)  VALUE SPACES.                                   
           02 FILLER PIC X(07)  VALUE 'PAGE : '.                                
           02 L-PAGE PIC ZZ9.                                                   
           02 FILLER PIC X(04)  VALUE SPACES.                                   
       01  E2.                                                                  
           02 FILLER PIC X(44)  VALUE SPACES.                                   
           02 FILLER PIC X(31)  VALUE 'LISTE DES ANOMALIES RECONTREES '.        
           02 FILLER PIC X(57)  VALUE SPACES.                                   
       01  E3.                                                                  
           02 FILLER PIC X(132)  VALUE ALL '-'.                                 
       01  E4.                                                                  
           02 FILLER PIC X(01)   VALUE '!'.                                     
           02 FILLER PIC X(09)   VALUE ' MAGASIN '.                             
           02 FILLER PIC X(01)   VALUE '!'.                                     
           02 FILLER PIC X(13)   VALUE ' FAM.  MARQ. '.                         
           02 FILLER PIC X(01)   VALUE '!'.                                     
           02 FILLER PIC X(19)   VALUE '  CODIC   REFERENCE'.                   
           02 FILLER PIC X(12)   VALUE SPACES.                                  
           02 FILLER PIC X(01)   VALUE '!'.                                     
           02 FILLER PIC X(08)   VALUE ' QTE   !'.                              
           02 FILLER PIC X(12)   VALUE ' TYPE ENREG '.                          
           02 FILLER PIC X(01)   VALUE '!'.                                     
           02 FILLER PIC X(15)   VALUE '    MESSAGE    '.                       
           02 FILLER PIC X(38)   VALUE SPACES.                                  
           02 FILLER PIC X(01)   VALUE '!'.                                     
       01  L1.                                                                  
           02 FILLER PIC X(01)   VALUE '!'.                                     
           02 FILLER PIC X(01)   VALUE ' '.                                     
           02 L-NSOC PIC X(03).                                                 
           02 FILLER PIC X(01)   VALUE ' '.                                     
           02 L-NLIEU PIC X(03).                                                
           02 FILLER PIC X(03)   VALUE ' ! '.                                   
           02 L-CFAM  PIC X(05).                                                
           02 FILLER PIC X(01)   VALUE ' '.                                     
           02 L-CMARQ PIC X(05).                                                
           02 FILLER PIC X(03)   VALUE ' ! '.                                   
           02 L-CODIC PIC X(07).                                                
           02 FILLER PIC X(02)   VALUE SPACES.                                  
           02 L-LREFFOURN  PIC X(20).                                           
           02 FILLER PIC X(03)   VALUE ' ! '.                                   
           02 L-QTE PIC X(05).                                                  
           02 FILLER PIC X(05)   VALUE ' !   '.                                 
           02 L-TYPEL PIC X(07).                                                
           02 FILLER PIC X(05)   VALUE '  ! '.                                  
           02 L-MESS PIC X(51).                                                 
           02 FILLER PIC X(1) VALUE '!'.                                        
      *                                                                         
      * ============================================================= *         
      *        D E S C R I P T I O N      D E S      Z O N E S        *         
      *           D ' A P P E L    M O D U L E    A B E N D           *         
      * ============================================================= *         
      *                                                                         
           COPY  ABENDCOP.                                                      
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      * ============================================================= *         
      *              T R A M E   DU   P R O G R A M M E               *         
      * ============================================================= *         
      *                                                               *         
      * ============================================================= *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BGS106 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * ============================================================= *         
      *                                                                         
       MODULE-BGS106.                                                           
      *                                                                         
           PERFORM  DEBUT-BGS106.                                               
           PERFORM  TRAIT-BGS106   UNTIL   END-OF-FILE.                         
           PERFORM  FIN-BGS106.                                                 
      *                                                                         
      * ============================================================= *         
      *                  D E B U T        B G S 1 0 6                 *         
      * ============================================================= *         
      *                                                                         
       DEBUT-BGS106              SECTION.                                       
      *                                                                         
           OPEN  INPUT   FEL032                                                 
                 OUTPUT  IFEL031.                                               
           MOVE 0  TO CTR-FEL032-LUS.                                           
           PERFORM LECTURE-FEL032.                                              
      *                                                                         
       FIN-DEBUT-BGS106.  EXIT.                                                 
      *                                                                         
      * ============================================================= *         
      *                    T R A I T        B G S 1 0 6               *         
      * ============================================================= *         
      *                                                                         
       TRAIT-BGS106          SECTION.                                           
      *                                                                         
           IF I = 55                                                            
             WRITE LIGNE-IFEL031 FROM E3                                        
             MOVE 0 TO I                                                        
           END-IF                                                               
      *                                                                         
           ADD 1 TO I                                                           
      *                                                                         
           IF I = 1                                                             
             ADD 1 TO W-PAGE                                                    
             MOVE W-PAGE TO L-PAGE                                              
             WRITE LIGNE-IFEL031 FROM E1 AFTER ADVANCING PAGE                   
             WRITE LIGNE-IFEL031 FROM E2                                        
             MOVE '2' TO F-SAUT                                                 
             WRITE LIGNE-IFEL031 FROM E3                                        
             WRITE LIGNE-IFEL031 FROM E4                                        
             WRITE LIGNE-IFEL031 FROM E3                                        
             MOVE '2' TO F-SAUT                                                 
             ADD 9 TO I                                                         
           END-IF                                                               
           MOVE '1' TO F-SAUT                                                   
           MOVE FEL032-NSOC   TO  L-NSOC                                        
           MOVE FEL032-NLIEU  TO  L-NLIEU                                       
           MOVE FEL032-CODIC TO  L-CODIC                                        
           MOVE FEL032-CFAM   TO  L-CFAM                                        
           MOVE FEL032-CMARQ  TO  L-CMARQ                                       
           MOVE FEL032-LREFFOURN TO  L-LREFFOURN                                
           MOVE FEL032-QTE    TO  Z-QTE                                         
           MOVE Z-QTE         TO  L-QTE                                         
           MOVE FEL032-TYPEL  TO  L-TYPEL                                       
           MOVE FEL032-MESS   TO  L-MESS                                        
           WRITE LIGNE-IFEL031 FROM L1.                                         
      *                                                                         
           PERFORM LECTURE-FEL032.                                              
      *                                                                         
       FIN-TRAIT1-BGS106. EXIT.                                                 
      *                                                                         
      * ============================================================= *         
      *                      F I N        B G S 1 0 6                 *         
      * ============================================================= *         
      *                                                                         
       FIN-BGS106            SECTION.                                           
           WRITE LIGNE-IFEL031 FROM E3.                                         
           PERFORM  COMPTE-RENDU.                                               
           PERFORM  FERMETURE-FICHIERS.                                         
           PERFORM  FIN-PROGRAMME.                                              
       FIN-FIN-BGS106.  EXIT.                                                   
      *                                                                         
      * ============================================================= *         
      *       L E C T U R E    D E    F E L 0 3 2                     *         
      * ============================================================= *         
      *                                                                         
       LECTURE-FEL032           SECTION.                                        
           READ FEL032 AT END SET END-OF-FILE TO TRUE.                          
           IF END-OF-FILE                                                       
              NEXT SENTENCE                                                     
           ELSE                                                                 
              ADD 1 TO CTR-FEL032-LUS                                           
           END-IF.                                                              
       END-LECTURE-FEL032.         EXIT.                                        
      *                                                                         
      * ============================================================= *         
      *          M O D U L E S      F I N      B G S 1 0 6            *         
      *                                                                         
      * ============================================================= *         
      *--------------------------------------------------------------           
      *                     F I N  -  A N O R M A L E                           
      *--------------------------------------------------------------           
      *                                                                         
       FIN-ANORMALE                 SECTION.                                    
      *                                                                         
           PERFORM  FERMETURE-FICHIERS.                                         
           MOVE  'BGS106'                  TO  ABEND-PROG.                      
      *                                                                         
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
      *                                                                         
       FIN-FIN-ANORMALE.   EXIT.                                                
      *                                                                         
      *--------------------------------------------------------------           
      *                  C O M P T E  -  R E N D U                              
      *--------------------------------------------------------------           
      *                                                                         
       COMPTE-RENDU            SECTION.                                         
      *                                                                         
           DISPLAY  '***        +-----------------+     '.                      
           DISPLAY  '***        !   B G S 1 0 6   !     '.                      
           DISPLAY  '***        +-----------------+     '.                      
           MOVE     CTR-FEL032-LUS TO ZZZ.                                      
           DISPLAY  '***  NOMBRE ENREG FEL031     LUS       : '                 
                    ZZZ.                                                        
       FIN-COMPTE-RENDU.  EXIT.                                                 
      *                                                                         
      *--------------------------------------------------------------           
      *           F E R M E T U R E    F I C H I E R S                          
      *--------------------------------------------------------------           
      *                                                                         
       FERMETURE-FICHIERS           SECTION.                                    
      *                                                                         
           CLOSE   FEL032 IFEL031.                                              
      *                                                                         
       F-FERMETURE-FICHIERS.  EXIT.                                             
      *                                                                         
      *--------------------------------------------------------------           
      *                 F I N    P R O G R A M M E                              
      *--------------------------------------------------------------           
      *                                                                         
       FIN-PROGRAMME        SECTION.                                            
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                           
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       F-FIN-PROGRAMME.  EXIT.                                                  
