      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.       BGQ101.                                                
       DATE-COMPILED.                                                           
       AUTHOR.           DSA065.                                                
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
      *                                                                         
      *                    ECRITURE DU FICHIER DU                               
      *              SUIVI DES ACTIVITES PF DE LIVRAISON                        
      *                                                                         
      * FGQ100 => FICHIER TRIE PROVENANT DU BGQ100                              
      *                                                                         
      * LE 29/11/2010                                                           
      * DSA065 - C. LAVAURE                                                     
      *                                                                         
      *-----------------------------------------------------------------        
      *-----------------------------------------------------------------        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGQ100   ASSIGN TO FGQ100   FILE STATUS IS ST-FGQ100.         
      *--                                                                       
           SELECT FGQ100   ASSIGN TO FGQ100   FILE STATUS IS ST-FGQ100          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGQ101   ASSIGN TO FGQ101   FILE STATUS IS ST-FGQ101.         
      *--                                                                       
           SELECT FGQ101   ASSIGN TO FGQ101   FILE STATUS IS ST-FGQ101          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FGQ100                                                               
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD                                                
           DATA RECORD FGQ100-ENR.                                              
       01 FGQ100-ENR                 PIC X(250).                                
       FD  FGQ101                                                               
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD                                                
           DATA RECORD FGQ101-ENR.                                              
       01 FGQ101-ENR                 PIC X(250).                                
       WORKING-STORAGE SECTION.                                                 
      *- ZONE DE TRAVAIL                                                        
       77 WS-WHEN-COMPILED           PIC X(16)        VALUE SPACE.              
       77 WS-NMODULE                 PIC X(08)        VALUE SPACE.              
       77 WZ-EDITION                 PIC Z(06)9       VALUE ZERO.               
       77 WP-NBR-LECT-FGQ100         PIC S9(4) COMP-3 VALUE ZERO.               
       77 WP-NBR-ECR-FGQ101          PIC S9(4) COMP-3 VALUE ZERO.               
       01 WS-DATE-COMPILE.                                                      
          05 WS-COMPILE-JJ           PIC X(02).                                 
          05 FILLER                  PIC X     VALUE '/'.                       
          05 WS-COMPILE-MM           PIC X(02).                                 
          05 FILLER                  PIC X(03) VALUE '/20'.                     
          05 WS-COMPILE-AA           PIC X(02).                                 
          05 FILLER                  PIC X(02).                                 
          05 WS-COMPILE-HE           PIC X(02).                                 
          05 FILLER                  PIC X(01) VALUE ':'.                       
          05 WS-COMPILE-MI           PIC X(02).                                 
          05 FILLER                  PIC X(01) VALUE ':'.                       
          05 WS-COMPILE-SE           PIC X(02).                                 
      *                                                                         
       01 WF-FGQ100                  PIC X     VALUE '0'.                       
          88 WC-FGQ100-SUITE                   VALUE '0'.                       
          88 WC-FGQ100-FIN                     VALUE '1'.                       
      *--  LIGNES DU FICHIER                                                    
       77 ST-FGQ100                  PIC 99    VALUE ZERO.                      
       77 ST-FGQ101                  PIC 99    VALUE ZERO.                      
       77 WS-ENR-FGQ101              PIC X(250).                                
       01 WS-LIGNE-ENTETE.                                                      
          05 FILLER                  PIC X(10) VALUE 'PF;LIBELLE'.              
          05 FILLER                  PIC X     VALUE ';'.               00790003
          05 FILLER                  PIC X(17) VALUE 'PERIMETRE/PROFIL'.        
          05 FILLER                  PIC X(15) VALUE 'TOURNEE;LIBELLE'.         
          05 FILLER                  PIC X     VALUE ';'.               00790003
          05 FILLER                  PIC X(16) VALUE 'ZONE ELEMENTAIRE'.        
          05 FILLER                  PIC X     VALUE ';'.               00790003
          05 FILLER                  PIC X(07) VALUE 'LIBELLE'.                 
          05 FILLER                  PIC X     VALUE ';'.               00790003
          05 FILLER                  PIC X(10) VALUE 'CP;COMMUNE'.              
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(06) VALUE 'CINSEE'.          00790003
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(14) VALUE 'NB LIVRAISONS '.          
          05 FILLER                  PIC X(09) VALUE 'CLASSIQUE'.               
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(13) VALUE 'CA LIVRAISON'.            
          05 FILLER                  PIC X(09) VALUE 'CLASSIQUE'.               
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(14) VALUE 'NB LIVRAISONS '.          
          05 FILLER                  PIC X(06) VALUE 'B TO B' .                 
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(13) VALUE 'CA LIVRAISON'.            
          05 FILLER                  PIC X(06) VALUE 'B TO B' .                 
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(14) VALUE 'NB OPERATIONS '.          
          05 FILLER                  PIC X(03) VALUE 'SAV'.                     
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(14) VALUE 'NB OPERATIONS '.          
          05 FILLER                  PIC X(08) VALUE 'DIVERSES'.                
          05 FILLER                  PIC X     VALUE ';'.                       
          05 FILLER                  PIC X(34) VALUE SPACE.                     
      *                                                                         
      *- COPIES                                                                 
           COPY  WORKDATC.                                                      
           COPY  SYKWSQ10.                                                      
           COPY  SYBWDIV0.                                                      
      *                                                                         
       LINKAGE SECTION.                                                         
      *                                                                         
      *=================================================================        
      *=================================================================        
       PROCEDURE DIVISION.                                                      
      *=================================================================        
      *=================================================================        
       MODULE-BGQ101 SECTION.                                                   
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
       FIN-MODULE-BGQ101. EXIT.                                                 
      *                                                                         
      *=================================================================        
       MODULE-ENTREE SECTION.                                                   
           DISPLAY  '*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*'                
           DISPLAY  '             BGQ101                       '                
           DISPLAY  '                                          '                
           MOVE WHEN-COMPILED            TO WS-WHEN-COMPILED                    
           MOVE WS-WHEN-COMPILED(4:2)    TO WS-COMPILE-JJ                       
           MOVE WS-WHEN-COMPILED(7:2)    TO WS-COMPILE-AA                       
           MOVE WS-WHEN-COMPILED(1:2)    TO WS-COMPILE-MM                       
           MOVE WS-WHEN-COMPILED(9:2)    TO WS-COMPILE-HE                       
           MOVE WS-WHEN-COMPILED(12:2)   TO WS-COMPILE-MI                       
           MOVE WS-WHEN-COMPILED(15:2)   TO WS-COMPILE-SE                       
           DISPLAY 'COMPILE DU ' WS-DATE-COMPILE                                
           DISPLAY  '                                          '                
           OPEN INPUT  FGQ100.                                                  
           OPEN OUTPUT FGQ101.                                                  
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
      *=================================================================        
       MODULE-TRAITEMENT  SECTION.                                              
           MOVE WS-LIGNE-ENTETE TO WS-ENR-FGQ101                                
           PERFORM ECRITURE-FGQ101                                              
           PERFORM LECTURE-FGQ100                                               
           PERFORM UNTIL WC-FGQ100-FIN                                          
              MOVE FGQ101-ENR   TO WS-ENR-FGQ101                                
              PERFORM ECRITURE-FGQ101                                           
              PERFORM LECTURE-FGQ100                                            
           END-PERFORM.                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
      *-------------------------------------------------------------            
       LECTURE-FGQ100 SECTION.                                                  
           READ FGQ100              INTO FGQ101-ENR                             
             AT END SET WC-FGQ100-FIN TO TRUE                                   
           END-READ.                                                            
           IF WC-FGQ100-SUITE                                                   
              ADD 1                   TO WP-NBR-LECT-FGQ100                     
           END-IF.                                                              
       FIN-LECT-FGQ100. EXIT.                                                   
      *                                                                         
      *-----------------------------------------------------------------        
       ECRITURE-FGQ101 SECTION.                                                 
           WRITE FGQ101-ENR FROM WS-ENR-FGQ101                                  
           ADD 1 TO WP-NBR-ECR-FGQ101 .                                         
       FIN-ECRITURE-FGQ101. EXIT.                                               
      *                                                                         
      *=================================================================        
       MODULE-SORTIE  SECTION.                                                  
           MOVE WP-NBR-LECT-FGQ100 TO WZ-EDITION.                               
           DISPLAY '                    '                                       
           DISPLAY '                    '                                       
           DISPLAY 'NB LIGNES LUES    :     ' WZ-EDITION                        
           MOVE WP-NBR-ECR-FGQ101  TO WZ-EDITION.                               
           DISPLAY 'NB LIGNES ECRITES :     ' WZ-EDITION                        
           DISPLAY '                                           '.               
           DISPLAY '          FIN NORMALE BGQ101               '.               
           DISPLAY '*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*'                 
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
