      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BGP806                                                    
       AUTHOR. DSA017.                                                          
      *------------------------------------------------------------*            
      *                PROGRAMME D'EDITION DE L'ETAT               *            
      *                DES STOCKS ET COMMANDES SUR ENTREPOT        *            
      *                DE DEBORDEMENT                              *            
      * ---------------------------------------------------------- *            
      * DATE CREATION : 21/04/2008                                 *            
      * ---------------------------------------------------------- *            
      *                                                            *            
      * ---------------------------------------------------------- *            
      * DATE MODIF :                                               *            
      *                                                            *            
      *------------------------------------------------------------*            
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGP805  ASSIGN TO FGP805.                                     
      *--                                                                       
           SELECT FGP805  ASSIGN TO FGP805                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IGP805 ASSIGN TO IGP805.                                      
      *--                                                                       
           SELECT IGP805 ASSIGN TO IGP805                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F D A T E            *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENREG.                                                 00000030
           02 FDATE-JJ              PIC X(02).                                  
           02 FDATE-MM              PIC X(02).                                  
           02 FDATE-SS              PIC X(02).                                  
           02 FDATE-AA              PIC X(02).                                  
           02 FILLER                PIC X(72).                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F G P 8 0 5          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FGP805                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENREG-FGP805 PIC X(150).                                             
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    I G P 8 0 5          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  IGP805                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENREG-IGP805 PIC X(133).                                             
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
       77  W-ZONE    PIC XX.                                                    
      *                                                                         
        COPY ABENDCOP.                                                          
      *                                                                         
        COPY WORKDATC.                                                          
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
       01  LIGNE-IGP805 .                                                       
           02 SAUT                    PIC X.                                    
           02 LIGNE                   PIC X(132).                               
       01  W-DATE.                                                              
           03  DATJMSA.                                                         
               05 JJ                  PIC  XX.                                  
               05 MM                  PIC  XX.                                  
               05 SS                  PIC  XX.                                  
               05 AA                  PIC  XX.                                  
           03  W-TIME.                                                          
               05 HH                  PIC  XX.                                  
               05 MN                  PIC  XX.                                  
               05 FILLER              PIC  X(14).                               
       01  JOUR-SEM                   PIC X.                                    
       01  DATSAMJ                    PIC X(08).                                
       01  DATSM-9                    PIC 9(06).                                
      *                                                                         
       01  RUPTURE                    PIC 99 VALUE 15.                          
      *                                                                         
       01  CTR-FGP805                 PIC 9(9) VALUE 0.                         
      *                                                                         
       01  FLAG-FICHIER               PIC X VALUE SPACE.                        
           88  EOF                          VALUE 'F'.                          
       01  FLAG-ENTETE                PIC X VALUE SPACE.                        
           88  RUPTURE-ENTETE               VALUE ' '.                          
           88  ENTETE-ECRITE                VALUE 'O'.                          
      *                                                                         
      *                                                                         
      *ENREGISTREMENT COURANT                                                   
       01 FGP805-ENTREPOT PIC X(06).                                            
      *01 COPY-FGP805.                                                          
              COPY FGP805 REPLACING ==:FGPXXX:== BY ==FGP805==.                 
      *                                                                         
      *ENREGISTREMENT PRECEDENT                                                 
      *01 COPY-W-FGP805.                                                        
              COPY FGP805 REPLACING ==:FGPXXX:== BY ==W-FGP805==.               
      *                                                                         
      * --------------- TABLEAU T1 TOTAUX PAR FAMILLE --------------            
       01 I                                  PIC S9(4) COMP-3 VALUE 0.          
       01 I-T1                               PIC S9(4) COMP-3 VALUE 0.          
       01 MAX-T1                             PIC S9(5) COMP-3 VALUE 50.         
       01 NB-T1                              PIC S9(5) COMP-3 VALUE 0.          
       01 TAB-CUMUL-T1.                                                         
          02 W-T1                            OCCURS 50.                         
             05  W-CLE-T1.                                                      
               10  W-T1-CFAM                 PIC X(05).                         
               10  W-T1-TRIDEP               PIC X(01).                         
               10  W-T1-NSOCDEPOT            PIC X(03).                         
               10  W-T1-NDEPOT               PIC X(03).                         
             05  W-REC-T1.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQ4S                PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T1-TFQ4S                PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQS-0               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T1-TFQS-0               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQS-1               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T1-TFQS-1               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQS-2               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T1-TFQS-2               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQS-3               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T1-TFQS-3               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQS-4               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T1-TFQS-4               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TSORTIES7J           PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T1-TSORTIES7J           PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TCOUV7J              PIC S999V9 COMP.                   
      *--                                                                       
               10  W-T1-TCOUV7J              PIC S999V9 COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TNBCOUV              PIC S9(4) COMP.                    
      *--                                                                       
               10  W-T1-TNBCOUV              PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQSTOCKMAG          PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T1-TFQSTOCKMAG          PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQSTOCKDIS          PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T1-TFQSTOCKDIS          PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQSTOCKRES          PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T1-TFQSTOCKRES          PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQCDE               PIC 9(7) COMP.                     
      *--                                                                       
               10  W-T1-TFQCDE               PIC 9(7) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T1-TFQCDERES            PIC 9(6) COMP.                     
      *--                                                                       
               10  W-T1-TFQCDERES            PIC 9(6) COMP-5.                   
      *}                                                                        
      * ZONES TAMPONS POUR TRI                                                  
       01 TAMP-T1.                                                              
             05  CLE-TAMP-T1.                                                   
               10  TAMP-T1-CFAM              PIC X(05).                         
               10  TAMP-T1-TRIDEP            PIC X(01).                         
               10  TAMP-T1-NSOCDEPOT         PIC X(03).                         
               10  TAMP-T1-NDEPOT            PIC X(03).                         
             05  REC-TAMP-T1.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQ4S             PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQ4S             PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQS-0            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQS-0            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQS-1            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQS-1            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQS-2            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQS-2            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQS-3            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQS-3            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQS-4            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQS-4            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TSORTIES7J        PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T1-TSORTIES7J        PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TCOUV7J           PIC S999V9 COMP.                   
      *--                                                                       
               10  TAMP-T1-TCOUV7J           PIC S999V9 COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TNBCOUV           PIC S9(4) COMP.                    
      *--                                                                       
               10  TAMP-T1-TNBCOUV           PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQSTOCKMAG       PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQSTOCKMAG       PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQSTOCKDIS       PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQSTOCKDIS       PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQSTOCKRES       PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T1-TFQSTOCKRES       PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQCDE            PIC 9(7) COMP.                     
      *--                                                                       
               10  TAMP-T1-TFQCDE            PIC 9(7) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T1-TFQCDERES         PIC 9(6) COMP.                     
      *--                                                                       
               10  TAMP-T1-TFQCDERES         PIC 9(6) COMP-5.                   
      *}                                                                        
      * --------------- TABLEAU T2 TOTAUX PAR RAYON   --------------            
       01 I-T2                               PIC S9(4) COMP-3 VALUE 0.          
       01 MAX-T2                             PIC S9(5) COMP-3 VALUE 20.         
       01 NB-T2                              PIC S9(5) COMP-3 VALUE 0.          
       01 TAB-CUMUL-T2.                                                         
          02  W-T2                           OCCURS 20.                         
             05  W-CLE-T2.                                                      
               10  W-T2-RAYON                PIC X(05).                         
               10  W-T2-TRIDEP               PIC X(01).                         
               10  W-T2-NSOCDEPOT            PIC X(03).                         
               10  W-T2-NDEPOT               PIC X(03).                         
             05  W-REC-T2.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQ4S                PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T2-TFQ4S                PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQS-0               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T2-TFQS-0               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQS-1               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T2-TFQS-1               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQS-2               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T2-TFQS-2               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQS-3               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T2-TFQS-3               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQS-4               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T2-TFQS-4               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TSORTIES7J           PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T2-TSORTIES7J           PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TCOUV7J              PIC S999V9 COMP.                   
      *--                                                                       
               10  W-T2-TCOUV7J              PIC S999V9 COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TNBCOUV              PIC S9(4) COMP.                    
      *--                                                                       
               10  W-T2-TNBCOUV              PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQSTOCKMAG          PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T2-TFQSTOCKMAG          PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQSTOCKDIS          PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T2-TFQSTOCKDIS          PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQSTOCKRES          PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T2-TFQSTOCKRES          PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQCDE               PIC 9(7) COMP.                     
      *--                                                                       
               10  W-T2-TFQCDE               PIC 9(7) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T2-TFQCDERES            PIC 9(6) COMP.                     
      *--                                                                       
               10  W-T2-TFQCDERES            PIC 9(6) COMP-5.                   
      *}                                                                        
      * ZONES TAMPONS POUR TRI                                                  
       01 TAMP-T2.                                                              
             05  CLE-TAMP-T2.                                                   
               10  TAMP-T2-RAYON             PIC X(05).                         
               10  TAMP-T2-TRIDEP            PIC X(01).                         
               10  TAMP-T2-NSOCDEPOT         PIC X(03).                         
               10  TAMP-T2-NDEPOT            PIC X(03).                         
             05  REC-TAMP-T2.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQ4S             PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQ4S             PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQS-0            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQS-0            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQS-1            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQS-1            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQS-2            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQS-2            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQS-3            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQS-3            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQS-4            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQS-4            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TSORTIES7J        PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T2-TSORTIES7J        PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TCOUV7J           PIC S999V9 COMP.                   
      *--                                                                       
               10  TAMP-T2-TCOUV7J           PIC S999V9 COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TNBCOUV           PIC S9(4) COMP.                    
      *--                                                                       
               10  TAMP-T2-TNBCOUV           PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQSTOCKMAG       PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQSTOCKMAG       PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQSTOCKDIS       PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQSTOCKDIS       PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQSTOCKRES       PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T2-TFQSTOCKRES       PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQCDE            PIC 9(7) COMP.                     
      *--                                                                       
               10  TAMP-T2-TFQCDE            PIC 9(7) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T2-TFQCDERES         PIC 9(6) COMP.                     
      *--                                                                       
               10  TAMP-T2-TFQCDERES         PIC 9(6) COMP-5.                   
      *}                                                                        
      *---------------- TABLEAU T3 TOTAUX PAR ENTREPOT -------------            
       01 I-T3                               PIC S9(4) COMP-3 VALUE 0.          
       01 MAX-T3                             PIC S9(5) COMP-3 VALUE 20.         
       01 NB-T3                              PIC S9(5) COMP-3 VALUE 0.          
       01 TAB-CUMUL-T3.                                                         
          02 W-T3               OCCURS 20.                                      
             05  W-CLE-T3.                                                      
               10  W-T3-ENTREPOT             PIC X(06).                         
               10  W-T3-TRIDEP               PIC X(01).                         
               10  W-T3-NSOCDEPOT            PIC X(03).                         
               10  W-T3-NDEPOT               PIC X(03).                         
             05  W-REC-T3.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQ4S                PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T3-TFQ4S                PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQS-0               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T3-TFQS-0               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQS-1               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T3-TFQS-1               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQS-2               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T3-TFQS-2               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQS-3               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T3-TFQS-3               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQS-4               PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T3-TFQS-4               PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TSORTIES7J           PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T3-TSORTIES7J           PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TCOUV7J              PIC S999V9 COMP.                   
      *--                                                                       
               10  W-T3-TCOUV7J              PIC S999V9 COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TNBCOUV              PIC S9(4) COMP.                    
      *--                                                                       
               10  W-T3-TNBCOUV              PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQSTOCKMAG          PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T3-TFQSTOCKMAG          PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQSTOCKDIS          PIC S9(7) COMP.                    
      *--                                                                       
               10  W-T3-TFQSTOCKDIS          PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQSTOCKRES          PIC S9(6) COMP.                    
      *--                                                                       
               10  W-T3-TFQSTOCKRES          PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQCDE               PIC 9(7) COMP.                     
      *--                                                                       
               10  W-T3-TFQCDE               PIC 9(7) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-T3-TFQCDERES            PIC 9(6) COMP.                     
      *--                                                                       
               10  W-T3-TFQCDERES            PIC 9(6) COMP-5.                   
      *}                                                                        
      * ZONES TAMPONS POUR TRI                                                  
       01 TAMP-T3.                                                              
             05  CLE-TAMP-T3.                                                   
               10  TAMP-T3-ENTREPOT          PIC X(06).                         
               10  TAMP-T3-TRIDEP            PIC X(01).                         
               10  TAMP-T3-NSOCDEPOT         PIC X(03).                         
               10  TAMP-T3-NDEPOT            PIC X(03).                         
             05  REC-TAMP-T3.                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQ4S             PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQ4S             PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQS-0            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQS-0            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQS-1            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQS-1            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQS-2            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQS-2            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQS-3            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQS-3            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQS-4            PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQS-4            PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TSORTIES7J        PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T3-TSORTIES7J        PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TCOUV7J           PIC S999V9 COMP.                   
      *--                                                                       
               10  TAMP-T3-TCOUV7J           PIC S999V9 COMP-5.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TNBCOUV           PIC S9(4) COMP.                    
      *--                                                                       
               10  TAMP-T3-TNBCOUV           PIC S9(4) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQSTOCKMAG       PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQSTOCKMAG       PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQSTOCKDIS       PIC S9(7) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQSTOCKDIS       PIC S9(7) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQSTOCKRES       PIC S9(6) COMP.                    
      *--                                                                       
               10  TAMP-T3-TFQSTOCKRES       PIC S9(6) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQCDE            PIC 9(7) COMP.                     
      *--                                                                       
               10  TAMP-T3-TFQCDE            PIC 9(7) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  TAMP-T3-TFQCDERES         PIC 9(6) COMP.                     
      *--                                                                       
               10  TAMP-T3-TFQCDERES         PIC 9(6) COMP-5.                   
      *}                                                                        
       01 SOMME-STOCK                        PIC S9(8) COMP-3 VALUE 0.          
      *-----------------------------------------------------------------        
      *                                                                         
      * ZONES POUR LE TRI BULLE DES TABLEAUX                                    
      *                                                                         
      * BOOLEENS                                                                
       01  WS-TOP-TRI                 PIC 9    VALUE ZERO.                      
           88 FIN-TRI-OK                              VALUE 1.                  
           88 FIN-TRI-KO                              VALUE 0.                  
      * INDICES                                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  NB-T-1                     PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  NB-T-1                     PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  J-TRI                      PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  J-TRI                      PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  J                          PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  J                          PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  K                          PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  K                          PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      ******************************************************************        
      *                                                                         
      *                                                                         
       01  CTR-PAGE     PIC 9(4) VALUE 0.                                       
      *MASQUE D EDITION                                                         
       01  W-MASKED3                         PIC ZZ9.                           
       01  W-MASKED3S                        PIC ---9.                          
       01  W-MASKED4                         PIC ZZZ9.                          
       01  W-MASKED4S                        PIC ----9.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL5                        PIC S9(4)  COMP.                   
      *--                                                                       
       01  W-MASKCAL5                        PIC S9(4) COMP-5.                  
      *}                                                                        
       01  W-MASKED5                         PIC ZZZZ9.                         
       01  W-MASKED5S                        PIC -----9.                        
       01  W-MASKED5V                        PIC ZZ9,9.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL6                        PIC S9(5)  COMP.                   
      *--                                                                       
       01  W-MASKCAL6                        PIC S9(5) COMP-5.                  
      *}                                                                        
       01  W-MASKED6                         PIC ZZZZZ9.                        
       01  W-MASKED6S                        PIC ------9.                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL7                        PIC S9(6)  COMP.                   
      *--                                                                       
       01  W-MASKCAL7                        PIC S9(6) COMP-5.                  
      *}                                                                        
       01  W-MASKED7                         PIC ZZZZZZ9.                       
       01  W-MASKED7S                        PIC -------9.                      
      *                                                                         
      *                                                                         
       01  CTR-LIGNE                         PIC 99   VALUE 0.                  
       01  MAX-LIGNE                         PIC 99   VALUE 58.                 
           EJECT                                                                
      *-------------------------- BLOC ECRITURE DU CODIC2 -------------         
       01 I-BLOC                             PIC S9(4) COMP-3 VALUE 0.          
       01 MAX-BLOC                           PIC S9(5) COMP-3 VALUE 500.        
       01 NB-BLOC                            PIC S9(5) COMP-3 VALUE 0.          
       01 NB-BLOC2                           PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-BLOC                      PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-BLOC2                     PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-CODIC1                    PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-TOTAL                     PIC S9(5) COMP-3 VALUE 0.          
       01 W-BLOC.                                                               
          02 BLOC-LIGNE OCCURS 500.                                             
               10  LIGNE-CODIC               PIC X(132).                        
       01 W-BLOC2.                                                              
          02 BLOC2-LIGNE OCCURS 500.                                            
               10  LIGNE-TOTAL               PIC X(132).                        
      *                                                                         
       01  IGP805-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  IGP805-E00.                                                      
               10  IGP805-E00-CETAT               PIC X(06).                    
               10  FILLER                         PIC X(50) VALUE               
           '                     SUIVI DU STOCK DES ENTREPOTS '.                
               10  FILLER                         PIC X(40) VALUE               
           'DE DEBORDEMENT                 EDITE LE '.                          
               10  IGP805-E00-DATEDITE.                                         
                   15 JJ                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 MM                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 SS                          PIC XX.                       
                   15 AA                          PIC XX.                       
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  IGP805-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  IGP805-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(15) VALUE               
           '          PAGE '.                                                   
               10  IGP805-E00-NOPAGE              PIC ZZZ.                      
      *--------------------------- LIGNE E05  --------------------------        
           05  IGP805-E05.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'SOCIETE      : '.                                                   
               10  IGP805-E05-NSOCIETE            PIC X(03).                    
               10  FILLER                         PIC X(111)                    
                                                  VALUE ALL SPACES.             
      *--------------------------- LIGNE E10  --------------------------        
           05  IGP805-E10.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'DEPOT GEN    : '.                                                   
               10  IGP805-E10-NLIEU               PIC X(03).                    
               10  FILLER                         PIC X(111)                    
                                                  VALUE ALL SPACES.             
      *--------------------------- LIGNE E15  --------------------------        
           05  IGP805-E15.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'RAYON        : '.                                                   
               10  IGP805-E15-RAYON               PIC X(05).                    
               10  FILLER                         PIC X(109)                    
                                                  VALUE ALL SPACES.             
      *--------------------------- LIGNE E20  --------------------------        
           05  IGP805-E20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE E30  --------------------------        
           05  IGP805-E30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+-----------------------------+--------------------------+'.        
               10  FILLER                         PIC X(58) VALUE               
           '-------+-----------+-------------+------------+-----+-----'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------+ '.                                                  
      *--------------------------- LIGNE E35  --------------------------        
           05  IGP805-E35.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '! CODIC    FAM  MARQ. STA O S !          VENTES          !'.        
               10  FILLER                         PIC X(58) VALUE               
           '       ! NB   DIS  !  ENTREPOT   ! SORT  COUV.! ENT ! STA '.        
               10  FILLER                         PIC X(16) VALUE               
           'PROCHAINES COM! '.                                                  
      *--------------------------- LIGNE E40  --------------------------        
           05  IGP805-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                         A A ! S-4  S-3  S-2  S-1  S-0  !'.        
               10  FILLER                         PIC X(56) VALUE               
           '  V4S  ! MAG  MAG  ! DISPO  RES  !  7J.   7J. !     !   '.          
               10  FILLER                         PIC X(18) VALUE               
           'DATE   QTE  RES ! '.                                                
      *--------------------------- LIGNE E45  --------------------------        
           05  IGP805-E45.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+-----------------------------+--------------------------+'.        
               10  FILLER                         PIC X(58) VALUE               
           '-------+-----------+-------------+------------+-----+-----'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------+ '.                                                  
      *--------------------------- LIGNE D05  --------------------------        
           05  IGP805-D05.                                                      
               10  FILLER                         PIC X(2)  VALUE               
           '! '.                                                                
               10  IGP805-D05-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-CFAM                PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-LSTATCOMP           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-WOA                 PIC X(01).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-WSENSAPPRO          PIC X(01).                    
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP805-D05-QS-4                PIC XXXXX.                    
               10  IGP805-D05-QS-3                PIC XXXXX.                    
               10  IGP805-D05-QS-2                PIC XXXXX.                    
               10  IGP805-D05-QS-1                PIC XXXXX.                    
               10  IGP805-D05-QS-0                PIC XXXXX.                    
               10  FILLER                         PIC X(02) VALUE               
           ' !'.                                                                
               10  IGP805-D05-Q4S                 PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-D05-NBMAG               PIC XXX.                      
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-QSTOCKMAG           PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-D05-QSTOCKDIS           PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-QSTOCKRES           PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-D05-SORTIES7J           PIC XXXX.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-COUV7J              PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-D05-NDEPOT              PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-D05-CSTATUT             PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-DCDE4               PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-QCDE                PIC XXXX.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D05-QCDERES             PIC XXXX.                     
               10  FILLER                         PIC X(01) VALUE               
           '!'.                                                                 
      *--------------------------- LIGNE D10  --------------------------        
           05  IGP805-D10.                                                      
               10  FILLER                         PIC X(10) VALUE               
           '!         '.                                                        
               10  IGP805-D10-LREFFOURN           PIC X(20).                    
               10  FILLER                         PIC X(27) VALUE               
           '!                          '.                                       
               10  FILLER                         PIC X(55) VALUE               
           '!       !           !             !            !     ! '.           
2705           10  IGP805-D10-CSTATUT             PIC X(03).                    
               10  FILLER                         PIC X(1) VALUE                
           ' '.                                                                 
               10  IGP805-D10-DCDE4               PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D10-QCDE                PIC XXXX.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-D10-QCDERES             PIC XXXX.                     
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
      *--------------------------- LIGNE T00  --------------------------        
           05  IGP805-T00.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '!                             !                          !'.        
               10  FILLER                         PIC X(58) VALUE               
           '       !           !             !            !     !     '.        
               10  FILLER                         PIC X(16) VALUE               
           '              ! '.                                                  
      *--------------------------- LIGNE TIRETS-------------------------        
           05  IGP805-T.                                                        
               10  FILLER                         PIC X(58) VALUE               
           '+---------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------+ '.                                                  
      *--------------------------- LIGNE T20  --------------------------        
           05  IGP805-T20.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '+-----------------------------+--------------------------+'.        
               10  FILLER                         PIC X(58) VALUE               
           '-------+-----------+-------------+------------+-----+-----'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------+ '.                                                  
      *--------------------------- LIGNE T1-ENTETE ---------------------        
           05  IGP805-T1-E1.                                                    
               10  FILLER                         PIC X(58) VALUE               
           '! TOTAL/FAMILLE !  S-4    S-3    S-2    S-1    S-0     V4S'.        
               10  FILLER                         PIC X(58) VALUE               
           '   !  DISP MAG ! DIS ENT  RES   ! SORT7J  COUV7J ! ENT ! Q'.        
               10  FILLER                         PIC X(16) VALUE               
           'T COM  QT RES !'.                                                   
      *--------------------------- LIGNE T1-ENTETE ---------------------        
           05  IGP805-T1-E2.                                                    
               10  FILLER                         PIC X(58) VALUE               
           '! ============= ! ====== ====== ====== ====== ====== ====='.        
               10  FILLER                         PIC X(58) VALUE               
           '== !   ======= ! ======= ====== ! =======  ===== ! === ! ='.        
               10  FILLER                         PIC X(16) VALUE               
           '====== ====== !'.                                                   
      *--------------------------- LIGNE T1 TOTAL PAR FAMILLE-----------        
           05  IGP805-T1.                                                       
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IGP805-T1-LIBFAM               PIC X(6).                     
               10  IGP805-T1-CFAM                 PIC X(5).                     
               10  FILLER                         PIC X(4) VALUE                
           '   !'.                                                              
               10  IGP805-T1-TFQS-4               PIC XXXXXXX.                  
               10  IGP805-T1-TFQS-3               PIC XXXXXXX.                  
               10  IGP805-T1-TFQS-2               PIC XXXXXXX.                  
               10  IGP805-T1-TFQS-1               PIC XXXXXXX.                  
               10  IGP805-T1-TFQS-0               PIC XXXXXXX.                  
               10  IGP805-T1-TFQ4S                PIC XXXXXXXX.                 
               10  FILLER                         PIC X(05) VALUE               
           ' !  '.                                                              
               10  IGP805-T1-TFQSTOCKMAG          PIC XXXXXXX.                  
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T1-TFQSTOCKDIS          PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-T1-TFQSTOCKRES          PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T1-TSORTIES7J           PIC XXXXXXX.                  
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGP805-T1-TCOUV7J              PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T1-NDEPOT               PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
            ' ! '.                                                              
               10  IGP805-T1-TFQCDE               PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-T1-TFQCDERES            PIC XXXXXX.                   
               10  FILLER                         PIC X(2) VALUE                
           ' !'.                                                                
      *--------------------------- LIGNE T2-ENTETE ---------------------        
           05  IGP805-T2-E1.                                                    
               10  FILLER                         PIC X(58) VALUE               
           '! TOTAL/ RAYON  !  S-4    S-3    S-2    S-1    S-0     V4S'.        
               10  FILLER                         PIC X(58) VALUE               
           '   !  DISP MAG ! DIS ENT  RES   ! SORT7J  COUV7J ! ENT ! Q'.        
               10  FILLER                         PIC X(16) VALUE               
           'T COM  QT RES !'.                                                   
      *--------------------------- LIGNE T2-ENTETE ---------------------        
           05  IGP805-T2-E2.                                                    
               10  FILLER                         PIC X(58) VALUE               
           '! ============= ! ====== ====== ====== ====== ====== ====='.        
               10  FILLER                         PIC X(58) VALUE               
           '== !   ======= ! ======= ====== ! =======  ===== ! === ! ='.        
               10  FILLER                         PIC X(16) VALUE               
           '====== ====== !'.                                                   
      *--------------------------- LIGNE T2 TOTAL PAR RAYON  -----------        
           05  IGP805-T2.                                                       
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IGP805-T2-LIBRAY               PIC X(6).                     
               10  IGP805-T2-RAYON                PIC X(5).                     
               10  FILLER                         PIC X(4) VALUE                
           '   !'.                                                              
               10  IGP805-T2-TFQS-4               PIC XXXXXXX.                  
               10  IGP805-T2-TFQS-3               PIC XXXXXXX.                  
               10  IGP805-T2-TFQS-2               PIC XXXXXXX.                  
               10  IGP805-T2-TFQS-1               PIC XXXXXXX.                  
               10  IGP805-T2-TFQS-0               PIC XXXXXXX.                  
               10  IGP805-T2-TFQ4S                PIC XXXXXXXX.                 
               10  FILLER                         PIC X(05) VALUE               
           ' !  '.                                                              
               10  IGP805-T2-TFQSTOCKMAG          PIC XXXXXXX.                  
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T2-TFQSTOCKDIS          PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-T2-TFQSTOCKRES          PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T2-TSORTIES7J           PIC XXXXXXX.                  
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGP805-T2-TCOUV7J              PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T2-NDEPOT               PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
            ' ! '.                                                              
               10  IGP805-T2-TFQCDE               PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-T2-TFQCDERES            PIC XXXXXX.                   
               10  FILLER                         PIC X(2) VALUE                
           ' !'.                                                                
      *--------------------------- LIGNE T3-ENTETE ---------------------        
           05  IGP805-T3-E1.                                                    
               10  FILLER                         PIC X(58) VALUE               
           '! TOTAL/ENTREPOT!  S-4    S-3    S-2    S-1    S-0     V4S'.        
               10  FILLER                         PIC X(58) VALUE               
           '   !  DISP MAG ! DIS ENT  RES   ! SORT7J  COUV7J ! ENT ! Q'.        
               10  FILLER                         PIC X(16) VALUE               
           'T COM  QT RES !'.                                                   
      *--------------------------- LIGNE T3-ENTETE ---------------------        
           05  IGP805-T3-E2.                                                    
               10  FILLER                         PIC X(58) VALUE               
           '! ============= ! ====== ====== ====== ====== ====== ====='.        
               10  FILLER                         PIC X(58) VALUE               
           '== !   ======= ! ======= ====== ! =======  ===== ! === ! ='.        
               10  FILLER                         PIC X(16) VALUE               
           '====== ====== !'.                                                   
      *--------------------------- LIGNE T3 TOTAL PAR ENTREPOT ---------        
           05  IGP805-T3.                                                       
               10  FILLER                         PIC X(02) VALUE               
           '! '.                                                                
               10  IGP805-T3-LIBENT               PIC X(5).                     
               10  IGP805-T3-ENT                  PIC X(6).                     
               10  FILLER                         PIC X(4) VALUE                
           '   !'.                                                              
               10  IGP805-T3-TFQS-4               PIC XXXXXXX.                  
               10  IGP805-T3-TFQS-3               PIC XXXXXXX.                  
               10  IGP805-T3-TFQS-2               PIC XXXXXXX.                  
               10  IGP805-T3-TFQS-1               PIC XXXXXXX.                  
               10  IGP805-T3-TFQS-0               PIC XXXXXXX.                  
               10  IGP805-T3-TFQ4S                PIC XXXXXXXX.                 
               10  FILLER                         PIC X(05) VALUE               
           ' !  '.                                                              
               10  IGP805-T3-TFQSTOCKMAG          PIC XXXXXXX.                  
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T3-TFQSTOCKDIS          PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-T3-TFQSTOCKRES          PIC XXXXXX.                   
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T3-TSORTIES7J           PIC XXXXXXX.                  
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGP805-T3-TCOUV7J              PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP805-T3-NDEPOT               PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
            ' ! '.                                                              
               10  IGP805-T3-TFQCDE               PIC XXXXXXX.                  
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP805-T3-TFQCDERES            PIC XXXXXX.                   
               10  FILLER                         PIC X(2) VALUE                
           ' !'.                                                                
      *-----------------------------------------------------------------        
           EJECT                                                                
       01 W1-CLE-PREC.                                                          
          05 W1-CFAM                              PIC X(05).                    
          05 W1-TRIDEP                            PIC X(01).                    
          05 W1-NSOCDEPOT                         PIC X(03).                    
          05 W1-NDEPOT                            PIC X(03).                    
       01 W2-CLE-PREC.                                                          
          05 W2-RAYON                             PIC X(05).                    
          05 W2-TRIDEP                            PIC X(01).                    
          05 W2-NSOCDEPOT                         PIC X(03).                    
          05 W2-NDEPOT                            PIC X(03).                    
       01 W3-CLE-PREC.                                                          
          05 W3-ENTREPOT                          PIC X(06).                    
          05 W3-TRIDEP                            PIC X(01).                    
          05 W3-NSOCDEPOT                         PIC X(03).                    
          05 W3-NDEPOT                            PIC X(03).                    
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    T A B L E S       *         
      *---------------------------------------------------------------*         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BGP806 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BGP806 SECTION.                                                   
      *-----------------------                                                  
      *                                                                         
           PERFORM  DEBUT-BGP806.                                               
           PERFORM  TRAIT-BGP806.                                               
           PERFORM  FIN-BGP806.                                                 
      *                                                                         
       F-MODULE-BGP806. EXIT.                                                   
      *-----------------------                                                  
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    D E B U T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       DEBUT-BGP806 SECTION.                                                    
      *-----------------------                                                  
      *                                                                         
           DISPLAY '*** DEBUT BGP806 ***'.                                      
           PERFORM OUVERTURE-FICHIERS.                                          
           PERFORM TRAIT-DATE.                                                  
      *                                                                         
       F-DEBUT-BGP806.                                                          
      *-----------------------                                                  
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    T R A I T        B G P 8 0 6               *         
      *---------------------------------------------------------------*         
      *                                                                         
       TRAIT-BGP806 SECTION.                                                    
      *-----------------------                                                  
      *                                                                         
           INITIALIZE CTR-PAGE.                                                 
           INITIALIZE TAB-CUMUL-T1                                              
           INITIALIZE TAB-CUMUL-T2                                              
           INITIALIZE TAB-CUMUL-T3                                              
           INITIALIZE W-BLOC NB-BLOC  NB-BLOC2                                  
           PERFORM LECTURE-FGP805.                                              
EOF        PERFORM UNTIL EOF                                                    
            PERFORM RUPTURE-NSOCIETE                                            
NSOC        PERFORM UNTIL                                                       
               FGP805-NSOCIETE NOT = W-FGP805-NSOCIETE                          
            OR EOF                                                              
             PERFORM RUPTURE-DEPOTGEN                                           
DEPG         PERFORM UNTIL                                                      
                FGP805-NSOCIETE   NOT = W-FGP805-NSOCIETE                       
             OR FGP805-NSOCDEPGEN NOT = W-FGP805-NSOCDEPGEN                     
             OR FGP805-NDEPOTGEN  NOT = W-FGP805-NDEPOTGEN                      
             OR EOF                                                             
               PERFORM ECRITURE-ENTETE                                          
RAYON          PERFORM RUPTURE-RAYON                                            
               PERFORM UNTIL                                                    
                  FGP805-NSOCIETE   NOT = W-FGP805-NSOCIETE                     
               OR FGP805-NSOCDEPGEN NOT = W-FGP805-NSOCDEPGEN                   
               OR FGP805-NDEPOTGEN  NOT = W-FGP805-NDEPOTGEN                    
               OR FGP805-RAYON      NOT = W-FGP805-RAYON                        
               OR EOF                                                           
                 PERFORM RUPTURE-FAMILLE                                        
FAMIL            PERFORM UNTIL                                                  
                    FGP805-NSOCIETE   NOT = W-FGP805-NSOCIETE                   
                 OR FGP805-NSOCDEPGEN NOT = W-FGP805-NSOCDEPGEN                 
                 OR FGP805-NDEPOTGEN  NOT = W-FGP805-NDEPOTGEN                  
                 OR FGP805-RAYON      NOT = W-FGP805-RAYON                      
                 OR FGP805-WSEQFAM    NOT = W-FGP805-WSEQFAM                    
                 OR EOF                                                         
CODIC              PERFORM RUPTURE-NCODIC                                       
                   PERFORM UNTIL                                                
                      FGP805-NSOCIETE   NOT = W-FGP805-NSOCIETE                 
                   OR FGP805-NSOCDEPGEN NOT = W-FGP805-NSOCDEPGEN               
                   OR FGP805-NDEPOTGEN  NOT = W-FGP805-NDEPOTGEN                
                   OR FGP805-RAYON      NOT = W-FGP805-RAYON                    
                   OR FGP805-WSEQFAM    NOT = W-FGP805-WSEQFAM                  
                   OR FGP805-NCODIC     NOT = W-FGP805-NCODIC                   
                   OR EOF                                                       
                     PERFORM RUPTURE-NDEPOT                                     
DEPO                 PERFORM UNTIL                                              
                        FGP805-NSOCIETE   NOT = W-FGP805-NSOCIETE               
                     OR FGP805-NSOCDEPGEN NOT = W-FGP805-NSOCDEPGEN             
                     OR FGP805-NDEPOTGEN  NOT = W-FGP805-NDEPOTGEN              
                     OR FGP805-RAYON      NOT = W-FGP805-RAYON                  
                     OR FGP805-WSEQFAM    NOT = W-FGP805-WSEQFAM                
                     OR FGP805-NCODIC     NOT = W-FGP805-NCODIC                 
                     OR FGP805-NSOCDEP    NOT = W-FGP805-NSOCDEP                
                     OR FGP805-NDEPOT     NOT = W-FGP805-NDEPOT                 
                     OR EOF                                                     
                            PERFORM CUMUL-TOTAUX                                
                            PERFORM ECRITURE-DETAIL                             
                            PERFORM LECTURE-FGP805                              
DEPOT                  END-PERFORM                                              
CODIC              END-PERFORM                                                  
      * APR�S CHAQUE CODIC TRAIT� ON �CRIT LE BLOC DE LIGNE DU CODIC            
                   PERFORM BLOC-CODIC                                           
FAM            END-PERFORM                                                      
RAYON        END-PERFORM                                                        
      * APR�S RUPTURE RAYON ON �CRIT LES TOTAUX PAR FAMILLE ET PAR RAYON        
             PERFORM ECRITURE-TAB-T1                                            
             PERFORM ECRITURE-T1                                                
             PERFORM ECRITURE-TAB-T2                                            
             PERFORM ECRITURE-T1                                                
DEPG        END-PERFORM                                                         
      * APR�S RUPTURE DEP G�N�RALISTE ON IMPRIME LE TOTAL DE L'ENTREPOT         
            PERFORM ECRITURE-TAB-T3                                             
            PERFORM ECRITURE-T1                                                 
NSOC        END-PERFORM                                                         
EOF        END-PERFORM.                                                         
       FIN-TRAIT-BGP806. EXIT.                                                  
      *-----------------------                                                  
      *                                                                         
       FIN-BGP806 SECTION.                                                      
      *-------------------                                                      
      *                                                                         
           CLOSE FDATE IGP805 FGP805.                                           
           DISPLAY '**********************************************'.            
           DISPLAY '*    PROGRAMME BGP806  TERMINE NORMALEMENT   *'.            
           DISPLAY '*                                            *'.            
           DISPLAY '*  NOMBRE D''ENREG LUS SUR FGP805    = ' CTR-FGP805         
                   '  *'.                                                       
           DISPLAY '*                                            *'.            
           DISPLAY '**********************************************'.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-BGP806. EXIT.                                                    
      *-------------------                                                      
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    D E B U T  B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       OUVERTURE-FICHIERS SECTION.                                              
      *-------------------                                                      
           OPEN INPUT FDATE FGP805.                                             
           OPEN OUTPUT IGP805.                                                  
       FIN-OUVERTURE-FICHIERS. EXIT.                                            
      *------------------------------                                           
      *                                                                         
       TRAIT-DATE SECTION.                                                      
      *-------------------                                                      
           READ FDATE                                                           
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                      
                  PERFORM FIN-ANORMALE.                                         
           MOVE '1'                 TO GFDATA.                                  
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                              
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                            
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS            
              PERFORM FIN-ANORMALE                                              
           ELSE                                                                 
              DISPLAY '** FDATE  ** ' GFJMSA-5                                  
           END-IF.                                                              
           COMPUTE GFQNT0 = GFQNT0 + 1 .                                        
           MOVE '3' TO GFDATA.                                                  
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS                
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFJJMMSSAA       TO DATJMSA.                                    
           MOVE GFSMN            TO JOUR-SEM.                                   
2705       MOVE GFSAMJ-0         TO DATSAMJ.                                    
2705       MOVE DATSAMJ (1:6)    TO DATSM-9.                                    
           DISPLAY 'DATE RECEPTION ETAT DANS BOITE EOS : ' DATJMSA.             
      *    MOVE 'FICHIER DATE VIDE' TO ABEND-MESS.                              
           ACCEPT W-TIME FROM TIME.                                             
       FIN-TRAIT-DATE. EXIT.                                                    
      *-----------------------                                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    T R A I T  B G P 8 6 5               *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FGP805 SECTION.                                                  
      *-----------------------                                                  
           MOVE FGP805-ENREG TO W-FGP805-ENREG.                                 
           INITIALIZE FGP805-ENREG.                                             
             READ FGP805                                                        
                  INTO FGP805-ENREG                                             
                  AT END SET EOF TO TRUE                                        
             END-READ                                                           
              IF CTR-FGP805 = 0                                                 
                 MOVE FGP805-ENREG TO W-FGP805-ENREG                            
              END-IF                                                            
              ADD 1 TO CTR-FGP805.                                              
              PERFORM NUMERO-RUPTURE.                                           
       FIN-LECTURE-FGP805. EXIT.                                                
      *-----------------------------                                            
      *---------------- RUPTURE DE L ETAT ----------------------------          
       NUMERO-RUPTURE SECTION.                                                  
      *-----------------------------                                            
           IF FGP805-NSOCIETE = W-FGP805-NSOCIETE                               
            MOVE 9 TO RUPTURE                                                   
              IF FGP805-NSOCDEPGEN = W-FGP805-NSOCDEPGEN                        
                MOVE 8  TO RUPTURE                                              
                IF FGP805-NDEPOTGEN = W-FGP805-NDEPOTGEN                        
                 MOVE 7  TO RUPTURE                                             
                 IF FGP805-RAYON = W-FGP805-RAYON                               
                  MOVE 6  TO RUPTURE                                            
                  IF FGP805-WSEQFAM = W-FGP805-WSEQFAM                          
                   MOVE 5  TO RUPTURE                                           
                   IF FGP805-NCODIC = W-FGP805-NCODIC                           
                     MOVE 4  TO RUPTURE                                         
                    IF FGP805-NSOCDEP = W-FGP805-NSOCDEP                        
                     MOVE 3  TO RUPTURE                                         
                     IF FGP805-NDEPOT = W-FGP805-NDEPOT                         
                      MOVE 2  TO RUPTURE                                        
                      IF FGP805-DCDE = W-FGP805-DCDE                            
                       MOVE 1  TO RUPTURE                                       
                      END-IF                                                    
                     END-IF                                                     
                    END-IF                                                      
                   END-IF                                                       
                  END-IF                                                        
                 END-IF                                                         
                END-IF                                                          
               END-IF                                                           
           ELSE                                                                 
            MOVE 10 TO RUPTURE                                                  
           END-IF.                                                              
           IF CTR-FGP805 = 1 MOVE 10 TO RUPTURE.                                
       FIN-NUMERO-RUPTURE. EXIT.                                                
      *-----------------------------                                            
      *                                                                         
       RUPTURE-NSOCIETE SECTION.                                                
      *-----------------------------                                            
           MOVE FGP805-NSOCIETE TO W-FGP805-NSOCIETE.                           
       FIN-RUPTURE-NSOCIETE. EXIT.                                              
      *-----------------------------                                            
      *                                                                         
       RUPTURE-DEPOTGEN SECTION.                                                
      *-----------------------------                                            
           MOVE FGP805-NSOCIETE   TO W-FGP805-NSOCIETE.                         
           MOVE FGP805-NSOCDEPGEN TO W-FGP805-NSOCDEPGEN.                       
           MOVE FGP805-NDEPOTGEN  TO W-FGP805-NDEPOTGEN.                        
       FIN-RUPTURE-DEPOTGEN. EXIT.                                              
      *-----------------------------                                            
      *                                                                         
       RUPTURE-RAYON    SECTION.                                                
      *-----------------------------                                            
           MOVE FGP805-NSOCIETE   TO W-FGP805-NSOCIETE                          
           MOVE FGP805-NSOCDEPGEN TO W-FGP805-NSOCDEPGEN                        
           MOVE FGP805-NDEPOTGEN  TO W-FGP805-NDEPOTGEN                         
           MOVE FGP805-RAYON      TO W-FGP805-RAYON.                            
       FIN-RUPTURE-RAYON. EXIT.                                                 
      *-----------------------------                                            
      *                                                                         
       RUPTURE-FAMILLE SECTION.                                                 
      *-----------------------------                                            
           MOVE FGP805-NSOCIETE   TO W-FGP805-NSOCIETE                          
           MOVE FGP805-NSOCDEPGEN TO W-FGP805-NSOCDEPGEN                        
           MOVE FGP805-NDEPOTGEN  TO W-FGP805-NDEPOTGEN                         
           MOVE FGP805-RAYON      TO W-FGP805-RAYON                             
           MOVE FGP805-WSEQFAM    TO W-FGP805-WSEQFAM.                          
       FIN-RUPTURE-FAMILLE. EXIT.                                               
      *-----------------------------                                            
      *                                                                         
       RUPTURE-NCODIC     SECTION.                                              
      *-----------------------------                                            
           MOVE FGP805-NSOCIETE   TO W-FGP805-NSOCIETE                          
           MOVE FGP805-NSOCDEPGEN TO W-FGP805-NSOCDEPGEN                        
           MOVE FGP805-NDEPOTGEN  TO W-FGP805-NDEPOTGEN                         
           MOVE FGP805-RAYON      TO W-FGP805-RAYON                             
           MOVE FGP805-WSEQFAM    TO W-FGP805-WSEQFAM                           
           MOVE FGP805-NCODIC     TO W-FGP805-NCODIC.                           
       FIN-RUPTURE-NCODIC.     EXIT.                                            
      *-----------------------------                                            
      *                                                                         
       RUPTURE-NDEPOT  SECTION.                                                 
      *------------------------                                                 
           MOVE FGP805-NSOCIETE   TO W-FGP805-NSOCIETE                          
           MOVE FGP805-NSOCDEPGEN TO W-FGP805-NSOCDEPGEN                        
           MOVE FGP805-NDEPOTGEN  TO W-FGP805-NDEPOTGEN                         
           MOVE FGP805-RAYON      TO W-FGP805-RAYON                             
           MOVE FGP805-WSEQFAM    TO W-FGP805-WSEQFAM                           
           MOVE FGP805-NCODIC     TO W-FGP805-NCODIC                            
           MOVE FGP805-NSOCDEP    TO W-FGP805-NSOCDEP                           
           MOVE FGP805-NDEPOT     TO W-FGP805-NDEPOT.                           
       FIN-RUPTURE-NDEPOT.  EXIT.                                               
      *--------------------------                                               
      *                                                                         
      *-------------------- ECRITURE DES LIGNES ETAT -----------------          
      *                                                                         
       ECRITURE-BLOC SECTION.                                                   
      *----------------------                                                   
           ADD 1                        TO NB-BLOC                              
           EVALUATE SAUT                                                        
             WHEN ' '  ADD 1            TO NB-LIGNE-BLOC NB-LIGNE-CODIC1        
             WHEN '0'  ADD 2            TO NB-LIGNE-BLOC NB-LIGNE-CODIC1        
             WHEN OTHER CONTINUE                                                
           END-EVALUATE                                                         
           IF NB-BLOC > MAX-BLOC                                                
              MOVE 'TAB BLOC TROP PETIT' TO ABEND-MESS                          
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           MOVE LIGNE-IGP805             TO LIGNE-CODIC(NB-BLOC)                
           IF RUPTURE-ENTETE                                                    
              PERFORM ECRITURE-ENTETE                                           
           END-IF.                                                              
       FIN-ECRITURE-BLOC. EXIT.                                                 
      *------------------------                                                 
      *                                                                         
       ECRITURE-BLOC-2 SECTION.                                                 
      *------------------------                                                 
           ADD 1                        TO NB-BLOC2.                            
           EVALUATE SAUT                                                        
             WHEN ' '  ADD 1            TO NB-LIGNE-BLOC2 NB-LIGNE-TOTAL        
             WHEN '0'  ADD 2            TO NB-LIGNE-BLOC2 NB-LIGNE-TOTAL        
             WHEN OTHER CONTINUE                                                
           END-EVALUATE.                                                        
           IF NB-BLOC2 > MAX-BLOC                                               
              MOVE 'TAB BLOC TROP PETIT' TO ABEND-MESS                          
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE LIGNE-IGP805             TO LIGNE-TOTAL(NB-BLOC2).              
      *                                                                         
       FIN-ECRITURE-BLOC-2. EXIT.                                               
      *--------------------------                                               
      *                                                                         
       ECRITURE-ENTETE SECTION.                                                 
      *------------------------                                                 
           IF RUPTURE-ENTETE                                                    
             ADD 1                      TO CTR-PAGE                             
             MOVE 0                     TO CTR-LIGNE                            
             MOVE 'IGP805'              TO IGP805-E00-CETAT                     
             MOVE CORRESPONDING DATJMSA TO IGP805-E00-DATEDITE                  
             MOVE HH OF W-TIME          TO IGP805-E00-HEUREDITE                 
             MOVE MN OF W-TIME          TO IGP805-E00-MINUEDITE                 
             MOVE CTR-PAGE              TO IGP805-E00-NOPAGE                    
             MOVE '1'                   TO SAUT                                 
             MOVE IGP805-E00            TO LIGNE                                
             WRITE ENREG-IGP805 FROM LIGNE-IGP805                               
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP805-NSOCIETE       TO IGP805-E05-NSOCIETE                  
             MOVE ' '                   TO SAUT                                 
             MOVE IGP805-E05            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP805-NDEPOTGEN      TO IGP805-E10-NLIEU                     
             MOVE ' '                   TO SAUT                                 
             MOVE IGP805-E10            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
      *                                                                         
             MOVE FGP805-RAYON          TO IGP805-E15-RAYON                     
             MOVE '0'                   TO SAUT                                 
             MOVE IGP805-E15            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 2 TO CTR-LIGNE                                                 
      *                                                                         
      *                                                                         
             MOVE ' '                   TO SAUT                                 
             MOVE IGP805-E30            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP805-E35            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP805-E40            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP805-E45            TO LIGNE                                
             WRITE ENREG-IGP805         FROM LIGNE-IGP805                       
             ADD 1 TO CTR-LIGNE                                                 
             SET ENTETE-ECRITE          TO TRUE                                 
           ELSE                                                                 
             SET RUPTURE-ENTETE         TO TRUE                                 
           END-IF.                                                              
       FIN-ECRITURE-ENTETE. EXIT.                                               
      *--------------------------                                               
      *                                                                         
       ECRITURE-ENTETE-PRE SECTION.                                             
      *----------------------------                                             
           ADD 1                        TO CTR-PAGE.                            
           MOVE 0                       TO CTR-LIGNE.                           
           MOVE 'IGP805'                TO IGP805-E00-CETAT.                    
           MOVE CORRESPONDING DATJMSA   TO IGP805-E00-DATEDITE.                 
           MOVE HH OF W-TIME            TO IGP805-E00-HEUREDITE.                
           MOVE MN OF W-TIME            TO IGP805-E00-MINUEDITE.                
           MOVE CTR-PAGE                TO IGP805-E00-NOPAGE.                   
           MOVE '1'                     TO SAUT.                                
           MOVE IGP805-E00              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP805-NSOCIETE       TO IGP805-E05-NSOCIETE.                 
           MOVE ' '                     TO SAUT.                                
           MOVE IGP805-E05              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP805-NDEPOTGEN      TO IGP805-E10-NLIEU.                    
           MOVE ' '                     TO SAUT.                                
           MOVE IGP805-E10              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
      *                                                                         
           MOVE W-FGP805-RAYON          TO IGP805-E15-RAYON.                    
           MOVE '0'                     TO SAUT.                                
           MOVE IGP805-E15              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 2 TO CTR-LIGNE.                                                  
           MOVE ' '                     TO SAUT.                                
      *                                                                         
      *                                                                         
      *    MOVE ' '               TO SAUT.                                      
      *    MOVE IGP805-E20        TO LIGNE.                                     
      *    WRITE ENREG-IGP805 FROM LIGNE-IGP805.                                
      *    ADD 1 TO CTR-LIGNE.                                                  
           IF NB-LIGNE-BLOC2 > 0                                                
              GO TO FIN-ECRITURE-ENTETE-PRE                                     
           END-IF.                                                              
           MOVE IGP805-E30              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP805-E35              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP805-E40              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP805-E45              TO LIGNE.                               
           WRITE ENREG-IGP805           FROM LIGNE-IGP805.                      
           ADD 1 TO CTR-LIGNE.                                                  
       FIN-ECRITURE-ENTETE-PRE. EXIT.                                           
      *------------------------------                                           
      *                                                                         
       ECRITURE-DETAIL SECTION.                                                 
      *------------------------                                                 
           IF RUPTURE > 4                                                       
              MOVE FGP805-NCODIC        TO IGP805-D05-NCODIC                    
              MOVE FGP805-LREFFOURN     TO IGP805-D10-LREFFOURN                 
              MOVE FGP805-CFAM          TO IGP805-D05-CFAM                      
              MOVE FGP805-CMARQ         TO IGP805-D05-CMARQ                     
              MOVE FGP805-LSTATCOMP     TO IGP805-D05-LSTATCOMP                 
              MOVE FGP805-WOA           TO IGP805-D05-WOA                       
              MOVE FGP805-SENSAPPRO     TO IGP805-D05-WSENSAPPRO                
              MOVE FGP805-V4S           TO W-MASKED5S                           
              MOVE W-MASKED5S           TO IGP805-D05-Q4S                       
              MOVE FGP805-QS-0          TO W-MASKED4S                           
2705          MOVE W-MASKED4S           TO IGP805-D05-QS-0                      
              MOVE FGP805-QS-1          TO W-MASKED4S                           
              MOVE W-MASKED4S           TO IGP805-D05-QS-1                      
              MOVE FGP805-QS-2          TO W-MASKED4S                           
              MOVE W-MASKED4S           TO IGP805-D05-QS-2                      
              MOVE FGP805-QS-3          TO W-MASKED4S                           
              MOVE W-MASKED4S           TO IGP805-D05-QS-3                      
              MOVE FGP805-QS-4          TO W-MASKED4S                           
              MOVE W-MASKED4S           TO IGP805-D05-QS-4                      
              MOVE FGP805-NBMAG         TO W-MASKED3                            
              MOVE W-MASKED3            TO IGP805-D05-NBMAG                     
              MOVE FGP805-QSTOCKMAG     TO W-MASKED5                            
              MOVE W-MASKED5            TO IGP805-D05-QSTOCKMAG                 
              MOVE FGP805-SORTIES7J     TO W-MASKED4                            
              MOVE W-MASKED4            TO IGP805-D05-SORTIES7J                 
              MOVE FGP805-COUV7J        TO W-MASKED5V                           
              MOVE W-MASKED5V           TO IGP805-D05-COUV7J                    
           ELSE                                                                 
              MOVE SPACES               TO IGP805-D05-NCODIC                    
                                           IGP805-D05-CFAM                      
                                           IGP805-D05-CMARQ                     
                                           IGP805-D05-LSTATCOMP                 
                                           IGP805-D05-WOA                       
                                           IGP805-D05-WSENSAPPRO                
                                           IGP805-D05-Q4S                       
                                           IGP805-D05-QS-0                      
                                           IGP805-D05-QS-1                      
                                           IGP805-D05-QS-2                      
                                           IGP805-D05-QS-3                      
                                           IGP805-D05-QS-4                      
                                           IGP805-D05-NBMAG                     
                                           IGP805-D05-QSTOCKMAG                 
                                           IGP805-D05-SORTIES7J                 
                                           IGP805-D05-COUV7J                    
           END-IF                                                               
           IF RUPTURE > 2                                                       
              MOVE FGP805-QSTOCKDIS       TO W-MASKED5                          
              MOVE W-MASKED5              TO IGP805-D05-QSTOCKDIS               
              MOVE FGP805-QSTOCKRES       TO W-MASKED5                          
              MOVE W-MASKED5              TO IGP805-D05-QSTOCKRES               
              MOVE FGP805-NDEPOT          TO IGP805-D05-NDEPOT                  
           ELSE                                                                 
              MOVE SPACES                 TO IGP805-D05-QSTOCKDIS               
                                             IGP805-D05-QSTOCKRES               
                                             IGP805-D05-NDEPOT                  
           END-IF                                                               
           IF RUPTURE > 1                                                       
              MOVE FGP805-STATUT          TO IGP805-D05-CSTATUT                 
              MOVE FGP805-DCDE-MMJJ(1:2)  TO IGP805-D05-DCDE4(3:2)              
              MOVE FGP805-DCDE-MMJJ(3:2)  TO IGP805-D05-DCDE4(1:2)              
           ELSE                                                                 
              MOVE SPACES                 TO IGP805-D05-CSTATUT                 
              MOVE SPACES                 TO IGP805-D05-DCDE4                   
           END-IF.                                                              
           MOVE  FGP805-QCDE              TO W-MASKED4                          
           MOVE  W-MASKED4                TO IGP805-D05-QCDE                    
           MOVE  FGP805-QCDERES           TO W-MASKED4                          
           MOVE  W-MASKED4                TO IGP805-D05-QCDERES                 
           IF NB-BLOC = 1                                                       
              IF RUPTURE < 3                                                    
                 MOVE IGP805-D05-CSTATUT  TO IGP805-D10-CSTATUT                 
                 MOVE IGP805-D05-DCDE4    TO IGP805-D10-DCDE4                   
                 MOVE IGP805-D05-QCDE     TO IGP805-D10-QCDE                    
                 MOVE IGP805-D05-QCDERES  TO IGP805-D10-QCDERES                 
                 MOVE ' '                 TO SAUT                               
                 MOVE IGP805-D10          TO LIGNE                              
                 INITIALIZE  IGP805-D10-CSTATUT                                 
                             IGP805-D10-DCDE4                                   
                             IGP805-D10-QCDE                                    
                             IGP805-D10-QCDERES                                 
              ELSE                                                              
                 MOVE SPACES              TO IGP805-D10-CSTATUT                 
                 MOVE SPACES              TO IGP805-D10-DCDE4                   
                 MOVE SPACES              TO IGP805-D10-QCDE                    
                 MOVE SPACES              TO IGP805-D10-QCDERES                 
                 MOVE ' '                 TO SAUT                               
                 MOVE IGP805-D10          TO LIGNE                              
                 PERFORM ECRITURE-BLOC                                          
                 MOVE ' '                 TO SAUT                               
                 MOVE IGP805-D05          TO LIGNE                              
              END-IF                                                            
           ELSE                                                                 
              MOVE ' '                    TO SAUT                               
              MOVE IGP805-D05             TO LIGNE                              
           END-IF                                                               
           PERFORM ECRITURE-BLOC.                                               
       FIN-ECRITURE-DETAIL. EXIT.                                               
      *--------------------------                                               
       CUMUL-TOTAUX SECTION.                                                    
      *---------------------                                                    
            PERFORM REMPLISSAGE-TAB-T1                                          
            PERFORM REMPLISSAGE-TAB-T2                                          
            PERFORM REMPLISSAGE-TAB-T3.                                         
       FIN-CUMUL-TOTAUX. EXIT.                                                  
      *-----------------------                                                  
      *                                                                         
      *                                                                         
       BLOC-CODIC  SECTION.                                                     
      *--------------------                                                     
           IF NB-LIGNE-BLOC > 0                                                 
      * LORSQU'IL N'Y A QU'UNE LIGNE DANS LE BLOC-CODIC IL FAUT RAJOUTER        
      * UNE 2EME LIGNE POUR LA R�F�RENCE PRODUIT                                
              IF NB-BLOC = 1                                                    
                 MOVE IGP805-D10                   TO LIGNE                     
                 MOVE ' '                          TO SAUT                      
                 PERFORM ECRITURE-BLOC                                          
              END-IF                                                            
              IF ((NB-LIGNE-BLOC + CTR-LIGNE) > (MAX-LIGNE))                    
                  MOVE ' ' TO SAUT                                              
                  INITIALIZE LIGNE                                              
                  MOVE '** SUITE **'               TO LIGNE                     
                  MOVE MAX-LIGNE                   TO CTR-LIGNE                 
                  PERFORM ECRITURE-LIGNE-SUI                                    
              END-IF                                                            
              PERFORM VARYING I-BLOC FROM 1 BY 1                                
                      UNTIL I-BLOC > NB-BLOC                                    
                      MOVE LIGNE-CODIC(I-BLOC)     TO LIGNE-IGP805              
                      IF SAUT = '+'                                             
                         SUBTRACT 1 FROM CTR-LIGNE                              
                      END-IF                                                    
                      PERFORM ECRITURE-LIGNE-SUI                                
              END-PERFORM                                                       
              INITIALIZE W-BLOC NB-BLOC NB-LIGNE-BLOC                           
              MOVE ' '                             TO SAUT                      
              MOVE IGP805-T20                      TO LIGNE                     
              PERFORM ECRITURE-LIGNE-SUI                                        
           END-IF.                                                              
       FIN-BLOC-CODIC. EXIT.                                                    
      *-----------------------                                                  
      *                                                                         
       ECRITURE-T1 SECTION.                                                     
      *--------------------                                                     
           IF NB-LIGNE-BLOC2 > 0                                                
              IF ((NB-LIGNE-BLOC2 + CTR-LIGNE) > (MAX-LIGNE))                   
                  MOVE ' ' TO SAUT                                              
                  INITIALIZE LIGNE                                              
                  MOVE '** SUITE **'                TO LIGNE                    
                  MOVE MAX-LIGNE                    TO CTR-LIGNE                
                  PERFORM ECRITURE-LIGNE-SUI                                    
              END-IF                                                            
              PERFORM VARYING I-BLOC FROM 1 BY 1                                
                      UNTIL I-BLOC > NB-BLOC2                                   
                      MOVE LIGNE-TOTAL(I-BLOC)      TO LIGNE-IGP805             
                      IF SAUT = '+'                                             
                         SUBTRACT 1 FROM CTR-LIGNE                              
                      END-IF                                                    
                      PERFORM ECRITURE-LIGNE-SUI                                
              END-PERFORM                                                       
              INITIALIZE W-BLOC2 NB-BLOC2 NB-LIGNE-BLOC2                        
           END-IF.                                                              
       FIN-ECRITURE-T1. EXIT.                                                   
      *----------------------                                                   
      *----------------- REMPLISSAGE DES TABLEAUX DE TOTAUX ---------           
      *                                                                         
       REMPLISSAGE-TAB-T1 SECTION.                                              
      *---------------------------                                              
           PERFORM VARYING I-T1 FROM 1 BY 1                                     
                   UNTIL ( I-T1 > NB-T1 )                                       
                   OR    ( W-T1-NSOCDEPOT (I-T1)    = FGP805-NSOCDEP            
                   AND     W-T1-NDEPOT     (I-T1)   = FGP805-NDEPOT             
                   AND     W-T1-CFAM       (I-T1)   = FGP805-CFAM )             
           END-PERFORM.                                                         
           IF (I-T1 > NB-T1)                                                    
                 ADD 1 TO NB-T1                                                 
                 IF NB-T1 > MAX-T1                                              
                    MOVE 'TAB T1 TROP PETIT' TO ABEND-MESS                      
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP805-CFAM            TO W-T1-CFAM        (NB-T1)        
                 MOVE FGP805-NSOCDEP         TO W-T1-NSOCDEPOT   (NB-T1)        
                 MOVE FGP805-NDEPOT          TO W-T1-NDEPOT      (NB-T1)        
                 MOVE NB-T1                  TO I-T1                            
           END-IF.                                                              
           IF RUPTURE > 2                                                       
              ADD FGP805-QSTOCKDIS           TO W-T1-TFQSTOCKDIS  (I-T1)        
              ADD FGP805-QSTOCKRES           TO W-T1-TFQSTOCKRES  (I-T1)        
           END-IF                                                               
           IF RUPTURE > 4                                                       
              ADD FGP805-V4S                 TO W-T1-TFQ4S        (I-T1)        
              ADD FGP805-QS-0                TO W-T1-TFQS-0       (I-T1)        
              ADD FGP805-QS-1                TO W-T1-TFQS-1       (I-T1)        
              ADD FGP805-QS-2                TO W-T1-TFQS-2       (I-T1)        
              ADD FGP805-QS-3                TO W-T1-TFQS-3       (I-T1)        
              ADD FGP805-QS-4                TO W-T1-TFQS-4       (I-T1)        
              ADD FGP805-QSTOCKMAG           TO W-T1-TFQSTOCKMAG  (I-T1)        
              ADD FGP805-SORTIES7J           TO W-T1-TSORTIES7J   (I-T1)        
              IF  FGP805-COUV7J < 999,9                                         
                  ADD FGP805-COUV7J          TO W-T1-TCOUV7J      (I-T1)        
                  ADD 1                      TO W-T1-TNBCOUV      (I-T1)        
              END-IF                                                            
           END-IF                                                               
           IF RUPTURE > 1                                                       
              ADD FGP805-QCDE                TO W-T1-TFQCDE       (I-T1)        
              ADD FGP805-QCDERES             TO W-T1-TFQCDERES    (I-T1)        
              MOVE FGP805-TRIDEP             TO W-T1-TRIDEP       (I-T1)        
           END-IF.                                                              
       FIN-REMPLISSAGE-TAB-T1. EXIT.                                            
      *-----------------------------                                            
       REMPLISSAGE-TAB-T2 SECTION.                                              
      *-----------------------------                                            
           PERFORM VARYING I-T2 FROM 1 BY 1                                     
                   UNTIL ( I-T2 > NB-T2 )                                       
                   OR    ( W-T2-NSOCDEPOT (I-T2)    = FGP805-NSOCDEP            
                   AND     W-T2-NDEPOT     (I-T2)   = FGP805-NDEPOT             
                   AND     W-T2-RAYON      (I-T2)   = FGP805-RAYON )            
           END-PERFORM.                                                         
           IF (I-T2 > NB-T2)                                                    
                 ADD 1 TO NB-T2                                                 
                 IF NB-T2 > MAX-T2                                              
                    MOVE 'TAB T2 TROP PETIT' TO ABEND-MESS                      
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP805-RAYON           TO W-T2-RAYON       (NB-T2)        
                 MOVE FGP805-NSOCDEP         TO W-T2-NSOCDEPOT   (NB-T2)        
                 MOVE FGP805-NDEPOT          TO W-T2-NDEPOT      (NB-T2)        
                 MOVE NB-T2                  TO I-T2                            
           END-IF.                                                              
           IF RUPTURE > 2                                                       
              ADD FGP805-QSTOCKDIS           TO W-T2-TFQSTOCKDIS  (I-T2)        
              ADD FGP805-QSTOCKRES           TO W-T2-TFQSTOCKRES  (I-T2)        
           END-IF                                                               
           IF RUPTURE > 4                                                       
              ADD FGP805-V4S                 TO W-T2-TFQ4S        (I-T2)        
              ADD FGP805-QS-0                TO W-T2-TFQS-0       (I-T2)        
              ADD FGP805-QS-1                TO W-T2-TFQS-1       (I-T2)        
              ADD FGP805-QS-2                TO W-T2-TFQS-2       (I-T2)        
              ADD FGP805-QS-3                TO W-T2-TFQS-3       (I-T2)        
              ADD FGP805-QS-4                TO W-T2-TFQS-4       (I-T2)        
              ADD FGP805-QSTOCKMAG           TO W-T2-TFQSTOCKMAG  (I-T2)        
              ADD FGP805-SORTIES7J           TO W-T2-TSORTIES7J   (I-T2)        
              IF  FGP805-COUV7J < 999,9                                         
                  ADD FGP805-COUV7J          TO W-T2-TCOUV7J      (I-T2)        
                  ADD 1                      TO W-T2-TNBCOUV      (I-T2)        
              END-IF                                                            
           END-IF                                                               
           IF RUPTURE > 1                                                       
              ADD FGP805-QCDE                TO W-T2-TFQCDE       (I-T2)        
              ADD FGP805-QCDERES             TO W-T2-TFQCDERES    (I-T2)        
              MOVE FGP805-TRIDEP             TO W-T2-TRIDEP       (I-T2)        
           END-IF.                                                              
       FIN-REMPLISSAGE-TAB-T2. EXIT.                                            
      *-----------------------------                                            
      *                                                                         
       REMPLISSAGE-TAB-T3 SECTION.                                              
      *-----------------------------                                            
      *                                                                         
           MOVE FGP805-NSOCDEPGEN            TO  FGP805-ENTREPOT (1:3)          
           MOVE FGP805-NDEPOTGEN             TO  FGP805-ENTREPOT (4:3)          
           PERFORM VARYING I-T3 FROM 1 BY 1                                     
                   UNTIL ( I-T3 > NB-T3 )                                       
                   OR    ( W-T3-NSOCDEPOT  (I-T3)   = FGP805-NSOCDEP            
                   AND     W-T3-NDEPOT     (I-T3)   = FGP805-NDEPOT             
                   AND     W-T3-ENTREPOT   (I-T3)   = FGP805-ENTREPOT)          
           END-PERFORM.                                                         
           IF (I-T3 > NB-T3)                                                    
                 ADD 1 TO NB-T3                                                 
                 IF NB-T3 > MAX-T3                                              
                    MOVE 'TAB T3 TROP PETIT' TO ABEND-MESS                      
                    PERFORM FIN-ANORMALE                                        
                 END-IF                                                         
                 MOVE FGP805-ENTREPOT        TO W-T3-ENTREPOT    (NB-T3)        
                 MOVE FGP805-NSOCDEP         TO W-T3-NSOCDEPOT   (NB-T3)        
                 MOVE FGP805-NDEPOT          TO W-T3-NDEPOT      (NB-T3)        
                 MOVE NB-T3                  TO I-T3                            
           END-IF.                                                              
           IF RUPTURE > 2                                                       
              ADD FGP805-QSTOCKDIS           TO W-T3-TFQSTOCKDIS  (I-T3)        
              ADD FGP805-QSTOCKRES           TO W-T3-TFQSTOCKRES  (I-T3)        
           END-IF                                                               
           IF RUPTURE > 4                                                       
              ADD FGP805-V4S                 TO W-T3-TFQ4S        (I-T3)        
              ADD FGP805-QS-0                TO W-T3-TFQS-0       (I-T3)        
              ADD FGP805-QS-1                TO W-T3-TFQS-1       (I-T3)        
              ADD FGP805-QS-2                TO W-T3-TFQS-2       (I-T3)        
              ADD FGP805-QS-3                TO W-T3-TFQS-3       (I-T3)        
              ADD FGP805-QS-4                TO W-T3-TFQS-4       (I-T3)        
              ADD FGP805-QSTOCKMAG           TO W-T3-TFQSTOCKMAG  (I-T3)        
              ADD FGP805-SORTIES7J           TO W-T3-TSORTIES7J   (I-T3)        
              IF  FGP805-COUV7J < 999,9                                         
                  ADD FGP805-COUV7J          TO W-T3-TCOUV7J      (I-T3)        
                  ADD 1                      TO W-T3-TNBCOUV      (I-T3)        
              END-IF                                                            
           END-IF                                                               
           IF RUPTURE > 1                                                       
              ADD FGP805-QCDE                TO W-T3-TFQCDE       (I-T3)        
              ADD FGP805-QCDERES             TO W-T3-TFQCDERES    (I-T3)        
              MOVE FGP805-TRIDEP             TO W-T3-TRIDEP       (I-T3)        
           END-IF.                                                              
       FIN-REMPLISSAGE-TAB-T3. EXIT.                                            
      *-----------------------------                                            
      *                                                                         
       ECRITURE-TAB-T1 SECTION.                                                 
      *------------------------                                                 
           IF NB-T1 > 0                                                         
              INITIALIZE W1-CLE-PREC SOMME-STOCK                                
      * TRI DES LIGNES DE TOTAUX PAR DEP GEN ET DEP GRO                         
              PERFORM TRI-TAB-T1                                                
              INITIALIZE W1-CLE-PREC SOMME-STOCK                                
              MOVE 1 TO I-T1                                                    
              ADD W-T1-TFQSTOCKDIS(I-T1) TO SOMME-STOCK                         
              PERFORM VARYING I-T1 FROM 1 BY 1                                  
                      UNTIL I-T1 > NB-T1                                        
                      COMPUTE I = I-T1 - 1                                      
                      IF W-T1-CFAM (I-T1) = W-T1-CFAM (I)                       
                         ADD W-T1-TFQSTOCKDIS(I-T1) TO SOMME-STOCK              
                      END-IF                                                    
              END-PERFORM                                                       
      * ON NE VEUT AFFICHER LE TOTAL PAR FAMILLE QUE S'IL Y A DU STOCK          
      * SUR UN ENTREPOT GROSSISTE                                               
              IF SOMME-STOCK > 0                                                
                 MOVE ' '             TO SAUT                                   
                 MOVE IGP805-T        TO LIGNE                                  
                 PERFORM ECRITURE-BLOC-2                                        
                 MOVE ' '             TO SAUT                                   
                 MOVE IGP805-T1-E1    TO LIGNE                                  
                 PERFORM ECRITURE-BLOC-2                                        
                 PERFORM VARYING I-T1 FROM 1 BY 1                               
                         UNTIL I-T1 > NB-T1                                     
      ** CHANGEMENT DE CODE FAMILLE                                             
                    IF W-T1-CFAM (I-T1) NOT = W1-CFAM                           
                       PERFORM VARYING I FROM I-T1 BY 1                         
                            UNTIL I > NB-T1 OR                                  
                             W-T1-CFAM (I) NOT =  W-T1-CFAM (I-T1)              
                             IF I > I-T1                                        
                                ADD W-T1-TFQSTOCKDIS (I) TO SOMME-STOCK         
                             ELSE                                               
                                INITIALIZE SOMME-STOCK                          
                             END-IF                                             
                       END-PERFORM                                              
                       IF SOMME-STOCK > 0                                       
                          MOVE ' '                   TO SAUT                    
                          MOVE IGP805-T1-E2          TO LIGNE                   
                          PERFORM ECRITURE-BLOC-2                               
                          MOVE 'CFAM: '              TO IGP805-T1-LIBFAM        
                          MOVE W-T1-CFAM (I-T1)      TO IGP805-T1-CFAM          
                          MOVE W-T1-TFQ4S     (I-T1) TO W-MASKED7S              
                          MOVE W-MASKED7S            TO IGP805-T1-TFQ4S         
                          MOVE W-T1-TFQS-0    (I-T1) TO W-MASKED6S              
                          MOVE W-MASKED6S            TO IGP805-T1-TFQS-0        
                          MOVE W-T1-TFQS-1    (I-T1) TO W-MASKED6S              
                          MOVE W-MASKED6S            TO IGP805-T1-TFQS-1        
                          MOVE W-T1-TFQS-2    (I-T1) TO W-MASKED6S              
                          MOVE W-MASKED6S            TO IGP805-T1-TFQS-2        
                          MOVE W-T1-TFQS-3    (I-T1) TO W-MASKED6S              
                          MOVE W-MASKED6S            TO IGP805-T1-TFQS-3        
                          MOVE W-T1-TFQS-4    (I-T1) TO W-MASKED6S              
                          MOVE W-MASKED6S            TO IGP805-T1-TFQS-4        
                          MOVE W-T1-TFQSTOCKMAG (I-T1)                          
                                                TO W-MASKED7                    
                          MOVE W-MASKED7                                        
                                                TO IGP805-T1-TFQSTOCKMAG        
                          MOVE W-T1-TSORTIES7J  (I-T1)                          
                                                TO W-MASKED7                    
                          MOVE W-MASKED7                                        
                                                TO IGP805-T1-TSORTIES7J         
                          IF W-T1-TNBCOUV(I-T1) > 0                             
                             COMPUTE W-T1-TCOUV7J(I-T1)                         
                             =  W-T1-TCOUV7J(I-T1) / W-T1-TNBCOUV(I-T1)         
                          ELSE                                                  
                             MOVE 999,9         TO  W-T1-TCOUV7J (I-T1)         
                          END-IF                                                
                          MOVE W-T1-TCOUV7J    (I-T1)                           
                                                TO W-MASKED5V                   
                          MOVE W-MASKED5V       TO IGP805-T1-TCOUV7J            
                          MOVE W-T1-NDEPOT     (I-T1)                           
                                                TO IGP805-T1-NDEPOT             
                          MOVE W-T1-TFQSTOCKDIS(I-T1)                           
                                                TO W-MASKED7                    
                          MOVE W-MASKED7        TO IGP805-T1-TFQSTOCKDIS        
                          MOVE W-T1-TFQSTOCKRES(I-T1)                           
                                                TO W-MASKED6                    
                          MOVE W-MASKED6        TO IGP805-T1-TFQSTOCKRES        
                          MOVE W-T1-TFQCDE     (I-T1)                           
                                                TO W-MASKED7                    
                          MOVE W-MASKED7        TO IGP805-T1-TFQCDE             
                          MOVE W-T1-TFQCDERES  (I-T1)                           
                                                TO W-MASKED6                    
                          MOVE W-MASKED6        TO IGP805-T1-TFQCDERES          
                          MOVE ' '              TO SAUT                         
                          MOVE IGP805-T1        TO LIGNE                        
                          PERFORM ECRITURE-BLOC-2                               
                       END-IF                                                   
                    ELSE                                                        
                       MOVE SPACES TO IGP805-T1-CFAM                            
                       MOVE SPACES TO IGP805-T1-LIBFAM                          
                                      IGP805-T1-TSORTIES7J                      
                                      IGP805-T1-TCOUV7J                         
                                      IGP805-T1-TFQSTOCKMAG                     
                                      IGP805-T1-TFQ4S                           
                                      IGP805-T1-TFQS-0                          
                                      IGP805-T1-TFQS-1                          
                                      IGP805-T1-TFQS-2                          
                                      IGP805-T1-TFQS-3                          
                                      IGP805-T1-TFQS-4                          
                       IF   W-T1-TFQSTOCKDIS   (I-T1) > 0                       
                          MOVE W-T1-NDEPOT     (I-T1)                           
                                                TO IGP805-T1-NDEPOT             
                          MOVE W-T1-TFQSTOCKDIS(I-T1)                           
                                                TO W-MASKED7                    
                          MOVE W-MASKED7        TO IGP805-T1-TFQSTOCKDIS        
                          MOVE W-T1-TFQSTOCKRES(I-T1)                           
                                                TO W-MASKED6                    
                          MOVE W-MASKED6        TO IGP805-T1-TFQSTOCKRES        
                          MOVE W-T1-TFQCDE     (I-T1)                           
                                                TO W-MASKED7                    
                          MOVE W-MASKED7        TO IGP805-T1-TFQCDE             
                          MOVE W-T1-TFQCDERES  (I-T1)                           
                                                TO W-MASKED6                    
                          MOVE W-MASKED6        TO IGP805-T1-TFQCDERES          
                          MOVE ' '              TO SAUT                         
                          MOVE IGP805-T1        TO LIGNE                        
                          PERFORM ECRITURE-BLOC-2                               
                       END-IF                                                   
                    END-IF                                                      
                    MOVE SPACES                 TO IGP805-T1-CFAM               
                    MOVE SPACES                 TO IGP805-T1-LIBFAM             
                    MOVE W-T1-CFAM       (I-T1) TO W1-CFAM                      
                    MOVE W-T1-TRIDEP     (I-T1) TO W1-TRIDEP                    
                    MOVE W-T1-NSOCDEPOT  (I-T1) TO W1-NSOCDEPOT                 
                    MOVE W-T1-NDEPOT     (I-T1) TO W1-NDEPOT                    
                 END-PERFORM                                                    
                 MOVE ' '         TO SAUT                                       
                 MOVE IGP805-T    TO LIGNE                                      
                 PERFORM ECRITURE-BLOC-2                                        
             END-IF                                                             
           END-IF.                                                              
           INITIALIZE TAB-CUMUL-T1 NB-T1.                                       
       FIN-ECRITURE-TAB-T1. EXIT.                                               
      *--------------------------                                               
      *                                                                         
       ECRITURE-TAB-T2 SECTION.                                                 
      *------------------------                                                 
           IF NB-T2 > 0                                                         
      * TRI DES LIGNES DE TOTAUX PAR DEP GEN ET DEP GRO                         
              PERFORM TRI-TAB-T2                                                
              INITIALIZE W2-CLE-PREC                                            
              MOVE ' '         TO SAUT                                          
              MOVE IGP805-T    TO LIGNE                                         
              PERFORM ECRITURE-BLOC-2                                           
              MOVE ' '         TO SAUT                                          
              MOVE IGP805-T2-E1    TO LIGNE                                     
              PERFORM ECRITURE-BLOC-2                                           
              PERFORM VARYING I-T2 FROM 1 BY 1                                  
                    UNTIL I-T2 > NB-T2                                          
                    IF W-T2-RAYON (I-T2) NOT = W2-RAYON                         
                       MOVE ' '         TO SAUT                                 
                       MOVE IGP805-T2-E2    TO LIGNE                            
                       PERFORM ECRITURE-BLOC-2                                  
                       MOVE 'RAYON:'                TO IGP805-T2-LIBRAY         
                       MOVE W-T2-RAYON  (I-T2)      TO IGP805-T2-RAYON          
                       MOVE W-T2-TFQ4S       (I-T2) TO W-MASKED7S               
                       MOVE W-MASKED7S              TO IGP805-T2-TFQ4S          
                       MOVE W-T2-TFQS-0      (I-T2) TO W-MASKED6S               
                       MOVE W-MASKED6S              TO IGP805-T2-TFQS-0         
                       MOVE W-T2-TFQS-1      (I-T2) TO W-MASKED6S               
                       MOVE W-MASKED6S              TO IGP805-T2-TFQS-1         
                       MOVE W-T2-TFQS-2      (I-T2) TO W-MASKED6S               
                       MOVE W-MASKED6S              TO IGP805-T2-TFQS-2         
                       MOVE W-T2-TFQS-3      (I-T2) TO W-MASKED6S               
                       MOVE W-MASKED6S              TO IGP805-T2-TFQS-3         
                       MOVE W-T2-TFQS-4      (I-T2) TO W-MASKED6S               
                       MOVE W-MASKED6S              TO IGP805-T2-TFQS-4         
                       MOVE W-T2-TFQSTOCKMAG (I-T2) TO W-MASKED7                
                       MOVE W-MASKED7               TO                          
                                                   IGP805-T2-TFQSTOCKMAG        
                       MOVE W-T2-TSORTIES7J  (I-T2) TO W-MASKED7                
                       MOVE W-MASKED7               TO                          
                                                    IGP805-T2-TSORTIES7J        
                       IF W-T2-TNBCOUV(I-T2) > 0                                
                          COMPUTE W-T2-TCOUV7J(I-T2)                            
                          =  W-T2-TCOUV7J(I-T2) / W-T2-TNBCOUV(I-T2)            
                       ELSE                                                     
                          MOVE 999,9      TO  W-T2-TCOUV7J    (I-T2)            
                       END-IF                                                   
                       MOVE W-T2-TCOUV7J     (I-T2) TO W-MASKED5V               
                       MOVE W-MASKED5V              TO IGP805-T2-TCOUV7J        
                    ELSE                                                        
                       MOVE SPACES TO IGP805-T2-RAYON                           
                       MOVE SPACES TO IGP805-T2-LIBRAY                          
                                      IGP805-T2-TSORTIES7J                      
                                      IGP805-T2-TCOUV7J                         
                                      IGP805-T2-TFQSTOCKMAG                     
                                      IGP805-T2-TFQ4S                           
                                      IGP805-T2-TFQS-0                          
                                      IGP805-T2-TFQS-1                          
                                      IGP805-T2-TFQS-2                          
                                      IGP805-T2-TFQS-3                          
                                      IGP805-T2-TFQS-4                          
                    END-IF                                                      
                    MOVE W-T2-NDEPOT     (I-T2) TO IGP805-T2-NDEPOT             
                    MOVE W-T2-TFQSTOCKDIS(I-T2) TO W-MASKED7                    
                    MOVE W-MASKED7              TO IGP805-T2-TFQSTOCKDIS        
                    MOVE W-T2-TFQSTOCKRES(I-T2) TO W-MASKED6                    
                    MOVE W-MASKED6              TO IGP805-T2-TFQSTOCKRES        
                    MOVE W-T2-TFQCDE     (I-T2) TO W-MASKED7                    
                    MOVE W-MASKED7              TO IGP805-T2-TFQCDE             
                    MOVE W-T2-TFQCDERES  (I-T2) TO W-MASKED6                    
                    MOVE W-MASKED6              TO IGP805-T2-TFQCDERES          
                    MOVE ' '                    TO SAUT                         
                    MOVE IGP805-T2              TO LIGNE                        
                    PERFORM ECRITURE-BLOC-2                                     
                    MOVE SPACES                 TO IGP805-T2-RAYON              
                    MOVE SPACES                 TO IGP805-T2-LIBRAY             
                    MOVE W-T2-RAYON      (I-T2) TO W2-RAYON                     
                    MOVE W-T2-TRIDEP     (I-T2) TO W2-TRIDEP                    
                    MOVE W-T2-NSOCDEPOT  (I-T2) TO W2-NSOCDEPOT                 
                    MOVE W-T2-NDEPOT     (I-T2) TO W2-NDEPOT                    
              END-PERFORM                                                       
              MOVE ' '         TO SAUT                                          
              MOVE IGP805-T    TO LIGNE                                         
              PERFORM ECRITURE-BLOC-2                                           
           END-IF.                                                              
           INITIALIZE TAB-CUMUL-T2 NB-T2 .                                      
       FIN-ECRITURE-TAB-T2. EXIT.                                               
      *--------------------------                                               
      *                                                                         
       ECRITURE-TAB-T3 SECTION.                                                 
      *------------------------                                                 
           IF NB-T3 > 0                                                         
      * TRI DES LIGNES DE TOTAUX PAR DEP GEN ET DEP GRO                         
              PERFORM TRI-TAB-T3                                                
              INITIALIZE W2-CLE-PREC                                            
              MOVE ' '             TO SAUT                                      
              MOVE IGP805-T        TO LIGNE                                     
              PERFORM ECRITURE-BLOC-2                                           
              MOVE ' '             TO SAUT                                      
              MOVE IGP805-T3-E1    TO LIGNE                                     
              PERFORM ECRITURE-BLOC-2                                           
              PERFORM VARYING I-T3 FROM 1 BY 1                                  
                    UNTIL I-T3 > NB-T3                                          
                    IF W-T3-ENTREPOT(I-T3) NOT = W3-ENTREPOT                    
                       MOVE ' '                      TO SAUT                    
                       MOVE IGP805-T3-E2             TO LIGNE                   
                       PERFORM ECRITURE-BLOC-2                                  
                       MOVE 'ENT: '                  TO IGP805-T3-LIBENT        
                       MOVE W-T3-ENTREPOT(I-T3)      TO IGP805-T3-ENT           
                       MOVE W-T3-TFQ4S        (I-T3) TO W-MASKED7S              
                       MOVE W-MASKED7S               TO IGP805-T3-TFQ4S         
                       MOVE W-T3-TFQS-0       (I-T3) TO W-MASKED6S              
                       MOVE W-MASKED6S               TO IGP805-T3-TFQS-0        
                       MOVE W-T3-TFQS-1       (I-T3) TO W-MASKED6S              
                       MOVE W-MASKED6S               TO IGP805-T3-TFQS-1        
                       MOVE W-T3-TFQS-2       (I-T3) TO W-MASKED6S              
                       MOVE W-MASKED6S               TO IGP805-T3-TFQS-2        
                       MOVE W-T3-TFQS-3       (I-T3) TO W-MASKED6S              
                       MOVE W-MASKED6S               TO IGP805-T3-TFQS-3        
                       MOVE W-T3-TFQS-4       (I-T3) TO W-MASKED6S              
                       MOVE W-MASKED6S               TO IGP805-T3-TFQS-4        
                       MOVE W-T3-TFQSTOCKMAG  (I-T3) TO W-MASKED7               
                       MOVE W-MASKED7                                           
                                                TO IGP805-T3-TFQSTOCKMAG        
                       MOVE W-T3-TSORTIES7J   (I-T3) TO W-MASKED7               
                       MOVE W-MASKED7                                           
                                                TO IGP805-T3-TSORTIES7J         
                       IF W-T3-TNBCOUV(I-T3) > 0                                
                          COMPUTE W-T3-TCOUV7J(I-T3)                            
                          =  W-T3-TCOUV7J(I-T3) / W-T3-TNBCOUV(I-T3)            
                       ELSE                                                     
                          MOVE 999,9            TO  W-T3-TCOUV7J (I-T3)         
                       END-IF                                                   
                       MOVE W-T3-TCOUV7J(I-T3)  TO W-MASKED5V                   
                       MOVE W-MASKED5V          TO IGP805-T3-TCOUV7J            
                    ELSE                                                        
                       MOVE SPACES TO IGP805-T3-ENT                             
                       MOVE SPACES TO IGP805-T3-LIBENT                          
                                      IGP805-T3-TSORTIES7J                      
                                      IGP805-T3-TCOUV7J                         
                                      IGP805-T3-TFQSTOCKMAG                     
                                      IGP805-T3-TFQ4S                           
                                      IGP805-T3-TFQS-0                          
                                      IGP805-T3-TFQS-1                          
                                      IGP805-T3-TFQS-2                          
                                      IGP805-T3-TFQS-3                          
                                      IGP805-T3-TFQS-4                          
                    END-IF                                                      
                    MOVE W-T3-NDEPOT     (I-T3) TO IGP805-T3-NDEPOT             
                    MOVE W-T3-TFQSTOCKDIS(I-T3) TO W-MASKED7                    
                    MOVE W-MASKED7              TO IGP805-T3-TFQSTOCKDIS        
                    MOVE W-T3-TFQSTOCKRES(I-T3) TO W-MASKED6                    
                    MOVE W-MASKED6              TO IGP805-T3-TFQSTOCKRES        
                    MOVE W-T3-TFQCDE     (I-T3) TO W-MASKED7                    
                    MOVE W-MASKED7              TO IGP805-T3-TFQCDE             
                    MOVE W-T3-TFQCDERES  (I-T3) TO W-MASKED6                    
                    MOVE W-MASKED6              TO IGP805-T3-TFQCDERES          
                    MOVE ' '                    TO SAUT                         
                    MOVE IGP805-T3              TO LIGNE                        
                    PERFORM ECRITURE-BLOC-2                                     
                    MOVE SPACES                 TO IGP805-T3-ENT                
                    MOVE SPACES                 TO IGP805-T3-LIBENT             
                    MOVE W-T3-ENTREPOT   (I-T3) TO W3-ENTREPOT                  
                    MOVE W-T3-TRIDEP     (I-T3) TO W3-TRIDEP                    
                    MOVE W-T3-NSOCDEPOT  (I-T3) TO W3-NSOCDEPOT                 
                    MOVE W-T3-NDEPOT     (I-T3) TO W3-NDEPOT                    
              END-PERFORM                                                       
              MOVE ' '         TO SAUT                                          
              MOVE IGP805-T    TO LIGNE                                         
              PERFORM ECRITURE-BLOC-2                                           
           END-IF.                                                              
           INITIALIZE TAB-CUMUL-T3 NB-T3 .                                      
       FIN-ECRITURE-TAB-T3. EXIT.                                               
      *                                                                         
       TRI-TAB-T1 SECTION.                                                      
      *------------------------                                                 
           SET FIN-TRI-KO TO TRUE                                               
           COMPUTE NB-T-1  = NB-T1 - 1                                          
           MOVE 1 TO I-T1                                                       
           PERFORM VARYING I-T1 FROM 1 BY 1                                     
                   UNTIL FIN-TRI-OK OR I-T1  > NB-T-1                           
                   SET FIN-TRI-OK TO TRUE                                       
                   MOVE 1 TO J-TRI                                              
                   PERFORM VARYING J-TRI FROM 1  BY 1                           
                           UNTIL J-TRI > NB-T-1                                 
                           COMPUTE K =  J-TRI + 1                               
                           IF  W-CLE-T1 (J-TRI) > W-CLE-T1 (K)                  
      *  LES RANGS DOIVENT ETRE INVERSES                                        
                               MOVE W-T1 (J-TRI)  TO TAMP-T1                    
                               MOVE W-T1 (K)      TO W-T1 (J-TRI)               
                               MOVE TAMP-T1       TO W-T1 (K)                   
                               SET FIN-TRI-KO TO TRUE                           
                           END-IF                                               
                   END-PERFORM                                                  
           END-PERFORM.                                                         
       FIN-TRI-TAB-T1. EXIT.                                                    
      *--------------------------                                               
      *                                                                         
       TRI-TAB-T2 SECTION.                                                      
      *------------------------                                                 
           SET FIN-TRI-KO TO TRUE                                               
           COMPUTE NB-T-1  = NB-T2 - 1                                          
           MOVE 1 TO I-T2                                                       
           PERFORM VARYING I-T2 FROM 1 BY 1                                     
                   UNTIL FIN-TRI-OK OR I-T2  > NB-T-1                           
                   SET FIN-TRI-OK TO TRUE                                       
                   MOVE 1 TO J-TRI                                              
                   PERFORM VARYING J-TRI FROM 1  BY 1                           
                           UNTIL J-TRI > NB-T-1                                 
                           COMPUTE K =  J-TRI + 1                               
                           IF  W-CLE-T2 (J-TRI) > W-CLE-T2 (K)                  
    ***  LES RANGS DOIVENT ETRE INVERSES                                        
                               MOVE W-T2 (J-TRI)  TO TAMP-T2                    
                               MOVE W-T2 (K)      TO W-T2 (J-TRI)               
                               MOVE TAMP-T2       TO W-T2 (K)                   
                               SET FIN-TRI-KO TO TRUE                           
                           END-IF                                               
                   END-PERFORM                                                  
           END-PERFORM.                                                         
       FIN-TRI-TAB-T2. EXIT.                                                    
      *--------------------------                                               
      *                                                                         
       TRI-TAB-T3 SECTION.                                                      
      *------------------------                                                 
           SET FIN-TRI-KO TO TRUE                                               
           COMPUTE NB-T-1  = NB-T3 - 1                                          
           MOVE 1 TO I-T3                                                       
           PERFORM VARYING I-T3 FROM 1 BY 1                                     
                   UNTIL FIN-TRI-OK OR I-T3  > NB-T-1                           
                   SET FIN-TRI-OK TO TRUE                                       
                   MOVE 1 TO J-TRI                                              
                   PERFORM VARYING J-TRI FROM 1  BY 1                           
                           UNTIL J-TRI > NB-T-1                                 
                           COMPUTE K =  J-TRI + 1                               
                           IF  W-CLE-T3 (J-TRI) > W-CLE-T3 (K)                  
      *  LES RANGS DOIVENT ETRE INVERSES                                        
                               MOVE W-T3 (J-TRI)  TO TAMP-T3                    
                               MOVE W-T3 (K)      TO W-T3 (J-TRI)               
                               MOVE TAMP-T3       TO W-T3 (K)                   
                               SET FIN-TRI-KO TO TRUE                           
                           END-IF                                               
                   END-PERFORM                                                  
           END-PERFORM.                                                         
       FIN-TRI-TAB-T3. EXIT.                                                    
      *--------------------------                                               
      *                                                                         
      * ECRITURE UNE LIGNE                                                      
      *                                                                         
       ECRITURE-LIGNE-SUI SECTION.                                              
      *---------------------------                                              
           WRITE ENREG-IGP805 FROM LIGNE-IGP805.                                
           ADD 1 TO CTR-LIGNE.                                                  
           IF CTR-LIGNE > MAX-LIGNE                                             
              PERFORM ECRITURE-ENTETE-PRE                                       
           END-IF.                                                              
       FIN-ECRITURE-LIGNE-SUI. EXIT.                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *         P A R A G R A P H E   F I N  A N O R M A L E          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FIN-ANORMALE SECTION.                                                    
      *-------------                                                            
           MOVE 'BGP806' TO ABEND-PROG.                                         
           CALL 'ABEND' USING                                                   
                        ABEND-PROG                                              
                        ABEND-MESS.                                             
      *                                                                         
       F-FIN-ANORMALE.                                                          
      *-------------                                                            
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
