      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.                     BMU212.                          00020000
       AUTHOR.  DSA054.                                                 00030000
                                                                        00043000
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : COMPARAISON DES QUOTAS EN M3                     *        
      *                  AUX REMPLISSAGES DES MUTATIONS DE J-4 A J+2   *        
      *  PROGRAMME  : BMU212                                           *        
      *  TITRE      : EDITION DE L'OCCUPATION DU CALENDRIER            *        
      *                            DES MUTATIONS                       *        
      *  CREATION   : 07/05/2001                                       *        
      *  PERIODICITE: JOURNALIERE                                      *        
      *  FONCTION   : EDITION D'UN ETAT IMU212 DU CALENDRIER DES MUT.  *        
      *                                                                *        
      ******************************************************************        
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *        
      ******************************************************************        
      *  DATE       : ...                                              *        
      *  AUTEUR     : ...                                              *        
      *  REPAIRE    : ...                                              *        
      *****************************************************************         
       ENVIRONMENT DIVISION.                                            00050000
       CONFIGURATION SECTION.                                           00060000
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00070000
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FMU211  ASSIGN TO FMU211 FILE STATUS IS ST-FMU211.            
      *--                                                                       
           SELECT FMU211  ASSIGN TO FMU211 FILE STATUS IS ST-FMU211             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FMU212  ASSIGN TO FMU212 FILE STATUS IS ST-FMU212.            
      *--                                                                       
           SELECT FMU212  ASSIGN TO FMU212 FILE STATUS IS ST-FMU212             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00090000
       FILE SECTION.                                                            
      *                                                                         
       FD  FMU211                                                               
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
      *                                                                         
       01  FMU211-ENR  PIC X(512).                                              
      *                                                                         
       FD  FMU212                                                               
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
      *                                                                         
       01  FMU212-ENR  PIC X(512).                                              
      ***************************************************************           
       WORKING-STORAGE SECTION.                                         00091000
      ***************************************************************           
       77  FILLER               PIC X(20) VALUE '**DEBUT DE WORKING**'. 00092000
                                                                        00094000
      *================================================================*00095000
      *    I N D I C E S                                               *00096000
      *================================================================*00097000
       01  NBRE-LUS-FMU211            PIC 9(9)            VALUE ZERO.   00181000
       01  NBRE-ECR-FMU212            PIC 9(9)            VALUE ZERO.   00181000
                                                                        00251000
      *================================================================*00230000
      *    Z O N E S   D E   T R A V A I L                             *00240000
      *================================================================*00250000
       COPY IMU211.                                                             
       COPY IMU212.                                                             
      **********************************************                            
      *   FLAG FICHIERS                                                         
      **********************************************                            
       01  ST-FMU211           PIC 99             VALUE 00.                     
           88  FMU211-NORMAL                      VALUES 00.                    
           88  FIN-FMU211                         VALUES 01.                    
       01  ST-FMU212           PIC 99             VALUE 00.                     
           88  FMU212-NORMAL                      VALUES 00.                    
           88  FIN-FMU212                         VALUES 01.                    
      *================================================================*00340000
      *     DESCRIPTION COPY AIDA                                               
      *================================================================*        
      *   SAUVEGARDE  DE ZONES IMPORTANTES EN CAS DE PLANTAGE                   
           COPY SYBWDIV0.                                                       
       01  Z-INOUT           PIC X(236)           VALUE SPACES.                 
      * ZONE D'INTERFACE POUR LA GESTION DES ERREURS NON RECOUVRABLES *         
           COPY SYBWERRO.                                                       
      *================================================================*00301000
      *    ZONE  PARAMETRES POUR LE PROGRAMME ABEND                    *00302000
      *================================================================*00303000
                                                                        00320000
           COPY ABENDCOP.                                               00330000
                                                                        00340000
      *===============================================================* 03461000
       PROCEDURE DIVISION.                                              03470000
      *===============================================================* 03471000
                                                                        03472000
      *===============================================================* 03651000
       MODULE-BMU212        SECTION.                                    03660000
      *===============================================================* 03661000
                                                                        03670000
           PERFORM INIT.                                                03680000
           PERFORM TRAITEMENT.                                                  
           PERFORM FIN.                                                         
      *===============================================================* 03651000
       INIT                 SECTION.                                    03660000
      *===============================================================* 03661000
           OPEN INPUT  FMU211                                                   
                OUTPUT FMU212                                                   
           DISPLAY  '************************************************'          
           DISPLAY  '**              PROGRAMME BMU212              **'          
           DISPLAY  '**        REMPLISSAGE DU CALENDRIER DES       **'          
           DISPLAY  '**                  MUTATIONS                 **'          
           DISPLAY  '**                                            **'          
           .                                                                    
       FIN-INIT.         EXIT.                                                  
      *===============================================================* 03511000
       TRAITEMENT                  SECTION.                             03660000
      *===============================================================* 03511000
      *--- 1ERE LECTURE FICHIER FMU211                                          
           PERFORM READ-FMU211.                                                 
      *---- BOUCLE LECTURE FICHIER FMU211 / ECRITURE FICHIER FMU212             
           PERFORM  UNTIL FIN-FMU211                                            
              INITIALIZE          DSECT-IMU212                                  
              MOVE FMU211-ENR  TO DSECT-IMU211                                  
              ADD 1            TO NBRE-LUS-FMU211                               
              PERFORM LOAD-FMU212                                               
              PERFORM WRITE-FMU212                                              
              PERFORM READ-FMU211                                               
           END-PERFORM.                                                         
       FIN-TRAITEMENT.                       EXIT.                              
      *===============================================================* 03651000
       FIN               SECTION.                                       03660000
      *===============================================================* 03661000
           CLOSE FMU211.                                                        
           CLOSE FMU212.                                                        
           DISPLAY '*************************************************'.         
           DISPLAY '*      PROGRAMME BMU212 TERMINE NORMALEMENT     *'.         
           DISPLAY '* NBRE LECTURES  FMU211 : ' NBRE-LUS-FMU211.                
           DISPLAY '* NBRE ECRITURES FMU212 : ' NBRE-ECR-FMU212.                
           DISPLAY '*************************************************'.         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ****************************************************************          
       READ-FMU211                           SECTION.                           
      ****************************************************************          
           READ FMU211 AT END SET FIN-FMU211   TO TRUE                          
           IF NOT FMU211-NORMAL                                                 
           AND NOT FIN-FMU211                                                   
              MOVE 'PB SUR FICHIER FMU211 STAT =' TO ABEND-MESS                 
              MOVE ST-FMU211                      TO ABEND-CODE                 
              GO TO FIN-ANORMALE                                                
           END-IF                                                               
           .                                                                    
       FIN-READ-FMU211. EXIT.                                                   
      *****************************************************************         
       LOAD-FMU212 SECTION.                                             03660000
      *================================================================*        
           MOVE  1                        TO IMU212-SEQUENCE                    
           MOVE  'IMU212'                 TO NOMETAT-IMU212                     
           MOVE  IMU211-CSELART           TO IMU212-CSELART                     
           MOVE  IMU211-NSOCENTR          TO IMU212-NSOCENTR                    
           MOVE  IMU211-DATEJ             TO IMU212-DATEJ                       
           MOVE  IMU211-DDEBUT            TO IMU212-DDEBUT                      
           MOVE  IMU211-LLIEU             TO IMU212-LLIEU                       
           MOVE  IMU211-LIBELIEU          TO IMU212-LIBELIEU                    
           MOVE  IMU211-NDEPOT            TO IMU212-NDEPOT                      
           MOVE  IMU211-NLIEU             TO IMU212-NLIEU                       
           MOVE  IMU211-QNBPIECES-4       TO IMU212-QNBPIECES-4                 
           MOVE  IMU211-QVOLUME-4         TO IMU212-QVOLUME-4                   
           MOVE  IMU211-QNBM3QUOTA-4      TO IMU212-QNBM3QUOTA-4                
           MOVE  IMU211-QNBPIECES-3       TO IMU212-QNBPIECES-3                 
           MOVE  IMU211-QVOLUME-3         TO IMU212-QVOLUME-3                   
           MOVE  IMU211-QNBM3QUOTA-3      TO IMU212-QNBM3QUOTA-3                
           MOVE  IMU211-QNBPIECES-2       TO IMU212-QNBPIECES-2                 
           MOVE  IMU211-QVOLUME-2         TO IMU212-QVOLUME-2                   
           MOVE  IMU211-QNBM3QUOTA-2      TO IMU212-QNBM3QUOTA-2                
           MOVE  IMU211-QNBPIECES-1       TO IMU212-QNBPIECES-1                 
           MOVE  IMU211-QVOLUME-1         TO IMU212-QVOLUME-1                   
           MOVE  IMU211-QNBM3QUOTA-1      TO IMU212-QNBM3QUOTA-1                
           MOVE  IMU211-DATEJ             TO IMU212-DATEJ                       
           MOVE  IMU211-QNBPIECESJ        TO IMU212-QNBPIECESJ                  
           MOVE  IMU211-QVOLUMEJ          TO IMU212-QVOLUMEJ                    
           MOVE  IMU211-QNBM3QUOTAJ       TO IMU212-QNBM3QUOTAJ                 
           MOVE  IMU211-QNBPIECES1        TO IMU212-QNBPIECES1                  
           MOVE  IMU211-QVOLUME1          TO IMU212-QVOLUME1                    
           MOVE  IMU211-QNBM3QUOTA1       TO IMU212-QNBM3QUOTA1                 
           MOVE  IMU211-QNBPIECES2        TO IMU212-QNBPIECES2                  
           MOVE  IMU211-QVOLUME2          TO IMU212-QVOLUME2                    
           MOVE  IMU211-QNBM3QUOTA2       TO IMU212-QNBM3QUOTA2.                
      *****************************************************************         
       WRITE-FMU212 SECTION.                                            03660000
      *================================================================*        
           WRITE FMU212-ENR FROM DSECT-IMU212                                   
           IF NOT FMU212-NORMAL                                                 
              MOVE 'PB SUR FICHIER FMU212 STAT =' TO ABEND-MESS                 
              MOVE ST-FMU212                      TO ABEND-CODE                 
              GO TO FIN-ANORMALE                                                
           END-IF.                                                              
           ADD 1 TO NBRE-ECR-FMU212.                                            
      *================================================================*        
       FIN-ANORMALE    SECTION.                                         03660000
      *================================================================*        
           CLOSE FMU211 FMU212                                                  
           MOVE 'BMU212' TO ABEND-PROG.                                         
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                            
      *MODULE D'ABANDON                                                         
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
