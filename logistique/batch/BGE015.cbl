      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                BGE015.                                       
       AUTHOR.                    BOILLOT.                                      
      *                                                                         
      *===============================================================*         
      * PROGRAMME D'EDITION JOURNALIERE SUITE DE L'EXTRACTION BGE010  *         
      *              SUIVI ATTRIBUTION D'EMPLACEMENTS                 *         
      *                      PAR REFERENCE                            *         
      * JEROME BOILLOT  LMP INFORMATIQUE                              *         
      *===============================================================*         
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA                                               
      *    C01 IS SAUT.                                                         
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           C01 IS SAUT                                                          
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FICIN  ASSIGN TO FICIN.                                      
      *--                                                                       
            SELECT FICIN  ASSIGN TO FICIN                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IMPR   ASSIGN TO IMPR.                                       
      *--                                                                       
            SELECT IMPR   ASSIGN TO IMPR                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
       FD   IMPR                                                                
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD                                               
            DATA RECORD LIGNE.                                                  
       01  LIGNE.                                                               
           02  FILLER               PIC X.                                      
           02  W-LIGNE              PIC X(132).                                 
      *                                                                         
       FD   FICIN                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD                                               
            DATA RECORD ENREG.                                                  
       01  ENREG                    PIC X(100).                                 
      *                                                                         
      *================================================================*        
       WORKING-STORAGE         SECTION.                                         
      *================================================================*        
      *                                                                         
       01  FILLER             PIC X(22) VALUE '*** WORKING STORAGE***'.         
       01  FILLER.                                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CTR-ENRG             PIC S9(8) COMP VALUE ZERO.                  
      *--                                                                       
           02  CTR-ENRG             PIC S9(8) COMP-5 VALUE ZERO.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CTR-LINE             PIC S9(4) COMP VALUE ZERO.                  
      *--                                                                       
           02  CTR-LINE             PIC S9(4) COMP-5 VALUE ZERO.                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  CTR-PAGE             PIC S9(4) COMP VALUE ZERO.                  
      *--                                                                       
           02  CTR-PAGE             PIC S9(4) COMP-5 VALUE ZERO.                
      *}                                                                        
           02  CTR-DISP             PIC Z(03)9.                                 
           02  ENTRP-RES            PIC X(06).                                  
           02  CODE-RUPTURE         PIC 9          VALUE ZERO.                  
               88  RUPT-ENTREPOT                   VALUE 1.                     
               88  NEW-ENTREPOT                    VALUE 0.                     
           02  CODE-LECTURE         PIC 9          VALUE ZERO.                  
               88  FIN-FICHIER                     VALUE 1.                     
           02  CODE-PAGE            PIC 9          VALUE ZERO.                  
               88  NEXT-PAGE                       VALUE 1.                     
               88  FIRST-PAGE                      VALUE 0.                     
           02  W-DATE.                                                          
               03  W-AA         PIC X(02).                                      
               03  W-MM         PIC X(02).                                      
               03  W-JJ         PIC X(02).                                      
           02  W-DATED.                                                         
               03  W-JJ         PIC X(02).                                      
               03  FILLER       PIC X(01) VALUE '/'.                            
               03  W-MM         PIC X(02).                                      
               03  FILLER       PIC X(01) VALUE '/'.                            
               03  W-AA         PIC X(02).                                      
      *================================================================*        
      *    L  I  G  N  E  S    D  '  E  D  I  T  I  O  N  *                     
      *================================================================*        
      *                                                                         
       01  IGE015.                                                              
          02 HENTET01.                                                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(12) VALUE SPACES.                          
          02 HENTET02.                                                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE 'ETABLISSEMENTS DARTY'.          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(12) VALUE SPACES.                          
          02 HENTET03.                                                          
            04 FILLER          PIC X(18) VALUE SPACES.                          
            04 FILLER          PIC X(10) VALUE 'ENTREPOT :'.                    
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 ENTRPED         PIC X(06) VALUE SPACES.                          
            04 FILLER          PIC X(11) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE '        ATTRIBUTION '.          
            04 FILLER          PIC X(20) VALUE 'DES EMPLACEMENTS PAR'.          
            04 FILLER          PIC X(14) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE '    ETAT IGE010     '.          
            04 FILLER          PIC X(12) VALUE SPACES.                          
          02 HENTET04.                                                          
            04 FILLER          PIC X(18) VALUE SPACES.                          
            04 FILLER          PIC X(10) VALUE 'DATE     :'.                    
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 DATED           PIC X(08) VALUE SPACES.                          
            04 FILLER          PIC X(11) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE '                REFE'.          
            04 FILLER          PIC X(20) VALUE 'RENCE               '.          
            04 FILLER          PIC X(12) VALUE SPACES.                          
            04 FILLER          PIC X(11) VALUE '    PAGE : '.                   
            04 PAGED           PIC Z(3)9 VALUE SPACES.                          
            04 FILLER          PIC X(17) VALUE SPACES.                          
          02 HENTET05.                                                          
            04 FILLER          PIC X(20) VALUE '                 +--'.          
            04 FILLER          PIC X(20) VALUE '--------------------'.          
            04 FILLER          PIC X(20) VALUE '--------------------'.          
            04 FILLER          PIC X(20) VALUE '-+------------------'.          
            04 FILLER          PIC X(20) VALUE '-------+------------'.          
            04 FILLER          PIC X(20) VALUE '---------------+    '.          
            04 FILLER          PIC X(12) VALUE SPACES.                          
          02 HENTET06.                                                          
            04 FILLER          PIC X(20) VALUE '                 !  '.          
            04 FILLER          PIC X(20) VALUE 'CODIC   FAM   MQE   '.          
            04 FILLER          PIC X(20) VALUE '     REFERENCE      '.          
            04 FILLER          PIC X(20) VALUE ' !  MODE  EMPLACNT  '.          
            04 FILLER          PIC X(20) VALUE ' STOCK !   DATE   ZA'.          
            04 FILLER          PIC X(20) VALUE '     RACK  SOL !    '.          
            04 FILLER          PIC X(12) VALUE SPACES.                          
          02 HENTET07.                                                          
            04 FILLER          PIC X(20) VALUE '                 !  '.          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(20) VALUE ' !                  '.          
            04 FILLER          PIC X(20) VALUE '       !  ATTRIB    '.          
            04 FILLER          PIC X(20) VALUE '    T C S   P  !    '.          
            04 FILLER          PIC X(12) VALUE SPACES.                          
          02 DETAIL01.                                                          
            04 FILLER          PIC X(19) VALUE '                 ! '.           
            04 CODIED          PIC X(07) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 FAMED           PIC X(05) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 MARQED          PIC X(05) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 REFED           PIC X(20) VALUE SPACES.                          
            04 FILLER          PIC X(04) VALUE '  ! '.                          
            04 MODED           PIC X(05) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 EMPED1          PIC X(02) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 EMPED2          PIC X(02) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 EMPED3          PIC X(03) VALUE SPACES.                          
            04 FILLER          PIC X(03) VALUE SPACES.                          
            04 STCKED          PIC Z(4)9 BLANK WHEN ZERO.                       
            04 FILLER          PIC X(03) VALUE ' ! '.                           
            04 DATRED          PIC X(08) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 ZOACCED         PIC X(02) VALUE SPACES.                          
            04 FILLER          PIC X(04) VALUE SPACES.                          
            04 TYPEMPED        PIC X(01) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 TYPCONED        PIC X(01) VALUE SPACES.                          
            04 FILLER          PIC X(01) VALUE SPACES.                          
            04 SPECIFED        PIC X(01) VALUE SPACES.                          
            04 FILLER          PIC X(03) VALUE SPACES.                          
            04 FLAGED          PIC X(01) VALUE SPACES.                          
            04 FILLER          PIC X(18) VALUE '  !               '.            
      *================================================================*        
      *       DESCRIPTION DES ZONES D'APPEL AU MODULE ABEND            *        
      *================================================================*        
            COPY  ABENDCOP.                                                     
      *================================================================*        
      *       DESCRIPTION DE LA ZONE D'ENREGISTREMENT (FICIN)         *         
      *================================================================*        
       01  F-ENREG.                                                             
           02  F-CODIC              PIC X(07).                                  
           02  F-CFAM               PIC X(05).                                  
           02  F-CMARQ              PIC X(05).                                  
           02  F-REF                PIC X(20).                                  
           02  F-SPECIF             PIC X(01).                                  
           02  F-ENTRP              PIC X(06).                                  
           02  F-MODE               PIC X(04).                                  
           02  F-DATRIB             PIC X(08).                                  
           02  F-EMP1               PIC X(02).                                  
           02  F-EMP2               PIC X(02).                                  
           02  F-EMP3               PIC X(03).                                  
           02  F-STOCK              PIC 9(05).                                  
           02  F-ZOACC              PIC X(01).                                  
           02  F-TYPEMP             PIC X(01).                                  
           02  F-TYPCON             PIC X(01).                                  
           02  F-FLAG               PIC X(01).                                  
           02  FILLER               PIC X(28).                                  
      *================================================================*        
       PROCEDURE               DIVISION.                                        
      *================================================================*        
      *===============================================================*         
      *              T R A M E   DU   P R O G R A M M E               *         
      *===============================================================*         
      *                                                                         
      *===============================================================*         
      *                                                               *         
      *           -----------------------------------------           *         
      *           -           MODULE    BGE015            -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      *===============================================================*         
      *                                                                         
      *===============================================================*         
       MODULE-BGE015           SECTION.                                         
      *===============================================================*         
      *                                                                         
           PERFORM ENTREE.                                                      
           PERFORM TRAITEMENT.                                                  
           PERFORM SORTIE.                                                      
      *                                                                         
       FIN-MODULE-BGE015.  EXIT.                                                
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       ENTREE                     SECTION.                                      
      *===============================================================*         
      *                                                                         
           OPEN INPUT FICIN OUTPUT IMPR.                                        
      *                                                                         
           DISPLAY '**********************************************'             
           DISPLAY '*             PROGRAMME  BGE015              *'             
           DISPLAY '**********************************************'             
           DISPLAY '*               DEBUT  NORMAL                *'             
           DISPLAY '**********************************************'.            
      *                                                                         
           ACCEPT W-DATE FROM DATE                                              
           MOVE CORR W-DATE TO W-DATED                                          
           PERFORM LECTURE.                                                     
      *                                                                         
       FIN-ENTREE. EXIT.                                                        
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       TRAITEMENT                 SECTION.                                      
      *===============================================================*         
      *                                                                         
           PERFORM UNTIL FIN-FICHIER                                            
               PERFORM MEF-ENTETE                                               
               PERFORM UNTIL RUPT-ENTREPOT OR FIN-FICHIER                       
                   PERFORM MEF-LIGNE                                            
                   PERFORM ECRITURE                                             
                   PERFORM LECTURE                                              
               END-PERFORM                                                      
           END-PERFORM.                                                         
           WRITE LIGNE FROM HENTET05.                                           
      *                                                                         
       FIN-TRAITEMENT. EXIT.                                                    
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       SORTIE                  SECTION.                                         
      *===============================================================*         
      *                                                                         
           IF CTR-ENRG = 0                                                      
              DISPLAY '****   AUCUN ENREGISTREMENT A ECRIRE      ****'          
              DISPLAY '**********************************************'          
           ELSE                                                                 
              MOVE CTR-ENRG TO CTR-DISP                                         
              DISPLAY '**********************************************'          
              DISPLAY '* NOMBRE D''ENR ECRITS : ' CTR-DISP '        *'          
              DISPLAY '**********************************************'          
              DISPLAY '*               FIN   NORMALE                *'          
              DISPLAY '**********************************************'          
           END-IF.                                                              
           PERFORM FERMETURE-FICHIERS.                                          
           PERFORM FIN-PROGRAMME.                                               
      *                                                                         
       FIN-SORTIE. EXIT.                                                        
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       LECTURE                 SECTION.                                         
      *===============================================================*         
      *                                                                         
           READ FICIN INTO F-ENREG                                              
              AT END SET FIN-FICHIER TO TRUE.                                   
           IF F-ENTRP NOT = ENTRP-RES                                           
              MOVE F-ENTRP TO ENTRP-RES                                         
              SET RUPT-ENTREPOT TO TRUE                                         
           ELSE                                                                 
              SET NEW-ENTREPOT TO TRUE                                          
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE. EXIT.                                                       
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       MEF-LIGNE               SECTION.                                         
      *===============================================================*         
      *                                                                         
           MOVE F-CODIC  TO  CODIED.                                            
           MOVE F-CFAM   TO  FAMED.                                             
           MOVE F-CMARQ  TO  MARQED.                                            
           MOVE F-REF    TO  REFED.                                             
           MOVE F-SPECIF TO  SPECIFED.                                          
           MOVE F-MODE   TO  MODED.                                             
           MOVE F-DATRIB TO  DATRED.                                            
           MOVE F-EMP1   TO  EMPED1.                                            
           MOVE F-EMP2   TO  EMPED2.                                            
           MOVE F-EMP3   TO  EMPED3.                                            
           MOVE F-STOCK  TO  STCKED.                                            
           MOVE F-ZOACC  TO  ZOACCED.                                           
           MOVE F-TYPEMP TO  TYPEMPED.                                          
           MOVE F-TYPCON TO  TYPCONED.                                          
           MOVE F-FLAG   TO  FLAGED.                                            
      *                                                                         
       FIN-MEF-LIGNE. EXIT.                                                     
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       MEF-ENTETE              SECTION.                                         
      *===============================================================*         
      *                                                                         
           SET NEW-ENTREPOT TO TRUE.                                            
           MOVE ENTRP-RES TO ENTRPED.                                           
           MOVE   W-DATED TO DATED.                                             
           MOVE       +55 TO CTR-LINE.                                          
           MOVE         0 TO CTR-PAGE.                                          
      *                                                                         
       FIN-MEF-ENTETE. EXIT.                                                    
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       ECRITURE-ENTETE         SECTION.                                         
      *===============================================================*         
      *                                                                         
           IF FIRST-PAGE                                                        
              SET NEXT-PAGE TO TRUE                                             
           ELSE                                                                 
              WRITE LIGNE FROM HENTET05                                         
           END-IF.                                                              
           ADD 1 TO CTR-PAGE.                                                   
           MOVE CTR-PAGE TO PAGED.                                              
           WRITE LIGNE FROM HENTET01 AFTER ADVANCING SAUT.                      
           WRITE LIGNE FROM HENTET02.                                           
           WRITE LIGNE FROM HENTET01.                                           
           WRITE LIGNE FROM HENTET03.                                           
           WRITE LIGNE FROM HENTET04.                                           
           WRITE LIGNE FROM HENTET01.                                           
           WRITE LIGNE FROM HENTET05.                                           
           WRITE LIGNE FROM HENTET06.                                           
           WRITE LIGNE FROM HENTET07.                                           
           WRITE LIGNE FROM HENTET05.                                           
           MOVE +9 TO CTR-LINE.                                                 
      *                                                                         
       FIN-ECRITURE-ENTETE. EXIT.                                               
        EJECT                                                                   
      *                                                                         
      *===============================================================*         
       ECRITURE                SECTION.                                         
      *===============================================================*         
      *                                                                         
           IF CTR-LINE > 54                                                     
              PERFORM ECRITURE-ENTETE                                           
           END-IF.                                                              
           WRITE LIGNE FROM DETAIL01.                                           
           ADD 1 TO CTR-LINE CTR-ENRG.                                          
      *                                                                         
       FIN-ECRITURE. EXIT.                                                      
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       PLANTAGE                  SECTION.                                       
      *================================================================*        
      *                                                                         
           PERFORM  FIN-ANORMALE.                                               
           MOVE  'BGE015' TO ABEND-PROG.                                        
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                          
      *                                                                         
       FIN-PLANTAGE. EXIT.                                                      
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       FIN-ANORMALE              SECTION.                                       
      *================================================================*        
      *                                                                         
           DISPLAY  '**********************************************'.           
           DISPLAY  '***      INTERRUPTION DU PROGRAMME         ***'.           
           DISPLAY  '***              BGE015                    ***'.           
           DISPLAY  '**********************************************'.           
           PERFORM  FERMETURE-FICHIERS.                                         
      *                                                                         
       FIN-FIN-ANORMALE. EXIT.                                                  
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       FERMETURE-FICHIERS       SECTION.                                        
      *================================================================*        
      *                                                                         
           CLOSE IMPR FICIN.                                                    
      *                                                                         
       F-FERMETURE-FICHIERS. EXIT.                                              
        EJECT                                                                   
      *                                                                         
      *================================================================*        
       FIN-PROGRAMME          SECTION.                                          
      *================================================================*        
      *                                                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                           
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-PROGRAMME. EXIT.                                                 
        EJECT                                                                   
