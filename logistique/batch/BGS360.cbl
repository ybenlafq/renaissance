      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID DIVISION.                                                             
       PROGRAM-ID. BGS360.                                                      
      *****************************************************************         
      *            CREE LE 08/10/2002 PAR DSA079                      *         
      *****************************************************************         
      *                                                               *         
      *            PROGRAMME D EXTRACTION VISANT A NETTOYER LA RTGS36 *         
      *   DES MAGASINS MGI BASCULES NMD                               *         
      *****************************************************************         
      * FICHIER EN ENTREE                                             *         
      *            -FGS36  : UNLOAD DE LA RTGS36                      *         
      *            -FNMD   : UNLOAD DE LA RVMGNMD                     *         
      *                                                               *         
      * FICHIER EN SORTIE :                                           *         
      *            -FGS36S : FICHIER NETTOYER DES MGI AYANT BASCULES  *         
      *                                                               *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT  SECTION.                                                   
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE    ASSIGN TO FDATE    FILE STATUS IS ST-FDATE.          
      *--                                                                       
           SELECT FDATE    ASSIGN TO FDATE    FILE STATUS IS ST-FDATE           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGS36    ASSIGN TO FGS36    FILE STATUS IS ST-FGS36.          
      *--                                                                       
           SELECT FGS36    ASSIGN TO FGS36    FILE STATUS IS ST-FGS36           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNMD     ASSIGN TO FNMD     FILE STATUS IS ST-FNMD.           
      *--                                                                       
           SELECT FNMD     ASSIGN TO FNMD     FILE STATUS IS ST-FNMD            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGS36S   ASSIGN TO FGS36S   FILE STATUS IS ST-FGS36S.         
      *--                                                                       
           SELECT FGS36S   ASSIGN TO FGS36S   FILE STATUS IS ST-FGS36S          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENREG.                                                         
           02 FDATE-JJ              PIC X(02).                                  
           02 FDATE-MM              PIC X(02).                                  
           02 FDATE-SS              PIC X(02).                                  
           02 FDATE-AA              PIC X(02).                                  
           02 FILLER                PIC X(72).                                  
       FD  FGS36                                                                
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FGS36-ENR       PIC X(66).                                           
       FD  FNMD                                                                 
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FNMD-ENR       PIC X(95).                                            
       FD  FGS36S                                                               
           RECORDING MODE IS F                                                  
           BLOCK CONTAINS 0                                                     
           LABEL RECORD STANDARD.                                               
       01  FGS36S-ENR       PIC X(66).                                          
       WORKING-STORAGE SECTION.                                                 
      * ETAT DES FICHIERS                                                       
       01  W-FLAGS.                                                             
           05 FICHIER-FGS36            PIC X VALUE 'O'.                         
              88 NON-FIN-FGS36               VALUE 'O'.                         
              88 FIN-FGS36                   VALUE 'N'.                         
           05 FICHIER-FNMD             PIC X VALUE 'O'.                         
              88 NON-FIN-FNMD                VALUE 'O'.                         
              88 FIN-FNMD                    VALUE 'N'.                         
       01 ST-FGS36                  PIC 99 VALUE ZERO.                          
       01 ST-FGS36S                 PIC 99 VALUE ZERO.                          
       01 ST-FDATE                  PIC 99 VALUE ZERO.                          
       01 ST-FNMD                   PIC 99 VALUE ZERO.                          
      * LES FICHIERS EN ENTREE SONT TRIES PAR CODE SOCIETE                      
       01  W-NSOCIETE               PIC X(03).                                  
       01  W-CPROG                  PIC X(06).                                  
      * FORMAT : SSAAMMJJ                                                       
       01  W-DATESAMJ               PIC X(08).                                  
       01  MGNMD-DATESAMJ-0         PIC X(08).                                  
       01  W-MGNMD-DATESAMJ-0       PIC X(08).                                  
      * COMPTEURS ECRITURE LECTURE                                              
       01  W-FGS36S-ECRIT           PIC S9(09) COMP-3 VALUE ZERO.               
       01  W-FGS36-LU               PIC S9(09) COMP-3 VALUE ZERO.               
       01  GS36-ENREG-SUP           PIC S9(09) COMP-3 VALUE ZERO.               
       01  W-FNMD-LU                PIC S9(09) COMP-3 VALUE ZERO.               
       01  W-FNMD-BMG921            PIC S9(09) COMP-3 VALUE ZERO.               
      *-- ZONES D'APPEL AU MODULE D'ABEND                                       
           COPY ABENDCOP.                                                       
           COPY SYKWERRO.                                                       
           COPY WORKDATC.                                                       
       01  RVGS3600.                                                            
           02  GS36-NCODIC                                                      
               PIC X(0007).                                                     
           02  GS36-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GS36-NMAG                                                        
               PIC X(0003).                                                     
           02  GS36-NSSLIEU                                                     
               PIC X(0003).                                                     
           02  GS36-QSTOCK                                                      
               PIC S9(5) COMP-3.                                                
           02  GS36-NORIGINE                                                    
               PIC X(0007).                                                     
           02  GS36-DEXTRACTION                                                 
               PIC X(0008).                                                     
           02  GS36-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GS36-LMOTIF                                                      
               PIC X(025).                                                      
      * COPY DE LA SOUS TABLE UTILISEE DANS LE TRAITEMENT DU PROG               
           COPY RVMGNMD.                                                        
      * COPY DE L'ENREGISTREMENT SUIVANT DE LA SOUS TABLE                       
       01  W-RVMGNMD.                                                           
           05  W-MGNMD-CTABLEG2       PIC X(15).                                
           05  W-MGNMD-CTABLEG2-REDEF REDEFINES W-MGNMD-CTABLEG2.               
               10  W-MGNMD-NSOCMG     PIC X(03).                                
               10  W-MGNMD-NSOCMG-N   REDEFINES W-MGNMD-NSOCMG                  
                                      PIC 9(03).                                
               10  W-MGNMD-DATE       PIC X(06).                                
               10  W-MGNMD-CPROG      PIC X(06).                                
           05  W-MGNMD-WTABLEG        PIC X(80).                                
           05  W-MGNMD-WTABLEG-REDEF  REDEFINES W-MGNMD-WTABLEG.                
               10  W-MGNMD-SOCMAG     PIC X(06).                                
               10  W-MGNMD-LCOMMENT   PIC X(30).                                
      *                                                                         
      *================================================================         
       PROCEDURE DIVISION.                                                      
      *================================================================         
      *                                                                         
      *****************************************************************         
       MODULE-BGS360 SECTION.                                                   
      *****************************************************************         
      *                                                                         
           PERFORM DEBUT.                                                       
           PERFORM TRAITEMENT.                                                  
           PERFORM SORTIE.                                                      
      *                                                                         
       FIN-MODULE-BGS360. EXIT.                                                 
      *                                                                         
      *****************************************************************         
       DEBUT    SECTION.                                                        
      *****************************************************************         
      *                                                                         
           DISPLAY 'DEBUT TRAITEMENT BGS360'.                                   
           OPEN INPUT  FGS36                                                    
                       FNMD                                                     
                       FDATE                                                    
                OUTPUT FGS36S.                                                  
           PERFORM RECUP-DATE.                                                  
      *                                                                         
       FIN-DEBUT. EXIT.                                                         
      *                                                                         
      *****************************************************************         
       TRAITEMENT                   SECTION.                                    
      *****************************************************************         
      *                                                                         
           INITIALIZE RVGS3600                                                  
                      RVMGNMD                                                   
                      MGNMD-DATESAMJ-0                                          
                      W-RVMGNMD                                                 
                      W-MGNMD-DATESAMJ-0.                                       
           PERFORM LECTURE-FGS36.                                               
           PERFORM TRAITEMENT-FNMD.                                             
           PERFORM UNTIL FIN-FGS36                                              
              MOVE GS36-NSOCIETE TO W-NSOCIETE                                  
              PERFORM UNTIL FIN-FGS36                                           
              OR GS36-NSOCIETE NOT = W-NSOCIETE                                 
                IF GS36-NSOCIETE =  MGNMD-NSOCMG                                
                AND W-DATESAMJ   >= MGNMD-DATESAMJ-0                            
                   PERFORM UNTIL FIN-FGS36                                      
                   OR GS36-NSOCIETE NOT = W-NSOCIETE                            
                      ADD 1 TO GS36-ENREG-SUP                                   
                      PERFORM LECTURE-FGS36                                     
                   END-PERFORM                                                  
                ELSE                                                            
                   PERFORM ECRITURE-FGS36S                                      
                   PERFORM LECTURE-FGS36                                        
                END-IF                                                          
              END-PERFORM                                                       
              PERFORM TRAITEMENT-FNMD                                           
           END-PERFORM.                                                         
           CLOSE FGS36                                                          
                 FGS36S                                                         
                 FNMD                                                           
                 FDATE.                                                         
      *                                                                         
       FIN-TRAITEMENT. EXIT.                                                    
      *                                                                         
      *****************************************************************         
       SORTIE                       SECTION.                                    
      *****************************************************************         
           DISPLAY 'NOMBRE D ENREGISTREMENT ECRIT    : ' W-FGS36S-ECRIT         
           DISPLAY 'NOMBRE LIGNE RTGS36 SUPPRIMEES   : ' GS36-ENREG-SUP         
           DISPLAY '                                   ---------'               
           DISPLAY 'NOMBRE D ENREGISTREMENT FGS36 LU : ' W-FGS36-LU             
           DISPLAY 'NOMBRE FNMD LU AVEC PARAM BMG921 : ' W-FNMD-BMG921          
           DISPLAY 'NOMBRE D ENREGISTREMENT FNMD  LU : ' W-FNMD-LU              
           GOBACK.                                                              
      *                                                                         
       FIN-SORTIE. EXIT.                                                        
      *                                                                 06070001
      *                                                                         
      *****************************************************************         
      *****************************************************************         
      *                                                               *         
      *     S O U S   S E C T I O N    D E B U T                      *         
      *                                                               *         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      *------------------------------------*                                    
       RECUP-DATE                   SECTION.                                    
      *------------------------------------*                                    
           READ FDATE                                                           
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                      
                  PERFORM PLANTAGE.                                             
           MOVE '1'                 TO GFDATA.                                  
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                              
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                            
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS            
              PERFORM PLANTAGE                                                  
           ELSE                                                                 
              DISPLAY '** DATE DE TRAITEMENT INFORMATIQUE ** ' GFJMSA-5         
              MOVE GFSAMJ-0         TO W-DATESAMJ                               
           END-IF.                                                              
       FIN-RECUP-DATE.              EXIT.                                       
      *                                                                         
      *                                                                 06070001
      *                                                                         
      *****************************************************************         
      *****************************************************************         
      *                                                               *         
      *     S O U S   S E C T I O N S    D U   T R A I T E M E N T    *         
      *                                                               *         
      *****************************************************************         
      *****************************************************************         
      *                                                                         
      *                                                                 06070001
      *                                                                         
      *                                                                         
      *****************************************************************         
       LECTURE-FGS36                SECTION.                                    
      *****************************************************************         
      *                                                                         
           SET NON-FIN-FGS36 TO TRUE                                            
           READ FGS36 INTO RVGS3600 AT END                                      
                SET FIN-FGS36 TO TRUE                                           
           END-READ.                                                            
           IF NON-FIN-FGS36                                                     
              ADD 1 TO W-FGS36-LU                                               
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FGS36. EXIT.                                                 
      *                                                                         
      *****************************************************************         
       LECTURE-FNMD                 SECTION.                                    
      *****************************************************************         
      * LECTURE DES SOCIETES BASCULANT MGI VERS NMD : LE CODE PROG              
      * DE LA SOUS TABLE EST BMG921                                             
      *                                                                         
           INITIALIZE W-CPROG.                                                  
           PERFORM UNTIL FIN-FNMD                                               
           OR W-CPROG = 'BMG921'                                                
                  READ FNMD INTO W-RVMGNMD AT END                               
                      SET FIN-FNMD TO TRUE                                      
                  END-READ                                                      
                  IF NON-FIN-FNMD                                               
                     ADD 1 TO W-FNMD-LU                                         
                  END-IF                                                        
                  MOVE W-MGNMD-CPROG  TO W-CPROG                                
           END-PERFORM.                                                         
           IF W-MGNMD-CPROG = 'BMG921'                                          
           AND NON-FIN-FNMD                                                     
              ADD 1 TO W-FNMD-BMG921                                            
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FNMD. EXIT.                                                  
      *                                                                         
      *****************************************************************         
       TRAITEMENT-FNMD              SECTION.                                    
      *****************************************************************         
      * LA DATE DE LA SOUS TABLE MGNMD INDIQUE LA DATE DE LA BASCULE            
      * (MGI VERS NMD) MAIS CETTE SOUS TABLE SERT EGALEMENT D' HISTORIQU        
      * CAD QU'IL PEUT Y AVOIR PLUSIEURS DATE POUR UNE MEME SOCIETE             
      * C'EST DONC CELLE QUI A LA DATE MAX QUI EST LA DATE DE BASCULE           
      * REELLE                                                                  
      *                                                                         
           PERFORM UNTIL FIN-FNMD                                               
           OR MGNMD-NSOCMG >= GS36-NSOCIETE                                     
              MOVE W-RVMGNMD          TO RVMGNMD                                
              MOVE W-MGNMD-DATESAMJ-0 TO MGNMD-DATESAMJ-0                       
              PERFORM UNTIL FIN-FNMD                                            
              OR W-MGNMD-NSOCMG NOT = MGNMD-NSOCMG                              
                 IF  W-MGNMD-DATESAMJ-0 > MGNMD-DATESAMJ-0                      
                    MOVE W-RVMGNMD          TO RVMGNMD                          
                    MOVE W-MGNMD-DATESAMJ-0 TO MGNMD-DATESAMJ-0                 
                 END-IF                                                         
                 PERFORM LECTURE-FNMD                                           
                 PERFORM CONVERSION-DATE                                        
              END-PERFORM                                                       
           END-PERFORM.                                                         
           IF W-DATESAMJ >= MGNMD-DATESAMJ-0                                    
           AND MGNMD-NSOCMG = GS36-NSOCIETE                                     
           AND NON-FIN-FGS36                                                    
              DISPLAY 'MAGASIN ' MGNMD-NSOCMG ' SUPPRIME'                       
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT-FNMD. EXIT.                                               
      *                                                                         
      *                                                                 06070001
      ***************************************************************** 06070001
       CONVERSION-DATE   SECTION.                                       06080001
      ***************************************************************** 06070001
      *                                                                 06070001
           MOVE '6'                 TO GFDATA .                                 
           MOVE W-MGNMD-DATE        TO GFJMA-3 .                                
           CALL 'BETDATC' USING WORK-BETDATC .                                  
           IF GFVDAT NOT = '1'                                                  
             DISPLAY 'DATE ' W-MGNMD-DATE ' DE RVMGNMD INVALIDE POUR '          
                     W-MGNMD-NSOCMG ' CORRIGER CETTE DATE'                      
             MOVE '99999999'       TO W-MGNMD-DATESAMJ-0                        
           ELSE                                                                 
             MOVE GFSAMJ-0         TO W-MGNMD-DATESAMJ-0                        
           END-IF.                                                              
      *                                                                         
       FIN-CONVERSION-DATE. EXIT.                                               
      *                                                                         
      *  ECRITURES DAND LE FICHIER DE SORTIE                            06070001
      ***************************************************************** 06070001
       ECRITURE-FGS36S   SECTION.                                       06080001
      ***************************************************************** 06070001
      *                                                                 06070001
           WRITE FGS36S-ENR FROM RVGS3600                                       
           ADD 1 TO W-FGS36S-ECRIT.                                             
      *                                                                         
       FIN-ECRITURE-FGS36S. EXIT.                                               
      *****************************************************************         
      *                                                                         
      *****************************************************************         
       PLANTAGE                     SECTION.                                    
      *****************************************************************         
      *                                                                         
           DISPLAY '*--------------------*'                                     
           DISPLAY '   P L A N T A G E    '                                     
           DISPLAY '*--------------------*'                                     
           DISPLAY 'SUR LA SOCIETE : ' W-NSOCIETE.                              
           DISPLAY 'NOMBRE D ENREGISTREMENT ECRIT    : ' W-FGS36S-ECRIT         
           DISPLAY 'NOMBRE D ENREGISTREMENT FGS36 LU : ' W-FGS36-LU             
           DISPLAY 'NOMBRE D ENREGISTREMENT FNMD  LU : ' W-FNMD-LU              
           MOVE 'BGS360'   TO ABEND-PROG.                                       
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                            
      *                                                                         
       FIN-PLANTAGE. EXIT.                                                      
