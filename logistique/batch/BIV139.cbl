      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.                     BIV139.                          00020000
       AUTHOR. PROJET AIDA.                                             00030000
      *                                                                 00040000
      ******************************************************************00050000
      * PROJET: GESTION INVENTAIRES / EDITION DES ECARTS INVENTAIRE PAR 00060000
      *         ARTICLES TOUS MAGASINS CONFONDUS                        00070000
      * DATE CREATION: 03/07/90                                         00080000
      * BUT DU PROGRAMME :                                              00090000
      *   EDITION LISTE DES ECARTS D'INVENTAIRE PAR ARTICLE / SOCIETE   00100000
      *   POUR TS LES ARTICLES                                          00110000
      *   A PARTIR DU FICHIER FIV130                                    00120000
      *   ET TRIE SUR :  - CODE SOCIETE                                 00130000
      *                  - NUMERO SEQUENCE FAMILLE                      00140000
      *                  - CODE ARTICLE                                 00150000
      *                                                                 00160000
      ******************************************************************00170006
      * DSA026 - FQ EN COL1  CHANGEMENT DU TITRE DE L'ETAT EN FONCTION *00180006
      *        DE LA VARIABLE F130-CODE                                *00190006
      *-----------------------------------------------------------------00200006
      ******************************************************************00210006
       ENVIRONMENT DIVISION.                                            00220000
       CONFIGURATION SECTION.                                           00230000
       SPECIAL-NAMES.                                                   00240000
           DECIMAL-POINT IS COMMA.                                      00250000
       INPUT-OUTPUT SECTION.                                            00260000
      *                                                                 00270000
       FILE-CONTROL.                                                    00280000
      *                                                                 00290000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FIV130 ASSIGN TO FIV130.                             00300000
      *--                                                                       
            SELECT FIV130 ASSIGN TO FIV130                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00310000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IIV139 ASSIGN TO IIV139.                             00320000
      *--                                                                       
            SELECT IIV139 ASSIGN TO IIV139                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00330000
       DATA DIVISION.                                                   00340000
       FILE SECTION.                                                    00350000
      *                                                                 00360000
       FD  FIV130                                                       00370000
           RECORDING F                                                  00380000
           BLOCK CONTAINS 0 RECORDS                                     00390000
           LABEL RECORD STANDARD                                        00400000
           DATA RECORD W-FIN130.                                        00410000
      *                                                                 00420000
       COPY FIV130.                                                     00430000
      *                                                                 00440000
      *                                                                 00450000
       FD  IIV139                                                       00460000
           BLOCK CONTAINS 0 RECORDS                                     00470000
           LABEL RECORD STANDARD                                        00480000
           DATA RECORD ENR-IIV139.                                      00490000
      *                                                                 00500000
       01  ENR-IIV139.                                                  00510000
           02  SAUT-IIV139 PIC X(01).                                   00520000
           02  LIG-IIV139  PIC X(132).                                  00530000
      ***************************************************************** 00540000
       WORKING-STORAGE SECTION.                                         00550000
      ***************************************************************** 00560000
      *                         ZONES   SPECISCQUES   AU   PROGRAMME  * 00570000
      ***************************************************************** 00580000
      *                                                               * 00590000
       77  FILLER           PIC X(20) VALUE '**DEBUT DE WORKING**'.     00600000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  WRET               PIC S9(04) COMP  VALUE ZERO.              00610000
      *--                                                                       
       77  WRET               PIC S9(04) COMP-5  VALUE ZERO.                    
      *}                                                                        
       77  DATHEUR            PIC S9(13) COMP-3 VALUE ZERO.             00620000
      *                                                                 00630000
       01  W-DONNEES.                                                   00640000
      *                                                                 00650000
      *------------------------------ NB LIGNE PAGE                     00660000
             02 W-LIG-PAG                   PIC 9(2) VALUE ZERO.        00670000
      *------------------------------ NB PAGE                           00680000
             02 W-NUM-PAGE                  PIC 9(3) VALUE ZERO.        00690000
      *                                                                 00700000
      ***************************************************************** 00710000
      *                                                                 00720000
      *------------------------------ CODE SOCIETE MAGASIN              00730000
             02 W-NSOCMAG-REF               PIC X(03) VALUE SPACE.      00740000
      *------------------------------ CODE ARTICLE                      00750000
             02 W-NCODIC-REF                PIC X(07) VALUE SPACE.      00760000
      *------------------------------ CODE FAMILLE                      00770000
             02 W-CFAM-REF                  PIC X(05) VALUE SPACE.      00780000
      *------------------------------ CODE MARQUE                       00790000
             02 W-CMARQ-REF                 PIC X(05) VALUE SPACE.      00800000
      *------------------------------ CODE LIEU                         00810000
             02 W-NLIEU-REF                 PIC X(05) VALUE SPACE.      00820000
      *------------------------------ CODE LIEU MAGASIN                 00830000
             02 W-NMAG-REF                  PIC X(03) VALUE SPACE.      00840000
      *------------------------------ CODE SOUS LIEU                    00850000
             02 W-NSSLIEU-REF               PIC X(03) VALUE SPACE.      00860000
      *------------------------------ LIBELLE REFERENCE                 00870000
             02 W-LREFFOURN-REF             PIC X(20) VALUE SPACE.      00880000
      *                                                                 00890000
      ***************************************************************** 00900000
      *-------------------------------------                            00910000
      *        TOTALISATION ARTICLE                                     00920000
      *-------------------------------------  QUANTITE STOCK EN ECART   00930000
             02 W-QSTOCKEC-ART PIC S9(09)     COMP-3 VALUE ZERO.        00940000
      *-------------------------------------  VALEUR STOCK ECART        00950000
      *                                       INVENTAIRE                00960000
             02 W-VSTOCKEC-ART PIC S9(11)V9(06) COMP-3 VALUE ZERO.      00970000
      *-------------------------------------  VALEUR STOCK ECART        00980000
      *                                       INVENTAIRE / ARTICLE      00990000
             02 W-VSTOCKEC     PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01000000
      *-------------------------------------                            01010000
      *        TOTALISATION ARTICLE MEME LIEU                           01020000
      *-------------------------------------  QUANTITE STOCK EN ECART   01030000
             02 W-QSTOCKEC-MAG PIC S9(09)     COMP-3 VALUE ZERO.        01040000
      *-------------------------------------  VALEUR STOCK ECART        01050000
      *                                       INVENTAIRE                01060000
             02 W-VSTOCKEC-MAG PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01070000
      *                                                                 01080000
      **************************************************************    01090000
      *                                                                 01100000
      ***************************************************************** 01110000
      ***************************************************************** 01120000
      *                                                               * 01130000
       01  I                            PIC 9(02) VALUE ZERO.           01140000
       01  W-IND-LIG                    PIC 9(02) VALUE ZERO.           01150000
       01  W-IND-PAG                    PIC 9(02) VALUE ZERO.           01160000
      *                                                               * 01170000
       01  W-DATE.                                                      01180000
           02  W-DATE-SS                PIC X(02).                      01190000
           02  W-DATE-AA                PIC X(02).                      01200000
           02  W-DATE-MM                PIC X(02).                      01210000
           02  W-DATE-JJ                PIC X(02).                      01220000
      *                                                               * 01230000
       01  W-DATS.                                                      01240000
           02  W-DATS-AA                PIC X(02).                      01250000
           02  W-DATS-MM                PIC X(02).                      01260000
           02  W-DATS-JJ                PIC X(02).                      01270000
      *                                                               * 01280000
       01  W-DATJ.                                                      01290000
           02  W-DATJ-JJ                PIC X(02).                      01300000
           02  FILLER                   PIC X(01) VALUE '/'.            01310000
           02  W-DATJ-MM                PIC X(02).                      01320000
           02  FILLER                   PIC X(01) VALUE '/'.            01330000
           02  W-DATJ-AA                PIC X(02).                      01340000
      *                                                               * 01350000
      *                                                               * 01360000
       01  W-DJOUR.                                                     01370000
           02  W-DJOUR-JJ               PIC X(02).                      01380000
           02  FILLER                   PIC X(01) VALUE '/'.            01390000
           02  W-DJOUR-MM               PIC X(02).                      01400000
           02  FILLER                   PIC X(01) VALUE '/'.            01410000
           02  W-DJOUR-AA               PIC X(02).                      01420000
      *                                                               * 01430000
       01  W-DAT2.                                                      01440000
           02  W-DAT2-AA                 PIC X(02).                     01450000
           02  W-DAT2-MM                 PIC X(02).                     01460000
           02  W-DAT2-JJ                 PIC X(02).                     01470000
      *                                                               * 01480000
      *                                                               * 01490000
       01  W-LIGNE.                                                     01500000
           02  W-LIG-SAUT               PIC X(01).                      01510000
           02  W-LIG-DONNEE             PIC X(132).                     01520000
      *                                                               * 01530000
       01  W-MESSAGE.                                                   01540000
           02  W-MESSAGE1   PIC X(40).                                  01550000
           02  W-MESSAGE2.                                              01560000
               03  W-MESSAGE21  PIC X(07).                              01570000
               03  W-MESSAGE22  PIC X(07).                              01580000
               03  W-MESSAGE23  PIC X(07).                              01590000
               03  W-MESSAGE24  PIC X(07).                              01600000
               03  W-MESSAGE25  PIC X(07).                              01610000
      *                                                               * 01620000
      ***************************************************************** 01630000
      *                 ZONE COMPTEURS                                * 01640000
      ***************************************************************** 01650000
      *                                                               * 01660000
       01  COMPTEURS.                                                   01670000
           02 W-LUS-FIV130            PIC S9(5) COMP-3 VALUE  +0.       01680000
      *                                                               * 01690000
       01  W-FIV130              PIC X(01)  VALUE '0'.                  01700000
           88 OK-FIV130                     VALUE '1'.                  01710000
           88 FIN-FIV130                    VALUE '3'.                  01720000
      *                                                               * 01730000
       01  W-RUPTSOC             PIC X(01)  VALUE '0'.                  01740000
           88 OK-RUPTSOC                    VALUE '1'.                  01750000
           88 NOK-RUPTSOC                   VALUE '2'.                  01760000
      *                                                               * 01770000
       01  W-MAGASIN             PIC X(01)  VALUE '0'.                  01780000
           88 FIN-MAGASIN                   VALUE '1'.                  01790000
      *                                                               * 01800000
       01  W-ARTICLE             PIC X(01)  VALUE '0'.                  01810000
           88 DEB-ARTICLE                   VALUE '1'.                  01820000
      *                                                               * 01830000
      * -AIDA ********************************************************* 01840000
      *  DESCRIPTION DE L ETAT DES ECARTS INVENTAIRE /MAG RAYON FAMILLE 01850000
      ***************************************************************** 01860000
      *                                                                 01870000
       01  W-LIG-IIV139.                                                01880000
      ***************************************************************** 01890000
      *  ENTETE PHYSIQUE DE PAGE                                        01900000
      ***************************************************************** 01910000
          02 W-LIG-ENTETE1.                                             01920000
            04 FILLER PIC X(02) VALUE SPACES.                           01930000
            04 FILLER PIC X(6) VALUE 'IIV139'.                          01940000
            04 FILLER PIC X(34) VALUE SPACES.                           01950000
            04 FILLER PIC X(20) VALUE ' E T A B L I S S E M'.           01960000
            04 FILLER PIC X(20) VALUE ' E N T S    D A R T '.           01970000
            04 FILLER PIC X(17) VALUE 'Y                '.              01980000
            04 FILLER PIC X(19) VALUE ' E D I T E    L E  '.            01990000
            04 FILLER PIC X(02) VALUE SPACES.                           02000000
            04 W-LIG-DATE PIC X(8).                                     02010000
            04 FILLER PIC X(03) VALUE SPACES.                           02020000
      ***************************************************************** 02030000
          02 W-LIG-ENTETE1F.                                            02040007
            04 FILLER PIC X(02) VALUE SPACES.                           02050007
            04 FILLER PIC X(7) VALUE 'IIV139F'.                         02060009
            04 FILLER PIC X(33) VALUE SPACES.                           02070009
            04 FILLER PIC X(20) VALUE ' E T A B L I S S E M'.           02080007
            04 FILLER PIC X(20) VALUE ' E N T S    D A R T '.           02090007
            04 FILLER PIC X(17) VALUE 'Y                '.              02100007
            04 FILLER PIC X(19) VALUE ' E D I T E    L E  '.            02110007
            04 FILLER PIC X(02) VALUE SPACES.                           02120007
            04 W-LIG-DATEF PIC X(8).                                    02130007
            04 FILLER PIC X(03) VALUE SPACES.                           02140007
          02 W-LIG-ENTETE1R.                                            02150007
            04 FILLER PIC X(02) VALUE SPACES.                           02160007
            04 FILLER PIC X(7) VALUE 'IIV139R'.                         02170009
            04 FILLER PIC X(33) VALUE SPACES.                           02180009
            04 FILLER PIC X(20) VALUE ' E T A B L I S S E M'.           02190007
            04 FILLER PIC X(20) VALUE ' E N T S    D A R T '.           02200007
            04 FILLER PIC X(17) VALUE 'Y                '.              02210007
            04 FILLER PIC X(19) VALUE ' E D I T E    L E  '.            02220007
            04 FILLER PIC X(02) VALUE SPACES.                           02230007
            04 W-LIG-DATER PIC X(8).                                    02240007
            04 FILLER PIC X(03) VALUE SPACES.                           02250007
      ***************************************************************** 02260007
          02 W-LIG-ENTETE2.                                             02270000
            04 FILLER PIC X(20) VALUE SPACES.                           02280000
            04 FILLER PIC X(20) VALUE SPACES.                           02290000
            04 FILLER PIC X(20) VALUE SPACES.                           02300000
            04 FILLER PIC X(20) VALUE SPACES.                           02310000
            04 FILLER PIC X(20) VALUE SPACES.                           02320000
            04 FILLER PIC X(20) VALUE SPACES.                           02330000
            04 FILLER PIC X(11) VALUE SPACES.                           02340000
            04 FILLER PIC X(01) VALUE SPACES.                           02350000
      ***************************************************************** 02360000
          02 W-LIG-ENTETE3.                                             02370000
            04 FILLER PIC X(20) VALUE SPACES.                           02380000
            04 FILLER PIC X(21) VALUE SPACES.                           02390000
            04 FILLER PIC X(18) VALUE '       ETAT DES SY'.             02400000
            04 FILLER PIC X(22) VALUE 'NTHESE PAR CODIC      '.         02410000
            04 FILLER PIC X(20) VALUE SPACES.                           02420000
            04 FILLER PIC X(23) VALUE '                PAGE : '.        02430000
            04 FILLER PIC X(01) VALUE SPACES.                           02440000
            04 W-LIG-PAGE PIC ZZ9.                                      02450000
            04 FILLER PIC X(04) VALUE SPACES.                           02460000
      ***************************************************************** 02470000
FQ        02 W-LIG-ENTETE3F.                                            02480006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02490006
FQ          04 FILLER PIC X(21) VALUE SPACES.                           02500006
FQ          04 FILLER PIC X(18) VALUE '       ETAT DES EC'.             02510006
FQ          04 FILLER PIC X(22) VALUE 'ARTS D''INVENTAIRE     '.        02520006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02530006
FQ          04 FILLER PIC X(23) VALUE '                PAGE : '.        02540006
FQ          04 FILLER PIC X(01) VALUE SPACES.                           02550006
FQ          04 W-LIG-PAGEF PIC ZZ9.                                     02560006
FQ          04 FILLER PIC X(04) VALUE SPACES.                           02570006
FQ    ***************************************************************** 02580006
FQ        02 W-LIG-ENTETE3R.                                            02590006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02600006
FQ          04 FILLER PIC X(21) VALUE SPACES.                           02610006
FQ          04 FILLER PIC X(18) VALUE '       IMPACTS DES'.             02620006
FQ          04 FILLER PIC X(22) VALUE ' RECYCLAGES SUR       '.         02630006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02640006
FQ          04 FILLER PIC X(23) VALUE '                PAGE : '.        02650006
FQ          04 FILLER PIC X(01) VALUE SPACES.                           02660006
FQ          04 W-LIG-PAGER PIC ZZ9.                                     02670006
FQ          04 FILLER PIC X(04) VALUE SPACES.                           02680006
FQ                                                                      02690006
      ***************************************************************** 02700001
          02 W-LIG-ENTETE4.                                             02710001
            04 FILLER PIC X(20) VALUE SPACES.                           02720000
            04 FILLER PIC X(20) VALUE SPACES.                           02730000
            04 FILLER PIC X(24) VALUE '          DES ECARTS D I'.       02740000
            04 FILLER PIC X(20) VALUE 'NVENTAIRES          '.           02750000
            04 FILLER PIC X(20) VALUE SPACES.                           02760000
            04 FILLER PIC X(20) VALUE SPACES.                           02770000
            04 FILLER PIC X(08) VALUE SPACES.                           02780000
      ***************************************************************** 02790001
FQ        02 W-LIG-ENTETE4F.                                            02800006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02810006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02820006
FQ          04 FILLER PIC X(24) VALUE '        VALORISES AU PRM'.       02830006
FQ          04 FILLER PIC X(20) VALUE 'P FIN DE MOIS       '.           02840006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02850006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02860006
FQ          04 FILLER PIC X(08) VALUE SPACES.                           02870006
FQ    ***************************************************************** 02880006
FQ        02 W-LIG-ENTETE4R.                                            02890006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02900006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02910006
FQ          04 FILLER PIC X(24) VALUE '          LES ECARTS D''I'.      02920006
FQ          04 FILLER PIC X(20) VALUE 'NVENTAIRE           '.           02930006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02940006
FQ          04 FILLER PIC X(20) VALUE SPACES.                           02950006
FQ          04 FILLER PIC X(08) VALUE SPACES.                           02960006
      ***************************************************************** 02970001
      *----------------------  FQ  -----------------------------------* 02980001
      ***************************************************************** 02990000
          02 W-LIG-ENTETE5.                                             03000000
            04 FILLER PIC X(02) VALUE '  '.                             03010000
            04 FILLER PIC X(18) VALUE 'DATE INVENTAIRE : '.             03020000
            04 W-LIG-DJOUR     PIC X(8).                                03030000
      ***************************************************************** 03040000
          02 W-LIG-ENTETE6.                                             03050000
            04 FILLER PIC X(02) VALUE '  '.                             03060000
            04 FILLER PIC X(18) VALUE 'SOCIETE         : '.             03070000
            04 W-LIG-NSOCMAG   PIC X(3).                                03080000
      ***************************************************************** 03090000
          02 W-LIG-ENTETE9.                                             03100000
            04 FILLER PIC X(10) VALUE SPACE.                            03110000
            04 FILLER PIC X(20) VALUE '    +---------------'.           03120000
            04 FILLER PIC X(20) VALUE '--------------------'.           03130000
            04 FILLER PIC X(20) VALUE '--------------------'.           03140000
            04 FILLER PIC X(20) VALUE '--------------------'.           03150000
            04 FILLER PIC X(28) VALUE '---------------------------+'.   03160000
            04 FILLER PIC X(10) VALUE SPACE.                            03170000
      ***************************************************************** 03180000
          02 W-LIG-ENTETE10.                                            03190000
            04 FILLER PIC X(10) VALUE SPACE.                            03200000
            04 FILLER PIC X(20) VALUE '    !  FAMILLE MARQU'.           03210000
            04 FILLER PIC X(20) VALUE 'E     CODIC         '.           03220000
            04 FILLER PIC X(20) VALUE 'REFERENCE       LIEU'.           03230000
            04 FILLER PIC X(20) VALUE '   SOUS-LIEU     !  '.           03240000
            04 FILLER PIC X(28) VALUE '   ECART         ECART     !'.   03250000
            04 FILLER PIC X(10) VALUE SPACE.                            03260000
      ***************************************************************** 03270000
          02 W-LIG-ENTETE11.                                            03280000
            04 FILLER PIC X(10) VALUE SPACE.                            03290000
            04 FILLER PIC X(20) VALUE '    !               '.           03300000
            04 FILLER PIC X(20) VALUE '                    '.           03310000
            04 FILLER PIC X(20) VALUE '                    '.           03320000
            04 FILLER PIC X(20) VALUE '    DE STOCK     !  '.           03330000
            04 FILLER PIC X(28) VALUE ' QUANTITE       VALORISE   !'.   03340000
            04 FILLER PIC X(10) VALUE SPACE.                            03350000
      ***************************************************************** 03360000
          02 W-LIG-ENTETE12.                                            03370000
            04 FILLER PIC X(10) VALUE SPACE.                            03380000
            04 FILLER PIC X(20) VALUE '    !---------------'.           03390000
            04 FILLER PIC X(20) VALUE '--------------------'.           03400000
            04 FILLER PIC X(20) VALUE '--------------------'.           03410000
            04 FILLER PIC X(20) VALUE '-----------------!--'.           03420000
            04 FILLER PIC X(28) VALUE '---------------------------!'.   03430000
            04 FILLER PIC X(10) VALUE SPACE.                            03440000
      ***************************************************************** 03450000
          02 W-LIG-ENTETE13.                                            03460000
            04 FILLER PIC X(10) VALUE SPACE.                            03470000
            04 FILLER PIC X(20) VALUE '    !               '.           03480000
            04 FILLER PIC X(20) VALUE '                    '.           03490000
            04 FILLER PIC X(20) VALUE '                    '.           03500000
            04 FILLER PIC X(20) VALUE '                 !  '.           03510000
            04 FILLER PIC X(28) VALUE '                           !'.   03520000
            04 FILLER PIC X(10) VALUE SPACE.                            03530000
      ***************************************************************** 03540000
      ***************************************************************** 03550000
          02 W-LIG-DETAIL.                                              03560000
            04 FILLER PIC X(10) VALUE SPACE.                            03570000
            04 FILLER PIC X(08) VALUE '    !   '.                       03580000
            04 W-LIG-CFAM           PIC X(05).                          03590000
            04 FILLER PIC X(03) VALUE '   '.                            03600000
            04 W-LIG-CMARQ          PIC X(05).                          03610000
            04 FILLER PIC X(04) VALUE '    '.                           03620000
            04 W-LIG-NCODIC         PIC X(07).                          03630000
            04 FILLER PIC X(02) VALUE '  '.                             03640000
            04 W-LIG-LREFFOURN      PIC X(20).                          03650000
            04 FILLER PIC X(02) VALUE '  '.                             03660000
            04 W-LIG-NLIEU          PIC X(03).                          03670000
            04 FILLER PIC X(08) VALUE SPACE.                            03680000
            04 W-LIG-NSSLIEU        PIC X(03).                          03690000
            04 FILLER PIC X(08) VALUE '       !'.                       03700000
            04 W-LIG-QSTOCKEC       PIC ---------9.                     03710000
            04 FILLER PIC X(01) VALUE SPACES.                           03720000
            04 W-LIG-VSTOCKEC       PIC ------------9,99.               03730000
            04 FILLER PIC X(03) VALUE '  !'.                            03740000
            04 FILLER PIC X(10) VALUE SPACE.                            03750000
      ***************************************************************** 03760000
      *                                                                 03770000
          02 W-LIG-TOTART.                                              03780000
            04 FILLER PIC X(10) VALUE SPACE.                            03790000
            04 FILLER PIC X(08) VALUE '    !   '.                       03800000
            04 FILLER PIC X(17) VALUE 'TOTAL CODIC      '.              03810000
            04 W-LIG-NCODICTOT      PIC X(07).                          03820000
            04 FILLER PIC X(02) VALUE '  '.                             03830000
            04 FILLER PIC X(20) VALUE SPACE.                            03840000
            04 FILLER PIC X(02) VALUE '  '.                             03850000
            04 W-LIG-NLIEU-ART      PIC X(03).                          03860000
            04 FILLER PIC X(08) VALUE SPACE.                            03870000
            04 W-LIG-NSSLIEU-ART    PIC X(03).                          03880000
            04 FILLER PIC X(08) VALUE '       !'.                       03890000
            04 W-LIG-QSTOCKEC-ART   PIC ---------9.                     03900000
            04 FILLER PIC X(01) VALUE SPACES.                           03910000
            04 W-LIG-VSTOCKEC-ART   PIC ------------9,99.               03920000
            04 FILLER PIC X(03) VALUE '  !'.                            03930000
            04 FILLER PIC X(10) VALUE SPACE.                            03940000
      ***************************************************************** 03950000
      *                                                                 03960000
      *                                                                 03970000
      ***************************************************************** 03980000
      *   ZONES PLANTAGES                                             * 03990000
      ***************************************************************** 04000000
           COPY ABENDCOP.                                               04010000
      *                                                                 04020000
      * -AIDA ********************************************************* 04030000
      *   ZONES GENERALES OBLIGATOIRES                                * 04040000
      ***************************************************************** 04050000
      *                                                                 04060000
       01   FILLER    PIC X(16)    VALUE      '*** Z-INOUT ****'.       04070000
       01   Z-INOUT   PIC X(4096)  VALUE      SPACES.                   04080000
      *                                                                 04090000
      ***************************************************************** 04100000
      *     ZONE  DE  GESTION  DES  ERREURS                           * 04110000
      ***************************************************************** 04120000
      *                                                               * 04130000
           COPY SYBWERRO.                                               04140000
      *                                                                 04150000
           COPY  SYBWDIV0.                                              04160000
      *                                                                 04170000
           COPY WORKDATC.                                               04180000
      *                                                               * 04190000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04200000
      *                                                                 04210000
       PROCEDURE DIVISION.                                              04220000
      *                                                                 04230000
      ***************************************************************** 04240000
      *              T R A M E   DU   P R O G R A M M E               * 04250000
      ***************************************************************** 04260000
      *                                                                 04270000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04280000
      *                                                               * 04290000
      *           -----------------------------------------           * 04300000
      *           - MODULE DE BASE DE LA TRANSACTION GD00 -           * 04310000
      *           -----------------------------------------           * 04320000
      *                               I                               * 04330000
      *           -----------------------------------------           * 04340000
      *           I                   I                   I           * 04350000
      *   -----------------   -----------------   -----------------   * 04360000
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   * 04370000
      *   -----------------   -----------------   -----------------   * 04380000
      *                                                               * 04390000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04400000
      *                                                                 04410000
       MODULE-BIV139                  SECTION.                          04420000
      *                                                                 04430000
           PERFORM MODULE-ENTREE.                                       04440000
      *                                                                 04450000
           PERFORM MODULE-TRAITEMENT.                                   04460000
      *                                                                 04470000
           PERFORM MODULE-SORTIE.                                       04480000
      *                                                                 04490000
       FIN-MODULE-BIV139.  EXIT.                                        04500000
      *                                                                 04510000
      * -AIDA ********************************************************* 04520000
      *                                                               * 04530000
      *  EEEEEEEEE NN     NN TTTTTTTTT RRRRRRRR  EEEEEEEEE EEEEEEEEE  * 04540000
      *  EEEEEEEEE NNN    NN TTTTTTTTT RRRRRRRRR EEEEEEEEE EEEEEEEEE  * 04550000
      *  EEE       NNN    NN    TTT    RRR   RRR EEE       EEE        * 04560000
      *  EEE       NNNN   NN    TTT    RRR   RRR EEE       EEE        * 04570000
      *  EEEEEEEEE NN NN  NN    TTT    RRRRRRRRR EEEEEEEEE EEEEEEEEE  * 04580000
      *  EEEEEEEEE NN  NN NN    TTT    RRRRRRRR  EEEEEEEEE EEEEEEEEE  * 04590000
      *  EEE       NN   NNNN    TTT    RRR RRRR  EEE       EEE        * 04600000
      *  EEE       NN    NNN    TTT    RRR   RR  EEE       EEE        * 04610000
      *  EEEEEEEEE NN    NNN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  * 04620000
      *  EEEEEEEEE NN     NN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  * 04630000
      *                                                               * 04640000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04650000
      *                                                               * 04660000
      *                    -----------------------                    * 04670000
      *                    -   MODULE D'ENTREE   -                    * 04680000
      *                    -----------------------                    * 04690000
      *                               -                               * 04700000
      *           -----------------------------------------           * 04710000
      *                                                               * 04720000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04730000
       MODULE-ENTREE              SECTION.                              04740000
      *                                                                 04750000
           MOVE   'BIV139'     TO   ABEND-PROG.                         04760000
           OPEN INPUT FIV130.                                           04770000
           OPEN OUTPUT IIV139.                                          04780000
      *                                                                 04790000
           ACCEPT W-DATS    FROM DATE.                                  04800000
           MOVE W-DATS-JJ  TO W-DATJ-JJ.                                04810000
           MOVE W-DATS-MM  TO W-DATJ-MM.                                04820000
           MOVE W-DATS-AA  TO W-DATJ-AA.                                04830000
           INITIALIZE W-MESSAGE.                                        04840000
           INITIALIZE W-DONNEES.                                        04850000
      *                                                                 04860000
       FIN-MODULE-ENTREE.   EXIT.                                       04870000
      *                                                                 04880000
      * -AIDA ********************************************************* 04890000
      *                                                               * 04900000
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  * 04910000
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  * 04920000
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        * 04930000
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        * 04940000
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  * 04950000
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  * 04960000
      *     TTT    RRR RRRR  AAA   AAA    III       TTT    EEE        * 04970000
      *     TTT    RRR   RR  AAA   AAA    III       TTT    EEE        * 04980000
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  * 04990000
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  * 05000000
      *                                                               * 05010000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05020000
      * MODULE DE TRAITEMENT                                          * 05030000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05040000
      *                                                                 05050000
       MODULE-TRAITEMENT          SECTION.                              05060000
      *                                                                 05070000
           PERFORM TRT-LISTE-FIV130 UNTIL FIN-FIV130.                   05080000
      *                                                                 05090000
       FIN-MODULE-TRAITEMENT. EXIT.                                     05100000
      *                                                                 05110000
      *================================================================ 05120000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05130000
      * LECTURE MVTS ECARTS D'INVENTAIRE PAR ARTICLE FIV130             05140000
      * MISE EN PAGE ET EDITION LISTE DES ECARTS PAR ARTICLE / SOCIETE* 05150000
      *    CLASSES PAR : CODE SOCIETE         ( AVEC RUPTURE PAGE )   * 05160000
      *                  N� SEQUENCE FAMILLE                          * 05170000
      *                  CODE ARTICLE         ( AVEC RUPTURE LIGNE )  * 05180000
      * MISE EN PAGE ET EDITION TOTAUX PAR ARTICLE ET LIEU            * 05190000
      * MISE EN PAGE ET EDITION TOTAUX PAR ARTICLE / TS LIEUX /SOCIETE* 05200000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05210000
      *                                                                 05220000
       TRT-LISTE-FIV130               SECTION.                          05230000
      *                                                                 05240000
           PERFORM TRT-LECT-FIV130 UNTIL ( FIN-MAGASIN OR FIN-FIV130).  05250000
      *                                                                 05260000
           IF NOT FIN-FIV130                                            05270000
              PERFORM TRT-EDIT-LIGART                                   05280000
              PERFORM TRT-EDIT-ENTETE                                   05290000
           END-IF.                                                      05300000
      *                                                                 05310000
           IF FIN-FIV130                                                05320000
              PERFORM TRT-LIGNE-ARTICLE                                 05330000
              PERFORM TRT-EDIT-TOTART                                   05340000
              PERFORM TRT-ENTETE-LIGNE9                                 05350000
              PERFORM TRT-EDIT-LIGNE                                    05360000
           END-IF.                                                      05370000
      *                                                                 05380000
       FIN-TRT-LISTE-FIV130. EXIT.                                      05390000
      *                                                                 05400000
      *================================================================ 05410000
      *  ==>  TRT-LISTE-FIV130                                          05420000
      *                                                               * 05430000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05440000
      * LECTURE   FIV130  ET CUMUL PAR ARTICLE                        * 05450000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05460000
      *                                                                 05470000
       TRT-LECT-FIV130              SECTION.                            05480000
      *                                                                 05490000
           READ FIV130                                                  05500000
                AT END MOVE '3' TO W-FIV130.                            05510000
      *                                                                 05520000
           IF W-FIV130 NOT = '3'                                        05530000
              ADD 1 TO W-LUS-FIV130                                     05540000
              IF F130-CFAM = SPACE OR LOW-VALUE                         05550000
                 MOVE 'XXXXX' TO F130-CFAM                              05560000
              END-IF                                                    05570000
              IF F130-CMARQ = SPACE OR LOW-VALUE                        05580000
                 MOVE 'XXXXX' TO F130-CMARQ                             05590000
              END-IF                                                    05600000
      *       DISPLAY ' MAGASIN ' F130-NSOCMAG                          05610000
      *                       ' ' F130-NMAG                             05620000
      *               ' FAMILLE ' F130-CFAM                             05630000
      *               ' CODIC '   F130-NCODIC                           05640000
              IF F130-NCODIC NOT = W-NCODIC-REF                         05650000
                 OR  W-NCODIC-REF = SPACE                               05660000
                 MOVE '1' TO W-FIV130                                   05670000
                 IF  W-NCODIC-REF = SPACE                               05680000
                     MOVE '1' TO W-ARTICLE                              05690000
                 END-IF                                                 05700000
              END-IF                                                    05710000
              IF ( F130-NCODIC = W-NCODIC-REF                           05720000
                   OR  W-NCODIC-REF = SPACE )                           05730000
                 AND ( F130-NMAG = W-NMAG-REF                           05740000
                       OR  W-NMAG-REF = SPACE )                         05750000
                 AND ( F130-NSSLIEU = W-NSSLIEU-REF                     05760000
                       OR  W-NSSLIEU-REF = SPACE )                      05770000
                 IF F130-PRMP NOT = ZERO                                05780000
                    COMPUTE W-VSTOCKEC = F130-QSTOCKEC1 * F130-PRMP     05790000
                 END-IF                                                 05800000
                 COMPUTE W-QSTOCKEC-MAG =                               05810000
                                   W-QSTOCKEC-MAG + F130-QSTOCKEC1      05820000
                 COMPUTE W-VSTOCKEC-MAG = W-VSTOCKEC-MAG + W-VSTOCKEC   05830000
                 MOVE F130-NMAG    TO W-NMAG-REF                        05840000
                 MOVE F130-NLIEU   TO W-NLIEU-REF                       05850000
                 MOVE F130-NSSLIEU TO W-NSSLIEU-REF                     05860000
                 MOVE F130-NCODIC  TO W-NCODIC-REF                      05870000
                 MOVE F130-CFAM    TO W-CFAM-REF                        05880000
                 MOVE F130-CMARQ   TO W-CMARQ-REF                       05890000
                 MOVE F130-LREFFOURN TO W-LREFFOURN-REF                 05900000
                 MOVE '0' TO W-MAGASIN                                  05910000
              END-IF                                                    05920000
              IF F130-NMAG NOT = W-NMAG-REF                             05930000
                 OR F130-NSSLIEU NOT = W-NSSLIEU-REF                    05940000
                    OR F130-NCODIC NOT = W-NCODIC-REF                   05950000
                       MOVE '1' TO W-MAGASIN                            05960000
              END-IF                                                    05970000
           END-IF.                                                      05980000
      *                                                                 05990000
           IF W-LUS-FIV130 = 1                                          06000000
              MOVE F130-DINVENTAIRE TO W-DATE                           06010000
              MOVE W-DATE-JJ        TO  W-DJOUR-JJ                      06020000
              MOVE W-DATE-MM        TO  W-DJOUR-MM                      06030000
              MOVE W-DATE-AA        TO  W-DJOUR-AA                      06040000
              PERFORM TRT-EDIT-ENTETE                                   06050000
           END-IF.                                                      06060000
      *                                                                 06070000
       FIN-TRT-LECT-FIV130. EXIT.                                       06080000
      *                                                               * 06090000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06100000
      * TRAITEMENT EDITION ENTETE (RUPTURE SOCIETE)                   * 06110000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06120000
      *                                                                 06130000
       TRT-EDIT-ENTETE                SECTION.                          06140000
      *                                                                 06150000
           IF ( F130-NSOCMAG NOT = W-NSOCMAG-REF                        06160000
                AND W-NSOCMAG-REF NOT = SPACE )                         06170000
                IF W-LIG-PAG = ZERO                                     06180000
                   PERFORM TRT-EDIT-ENTETE1                             06190000
                END-IF                                                  06200000
           END-IF.                                                      06210000
      *                                                                 06220000
      *                                                                 06230000
           IF F130-NSOCMAG NOT = W-NSOCMAG-REF                          06240000
              OR  W-NSOCMAG-REF = SPACE                                 06250000
                  PERFORM TRT-RUPT-SOC                                  06260000
           END-IF.                                                      06270000
      *                                                                 06280000
           IF OK-RUPTSOC                                                06290000
                  PERFORM TRT-EDIT-ENTETE1                              06300000
           END-IF.                                                      06310000
      *                                                               * 06320000
       FIN-TRT-EDIT-ENTETE. EXIT.                                       06330000
      *                                                               * 06340000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06350000
      * TRAITEMENT EDITION LIGNE ARTICLE ET TOTAUX ARTICLE            * 06360000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06370000
      *                                                                 06380000
       TRT-EDIT-LIGART               SECTION.                           06390000
      *                                                                 06400000
           IF W-LIG-PAG = ZERO                                          06410000
              PERFORM TRT-EDIT-ENTETE1                                  06420000
           END-IF.                                                      06430000
      *                                                                 06440000
           PERFORM TRT-LIGNE-ARTICLE.                                   06450000
      *                                                                 06460000
           IF ( F130-NCODIC NOT = W-NCODIC-REF                          06470000
                 AND W-NCODIC-REF NOT = SPACE )                         06480000
              OR ( F130-NSOCMAG NOT = W-NSOCMAG-REF                     06490000
                   AND W-NSOCMAG-REF NOT = SPACE )                      06500000
                       IF W-LIG-PAG = ZERO                              06510000
                          PERFORM TRT-EDIT-ENTETE1                      06520000
                       END-IF                                           06530000
                       PERFORM TRT-EDIT-TOTART                          06540000
           END-IF.                                                      06550000
      *                                                                 06560000
           PERFORM TRT-CUMUL-ARTICLE.                                   06570000
      *                                                                 06580000
       FIN-TRT-EDIT-LIGART. EXIT.                                       06590000
      *                                                               * 06600000
      *                                                               * 06610000
      *================================================================ 06620000
      *                                                               * 06630000
      * ==>    TRT-EDIT-ENTETE                                          06640000
      *                                                                 06650000
      *================================================================ 06660000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06670000
      * RUPTURE   SOCIETE                                             * 06680000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06690000
       TRT-EDIT-TOTSOC    SECTION.                                      06700000
      *                                                                 06710000
           PERFORM TRT-ENTETE-LIGNE12.                                  06720000
           PERFORM TRT-EDIT-LIGNE.                                      06730000
      *                                                                 06740000
           IF W-LIG-PAG > 55                                            06750000
              MOVE ZERO TO W-LIG-PAG                                    06760000
           END-IF.                                                      06770000
      *                                                                 06780000
       FIN-TRT-EDIT-TOTSOC. EXIT.                                       06790000
      *                                                                 06800000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06810000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06820000
      * RUPTURE   SOCIETE                                             * 06830000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06840000
      *                                                                 06850000
       TRT-RUPT-SOC                SECTION.                             06860000
      *                                                                 06870000
           IF F130-NSOCMAG NOT = W-NSOCMAG-REF                          06880000
              OR  W-NSOCMAG-REF = SPACE                                 06890000
              MOVE F130-NSOCMAG TO W-NSOCMAG-REF                        06900000
              MOVE '1'          TO W-RUPTSOC                            06910000
           END-IF.                                                      06920000
      * --------------------------------  FIN DE PAGE                   06930000
      *                                                                 06940000
           IF W-LIG-PAG > ZERO                                          06950000
              PERFORM TRT-ENTETE-LIGNE9                                 06960000
              PERFORM TRT-EDIT-LIGNE                                    06970000
           END-IF.                                                      06980000
      *                                                                 06990000
           MOVE ZERO        TO W-LIG-PAG.                               07000000
      *                                                                 07010000
       FIN-TRT-RUPT-SOC. EXIT.                                          07020000
      *                                                                 07030000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07040000
      *================================================================ 07050000
      *================================================================ 07060000
      * ==>    TRT-EDIT-LIGART                                          07070000
      *================================================================ 07080000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07090000
      * RUPTURE   ARTICLE                                             * 07100000
      * EDITION LIGNE  TOTAL ARTICLE                                  * 07110000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07120000
      *                                                                 07130000
       TRT-EDIT-TOTART    SECTION.                                      07140000
      *                                                                 07150000
           PERFORM TRT-ENTETE-LIGNE13.                                  07160000
           PERFORM TRT-EDIT-LIGNE.                                      07170000
           PERFORM TRT-ENTETE-LIGNE12.                                  07180000
           PERFORM TRT-EDIT-LIGNE.                                      07190000
      *                                                                 07200000
           PERFORM TRT-TOTART-LIGNE.                                    07210000
           PERFORM TRT-EDIT-LIGNE.                                      07220000
      *                                                                 07230000
           PERFORM TRT-ENTETE-LIGNE12.                                  07240000
           PERFORM TRT-EDIT-LIGNE.                                      07250000
      *                                                                 07260000
           IF NOT FIN-FIV130  AND                                       07270000
              F130-NSOCMAG = W-NSOCMAG-REF                              07280000
              IF W-LIG-PAG > 50                                         07290000
                 MOVE ZERO TO W-LIG-PAG                                 07300000
              ELSE                                                      07310000
                 PERFORM TRT-ENTETE-LIGNE13                             07320000
                 PERFORM TRT-EDIT-LIGNE                                 07330000
              END-IF                                                    07340000
           END-IF.                                                      07350000
      *                                                                 07360000
       FIN-TRT-EDIT-TOTART. EXIT.                                       07370000
      *                                                                 07380000
      *                                                                 07390000
       TRT-TOTART-LIGNE       SECTION.                                  07400000
      *                                                                 07410000
           MOVE SPACE           TO W-LIGNE.                             07420000
           MOVE W-NCODIC-REF    TO W-LIG-NCODICTOT.                     07430000
           MOVE W-NMAG-REF      TO W-LIG-NLIEU.                         07440000
           MOVE W-NSSLIEU-REF   TO W-LIG-NSSLIEU.                       07450000
           MOVE SPACE           TO W-LIG-CMARQ.                         07460000
           MOVE SPACE           TO W-LIG-CFAM.                          07470000
           MOVE SPACE           TO W-LIG-LREFFOURN.                     07480000
           MOVE W-QSTOCKEC-ART  TO W-LIG-QSTOCKEC-ART.                  07490000
           MOVE W-VSTOCKEC-ART  TO W-LIG-VSTOCKEC-ART.                  07500000
           ADD 1                TO W-LIG-PAG.                           07510000
           MOVE ' '             TO W-LIG-SAUT.                          07520000
           MOVE W-LIG-TOTART    TO W-LIG-DONNEE.                        07530000
      *                                                                 07540000
           MOVE F130-NCODIC             TO W-NCODIC-REF.                07550000
           MOVE F130-NMAG               TO W-NMAG-REF.                  07560000
           MOVE F130-CFAM               TO W-CFAM-REF.                  07570000
           MOVE F130-CMARQ              TO W-CMARQ-REF.                 07580000
           MOVE F130-NLIEU              TO W-NLIEU-REF.                 07590000
           MOVE F130-NSSLIEU            TO W-NSSLIEU-REF.               07600000
           MOVE F130-LREFFOURN          TO W-LREFFOURN-REF.             07610000
                                                                        07620000
           MOVE '1'               TO W-ARTICLE.                         07630000
      *                                                                 07640000
           INITIALIZE              W-QSTOCKEC-ART                       07650000
                                   W-VSTOCKEC-ART.                      07660000
      *                                                                 07670000
       FIN-TRT-TOTART-LIGNE. EXIT.                                      07680000
      *                                                                 07690000
      *================================================================ 07700000
      * ==>    TRT-EDIT-LIGART                                          07710000
      *     -->    TRT-LIGNE-ARTICLE                                    07720000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07730000
      * EDITION LIGNE ARTICLE                                         * 07740000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07750000
      *                                                               * 07760000
       TRT-LIGNE-ARTICLE          SECTION.                              07770000
      *                                                                 07780000
           PERFORM TRT-ARTICLE-LIGNE.                                   07790000
           PERFORM TRT-EDIT-LIGNE.                                      07800000
      *                                                                 07810000
       FIN-TRT-LIGNE-ARTICLE. EXIT.                                     07820000
      *                                                                 07830000
      *                                                                 07840000
       TRT-ARTICLE-LIGNE     SECTION.                                   07850000
      *                                                                 07860000
           MOVE SPACE             TO W-LIGNE.                           07870000
           MOVE W-QSTOCKEC-MAG    TO W-LIG-QSTOCKEC.                    07880000
           MOVE W-VSTOCKEC-MAG    TO W-LIG-VSTOCKEC.                    07890000
           MOVE W-NMAG-REF        TO W-LIG-NLIEU.                       07900000
           MOVE W-NSSLIEU-REF     TO W-LIG-NSSLIEU.                     07910000
           IF DEB-ARTICLE                                               07920000
              MOVE W-LREFFOURN-REF   TO W-LIG-LREFFOURN                 07930000
              MOVE W-CFAM-REF        TO W-LIG-CFAM                      07940000
              MOVE W-CMARQ-REF       TO W-LIG-CMARQ                     07950000
              MOVE W-NCODIC-REF      TO W-LIG-NCODIC                    07960000
              MOVE '0'               TO W-ARTICLE                       07970000
           ELSE                                                         07980000
              MOVE SPACE             TO W-LIG-LREFFOURN                 07990000
              MOVE SPACE             TO W-LIG-CFAM                      08000000
              MOVE SPACE             TO W-LIG-CMARQ                     08010000
              MOVE SPACE             TO W-LIG-NCODIC                    08020000
           END-IF.                                                      08030000
      *                                                                 08040000
           ADD 1                  TO W-LIG-PAG.                         08050000
           MOVE ' '               TO W-LIG-SAUT.                        08060000
           MOVE W-LIG-DETAIL      TO W-LIG-DONNEE.                      08070000
           IF W-LIG-PAG > 55                                            08080000
              MOVE ZERO TO W-LIG-PAG                                    08090000
           END-IF.                                                      08100000
      *                                                                 08110000
           IF NOT FIN-FIV130                                            08120000
              MOVE '0'                  TO W-FIV130                     08130000
              MOVE '0'                  TO W-MAGASIN                    08140000
              MOVE '0'                  TO W-RUPTSOC                    08150000
           END-IF.                                                      08160000
      *                                                                 08170000
           COMPUTE W-QSTOCKEC-ART = W-QSTOCKEC-ART + W-QSTOCKEC-MAG.    08180000
           COMPUTE W-VSTOCKEC-ART = W-VSTOCKEC-ART + W-VSTOCKEC-MAG.    08190000
      *                                                                 08200000
           INITIALIZE              W-QSTOCKEC-MAG                       08210000
                                   W-VSTOCKEC-MAG.                      08220000
      *                                                                 08230000
       FIN-TRT-ARTICLE-LIGNE. EXIT.                                     08240000
      *                                                                 08250000
       TRT-CUMUL-ARTICLE        SECTION.                                08260000
      *                                                                 08270000
      *                                                                 08280000
           MOVE F130-NCODIC             TO W-NCODIC-REF.                08290000
           MOVE F130-NLIEU              TO W-NLIEU-REF.                 08300000
           MOVE F130-NMAG               TO W-NMAG-REF.                  08310000
           MOVE F130-CFAM               TO W-CFAM-REF.                  08320000
           MOVE F130-CMARQ              TO W-CMARQ-REF.                 08330000
           MOVE F130-NSSLIEU            TO W-NSSLIEU-REF.               08340000
           MOVE F130-LREFFOURN          TO W-LREFFOURN-REF.             08350000
      *                                                                 08360000
           IF F130-PRMP NOT = ZERO                                      08370000
              COMPUTE W-VSTOCKEC = F130-QSTOCKEC1 * F130-PRMP           08380000
           END-IF.                                                      08390000
           COMPUTE W-QSTOCKEC-MAG = W-QSTOCKEC-MAG + F130-QSTOCKEC1.    08400000
           COMPUTE W-VSTOCKEC-MAG = W-VSTOCKEC-MAG + W-VSTOCKEC.        08410000
      *                                                                 08420000
           INITIALIZE              F130-QSTOCKEC1                       08430000
                                   W-VSTOCKEC.                          08440000
      *                                                                 08450000
       FIN-TRT-CUMUL-ARTICLE. EXIT.                                     08460000
      *                                                                 08470000
      *                                                                 08480000
      *================================================================ 08490000
      *                                                                 08500000
       TRT-EDIT-LIGNE        SECTION.                                   08510000
      *                                                                 08520000
            MOVE W-LIG-SAUT        TO SAUT-IIV139.                      08530000
            MOVE W-LIG-DONNEE      TO LIG-IIV139.                       08540000
            WRITE ENR-IIV139.                                           08550000
      *                                                                 08560000
       FIN-TRT-EDIT-LIGNE. EXIT.                                        08570000
      *                                                                 08580000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 08590000
      *================================================================ 08600000
      *                                                               * 08610000
      * ==>    TRT-EDIT-ENTETE                                          08620000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 08630000
      * RUPTURE   PAGE       SUR SOCIETE                              * 08640000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 08650000
      *                                                                 08660000
       TRT-EDIT-ENTETE1           SECTION.                              08670000
      *                                                                 08680000
           PERFORM TRT-ENTETE-LIGNE VARYING W-IND-LIG                   08690000
                                    FROM 1 BY 1 UNTIL W-IND-LIG > 16.   08700000
      *                                                                 08710000
       FIN-EDIT-ENTETE1. EXIT.                                          08720000
      *                                                                 08730000
       TRT-ENTETE-LIGNE           SECTION.                              08740000
      *                                                                 08750000
           IF W-IND-LIG = 1                                             08760000
              PERFORM TRT-ENTETE-LIGNE1                                 08770000
           END-IF.                                                      08780000
           IF W-IND-LIG = 2                                             08790000
              PERFORM TRT-ENTETE-LIGNE2                                 08800000
           END-IF.                                                      08810000
           IF W-IND-LIG = 3                                             08820000
              PERFORM TRT-ENTETE-LIGNE3                                 08830001
           END-IF.                                                      08840000
           IF W-IND-LIG = 4                                             08850000
              PERFORM TRT-ENTETE-LIGNE4                                 08860000
           END-IF.                                                      08870000
           IF W-IND-LIG = 5                                             08880000
              PERFORM TRT-ENTETE-LIGNE2                                 08890000
           END-IF.                                                      08900000
           IF W-IND-LIG = 6                                             08910000
              PERFORM TRT-ENTETE-LIGNE5                                 08920000
           END-IF.                                                      08930000
           IF W-IND-LIG = 7                                             08940000
              PERFORM TRT-ENTETE-LIGNE6                                 08950000
           END-IF.                                                      08960000
           IF W-IND-LIG = 8                                             08970000
              PERFORM TRT-ENTETE-LIGNE2                                 08980000
           END-IF.                                                      08990000
           IF W-IND-LIG = 9                                             09000000
              PERFORM TRT-ENTETE-LIGNE2                                 09010000
           END-IF.                                                      09020000
           IF W-IND-LIG = 10                                            09030000
              PERFORM TRT-ENTETE-LIGNE2                                 09040000
           END-IF.                                                      09050000
           IF W-IND-LIG = 11                                            09060000
              PERFORM TRT-ENTETE-LIGNE2                                 09070000
           END-IF.                                                      09080000
           IF W-IND-LIG = 12                                            09090000
              PERFORM TRT-ENTETE-LIGNE9                                 09100000
           END-IF.                                                      09110000
           IF W-IND-LIG = 13                                            09120000
              PERFORM TRT-ENTETE-LIGNE10                                09130000
           END-IF.                                                      09140000
           IF W-IND-LIG = 14                                            09150000
              PERFORM TRT-ENTETE-LIGNE11                                09160000
           END-IF.                                                      09170000
           IF W-IND-LIG = 15                                            09180000
              PERFORM TRT-ENTETE-LIGNE12                                09190000
           END-IF.                                                      09200000
           IF W-IND-LIG = 16                                            09210000
              PERFORM TRT-ENTETE-LIGNE13                                09220000
           END-IF.                                                      09230000
           PERFORM TRT-EDIT-LIGNE.                                      09240000
      *                                                                 09250000
       FIN-ENTETE-LIGNE. EXIT.                                          09260000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 09270000
      *                                                                 09280000
       TRT-ENTETE-LIGNE1          SECTION.                              09290000
      *                                                                 09300000
           MOVE SPACE                TO W-LIGNE.                        09310000
           MOVE ZERO                 TO W-LIG-PAG.                      09320000
      *    MOVE W-DATJ               TO W-LIG-DATE                      09330007
           MOVE W-DATE-AA            TO W-DAT2-AA.                      09340000
           MOVE W-DATE-MM            TO W-DAT2-MM.                      09350000
           MOVE W-DATE-JJ            TO W-DAT2-JJ.                      09360000
           ADD 1                     TO W-LIG-PAG.                      09370000
           MOVE '1'                  TO W-LIG-SAUT.                     09380000
      *    MOVE W-LIG-ENTETE1        TO W-LIG-DONNEE.                   09390008
                                                                        09400007
FQ         EVALUATE F130-CODE                                           09410007
FQ          WHEN 'F'                                                    09420007
FQ            MOVE W-DATJ               TO W-LIG-DATEF                  09430007
FQ            MOVE W-LIG-ENTETE1F       TO W-LIG-DONNEE                 09440007
FQ          WHEN 'R'                                                    09450007
FQ            MOVE W-DATJ               TO W-LIG-DATER                  09460007
FQ            MOVE W-LIG-ENTETE1R       TO W-LIG-DONNEE                 09470007
FQ          WHEN OTHER                                                  09480007
FQ            MOVE W-DATJ               TO W-LIG-DATE                   09490007
FQ            MOVE W-LIG-ENTETE1        TO W-LIG-DONNEE                 09500007
FQ         END-EVALUATE                                                 09510007
           .                                                            09520008
      *                                                                 09530000
       FIN-ENTETE-LIGNE1. EXIT.                                         09540000
      *                                                                 09550000
      *                                                                 09560000
       TRT-ENTETE-LIGNE2          SECTION.                              09570000
      *                                                                 09580000
           MOVE SPACE                TO W-LIGNE.                        09590000
           ADD 1                     TO W-LIG-PAG.                      09600000
           MOVE ' '                  TO W-LIG-SAUT.                     09610000
           MOVE W-LIG-ENTETE2        TO W-LIG-DONNEE.                   09620000
      *                                                                 09630000
       FIN-ENTETE-LIGNE2. EXIT.                                         09640000
      *                                                                 09650000
       TRT-ENTETE-LIGNE3          SECTION.                              09660000
      *                                                                 09670000
           MOVE SPACE                TO W-LIGNE.                        09680000
           ADD 1                     TO W-NUM-PAGE.                     09690000
           ADD 1                     TO W-LIG-PAG.                      09700000
           MOVE ' '                  TO W-LIG-SAUT.                     09710000
FQ                                                                      09720006
FQ         EVALUATE F130-CODE                                           09730006
FQ          WHEN 'F'                                                    09740006
FQ            MOVE W-NUM-PAGE           TO W-LIG-PAGEF                  09750006
FQ            MOVE W-LIG-ENTETE3F       TO W-LIG-DONNEE                 09760006
FQ          WHEN 'R'                                                    09770006
FQ            MOVE W-NUM-PAGE           TO W-LIG-PAGER                  09780006
FQ            MOVE W-LIG-ENTETE3R       TO W-LIG-DONNEE                 09790006
FQ          WHEN OTHER                                                  09800006
FQ            MOVE W-NUM-PAGE           TO W-LIG-PAGE                   09810006
FQ            MOVE W-LIG-ENTETE3        TO W-LIG-DONNEE                 09820006
FQ         END-EVALUATE                                                 09830006
           .                                                            09840003
      *                                                                 09850000
       FIN-ENTETE-LIGNE3. EXIT.                                         09860000
      *                                                                 09870000
       TRT-ENTETE-LIGNE4          SECTION.                              09880000
      *                                                                 09890000
           MOVE SPACE                TO W-LIGNE.                        09900000
           ADD 1                     TO W-LIG-PAG.                      09910000
           MOVE ' '                  TO W-LIG-SAUT.                     09920000
FQ                                                                      09930006
FQ         EVALUATE F130-CODE                                           09940006
FQ          WHEN 'F'                                                    09950006
FQ            MOVE W-LIG-ENTETE4F       TO W-LIG-DONNEE                 09960006
FQ          WHEN 'R'                                                    09970006
FQ            MOVE W-LIG-ENTETE4R       TO W-LIG-DONNEE                 09980006
FQ          WHEN OTHER                                                  09990006
FQ            MOVE W-LIG-ENTETE4        TO W-LIG-DONNEE                 10000006
FQ         END-EVALUATE                                                 10010006
           .                                                            10020003
      *                                                                 10030000
       FIN-ENTETE-LIGNE4. EXIT.                                         10040000
      *                                                                 10050000
       TRT-ENTETE-LIGNE5          SECTION.                              10060000
      *                                                                 10070000
           MOVE SPACE                TO W-LIGNE.                        10080000
           MOVE W-DJOUR              TO W-LIG-DJOUR.                    10090000
           ADD 1                     TO W-LIG-PAG.                      10100000
           MOVE ' '                  TO W-LIG-SAUT.                     10110000
           MOVE W-LIG-ENTETE5        TO W-LIG-DONNEE.                   10120000
      *                                                                 10130000
       FIN-ENTETE-LIGNE5. EXIT.                                         10140000
      *                                                                 10150000
      *                                                                 10160000
       TRT-ENTETE-LIGNE6          SECTION.                              10170000
      *                                                                 10180000
           MOVE SPACE                TO W-LIGNE.                        10190000
           MOVE W-NSOCMAG-REF        TO W-LIG-NSOCMAG.                  10200000
           ADD 1                     TO W-LIG-PAG.                      10210000
           MOVE ' '                  TO W-LIG-SAUT.                     10220000
           MOVE W-LIG-ENTETE6        TO W-LIG-DONNEE.                   10230000
      *                                                                 10240000
       FIN-ENTETE-LIGNE6. EXIT.                                         10250000
      *                                                                 10260000
      *                                                                 10270000
       TRT-ENTETE-LIGNE9         SECTION.                               10280000
      *                                                                 10290000
           MOVE SPACE                TO W-LIGNE.                        10300000
           ADD 1                     TO W-LIG-PAG.                      10310000
           MOVE '+'                  TO W-LIG-SAUT.                     10320000
           MOVE W-LIG-ENTETE9        TO W-LIG-DONNEE.                   10330000
      *                                                                 10340000
       FIN-ENTETE-LIGNE9. EXIT.                                         10350000
      *                                                                 10360000
       TRT-ENTETE-LIGNE10        SECTION.                               10370000
      *                                                                 10380000
           MOVE SPACE                TO W-LIGNE.                        10390000
           ADD 1                     TO W-LIG-PAG.                      10400000
           MOVE ' '                  TO W-LIG-SAUT.                     10410000
           MOVE W-LIG-ENTETE10       TO W-LIG-DONNEE.                   10420000
      *                                                                 10430000
       FIN-ENTETE-LIGNE10. EXIT.                                        10440000
      *                                                                 10450000
       TRT-ENTETE-LIGNE11         SECTION.                              10460000
      *                                                                 10470000
           MOVE SPACE                TO W-LIGNE.                        10480000
           ADD 1                     TO W-LIG-PAG.                      10490000
           MOVE ' '                  TO W-LIG-SAUT.                     10500000
           MOVE W-LIG-ENTETE11       TO W-LIG-DONNEE.                   10510000
      *                                                                 10520000
       FIN-ENTETE-LIGNE11. EXIT.                                        10530000
      *                                                                 10540000
       TRT-ENTETE-LIGNE12         SECTION.                              10550000
      *                                                                 10560000
           MOVE SPACE                TO W-LIGNE.                        10570000
           ADD 1                     TO W-LIG-PAG.                      10580000
           MOVE ' '                  TO W-LIG-SAUT.                     10590000
           MOVE W-LIG-ENTETE12       TO W-LIG-DONNEE.                   10600000
      *                                                                 10610000
       FIN-ENTETE-LIGNE12. EXIT.                                        10620000
      *                                                                 10630000
      *                                                                 10640000
       TRT-ENTETE-LIGNE13         SECTION.                              10650000
      *                                                                 10660000
           MOVE SPACE                TO W-LIGNE.                        10670000
           ADD 1                     TO W-LIG-PAG.                      10680000
           MOVE ' '                  TO W-LIG-SAUT.                     10690000
           MOVE W-LIG-ENTETE13       TO W-LIG-DONNEE.                   10700000
      *                                                                 10710000
       FIN-ENTETE-LIGNE13. EXIT.                                        10720000
      *                                                                 10730000
      * -AIDA ********************************************************* 10740000
      *                                                               * 10750000
      *  SSSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  * 10760000
      *  SSSSSSSS  OOOOOOOOO RRRRRRRRR TTTTTTTTT IIIIIIIII EEEEEEEEE  * 10770000
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        * 10780000
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        * 10790000
      *    SSSS    OOO   OOO RRR   RRR    TTT       III    EEEEEEEEE  * 10800000
      *     SSSS   OOO   OOO RRRRRRRRR    TTT       III    EEEEEEEEE  * 10810000
      *      SSSS  OOO   OOO RRRRRRRR     TTT       III    EEE        * 10820000
      *      SSSS  OOO   OOO RRR   RR     TTT       III    EEE        * 10830000
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  * 10840000
      *  SSSSSSSS   OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  * 10850000
      *                                                               * 10860000
      ***************************************************************** 10870000
      *                                                                 10880000
       MODULE-SORTIE              SECTION.                              10890000
      *                                                                 10900000
      *                                                                 10910000
           PERFORM DISP-COMPTEURS.                                      10920000
           CLOSE FIV130.                                                10930000
           CLOSE IIV139.                                                10940000
           GOBACK.                                                      10950000
      *                                                                 10960000
       FIN-MODULE-SORTIE. EXIT.                                         10970000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   10980000
      *                 *      COMPTEURS                                10990000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   11000000
      *                                                                 11010000
       DISP-COMPTEURS SECTION.                                          11020000
      *                                                                 11030000
            DISPLAY '************************************************'. 11040000
            DISPLAY '*****  EDITION DES COMPTEURS  PGM :BIV139  *****'. 11050000
            DISPLAY '************************************************'. 11060000
            DISPLAY 'DATE DE TRAITEMENT : ' W-DAT2.                     11070000
            DISPLAY '******     NB CODICS LUS DANS FIV130         = '   11080000
                                             W-LUS-FIV130.              11090000
            DISPLAY '************************************************'. 11100000
      *                                                                 11110000
       FIN-DISP-COMPTEURS. EXIT.                                        11120000
      *                                                                 11130000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   11140000
      *                 *      FIN ANORMALE                             11150000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   11160000
      *                                                                 11170000
       ABEND-PROGRAMME SECTION.                                         11180000
      *                                                                 11190000
           PERFORM DISP-COMPTEURS.                                      11200000
           CLOSE FIV130, IIV139.                                        11210000
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    11220000
      *                                                                 11230000
       FIN-ABEND-PROGRAMME. EXIT.                                       11240000
      *                                                                 11250000
       IG3-COPY SECTION. CONTINUE. COPY SYBCERRO.                       11260000
