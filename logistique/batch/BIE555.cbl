      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
      *                                                                         
       PROGRAM-ID.  BIE555.                                                     
       AUTHOR.  DSA014.                                                         
       DATE-WRITTEN.  26/06/90.                                                 
      *                                                                         
      *************************************************************             
      *                                                           *             
      *   EDITION DU STOCK DISPONIBLE NEGATIF APRES INVENTAIRE    *             
      *                                                           *             
      *************************************************************             
      * MODIF JA NLG2 LE 7/04/2000                                *             
      * PASSAGE UNIQUE DU PROGRAMME POUR L ENSEMBLE DES FILIALES  *             
      *************************************************************             
      *                                                                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
      *                                                                         
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FIE555  ASSIGN TO FIE555.                                     
      *--                                                                       
           SELECT FIE555  ASSIGN TO FIE555                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT JIE555  ASSIGN TO JIE555.                                     
      *--                                                                       
           SELECT JIE555  ASSIGN TO JIE555                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
      *                                                                         
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE FDATE                          * 00099058
      *---------------------------------------------------------------* 00100055
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FDATE.                                                          
            03  FILLER      PIC X(80).                                          
      *                                                                 00097055
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE FIE555                         * 00099058
      *---------------------------------------------------------------* 00100055
       FD   FIE555                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FIE555.                                                         
            03  FILLER      PIC X(093).                                         
      *                                                                 00097055
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE JIE555                         * 00099058
      *---------------------------------------------------------------* 00100055
       FD   JIE555                                                              
            LABEL RECORD STANDARD.                                              
       01   ENR-JIE555.                                                         
            03  FILLER      PIC X(133).                                         
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                 W O R K I N G  -  S T O R A G E               *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                                 
      *------------------------                                                 
      *                                                                         
      *===============================================================*         
      *                 DESCRIPTION DE FDATE                          *         
      *===============================================================*         
       01  FDATE-ENREG.                                                         
            02  FDATE-JJ       PIC X(02).                                       
            02  FDATE-MM       PIC X(02).                                       
            02  FDATE-SS       PIC X(02).                                       
            02  FDATE-AA       PIC X(02).                                       
            02  FILLER         PIC X(72).                                       
      *                                                                         
      *===============================================================*         
      *                 DESCRIPTION DE FIE555                         *         
      *===============================================================*         
       01  FIE555-ENREG.                                                        
           05  FIE555-NSOCDEPOT              PIC X(3).                          
           05  FIE555-NDEPOT                 PIC X(3).                          
           05  FIE555-NCODIC                 PIC X(7).                          
           05  FIE555-REFERENCE              PIC X(20).                         
           05  FIE555-WSEQFAM                PIC S9(5)      COMP-3.             
           05  FIE555-CMARQ                  PIC X(5).                          
           05  FIE555-CFAM                   PIC X(5).                          
           05  FIE555-QSTOCKVEND             PIC S9(5)      COMP-3.             
           05  FIE555-QSTOCKDISPO            PIC S9(5)      COMP-3.             
           05  FIE555-QSTOCKRES              PIC S9(5)      COMP-3.             
           05  FIE555-TYPRES                 PIC X.                             
           05  FIE555-NSOCDEMANDEUR          PIC X(3).                          
           05  FIE555-NDEPDEMANDEUR          PIC X(3).                          
           05  FIE555-NVENTE                 PIC X(7).                          
           05  FIE555-NMUTATION              PIC X(7).                          
           05  FIE555-MODDELIV               PIC X(3).                          
           05  FIE555-DATEDELIV              PIC X(8).                          
           05  FIE555-QTRESERVE              PIC S9(5)      COMP-3.             
           05  FILLER                        PIC X(3) VALUE SPACES.             
      *                                                                         
      *                                                                         
      *                                                                         
       01  ETAT-FIE555                   PIC X.                                 
           88  FIE555-FINI               VALUE '0'.                             
           88  FIE555-NON-FINI           VALUE '1'.                             
      *                                                                         
       01  W-LIGNES-FIE555-LUES          PIC S9(9) COMP-3  VALUE +0.            
       01  W-LIGNES-JIE555-ECRITES       PIC S9(9) COMP-3  VALUE +0.            
      *                                                                         
      *---------------------------------------------------------------*         
      *              LIGNES DE L'ETAT JIE555                          *         
      *---------------------------------------------------------------*         
      *                                                                         
       01  SAUT-PAGE                 PIC X VALUE '1'.                           
       01  SAUT-LIGNE                PIC X VALUE '-'.                           
      *                                                                         
       01  LIGNE-TIRETS.                                                        
           02  FILLER                PIC XX VALUE ' !'.                         
           02  FILLER                PIC X(129) VALUE ALL '-'.                  
           02  FILLER                PIC XX VALUE '! '.                         
      *                                                                         
       01  LIGNE-RESULTAT.                                                      
           02  FILLER                 PIC X(4) VALUE ' !  '.                    
           02  ETAT-CFAM              PIC X(5).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-CMARQ             PIC X(5).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-REFERENCE         PIC X(20).                                
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-NCODIC            PIC X(7).                                 
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  ETATX-QSTOCKVEND.                                                
               03  ETAT-QSTOCKVEND    PIC -(5)9.                                
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  ETATX-QSTOCKDISPO.                                               
               03  ETAT-QSTOCKDISPO   PIC -(5)9.                                
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  ETATX-QSTOCKRES.                                                 
               03  ETAT-QSTOCKRES     PIC -(5)9.                                
           02  FILLER                 PIC X(3) VALUE ' ! '.                     
           02  ETAT-TYPRES            PIC X.                                    
           02  FILLER                 PIC X(3) VALUE ' ! '.                     
           02  ETAT-NSOCDEMANDEUR     PIC X(3).                                 
           02  FILLER                 PIC X   VALUE SPACE.                      
           02  ETAT-NDEPDEMANDEUR     PIC X(3).                                 
           02  FILLER                 PIC X(3) VALUE ' ! '.                     
           02  ETAT-NVENTE            PIC X(7).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-NMUTATION         PIC X(7).                                 
           02  FILLER                 PIC X(3) VALUE ' ! '.                     
           02  ETAT-MODDELIV          PIC X(3).                                 
           02  FILLER                 PIC X(3) VALUE ' ! '.                     
           02  ETAT-DATEDELIV         PIC X(8).                                 
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  ETAT-QTRESERVE         PIC -(6).                                 
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
      *                                                                         
       01  LIGNE-ENTETE1.                                                       
           02  FILLER                 PIC X VALUE SPACE.                        
           02  FILLER                 PIC X(6) VALUE 'JIE555'.                  
           02  FILLER                 PIC X(47) VALUE SPACES.                   
           02  FILLER                 PIC X(18)                                 
                 VALUE 'DISPONIBLE NEGATIF'.                                    
           02  FILLER                 PIC X(47) VALUE SPACES.                   
           02  FILLER                 PIC X(07) VALUE 'PAGE : '.                
           02  ENTETE-PAGE            PIC X(3).                                 
           02  FILLER                 PIC X(4) VALUE SPACES.                    
      *                                                                         
       01  LIGNE-ENTETE2.                                                       
           02  FILLER                 PIC X VALUE '-'.                          
           02  FILLER                 PIC X(7) VALUE 'DATE : '.                 
           02  ENTETE-DATE            PIC X(8).                                 
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(7) VALUE 'TIME : '.                 
           02  ENTETE-TIME            PIC X(5).                                 
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(10) VALUE 'SOCIETE : '.             
           02  ENTETE-SOCIETE         PIC X(3).                                 
           02  FILLER                 PIC X(86) VALUE SPACES.                   
      *                                                                         
       01  LIGNE-ENTETE3.                                                       
           02  FILLER                 PIC X VALUE '-'.                          
           02  FILLER                 PIC X(11) VALUE 'ENTREPOT : '.            
           02  ENTETE-ENTREPOT        PIC X(3).                                 
           02  FILLER                 PIC X(118) VALUE SPACES.                  
      *                                                                         
      *                                                                         
       01  LIGNE1.                                                              
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(42) VALUE SPACES.                   
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(23) VALUE                           
                                             ' QUANTITE  EN  STOCK   '.         
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X    VALUE 'T'.                       
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(9) VALUE SPACES.                    
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X(17) VALUE SPACES.                   
           02  FILLER                 PIC X(1) VALUE '!'.                       
           02  FILLER                 PIC X(16) VALUE                           
                                            '   DELIVRANCE   '.                 
           02  FILLER                 PIC X(1) VALUE '!'.                       
           02  FILLER                 PIC X(8) VALUE SPACES.                    
           02  FILLER                 PIC X(2) VALUE '! '.                      
      *                                                                         
       01  LIGNE2.                                                              
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(42) VALUE SPACES.                   
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(23) VALUE ALL '-'.                  
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X    VALUE 'Y'.                       
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(9) VALUE ' LIEU    '.               
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X(5) VALUE 'NO DE'.                   
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(8) VALUE ' NO DE  '.                
           02  FILLER                 PIC X(1) VALUE '!'.                       
           02  FILLER                 PIC X(16) VALUE ALL '-'.                  
           02  FILLER                 PIC X(1) VALUE '!'.                       
           02  FILLER                 PIC X(8) VALUE 'QUANTITE'.                
           02  FILLER                 PIC X(2) VALUE '! '.                      
      *                                                                         
       01  LIGNE3.                                                              
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(3) VALUE 'FAM'.                     
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(6) VALUE 'MARQUE'.                  
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(9) VALUE 'REFERENCE'.               
           02  FILLER                 PIC X(12) VALUE SPACES.                   
           02  FILLER                 PIC X(5) VALUE 'CODIC'.                   
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
           02  FILLER                 PIC X(4) VALUE 'VEND'.                    
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
           02  FILLER                 PIC X(5) VALUE 'DISPO'.                   
           02  FILLER                 PIC X(4) VALUE ' !  '.                    
           02  FILLER                 PIC X(3) VALUE 'RES'.                     
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
           02  FILLER                 PIC X    VALUE 'P'.                       
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(9) VALUE 'DEMANDEUR'.               
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X(5) VALUE 'VENTE'.                   
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(8) VALUE 'MUTATION'.                
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X(4) VALUE 'MODE'.                    
           02  FILLER                 PIC X(4) VALUE '!   '.                    
           02  FILLER                 PIC X(4) VALUE 'DATE'.                    
           02  FILLER                 PIC X(4) VALUE '   !'.                    
           02  FILLER                 PIC X(8) VALUE 'RESERVEE'.                
           02  FILLER                 PIC X(2) VALUE '! '.                      
      *                                                                         
       01  LIGNE-VIDE.                                                          
           02  FILLER                 PIC X(5) VALUE ' !   '.                   
           02  FILLER                 PIC X(42) VALUE SPACES.                   
           02  FILLER                 PIC X(3) VALUE ' ! '.                     
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
           02  FILLER                 PIC X(5) VALUE SPACES.                    
           02  FILLER                 PIC X(4) VALUE ' !  '.                    
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
           02  FILLER                 PIC X    VALUE SPACE.                     
           02  FILLER                 PIC X(2) VALUE ' !'.                      
           02  FILLER                 PIC X(9) VALUE SPACES.                    
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X(5) VALUE SPACES.                    
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(8) VALUE SPACES.                    
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(4) VALUE '!   '.                    
           02  FILLER                 PIC X(4) VALUE SPACES.                    
           02  FILLER                 PIC X(4) VALUE '   !'.                    
           02  FILLER                 PIC X(8) VALUE SPACES.                    
           02  FILLER                 PIC X(2) VALUE '! '.                      
      *                                                                         
       01  WX-QSTOCKVEND.                                                       
           02  W9-QSTOCKVEND         PIC S9(5).                                 
       01  WX-QSTOCKDISPO.                                                      
           02  W9-QSTOCKDISPO        PIC S9(5).                                 
       01  WX-QSTOCKRES.                                                        
           02  W9-QSTOCKRES          PIC S9(5).                                 
       01  WX-QTRESERVE.                                                        
           02  W9-QTRESERVE          PIC S9(5).                                 
      *                                                                         
       01  NLIGNE                    PIC 9(2).                                  
       01  NLIGNE-INIT               PIC 9(2) VALUE 15.                         
       01  NLIGNE-MAX                PIC 9(2) VALUE 55.                         
       01  W-PAGE                    PIC 9(3).                                  
       01  NCODIC-COURANT            PIC X(7).                                  
       01  ENTREPOT-COURANT          PIC X(3).                                  
       01  NSOCDEPOT-COURANT         PIC X(3).                                  
      *                                                                         
       01  W1-DATE-ACCEPT.                                                      
           02  W1-AA-SM                  PIC XX.                                
           02  W1-MM-SM                  PIC XX.                                
           02  W1-JJ-SM                  PIC XX.                                
       01  W1-DATE.                                                             
           02  W1-SS                     PIC XX.                                
           02  W1-AA                     PIC XX.                                
           02  W1-MM                     PIC XX.                                
           02  W1-JJ                     PIC XX.                                
       01  W2-DATE.                                                             
           02  W2-JJ                     PIC XX.                                
           02  FILLER                    PIC X VALUE '/'.                       
           02  W2-MM                     PIC XX.                                
           02  FILLER                    PIC X VALUE '/'.                       
           02  W2-AA                     PIC XX.                                
      *                                                                         
       01  W-TIME                        PIC 9(8).                              
       01  W1-TIME       REDEFINES W-TIME.                                      
           02  W1-TIME-HH                PIC 99.                                
           02  W1-TIME-MM                PIC 99.                                
           02  W1-TIME-SS                PIC 99.                                
           02  W1-TIME-CC                PIC 99.                                
       01  W2-TIME.                                                             
           02  W2-TIME-HH                PIC 99.                                
           02  FILLER                    PIC X VALUE 'H'.                       
           02  W2-TIME-MM                PIC 99.                                
      *                                                                         
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E   D I V I S I O N               *         
      *****************************************************************         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *---------------------------------------------------------------*         
      *              D E B U T   B I E 5 5 5                          *         
      *---------------------------------------------------------------*         
      *                                                                         
      *==============================================================*          
      *                                                              *          
      *==============================================================*          
      *                                                                         
      *                                                                         
       MODULE-BIE555              SECTION.                                      
      *---------------                                                          
      *                                                                         
           PERFORM  DEBUT.                                                      
           PERFORM  TRAITEMENT.                                                 
           PERFORM  FIN.                                                        
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-BIE555.  EXIT.                                                
              EJECT                                                             
      *                                                                         
      *===============================================================*         
      *                     D E B U T                                 *         
      *===============================================================*         
      *                                                                         
       DEBUT                 SECTION.                                           
      *------------------------------                                           
      *                                                                         
           DISPLAY  '************************************************'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '**   PROGRAMME  :  BIE555                     **'.         
           DISPLAY  '**   EDITION DU STOCK DISPONIBLE NEGATIF      **'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '************************************************'.         
           DISPLAY  '***     PROGRAMME DEBUTE NORMALEMENT         ***'.         
      *                                                                         
           OPEN  INPUT  FDATE                                                   
                        FIE555.                                                 
      *    DISPLAY '  OPEN  FICHIERS DATE FIE555'.                              
           OPEN OUTPUT  JIE555.                                                 
      *    DISPLAY '  01-OPEN  FICHIERS       ' .                               
      *                                                                         
      *                                                                         
           READ   FDATE  INTO   FDATE-ENREG   AT END                            
                MOVE  ' FDATE ! DATE DU JOUR            '                       
                                       TO   ABEND-MESS                          
                MOVE  01                     TO   ABEND-CODE                    
                PERFORM  ABANDON-PROGRAMME.                                     
      *                                                                         
      *                                                                         
           ACCEPT W1-DATE-ACCEPT FROM DATE.                                     
           MOVE W1-JJ-SM TO W2-JJ.                                              
           MOVE W1-MM-SM TO W2-MM.                                              
           MOVE W1-AA-SM TO W2-AA.                                              
           MOVE W2-DATE  TO ENTETE-DATE.                                        
      *                                                                         
      *                                                                         
      *    DISPLAY '  LECTURE  FDATE          ' .                               
      *                                                                         
      *---------------------------------------------------------------*         
      *            RECHERCHE DE L'HEURE                               *         
      *---------------------------------------------------------------*         
           ACCEPT W-TIME FROM TIME.                                             
           MOVE W1-TIME-HH TO W2-TIME-HH.                                       
           MOVE W1-TIME-MM TO W2-TIME-MM.                                       
           MOVE W2-TIME TO ENTETE-TIME.                                         
      *                                                                         
      *                                                                         
       FIN-DEBUT.                 EXIT.                                         
      *                                                                         
      *===============================================================*         
      *       F I N   N O R M A L E   D U   P R O G R A M M E         *         
      *===============================================================*         
      *                                                                         
       FIN                        SECTION.                                      
      *-----------------------------------                                      
      *                                                                         
           DISPLAY  '************************************************'.         
           DISPLAY  '***       FIN  NORMALE  DU PROGRAMME         ***'.         
           DISPLAY  '***                 BIE555                   ***'.         
           DISPLAY  '************************************************'.         
           DISPLAY  '** NBR DE LIGNES FIE555 LUES          : '          07480000
                    W-LIGNES-FIE555-LUES.                               07490000
           DISPLAY  '** NBR DE LIGNES JIE555 ECRITES       : '          07500000
                    W-LIGNES-JIE555-ECRITES                             07510000
      *                                                                         
           CLOSE  FDATE.                                                        
           CLOSE  FIE555.                                                       
           CLOSE  JIE555.                                                       
      *                                                                         
       FIN-FIN.        EXIT.                                                    
      *                                                                         
      *==============================================================*          
      *                  T R A I T E M E N T                         *          
      *==============================================================*          
       TRAITEMENT    SECTION.                                                   
      *                                                                         
           MOVE ZERO TO  W-PAGE.                                                
           SET FIE555-NON-FINI TO TRUE.                                         
           PERFORM LIRE-FIE555.                                                 
           MOVE FIE555-NDEPOT    TO ENTREPOT-COURANT.                           
           MOVE FIE555-NSOCDEPOT TO NSOCDEPOT-COURANT.                          
           PERFORM TRAITER-PAGE UNTIL FIE555-FINI.                              
      *                                                                         
       FIN-TRAITEMENT.             EXIT.                                        
      *                                                                         
      *****************************************************************         
      *     PROCEDURE : TRAITER-PAGE                                  *         
      *                                                               *         
      *     FONCTION  : ECRITURE D'UNE PAGE D'EDITION                 *         
      *                                                               *         
      *****************************************************************         
       TRAITER-PAGE               SECTION.                                      
      *                                                                         
           ADD 1 TO W-PAGE.                                                     
      *                                                                         
           IF FIE555-NSOCDEPOT NOT = NSOCDEPOT-COURANT                          
           OR FIE555-NDEPOT    NOT = ENTREPOT-COURANT                           
              MOVE 1 TO W-PAGE                                                  
           END-IF                                                               
           MOVE W-PAGE           TO ENTETE-PAGE.                                
           MOVE FIE555-NSOCDEPOT TO ENTETE-SOCIETE.                             
           MOVE FIE555-NDEPOT    TO ENTETE-ENTREPOT.                            
           MOVE FIE555-NDEPOT    TO ENTREPOT-COURANT.                           
           MOVE FIE555-NSOCDEPOT TO NSOCDEPOT-COURANT.                          
      *                                                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *          ECRITURE DE L'ENTETE POUR UNE PAGE                   *         
      *---------------------------------------------------------------*         
           WRITE ENR-JIE555 FROM LIGNE-ENTETE1 AFTER PAGE.                      
           WRITE ENR-JIE555 FROM LIGNE-ENTETE2 AFTER 3.                         
           WRITE ENR-JIE555 FROM LIGNE-ENTETE3 AFTER 2.                         
           WRITE ENR-JIE555 FROM LIGNE-TIRETS  AFTER 3.                         
           WRITE ENR-JIE555 FROM LIGNE1.                                        
           WRITE ENR-JIE555 FROM LIGNE2.                                        
           WRITE ENR-JIE555 FROM LIGNE3.                                        
           WRITE ENR-JIE555 FROM LIGNE-VIDE.                                    
           WRITE ENR-JIE555 FROM LIGNE-TIRETS.                                  
           WRITE ENR-JIE555 FROM LIGNE-VIDE.                                    
      *                                                                         
           MOVE NLIGNE-INIT TO NLIGNE.                                          
      *---------------------------------------------------------------*         
      *        APPEL DE LA BOUCLE DE TRAITEMENT POUR UN CODIC         *         
      *---------------------------------------------------------------*         
           PERFORM TRAITER-CODIC UNTIL FIE555-FINI                              
                           OR NLIGNE > NLIGNE-MAX                               
                           OR FIE555-NDEPOT NOT = ENTREPOT-COURANT              
                           OR FIE555-NSOCDEPOT NOT = NSOCDEPOT-COURANT.         
      *                                                                         
           WRITE ENR-JIE555 FROM LIGNE-TIRETS.                                  
      *                                                                         
       FIN-TRAITER-PAGE.           EXIT.                                        
      *                                                                         
      *****************************************************************         
      *        PROCEDURE : TRAITER-CODIC                              *         
      *                                                               *         
      *        FONCTION  : ECRITURE DE TOUTES LES LIGNES POUR 1 CODIC *         
      *                                                               *         
      *****************************************************************         
       TRAITER-CODIC              SECTION.                                      
      *                                                                         
           MOVE FIE555-NCODIC TO NCODIC-COURANT.                                
      *                                                                         
           MOVE FIE555-CFAM        TO ETAT-CFAM.                                
           MOVE FIE555-CMARQ       TO ETAT-CMARQ.                               
           MOVE FIE555-REFERENCE   TO ETAT-REFERENCE.                           
           MOVE FIE555-NCODIC      TO ETAT-NCODIC.                              
           MOVE FIE555-QSTOCKVEND  TO ETAT-QSTOCKVEND.                          
      *    MOVE WX-QSTOCKVEND      TO ETAT-QSTOCKVEND.                          
           MOVE FIE555-QSTOCKDISPO TO ETAT-QSTOCKDISPO.                         
      *    MOVE WX-QSTOCKDISPO     TO ETAT-QSTOCKDISPO.                         
           MOVE FIE555-QSTOCKRES   TO ETAT-QSTOCKRES.                           
      *    MOVE WX-QSTOCKRES       TO ETAT-QSTOCKRES.                           
           PERFORM ECRIRE-LIGNE.                                                
           ADD 1 TO NLIGNE.                                                     
      *                                                                         
           PERFORM LIRE-FIE555.                                                 
      *                                                                         
           MOVE SPACES             TO ETAT-CFAM                                 
                                      ETAT-CMARQ                                
                                      ETAT-REFERENCE                            
                                      ETAT-NCODIC                               
                                      ETATX-QSTOCKVEND                          
                                      ETATX-QSTOCKDISPO                         
                                      ETATX-QSTOCKRES.                          
      *                                                                         
      *                                                                         
           PERFORM UNTIL  FIE555-FINI                                           
                       OR NLIGNE            > NLIGNE-MAX                        
                       OR FIE555-NCODIC NOT = NCODIC-COURANT                    
                       OR FIE555-NDEPOT NOT = ENTREPOT-COURANT                  
                       OR FIE555-NSOCDEPOT NOT = NSOCDEPOT-COURANT              
      *                                                                         
                   PERFORM ECRIRE-LIGNE                                         
                   ADD 1 TO NLIGNE                                              
                   PERFORM LIRE-FIE555                                          
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
           WRITE ENR-JIE555 FROM LIGNE-VIDE.                                    
           ADD 1 TO NLIGNE.                                                     
      *                                                                         
       FIN-TRAITER-CODIC.            EXIT.                                      
      *                                                                         
      *                                                                         
      *****************************************************************         
      *        PROCEDURE : ECRIRE-LIGNE                               *         
      *                                                               *         
      *        FONCTION  : ECRITURE D'UNE LIGNE DE L'ETAT             *         
      *                                                               *         
      *****************************************************************         
       ECRIRE-LIGNE               SECTION.                                      
      *                                                                         
           MOVE FIE555-TYPRES        TO ETAT-TYPRES.                            
           MOVE FIE555-NSOCDEMANDEUR TO ETAT-NSOCDEMANDEUR.                     
           MOVE FIE555-NDEPDEMANDEUR TO ETAT-NDEPDEMANDEUR.                     
           MOVE FIE555-NVENTE        TO ETAT-NVENTE.                            
           MOVE FIE555-NMUTATION     TO ETAT-NMUTATION.                         
           MOVE FIE555-MODDELIV      TO ETAT-MODDELIV.                          
           IF   FIE555-DATEDELIV NOT = SPACES                                   
                MOVE FIE555-DATEDELIV     TO W1-DATE                            
                MOVE W1-JJ                TO W2-JJ                              
                MOVE W1-MM                TO W2-MM                              
                MOVE W1-AA                TO W2-AA                              
                MOVE W2-DATE              TO ETAT-DATEDELIV                     
           ELSE                                                                 
                MOVE SPACES          TO ETAT-DATEDELIV                          
           END-IF.                                                              
           MOVE FIE555-QTRESERVE     TO ETAT-QTRESERVE.                         
      *    MOVE WX-QTRESERVE         TO ETAT-QTRESERVE.                         
      *                                                                         
           WRITE ENR-JIE555 FROM LIGNE-RESULTAT.                                
           ADD 1 TO W-LIGNES-JIE555-ECRITES.                                    
      *                                                                         
       FIN-ECRIRE-LIGNE.            EXIT.                                       
      *                                                                         
      *                                                                         
      *****************************************************************         
      *        PROCEDURE : LIRE-FIE555                                *         
      *                                                               *         
      *        FONCTION  : LECTURE DU FICHIER DES ECARTS D'INVENTAIRE *         
      *                    FIE555.                                    *         
      *                                                               *         
      *****************************************************************         
       LIRE-FIE555                  SECTION.                                    
      *                                                                         
           READ   FIE555 INTO   FIE555-ENREG   AT END                           
                SET FIE555-FINI TO TRUE.                                        
           IF FIE555-NON-FINI                                                   
                ADD 1 TO W-LIGNES-FIE555-LUES                                   
           END-IF.                                                              
      *                                                                         
       FIN-LIRE-FIE555.             EXIT.                                       
      *                                                                         
      *                                                                         
      *                                                                         
      *---------------------------------------------------------------*         
      *             A B A N D O N - P R O G R A M M E                 *         
      *---------------------------------------------------------------*         
      *                                                                         
       ABANDON-PROGRAMME        SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           CLOSE  FDATE.                                                        
           CLOSE  FIE555.                                                       
           CLOSE  JIE555.                                                       
      *                                                                         
           DISPLAY '***************************************'.                   
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.                   
           DISPLAY '***************************************'.                   
      *                                                                         
           MOVE  'BIE555'  TO    ABEND-PROG.                                    
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
      *                                                                         
       FIN-ABANDON-PROGRAMME. EXIT.                                             
          EJECT                                                                 
      *                                                                         
