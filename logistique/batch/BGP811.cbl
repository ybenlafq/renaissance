      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BGP811                                                    
       AUTHOR. DSA052.                                                          
      *------------------------------------------------------------*            
      *                PROGRAMME D'EDITION DE L'ETAT               *            
      *                DE SUIVI DES PRODUITS A CALER               *            
      * ---------------------------------------------------------- *            
      * DATE CREATION : 17/04/2009                                 *            
      * ---------------------------------------------------------- *            
      *                                                            *            
      * ---------------------------------------------------------- *            
      * DATE MODIF : GESTION DES BL EN AUTOMATIQUE (D004332)       *            
      *                                                            *            
      *------------------------------------------------------------*            
E0576 * 07/06/2011 EVOL D003686 - L.ARMANT                                      
      *                    RAJOUT DES QUANTITES RESERVEES SUR COMMANDE          
      *                    FOURNISSEUR ET DU DEPOT EXPEDITEUR                   
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGP810  ASSIGN TO FGP810.                                     
      *--                                                                       
           SELECT FGP810  ASSIGN TO FGP810                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IGP810 ASSIGN TO IGP810.                                      
      *--                                                                       
           SELECT IGP810 ASSIGN TO IGP810                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F D A T E            *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENREG.                                                 00000030
           02 FDATE-JJ              PIC X(02).                                  
           02 FDATE-MM              PIC X(02).                                  
           02 FDATE-SS              PIC X(02).                                  
           02 FDATE-AA              PIC X(02).                                  
           02 FILLER                PIC X(72).                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F G P 8 1 0          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FGP810                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
E0576 *01  ENREG-FGP810 PIC X(150).                                             
E0576  01  ENREG-FGP810 PIC X(200).                                             
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    I G P 8 1 0          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  IGP810                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENREG-IGP810 PIC X(133).                                             
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
       77  W-ZONE    PIC XX.                                                    
      *                                                                         
        COPY ABENDCOP.                                                          
      *                                                                         
        COPY WORKDATC.                                                          
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
       01  LIGNE-IGP810 .                                                       
           02 SAUT                    PIC X.                                    
           02 LIGNE                   PIC X(132).                               
       01  W-DATE.                                                              
           03  DATJMSA.                                                         
               05 JJ                  PIC  XX.                                  
               05 MM                  PIC  XX.                                  
               05 SS                  PIC  XX.                                  
               05 AA                  PIC  XX.                                  
           03  W-TIME.                                                          
               05 HH                  PIC  XX.                                  
               05 MN                  PIC  XX.                                  
               05 FILLER              PIC  X(14).                               
       01  JOUR-SEM                   PIC X.                                    
       01  DATSAMJ                    PIC X(08).                                
       01  DATSM-9                    PIC 9(06).                                
      *                                                                         
       01  RUPTURE                    PIC 99 VALUE 15.                          
      *                                                                         
       01  CTR-FGP810                 PIC 9(9) VALUE 0.                         
      *                                                                         
       01  FLAG-FICHIER               PIC X VALUE SPACE.                        
           88  EOF                          VALUE 'F'.                          
       01  FLAG-ENTETE                PIC X VALUE SPACE.                        
           88  RUPTURE-ENTETE               VALUE ' '.                          
           88  ENTETE-ECRITE                VALUE 'O'.                          
E0576 * VARIABLE DETERMINANT SI LE DEPOT EXPEDITEUR DOIT ETRE AFFICHE           
E0576  01  RECUP-DEPOT-EXP            PIC X VALUE SPACE.                        
E0576      88  LB-DEPOTEXP                  VALUE 'O'.                          
      *                                                                         
      *                                                                         
      *ENREGISTREMENT COURANT                                                   
      *01 COPY-FGP810.                                                          
              COPY FGP810 REPLACING ==:FGPXXX:== BY ==FGP810==.                 
      *                                                                         
      *ENREGISTREMENT PRECEDENT                                                 
      *01 COPY-W-FGP810.                                                        
              COPY FGP810 REPLACING ==:FGPXXX:== BY ==W-FGP810==.               
      *                                                                         
      * --------------- TABLEAU T1 TOTAUX PAR FAMILLE --------------            
       01 I                                  PIC S9(4) COMP-3 VALUE 0.          
      *-----------------------------------------------------------------        
      *                                                                         
      * ZONES POUR LE TRI BULLE DES TABLEAUX                                    
      *                                                                         
      * BOOLEENS                                                                
       01  WS-TOP-TRI                 PIC 9    VALUE ZERO.                      
           88 FIN-TRI-OK                              VALUE 1.                  
           88 FIN-TRI-KO                              VALUE 0.                  
      * INDICES                                                                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  NB-T-1                     PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  NB-T-1                     PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  J-TRI                      PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  J-TRI                      PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  J                          PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  J                          PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  K                          PIC S9(03) COMP          VALUE +0.        
      *--                                                                       
       77  K                          PIC S9(03) COMP-5          VALUE          
                                                                     +0.        
      *}                                                                        
      ******************************************************************        
      *                                                                         
      *                                                                         
       01  CTR-PAGE     PIC 9(4) VALUE 0.                                       
      *MASQUE D EDITION                                                         
       01  W-MASKED3                         PIC ZZ9.                           
       01  W-MASKED3S                        PIC ---9.                          
       01  W-MASKED4                         PIC ZZZ9.                          
       01  W-MASKED4S                        PIC ----9.                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL5                        PIC S9(4)  COMP.                   
      *--                                                                       
       01  W-MASKCAL5                        PIC S9(4) COMP-5.                  
      *}                                                                        
       01  W-MASKED5                         PIC ZZZZ9.                         
       01  W-MASKED5S                        PIC -----9.                        
       01  W-MASKED5V                        PIC ZZ9,9.                         
E0555  01  W-MASKED7V                        PIC ZZZ9,99.                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL6                        PIC S9(5)  COMP.                   
      *--                                                                       
       01  W-MASKCAL6                        PIC S9(5) COMP-5.                  
      *}                                                                        
       01  W-MASKED6                         PIC ZZZZZ9.                        
       01  W-MASKED6S                        PIC ------9.                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MASKCAL7                        PIC S9(6)  COMP.                   
      *--                                                                       
       01  W-MASKCAL7                        PIC S9(6) COMP-5.                  
      *}                                                                        
       01  W-MASKED7                         PIC ZZZZZZ9.                       
       01  W-MASKED7S                        PIC -------9.                      
      *                                                                         
      *                                                                         
       01  CTR-LIGNE                         PIC 99   VALUE 0.                  
       01  MAX-LIGNE                         PIC 99   VALUE 58.                 
           EJECT                                                                
      *-------------------------- BLOC ECRITURE DU CODIC2 -------------         
       01 I-BLOC                             PIC S9(4) COMP-3 VALUE 0.          
       01 MAX-BLOC                           PIC S9(5) COMP-3 VALUE 500.        
       01 NB-BLOC                            PIC S9(5) COMP-3 VALUE 0.          
       01 NB-BLOC2                           PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-BLOC                      PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-BLOC2                     PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-CODIC1                    PIC S9(5) COMP-3 VALUE 0.          
       01 NB-LIGNE-TOTAL                     PIC S9(5) COMP-3 VALUE 0.          
       01 W-BLOC.                                                               
          02 BLOC-LIGNE OCCURS 500.                                             
               10  LIGNE-CODIC               PIC X(132).                        
       01 W-BLOC2.                                                              
          02 BLOC2-LIGNE OCCURS 500.                                            
               10  LIGNE-TOTAL               PIC X(132).                        
      *                                                                         
       01  IGP810-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  IGP810-E00.                                                      
               10  IGP810-E00-CETAT               PIC X(06).                    
               10  FILLER                         PIC X(50) VALUE               
           '                     SUIVI DES PRODUITS A CALER   '.                
               10  FILLER                         PIC X(40) VALUE               
           '                               EDITE LE '.                          
               10  IGP810-E00-DATEDITE.                                         
                   15 JJ                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 MM                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 SS                          PIC XX.                       
                   15 AA                          PIC XX.                       
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  IGP810-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  IGP810-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(15) VALUE               
           '          PAGE '.                                                   
               10  IGP810-E00-NOPAGE              PIC ZZZ.                      
      *--------------------------- LIGNE E05  --------------------------        
           05  IGP810-E05.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'SOCIETE      : '.                                                   
               10  IGP810-E05-NSOCIETE            PIC X(03).                    
               10  FILLER                         PIC X(111)                    
                                                  VALUE ALL SPACES.             
      *--------------------------- LIGNE E10  --------------------------        
           05  IGP810-E10.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'DEPOT GEN    : '.                                                   
               10  IGP810-E10-NLIEU               PIC X(03).                    
               10  FILLER                         PIC X(111)                    
                                                  VALUE ALL SPACES.             
      *--------------------------- LIGNE E15  --------------------------        
           05  IGP810-E15.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'TYPE         : '.                                                   
               10  IGP810-E15-TLMELA              PIC X(03).                    
               10  FILLER                         PIC X(111)                    
                                                  VALUE ALL SPACES.             
      *--------------------------- LIGNE E20  --------------------------        
           05  IGP810-E20.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'CHEF PRODUIT : '.                                                   
               10  IGP810-E20-CHEFPROD            PIC X(05).                    
               10  FILLER                         PIC X(112)                    
                                                  VALUE ALL SPACES.             
E0576 *--------------------------- LIGNE E25  --------------------------        
           05  IGP810-E25.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'CODE MARQUE  : '.                                                   
               10  IGP810-E25-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(112)                    
E0576                                             VALUE ALL SPACES.             
      *--------------------------- LIGNE E30  --------------------------        
           05  IGP810-E30.                                                      
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '+---------------------------+-----------+--------+'.                
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '-----------+-------------+-------+-----+----------'.                
E0576 *        10  FILLER                         PIC X(32) VALUE               
E0576 *    '------------------------------+ '.                                  
E0576          10  FILLER                         PIC X(50) VALUE               
E0576      '+--------------------------+-----------+-------+--'.                
E0576          10  FILLER                         PIC X(50) VALUE               
E0576      '---------+------------------+-------+---+---------'.                
E0576          10  FILLER                         PIC X(32) VALUE               
E0576      '------------------------------+ '.                                  
      *--------------------------- LIGNE E35  --------------------------        
           05  IGP810-E35.                                                      
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '! CODIC   FAM   MARQ. STA O !   VENTES  !        !'.                
E0576 *        10  FILLER                         PIC X(51) VALUE               
E0576 *    ' NB   DIS  !  ENTREPOT   ! COUV. ! ENT ! STA   CDE '.               
E0576 *        10  FILLER                         PIC X(32) VALUE               
E0576 *    '   DATE    QTE   QTE    VOL  !  '.                                  
E0576          10  FILLER                         PIC X(48) VALUE               
E0576      '!CODIC   FAM   MARQ. STA  O!   VENTES  !       !'.                  
E0576          10  FILLER                         PIC X(54) VALUE               
E0576      ' NB   DIS  !  ENTREPOT        ! COUV. !ENT! STA   CDE '.            
E0576          10  FILLER                         PIC X(31) VALUE               
E0576      '   DATE    QTE   QTE    VOL !  '.                                   
      *--------------------------- LIGNE E40  --------------------------        
           05  IGP810-E40.                                                      
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '!                         A ! S-1   S-0 !  V4S   !'.                
E0576 *        10  FILLER                         PIC X(51) VALUE               
E0576 *    ' MAG  MAG  ! DISPO  RES  !  7J.  !     !           '.               
E0576 *        10  FILLER                         PIC X(32) VALUE               
E0576 *    '           CDE   BL          !  '.                                  
E0576          10  FILLER                         PIC X(48) VALUE               
E0576      '!     DEPOTEXP            A! S-1   S-0 !  V4S  !'.                  
E0576          10  FILLER                         PIC X(54) VALUE               
E0576      ' MAG  MAG  ! DISPO  RES RESCDE!  7J.  !   !           '.            
E0576          10  FILLER                         PIC X(31) VALUE               
E0576      '           CDE   BL         !  '.                                   
      *--------------------------- LIGNE E45  --------------------------        
           05  IGP810-E45.                                                      
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '+---------------------------+-----------+--------+'.                
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '-----------+-------------+-------+-----+----------'.                
E0576 *        10  FILLER                         PIC X(32) VALUE               
E0576 *    '------------------------------+ '.                                  
E0576          10  FILLER                         PIC X(50) VALUE               
E0576      '+--------------------------+-----------+-------+--'.                
E0576          10  FILLER                         PIC X(50) VALUE               
E0576      '---------+------------------+-------+---+---------'.                
E0576          10  FILLER                         PIC X(32) VALUE               
E0576      '------------------------------+ '.                                  
      *--------------------------- LIGNE D05  --------------------------        
           05  IGP810-D05.                                                      
E0576 *        10  FILLER                         PIC X(2)  VALUE               
E0576 *    '! '.                                                                
E0576          10  FILLER                         PIC X(1)  VALUE               
E0576      '!'.                                                                 
               10  IGP810-D05-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-CFAM                PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-LSTATCOMP           PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-WOA                 PIC X(01).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP810-D05-QS-1                PIC XXXX.                     
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-QS-0                PIC XXXX.                     
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP810-D05-Q4S                 PIC XXXXXX.                   
E0576 *        10  FILLER                         PIC X(03) VALUE               
E0576 *    ' ! '.                                                               
E0576          10  FILLER                         PIC X(02) VALUE               
E0576      '! '.                                                                
               10  IGP810-D05-NBMAG               PIC XXX.                      
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-QSTOCKMAG           PIC XXXXX.                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IGP810-D05-QSTOCKDIS           PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-QSTOCKRES           PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
E0576      ' '.                                                                 
E0576          10  IGP810-D05-TOTQCDERES          PIC XXXXX.                    
E0576 *        10  FILLER                         PIC X(03) VALUE               
E0576 *    ' ! '.                                                               
E0576          10  FILLER                         PIC X(02) VALUE               
E0576      '! '.                                                                
               10  IGP810-D05-COUV7J              PIC XXXXX.                    
E0576 *        10  FILLER                         PIC X(03) VALUE               
E0576 *    ' ! '.                                                               
E0576          10  FILLER                         PIC X(02) VALUE               
E0576      ' !'.                                                                
               10  IGP810-D05-NDEPOT              PIC X(03).                    
E0576 *        10  FILLER                         PIC X(03) VALUE               
E0576 *    ' ! '.                                                               
E0576          10  FILLER                         PIC X(02) VALUE               
E0576      '! '.                                                                
               10  IGP810-D05-CSTATUT             PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-NCDE                PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-DCDE4               PIC X(06).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-QCDE                PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05-QBL                 PIC XXXXX.                    
E0555          10  FILLER                         PIC X(01) VALUE               
E0555      ' '.                                                                 
E0555          10  IGP810-D05-NVOLUME             PIC XXXXXXX.                  
E0576 *        10  FILLER                         PIC X(03) VALUE               
E0576 *    ' ! '.                                                               
E0576          10  FILLER                         PIC X(02) VALUE               
E0576      '! '.                                                                
NEW   *--------------------------- LIGNE D05B --------------------------        
           05  IGP810-D05B.                                                     
               10  FILLER                         PIC X(2)  VALUE               
           '! '.                                                                
               10  IGP810-D05B-NCODIC             PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05B-CFAM               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05B-CMARQ              PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05B-LSTATCOMP          PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D05B-WOA                PIC X(01).                    
               10  FILLER                         PIC X(105) VALUE              
                                                  SPACES.                       
      *--------------------------- LIGNE D10  --------------------------        
           05  IGP810-D10.                                                      
               10  FILLER                         PIC X(07) VALUE               
           '!      '.                                                           
               10  IGP810-D10-LREFFOURN           PIC X(20).                    
E0576 *        10  FILLER                         PIC X(13) VALUE               
E0576 *    ' !           '.                                                     
E0576          10  FILLER                         PIC X(12) VALUE               
E0576      '!           '.                                                      
E0576 *        10  FILLER                         PIC X(51) VALUE               
E0576 *    '!        !           !             !       !     ! '.               
E0576          10  FILLER                         PIC X(53) VALUE               
E0576      '!       !           !                  !       !   ! '.             
               10  IGP810-D10-CSTATUT             PIC X(03).                    
               10  FILLER                         PIC X(1) VALUE                
           ' '.                                                                 
               10  IGP810-D10-NCDE                PIC X(07).                    
               10  FILLER                         PIC X(1) VALUE                
           ' '.                                                                 
               10  IGP810-D10-DCDE4               PIC X(06).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D10-QCDE                PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D10-QBL                 PIC XXXXX.                    
E0555          10  FILLER                         PIC X(01) VALUE               
E0555      ' '.                                                                 
E0555          10  IGP810-D10-NVOLUME             PIC XXXXXXX.                  
E0576 *        10  FILLER                         PIC X(03) VALUE               
E0576 *    ' ! '.                                                               
E0576          10  FILLER                         PIC X(02) VALUE               
E0576      '! '.                                                                
NEW   *--------------------------- LIGNE D10B --------------------------        
           05  IGP810-D10B.                                                     
               10  FILLER                         PIC X(07) VALUE               
           '!      '.                                                           
               10  IGP810-D10B-LREFFOURN          PIC X(20).                    
               10  FILLER                         PIC X(105) VALUE              
                                                   SPACES.                      
E0576 *--------------------------- LIGNE D15  --------------------------        
           05  IGP810-D15.                                                      
               10  FILLER                         PIC X(07) VALUE               
           '!      '.                                                           
               10  IGP810-D15-DEPOTEXP            PIC X(20).                    
               10  FILLER                         PIC X(12) VALUE               
           '!           '.                                                      
               10  FILLER                         PIC X(53) VALUE               
           '!       !           !                  !       !   ! '.             
               10  IGP810-D15-CSTATUT             PIC X(03).                    
               10  FILLER                         PIC X(1) VALUE                
           ' '.                                                                 
               10  IGP810-D15-NCDE                PIC X(07).                    
               10  FILLER                         PIC X(1) VALUE                
           ' '.                                                                 
               10  IGP810-D15-DCDE4               PIC X(06).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D15-QCDE                PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D15-QBL                 PIC XXXXX.                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGP810-D15-NVOLUME             PIC XXXXXXX.                  
               10  FILLER                         PIC X(02) VALUE               
E0576      '! '.                                                                
      *--------------------------- LIGNE T00  --------------------------        
           05  IGP810-T00.                                                      
               10  FILLER                         PIC X(55) VALUE               
           '!                           !               !         !'.           
               10  FILLER                         PIC X(55) VALUE               
           '           !                !       !    !             '.           
               10  FILLER                         PIC X(22) VALUE               
           '                    ! '.                                            
      *--------------------------- LIGNE TIRETS-------------------------        
           05  IGP810-T.                                                        
               10  FILLER                         PIC X(55) VALUE               
           '+------------------------------------------------------'.           
               10  FILLER                         PIC X(55) VALUE               
           '-------------------------------------------------------'.           
               10  FILLER                         PIC X(22) VALUE               
           '--------------------+ '.                                            
      *--------------------------- LIGNE T20  -------------------------A        
           05  IGP810-T20.                                                      
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '+---------------------------+-----------+--------+'.                
E0576 *        10  FILLER                         PIC X(50) VALUE               
E0576 *    '-----------+-------------+-------+-----+----------'.                
E0576 *        10  FILLER                         PIC X(32) VALUE               
E0576 *    '------------------------------+ '.                                  
E0576          10  FILLER                         PIC X(50) VALUE               
E0576      '+--------------------------+-----------+-------+--'.                
E0576          10  FILLER                         PIC X(50) VALUE               
E0576      '---------+------------------+-------+---+---------'.                
E0576          10  FILLER                         PIC X(32) VALUE               
E0576      '------------------------------+ '.                                  
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    T A B L E S       *         
      *---------------------------------------------------------------*         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BGP806 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BGP811 SECTION.                                                   
      *-----------------------                                                  
      *                                                                         
           PERFORM  DEBUT-BGP811.                                               
           PERFORM  TRAIT-BGP811.                                               
           PERFORM  FIN-BGP811.                                                 
      *                                                                         
       F-MODULE-BGP811. EXIT.                                                   
      *-----------------------                                                  
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    D E B U T        B G P 8 1 1               *         
      *---------------------------------------------------------------*         
      *                                                                         
       DEBUT-BGP811 SECTION.                                                    
      *-----------------------                                                  
      *                                                                         
           DISPLAY '*** DEBUT BGP811 ***'.                                      
           PERFORM OUVERTURE-FICHIERS.                                          
           PERFORM TRAIT-DATE.                                                  
      *                                                                         
       F-DEBUT-BGP811.                                                          
      *-----------------------                                                  
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    T R A I T        B G P 8 1 1               *         
      *---------------------------------------------------------------*         
      *                                                                         
       TRAIT-BGP811 SECTION.                                                    
      *-----------------------                                                  
      *                                                                         
           INITIALIZE CTR-PAGE.                                                 
           INITIALIZE W-BLOC NB-BLOC  NB-BLOC2                                  
E0576      INITIALIZE RECUP-DEPOT-EXP                                           
           PERFORM LECTURE-FGP810.                                              
EOF        PERFORM UNTIL EOF                                                    
            PERFORM RUPTURE-NSOCIETE                                            
NSOC        PERFORM UNTIL                                                       
               FGP810-NSOCIETE NOT = W-FGP810-NSOCIETE                          
            OR EOF                                                              
             PERFORM RUPTURE-DEPOTGEN                                           
DEPG         PERFORM UNTIL                                                      
                FGP810-NSOCIETE   NOT = W-FGP810-NSOCIETE                       
             OR FGP810-NSOCDEPGEN NOT = W-FGP810-NSOCDEPGEN                     
             OR FGP810-NDEPOTGEN  NOT = W-FGP810-NDEPOTGEN                      
             OR EOF                                                             
TLMELA         PERFORM RUPTURE-TLMELA                                           
               PERFORM UNTIL                                                    
                  FGP810-NSOCIETE   NOT = W-FGP810-NSOCIETE                     
               OR FGP810-NSOCDEPGEN NOT = W-FGP810-NSOCDEPGEN                   
               OR FGP810-NDEPOTGEN  NOT = W-FGP810-NDEPOTGEN                    
               OR FGP810-TRI1       NOT = W-FGP810-TRI1                         
               OR EOF                                                           
E0576 *          PERFORM ECRITURE-ENTETE                                        
                 PERFORM RUPTURE-CHEFPRODUIT                                    
CHEFPR           PERFORM UNTIL                                                  
                    FGP810-NSOCIETE   NOT = W-FGP810-NSOCIETE                   
                 OR FGP810-NSOCDEPGEN NOT = W-FGP810-NSOCDEPGEN                 
                 OR FGP810-NDEPOTGEN  NOT = W-FGP810-NDEPOTGEN                  
                 OR FGP810-TRI1       NOT = W-FGP810-TRI1                       
                 OR FGP810-CHEFPROD   NOT = W-FGP810-CHEFPROD                   
                 OR EOF                                                         
E0576              PERFORM ECRITURE-ENTETE                                      
E0576              PERFORM RUPTURE-MARQUE                                       
E0576            PERFORM UNTIL                                                  
E0576               FGP810-NSOCIETE   NOT = W-FGP810-NSOCIETE                   
E0576            OR FGP810-NSOCDEPGEN NOT = W-FGP810-NSOCDEPGEN                 
E0576            OR FGP810-NDEPOTGEN  NOT = W-FGP810-NDEPOTGEN                  
E0576            OR FGP810-TRI1       NOT = W-FGP810-TRI1                       
E0576            OR FGP810-CHEFPROD   NOT = W-FGP810-CHEFPROD                   
E0576            OR FGP810-CMARQ      NOT = W-FGP810-CMARQ                      
E0576            OR EOF                                                         
CODIC              PERFORM RUPTURE-NCODIC                                       
                   PERFORM UNTIL                                                
                      FGP810-NSOCIETE   NOT = W-FGP810-NSOCIETE                 
                   OR FGP810-NSOCDEPGEN NOT = W-FGP810-NSOCDEPGEN               
                   OR FGP810-NDEPOTGEN  NOT = W-FGP810-NDEPOTGEN                
                   OR FGP810-TRI1       NOT = W-FGP810-TRI1                     
                   OR FGP810-CHEFPROD   NOT = W-FGP810-CHEFPROD                 
E0576              OR FGP810-CMARQ      NOT = W-FGP810-CMARQ                    
                   OR FGP810-NCODIC     NOT = W-FGP810-NCODIC                   
                   OR EOF                                                       
                     PERFORM RUPTURE-NDEPOT                                     
DEPO                 PERFORM UNTIL                                              
                        FGP810-NSOCIETE   NOT = W-FGP810-NSOCIETE               
                     OR FGP810-NSOCDEPGEN NOT = W-FGP810-NSOCDEPGEN             
                     OR FGP810-NDEPOTGEN  NOT = W-FGP810-NDEPOTGEN              
                     OR FGP810-TRI1       NOT = W-FGP810-TRI1                   
                     OR FGP810-CHEFPROD   NOT = W-FGP810-CHEFPROD               
E0576                OR FGP810-CMARQ      NOT = W-FGP810-CMARQ                  
                     OR FGP810-NCODIC     NOT = W-FGP810-NCODIC                 
                     OR FGP810-NSOCDEP    NOT = W-FGP810-NSOCDEP                
                     OR FGP810-NDEPOT     NOT = W-FGP810-NDEPOT                 
                     OR EOF                                                     
                            PERFORM ECRITURE-DETAIL                             
                            PERFORM LECTURE-FGP810                              
DEPOT                END-PERFORM                                                
CODIC              END-PERFORM                                                  
      * APR�S CHAQUE CODIC TRAIT� ON �CRIT LE BLOC DE LIGNE DU CODIC            
                   PERFORM BLOC-CODIC                                           
CHEFPR         END-PERFORM                                                      
TLMELA       END-PERFORM                                                        
DEPG        END-PERFORM                                                         
E0576       END-PERFORM                                                         
NSOC        END-PERFORM                                                         
EOF        END-PERFORM.                                                         
       FIN-TRAIT-BGP811. EXIT.                                                  
      *-----------------------                                                  
      *                                                                         
       FIN-BGP811 SECTION.                                                      
      *-------------------                                                      
      *                                                                         
           CLOSE FDATE IGP810 FGP810.                                           
           DISPLAY '**********************************************'.            
           DISPLAY '*    PROGRAMME BGP811  TERMINE NORMALEMENT   *'.            
           DISPLAY '*                                            *'.            
           DISPLAY '*  NOMBRE D''ENREG LUS SUR FGP810    = ' CTR-FGP810         
                   '  *'.                                                       
           DISPLAY '*                                            *'.            
           DISPLAY '**********************************************'.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-BGP811. EXIT.                                                    
      *-------------------                                                      
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    D E B U T  B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       OUVERTURE-FICHIERS SECTION.                                              
      *-------------------                                                      
           OPEN INPUT FDATE FGP810.                                             
           OPEN OUTPUT IGP810.                                                  
       FIN-OUVERTURE-FICHIERS. EXIT.                                            
      *------------------------------                                           
      *                                                                         
       TRAIT-DATE SECTION.                                                      
      *-------------------                                                      
           READ FDATE                                                           
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                      
                  PERFORM FIN-ANORMALE.                                         
           MOVE '1'                 TO GFDATA.                                  
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                              
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                            
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS            
              PERFORM FIN-ANORMALE                                              
           ELSE                                                                 
              DISPLAY '** FDATE  ** ' GFJMSA-5                                  
           END-IF.                                                              
           COMPUTE GFQNT0 = GFQNT0 + 1 .                                        
           MOVE '3' TO GFDATA.                                                  
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS                
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFJJMMSSAA       TO DATJMSA.                                    
           MOVE GFSMN            TO JOUR-SEM.                                   
           MOVE GFSAMJ-0         TO DATSAMJ.                                    
           MOVE DATSAMJ (1:6)    TO DATSM-9.                                    
           DISPLAY 'DATE RECEPTION ETAT DANS BOITE EOS : ' DATJMSA.             
           ACCEPT W-TIME FROM TIME.                                             
       FIN-TRAIT-DATE. EXIT.                                                    
      *-----------------------                                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    T R A I T  B G P 8 1 1               *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FGP810 SECTION.                                                  
      *-----------------------                                                  
           MOVE FGP810-ENREG TO W-FGP810-ENREG.                                 
           INITIALIZE FGP810-ENREG.                                             
             READ FGP810                                                        
                  INTO FGP810-ENREG                                             
                  AT END SET EOF TO TRUE                                        
             END-READ                                                           
              IF CTR-FGP810 = 0                                                 
                 MOVE FGP810-ENREG TO W-FGP810-ENREG                            
              END-IF                                                            
              ADD 1 TO CTR-FGP810.                                              
              PERFORM NUMERO-RUPTURE.                                           
       FIN-LECTURE-FGP810. EXIT.                                                
      *-----------------------------                                            
      *---------------- RUPTURE DE L ETAT ----------------------------          
       NUMERO-RUPTURE SECTION.                                                  
      *-----------------------------                                            
           IF FGP810-NSOCIETE = W-FGP810-NSOCIETE                               
E0576 *     MOVE 10 TO RUPTURE                                                  
E0576       MOVE 11 TO RUPTURE                                                  
              IF FGP810-NSOCDEPGEN = W-FGP810-NSOCDEPGEN                        
E0576 *         MOVE 9  TO RUPTURE                                              
E0576           MOVE 10 TO RUPTURE                                              
                IF FGP810-NDEPOTGEN = W-FGP810-NDEPOTGEN                        
E0576 *          MOVE 8  TO RUPTURE                                             
E0576            MOVE 9  TO RUPTURE                                             
                 IF FGP810-TRI1  = W-FGP810-TRI1                                
E0576 *           MOVE 7  TO RUPTURE                                            
E0576             MOVE 8  TO RUPTURE                                            
                  IF FGP810-CHEFPROD = W-FGP810-CHEFPROD                        
E0576 *            MOVE 6  TO RUPTURE                                           
E0576              MOVE 7  TO RUPTURE                                           
E0576              IF FGP810-CMARQ  = W-FGP810-CMARQ                            
E0576                MOVE 6  TO RUPTURE                                         
                     IF FGP810-NCODIC = W-FGP810-NCODIC                         
                       MOVE 5  TO RUPTURE                                       
                      IF FGP810-NSOCDEP = W-FGP810-NSOCDEP                      
                       MOVE 4  TO RUPTURE                                       
                       IF FGP810-NDEPOT = W-FGP810-NDEPOT                       
                        MOVE 3  TO RUPTURE                                      
                        IF FGP810-NCDE = W-FGP810-NCDE                          
                         MOVE 2  TO RUPTURE                                     
                         IF FGP810-DCDE = W-FGP810-DCDE                         
                          MOVE 1 TO RUPTURE                                     
                         END-IF                                                 
                       END-IF                                                   
                      END-IF                                                    
                     END-IF                                                     
                    END-IF                                                      
                   END-IF                                                       
                  END-IF                                                        
                 END-IF                                                         
                END-IF                                                          
               END-IF                                                           
           ELSE                                                                 
E0576 *     MOVE 11 TO RUPTURE                                                  
E0576       MOVE 12 TO RUPTURE                                                  
           END-IF.                                                              
E0576 *    IF CTR-FGP810 = 1 MOVE 11 TO RUPTURE.                                
E0576      IF CTR-FGP810 = 1 MOVE 12 TO RUPTURE.                                
       FIN-NUMERO-RUPTURE. EXIT.                                                
      *-----------------------------                                            
      *                                                                         
       RUPTURE-NSOCIETE SECTION.                                                
      *-----------------------------                                            
           MOVE FGP810-NSOCIETE TO W-FGP810-NSOCIETE.                           
       FIN-RUPTURE-NSOCIETE. EXIT.                                              
      *-----------------------------                                            
      *                                                                         
       RUPTURE-DEPOTGEN SECTION.                                                
      *-----------------------------                                            
           MOVE FGP810-NSOCIETE   TO W-FGP810-NSOCIETE.                         
           MOVE FGP810-NSOCDEPGEN TO W-FGP810-NSOCDEPGEN.                       
           MOVE FGP810-NDEPOTGEN  TO W-FGP810-NDEPOTGEN.                        
       FIN-RUPTURE-DEPOTGEN. EXIT.                                              
      *-----------------------------                                            
      *                                                                         
       RUPTURE-TLMELA   SECTION.                                                
      *-----------------------------                                            
           MOVE FGP810-NSOCIETE   TO W-FGP810-NSOCIETE                          
           MOVE FGP810-NSOCDEPGEN TO W-FGP810-NSOCDEPGEN                        
           MOVE FGP810-NDEPOTGEN  TO W-FGP810-NDEPOTGEN                         
           MOVE FGP810-TRI1       TO W-FGP810-TRI1 .                            
       FIN-RUPTURE-TLMELA. EXIT.                                                
      *-----------------------------                                            
      *                                                                         
       RUPTURE-CHEFPRODUIT SECTION.                                             
      *-----------------------------                                            
           MOVE FGP810-NSOCIETE   TO W-FGP810-NSOCIETE                          
           MOVE FGP810-NSOCDEPGEN TO W-FGP810-NSOCDEPGEN                        
           MOVE FGP810-NDEPOTGEN  TO W-FGP810-NDEPOTGEN                         
           MOVE FGP810-TRI1       TO W-FGP810-TRI1                              
           MOVE FGP810-CHEFPROD   TO W-FGP810-CHEFPROD.                         
       FIN-RUPTURE-CHEFPRODUIT. EXIT.                                           
      *-----------------------------                                            
      *                                                                         
E0576  RUPTURE-MARQUE     SECTION.                                              
      *-----------------------------                                            
           MOVE FGP810-NSOCIETE   TO W-FGP810-NSOCIETE                          
           MOVE FGP810-NSOCDEPGEN TO W-FGP810-NSOCDEPGEN                        
           MOVE FGP810-NDEPOTGEN  TO W-FGP810-NDEPOTGEN                         
           MOVE FGP810-TRI1       TO W-FGP810-TRI1                              
           MOVE FGP810-CHEFPROD   TO W-FGP810-CHEFPROD                          
           MOVE FGP810-CMARQ      TO W-FGP810-CMARQ.                            
E0576  FIN-RUPTURE-MARQUE.     EXIT.                                            
      *-----------------------------                                            
      *                                                                         
       RUPTURE-NCODIC     SECTION.                                              
      *-----------------------------                                            
           MOVE FGP810-NSOCIETE   TO W-FGP810-NSOCIETE                          
           MOVE FGP810-NSOCDEPGEN TO W-FGP810-NSOCDEPGEN                        
           MOVE FGP810-NDEPOTGEN  TO W-FGP810-NDEPOTGEN                         
           MOVE FGP810-TRI1       TO W-FGP810-TRI1                              
           MOVE FGP810-CHEFPROD   TO W-FGP810-CHEFPROD                          
E0576      MOVE FGP810-CMARQ      TO W-FGP810-CMARQ.                            
           MOVE FGP810-NCODIC     TO W-FGP810-NCODIC.                           
       FIN-RUPTURE-NCODIC.     EXIT.                                            
      *-----------------------------                                            
      *                                                                         
       RUPTURE-NDEPOT  SECTION.                                                 
      *------------------------                                                 
           MOVE FGP810-NSOCIETE   TO W-FGP810-NSOCIETE                          
           MOVE FGP810-NSOCDEPGEN TO W-FGP810-NSOCDEPGEN                        
           MOVE FGP810-NDEPOTGEN  TO W-FGP810-NDEPOTGEN                         
           MOVE FGP810-TRI1       TO W-FGP810-TRI1                              
           MOVE FGP810-CHEFPROD   TO W-FGP810-CHEFPROD                          
E0576      MOVE FGP810-CMARQ      TO W-FGP810-CMARQ.                            
           MOVE FGP810-NCODIC     TO W-FGP810-NCODIC                            
           MOVE FGP810-NSOCDEP    TO W-FGP810-NSOCDEP                           
           MOVE FGP810-NDEPOT     TO W-FGP810-NDEPOT.                           
       FIN-RUPTURE-NDEPOT.  EXIT.                                               
      *--------------------------                                               
      *                                                                         
      *-------------------- ECRITURE DES LIGNES ETAT -----------------          
      *                                                                         
       ECRITURE-BLOC SECTION.                                                   
      *----------------------                                                   
           ADD 1                        TO NB-BLOC                              
           EVALUATE SAUT                                                        
             WHEN ' '  ADD 1            TO NB-LIGNE-BLOC NB-LIGNE-CODIC1        
             WHEN '0'  ADD 2            TO NB-LIGNE-BLOC NB-LIGNE-CODIC1        
             WHEN OTHER CONTINUE                                                
           END-EVALUATE                                                         
           IF NB-BLOC > MAX-BLOC                                                
              MOVE 'TAB BLOC TROP PETIT' TO ABEND-MESS                          
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           MOVE LIGNE-IGP810             TO LIGNE-CODIC(NB-BLOC)                
           IF RUPTURE-ENTETE                                                    
              PERFORM ECRITURE-ENTETE                                           
           END-IF.                                                              
       FIN-ECRITURE-BLOC. EXIT.                                                 
      *------------------------                                                 
      *                                                                         
      *                                                                         
       ECRITURE-ENTETE SECTION.                                                 
      *------------------------                                                 
           IF RUPTURE-ENTETE                                                    
             ADD 1                      TO CTR-PAGE                             
             MOVE 0                     TO CTR-LIGNE                            
             MOVE 'IGP810'              TO IGP810-E00-CETAT                     
             MOVE CORRESPONDING DATJMSA TO IGP810-E00-DATEDITE                  
             MOVE HH OF W-TIME          TO IGP810-E00-HEUREDITE                 
             MOVE MN OF W-TIME          TO IGP810-E00-MINUEDITE                 
             MOVE CTR-PAGE              TO IGP810-E00-NOPAGE                    
             MOVE '1'                   TO SAUT                                 
             MOVE IGP810-E00            TO LIGNE                                
             WRITE ENREG-IGP810 FROM LIGNE-IGP810                               
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP810-NSOCIETE       TO IGP810-E05-NSOCIETE                  
             MOVE ' '                   TO SAUT                                 
             MOVE IGP810-E05            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP810-NDEPOTGEN      TO IGP810-E10-NLIEU                     
             MOVE ' '                   TO SAUT                                 
             MOVE IGP810-E10            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
      *                                                                         
             MOVE FGP810-TLMELA         TO IGP810-E15-TLMELA                    
             MOVE ' '                   TO SAUT                                 
             MOVE IGP810-E15            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
             MOVE FGP810-CHEFPROD       TO IGP810-E20-CHEFPROD                  
E0576 *      MOVE '0'                   TO SAUT                                 
E0576        MOVE ' '                   TO SAUT                                 
             MOVE IGP810-E20            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
E0576 *      ADD 2 TO CTR-LIGNE                                                 
E0576        ADD 1 TO CTR-LIGNE                                                 
      *                                                                         
E0576        MOVE FGP810-CMARQ          TO IGP810-E25-CMARQ                     
E0576        MOVE '0'                   TO SAUT                                 
E0576        MOVE IGP810-E25            TO LIGNE                                
E0576        WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
E0576        ADD 2 TO CTR-LIGNE                                                 
      *                                                                         
      *                                                                         
             MOVE ' '                   TO SAUT                                 
             MOVE IGP810-E30            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP810-E35            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP810-E40            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
             MOVE IGP810-E45            TO LIGNE                                
             WRITE ENREG-IGP810         FROM LIGNE-IGP810                       
             ADD 1 TO CTR-LIGNE                                                 
             SET ENTETE-ECRITE          TO TRUE                                 
           ELSE                                                                 
             SET RUPTURE-ENTETE         TO TRUE                                 
           END-IF.                                                              
       FIN-ECRITURE-ENTETE. EXIT.                                               
      *--------------------------                                               
      *                                                                         
       ECRITURE-ENTETE-PRE SECTION.                                             
      *----------------------------                                             
           ADD 1                        TO CTR-PAGE.                            
           MOVE 0                       TO CTR-LIGNE.                           
           MOVE 'IGP810'                TO IGP810-E00-CETAT.                    
           MOVE CORRESPONDING DATJMSA   TO IGP810-E00-DATEDITE.                 
           MOVE HH OF W-TIME            TO IGP810-E00-HEUREDITE.                
           MOVE MN OF W-TIME            TO IGP810-E00-MINUEDITE.                
           MOVE CTR-PAGE                TO IGP810-E00-NOPAGE.                   
           MOVE '1'                     TO SAUT.                                
           MOVE IGP810-E00              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP810-NSOCIETE       TO IGP810-E05-NSOCIETE.                 
           MOVE ' '                     TO SAUT.                                
           MOVE IGP810-E05              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP810-NDEPOTGEN      TO IGP810-E10-NLIEU.                    
           MOVE ' '                     TO SAUT.                                
           MOVE IGP810-E10              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGP810-TLMELA         TO IGP810-E15-TLMELA.                   
           MOVE ' '                     TO SAUT.                                
           MOVE IGP810-E15              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
GD1204     ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
GD1204     MOVE W-FGP810-CHEFPROD       TO IGP810-E20-CHEFPROD.                 
GD1204*    MOVE '0'                     TO SAUT.                                
E0576      MOVE ' '                     TO SAUT.                                
GD1204     MOVE  IGP810-E20             TO LIGNE.                               
GD1204     WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
E0576 *    ADD 2 TO CTR-LIGNE.                                                  
E0576      ADD 1 TO CTR-LIGNE.                                                  
E0576 *    MOVE ' '                     TO SAUT.                                
      *                                                                         
E0576      MOVE W-FGP810-CMARQ          TO IGP810-E25-CMARQ.                    
E0576      MOVE '0'                     TO SAUT.                                
E0576      MOVE  IGP810-E25             TO LIGNE.                               
E0576      WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
E0576      ADD 2 TO CTR-LIGNE.                                                  
           MOVE ' '                     TO SAUT.                                
      *                                                                         
           MOVE IGP810-E30              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP810-E35              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP810-E40              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGP810-E45              TO LIGNE.                               
           WRITE ENREG-IGP810           FROM LIGNE-IGP810.                      
           ADD 1 TO CTR-LIGNE.                                                  
       FIN-ECRITURE-ENTETE-PRE. EXIT.                                           
      *------------------------------                                           
      *                                                                         
       ECRITURE-DETAIL SECTION.                                                 
      *------------------------                                                 
E0576      MOVE FGP810-DEPOTEXP        TO IGP810-D15-DEPOTEXP                   
E0576      IF FGP810-QBL > 0                                                    
E0576         SET LB-DEPOTEXP          TO TRUE                                  
E0576      END-IF                                                               
           IF RUPTURE > 5                                                       
              MOVE FGP810-NCODIC        TO IGP810-D05-NCODIC                    
              MOVE FGP810-LREFFOURN     TO IGP810-D10-LREFFOURN                 
              MOVE FGP810-CFAM          TO IGP810-D05-CFAM                      
              MOVE FGP810-CMARQ         TO IGP810-D05-CMARQ                     
              MOVE FGP810-LSTATCOMP     TO IGP810-D05-LSTATCOMP                 
              MOVE FGP810-WOA           TO IGP810-D05-WOA                       
              MOVE FGP810-V4S           TO W-MASKED5                            
              MOVE W-MASKED5            TO IGP810-D05-Q4S                       
              MOVE FGP810-QS-0          TO W-MASKED4                            
              MOVE W-MASKED4            TO IGP810-D05-QS-0                      
              MOVE FGP810-QS-1          TO W-MASKED4                            
              MOVE W-MASKED4            TO IGP810-D05-QS-1                      
              MOVE FGP810-NBMAG         TO W-MASKED3                            
              MOVE W-MASKED3            TO IGP810-D05-NBMAG                     
              MOVE FGP810-QSTOCKMAG     TO W-MASKED5                            
              MOVE W-MASKED5            TO IGP810-D05-QSTOCKMAG                 
              MOVE FGP810-COUV7J        TO W-MASKED5V                           
              MOVE W-MASKED5V           TO IGP810-D05-COUV7J                    
           ELSE                                                                 
              MOVE SPACES               TO IGP810-D05-NCODIC                    
                                           IGP810-D05-CFAM                      
                                           IGP810-D05-CMARQ                     
                                           IGP810-D05-LSTATCOMP                 
                                           IGP810-D05-WOA                       
                                           IGP810-D05-Q4S                       
                                           IGP810-D05-QS-0                      
                                           IGP810-D05-QS-1                      
                                           IGP810-D05-NBMAG                     
                                           IGP810-D05-QSTOCKMAG                 
                                           IGP810-D05-COUV7J                    
           END-IF                                                               
           IF RUPTURE > 3                                                       
              MOVE FGP810-QSTOCKDIS       TO W-MASKED5                          
              MOVE W-MASKED5              TO IGP810-D05-QSTOCKDIS               
              MOVE FGP810-QSTOCKRES       TO W-MASKED5                          
              MOVE W-MASKED5              TO IGP810-D05-QSTOCKRES               
E0576         MOVE FGP810-TOTQCDERES      TO W-MASKED5                          
E0576         MOVE W-MASKED5              TO IGP810-D05-TOTQCDERES              
              MOVE FGP810-NDEPOT          TO IGP810-D05-NDEPOT                  
           ELSE                                                                 
              MOVE SPACES                 TO IGP810-D05-QSTOCKDIS               
                                             IGP810-D05-QSTOCKRES               
E0576                                        IGP810-D05-TOTQCDERES              
                                             IGP810-D05-NDEPOT                  
           END-IF                                                               
           IF RUPTURE > 2                                                       
              IF FGP810-STATUT = SPACES                                         
                 MOVE SPACES             TO IGP810-D05-QBL                      
E0555            MOVE SPACES             TO IGP810-D05-NVOLUME                  
              ELSE                                                              
                 MOVE FGP810-QBL         TO W-MASKED5                           
                 MOVE W-MASKED5          TO IGP810-D05-QBL                      
E0555            MOVE FGP810-NVOLUME     TO W-MASKED7V                          
E0555            MOVE W-MASKED7V         TO IGP810-D05-NVOLUME                  
              END-IF                                                            
              MOVE FGP810-NCDE            TO IGP810-D05-NCDE                    
           ELSE                                                                 
              MOVE SPACES                 TO IGP810-D05-QBL                     
E0555                                        IGP810-D05-NVOLUME                 
                                             IGP810-D05-NCDE                    
           END-IF                                                               
           IF RUPTURE > 1                                                       
              MOVE FGP810-STATUT          TO IGP810-D05-CSTATUT                 
              MOVE FGP810-DCDE(3:2)       TO IGP810-D05-DCDE4(5:2)              
              MOVE FGP810-DCDE(5:2)       TO IGP810-D05-DCDE4(3:2)              
              MOVE FGP810-DCDE(7:2)       TO IGP810-D05-DCDE4(1:2)              
              IF FGP810-STATUT = SPACES                                         
                 MOVE SPACES              TO IGP810-D05-QCDE                    
              ELSE                                                              
                 MOVE FGP810-QCDE            TO W-MASKED5                       
                 MOVE  W-MASKED5             TO IGP810-D05-QCDE                 
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES                 TO IGP810-D05-CSTATUT                 
              MOVE SPACES                 TO IGP810-D05-DCDE4                   
           END-IF.                                                              
E0576 *    IF NB-BLOC = 1                                                       
E0576      IF NB-BLOC = 1 OR 2                                                  
              IF RUPTURE < 4                                                    
                 IF NB-BLOC = 1                                                 
                   MOVE IGP810-D05-CSTATUT  TO IGP810-D10-CSTATUT               
                   MOVE IGP810-D05-NCDE     TO IGP810-D10-NCDE                  
                   MOVE IGP810-D05-DCDE4    TO IGP810-D10-DCDE4                 
                   IF IGP810-D05-CSTATUT = SPACES                               
                      MOVE SPACES           TO IGP810-D10-QCDE                  
                                               IGP810-D10-QBL                   
E0555                                          IGP810-D10-NVOLUME               
                   ELSE                                                         
                      MOVE IGP810-D05-QCDE  TO IGP810-D10-QCDE                  
                      MOVE IGP810-D05-QBL   TO IGP810-D10-QBL                   
E0555                 MOVE IGP810-D05-NVOLUME TO IGP810-D10-NVOLUME             
                   END-IF                                                       
                   MOVE ' '                 TO SAUT                             
                   MOVE IGP810-D10          TO LIGNE                            
                   INITIALIZE  IGP810-D10-CSTATUT                               
                               IGP810-D10-NCDE                                  
                               IGP810-D10-DCDE4                                 
                               IGP810-D10-QCDE                                  
                               IGP810-D10-QBL                                   
E0555                          IGP810-D10-NVOLUME                               
E0576            ELSE                                                           
E0576              MOVE IGP810-D05-CSTATUT  TO IGP810-D15-CSTATUT               
E0576              MOVE IGP810-D05-NCDE     TO IGP810-D15-NCDE                  
E0576              MOVE IGP810-D05-DCDE4    TO IGP810-D15-DCDE4                 
E0576              IF IGP810-D05-CSTATUT = SPACES                               
E0576                 MOVE SPACES           TO IGP810-D15-QCDE                  
E0576                                          IGP810-D15-QBL                   
E0576                                          IGP810-D15-NVOLUME               
E0576              ELSE                                                         
E0576                 MOVE IGP810-D05-QCDE  TO IGP810-D15-QCDE                  
E0576                 MOVE IGP810-D05-QBL   TO IGP810-D15-QBL                   
E0576                 MOVE IGP810-D05-NVOLUME TO IGP810-D15-NVOLUME             
E0576              END-IF                                                       
E0576              MOVE ' '                 TO SAUT                             
E0576              MOVE IGP810-D15          TO LIGNE                            
E0576              INITIALIZE  IGP810-D15-CSTATUT                               
E0576                          IGP810-D15-NCDE                                  
E0576                          IGP810-D15-DCDE4                                 
E0576                          IGP810-D15-QCDE                                  
E0576                          IGP810-D15-QBL                                   
E0576                          IGP810-D15-NVOLUME                               
E0576            END-IF                                                         
              ELSE                                                              
E0576           IF NB-BLOC = 1                                                  
                   MOVE SPACES              TO IGP810-D10-CSTATUT               
                   MOVE SPACES              TO IGP810-D10-NCDE                  
                   MOVE SPACES              TO IGP810-D10-DCDE4                 
                   MOVE SPACES              TO IGP810-D10-QCDE                  
                   MOVE SPACES              TO IGP810-D10-QBL                   
E0555              MOVE SPACES              TO IGP810-D10-NVOLUME               
                   MOVE ' '                 TO SAUT                             
                   MOVE IGP810-D10          TO LIGNE                            
                   PERFORM ECRITURE-BLOC                                        
E0576           END-IF                                                          
E0576              MOVE SPACES              TO IGP810-D15-CSTATUT               
E0576              MOVE SPACES              TO IGP810-D15-NCDE                  
E0576              MOVE SPACES              TO IGP810-D15-DCDE4                 
E0576              MOVE SPACES              TO IGP810-D15-QCDE                  
E0576              MOVE SPACES              TO IGP810-D15-QBL                   
E0576              MOVE SPACES              TO IGP810-D15-NVOLUME               
E0576              IF IGP810-D15-DEPOTEXP NOT = SPACES AND LOW-VALUES           
E0576                 MOVE ' '                 TO SAUT                          
E0576                 MOVE IGP810-D15          TO LIGNE                         
E0576                 PERFORM ECRITURE-BLOC                                     
E0576              END-IF                                                       
                   MOVE ' '                 TO SAUT                             
                   MOVE IGP810-D05          TO LIGNE                            
              END-IF                                                            
           ELSE                                                                 
              MOVE ' '                    TO SAUT                               
              MOVE IGP810-D05             TO LIGNE                              
           END-IF                                                               
           PERFORM ECRITURE-BLOC.                                               
       FIN-ECRITURE-DETAIL. EXIT.                                               
      *--------------------------                                               
      *                                                                         
       BLOC-CODIC  SECTION.                                                     
      *--------------------                                                     
           IF NB-LIGNE-BLOC > 0                                                 
      * LORSQU'IL N'Y A QU'UNE LIGNE DANS LE BLOC-CODIC IL FAUT RAJOUTER        
      * UNE 2EME LIGNE POUR LA R�F�RENCE PRODUIT                                
              IF NB-BLOC = 1                                                    
E0576            INITIALIZE  IGP810-D10-CSTATUT                                 
E0576                        IGP810-D10-NCDE                                    
E0576                        IGP810-D10-DCDE4                                   
E0576                        IGP810-D10-QCDE                                    
E0576                        IGP810-D10-QBL                                     
E0576                        IGP810-D10-NVOLUME                                 
                 MOVE IGP810-D10                   TO LIGNE                     
                 MOVE ' '                          TO SAUT                      
                 PERFORM ECRITURE-BLOC                                          
E0576            IF NOT LB-DEPOTEXP                                             
E0576               MOVE SPACES             TO IGP810-D15-DEPOTEXP              
E0576            END-IF                                                         
E0576            IF IGP810-D15-DEPOTEXP NOT = SPACES AND LOW-VALUES             
E0576              INITIALIZE  IGP810-D15-CSTATUT                               
E0576                          IGP810-D15-NCDE                                  
E0576                          IGP810-D15-DCDE4                                 
E0576                          IGP810-D15-QCDE                                  
E0576                          IGP810-D15-QBL                                   
E0576                          IGP810-D15-NVOLUME                               
E0576               MOVE IGP810-D15                   TO LIGNE                  
E0576               MOVE ' '                          TO SAUT                   
E0576               PERFORM ECRITURE-BLOC                                       
E0576            END-IF                                                         
E0576         ELSE                                                              
E0576            IF NB-BLOC = 2 AND LB-DEPOTEXP                                 
E0576            AND IGP810-D15-DEPOTEXP NOT = SPACES AND LOW-VALUES            
E0576              INITIALIZE  IGP810-D15-CSTATUT                               
E0576                          IGP810-D15-NCDE                                  
E0576                          IGP810-D15-DCDE4                                 
E0576                          IGP810-D15-QCDE                                  
E0576                          IGP810-D15-QBL                                   
E0576                          IGP810-D15-NVOLUME                               
E0576               MOVE IGP810-D15                   TO LIGNE                  
E0576               MOVE ' '                          TO SAUT                   
E0576               PERFORM ECRITURE-BLOC                                       
E0576            END-IF                                                         
              END-IF                                                            
              IF ((NB-LIGNE-BLOC + CTR-LIGNE) > (MAX-LIGNE))                    
                  MOVE ' ' TO SAUT                                              
                  INITIALIZE LIGNE                                              
                  MOVE '** SUITE **'               TO LIGNE                     
                  MOVE MAX-LIGNE                   TO CTR-LIGNE                 
                  PERFORM ECRITURE-LIGNE-SUI                                    
              END-IF                                                            
              PERFORM VARYING I-BLOC FROM 1 BY 1                                
                      UNTIL I-BLOC > NB-BLOC                                    
                      MOVE LIGNE-CODIC(I-BLOC)     TO LIGNE-IGP810              
                      IF SAUT = '+'                                             
                         SUBTRACT 1 FROM CTR-LIGNE                              
                      END-IF                                                    
                      PERFORM ECRITURE-LIGNE-SUI                                
              END-PERFORM                                                       
              INITIALIZE W-BLOC NB-BLOC NB-LIGNE-BLOC                           
E0576                    IGP810-D10-LREFFOURN                                   
              MOVE ' '                             TO SAUT                      
              MOVE IGP810-T20                      TO LIGNE                     
              PERFORM ECRITURE-LIGNE-SUI                                        
E0576         INITIALIZE RECUP-DEPOT-EXP                                        
           END-IF.                                                              
       FIN-BLOC-CODIC. EXIT.                                                    
      *-----------------------                                                  
      * ECRITURE UNE LIGNE                                                      
      *                                                                         
       ECRITURE-LIGNE-SUI SECTION.                                              
      *---------------------------                                              
           WRITE ENREG-IGP810 FROM LIGNE-IGP810.                                
           ADD 1 TO CTR-LIGNE.                                                  
           IF CTR-LIGNE > MAX-LIGNE                                             
              PERFORM ECRITURE-ENTETE-PRE                                       
           END-IF.                                                              
       FIN-ECRITURE-LIGNE-SUI. EXIT.                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *         P A R A G R A P H E   F I N  A N O R M A L E          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FIN-ANORMALE SECTION.                                                    
      *-------------                                                            
           MOVE 'BGP811' TO ABEND-PROG.                                         
           CALL 'ABEND' USING                                                   
                        ABEND-PROG                                              
                        ABEND-MESS.                                             
      *                                                                         
       F-FIN-ANORMALE.                                                          
      *-------------                                                            
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
