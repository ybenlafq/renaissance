      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.            BIV105.                                   00020001
       AUTHOR.      ANNICK.                                             00030000
      *                                                                 00040000
      * ============================================================= * 00050000
      *                                                               * 00060000
      *       PROJET        :  EDITION DES ETIQUETTES                 * 00070001
      *       PROGRAMME     :  BIV105.                                * 00080001
      *       PERIODICITE   :  EDITION A LA DEMANDE                   * 00090001
      *       FONCTION      :  CREATION DES ETIQUETTES                * 00100001
      *                        A PARTIR DES EDITIONS PRE-INVENTAIRES  * 00110001
      *       DATE CREATION :  12/06/1990.                            * 00120001
      *                                                               * 00130000
      * ============================================================= * 00140000
      *                                                                 00150000
       ENVIRONMENT DIVISION.                                            00160000
       CONFIGURATION SECTION.                                           00170000
       SPECIAL-NAMES.                                                   00180000
           DECIMAL-POINT IS COMMA.                                      00190000
       INPUT-OUTPUT SECTION.                                            00200000
      *                                                                 00210000
       FILE-CONTROL.                                                    00220000
      *                                                                 00230000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FIN105        ASSIGN  TO  FIN105.                   00240001
      *--                                                                       
            SELECT  FIN105        ASSIGN  TO  FIN105                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  IIV105        ASSIGN  TO  IIV105.                   00250001
      *--                                                                       
            SELECT  IIV105        ASSIGN  TO  IIV105                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE         ASSIGN  TO  FDATE.                    00260001
      *--                                                                       
            SELECT  FDATE         ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00270000
                                                                        00280000
      *                                                                 00290000
       DATA DIVISION.                                                   00300000
       FILE SECTION.                                                    00310000
      *                                                                 00320000
      * ============================================================= * 00330000
      *  D E F I N I T I O N    F I C H I E R    E X T R A C T I O N  * 00340000
      * ============================================================= * 00350000
      *                                                                 00360000
       FD   FIN105                                                      00370001
            RECORDING F                                                 00380000
            BLOCK 0 RECORDS                                             00390000
            LABEL RECORD STANDARD.                                      00400000
                                                                        00410000
       01  FIN105-RECORD.                                               00420001
           COPY SFIN000.                                                00421002
      *    03   FIN105-CODENR             PIC  X.                       00430002
      *    03   FIN105-CLEF.                                            00440002
      *         05   FIN105-TYPE          PIC  X(3).                    00450002
      *         05   FIN105-LIEU          PIC  X(3).                    00460002
      *         05   FIN105-RAYON         PIC  X(5).                    00470002
      *         05   FIN105-LIEUSTK       PIC  X(3).                    00480002
      *         05   FIN105-SEQFAM        PIC  9(5) COMP-3.             00490002
      *    03   FIN105-MARQUE             PIC  X(5).                    00500002
      *    03   FIN105-NCODIC             PIC  X(07).                   00510002
      *    03   FIN105-CFAM               PIC  X(5).                    00520002
      *    03   FIN105-LIBFAM             PIC  X(30).                   00530002
      *    03   FIN105-REFERENCE          PIC  X(20).                   00540002
      *    03   FIN105-NBETIQ             PIC  9(5) COMP-3.             00550002
      *    03   FIN105-DATEINV            PIC  X(8).                    00560002
      *    03   FIN105-FILLER             PIC  X(25).                   00561002
      *    03   FIN105-NEAN               PIC  X(13).                   00562002
      *    03   FILLER                    PIC  X(1).                    00570002
           EJECT                                                        00580000
      *                                                                 00590000
       FD   IIV105                                                      00600001
            RECORDING F                                                 00610001
            BLOCK 0 RECORDS                                             00620001
            LABEL RECORD STANDARD.                                      00630001
                                                                        00640001
       01  IIV105-RECORD.                                               00650001
           03   IIV105-LIGNE              PIC  X(133).                  00660001
      *                                                                 00670000
       FD   FDATE                                                       00680001
            RECORDING F                                                 00690001
            BLOCK 0 RECORDS                                             00700001
            LABEL RECORD STANDARD.                                      00710001
                                                                        00720001
       01  FILE-DATE.                                                   00730001
           03  F-DATE.                                                  00740001
               06   JJ     PIC X(2).                                    00750001
               06   MM     PIC X(2).                                    00760001
               06   SS     PIC X(2).                                    00770001
               06   AA     PIC X(2).                                    00780001
           03  FILLER PIC X(72).                                        00790001
      * ============================================================= * 00800000
      *                 W O R K I N G  -  S T O R A G E               * 00810000
      * ============================================================= * 00820000
      *                                                                 00830000
       WORKING-STORAGE SECTION.                                         00840000
      *                                                                 00850000
       77  W-RAYON                 PIC  X(05) VALUE SPACE.              00860000
       77  W-MAG                   PIC  X(03) VALUE SPACE.              00870001
       77  W-LIEUSTK               PIC  X(05).                          00880001
       77  W-SEQFAM                PIC  9(5) COMP-3.                    00890001
       77  W-NBETIQ                PIC  9(5) COMP-3.                    00900001
      *                                                                 00910000
       77  I-COL                  PIC  9.                               00920001
       77  I-LIGNE                PIC  9.                               00930001
      *                                                                 00940000
      *                                                                 00950000
       77  NB-FAM-MAX             PIC  9(5) COMP-3 VALUE 500.           00960000
       77  NB-FAM                 PIC  9(5) COMP-3.                     00970000
       77  I-FAM                  PIC  9(5) COMP-3.                     00980000
       77  IND1                   PIC  9(5) COMP-3.                     00990000
       77  I                      PIC  9(5) COMP-3.                     01000000
      *                                                                 01010000
       01  W-MARQ-EAN.                                                  01011001
           05  W-MARQ    PIC X(6).                                      01012001
           05  W-NEAN    PIC X(14).                                     01013001
      *                                                                 01020000
       01  ZONE-JOU.                                                    01030000
           05  Z-DATE    PIC X(8).                                      01040000
           05  FILLER    PIC X  VALUE '%'.                              01050000
      *                                                                 01060000
       01  DATE-JOU.                                                    01070000
           05  DATE-SS   PIC 99.                                        01080000
           05  DATE-AN   PIC 99.                                        01090000
           05  DATE-MO   PIC 99.                                        01100000
           05  DATE-JO   PIC 99.                                        01110000
      *                                                                 01120000
       01  W-INVDATE.                                                   01130001
           05  INV-SS    PIC XX.                                        01140001
           05  INV-AA    PIC XX.                                        01150001
           05  INV-MM    PIC XX.                                        01160001
           05  INV-JJ    PIC XX.                                        01170001
                                                                        01180001
       01  W-FORMAT-DATE.                                               01190001
           05  FORMAT-JJ    PIC XX.                                     01200001
           05  FORMAT1      PIC X VALUE '/'.                            01210001
           05  FORMAT-MM    PIC XX.                                     01220001
           05  FORMAT2      PIC X VALUE '/'.                            01230001
           05  FORMAT-AA    PIC XX.                                     01240001
       01  SYS-DATE.                                                    01250000
           03  SYS-SS              PIC  X(02).                          01260000
           03  SYS-AA              PIC  X(02).                          01270000
           03  SYS-MM              PIC  X(02).                          01280000
           03  SYS-JJ              PIC  X(02).                          01290000
      *                                                                 01300000
       01  EDT-DATE.                                                    01310000
           03  EDT-JJ              PIC  X(02).                          01320000
           03  FILLER              PIC  X(01)  VALUE '/'.               01330000
           03  EDT-MM              PIC  X(02).                          01340000
           03  FILLER              PIC  X(01)  VALUE '/'.               01350000
           03  EDT-SS              PIC  X(02).                          01360000
           03  EDT-AA              PIC  X(02).                          01370000
      *                                                                 01380000
       01  W-CMODDEL.                                                   01390000
           03  W-MOD1              PIC X(01).                           01400000
           03  W-MOD2              PIC X(02).                           01410000
      *                                                                 01420000
       01  FIN-FICHIER         PIC X  VALUE  '0'.                       01430001
      *                                                                 01440000
      *                                                                 01450000
      *                                                                 01460000
       01  CODE-RETOUR             PIC  X(01)  VALUE  '0'.              01470000
           88  TROUVE              VALUE  '0'.                          01480000
           88  NORMAL              VALUE  '0'.                          01490000
           88  NON-TROUVE          VALUE  '1'.                          01500000
           88  ANORMAL             VALUE  '1'.                          01510000
           88  EXISTE-DEJA         VALUE  '2'.                          01520000
           88  DOUBLE              VALUE  '7'.                          01530000
      *                                                                 01540000
       01  W-FIN105-LUES         PIC  S9(09)  COMP-3  VALUE  +0.        01550001
       01  W-IIV105-ECRITS       PIC  S9(09)  COMP-3  VALUE  +0.        01560001
      *                                                                 01570000
      ****************************************************************  01580001
      *                   DEFINITION DES LIGNES ETATS                *  01590001
      ****************************************************************  01600001
                                                                        01610001
       01  SAUT-PAGE.                                                   01620001
           05  SAUTPG        PIC X VALUE '1'.                           01630001
       01  SAUT-2LIGNES.                                                01640001
           05  SAUTLG2       PIC X VALUE '-'.                           01650001
       01  SAUT-LIGNE.                                                  01660001
           05  SAUTLG1       PIC X VALUE SPACES.                        01670001
                                                                        01680001
       01  LIGNE-ETAT1.                                                 01690001
           05  W-SAUT            PIC  X  VALUE '-'.                     01700001
           05  POSTE-FAM OCCURS 4 .                                     01710001
               10  W-LIB1            PIC  X(10).                        01720001
               10  W-LIB-FAM         PIC  X(20).                        01730001
               10  W-LIEU-DISP       PIC  X(3).                         01740001
      *                                                                 01750000
       01  LIGNE-ETAT2.                                                 01760001
           05  FILLER            PIC  X  VALUE SPACE.                   01770001
           05  POSTE-MARQ OCCURS 4 .                                    01780001
               10  W-LIB2            PIC  X(10).                        01790001
               10  W-LIB-MARQ        PIC  X(20).                        01800001
               10  FILLER            PIC  X(3).                         01810001
      *                                                                 01820001
       01  LIGNE-ETAT3.                                                 01830001
           05  FILLER            PIC  X  VALUE SPACE.                   01840001
           05  POSTE-REF OCCURS 4 .                                     01850001
               10  W-LIB3            PIC  X(10).                        01860001
               10  W-LIB-REF         PIC  X(20).                        01870001
               10  FILLER            PIC  X(3).                         01880001
      *                                                                 01890001
       01  LIGNE-ETAT4.                                                 01900001
           05  FILLER            PIC  X  VALUE SPACE.                   01910001
           05  POSTE-CODIC OCCURS 4 .                                   01920001
               10  W-LIB4            PIC  X(10).                        01930001
               10  W-LIB-CODIC       PIC  X(7).                         01940001
               10  FILLER            PIC  X(5).                         01950001
               10  W-DATEINV         PIC  X(8).                         01960001
               10  FILLER            PIC  X(3).                         01970001
      *                                                                 01980000
       01  INTITULE.                                                    01990001
           05  FILLER     PIC X(4).                                     02000001
           05  W-LIBELLE  PIC X(13).                                    02010001
           05  FILLER     PIC X(3).                                     02020001
                                                                        02030001
       01  CADRE.                                                       02040001
           05  FILLER     PIC X(9).                                     02050001
           05  W-VALEUR   PIC X(5).                                     02060001
           05  FILLER     PIC X(6).                                     02070001
                                                                        02080001
      *                                                                 02090000
           EJECT                                                        02100000
      *                                                                 02110000
      * ============================================================= * 02120000
      *        D E S C R I P T I O N      D E S      Z O N E S        * 02130000
      *           D ' A P P E L    M O D U L E    A B E N D           * 02140000
      * ============================================================= * 02150000
      *                                                                 02160000
           COPY  ABENDCOP.                                              02170000
      *                                                                 02180000
      *                                                                 02190000
      * ============================================================= * 02200000
      *     D E S C R I P T I O N    R E C O R D    B I V 1 0 5       * 02210001
      * ============================================================= * 02220000
      *                                                                 02230000
      *                                                                 02240000
           EJECT                                                        02250000
      *                                                                 02260000
       PROCEDURE DIVISION.                                              02270000
      *                                                                 02280000
      * ============================================================= * 02290000
      *              T R A M E   DU   P R O G R A M M E               * 02300000
      * ============================================================= * 02310000
      *                                                               * 02320000
      * ============================================================= * 02330000
      *                                                               * 02340000
      *           ------------------------------------------          * 02350000
      *           --  MODULE DE BASE DU PROGRAMME  BIV105 --          * 02360001
      *           ------------------------------------------          * 02370000
      *                               I                               * 02380000
      *           -----------------------------------------           * 02390000
      *           I                   I                   I           * 02400000
      *   -----------------   -----------------   -----------------   * 02410000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 02420000
      *   -----------------   -----------------   -----------------   * 02430000
      *                                                               * 02440000
      * ============================================================= * 02450000
      *                                                                 02460000
      *                                                                 02470000
           PERFORM  DEBUT-BIV105.                                       02480001
           PERFORM  FIRST-LECT-FIN105.                                  02490001
           PERFORM  TRAIT-ETIQUETTES UNTIL FIN-FICHIER = '1'.           02500001
           PERFORM  FIN-BIV105.                                         02510001
      *                                                                 02520000
           EJECT                                                        02530000
      *                                                                 02540000
      * ============================================================= * 02550000
      *                  D E B U T        B I V 1 0 5                 * 02560001
      * ============================================================= * 02570000
      *                                                                 02580000
       DEBUT-BIV105      SECTION.                                       02590001
             DISPLAY 'DEBUT-BIV105      SECTION'.                       02600001
      *                                                                 02610000
           PERFORM  OUVERTURE-FICHIERS.                                 02620000
      * ON INITIALISE LA DATE DU JOUR                                   02630000
           PERFORM INIT-DAT.                                            02640000
           MOVE 0    TO W-FIN105-LUES .                                 02650001
           MOVE 0    TO W-IIV105-ECRITS.                                02660001
           MOVE 1    TO I-COL.                                          02670001
           MOVE SPACES  TO W-MAG.                                       02680001
           MOVE SPACES  TO W-RAYON.                                     02690001
           MOVE SPACES  TO W-LIEUSTK.                                   02700001
           INITIALIZE  LIGNE-ETAT1.                                     02710001
           MOVE FIN000-LIEU        TO W-LIEU-DISP(1)                    02711002
           INITIALIZE  LIGNE-ETAT2.                                     02720001
           INITIALIZE  LIGNE-ETAT3.                                     02730001
           INITIALIZE  LIGNE-ETAT4.                                     02740001
      *                                                                 02750000
       FIN-DEBUT-BIV105.  EXIT.                                         02760001
           EJECT                                                        02770001
                                                                        02780001
      * ============================================================= * 02790000
      *          M O D U L E S    D E B U T    B I V 1 0 5            * 02800001
      * ============================================================= * 02810000
      *                                                                 02820000
      *====>  TRAITEMENT  POUR RECUPERER LA DATE DU JOUR                02830000
      *                                                                 02840000
       INIT-DAT                SECTION.                                 02850000
                  DISPLAY 'INIT-DAT    SECTION'.                        02860000
           OPEN  INPUT   FDATE                                          02870000
           READ FDATE AT END                                            02880000
             MOVE  '**  PAS  DE  DATE  EN  ENTREE  **'                  02890000
                                           TO  ABEND-MESS               02900000
             PERFORM  FIN-ANORMALE                                      02910000
           END-READ.                                                    02920000
           MOVE SS TO DATE-SS EDT-SS.                                   02930000
           MOVE AA TO DATE-AN EDT-AA.                                   02940000
           MOVE MM TO DATE-MO EDT-MM.                                   02950000
           MOVE JJ TO DATE-JO EDT-JJ.                                   02960000
           CLOSE  FDATE .                                               02970000
       FIN-INIT-DAT.      EXIT.                                         02980000
      *                                                                 02990000
      *====>  O U V E R T U R E  -  F I C H I E R S .                   03000000
      *                                                                 03010000
       OUVERTURE-FICHIERS     SECTION.                                  03020000
                                                                        03030000
           OPEN  INPUT   FIN105.                                        03040001
           OPEN  OUTPUT  IIV105.                                        03050001
                                                                        03060000
       FIN-OUVERTURE-FICHIERS.     EXIT.                                03070000
      *                                                                 03080000
      *                                                                 03090000
      *                                                                 03100000
      * ============================================================= * 03110000
      *          M O D U L E S    T R A I T    B I V 1 0 5            * 03120001
      * ============================================================= * 03130000
                                                                        03140000
       FIRST-LECT-FIN105       SECTION.                                 03150001
                  DISPLAY 'FIRST-LECT  SECTION'.                        03160001
           READ FIN105 AT END                                           03170001
                   MOVE '1'   TO FIN-FICHIER                            03180001
           END-READ.                                                    03190001
                                                                        03200001
           IF FIN-FICHIER NOT = '1'                                     03210001
              MOVE   FIN000-LIEU          TO W-MAG                      03220002
              MOVE   FIN000-RAYON         TO W-RAYON                    03230002
              MOVE   FIN000-LIEUSTK       TO W-LIEUSTK                  03240002
              MOVE   FIN000-SEQFAM        TO W-SEQFAM                   03250002
              MOVE   FIN000-NBETIQ        TO W-NBETIQ                   03260002
              ADD   1                     TO W-FIN105-LUES              03270001
              PERFORM ECRITURE-ENTETE                                   03280001
           ELSE                                                         03290001
              DISPLAY 'PAS DE DEMANDE D''EDITION D''ETIQUETTES'         03300001
           END-IF.                                                      03310001
                                                                        03320001
       FIN-FIRST-LECT-FIN105. EXIT.                                     03330001
            EJECT                                                       03340001
      *----------------------------------------------------------------*03350001
      *   TRAITEMENT GENERAL DE L'EDITION DES ETIQUETTES               *03360001
      *----------------------------------------------------------------*03370001
                                                                        03380001
       TRAIT-ETIQUETTES   SECTION.                                      03390001
                                                                        03400001
      *    MODIFICATION DU SAUT DE PAGE A LA DEMANDE DE NADINE PAR P.M  03410001
      *    IF FIN000-LIEU = W-MAG AND FIN000-RAYON = W-RAYON AND        03410102
      *       FIN000-LIEUSTK = W-LIEUSTK AND FIN000-SEQFAM = W-SEQFAM   03420002
           IF FIN000-LIEU = W-MAG AND FIN000-RAYON = W-RAYON AND        03420102
              FIN000-LIEUSTK = W-LIEUSTK                                03420202
              PERFORM EDIT-ARTICLE-ETIQ                                 03430001
              PERFORM LECT-SUIV-FIN105                                  03440001
           ELSE                                                         03450001
              IF I-COL NOT EQUAL 1                                      03460001
                 MOVE '-'    TO W-SAUT                                  03470001
                 PERFORM ECRITURE-LIGNE-ETAT                            03480001
              END-IF                                                    03490001
      *       WRITE IIV105-RECORD    FROM  SAUT-PAGE                    03500001
              PERFORM ECRITURE-ENTETE                                   03510001
              PERFORM RECUP-DERNIERES-ZONES                             03520001
           END-IF.                                                      03530001
                                                                        03540001
       FIN-TRAIT-ETIQUETTES.   EXIT.                                    03550001
                                                                        03560001
      *----------------------------------------------------------------*03570001
      *   ECRITURE D'AUTANT D'ETIQUETTES DEMANDEES PAR RAYON/SLIEU     *03580001
      *   GESTION DES SAUTS LE LIGNE DANS LE CAS OU 4 ETIQUETTES ONT   *03590001
      *                        ETE ECRITES                             *03600001
      *----------------------------------------------------------------*03610001
       EDIT-ARTICLE-ETIQ  SECTION.                                      03620001
           PERFORM VARYING  W-NBETIQ FROM W-NBETIQ BY -1                03630001
                                  UNTIL W-NBETIQ < 1                    03640001
              MOVE 'FAMILLE : '       TO W-LIB1(I-COL)                  03650001
              MOVE FIN000-LIBFAM      TO W-LIB-FAM(I-COL)               03660002
              IF I-COL = 1                                              03661001
                 MOVE FIN000-LIEU        TO W-LIEU-DISP(I-COL)          03662002
              ELSE                                                      03663001
                 MOVE SPACE              TO W-LIEU-DISP(I-COL)          03664001
              END-IF                                                    03665001
              MOVE 'MARQUE  : '       TO W-LIB2(I-COL)                  03670001
              MOVE FIN000-MARQUE      TO W-MARQ                         03680002
              MOVE FIN000-NEAN        TO W-NEAN                         03681002
              MOVE W-MARQ-EAN         TO W-LIB-MARQ(I-COL)              03682001
              MOVE 'REFER   : '       TO W-LIB3(I-COL)                  03690001
              MOVE FIN000-REFERENCE   TO W-LIB-REF(I-COL)               03700002
              MOVE 'CODIC   : '       TO W-LIB4(I-COL)                  03710001
              MOVE FIN000-NCODIC      TO W-LIB-CODIC(I-COL)             03720002
              MOVE FIN000-DATEINV     TO W-INVDATE                      03730002
              MOVE INV-JJ             TO FORMAT-JJ                      03740001
              MOVE INV-MM             TO FORMAT-MM                      03750001
              MOVE INV-AA             TO FORMAT-AA                      03760001
              MOVE W-FORMAT-DATE      TO W-DATEINV(I-COL)               03770001
              ADD 1 TO I-COL                                            03780001
              IF I-COL > 4                                              03790001
                 MOVE '-'    TO W-SAUT                                  03800001
                 PERFORM ECRITURE-LIGNE-ETAT                            03810001
              END-IF                                                    03820001
           END-PERFORM.                                                 03830001
                                                                        03840001
       FIN-EDIT-ARTICLE-ETIQ. EXIT.                                     03850001
           EJECT                                                        03860001
      *----------------------------------------------------------------*03870001
      *         LECTURE SUIVANTE SANS SAUVEGARDE DES ZONES LUES        *03880001
      *----------------------------------------------------------------*03890001
       LECT-SUIV-FIN105     SECTION.                                    03900001
           READ FIN105 AT END                                           03910001
                   MOVE '1'   TO FIN-FICHIER                            03920001
           END-READ.                                                    03930001
                                                                        03940001
           IF FIN-FICHIER NOT = '1'                                     03950001
              ADD   1                     TO W-FIN105-LUES              03960001
              MOVE   FIN000-NBETIQ        TO W-NBETIQ                   03970002
           ELSE                                                         03980001
              IF I-COL > 1                                              03990001
                 MOVE '-'    TO W-SAUT                                  04000001
                 PERFORM ECRITURE-LIGNE-ETAT                            04010001
              END-IF                                                    04020001
           END-IF.                                                      04030001
                                                                        04040001
       FIN-LECT-SUIV-FIN105. EXIT.                                      04050001
            EJECT                                                       04060001
      *----------------------------------------------------------------*04070001
      *         SAUVEGARDE DES ZONES LUES PRECEDEMMENT DANS LE CAS DE  *04080001
      *         RUPTURE CE PROCEDE PERMET D'AVOIR UNE LECTURE D'AVANCE *04090001
      *----------------------------------------------------------------*04100001
       RECUP-DERNIERES-ZONES       SECTION.                             04110001
                                                                        04120001
           MOVE   FIN000-LIEU          TO W-MAG.                        04130002
           MOVE   FIN000-RAYON         TO W-RAYON.                      04140002
           MOVE   FIN000-LIEUSTK       TO W-LIEUSTK.                    04150002
           MOVE   FIN000-SEQFAM        TO W-SEQFAM.                     04160002
                                                                        04170001
       FIN-RECUP-DERNIERES-ZONES. EXIT.                                 04180001
          EJECT                                                         04190001
      *----------------------------------------------------------------*04200001
      *         ECRITURE DE L'ENTETE (MAGASIN RAYON LIEU DE STOCK)     *04210001
      *                      DANS LE CAS D'UNE RUPTURE                 *04220001
      *----------------------------------------------------------------*04230001
       ECRITURE-ENTETE     SECTION.                                     04240001
                                                                        04250001
           INITIALIZE  LIGNE-ETAT1.                                     04260001
           MOVE FIN000-LIEU        TO W-LIEU-DISP(1)                    04261002
           INITIALIZE  LIGNE-ETAT2.                                     04270001
           INITIALIZE  LIGNE-ETAT3.                                     04280001
           INITIALIZE  LIGNE-ETAT4.                                     04290001
           MOVE 1                TO   I-COL.                            04300001
           MOVE 'M A G A S I N'  TO   W-LIBELLE.                        04310001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04320001
           MOVE FIN000-LIEU      TO   W-VALEUR.                         04330002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04340001
           ADD 1                 TO   I-COL.                            04350001
           MOVE '   R A Y O N '  TO   W-LIBELLE.                        04360001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04370001
           MOVE FIN000-RAYON     TO   W-VALEUR.                         04380002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04390001
           ADD 1                 TO   I-COL.                            04400001
           MOVE '  S/L I E U  '  TO   W-LIBELLE.                        04410001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04420001
           MOVE FIN000-LIEUSTK   TO   W-VALEUR.                         04430002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04440001
           ADD 1                 TO   I-COL.                            04450001
           MOVE '   T Y P E   '  TO   W-LIBELLE.                        04460001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04470001
           MOVE FIN000-TYPE      TO   W-VALEUR.                         04480002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04490001
                                                                        04500001
           MOVE '1'      TO W-SAUT.                                     04510001
      *    MOVE '-'      TO W-SAUT.                                     04510100
           PERFORM ECRITURE-LIGNE-ETAT.                                 04520001
           MOVE '-'    TO W-SAUT.                                       04530001
                                                                        04540001
       FIN-ECRITURE-ENTETE.  EXIT.                                      04550001
           EJECT                                                        04560001
      *----------------------------------------------------------------*04570001
      *               ECRITURE DE LES LIGNES D'ETAT                    *04580001
      *----------------------------------------------------------------*04590001
                                                                        04600001
       ECRITURE-LIGNE-ETAT     SECTION.                                 04610001
                                                                        04620001
           WRITE IIV105-RECORD    FROM  LIGNE-ETAT1.                    04630001
           WRITE IIV105-RECORD    FROM  LIGNE-ETAT2.                    04640001
           WRITE IIV105-RECORD    FROM  LIGNE-ETAT3.                    04650001
           WRITE IIV105-RECORD    FROM  LIGNE-ETAT4.                    04660001
           INITIALIZE  LIGNE-ETAT1.                                     04670001
           MOVE FIN000-LIEU        TO W-LIEU-DISP(1)                    04671002
           INITIALIZE  LIGNE-ETAT2.                                     04680001
           INITIALIZE  LIGNE-ETAT3.                                     04690001
           INITIALIZE  LIGNE-ETAT4.                                     04700001
           MOVE 1   TO I-COL .                                          04710001
           ADD  4                  TO W-IIV105-ECRITS.                  04720001
       FIN-ECRITURE-LIGNE-ETAT. EXIT.                                   04730001
          EJECT                                                         04740001
                                                                        04750001
      *                                                                 04760000
      *                                                                 04770000
      * ============================================================= * 04780000
      *                      F I N        B I V 1 0 5                 * 04790001
      * ============================================================= * 04800000
      *                                                                 04810000
       FIN-BIV105     SECTION.                                          04820001
                                                                        04830000
           PERFORM  COMPTE-RENDU.                                       04840000
           PERFORM  FERMETURE-FICHIERS.                                 04850000
           PERFORM  FIN-PROGRAMME.                                      04860000
                                                                        04870000
       FIN-FIN-BIV105.   EXIT.                                          04880001
      * ============================================================= * 04890000
      *          M O D U L E S      F I N      B I V 1 0 5            * 04900001
      * ============================================================= * 04910000
      *                                                                 04920000
      *====>  F I N  -  A N O R M A L E.                                04930000
      *                                                                 04940000
       FIN-ANORMALE    SECTION.                                         04950000
                                                                        04960000
           PERFORM  FERMETURE-FICHIERS.                                 04970000
           MOVE  'BIV105'                  TO  ABEND-PROG.              04980001
                                                                        04990000
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                05000000
                                                                        05010000
       FIN-FIN-ANORMALE.    EXIT.                                       05020000
      *                                                                 05030000
      *====>  C O M P T E  -  R E N D U.                                05040000
      *                                                                 05050000
       COMPTE-RENDU      SECTION.                                       05060000
                                                                        05070000
           DISPLAY  '**'.                                               05080000
           DISPLAY  '**           B I V 1 0 5 '.                        05090001
           DISPLAY  '**'.                                               05100000
           DISPLAY  '** DATE  PARAMETRE                         : '     05110000
                     EDT-DATE.                                          05120000
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  ECRITS         : '     05130000
                    W-IIV105-ECRITS.                                    05140001
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  LUS SUR FIN105 : '     05150001
                    W-FIN105-LUES.                                      05160001
                                                                        05170000
       FIN-COMPTE-RENDU.        EXIT.                                   05180000
      *                                                                 05190000
      *====>  F E R M E T U R E    F I C H I E R S.                     05200000
      *                                                                 05210000
       FERMETURE-FICHIERS    SECTION.                                   05220000
                                                                        05230000
           CLOSE  FIN105.                                               05240001
           CLOSE  IIV105.                                               05250001
                                                                        05260000
       F-FERMETURE-FICHIERS.    EXIT.                                   05270000
      *                                                                 05280000
      *====>  F I N    P R O G R A M M E.                               05290000
      *                                                                 05300000
       FIN-PROGRAMME        SECTION.                                    05310000
                                                                        05320000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   05330000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        05340000
       F-FIN-PROGRAMME.      EXIT.                                      05350000
           EJECT                                                        05360000
                                                                        05370000
