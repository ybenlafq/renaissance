      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.            BIV142.                                   00020001
       AUTHOR.      ANNICK.                                             00030000
      *                                                                 00040000
      * ============================================================= * 00050000
      *                                                               * 00060000
      *       PROJET        :  EDITION DES ECARTS INVENTAIRE PRODUITS * 00070000
      *       PROGRAMME     :  BIV142.                                * 00080001
      *       PERIODICITE   :  EDITION A LA DEMANDE                   * 00090000
      *       FONCTION      :  EDITION DES ECARTS INVENTAIRE          * 00100000
      *       DATE CREATION :   4/07/1990.                            * 00110001
      *                                                               * 00120000
      * ============================================================= * 00130000
      *                                                                 00140000
       ENVIRONMENT DIVISION.                                            00150000
       CONFIGURATION SECTION.                                           00160000
       SPECIAL-NAMES.                                                   00170000
           DECIMAL-POINT IS COMMA.                                      00180000
       INPUT-OUTPUT SECTION.                                            00190000
      *                                                                 00200000
       FILE-CONTROL.                                                    00210000
      *                                                                 00220000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FIV142        ASSIGN  TO  FIV142.                   00230001
      *--                                                                       
            SELECT  FIV142        ASSIGN  TO  FIV142                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  IIV142        ASSIGN  TO  IIV142.                   00240001
      *--                                                                       
            SELECT  IIV142        ASSIGN  TO  IIV142                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  IIV142A       ASSIGN  TO  IIV142A.                  00250001
      *--                                                                       
            SELECT  IIV142A       ASSIGN  TO  IIV142A                           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE         ASSIGN  TO  FDATE.                    00260000
      *--                                                                       
            SELECT  FDATE         ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPARAM        ASSIGN  TO  FPARAM.                   00270001
      *--                                                                       
            SELECT  FPARAM        ASSIGN  TO  FPARAM                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00280000
                                                                        00290000
      *                                                                 00300000
       DATA DIVISION.                                                   00310000
       FILE SECTION.                                                    00320000
      *                                                                 00330000
      * ============================================================= * 00340000
      *  D E F I N I T I O N    F I C H I E R    E X T R A C T I O N  * 00350000
      * ============================================================= * 00360000
      *                                                                 00370000
       FD   FIV142                                                      00380001
            RECORDING F                                                 00390000
            BLOCK 0 RECORDS                                             00400000
            LABEL RECORD STANDARD.                                      00410000
                                                                        00420000
           COPY FIV130.                                                 00430000
      *01  F130-RECORD.                                                 00440000
      *    05   F130-NSOCMAG              PIC  X(3).                    00450000
      *    05   F130-NMAG                 PIC  X(3).                    00460000
      *    05   F130-NLIEU                PIC  X(3).                    00470000
      *    05   F130-DINVENTAIRE          PIC  X(8).                    00480000
      *    05   F130-RAYON                PIC  X(5).                    00490000
      *    05   F130-NCODIC               PIC  X(07).                   00500000
      *    05   F130-LREFFOURN            PIC  X(20).                   00510000
      *    05   F130-CAPPRO               PIC  X(5).                    00520000
      *    05   F130-CMARQ                PIC  X(5).                    00530000
      *    05   F130-CFAM                 PIC  X(5).                    00540000
      *    05   F130-WSEQED               PIC  S9(5)      COMP-3.       00550000
      *    05   F130-WSEQFAM              PIC  S9(5)      COMP-3.       00560000
      *    05   F130-WSTOCKMASQ           PIC  X.                       00570000
      *    05   F130-NSSLIEU              PIC  X(3).                    00580000
      *    05   F130-QSTOCK               PIC  S9(5)      COMP-3.       00590000
      *    05   F130-PRMP                 PIC  S9(7)V9(6) COMP-3.       00600000
      *    05   F130-QSTOCKEC1            PIC  S9(5)      COMP-3.       00610000
           EJECT                                                        00620000
      *                                                                 00630000
       FD   IIV142                                                      00640001
            RECORDING F                                                 00650000
            BLOCK 0 RECORDS                                             00660000
            LABEL RECORD STANDARD.                                      00670000
                                                                        00680000
       01  IIV142-RECORD.                                               00690001
           03   IIV142-LIGNE              PIC  X(133).                  00700001
       FD   IIV142A                                                     00710001
            RECORDING F                                                 00720001
            BLOCK 0 RECORDS                                             00730001
            LABEL RECORD STANDARD.                                      00740001
                                                                        00750001
       01  IIV142A-RECORD.                                              00760001
           03   IIV142A-LIGNE              PIC  X(133).                 00770001
      *                                                                 00780000
       FD   FPARAM                                                      00790001
            RECORDING F                                                 00800001
            BLOCK 0 RECORDS                                             00810001
            LABEL RECORD STANDARD.                                      00820001
       01  FILE-FPARAM.                                                 00830001
           03  F-PARAM.                                                 00840001
               06  PARAM  PIC X(3).                                     00850001
               06  FILLER PIC X(77).                                    00860001
                                                                        00870000
       FD   FDATE                                                       00880000
            RECORDING F                                                 00890000
            BLOCK 0 RECORDS                                             00900000
            LABEL RECORD STANDARD.                                      00910000
                                                                        00920000
       01  FILE-DATE.                                                   00930000
           03  F-DATE.                                                  00940000
               06   JJ     PIC X(2).                                    00950000
               06   MM     PIC X(2).                                    00960000
               06   SS     PIC X(2).                                    00970000
               06   AA     PIC X(2).                                    00980000
           03  FILLER PIC X(72).                                        00990000
      * ============================================================= * 01000000
      *                 W O R K I N G  -  S T O R A G E               * 01010000
      * ============================================================= * 01020000
      *                                                                 01030000
       WORKING-STORAGE SECTION.                                         01040000
      *                                                                 01050000
       77  Z-FAM                   PIC X(5) VALUE SPACES.               01060000
       77  ANO-FAM                 PIC X(5) VALUE SPACES.               01070001
       77  Z-REF                   PIC X(20) VALUE SPACES.              01080001
       77  Z-MARQUE                PIC X(5) VALUE SPACES.               01090001
       77  Z-RAYON                 PIC X(5) VALUE SPACES.               01100000
       77  ANO-RAYON               PIC X(5) VALUE SPACES.               01110001
       77  Z-MAG                   PIC X(3) VALUE SPACES.               01120000
       77  Z-SOC                   PIC X(3) VALUE SPACES.               01130001
       77  Z-NCODIC                PIC X(7) VALUE SPACES.               01140001
       77  Z-PRMP                  PIC  S9(7)V9(6) COMP-3.              01150001
       77  CPT-PAGE                PIC 999  VALUE 0.                    01160000
       77  CPT-LIGNE               PIC 99   VALUE 0.                    01170000
       77  CPT-PAGE-ANO            PIC 999  VALUE 0.                    01180001
       77  CPT-LIGNE-ANO           PIC 99   VALUE 0.                    01190001
       77  CPT-LIGNEBIS            PIC 99   VALUE 0.                    01200000
      *                                                                 01210000
       77  PASSAGE-ENTETE         PIC  X VALUE SPACES.                  01220000
       77  PASSAGE-ENTETE-ANO     PIC  X VALUE SPACES.                  01230001
      *                                                                 01240000
       01  W-RUPT-FAM             PIC X VALUE SPACES.                   01250000
       01  W-RUPT-FAMANO          PIC X VALUE SPACES.                   01260001
       01  W-RUPT-RAY             PIC X VALUE SPACES.                   01270000
       01  W-INTITULE             PIC X(21).                            01280001
       01  W-PARAM                PIC X(3).                             01290001
      *                                                                 01300000
      *                                                                 01310000
       01  ZONE-CUM-FAM.                                                01320000
           05  W-CUM-THEOFAM          PIC S9(9).                        01330001
           05  W-CUM-INVFAM           PIC S9(9).                        01340001
           05  W-CUM-VALTHEOFAM       PIC S9(12)V9(6).                  01350001
           05  W-CUM-VALINVFAM        PIC S9(12)V9(6).                  01360001
           05  W-CUM-VALORISEFAM      PIC S9(10)V9(6).                  01370001
           05  W-CUM-ECARTFAM         PIC S9(9).                        01380001
      *                                                                 01390001
       01  ZONE-CUM-NCOD.                                               01400001
           05  W-CUM-THEONCOD         PIC S9(9).                        01410001
           05  W-CUM-INVNCOD          PIC S9(9).                        01420001
           05  W-CUM-VALTHEONCOD      PIC S9(12)V9(6).                  01430001
           05  W-CUM-VALINVNCOD       PIC S9(12)V9(6).                  01440001
           05  W-CUM-VALORISENCOD     PIC S9(10)V9(6).                  01450001
           05  W-CUM-ECARTNCOD        PIC S9(9).                        01460001
      *                                                                 01470000
       01  ZONE-CUM-MAG.                                                01480000
           05  W-CUM-THEOMAG          PIC S9(9).                        01490001
           05  W-CUM-INVMAG           PIC S9(9).                        01500001
           05  W-CUM-VALTHEOMAG       PIC S9(12)V9(6).                  01510001
           05  W-CUM-VALINVMAG        PIC S9(12)V9(6).                  01520001
           05  W-CUM-VALORISEMAG      PIC S9(10)V9(6).                  01530001
           05  W-CUM-ECARTMAG         PIC S9(9).                        01540001
      *                                                                 01550000
      *                                                                 01560001
       01  ZONE-CUM-RAY.                                                01570000
           05  W-CUM-THEORAY          PIC S9(9).                        01580001
           05  W-CUM-INVRAY           PIC S9(9).                        01590001
           05  W-CUM-VALTHEORAY       PIC S9(12)V9(6).                  01600001
           05  W-CUM-VALINVRAY        PIC S9(12)V9(6).                  01610001
           05  W-CUM-ECARTRAY         PIC S9(9).                        01620001
           05  W-CUM-VALORISERAY      PIC S9(10)V9(6).                  01630001
      *                                                                 01640000
      *--------------------------------------------------------------*  01650001
      *       ZONES DE CUMULS POUR LES ANOMALIES                        01660001
      *--------------------------------------------------------------*  01670001
                                                                        01680001
       01  ZONE-ANOCUM-FAM.                                             01690001
           05  A-CUM-THEOFAM          PIC S9(9).                        01700001
           05  A-CUM-INVFAM           PIC S9(9).                        01710001
           05  A-CUM-VALECARTFAM      PIC S9(12)V9(6).                  01720001
           05  A-CUM-ECARTFAM         PIC S9(9).                        01730001
      *                                                                 01740001
       01  ZONE-ANOCUM-RAY.                                             01750001
           05  A-CUM-THEORAY          PIC S9(9).                        01760001
           05  A-CUM-INVRAY           PIC S9(9).                        01770001
           05  A-CUM-VALECARTRAY      PIC S9(12)V9(6).                  01780001
           05  A-CUM-ECARTRAY         PIC S9(9).                        01790001
      *                                                                 01800001
      *                                                                 01810001
       01  ZONE-ANOCUM-MAG.                                             01820001
           05  A-CUM-THEOMAG          PIC S9(9).                        01830001
           05  A-CUM-INVMAG           PIC S9(9).                        01840001
           05  A-CUM-VALECARTMAG      PIC S9(12)V9(6).                  01850001
           05  A-CUM-ECARTMAG         PIC S9(9).                        01860001
                                                                        01870001
       01  ZONE-CALCUL.                                                 01880001
           05  T-QTETHEO            PIC S9(5).                          01890001
           05  T-QTEINV             PIC S9(9).                          01900001
           05  T-QTECART            PIC S9(5).                          01910001
           05  T-VALTHEO            PIC S9(12)V9(6).                    01920001
           05  T-VALECART           PIC S9(12)V9(6).                    01930001
           05  T-VALINV             PIC S9(12)V9(6).                    01940001
           05  T-VALORISE           PIC S9(10)V9(6).                    01950001
           05  T-PRMP               PIC S9(7)V9(6).                     01960001
       01  ZONE-JOU.                                                    01970000
           05  Z-DATE    PIC X(8).                                      01980000
           05  FILLER    PIC X  VALUE '%'.                              01990000
      *                                                                 02000000
       01  DATE-JOU.                                                    02010000
           05  DATE-SS   PIC 99.                                        02020000
           05  DATE-AN   PIC 99.                                        02030000
           05  DATE-MO   PIC 99.                                        02040000
           05  DATE-JO   PIC 99.                                        02050000
      *                                                                 02060000
       01  Z-HOUR.                                                      02070000
           05  HOUR-HH   PIC 99.                                        02080000
           05  HOUR-MM   PIC 99.                                        02090000
           05  HOUR-SS   PIC 99.                                        02100000
           05  HOUR-CC   PIC 99.                                        02110000
       01  W-HEURE.                                                     02120000
           05  HEURE-HH   PIC 99.                                       02130000
           05  FILLEUR    PIC X VALUE 'H'.                              02140000
           05  HEURE-MM   PIC 99.                                       02150000
      *                                                                 02160000
       01  W-INVDATE.                                                   02170000
           05  INV-SS    PIC XX.                                        02180000
           05  INV-AA    PIC XX.                                        02190000
           05  INV-MM    PIC XX.                                        02200000
           05  INV-JJ    PIC XX.                                        02210000
                                                                        02220000
       01  W-FORMAT-DATE.                                               02230000
           05  FORMAT-JJ    PIC XX.                                     02240000
           05  FORMAT1      PIC X VALUE '/'.                            02250000
           05  FORMAT-MM    PIC XX.                                     02260000
           05  FORMAT2      PIC X VALUE '/'.                            02270000
           05  FORMAT-AA    PIC XX.                                     02280000
       01  SYS-DATE.                                                    02290000
           03  SYS-SS              PIC  X(02).                          02300000
           03  SYS-AA              PIC  X(02).                          02310000
           03  SYS-MM              PIC  X(02).                          02320000
           03  SYS-JJ              PIC  X(02).                          02330000
      *                                                                 02340000
       01  EDT-DATE.                                                    02350000
           03  EDT-JJ              PIC  X(02).                          02360000
           03  FILLER              PIC  X(01)  VALUE '/'.               02370000
           03  EDT-MM              PIC  X(02).                          02380000
           03  FILLER              PIC  X(01)  VALUE '/'.               02390000
           03  EDT-SS              PIC  X(02).                          02400000
           03  EDT-AA              PIC  X(02).                          02410000
      *                                                                 02420000
       01  W-CMODDEL.                                                   02430000
           03  W-MOD1              PIC X(01).                           02440000
           03  W-MOD2              PIC X(02).                           02450000
      *                                                                 02460000
       01  FIN-FICHIER         PIC X  VALUE  '0'.                       02470000
      *                                                                 02480000
      *                                                                 02490000
      *                                                                 02500000
       01  CODE-RETOUR             PIC  X(01)  VALUE  '0'.              02510000
           88  TROUVE              VALUE  '0'.                          02520000
           88  NORMAL              VALUE  '0'.                          02530000
           88  NON-TROUVE          VALUE  '1'.                          02540000
           88  ANORMAL             VALUE  '1'.                          02550000
           88  EXISTE-DEJA         VALUE  '2'.                          02560000
           88  DOUBLE              VALUE  '7'.                          02570000
      *                                                                 02580000
       01  W-FIV142-LUES           PIC  S9(09)  COMP-3  VALUE  +0.      02590001
       01  W-IIV142-ECRITS         PIC  S9(09)  COMP-3  VALUE  +0.      02600001
       01  W-IIV142A-ECRITS        PIC  S9(09)  COMP-3  VALUE  +0.      02610001
      *                                                                 02620000
      ****************************************************************  02630000
      *                   DEFINITION DES LIGNES ETATS                *  02640000
      ****************************************************************  02650000
                                                                        02660000
       01  SAUT-PAGE.                                                   02670000
           05  SAUTPG        PIC X VALUE '1'.                           02680000
       01  SAUT-2LIGNES.                                                02690000
           05  SAUTLG2       PIC X VALUE '-'.                           02700000
       01  SAUT-LIGNE.                                                  02710000
           05  SAUTLG1       PIC X VALUE SPACES.                        02720000
       01  ETAT-TIRET.                                                  02730000
           05  W-REECRIT     PIC X       VALUE SPACES.                  02740000
           05  FILLER        PIC X       VALUE '!'.                     02750000
           05  TIRET         PIC X(130)  VALUE ALL '-'.                 02760000
           05  FILLER        PIC X       VALUE '!'.                     02770000
                                                                        02780000
       01  LIGNE-SEPAR.                                                 02790000
           05 FILLER     PIC X     VALUE SPACE.                         02800000
           05 FILLER     PIC X     VALUE '!'.                           02810000
           05 W-TIR      PIC X(5)  VALUE SPACES.                        02820000
           05 FILLER     PIC X     VALUE '!'.                           02830000
           05 FILLER     PIC X(7)  VALUE ALL '-'.                       02840000
           05 FILLER     PIC X     VALUE '!'.                           02850000
           05 FILLER     PIC X(6)  VALUE ALL '-'.                       02860000
           05 FILLER     PIC X     VALUE '!'.                           02870000
           05 FILLER     PIC X(20) VALUE ALL '-'.                       02880000
           05 FILLER     PIC X     VALUE '!'.                           02890000
           05 FILLER     PIC X(7)  VALUE ALL '-'.                       02900000
           05 FILLER     PIC X     VALUE '!'.                           02910000
           05 FILLER     PIC X(6)  VALUE ALL '-'.                       02920000
           05 FILLER     PIC X     VALUE '!'.                           02930000
           05 FILLER     PIC X(6)  VALUE ALL '-'.                       02940000
           05 FILLER     PIC X     VALUE '!'.                           02950000
           05 FILLER     PIC X(7)  VALUE ALL '-'.                       02960000
           05 FILLER     PIC X     VALUE '!'.                           02970000
           05 FILLER     PIC X(09) VALUE ALL '-'.                       02980000
           05 FILLER     PIC X     VALUE '!'.                           02990000
           05 FILLER     PIC X(11) VALUE ALL '-'.                       03000000
           05 FILLER     PIC X     VALUE '!'.                           03010000
           05 FILLER     PIC X(11) VALUE ALL '-'.                       03020000
           05 FILLER     PIC X     VALUE '!'.                           03030000
           05 FILLER     PIC X(11) VALUE ALL '-'.                       03040000
           05 FILLER     PIC X     VALUE '!'.                           03050000
           05 FILLER     PIC X(12) VALUE ALL '-'.                       03060000
           05 FILLER     PIC X     VALUE '!'.                           03070000
       01  ENTETE1.                                                     03080000
           05 FILLER     PIC X  VALUE SPACES.                           03090001
           05 FILLER     PIC X(6)  VALUE 'IIV142'.                      03100001
           05 FILLER     PIC X(46) VALUE SPACES.                        03110001
           05 FILLER    PIC X(29) VALUE 'ETAT DES ECARTS D''INVENTAIRE'.03120000
           05 FILLER     PIC X(38) VALUE SPACES.                        03130000
           05 FILLER     PIC X(7) VALUE 'PAGE : '.                      03140000
           05 W-PAGE     PIC ZZ9.                                       03150000
           05 FILLER     PIC X(3) VALUE SPACES.                         03160000
       01  ENTETE2.                                                     03170000
           05 FILLER     PIC X(58) VALUE SPACES.                        03180000
           05 FILLER     PIC X(15) VALUE 'AU NIVEAU CODIC'.             03190000
           05 FILLER     PIC X(58) VALUE SPACES.                        03200001
                                                                        03210000
       01  ENTETE3.                                                     03220000
           05 FILLER     PIC X      VALUE '-'.                          03230000
           05 FILLER     PIC X(7)   VALUE 'DATE : '.                    03240000
           05 W-DATE     PIC X(8).                                      03250000
           05 FILLER     PIC X(3)   VALUE SPACES.                       03260000
           05 FILLER     PIC X(8)   VALUE 'HEURE : '.                   03270000
           05 W-TIME     PIC X(5).                                      03280000
           05 FILLER     PIC X(4)   VALUE SPACES.                       03290000
           05 FILLER     PIC X(10)  VALUE 'SOCIETE : '.                 03300000
           05 W-SOCIETE  PIC X(3).                                      03310000
           05 FILLER     PIC X(8)  VALUE SPACES.                        03320001
           05 W-LIB-ETAT PIC X(17).                                     03330001
           05 FILLER     PIC X(30)   VALUE SPACES.                      03340000
           05 FILLER     PIC X(21)  VALUE 'DATE D''INVENTAIRE : '.      03350000
           05 W-INVENT   PIC X(8).                                      03360000
           05 FILLER     PIC X(2)   VALUE SPACES.                       03370000
                                                                        03380000
                                                                        03390000
       01  LIGNE-ETAT1.                                                 03400000
           05 FILLER     PIC X     VALUE SPACE.                         03410000
           05 FILLER     PIC X     VALUE '!'.                           03420000
           05 FILLER     PIC X(5)  VALUE SPACES.                        03430000
           05 FILLER     PIC X     VALUE '!'.                           03440000
           05 FILLER     PIC X(7)  VALUE SPACES.                        03450000
           05 FILLER     PIC X     VALUE '!'.                           03460000
           05 FILLER     PIC X(6)  VALUE SPACES.                        03470000
           05 FILLER     PIC X     VALUE '!'.                           03480000
           05 FILLER     PIC X(20) VALUE SPACES.                        03490000
           05 FILLER     PIC X     VALUE '!'.                           03500000
           05 FILLER     PIC X(7)  VALUE SPACES.                        03510000
           05 FILLER     PIC X     VALUE '!'.                           03520000
           05 FILLER     PIC X(13)  VALUE '  QUANTITE   '.              03530001
           05 FILLER     PIC X     VALUE '!'.                           03540000
           05 FILLER     PIC X(7)  VALUE ' ECART '.                     03550000
           05 FILLER     PIC X     VALUE '!'.                           03560000
           05 FILLER     PIC X(09) VALUE '   PRMP  '.                   03570000
           05 FILLER     PIC X     VALUE '!'.                           03580000
           05 FILLER     PIC X(23) VALUE '        VALEUR  '.            03590000
           05 FILLER     PIC X     VALUE '!'.                           03600000
           05 FILLER     PIC X(24) VALUE '    ECART VALORISE     '.     03610000
           05 FILLER     PIC X     VALUE '!'.                           03620000
                                                                        03630000
       01  LIGNE-ETAT2.                                                 03640000
           05 FILLER     PIC X     VALUE SPACE.                         03650000
           05 FILLER     PIC X     VALUE '!'.                           03660000
           05 FILLER     PIC X(5)  VALUE 'RAYON'.                       03670000
           05 FILLER     PIC X     VALUE '!'.                           03680000
           05 FILLER     PIC X(7)  VALUE 'FAMILLE'.                     03690000
           05 FILLER     PIC X     VALUE '!'.                           03700000
           05 FILLER     PIC X(6)  VALUE 'MARQUE'.                      03710000
           05 FILLER     PIC X     VALUE '!'.                           03720000
           05 FILLER     PIC X(20) VALUE '     REFERENCE      '.        03730000
           05 FILLER     PIC X     VALUE '!'.                           03740000
           05 FILLER     PIC X(7)  VALUE ' CODIC '.                     03750000
           05 FILLER     PIC X     VALUE '!'.                           03760000
           05 FILLER     PIC X(6)  VALUE 'THEOR.'.                      03770000
           05 FILLER     PIC X     VALUE '!'.                           03780000
           05 FILLER     PIC X(6)  VALUE ' INV. '.                      03790000
           05 FILLER     PIC X     VALUE '!'.                           03800000
           05 FILLER     PIC X(7)  VALUE 'EN QTE '.                     03810000
           05 FILLER     PIC X     VALUE '!'.                           03820000
           05 FILLER     PIC X(09) VALUE '         '.                   03830000
           05 FILLER     PIC X     VALUE '!'.                           03840000
           05 FILLER     PIC X(11) VALUE '   THEOR.  '.                 03850000
           05 FILLER     PIC X     VALUE '!'.                           03860000
           05 FILLER     PIC X(11) VALUE '  INVENT.  '.                 03870000
           05 FILLER     PIC X     VALUE '!'.                           03880000
           05 FILLER     PIC X(11) VALUE '  POSITIF  '.                 03890000
           05 FILLER     PIC X     VALUE '!'.                           03900000
           05 FILLER     PIC X(12) VALUE '  NEGATIF  '.                 03910000
           05 FILLER     PIC X     VALUE '!'.                           03920000
                                                                        03930000
       01  LIGNE-ETAT3.                                                 03940000
           05 W-CAR-GRAS PIC X     VALUE SPACE.                         03950001
           05 FILLER     PIC X     VALUE '!'.                           03960000
           05 W-ZONE-AFF.                                               03970000
              10 W-RAYON    PIC X(5).                                   03980000
              10 W-FIL1     PIC X(2) VALUE '! '.                        03990000
              10 W-FAM      PIC X(5).                                   04000000
              10 W-FIL2     PIC X(2)  VALUE ' !'.                       04010000
              10 W-MARQUE   PIC X(5).                                   04020000
              10 W-FIL3     PIC X(2) VALUE ' !'.                        04030000
              10 W-REF      PIC X(20).                                  04040000
              10 W-REF-RDF REDEFINES W-REF.                             04050000
                 15  W-LIBELLE PIC X(7).                                04060000
                 15  W-BEAU    PIC X(3).                                04070000
                 15  W-FAMLIB  PIC X(10).                               04080000
           05 W-ZONE-AFF-RDF REDEFINES W-ZONE-AFF PIC X(41).            04090001
           05 FILLER     PIC X     VALUE '!'.                           04100000
           05 W-CODIC    PIC X(7).                                      04110000
           05 FILLER     PIC X     VALUE '!'.                           04120000
           05 W-QTETHEO  PIC ZZZZZ9       BLANK WHEN ZERO.              04130001
           05 W-QTETHEO-RDF REDEFINES W-QTETHEO  PIC -----9             04140001
                                          BLANK WHEN ZERO.              04150001
           05 FILLER     PIC X     VALUE '!'.                           04160000
           05 W-QTEINV   PIC ZZZZZ9       BLANK WHEN ZERO.              04170001
           05 W-QTEINV-RDF REDEFINES W-QTEINV  PIC -----9               04180001
                                          BLANK WHEN ZERO.              04190001
           05 FILLER     PIC X     VALUE '!'.                           04200000
           05 W-QTECART  PIC ------9      BLANK WHEN ZERO.              04210001
           05 FILLER     PIC X     VALUE '!'.                           04220000
           05 W-PRMP     PIC ZZZZZ9,99    BLANK WHEN ZERO.              04230001
           05 FILLER     PIC X     VALUE '!'.                           04240001
           05 W-VALTHEO  PIC ZZZZZZZ9,99  BLANK WHEN ZERO.              04250001
           05 W-VALTHEO-RDF REDEFINES   W-VALTHEO PIC -------9,99       04260001
                                        BLANK WHEN ZERO.                04270001
           05 TOT-VALTHEO-M   REDEFINES   W-VALTHEO-RDF PIC ----------9 04271004
                                        BLANK WHEN ZERO.                04272004
           05 TOT-VALTHEO-P   REDEFINES TOT-VALTHEO-M PIC ZZZZZZZZZZ9   04273004
                                        BLANK WHEN ZERO.                04274004
           05 W-FILLER   PIC X     VALUE '!'.                           04280001
           05 W-VALINV   PIC ZZZZZZZ9,99  BLANK WHEN ZERO.              04290001
           05 W-VALINV-RDF REDEFINES W-VALINV PIC -------9,99           04300001
                                          BLANK WHEN ZERO.              04310001
           05 TOT-VALINV-M   REDEFINES   W-VALINV-RDF PIC ----------9   04311004
                                        BLANK WHEN ZERO.                04312004
           05 TOT-VALINV-P   REDEFINES TOT-VALINV-M PIC ZZZZZZZZZZ9     04313004
                                        BLANK WHEN ZERO.                04314004
           05 FILLER     PIC X     VALUE '!'.                           04320000
           05 W-POSITIF  PIC ZZZZZZZ9,99  BLANK WHEN ZERO.              04330000
           05 FILLER     PIC X     VALUE '!'.                           04340000
           05 W-NEGATIF  PIC --------9,99 BLANK WHEN ZERO.              04350000
           05 FILLER     PIC X     VALUE '!'.                           04360000
                                                                        04370001
      ***************************************************************   04380001
      *         ETAT DES ECARTS NON IMPUTABLES                          04390001
      ***************************************************************   04400001
       01  ENTANO1.                                                     04410001
           05 FILLER     PIC X  VALUE SPACES.                           04420001
           05 FILLER     PIC X(7)  VALUE 'IIV142A'.                     04430001
           05 FILLER     PIC X(45) VALUE SPACES.                        04440001
           05 FILLER   PIC X(30) VALUE 'LISTE DES ECARTS NON APPLIQUES'.04450001
           05 FILLER     PIC X(37) VALUE SPACES.                        04460001
           05 FILLER     PIC X(7) VALUE 'PAGE : '.                      04470001
           05 A-PAGE     PIC ZZ9.                                       04480001
           05 FILLER     PIC X(3) VALUE SPACES.                         04490001
       01  ENTANO2.                                                     04500001
           05 FILLER     PIC X(58) VALUE SPACES.                        04510001
           05 FILLER     PIC X(15) VALUE ' INTEGRALEMENT '.             04520001
           05 FILLER     PIC X(58) VALUE SPACES.                        04530001
                                                                        04540001
       01  ENTANO3.                                                     04550001
           05 FILLER     PIC X      VALUE '-'.                          04560001
           05 FILLER     PIC X(7)   VALUE 'DATE : '.                    04570001
           05 A-DATE     PIC X(8).                                      04580001
           05 FILLER     PIC X(3)   VALUE SPACES.                       04590001
           05 FILLER     PIC X(8)   VALUE 'HEURE : '.                   04600001
           05 A-TIME     PIC X(5).                                      04610001
           05 FILLER     PIC X(4)   VALUE SPACES.                       04620001
           05 FILLER     PIC X(10)  VALUE 'SOCIETE : '.                 04630001
           05 A-SOCIETE  PIC X(3).                                      04640001
           05 FILLER     PIC X(8)  VALUE SPACES.                        04650001
           05 A-LIB-ETAT PIC X(17).                                     04660001
           05 FILLER     PIC X(30)   VALUE SPACES.                      04670001
           05 FILLER     PIC X(21)  VALUE 'DATE D''INVENTAIRE : '.      04680001
           05 A-INVENT   PIC X(8).                                      04690001
           05 FILLER     PIC X(2)   VALUE SPACES.                       04700001
       01  LIGNE-ANO-SEPAR.                                             04710001
           05 FILLER     PIC X     VALUE SPACE.                         04720001
           05 FILLER     PIC X(29) VALUE SPACES.                        04730001
           05 FILLER     PIC X     VALUE '!'.                           04740001
           05 A-TIR      PIC X(5)  VALUE SPACES.                        04750001
           05 FILLER     PIC X     VALUE '!'.                           04760001
           05 FILLER     PIC X(7)  VALUE ALL '-'.                       04770001
           05 FILLER     PIC X     VALUE '!'.                           04780001
           05 FILLER     PIC X(6)  VALUE ALL '-'.                       04790001
           05 FILLER     PIC X     VALUE '!'.                           04800001
           05 FILLER     PIC X(20) VALUE ALL '-'.                       04810001
           05 FILLER     PIC X     VALUE '!'.                           04820001
           05 FILLER     PIC X(7)  VALUE ALL '-'.                       04830001
           05 FILLER     PIC X     VALUE '!'.                           04840001
           05 FILLER     PIC X(13) VALUE ALL '-'.                       04850001
           05 FILLER     PIC X     VALUE '!'.                           04860001
           05 FILLER     PIC X(7)  VALUE ALL '-'.                       04870001
           05 FILLER     PIC X     VALUE '!'.                           04880001
           05 FILLER     PIC X(09) VALUE ALL '-'.                       04890001
           05 FILLER     PIC X     VALUE '!'.                           04900001
           05 FILLER     PIC X(14) VALUE ALL '-'.                       04910001
           05 FILLER     PIC X     VALUE '!'.                           04920001
           05 FILLER     PIC X(05) VALUE SPACES.                        04930001
                                                                        04940001
       01  LIGNE-ANO1.                                                  04950001
           05 FILLER     PIC X     VALUE SPACE.                         04960001
           05 FILLER     PIC X(29) VALUE SPACES.                        04970001
           05 FILLER     PIC X     VALUE '!'.                           04980001
           05 FILLER     PIC X(5)  VALUE SPACES.                        04990001
           05 FILLER     PIC X     VALUE '!'.                           05000001
           05 FILLER     PIC X(7)  VALUE SPACES.                        05010001
           05 FILLER     PIC X     VALUE '!'.                           05020001
           05 FILLER     PIC X(6)  VALUE SPACES.                        05030001
           05 FILLER     PIC X     VALUE '!'.                           05040001
           05 FILLER     PIC X(20) VALUE SPACES.                        05050001
           05 FILLER     PIC X     VALUE '!'.                           05060001
           05 FILLER     PIC X(7)  VALUE SPACES.                        05070001
           05 FILLER     PIC X     VALUE '!'.                           05080001
           05 FILLER     PIC X(13)  VALUE ' STK FIN PER.'.              05090001
           05 FILLER     PIC X     VALUE '!'.                           05100001
           05 FILLER     PIC X(7)  VALUE ' ECART '.                     05110001
           05 FILLER     PIC X     VALUE '!'.                           05120001
           05 FILLER     PIC X(09) VALUE '   PRMP  '.                   05130001
           05 FILLER     PIC X     VALUE '!'.                           05140001
           05 FILLER     PIC X(14) VALUE ' VALEUR ECART '.              05150001
           05 FILLER     PIC X     VALUE '!'.                           05160001
           05 FILLER     PIC X(05) VALUE SPACES.                        05170001
                                                                        05180001
       01  LIGNE-ANO2.                                                  05190001
           05 FILLER     PIC X     VALUE SPACE.                         05200001
           05 FILLER     PIC X(29) VALUE SPACES.                        05210001
           05 FILLER     PIC X     VALUE '!'.                           05220001
           05 FILLER     PIC X(5)  VALUE 'RAYON'.                       05230001
           05 FILLER     PIC X     VALUE '!'.                           05240001
           05 FILLER     PIC X(7)  VALUE 'FAMILLE'.                     05250001
           05 FILLER     PIC X     VALUE '!'.                           05260001
           05 FILLER     PIC X(6)  VALUE 'MARQUE'.                      05270001
           05 FILLER     PIC X     VALUE '!'.                           05280001
           05 FILLER     PIC X(20) VALUE '     REFERENCE      '.        05290001
           05 FILLER     PIC X     VALUE '!'.                           05300001
           05 FILLER     PIC X(7)  VALUE ' CODIC '.                     05310001
           05 FILLER     PIC X     VALUE '!'.                           05320001
           05 FILLER     PIC X(6)  VALUE ' AVANT'.                      05330001
           05 FILLER     PIC X     VALUE '!'.                           05340001
           05 FILLER     PIC X(6)  VALUE 'APRES '.                      05350001
           05 FILLER     PIC X     VALUE '!'.                           05360001
           05 FILLER     PIC X(7)  VALUE '  INV. '.                     05370001
           05 FILLER     PIC X     VALUE '!'.                           05380001
           05 FILLER     PIC X(09) VALUE SPACES.                        05390001
           05 FILLER     PIC X     VALUE '!'.                           05400001
           05 FILLER     PIC X(14) VALUE ' NON APPLIQUE '.              05410001
           05 FILLER     PIC X     VALUE '!'.                           05420001
           05 FILLER     PIC X(05) VALUE SPACES.                        05430001
      *                                                                 05440001
       01  TIRET-ANO.                                                   05450001
           05 W-REECRIT-ANO PIC X VALUE SPACES.                         05460001
           05 FILLER        PIC X(29) VALUE SPACES.                     05470001
           05 FILLER        PIC X(98)  VALUE ALL '-'.                   05480001
           05 FILLER        PIC X(05) VALUE SPACES.                     05490001
       01  LIGNE-ANO3.                                                  05500001
           05 A-CAR-GRAS PIC X     VALUE SPACE.                         05510001
           05 FILLER     PIC X(29) VALUE SPACES.                        05520001
           05 FILLER     PIC X     VALUE '!'.                           05530001
           05 A-ZONE-AFF.                                               05540001
              10 A-RAYON    PIC X(5).                                   05550001
              10 A-FIL1     PIC X(2) VALUE '! '.                        05560001
              10 A-FAM      PIC X(5).                                   05570001
              10 A-FIL2     PIC X(2)  VALUE ' !'.                       05580001
              10 A-MARQUE   PIC X(5).                                   05590001
              10 A-FIL3     PIC X(2) VALUE ' !'.                        05600001
              10 A-REF      PIC X(20).                                  05610001
              10 A-REF-RDF REDEFINES A-REF.                             05620001
                 15  A-LIBELLE PIC X(7).                                05630001
                 15  A-BEAU    PIC X(3).                                05640001
                 15  A-FAMLIB  PIC X(10).                               05650001
           05 A-ZONE-AFF-RDF REDEFINES A-ZONE-AFF PIC X(41).            05660001
           05 FILLER     PIC X     VALUE '!'.                           05670001
           05 A-CODIC    PIC X(7).                                      05680001
           05 FILLER     PIC X     VALUE '!'.                           05690001
           05 A-QTETHEO  PIC ZZZZZ9       BLANK WHEN ZERO.              05700001
           05 A-QTETHEO-RDF REDEFINES A-QTETHEO  PIC -----9             05710001
                                          BLANK WHEN ZERO.              05720001
           05 FILLER     PIC X     VALUE '!'.                           05730001
           05 A-QTEINV   PIC ZZZZZ9       BLANK WHEN ZERO.              05740001
           05 A-QTEINV-RDF REDEFINES A-QTEINV  PIC -----9               05750001
                                          BLANK WHEN ZERO.              05760001
           05 FILLER     PIC X     VALUE '!'.                           05770001
           05 A-QTECART  PIC ------9      BLANK WHEN ZERO.              05780001
           05 FILLER     PIC X     VALUE '!'.                           05790001
           05 A-PRMP     PIC ZZZZZ9,99    BLANK WHEN ZERO.              05800001
           05 FILLER     PIC X     VALUE '!'.                           05810001
           05 A-VALECART PIC ----------9,99  BLANK WHEN ZERO.           05820001
           05 FILLER     PIC X     VALUE '!'.                           05830001
           05 FILLER     PIC X(05) VALUE SPACES.                        05840001
      *---------------------------------------------------------------* 05850001
      *     ECRITURE LIGNE TOTAL LIEU SUR UNE PAGE SEPAREE            * 05860001
      *---------------------------------------------------------------* 05870001
       01  LIGNE-TOTAL1.                                                05880001
           05 FILLER        PIC X     VALUE SPACE.                      05890001
           05 FILLER        PIC X     VALUE '!'.                        05900001
           05 FILLER        PIC X(21).                                  05910001
           05 FILLER        PIC X     VALUE '!'.                        05920001
           05 FILLER        PIC X(23) VALUE '       QUANTITE        '.  05930001
           05 FILLER        PIC X     VALUE '!'.                        05940001
           05 FILLER        PIC X(11) VALUE '   ECART   '.              05950001
           05 FILLER        PIC X     VALUE '!'.                        05960001
           05 FILLER        PIC X(17) VALUE SPACES.                     05970001
           05 FILLER        PIC X(6)  VALUE 'VALEUR'.                   05980001
           05 FILLER        PIC X(16) VALUE SPACES.                     05990001
           05 FILLER        PIC X     VALUE '!'.                        06000001
           05 FILLER        PIC X(9)  VALUE SPACES.                     06010001
           05 FILLER        PIC X(14) VALUE 'ECART VALORISE'.           06020001
           05 FILLER        PIC X(9)  VALUE SPACES.                     06030001
           05 FILLER        PIC X     VALUE '!'.                        06040001
       01  LIGNE-TOTAL2.                                                06050001
           05 FILLER        PIC X     VALUE SPACE.                      06060001
           05 FILLER        PIC X     VALUE '!'.                        06070001
           05 FILLER        PIC X(21).                                  06080001
           05 FILLER        PIC X     VALUE '!'.                        06090001
           05 FILLER        PIC X(11) VALUE ' THEORIQUE '.              06100001
           05 FILLER        PIC X  VALUE '!'.                           06110001
           05 FILLER        PIC X(11) VALUE ' INVENTAIRE'.              06120001
           05 FILLER        PIC X     VALUE '!'.                        06130001
           05 FILLER        PIC X(11) VALUE '   EN QTE  '.              06140001
           05 FILLER        PIC X     VALUE '!'.                        06150001
           05 FILLER        PIC X(19) VALUE '     THEORIQUE     '.      06160001
           05 FILLER        PIC X     VALUE '!'.                        06170001
           05 FILLER        PIC X(19) VALUE '     INVENTAIRE    '.      06180001
           05 FILLER        PIC X     VALUE '!'.                        06190001
           05 FILLER        PIC X(15) VALUE '    POSITIF    '.          06200001
           05 FILLER        PIC X     VALUE '!'.                        06210001
           05 FILLER        PIC X(15) VALUE '    NEGATIF    '.          06220001
           05 FILLER        PIC X     VALUE '!'.                        06230001
       01  LIGNE-TOTAL3.                                                06240001
           05 W-GRAS-TOTAL  PIC X     VALUE SPACE.                      06250001
           05 FILLER        PIC X     VALUE '!'.                        06260001
           05 W-ZONE-TOT    PIC X(21).                                  06270001
           05 FILLER        PIC X     VALUE '!'.                        06280001
           05 W-QTETHEOTOT  PIC  ---------9  BLANK WHEN ZERO.           06290001
           05 FILLER        PIC X VALUE SPACES.                         06300001
           05 FILLER        PIC X  VALUE '!'.                           06310001
           05 W-QTEINVTOT   PIC ---------9   BLANK WHEN ZERO.           06320001
           05 FILLER        PIC X VALUE SPACES.                         06330001
           05 FILLER        PIC X     VALUE '!'.                        06340001
           05 W-QTECARTTOT  PIC ---------9      BLANK WHEN ZERO.        06350001
           05 FILLER        PIC X VALUE SPACES.                         06360001
           05 FILLER        PIC X     VALUE '!'.                        06370001
           05 W-VALTHEOTOT  PIC --------------9,99                      06380001
                                             BLANK WHEN ZERO.           06390001
           05 FILLER        PIC X VALUE SPACES.                         06400001
           05 FILLER        PIC X     VALUE '!'.                        06410001
           05 W-VALINVTOT   PIC --------------9,99                      06420001
                                             BLANK WHEN ZERO.           06430001
           05 FILLER        PIC X VALUE SPACES.                         06440001
           05 FILLER        PIC X     VALUE '!'.                        06450001
           05 W-POSITIFTOT  PIC ZZZZZZZZZZ9,99                          06460001
                                             BLANK WHEN ZERO.           06470001
           05 FILLER        PIC X VALUE SPACES.                         06480001
           05 FILLER        PIC X     VALUE '!'.                        06490001
           05 W-NEGATIFTOT  PIC -----------9,99 BLANK WHEN ZERO.        06500001
           05 FILLER        PIC X  VALUE SPACES.                        06510001
           05 FILLER        PIC X     VALUE '!'.                        06520001
                                                                        06530001
      * ECRITURE TOTALISATION ANOMALIES                                 06540001
       01  LIGNE-ANOTOTAL1.                                             06550001
           05 FILLER        PIC X     VALUE SPACE.                      06560001
           05 FILLER        PIC X(29) VALUE SPACE.                      06570001
           05 FILLER        PIC X     VALUE '!'.                        06580001
           05 FILLER        PIC X(21) VALUE SPACES.                     06590001
           05 FILLER        PIC X     VALUE '!'.                        06600001
           05 FILLER        PIC X(23) VALUE ' QUANTITE FIN PERIODE  '.  06610001
           05 FILLER        PIC X     VALUE '!'.                        06620001
           05 FILLER        PIC X(11) VALUE ' ECART QTE '.              06630001
           05 FILLER        PIC X     VALUE '!'.                        06640001
           05 FILLER        PIC X(19) VALUE ' VALORISATION ECART'.      06650001
           05 FILLER        PIC X     VALUE '!'.                        06660001
           05 FILLER        PIC X     VALUE SPACES.                     06670001
                                                                        06680001
       01  LIGNE-ANOTOTAL2.                                             06690001
           05 FILLER        PIC X     VALUE SPACE.                      06700001
           05 FILLER        PIC X(29) VALUE SPACE.                      06710001
           05 FILLER        PIC X     VALUE '!'.                        06720001
           05 FILLER        PIC X(21) VALUE SPACES.                     06730001
           05 FILLER        PIC X     VALUE '!'.                        06740001
           05 FILLER        PIC X(11) VALUE '   AVANT   '.              06750001
           05 FILLER        PIC X     VALUE '!'.                        06760001
           05 FILLER        PIC X(11) VALUE '   APRES   '.              06770001
           05 FILLER        PIC X     VALUE '!'.                        06780001
           05 FILLER        PIC X(11) VALUE ' INVENTAIRE'.              06790001
           05 FILLER        PIC X     VALUE '!'.                        06800001
           05 FILLER        PIC X(19) VALUE '   NON APPLIQUE    '.      06810001
           05 FILLER        PIC X     VALUE '!'.                        06820001
           05 FILLER        PIC X     VALUE SPACES.                     06830001
                                                                        06840001
       01  LIGNE-ANOTOTAL3.                                             06850001
           05 A-GRAS-TOTAL  PIC X     VALUE SPACE.                      06860001
           05 FILLER        PIC X(29) VALUE SPACE.                      06870001
           05 FILLER        PIC X     VALUE '!'.                        06880001
           05 A-ZONE-TOT    PIC X(21).                                  06890001
           05 FILLER        PIC X     VALUE '!'.                        06900001
           05 A-QTETHEOTOT  PIC  ---------9  BLANK WHEN ZERO.           06910001
           05 FILLER        PIC X VALUE SPACES.                         06920001
           05 FILLER        PIC X  VALUE '!'.                           06930001
           05 A-QTEINVTOT   PIC ---------9   BLANK WHEN ZERO.           06940001
           05 FILLER        PIC X VALUE SPACES.                         06950001
           05 FILLER        PIC X     VALUE '!'.                        06960001
           05 A-QTECARTTOT  PIC ---------9      BLANK WHEN ZERO.        06970001
           05 FILLER        PIC X VALUE SPACES.                         06980001
           05 FILLER        PIC X     VALUE '!'.                        06990001
           05 A-VALECARTTOT PIC --------------9,99                      07000001
                                             BLANK WHEN ZERO.           07010001
           05 FILLER        PIC X VALUE SPACES.                         07020001
           05 FILLER        PIC X     VALUE '!'.                        07030001
           05 FILLER        PIC X  VALUE SPACES.                        07040001
                                                                        07050001
       01  SAUT-TOTAL-ANO.                                              07060001
           05 FILLER        PIC X     VALUE SPACE.                      07070001
           05 FILLER        PIC X(29) VALUE SPACE.                      07080001
           05 FILLER        PIC X     VALUE '!'.                        07090001
           05 FILLER        PIC X(21) VALUE SPACES.                     07100001
           05 FILLER        PIC X     VALUE '!'.                        07110001
           05 FILLER        PIC X(11) VALUE SPACES.                     07120001
           05 FILLER        PIC X     VALUE '!'.                        07130001
           05 FILLER        PIC X(11) VALUE SPACES.                     07140001
           05 FILLER        PIC X     VALUE '!'.                        07150001
           05 FILLER        PIC X(11) VALUE SPACES.                     07160001
           05 FILLER        PIC X     VALUE '!'.                        07170001
           05 FILLER        PIC X(19) VALUE SPACES.                     07180001
           05 FILLER        PIC X     VALUE '!'.                        07190001
           05 FILLER        PIC X     VALUE SPACES.                     07200001
                                                                        07210001
       01  TIRET-ANO-TOTAL.                                             07220001
           05 FILLER        PIC X     VALUE SPACE.                      07230001
           05 FILLER        PIC X(29) VALUE SPACE.                      07240001
           05 FILLER        PIC X     VALUE '!'.                        07250001
           05 FILLER        PIC X(21) VALUE ALL '-'.                    07260001
           05 FILLER        PIC X     VALUE '!'.                        07270001
           05 FILLER        PIC X(11) VALUE ALL '-'.                    07280001
           05 FILLER        PIC X     VALUE '!'.                        07290001
           05 FILLER        PIC X(11) VALUE ALL '-'.                    07300001
           05 FILLER        PIC X     VALUE '!'.                        07310001
           05 FILLER        PIC X(11) VALUE ALL '-'.                    07320001
           05 FILLER        PIC X     VALUE '!'.                        07330001
           05 FILLER        PIC X(19) VALUE ALL '-'.                    07340001
           05 FILLER        PIC X     VALUE '!'.                        07350001
           05 FILLER        PIC X     VALUE SPACES.                     07360001
                                                                        07370001
       01  SAUT-TOTAL.                                                  07380001
           05 FILLER        PIC X     VALUE SPACE.                      07390001
           05 FILLER        PIC X     VALUE '!'.                        07400001
           05 FILLER        PIC X(21).                                  07410001
           05 FILLER        PIC X     VALUE '!'.                        07420001
           05 FILLER        PIC X(11).                                  07430001
           05 FILLER        PIC X     VALUE '!'.                        07440001
           05 FILLER        PIC X(11).                                  07450001
           05 FILLER        PIC X     VALUE '!'.                        07460001
           05 FILLER        PIC X(11).                                  07470001
           05 FILLER        PIC X     VALUE '!'.                        07480001
           05 FILLER        PIC X(19).                                  07490001
           05 FILLER        PIC X     VALUE '!'.                        07500001
           05 FILLER        PIC X(19).                                  07510001
           05 FILLER        PIC X     VALUE '!'.                        07520001
           05 FILLER        PIC X(15).                                  07530001
           05 FILLER        PIC X     VALUE '!'.                        07540001
           05 FILLER        PIC X(16).                                  07550001
           05 FILLER        PIC X     VALUE '!'.                        07560001
           EJECT                                                        07570001
      *                                                                 07580000
      * ============================================================= * 07590000
      *        D E S C R I P T I O N      D E S      Z O N E S        * 07600000
      *           D ' A P P E L    M O D U L E    A B E N D           * 07610000
      * ============================================================= * 07620000
      *                                                                 07630000
           COPY  ABENDCOP.                                              07640000
      *                                                                 07650000
      *                                                                 07660000
      * ============================================================= * 07670000
      *     D E S C R I P T I O N    R E C O R D    B I V 1 0 5       * 07680000
      * ============================================================= * 07690000
      *                                                                 07700000
      *                                                                 07710000
           EJECT                                                        07720000
      *                                                                 07730000
       PROCEDURE DIVISION.                                              07740000
      *                                                                 07750000
      * ============================================================= * 07760000
      *              T R A M E   DU   P R O G R A M M E               * 07770000
      * ============================================================= * 07780000
      *                                                               * 07790000
      * ============================================================= * 07800000
      *                                                               * 07810000
      *           ------------------------------------------          * 07820000
      *           --  MODULE DE BASE DU PROGRAMME  BIV142 --          * 07830001
      *           ------------------------------------------          * 07840000
      *                               I                               * 07850000
      *           -----------------------------------------           * 07860000
      *           I                   I                   I           * 07870000
      *   -----------------   -----------------   -----------------   * 07880000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 07890000
      *   -----------------   -----------------   -----------------   * 07900000
      *                                                               * 07910000
      * ============================================================= * 07920000
      *                                                                 07930000
      *                                                                 07940000
           PERFORM  DEBUT-BIV142.                                       07950001
           PERFORM  FIRST-LECT-FIV142.                                  07960001
           PERFORM  TRAIT-EDITION-ECART UNTIL FIN-FICHIER = '1'.        07970000
           PERFORM  FIN-BIV142.                                         07980001
      *                                                                 07990000
           EJECT                                                        08000000
      *                                                                 08010000
      * ============================================================= * 08020000
      *                  D E B U T        B I N 0 0 0                 * 08030000
      * ============================================================= * 08040000
      *                                                                 08050000
       DEBUT-BIV142      SECTION.                                       08060001
                                                                        08070000
      *                                                                 08080000
           PERFORM  OUVERTURE-FICHIERS.                                 08090000
      * ON INITIALISE LA DATE DU JOUR                                   08100000
           PERFORM INIT-DAT.                                            08110000
           PERFORM INIT-FPARAM.                                         08120001
           PERFORM INIT-HEURE.                                          08130000
           MOVE SPACES   TO PASSAGE-ENTETE-ANO                          08140001
                            PASSAGE-ENTETE.                             08150001
           MOVE 0    TO W-FIV142-LUES .                                 08160001
           MOVE 0    TO W-IIV142-ECRITS.                                08170001
           MOVE 0    TO W-IIV142A-ECRITS.                               08180001
           MOVE 0    TO CPT-PAGE.                                       08190000
           MOVE 0    TO CPT-PAGE-ANO.                                   08200001
           MOVE   'N'                TO W-RUPT-FAM W-RUPT-FAMANO.       08210001
           MOVE   'N'                TO W-RUPT-RAY.                     08220000
           INITIALIZE ZONE-CUM-FAM.                                     08230000
           INITIALIZE ZONE-CUM-RAY.                                     08240000
           INITIALIZE ZONE-CUM-MAG.                                     08250000
           INITIALIZE ZONE-ANOCUM-FAM.                                  08260001
           INITIALIZE ZONE-ANOCUM-RAY.                                  08270001
           INITIALIZE ZONE-ANOCUM-MAG.                                  08280001
           INITIALIZE ZONE-CUM-NCOD.                                    08290001
           MOVE SPACES  TO Z-FAM.                                       08300000
           MOVE SPACES  TO Z-RAYON.                                     08310000
           MOVE SPACES  TO Z-MAG.                                       08320000
           MOVE SPACES  TO Z-NCODIC.                                    08330001
           MOVE  0      TO Z-PRMP.                                      08340001
           MOVE SPACES  TO Z-MARQUE.                                    08350001
           MOVE SPACES  TO Z-REF.                                       08360001
      *                                                                 08370000
       FIN-DEBUT-BIV142.  EXIT.                                         08380001
           EJECT                                                        08390000
                                                                        08400000
      * ============================================================= * 08410000
      *          M O D U L E S    D E B U T    B I N 0 0 0            * 08420000
      * ============================================================= * 08430000
      *                                                                 08440000
      *====>  TRAITEMENT  POUR RECUPERER LA DATE DU JOUR                08450000
      *                                                                 08460000
       INIT-DAT                SECTION.                                 08470000
                  DISPLAY 'INIT-DAT    SECTION'.                        08480000
           OPEN  INPUT   FDATE                                          08490000
           READ FDATE AT END                                            08500000
             MOVE  '**  PAS  DE  DATE  EN  ENTREE  **'                  08510000
                                           TO  ABEND-MESS               08520000
             PERFORM  FIN-ANORMALE                                      08530000
           END-READ.                                                    08540000
           MOVE SS TO DATE-SS EDT-SS.                                   08550000
           MOVE AA TO DATE-AN EDT-AA FORMAT-AA.                         08560000
           MOVE MM TO DATE-MO EDT-MM FORMAT-MM.                         08570000
           MOVE JJ TO DATE-JO EDT-JJ FORMAT-JJ.                         08580000
           MOVE W-FORMAT-DATE TO W-DATE A-DATE.                         08590001
           CLOSE  FDATE .                                               08600000
       FIN-INIT-DAT.      EXIT.                                         08610000
                                                                        08620000
      *====>  TRAITEMENT  POUR RECUPERER LE PARAMETRE PERMETTANT DE     08630001
      *====>  SAVOIR S'IL S'AGIT D'ETAT AU NIVEAU TOUS MAGASINS         08640001
      *====>                     OU SOCIETE                             08650001
      *                                                                 08660001
       INIT-FPARAM             SECTION.                                 08670001
                  DISPLAY 'INIT-FPARAM  SECTION'.                       08680001
           OPEN  INPUT   FPARAM                                         08690001
           READ FPARAM AT END                                           08700001
             MOVE  '**  PAS  DE  PARAMETRE  EN  ENTREE  **'             08710001
                                           TO  ABEND-MESS               08720001
             PERFORM  FIN-ANORMALE                                      08730001
           END-READ.                                                    08740001
           MOVE PARAM TO W-PARAM.                                       08750001
           CLOSE  FPARAM.                                               08760001
           IF W-PARAM = 'SOC'                                           08770001
              MOVE '   - SOCIETE -   ' TO W-LIB-ETAT                    08780001
              MOVE 'TOTAL  SOCIETE'    TO W-INTITULE                    08790001
           ELSE                                                         08800001
              IF W-PARAM = 'MAG'                                        08810001
                 MOVE '- TOUS MAGASINS -' TO W-LIB-ETAT                 08820001
                 MOVE 'TOTAL  TOUS  MAGASINS'    TO W-INTITULE          08830001
              END-IF                                                    08840001
           END-IF.                                                      08850001
       FIN-INIT-FPARAM.      EXIT.                                      08860001
       INIT-HEURE     SECTION.                                          08870000
           ACCEPT Z-HOUR FROM TIME.                                     08880000
           MOVE HOUR-HH TO HEURE-HH.                                    08890000
           MOVE HOUR-MM TO HEURE-MM.                                    08900000
           MOVE W-HEURE TO W-TIME A-TIME.                               08910001
                                                                        08920000
       FIN-INIT-HEURE. EXIT.                                            08930000
      *                                                                 08940000
      *====>  O U V E R T U R E  -  F I C H I E R S .                   08950000
      *                                                                 08960000
       OUVERTURE-FICHIERS     SECTION.                                  08970000
                                                                        08980000
           OPEN  INPUT   FIV142.                                        08990001
           OPEN  OUTPUT  IIV142                                         09000001
                 OUTPUT  IIV142A.                                       09010001
                                                                        09020001
       FIN-OUVERTURE-FICHIERS.     EXIT.                                09030000
      *                                                                 09040000
      *                                                                 09050000
      *                                                                 09060000
      * ============================================================= * 09070000
      *          M O D U L E S    T R A I T    B I V 1 0 5            * 09080000
      * ============================================================= * 09090000
                                                                        09100000
       FIRST-LECT-FIV142       SECTION.                                 09110001
           READ FIV142 AT END                                           09120001
                   MOVE '1'   TO FIN-FICHIER                            09130000
           END-READ.                                                    09140000
                                                                        09150000
           IF FIN-FICHIER NOT = '1'                                     09160000
              MOVE   F130-NSOCMAG       TO Z-SOC                        09170001
              MOVE   F130-RAYON         TO Z-RAYON                      09180001
              MOVE   F130-CFAM          TO Z-FAM                        09190000
              MOVE   F130-NCODIC        TO Z-NCODIC                     09200001
              MOVE   F130-LREFFOURN     TO Z-REF                        09210001
              MOVE   F130-CMARQ         TO Z-MARQUE                     09220001
              MOVE   F130-PRMP          TO Z-PRMP                       09230001
              ADD   1                     TO W-FIV142-LUES              09240001
              PERFORM ECRITURE-ENTETE                                   09250000
           END-IF.                                                      09260000
                                                                        09270000
       FIN-FIRST-LECT-FIV142. EXIT.                                     09280001
            EJECT                                                       09290000
      *----------------------------------------------------------------*09300000
      *   TRAITEMENT GENERAL DE L'EDITION DES ETIQUETTES               *09310000
      *----------------------------------------------------------------*09320000
                                                                        09330000
       TRAIT-EDITION-ECART     SECTION.                                 09340000
                                                                        09350000
           IF F130-NSOCMAG NOT = Z-SOC                                  09360001
              PERFORM TRAIT-RUPTURE-RAYON                               09370001
              PERFORM TRAIT-RUPTURE-MAG                                 09380001
              PERFORM RECUP-DERNIERES-ZONES                             09390001
              WRITE IIV142-RECORD       FROM SAUT-PAGE                  09400001
              PERFORM ECRITURE-ENTETE                                   09410001
           ELSE                                                         09420001
              IF F130-RAYON  NOT = Z-RAYON                              09430001
                 PERFORM TRAIT-RUPTURE-RAYON                            09440001
                 PERFORM RECUP-DERNIERES-ZONES                          09450001
                 WRITE IIV142-RECORD       FROM SAUT-PAGE               09460001
                 PERFORM ECRITURE-ENTETE                                09470001
              ELSE                                                      09480001
                 IF F130-CFAM  NOT = Z-FAM                              09490001
                    PERFORM TRAIT-RUPTURE-FAMILLE                       09500001
                    PERFORM RECUP-DERNIERES-ZONES                       09510001
                    PERFORM TEST-SAUT-PAGE                              09520001
                 ELSE                                                   09530001
                    IF F130-NCODIC NOT = Z-NCODIC                       09540001
                       PERFORM TRAIT-RUPTURE-NCODIC                     09550001
                       PERFORM RECUP-DERNIERES-ZONES                    09560001
                       PERFORM TEST-SAUT-PAGE                           09570001
                    ELSE                                                09580001
                       PERFORM PAS-DE-RUPTURE                           09590001
                       PERFORM LECT-SUIV-FIV142                         09600001
                       PERFORM TEST-SAUT-PAGE                           09610001
                    END-IF                                              09620001
                 END-IF                                                 09630001
              END-IF                                                    09640001
           END-IF.                                                      09650000
                                                                        09660000
       FIN-TRAIT-EDITION-ECART.  EXIT.                                  09670000
          EJECT                                                         09680000
       TEST-SAUT-PAGE           SECTION.                                09690000
                                                                        09700000
           IF CPT-LIGNE > 50                                            09710000
              IF F130-CFAM  NOT = Z-FAM OR FIN-FICHIER = '1'            09720001
                 GO TO FIN-TEST-SAUT-PAGE                               09730000
              END-IF                                                    09740000
              IF W-RUPT-FAM = 'O' OR  W-RUPT-RAY = 'O'                  09750000
                 MOVE '+'  TO W-REECRIT                                 09760001
              ELSE                                                      09770000
                 MOVE SPACES  TO W-REECRIT                              09780001
              END-IF                                                    09790000
              WRITE IIV142-RECORD        FROM ETAT-TIRET                09800001
              MOVE SPACES  TO W-REECRIT                                 09810001
              WRITE IIV142-RECORD        FROM SAUT-PAGE                 09820001
              PERFORM ECRITURE-ENTETE                                   09830000
           END-IF.                                                      09840000
                                                                        09850000
       FIN-TEST-SAUT-PAGE. EXIT.                                        09860000
          EJECT                                                         09870000
                                                                        09880000
       PAS-DE-RUPTURE     SECTION.                                      09890000
           MOVE 'N'      TO PASSAGE-ENTETE.                             09900000
           MOVE F130-QSTOCK           TO T-QTETHEO.                     09910001
           MOVE F130-QSTOCKEC1        TO T-QTECART.                     09920001
           ADD  T-QTETHEO    T-QTECART GIVING T-QTEINV.                 09930000
           MOVE F130-PRMP            TO T-PRMP.                         09940001
           ADD  T-QTETHEO            TO W-CUM-THEONCOD.                 09950001
           ADD  T-QTEINV             TO W-CUM-INVNCOD.                  09960001
           ADD  T-QTECART            TO W-CUM-ECARTNCOD.                09970001
                                                                        09980000
           MOVE   'N'                 TO W-RUPT-FAM W-RUPT-RAY.         09990001
                                                                        10000000
       FIN-PAS-DE-RUPTURE. EXIT.                                        10010000
          EJECT                                                         10020000
                                                                        10030000
       TRAIT-RUPTURE-NCODIC     SECTION.                                10040001
           MOVE 'N'      TO PASSAGE-ENTETE.                             10050001
           MOVE Z-MARQUE              TO W-MARQUE.                      10060001
           MOVE Z-REF                 TO W-REF.                         10070001
           MOVE Z-NCODIC              TO W-CODIC.                       10080001
           MOVE Z-PRMP               TO W-PRMP T-PRMP.                  10090001
           IF W-CUM-THEONCOD NOT < 0                                    10100001
              MOVE   W-CUM-THEONCOD     TO W-QTETHEO                    10110001
           ELSE                                                         10120001
              MOVE   W-CUM-THEONCOD     TO W-QTETHEO-RDF                10130001
           END-IF.                                                      10140001
           ADD    W-CUM-THEONCOD     TO W-CUM-THEOFAM.                  10150001
           IF W-CUM-INVNCOD  NOT < 0                                    10160001
              MOVE   W-CUM-INVNCOD      TO W-QTEINV                     10170001
           ELSE                                                         10180001
              PERFORM ECART-NON-IMPUTABLE                               10190001
           END-IF.                                                      10200001
           ADD    W-CUM-INVNCOD      TO W-CUM-INVFAM.                   10210001
           MOVE   W-CUM-ECARTNCOD    TO W-QTECART.                      10220001
           ADD    W-CUM-ECARTNCOD    TO W-CUM-ECARTFAM.                 10230001
           COMPUTE W-CUM-VALTHEONCOD = Z-PRMP * W-CUM-THEONCOD          10240001
           COMPUTE W-CUM-VALORISENCOD = Z-PRMP * W-CUM-ECARTNCOD        10250001
           COMPUTE W-CUM-VALINVNCOD  = Z-PRMP * W-CUM-INVNCOD           10260001
           IF W-CUM-VALTHEONCOD  NOT < 0                                10270001
              MOVE   W-CUM-VALTHEONCOD  TO W-VALTHEO                    10280001
           ELSE                                                         10290001
              MOVE   W-CUM-VALTHEONCOD  TO W-VALTHEO-RDF                10300001
           END-IF.                                                      10310001
           ADD    W-CUM-VALTHEONCOD  TO W-CUM-VALTHEOFAM.               10320001
           IF W-CUM-VALINVNCOD NOT < 0                                  10330001
              MOVE   W-CUM-VALINVNCOD   TO W-VALINV                     10340001
           ELSE                                                         10350001
              MOVE   W-CUM-VALINVNCOD   TO W-VALINV-RDF                 10360001
           END-IF                                                       10370001
           ADD    W-CUM-VALINVNCOD   TO W-CUM-VALINVFAM.                10380001
           ADD    W-CUM-VALORISENCOD TO W-CUM-VALORISEFAM.              10390001
           IF  W-CUM-VALORISENCOD < 0                                   10400001
               MOVE W-CUM-VALORISENCOD TO W-NEGATIF                     10410001
           ELSE                                                         10420001
               MOVE W-CUM-VALORISENCOD TO W-POSITIF                     10430001
           END-IF.                                                      10440001
           INITIALIZE ZONE-CUM-NCOD.                                    10450001
           WRITE IIV142-RECORD       FROM LIGNE-ETAT3.                  10460001
           MOVE 0                     TO W-POSITIF W-NEGATIF.           10470001
           ADD  1                    TO CPT-LIGNE.                      10480001
           ADD 1                      TO   W-IIV142-ECRITS.             10490001
           MOVE  SPACES              TO W-RAYON.                        10500001
           MOVE  SPACES              TO W-FAM.                          10510001
                                                                        10520001
       FIN-TRAIT-RUPTURE-NCODIC. EXIT.                                  10530001
          EJECT                                                         10540001
                                                                        10550001
       TRAIT-RUPTURE-RAYON    SECTION.                                  10560000
                                                                        10570000
           PERFORM TRAIT-RUPTURE-FAMILLE.                               10580000
           MOVE 'N'      TO PASSAGE-ENTETE.                             10590000
           MOVE   SPACES             TO W-CODIC.                        10600001
           MOVE   0                  TO W-PRMP.                         10610001
           MOVE   SPACES             TO W-RAYON.                        10620001
           MOVE   'TOTAL'            TO W-FAM.                          10630001
           MOVE   SPACES             TO W-FIL2.                         10640001
           MOVE   'RAYON'            TO W-MARQUE.                       10650001
           MOVE   SPACES             TO W-FIL3.                         10660001
           MOVE   Z-RAYON            TO W-REF.                          10670001
           IF W-CUM-THEORAY NOT < 0                                     10680001
              MOVE   W-CUM-THEORAY      TO W-QTETHEO                    10690001
           ELSE                                                         10700001
              MOVE   W-CUM-THEORAY      TO W-QTETHEO-RDF                10710001
           END-IF.                                                      10720001
           ADD    W-CUM-THEORAY      TO W-CUM-THEOMAG.                  10730000
           IF W-CUM-INVRAY NOT < 0                                      10740001
              MOVE   W-CUM-INVRAY       TO W-QTEINV                     10750001
           ELSE                                                         10760001
              MOVE   W-CUM-INVRAY       TO W-QTEINV-RDF                 10770001
           END-IF.                                                      10780001
           ADD    W-CUM-INVRAY       TO W-CUM-INVMAG.                   10790000
           MOVE   W-CUM-ECARTRAY     TO W-QTECART.                      10800001
           ADD    W-CUM-ECARTRAY     TO W-CUM-ECARTMAG.                 10810000
           IF W-CUM-VALTHEORAY NOT < 0                                  10820001
              MOVE W-CUM-VALTHEORAY    TO TOT-VALTHEO-P                 10821004
      *       MOVE   W-CUM-VALTHEORAY   TO W-VALTHEO                    10830004
           ELSE                                                         10840001
              MOVE W-CUM-VALTHEORAY    TO TOT-VALTHEO-M                 10841004
      *       MOVE   W-CUM-VALTHEORAY   TO W-VALTHEO-RDF                10850004
           END-IF.                                                      10860001
           ADD    W-CUM-VALTHEORAY   TO W-CUM-VALTHEOMAG.               10870000
           IF W-CUM-VALINVRAY  NOT < 0                                  10880001
              MOVE W-CUM-VALINVRAY    TO TOT-VALINV-P                   10881004
      *       MOVE   W-CUM-VALINVRAY    TO W-VALINV                     10890004
           ELSE                                                         10900001
              MOVE W-CUM-VALINVRAY    TO TOT-VALINV-M                   10901004
      *       MOVE   W-CUM-VALINVRAY    TO W-VALINV-RDF                 10910004
           END-IF.                                                      10920001
           ADD    W-CUM-VALINVRAY    TO W-CUM-VALINVMAG.                10930000
           ADD    W-CUM-VALORISERAY  TO W-CUM-VALORISEMAG.              10940000
           IF  W-CUM-VALORISERAY < 0                                    10950000
               MOVE W-CUM-VALORISERAY TO W-NEGATIF                      10960000
           ELSE                                                         10970000
               MOVE W-CUM-VALORISERAY TO W-POSITIF                      10980000
           END-IF.                                                      10990000
           INITIALIZE ZONE-CUM-RAY.                                     11000000
           WRITE IIV142-RECORD       FROM LIGNE-ETAT3.                  11010001
           MOVE '+'                  TO W-CAR-GRAS.                     11020001
           WRITE IIV142-RECORD       FROM LIGNE-ETAT3.                  11030001
           MOVE '-----'              TO W-TIR.                          11040000
           WRITE IIV142-RECORD       FROM LIGNE-SEPAR.                  11050001
           ADD  2                    TO CPT-LIGNE.                      11060000
           MOVE  F130-RAYON          TO W-RAYON.                        11070001
           MOVE  F130-CFAM           TO W-FAM.                          11080001
           MOVE SPACES               TO W-TIR.                          11090001
           MOVE SPACES               TO W-CAR-GRAS.                     11100001
           MOVE 0                    TO W-POSITIF W-NEGATIF.            11110000
           MOVE   ' !'               TO W-FIL2.                         11120001
           MOVE   ' !'               TO W-FIL3.                         11130001
           MOVE   'N'                TO W-RUPT-FAM.                     11140000
           MOVE 'O'                  TO W-RUPT-RAY.                     11150000
           ADD CPT-LIGNE 2  GIVING CPT-LIGNEBIS.                        11160000
           IF CPT-LIGNEBIS > 50                                         11170000
              MOVE CPT-LIGNEBIS TO CPT-LIGNE                            11180000
              PERFORM TEST-SAUT-PAGE                                    11190000
           END-IF.                                                      11200000
                                                                        11210000
       FIN-TRAIT-RUPTURE-RAYON. EXIT.                                   11220000
           EJECT                                                        11230000
       TRAIT-RUPTURE-FAMILLE SECTION.                                   11240000
                                                                        11250001
           PERFORM TRAIT-RUPTURE-NCODIC.                                11260001
           IF PASSAGE-ENTETE = 'O'                                      11270001
              MOVE 'N'      TO PASSAGE-ENTETE                           11280001
           ELSE                                                         11290001
              WRITE IIV142-RECORD        FROM LIGNE-SEPAR               11300001
              ADD  1                    TO CPT-LIGNE                    11310001
           END-IF.                                                      11320001
           MOVE   SPACES             TO W-RAYON.                        11330001
           MOVE   SPACES             TO W-FAM.                          11340001
           MOVE   SPACES             TO W-CODIC.                        11350001
           MOVE   0                  TO W-PRMP.                         11360001
           MOVE   SPACES             TO W-FIL2.                         11370001
           MOVE   'TOTAL'            TO W-MARQUE.                       11380001
           MOVE   SPACES             TO W-FIL3.                         11390001
           MOVE   'FAMILLE'          TO W-LIBELLE.                      11400001
           MOVE   '   '              TO W-BEAU.                         11410001
           MOVE   Z-FAM              TO W-FAMLIB.                       11420001
           IF W-CUM-THEOFAM  NOT < 0                                    11430001
              MOVE   W-CUM-THEOFAM      TO W-QTETHEO                    11440001
           ELSE                                                         11450001
              MOVE   W-CUM-THEOFAM      TO W-QTETHEO-RDF                11460001
           END-IF.                                                      11470001
           ADD    W-CUM-THEOFAM      TO W-CUM-THEORAY.                  11480000
           IF W-CUM-INVFAM NOT < 0                                      11490001
              MOVE   W-CUM-INVFAM       TO W-QTEINV                     11500001
           ELSE                                                         11510001
              MOVE   W-CUM-INVFAM       TO W-QTEINV-RDF                 11520001
           END-IF                                                       11530001
           ADD    W-CUM-INVFAM       TO W-CUM-INVRAY.                   11540000
           MOVE   W-CUM-ECARTFAM     TO W-QTECART.                      11550001
           ADD    W-CUM-ECARTFAM     TO W-CUM-ECARTRAY.                 11560000
           IF W-CUM-VALTHEOFAM NOT < 0                                  11570001
              MOVE W-CUM-VALTHEOFAM     TO TOT-VALTHEO-P                11571004
      *       MOVE   W-CUM-VALTHEOFAM   TO W-VALTHEO                    11580004
           ELSE                                                         11590001
              MOVE W-CUM-VALTHEOFAM     TO TOT-VALTHEO-M                11591004
      *       MOVE   W-CUM-VALTHEOFAM   TO W-VALTHEO-RDF                11600004
           END-IF.                                                      11610001
           ADD    W-CUM-VALTHEOFAM   TO W-CUM-VALTHEORAY.               11620000
           IF W-CUM-VALINVFAM  NOT < 0                                  11630001
              MOVE W-CUM-VALINVFAM     TO TOT-VALINV-P                  11631004
      *       MOVE   W-CUM-VALINVFAM    TO W-VALINV                     11640004
           ELSE                                                         11650001
              MOVE W-CUM-VALINVFAM     TO TOT-VALINV-M                  11651004
      *       MOVE   W-CUM-VALINVFAM    TO W-VALINV-RDF                 11660004
           END-IF.                                                      11670001
           ADD    W-CUM-VALINVFAM    TO W-CUM-VALINVRAY.                11680000
           ADD    W-CUM-VALORISEFAM  TO W-CUM-VALORISERAY.              11690000
           IF  W-CUM-VALORISEFAM < 0                                    11700000
               MOVE W-CUM-VALORISEFAM TO W-NEGATIF                      11710000
           ELSE                                                         11720000
               MOVE W-CUM-VALORISEFAM TO W-POSITIF                      11730000
           END-IF.                                                      11740000
           INITIALIZE ZONE-CUM-FAM.                                     11750000
           WRITE IIV142-RECORD        FROM LIGNE-ETAT3.                 11760001
           WRITE IIV142-RECORD        FROM LIGNE-SEPAR.                 11770001
           ADD  2                TO CPT-LIGNE.                          11780000
           MOVE  F130-CFAM           TO W-FAM.                          11790001
           MOVE   ' !'               TO W-FIL2.                         11800000
           MOVE   ' !'               TO W-FIL3.                         11810000
           MOVE 0                    TO W-POSITIF W-NEGATIF.            11820000
           MOVE   'O'                TO W-RUPT-FAM.                     11830000
           MOVE 'N'                  TO W-RUPT-RAY.                     11840000
                                                                        11850000
       FIN-TRAIT-RUPTURE-FAMILLE. EXIT.                                 11860000
          EJECT                                                         11870000
      ****************************************************************  11880001
      *                EDITION DES ANOMALIES                         *  11890001
      ****************************************************************  11900001
       ECART-NON-IMPUTABLE  SECTION.                                    11910001
                                                                        11920001
           IF W-IIV142A-ECRITS = 0                                      11930001
              PERFORM ECRITURE-ENTANO                                   11940001
              MOVE Z-RAYON     TO ANO-RAYON                             11950001
              MOVE Z-FAM       TO ANO-FAM                               11960001
              MOVE Z-FAM       TO A-FAM                                 11970001
              MOVE Z-RAYON     TO A-RAYON                               11980001
           END-IF.                                                      11990001
           IF A-SOCIETE  NOT = W-SOCIETE                                12000001
              WRITE IIV142A-RECORD        FROM TIRET-ANO                12010001
              PERFORM  ECRITURE-ENTANO                                  12020001
           END-IF.                                                      12030001
           IF Z-RAYON  NOT = ANO-RAYON                                  12040001
              PERFORM TRAIT-RUPTURE-ANO-RAYON                           12050001
              MOVE Z-RAYON TO ANO-RAYON                                 12060001
              MOVE Z-FAM   TO ANO-FAM                                   12070001
              PERFORM ECRITURE-ENTANO                                   12080001
           ELSE                                                         12090001
              IF Z-FAM  NOT = ANO-FAM                                   12100001
                 PERFORM TRAIT-RUPTURE-ANO-FAMILLE                      12110001
                 MOVE Z-FAM  TO ANO-FAM                                 12120001
                 PERFORM TEST-SAUT-PAGE-ANO                             12130001
              END-IF                                                    12140001
           END-IF.                                                      12150001
           PERFORM PAS-DE-RUPTURE-ANO.                                  12160001
           PERFORM TEST-SAUT-PAGE-ANO.                                  12170001
                                                                        12180001
       FIN-ECART-NON-IMPUTABLE. EXIT.                                   12190001
       TEST-SAUT-PAGE-ANO       SECTION.                                12200001
                                                                        12210001
           IF CPT-LIGNE-ANO > 50                                        12220001
              IF Z-FAM NOT = ANO-FAM OR FIN-FICHIER = '1'               12230001
                 GO TO FIN-TEST-SAUT-PAGE-ANO                           12240001
              END-IF                                                    12250001
              IF W-RUPT-FAMANO = 'O'                                    12260001
                 MOVE '+'  TO W-REECRIT-ANO                             12270001
              ELSE                                                      12280001
                 MOVE SPACES  TO W-REECRIT-ANO                          12290001
              END-IF                                                    12300001
              WRITE IIV142A-RECORD        FROM TIRET-ANO                12310001
              PERFORM ECRITURE-ENTANO                                   12320001
           END-IF.                                                      12330001
                                                                        12340001
       FIN-TEST-SAUT-PAGE-ANO. EXIT.                                    12350001
          EJECT                                                         12360001
       PAS-DE-RUPTURE-ANO    SECTION.                                   12370001
                                                                        12380001
           MOVE 'N'                   TO PASSAGE-ENTETE-ANO.            12390001
           MOVE Z-PRMP                TO A-PRMP.                        12400001
           MOVE Z-MARQUE              TO A-MARQUE.                      12410001
           MOVE Z-REF                 TO A-REF.                         12420001
           MOVE Z-NCODIC              TO A-CODIC.                       12430001
           MOVE ZERO                  TO W-QTEINV.                      12440001
           MOVE W-CUM-INVNCOD         TO A-QTEINV-RDF.                  12450001
           ADD  W-CUM-INVNCOD         TO A-CUM-INVFAM.                  12460001
           ADD  W-CUM-THEONCOD        TO A-CUM-THEOFAM.                 12470001
           IF W-CUM-THEONCOD NOT < 0                                    12480001
              MOVE   W-CUM-THEONCOD     TO A-QTETHEO                    12490001
           ELSE                                                         12500001
              MOVE   W-CUM-THEONCOD     TO A-QTETHEO-RDF                12510001
           END-IF.                                                      12520001
           MOVE W-CUM-ECARTNCOD       TO A-QTECART.                     12530001
           ADD  W-CUM-ECARTNCOD       TO A-CUM-ECARTFAM.                12540001
           COMPUTE T-VALECART = Z-PRMP * W-CUM-INVNCOD.                 12550001
           MOVE T-VALECART            TO A-VALECART.                    12560001
           ADD  T-VALECART            TO A-CUM-VALECARTFAM.             12570001
           SUBTRACT W-CUM-INVNCOD FROM W-CUM-ECARTNCOD                  12580001
           MOVE ZERO                   TO W-CUM-INVNCOD.                12590001
           MOVE ZERO                   TO T-VALECART.                   12600001
           WRITE IIV142A-RECORD       FROM LIGNE-ANO3.                  12610001
           ADD 1                      TO W-IIV142A-ECRITS.              12620001
           ADD 1                      TO CPT-LIGNE-ANO.                 12630001
           MOVE SPACES                TO A-RAYON.                       12640001
           MOVE SPACES                TO A-FAM.                         12650001
           MOVE 'N'                   TO W-RUPT-FAMANO.                 12660001
                                                                        12670001
       FIN-PAS-DE-RUPTURE-ANO. EXIT.                                    12680001
           EJECT                                                        12690001
       TRAIT-RUPTURE-ANO-RAYON    SECTION.                              12700001
           PERFORM TRAIT-RUPTURE-ANO-FAMILLE.                           12710001
           MOVE 'N'      TO PASSAGE-ENTETE-ANO.                         12720001
           MOVE   SPACES             TO A-CODIC.                        12730001
           MOVE   0                  TO A-PRMP.                         12740001
           MOVE   SPACES             TO A-RAYON.                        12750001
           MOVE   'TOTAL'            TO A-FAM.                          12760001
           MOVE   SPACES             TO A-FIL2.                         12770001
           MOVE   'RAYON'            TO A-MARQUE.                       12780001
           MOVE   SPACES             TO A-FIL3.                         12790001
           MOVE   ANO-RAYON          TO A-REF.                          12800001
           IF A-CUM-THEORAY NOT < 0                                     12810001
              MOVE   A-CUM-THEORAY      TO A-QTETHEO                    12820001
           ELSE                                                         12830001
              MOVE   A-CUM-THEORAY      TO A-QTETHEO-RDF                12840001
           END-IF.                                                      12850001
           ADD    A-CUM-THEORAY      TO A-CUM-THEOMAG.                  12860001
           IF A-CUM-INVRAY NOT < 0                                      12870001
              MOVE   A-CUM-INVRAY       TO A-QTEINV                     12880001
           ELSE                                                         12890001
              MOVE   A-CUM-INVRAY       TO A-QTEINV-RDF                 12900001
           END-IF.                                                      12910001
           ADD    A-CUM-INVRAY       TO A-CUM-INVMAG.                   12920001
           MOVE   A-CUM-ECARTRAY     TO A-QTECART.                      12930001
           ADD    A-CUM-ECARTRAY     TO A-CUM-ECARTMAG.                 12940001
           MOVE   A-CUM-VALECARTRAY  TO A-VALECART.                     12950001
           ADD    A-CUM-VALECARTRAY  TO A-CUM-VALECARTMAG.              12960001
           INITIALIZE ZONE-ANOCUM-RAY.                                  12970001
           WRITE IIV142A-RECORD        FROM LIGNE-ANO3.                 12980001
           MOVE '+'                  TO A-CAR-GRAS.                     12990001
           WRITE IIV142A-RECORD       FROM LIGNE-ANO3.                  13000001
           MOVE '-----'              TO A-TIR.                          13010001
           WRITE IIV142A-RECORD       FROM LIGNE-ANO-SEPAR.             13020001
           ADD  2                    TO CPT-LIGNE-ANO.                  13030001
           MOVE  Z-RAYON             TO A-RAYON.                        13040001
           MOVE  Z-FAM               TO A-FAM.                          13050001
           MOVE SPACES               TO A-TIR.                          13060001
           MOVE SPACES               TO A-CAR-GRAS.                     13070001
           MOVE   ' !'               TO A-FIL2.                         13080001
           MOVE   ' !'               TO A-FIL3.                         13090001
           MOVE   'N'                TO W-RUPT-FAMANO.                  13100001
      *    MOVE 'O'                  TO W-RUPT-RAY.                     13110001
      *    ADD CPT-LIGNE 2  GIVING CPT-LIGNEBIS.                        13120001
      *    IF CPT-LIGNEBIS > 50                                         13130001
      *       MOVE CPT-LIGNEBIS TO CPT-LIGNE                            13140001
      *       PERFORM TEST-SAUT-PAGE                                    13150001
      *    END-IF.                                                      13160001
                                                                        13170001
       FIN-TRAIT-RUPTURE-ANO-RAYON. EXIT.                               13180001
       TRAIT-RUPTURE-ANO-FAMILLE SECTION.                               13190001
                                                                        13200001
           IF PASSAGE-ENTETE-ANO = 'O'                                  13210001
              MOVE 'N'      TO PASSAGE-ENTETE-ANO                       13220001
           ELSE                                                         13230001
              WRITE IIV142A-RECORD        FROM LIGNE-ANO-SEPAR          13240001
              ADD  1                    TO CPT-LIGNE-ANO                13250001
           END-IF.                                                      13260001
           MOVE   SPACES             TO A-RAYON.                        13270001
           MOVE   SPACES             TO A-FAM.                          13280001
           MOVE   SPACES             TO A-CODIC.                        13290001
           MOVE   0                  TO A-PRMP.                         13300001
           MOVE   SPACES             TO A-FIL2.                         13310001
           MOVE   'TOTAL'            TO A-MARQUE.                       13320001
           MOVE   SPACES             TO A-FIL3.                         13330001
           MOVE   'FAMILLE'          TO A-LIBELLE.                      13340001
           MOVE   '   '              TO A-BEAU.                         13350001
           MOVE   ANO-FAM              TO A-FAMLIB.                     13360001
           IF A-CUM-THEOFAM  NOT < 0                                    13370001
              MOVE   A-CUM-THEOFAM      TO A-QTETHEO                    13380001
           ELSE                                                         13390001
              MOVE   A-CUM-THEOFAM      TO A-QTETHEO-RDF                13400001
           END-IF.                                                      13410001
           ADD    A-CUM-THEOFAM      TO A-CUM-THEORAY.                  13420001
           IF A-CUM-INVFAM NOT < 0                                      13430001
              MOVE   A-CUM-INVFAM       TO A-QTEINV                     13440001
           ELSE                                                         13450001
              MOVE   A-CUM-INVFAM       TO A-QTEINV-RDF                 13460001
           END-IF                                                       13470001
           ADD    A-CUM-INVFAM       TO A-CUM-INVRAY.                   13480001
           MOVE   A-CUM-ECARTFAM     TO A-QTECART.                      13490001
           ADD    A-CUM-ECARTFAM     TO A-CUM-ECARTRAY.                 13500001
           MOVE   A-CUM-VALECARTFAM  TO A-VALECART.                     13510001
           ADD    A-CUM-VALECARTFAM  TO A-CUM-VALECARTRAY.              13520001
           INITIALIZE ZONE-ANOCUM-FAM.                                  13530001
           WRITE IIV142A-RECORD        FROM LIGNE-ANO3.                 13540001
           WRITE IIV142A-RECORD        FROM LIGNE-ANO-SEPAR.            13550001
           ADD  2                    TO CPT-LIGNE-ANO.                  13560001
           MOVE  Z-FAM               TO A-FAM.                          13570001
           MOVE   ' !'               TO A-FIL2.                         13580001
           MOVE   ' !'               TO A-FIL3.                         13590001
           MOVE   'O'                TO W-RUPT-FAMANO.                  13600001
      *    MOVE 'N'                  TO W-RUPT-RAY.                     13610001
                                                                        13620001
       FIN-TRAIT-RUPTURE-ANO-FAMILLE. EXIT.                             13630001
          EJECT                                                         13640001
      *----------------------------------------------------------------*13650000
      *         LECTURE SUIVANTE SANS SAUVEGARDE DES ZONES LUES        *13660000
      *----------------------------------------------------------------*13670000
       LECT-SUIV-FIV142     SECTION.                                    13680001
                                                                        13690000
           READ FIV142 AT END                                           13700001
                   MOVE '1'   TO FIN-FICHIER                            13710000
           END-READ.                                                    13720000
                                                                        13730000
           IF FIN-FICHIER NOT = '1'                                     13740000
              ADD   1                     TO W-FIV142-LUES              13750001
           ELSE                                                         13760000
              PERFORM TRAIT-RUPTURE-RAYON                               13770001
              PERFORM TRAIT-RUPTURE-MAG                                 13780001
              PERFORM TRAIT-RUPTURE-ANO-RAYON                           13790001
              PERFORM TRAIT-RUPTURE-ANO-MAG                             13800001
           END-IF.                                                      13810000
                                                                        13820000
       FIN-LECT-SUIV-FIV142. EXIT.                                      13830001
            EJECT                                                       13840000
      *---------------------------------------------------------------- 13850001
      *  DES QUE LA FIN DE FICHIER EST DETECTEE ON AFFICHE LES CUMULS * 13860001
      *                   TOUS MAGASINS CONFONDUS                     * 13870001
      *---------------------------------------------------------------- 13880001
       TRAIT-RUPTURE-MAG SECTION.                                       13890001
           WRITE IIV142-RECORD        FROM SAUT-PAGE.                   13900001
           ADD  1  TO CPT-PAGE.                                         13910001
           MOVE 'O'              TO PASSAGE-ENTETE.                     13920001
           MOVE  0               TO CPT-LIGNE.                          13930001
           MOVE CPT-PAGE         TO W-PAGE.                             13940001
           WRITE IIV142-RECORD   FROM ENTETE1.                          13950001
           WRITE IIV142-RECORD   FROM ENTETE2.                          13960001
           MOVE  Z-SOC            TO W-SOCIETE.                         13970001
           MOVE  F130-DINVENTAIRE TO W-INVDATE.                         13980001
           MOVE  INV-AA           TO FORMAT-AA.                         13990001
           MOVE  INV-MM           TO FORMAT-MM.                         14000001
           MOVE  INV-JJ           TO FORMAT-JJ.                         14010001
           WRITE IIV142-RECORD   FROM ENTETE3.                          14020001
           WRITE IIV142-RECORD    FROM SAUTLG2.                         14030001
           WRITE IIV142-RECORD    FROM ETAT-TIRET.                      14040001
                                                                        14050001
                                                                        14060001
           WRITE IIV142-RECORD        FROM LIGNE-TOTAL1.                14070001
           WRITE IIV142-RECORD        FROM LIGNE-TOTAL2.                14080001
           WRITE IIV142-RECORD        FROM ETAT-TIRET.                  14090001
           WRITE IIV142-RECORD        FROM SAUT-TOTAL.                  14100001
                                                                        14110001
                                                                        14120001
           MOVE   W-INTITULE           TO W-ZONE-TOT.                   14130001
           MOVE   W-CUM-THEOMAG        TO W-QTETHEOTOT.                 14140001
           MOVE   W-CUM-INVMAG         TO W-QTEINVTOT.                  14150001
           MOVE   W-CUM-ECARTMAG       TO W-QTECARTTOT.                 14160001
           MOVE   W-CUM-VALTHEOMAG     TO W-VALTHEOTOT.                 14170001
           MOVE   W-CUM-VALINVMAG      TO W-VALINVTOT.                  14180001
           IF  W-CUM-VALORISEMAG   < 0                                  14190001
               MOVE W-CUM-VALORISEMAG   TO W-NEGATIFTOT                 14200001
           ELSE                                                         14210001
               MOVE W-CUM-VALORISEMAG   TO W-POSITIFTOT                 14220001
           END-IF.                                                      14230001
           INITIALIZE ZONE-CUM-MAG.                                     14240001
           WRITE IIV142-RECORD        FROM LIGNE-TOTAL3.                14250001
           MOVE '+'                   TO W-GRAS-TOTAL.                  14260001
           WRITE IIV142-RECORD        FROM LIGNE-TOTAL3.                14270001
           WRITE IIV142-RECORD        FROM SAUT-TOTAL.                  14280001
           WRITE IIV142-RECORD        FROM ETAT-TIRET.                  14290001
           MOVE 0             TO W-POSITIF W-NEGATIF.                   14300001
           MOVE SPACES        TO W-GRAS-TOTAL.                          14310001
           MOVE '! '          TO W-FIL1.                                14320001
           MOVE ' !'          TO W-FIL2 W-FIL3.                         14330001
           MOVE   'N'         TO W-RUPT-FAM W-RUPT-RAY.                 14340001
                                                                        14350001
       FIN-TRAIT-RUPTURE-MAG. EXIT.                                     14360001
           EJECT                                                        14370001
       TRAIT-RUPTURE-ANO-MAG SECTION.                                   14380001
           WRITE IIV142A-RECORD        FROM SAUT-PAGE.                  14390001
           ADD  1  TO CPT-PAGE-ANO.                                     14400001
           MOVE 'O'              TO PASSAGE-ENTETE-ANO.                 14410001
           MOVE  0               TO CPT-LIGNE-ANO.                      14420001
           MOVE CPT-PAGE-ANO     TO A-PAGE.                             14430001
           WRITE IIV142A-RECORD   FROM ENTANO1.                         14440001
           WRITE IIV142A-RECORD   FROM ENTANO2.                         14450001
           MOVE  Z-SOC            TO A-SOCIETE.                         14460001
           MOVE  F130-DINVENTAIRE TO W-INVDATE.                         14470001
           MOVE  INV-AA           TO FORMAT-AA.                         14480001
           MOVE  INV-MM           TO FORMAT-MM.                         14490001
           MOVE  INV-JJ           TO FORMAT-JJ.                         14500001
           WRITE IIV142A-RECORD   FROM ENTANO3.                         14510001
           WRITE IIV142A-RECORD   FROM SAUTLG2.                         14520001
           WRITE IIV142A-RECORD   FROM TIRET-ANO-TOTAL.                 14530001
                                                                        14540001
                                                                        14550001
           WRITE IIV142A-RECORD        FROM LIGNE-ANOTOTAL1.            14560001
           WRITE IIV142A-RECORD        FROM LIGNE-ANOTOTAL2.            14570001
           WRITE IIV142A-RECORD        FROM TIRET-ANO-TOTAL.            14580001
           WRITE IIV142A-RECORD        FROM SAUT-TOTAL-ANO.             14590001
                                                                        14600001
                                                                        14610001
           MOVE   W-INTITULE           TO A-ZONE-TOT.                   14620001
           MOVE   A-CUM-THEOMAG        TO A-QTETHEOTOT.                 14630001
           MOVE   A-CUM-INVMAG         TO A-QTEINVTOT.                  14640001
           MOVE   A-CUM-ECARTMAG       TO A-QTECARTTOT.                 14650001
           MOVE   A-CUM-VALECARTMAG    TO A-VALECARTTOT.                14660001
           INITIALIZE ZONE-ANOCUM-MAG.                                  14670001
           WRITE IIV142A-RECORD        FROM LIGNE-ANOTOTAL3.            14680001
           MOVE '+'                    TO A-GRAS-TOTAL.                 14690001
           WRITE IIV142A-RECORD        FROM LIGNE-ANOTOTAL3.            14700001
           WRITE IIV142A-RECORD        FROM SAUT-TOTAL-ANO.             14710001
           WRITE IIV142A-RECORD        FROM TIRET-ANO-TOTAL.            14720001
           MOVE SPACES        TO A-GRAS-TOTAL.                          14730001
           MOVE '! '          TO A-FIL1.                                14740001
           MOVE ' !'          TO A-FIL2 W-FIL3.                         14750001
           MOVE   'N'         TO W-RUPT-FAMANO.                         14760001
                                                                        14770001
       FIN-TRAIT-RUPTURE-ANO-MAG. EXIT.                                 14780001
           EJECT                                                        14790001
      *----------------------------------------------------------------*14800000
      *         SAUVEGARDE DES ZONES LUES PRECEDEMMENT DANS LE CAS DE  *14810000
      *         RUPTURE CE PROCEDE PERMET D'AVOIR UNE LECTURE D'AVANCE *14820000
      *----------------------------------------------------------------*14830000
       RECUP-DERNIERES-ZONES       SECTION.                             14840000
                                                                        14850000
                                                                        14860000
           MOVE   F130-NSOCMAG         TO Z-SOC                         14870001
           MOVE   F130-RAYON           TO Z-RAYON.                      14880000
           MOVE   F130-CFAM            TO Z-FAM.                        14890000
           MOVE   F130-CMARQ           TO Z-MARQUE.                     14900001
           MOVE   F130-LREFFOURN       TO Z-REF.                        14910001
           MOVE   F130-NCODIC          TO Z-NCODIC.                     14920001
           MOVE   F130-PRMP            TO Z-PRMP.                       14930001
                                                                        14940000
       FIN-RECUP-DERNIERES-ZONES. EXIT.                                 14950000
          EJECT                                                         14960000
      *----------------------------------------------------------------*14970000
      *         ECRITURE DE L'ENTETE (MAGASIN RAYON LIEU DE STOCK)     *14980000
      *                      DANS LE CAS D'UNE RUPTURE                 *14990000
      *----------------------------------------------------------------*15000000
       ECRITURE-ENTETE     SECTION.                                     15010000
                                                                        15020000
                                                                        15030000
           ADD  1  TO CPT-PAGE.                                         15040000
           MOVE 'O'              TO PASSAGE-ENTETE.                     15050000
           MOVE  0               TO CPT-LIGNE.                          15060000
           MOVE CPT-PAGE         TO W-PAGE.                             15070000
           WRITE IIV142-RECORD   FROM ENTETE1.                          15080001
           WRITE IIV142-RECORD   FROM ENTETE2.                          15090001
           MOVE  F130-NSOCMAG     TO W-SOCIETE.                         15100000
           MOVE  F130-DINVENTAIRE TO W-INVDATE.                         15110000
           MOVE  INV-AA           TO FORMAT-AA.                         15120000
           MOVE  INV-MM           TO FORMAT-MM.                         15130000
           MOVE  INV-JJ           TO FORMAT-JJ.                         15140000
           MOVE  W-FORMAT-DATE    TO W-INVENT.                          15150000
           WRITE IIV142-RECORD   FROM ENTETE3.                          15160001
           WRITE IIV142-RECORD    FROM ETAT-TIRET.                      15170001
           WRITE IIV142-RECORD    FROM LIGNE-ETAT1.                     15180001
           WRITE IIV142-RECORD    FROM LIGNE-ETAT2.                     15190001
           MOVE '-----'          TO W-TIR.                              15200000
           WRITE IIV142-RECORD    FROM LIGNE-SEPAR.                     15210001
           MOVE  F130-CFAM       TO W-FAM.                              15220001
           MOVE  F130-RAYON      TO W-RAYON.                            15230001
           MOVE SPACES           TO W-TIR.                              15240001
           ADD  10               TO CPT-LIGNE.                          15250000
                                                                        15260000
                                                                        15270000
                                                                        15280000
       FIN-ECRITURE-ENTETE.  EXIT.                                      15290000
           EJECT                                                        15300000
      *                                                                 15310000
       ECRITURE-ENTANO        SECTION.                                  15320001
           ADD  1                TO CPT-PAGE-ANO.                       15330001
           MOVE 'O'              TO PASSAGE-ENTETE-ANO.                 15340001
           MOVE  0               TO CPT-LIGNE-ANO.                      15350001
           WRITE IIV142A-RECORD  FROM SAUT-PAGE.                        15360001
           MOVE CPT-PAGE-ANO     TO A-PAGE.                             15370001
           WRITE IIV142A-RECORD   FROM ENTANO1.                         15380001
           WRITE IIV142A-RECORD   FROM ENTANO2.                         15390001
           MOVE  W-SOCIETE        TO A-SOCIETE.                         15400001
           MOVE  W-FORMAT-DATE    TO A-INVENT.                          15410001
           WRITE IIV142A-RECORD   FROM ENTANO3.                         15420001
           WRITE IIV142A-RECORD   FROM TIRET-ANO.                       15430001
           WRITE IIV142A-RECORD   FROM LIGNE-ANO1.                      15440001
           WRITE IIV142A-RECORD   FROM LIGNE-ANO2.                      15450001
           WRITE IIV142A-RECORD   FROM TIRET-ANO.                       15460001
      *    MOVE '-----'          TO A-TIR.                              15470001
      *    WRITE IIV142A-RECORD    FROM LIGNE-ANO-SEPAR.                15480001
           MOVE  Z-FAM           TO A-FAM.                              15490001
           MOVE  Z-RAYON         TO A-RAYON.                            15500001
           MOVE SPACES           TO A-TIR.                              15510001
           MOVE  10              TO CPT-LIGNE-ANO.                      15520001
                                                                        15530001
       FIN-ECRITURE-ENTANO.  EXIT.                                      15540001
           EJECT                                                        15550001
      * ============================================================= * 15560000
      *                      F I N        B I V 1 4 2                 * 15570001
      * ============================================================= * 15580000
      *                                                                 15590000
       FIN-BIV142     SECTION.                                          15600001
                                                                        15610000
           PERFORM  COMPTE-RENDU.                                       15620000
           PERFORM  FERMETURE-FICHIERS.                                 15630000
           PERFORM  FIN-PROGRAMME.                                      15640000
                                                                        15650000
       FIN-FIN-BIV142.   EXIT.                                          15660001
      * ============================================================= * 15670000
      *          M O D U L E S      F I N      B I V 1 4 2            * 15680001
      * ============================================================= * 15690000
      *                                                                 15700000
      *====>  F I N  -  A N O R M A L E.                                15710000
      *                                                                 15720000
       FIN-ANORMALE    SECTION.                                         15730000
                                                                        15740000
           PERFORM  FERMETURE-FICHIERS.                                 15750000
           MOVE  'BIV142'                  TO  ABEND-PROG.              15760001
                                                                        15770000
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                15780000
                                                                        15790000
       FIN-FIN-ANORMALE.    EXIT.                                       15800000
      *                                                                 15810000
      *====>  C O M P T E  -  R E N D U.                                15820000
      *                                                                 15830000
       COMPTE-RENDU      SECTION.                                       15840000
                                                                        15850000
           DISPLAY  '**'.                                               15860000
           DISPLAY  '**           B I V 1 4 2 '.                        15870001
           DISPLAY  '**'.                                               15880000
           DISPLAY  '** DATE  PARAMETRE                         : '     15890000
                     EDT-DATE.                                          15900000
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  ECRITS SUR IIV142  : ' 15910001
                    W-IIV142-ECRITS.                                    15920001
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  ECRITS SUR IIV142A : ' 15930001
                    W-IIV142A-ECRITS.                                   15940001
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  LUS SUR FIV130 : '     15950000
                    W-FIV142-LUES.                                      15960001
                                                                        15970000
       FIN-COMPTE-RENDU.        EXIT.                                   15980000
      *                                                                 15990000
      *====>  F E R M E T U R E    F I C H I E R S.                     16000000
      *                                                                 16010000
       FERMETURE-FICHIERS    SECTION.                                   16020000
                                                                        16030000
           CLOSE  FIV142.                                               16040001
           CLOSE  IIV142.                                               16050001
           CLOSE  IIV142A.                                              16060001
                                                                        16070000
       F-FERMETURE-FICHIERS.    EXIT.                                   16080000
      *                                                                 16090000
      *====>  F I N    P R O G R A M M E.                               16100000
      *                                                                 16110000
       FIN-PROGRAMME        SECTION.                                    16120000
                                                                        16130000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   16140000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        16150000
       F-FIN-PROGRAMME.      EXIT.                                      16160000
           EJECT                                                        16170000
                                                                        16180000
