      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.                   BGE002.                            00020000
       DATE-COMPILED.                                                   00030000
       AUTHOR.    W GAULTIER (AGIR INF)                                 00040000
      **************************************************************    00050000
      *                                                                 00060000
      *          EDITION TABLEAU DE BORD CAPACITE DE STOCKAGE           00070000
      *               ( ETAT JOURNALIER )                               00080000
      *                                                                 00090000
      **************************************************************    00100000
      * PROJET: GE00  GESTION DES EMPLACEMENTS                     *    00110000
      * DATE CREATION : 22/03/87                                   *    00120000
      **************************************************************    00130000
      *                                                                 00140000
       ENVIRONMENT DIVISION.                                            00150000
       CONFIGURATION SECTION.                                           00160000
       SPECIAL-NAMES.                                                   00170000
           DECIMAL-POINT IS COMMA                                       00180000
           C01 IS SAUT.                                                 00190000
       INPUT-OUTPUT SECTION.                                            00200000
       FILE-CONTROL.                                                    00210000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FENTREE     ASSIGN     TO      FENTREE.              00220000
      *--                                                                       
            SELECT FENTREE     ASSIGN     TO      FENTREE                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IMPR        ASSIGN     TO      IMPR.                 00230000
      *--                                                                       
            SELECT IMPR        ASSIGN     TO      IMPR                          
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00240000
       FILE SECTION.                                                    00250000
      *                                                                 00260000
       FD   FENTREE                                                     00270000
            RECORDING F                                                 00280000
            BLOCK 0 RECORDS                                             00290000
            LABEL RECORD STANDARD                                       00300000
            DATA RECORD ENREG.                                          00310000
       01  ENREG   PIC X(100).                                          00320000
      *                                                                 00330000
       FD   IMPR                                                        00340000
            RECORDING F                                                 00350000
            BLOCK 0 RECORDS                                             00360000
            LABEL RECORD STANDARD                                       00370000
            DATA RECORD LIGNE.                                          00380000
       01  LIGNE.                                                       00390000
         02  FILLER        PIC X.                                       00400000
         02  W-LIGNE       PIC X(132).                                  00410000
      *                                                                 00420000
      ***************************************************************** 00430000
       WORKING-STORAGE SECTION.                                         00440000
      ***************************************************************** 00450000
      *                                                                 00460000
       77  FILLER           PIC X(24) VALUE '**DEBUT DE WORKING 77 **'. 00470000
      *                                                                 00480000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  I                     PIC S9(4) COMP VALUE  +0.              00490000
      *--                                                                       
       77  I                     PIC S9(4) COMP-5 VALUE  +0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  J                     PIC S9(4) COMP VALUE  +0.              00500000
      *--                                                                       
       77  J                     PIC S9(4) COMP-5 VALUE  +0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  K                     PIC S9(4) COMP VALUE  +0.              00510000
      *--                                                                       
       77  K                     PIC S9(4) COMP-5 VALUE  +0.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  CUMUL-MOY             PIC 9(10) COMP VALUE  ZERO.            00520000
      *--                                                                       
       77  CUMUL-MOY             PIC 9(10) COMP-5 VALUE  ZERO.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  WRES-RATIO            PIC 9(04) COMP VALUE  ZERO.            00530000
      *--                                                                       
       77  WRES-RATIO            PIC 9(04) COMP-5 VALUE  ZERO.                  
      *}                                                                        
       77  CTR6                  PIC 9(03)V99   VALUE  ZERO.            00540000
       77  CTR-PAGE              PIC 9(03)      VALUE  ZERO.            00550000
       77  CTR7                  PIC 9(06)      VALUE  ZERO.            00560000
       77  WZON9-7               PIC 9(07)      VALUE  ZERO.            00570000
       77  CTR10                 PIC 9(05)      VALUE  ZERO.            00580000
       77  CTR14                 PIC 9(05)      VALUE  ZERO.            00590000
       77  CTR115                PIC 9(05)      VALUE  ZERO.            00600000
       77  CTR119                PIC 9(05)      VALUE  ZERO.            00610000
       77  W1QUART               PIC 9(05)      VALUE  ZERO.            00620000
       77  WMED                  PIC 9(05)      VALUE  ZERO.            00630000
       77  W3QUART               PIC 9(05)      VALUE  ZERO.            00640000
       77  CTR44                 PIC 9(05)      VALUE  ZERO.            00650000
       77  CTR49                 PIC 9(05)      VALUE  ZERO.            00660000
       77  CTR58                 PIC 9(05)      VALUE  ZERO.            00670000
       77  CTRLEC                PIC 9(05)      VALUE  ZERO.            00680000
       77  CTRECR                PIC 9(05)      VALUE  ZERO.            00690000
      *                                                                 00700000
      *                                                                 00710000
       77  FILLER           PIC X(24) VALUE '**DEBUT DE WORKING 01 **'. 00720000
      *                                                                 00730000
      *---> BOULEENNES                                                  00740000
      *                                                                 00750000
       01  INDIC-OK            PIC X VALUE  '1'.                        00760000
             88  OK             VALUE  '1'.                             00770000
             88  NOT-OK         VALUE  '2'.                             00780000
      *                                                                 00790000
       01  INDIC-FICHIER       PIC X VALUE  '0'.                        00800000
             88  ENC-FICHIER    VALUE  '0'.                             00810000
             88  FIN-FICHIER    VALUE  '1'.                             00820000
      *                                                                 00830000
       01  INDIC-LECTURE       PIC X VALUE  '0'.                        00840000
             88  LECT-A-FAIRE       VALUE  '0'.                         00850000
             88  LECT-DEJA-FAITE    VALUE  '1'.                         00860000
      *                                                                 00870000
      *                                                                 00880000
       01  QUOTIENT              PIC 9(05)V99.                          00890000
       01  QUOTIENT-R REDEFINES QUOTIENT.                               00900000
             02  QUOT-5          PIC 9(05).                             00910000
             02  QUOT-2          PIC 9(02).                             00920000
      *                                                                 00930000
       01  WZON1                 PIC 9V99.                              00940000
       01  WZON1-R    REDEFINES  WZON1.                                 00950000
             02  FILLER          PIC 9(01).                             00960000
             02  WZON2           PIC 9(02).                             00970000
      *                                                                 00980000
       01  W-DATE9.                                                     00990000
             02  W-AA9           PIC 9(02).                             01000000
             02  W-MM9           PIC 9(02).                             01010000
             02  W-JJ9           PIC 9(02).                             01020000
      *                                                                 01030000
       01  W-DATE.                                                      01040000
             02  W-JJ                         PIC 9(02).                01050000
             02  FIL1                         PIC X     VALUE '/'.      01060000
             02  W-MM                         PIC 9(02).                01070000
             02  FIL2                         PIC X     VALUE '/'.      01080000
             02  W-AA                         PIC 9(02).                01090000
      *                                                                 01100000
      *                                                                 01110000
      *--> DESCRIPTION ENR A LIRE SUR LE FICHIER EN ENTREE(LG=100)      01120000
       01  W-ENR.                                                       01130000
             02  ENR-CLE.                                               01140000
                 03  ENR-ENTREPOT                 PIC X(06).            01150000
                 03  ENR-TYPE.                                          01160000
                    04  ENR-TYPE-STOCKAGE            PIC X.             01170000
                    04  ENR-TYPE-ENR                 PIC X.             01180000
                 03  ENR-ZON-RATIO                PIC 9(04).            01190000
             02  ENR-DONNEES-SOL.                                       01200000
                 03  CTR-SURF-TOT                 PIC 9(06).            01210000
                 03  CTR-SURF-ATT                 PIC 9(06).            01220000
                 03  CTR-NBEMPL-TOT               PIC 9(05).            01230000
                 03  CTR-NBEMPL-ATT               PIC 9(05).            01240000
                 03  CTR-NB-REF                   PIC 9(05).            01250000
                 03  CTR-STOCK                    PIC 9(06).            01260000
                 03  CTR-CAP-ATT                  PIC 9(06).            01270000
                 03  CTR-CAP-TOT                  PIC 9(06).            01280000
                 03  FILLER                       PIC X(43).            01290000
             02  ENR-DONNEES-RACK  REDEFINES ENR-DONNEES-SOL.           01300000
                 03  CTR-NBEMPL-TOT-PICK     PIC S9(05) COMP-3.         01310000
                 03  CTR-NBEMPL-ATT-PICK     PIC S9(05) COMP-3.         01320000
                 03  CTR-NBEMPL-TOT-STOC     PIC S9(05) COMP-3.         01330000
                 03  CTR-NBEMPL-ATT-STOC     PIC S9(05) COMP-3.         01340000
                 03  CTR-NB-REF-PICK         PIC S9(05) COMP-3.         01350000
                 03  CTR-NB-REF-STOC         PIC S9(05) COMP-3.         01360000
                 03  CTR-NB-REF-TOT          PIC S9(05) COMP-3.         01370000
                 03  CTR-STOCK-NBP-PICK      PIC S9(07) COMP-3.         01380000
                 03  CTR-STOCK-NBP-STOC      PIC S9(07) COMP-3.         01390000
                 03  CTR-NBP-CAP-ATT-PICK    PIC S9(05) COMP-3.         01400000
                 03  CTR-NBP-CAP-ATT-STOC    PIC S9(05) COMP-3.         01410000
                 03  CTR-NBP-CAP-TOT-PICK    PIC S9(07) COMP-3.         01420000
                 03  CTR-NBP-CAP-TOT-STOC    PIC S9(07) COMP-3.         01430000
                 03  CTR-STOCK-M3-PICK       PIC S9(07) COMP-3.         01440000
                 03  CTR-STOCK-M3-STOC       PIC S9(07) COMP-3.         01450000
                 03  CTR-M3-CAP-ATT-PICK     PIC S9(07) COMP-3.         01460000
                 03  CTR-M3-CAP-ATT-STOC     PIC S9(07) COMP-3.         01470000
                 03  CTR-M3-CAP-TOT-PICK     PIC S9(07) COMP-3.         01480000
                 03  CTR-M3-CAP-TOT-STOC     PIC S9(07) COMP-3.         01490000
                 03  FILLER                  PIC X(21).                 01500000
             02  ENR-DONNEES-VRAC REDEFINES ENR-DONNEES-SOL.            01510000
                 03  CTR-NBEMPL-TOT-V             PIC 9(05).            01520000
                 03  CTR-NBEMPL-ATT-V             PIC 9(05).            01530000
                 03  CTR-NB-REF-V                 PIC 9(05).            01540000
                 03  CTR-STOCK-V                  PIC 9(06).            01550000
                 03  FILLER                       PIC X(67).            01560000
      *                                                                 01570000
      *                                                                 01580000
      ** AIDA ************************************************* IRDF ** 01590000
      *                                                                 01600000
      *                                                                 01610000
      *                                                                 01620000
      *                DESCRIPTION DE L'ETAT IGE001                     01630000
      *                                                                 01640000
      *                                                                 01650000
      *                                                                 01660000
      ***************************************************************** 01670000
      *                                                                 01680000
       01  IGE001.                                                      01690000
      ***************************************************************** 01700000
      *  ENTETE PHYSIQUE DE PAGE                                        01710000
      ***************************************************************** 01720000
      ***************************************************************** 01730000
          02 HENTET01.                                                  01740000
            04 FILLER PIC X(20) VALUE SPACES.                           01750000
            04 FILLER PIC X(20) VALUE '                E T '.           01760000
            04 FILLER PIC X(20) VALUE 'A B L I S S E M E N '.           01770000
            04 FILLER PIC X(20) VALUE 'T S    D A R T Y    '.           01780000
            04 FILLER PIC X(20) VALUE SPACES.                           01790000
            04 FILLER PIC X(20) VALUE 'BGE001              '.           01800000
            04 FILLER PIC X(11) VALUE SPACES.                           01810000
            04 FILLER PIC X(01) VALUE SPACES.                           01820000
      ***************************************************************** 01830000
          02 HENTET02.                                                  01840000
            04 FILLER PIC X(20) VALUE SPACES.                           01850000
            04 FILLER PIC X(20) VALUE SPACES.                           01860000
            04 FILLER PIC X(20) VALUE SPACES.                           01870000
            04 FILLER PIC X(20) VALUE SPACES.                           01880000
            04 FILLER PIC X(20) VALUE SPACES.                           01890000
            04 FILLER PIC X(20) VALUE SPACES.                           01900000
            04 FILLER PIC X(11) VALUE SPACES.                           01910000
            04 FILLER PIC X(01) VALUE SPACES.                           01920000
      ***************************************************************** 01930000
          02 HENTET03.                                                  01940000
            04 FILLER PIC X(11) VALUE ' ENTREPOT :'.                    01950000
            04 FILLER PIC X(01) VALUE SPACES.                           01960000
            04 ZON1 PIC X(6).                                           01970000
            04 FILLER PIC X(01) VALUE SPACES.                           01980000
            04 FILLER PIC X(20) VALUE SPACES.                           01990000
            04 FILLER PIC X(20) VALUE ' TABLEAU DE BORD CAP'.           02000000
            04 FILLER PIC X(20) VALUE 'ACITE               '.           02010000
            04 FILLER PIC X(20) VALUE SPACES.                           02020000
            04 FILLER PIC X(7) VALUE ' PAGE :'.                         02030000
            04 ZON2 PIC ZZ9.                                            02040000
            04 FILLER PIC X(03) VALUE SPACES.                           02050000
            04 FILLER PIC X(19) VALUE SPACES.                           02060000
            04 FILLER PIC X(01) VALUE SPACES.                           02070000
      ***************************************************************** 02080000
          02 HENTET04.                                                  02090000
            04 FILLER PIC X(11) VALUE ' DATE     :'.                    02100000
            04 FILLER PIC X(01) VALUE SPACES.                           02110000
            04 ZON3 PIC X(8).                                           02120000
            04 FILLER PIC X(01) VALUE SPACES.                           02130000
            04 FILLER PIC X(20) VALUE SPACES.                           02140000
            04 FILLER PIC X(20) VALUE '     DE STOCKAGE    '.           02150000
            04 FILLER PIC X(20) VALUE SPACES.                           02160000
            04 FILLER PIC X(20) VALUE SPACES.                           02170000
            04 FILLER PIC X(20) VALUE SPACES.                           02180000
            04 FILLER PIC X(10) VALUE SPACES.                           02190000
            04 FILLER PIC X(01) VALUE SPACES.                           02200000
      ***************************************************************** 02210000
          02 HENTET05.                                                  02220000
            04 FILLER PIC X(20) VALUE '    ----------------'.           02230000
            04 FILLER PIC X(20) VALUE '--------------------'.           02240000
            04 FILLER PIC X(20) VALUE '--------------------'.           02250000
            04 FILLER PIC X(20) VALUE '--------------------'.           02260000
            04 FILLER PIC X(20) VALUE '--------------------'.           02270000
            04 FILLER PIC X(20) VALUE '------              '.           02280000
            04 FILLER PIC X(11) VALUE SPACES.                           02290000
            04 FILLER PIC X(01) VALUE SPACES.                           02300000
      ***************************************************************** 02310000
          02 HENTET06.                                                  02320000
            04 FILLER PIC X(20) VALUE '    !         !     '.           02330000
            04 FILLER PIC X(20) VALUE SPACES.                           02340000
            04 FILLER PIC X(20) VALUE SPACES.                           02350000
            04 FILLER PIC X(20) VALUE SPACES.                           02360000
            04 FILLER PIC X(20) VALUE SPACES.                           02370000
            04 FILLER PIC X(20) VALUE '     !              '.           02380000
            04 FILLER PIC X(11) VALUE SPACES.                           02390000
            04 FILLER PIC X(01) VALUE SPACES.                           02400000
      ***************************************************************** 02410000
          02 DETAIL01.                                                  02420000
            04 FILLER PIC X(20) VALUE '    ! SOL     ! SURF'.           02430000
            04 FILLER PIC X(20) VALUE 'ACE DE STOCKAGE    T'.           02440000
            04 FILLER PIC X(16) VALUE 'OTALE          :'.               02450000
            04 FILLER PIC X(01) VALUE SPACES.                           02460000
            04 ZON4 PIC ZZZZZ9.                                         02470000
            04 FILLER PIC X(01) VALUE SPACES.                           02480000
            04 FILLER PIC X(20) VALUE SPACES.                           02490000
            04 FILLER PIC X(20) VALUE SPACES.                           02500000
            04 FILLER PIC X(20) VALUE ' !                  '.           02510000
            04 FILLER PIC X(7) VALUE SPACES.                            02520000
            04 FILLER PIC X(01) VALUE SPACES.                           02530000
      ***************************************************************** 02540000
          02 DETAIL02.                                                  02550000
            04 FILLER PIC X(20) VALUE '    !         !     '.           02560000
            04 FILLER PIC X(20) VALUE '                   A'.           02570000
            04 FILLER PIC X(16) VALUE 'TTRIBUEE       :'.               02580000
            04 FILLER PIC X(01) VALUE SPACES.                           02590000
            04 ZON5 PIC ZZZZZ9.                                         02600000
            04 FILLER PIC X(01) VALUE SPACES.                           02610000
            04 ZON6 PIC ZZ9,99.                                         02620000
            04 FILLER PIC X(01) VALUE SPACES.                           02630000
            04 FILLER PIC X(20) VALUE '%                   '.           02640000
            04 FILLER PIC X(20) VALUE '              !     '.           02650000
            04 FILLER PIC X(20) VALUE SPACES.                           02660000
            04 FILLER PIC X(01) VALUE SPACES.                           02670000
      ***************************************************************** 02680000
          02 DETAIL03.                                                  02690000
            04 FILLER PIC X(20) VALUE '    !         !     '.           02700000
            04 FILLER PIC X(20) VALUE '                   D'.           02710000
            04 FILLER PIC X(16) VALUE 'ISPONIBLE      :'.               02720000
            04 FILLER PIC X(01) VALUE SPACES.                           02730000
            04 ZON7 PIC ZZZZZ9.                                         02740000
            04 FILLER PIC X(01) VALUE SPACES.                           02750000
            04 ZON8 PIC ZZ9,99.                                         02760000
            04 FILLER PIC X(01) VALUE SPACES.                           02770000
            04 FILLER PIC X(20) VALUE '%                   '.           02780000
            04 FILLER PIC X(20) VALUE '              !     '.           02790000
            04 FILLER PIC X(20) VALUE SPACES.                           02800000
            04 FILLER PIC X(01) VALUE SPACES.                           02810000
      ***************************************************************** 02820000
          02 DETAIL04.                                                  02830000
            04 FILLER PIC X(20) VALUE '    !         !     '.           02840000
            04 FILLER PIC X(20) VALUE SPACES.                           02850000
            04 FILLER PIC X(20) VALUE SPACES.                           02860000
            04 FILLER PIC X(20) VALUE SPACES.                           02870000
            04 FILLER PIC X(20) VALUE SPACES.                           02880000
            04 FILLER PIC X(20) VALUE '     !              '.           02890000
            04 FILLER PIC X(11) VALUE SPACES.                           02900000
            04 FILLER PIC X(01) VALUE SPACES.                           02910000
      ***************************************************************** 02920000
          02 DETAIL05.                                                  02930000
            04 FILLER PIC X(20) VALUE '    !         ! NOMB'.           02940000
            04 FILLER PIC X(20) VALUE 'RE D''EMPLACEMENTS  T'.          02950000
            04 FILLER PIC X(16) VALUE 'OTAL           :'.               02960000
            04 FILLER PIC X(01) VALUE SPACES.                           02970000
            04 ZON9 PIC ZZZZ9.                                          02980000
            04 FILLER PIC X(01) VALUE SPACES.                           02990000
            04 FILLER PIC X(20) VALUE SPACES.                           03000000
            04 FILLER PIC X(20) VALUE SPACES.                           03010000
            04 FILLER PIC X(20) VALUE '  !                 '.           03020000
            04 FILLER PIC X(8) VALUE SPACES.                            03030000
            04 FILLER PIC X(01) VALUE SPACES.                           03040000
      ***************************************************************** 03050000
          02 DETAIL06.                                                  03060000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03070000
            04 FILLER PIC X(20) VALUE '                   A'.           03080000
            04 FILLER PIC X(16) VALUE 'TTRIBUE        :'.               03090000
            04 FILLER PIC X(01) VALUE SPACES.                           03100000
            04 ZON10 PIC ZZZZ9.                                         03110000
            04 FILLER PIC X(01) VALUE SPACES.                           03120000
            04 ZON11 PIC ZZ9,99.                                        03130000
            04 FILLER PIC X(01) VALUE SPACES.                           03140000
            04 FILLER PIC X(20) VALUE '%                   '.           03150000
            04 FILLER PIC X(20) VALUE '               !    '.           03160000
            04 FILLER PIC X(20) VALUE SPACES.                           03170000
            04 FILLER PIC X(1) VALUE SPACES.                            03180000
            04 FILLER PIC X(01) VALUE SPACES.                           03190000
      ***************************************************************** 03200000
          02 DETAIL07.                                                  03210000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03220000
            04 FILLER PIC X(20) VALUE '                   D'.           03230000
            04 FILLER PIC X(16) VALUE 'ISPONIBLE      :'.               03240000
            04 FILLER PIC X(01) VALUE SPACES.                           03250000
            04 ZON12 PIC ZZZZ9.                                         03260000
            04 FILLER PIC X(01) VALUE SPACES.                           03270000
            04 ZON13 PIC ZZ9,99.                                        03280000
            04 FILLER PIC X(01) VALUE SPACES.                           03290000
            04 FILLER PIC X(20) VALUE '%                   '.           03300000
            04 FILLER PIC X(20) VALUE '               !    '.           03310000
            04 FILLER PIC X(20) VALUE SPACES.                           03320000
            04 FILLER PIC X(1) VALUE SPACES.                            03330000
            04 FILLER PIC X(01) VALUE SPACES.                           03340000
      ***************************************************************** 03350000
          02 DETAIL08.                                                  03360000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03370000
            04 FILLER PIC X(20) VALUE SPACES.                           03380000
            04 FILLER PIC X(20) VALUE SPACES.                           03390000
            04 FILLER PIC X(20) VALUE SPACES.                           03400000
            04 FILLER PIC X(20) VALUE SPACES.                           03410000
            04 FILLER PIC X(20) VALUE '     !              '.           03420000
            04 FILLER PIC X(11) VALUE SPACES.                           03430000
            04 FILLER PIC X(01) VALUE SPACES.                           03440000
      ***************************************************************** 03450000
          02 DETAIL09.                                                  03460000
            04 FILLER PIC X(20) VALUE '    !         ! CAPA'.           03470000
            04 FILLER PIC X(20) VALUE 'CITE STOCKAGE      N'.           03480000
            04 FILLER PIC X(16) VALUE 'B REFERENCES   :'.               03490000
            04 FILLER PIC X(01) VALUE SPACES.                           03500000
            04 ZON14 PIC ZZZZ9.                                         03510000
            04 FILLER PIC X(01) VALUE SPACES.                           03520000
            04 FILLER PIC X(20) VALUE SPACES.                           03530000
            04 FILLER PIC X(20) VALUE SPACES.                           03540000
            04 FILLER PIC X(20) VALUE '  !                 '.           03550000
            04 FILLER PIC X(8) VALUE SPACES.                            03560000
            04 FILLER PIC X(01) VALUE SPACES.                           03570000
      ***************************************************************** 03580000
          02 DETAIL10.                                                  03590000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03600000
            04 FILLER PIC X(20) VALUE '                   S'.           03610000
            04 FILLER PIC X(16) VALUE 'TOCK           :'.               03620000
            04 FILLER PIC X(01) VALUE SPACES.                           03630000
            04 ZON15 PIC ZZZZZ9.                                        03640000
            04 FILLER PIC X(01) VALUE SPACES.                           03650000
            04 FILLER PIC X(20) VALUE SPACES.                           03660000
            04 FILLER PIC X(20) VALUE SPACES.                           03670000
            04 FILLER PIC X(20) VALUE ' !                  '.           03680000
            04 FILLER PIC X(7) VALUE SPACES.                            03690000
            04 FILLER PIC X(01) VALUE SPACES.                           03700000
      ***************************************************************** 03710000
          02 DETAIL11.                                                  03720000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03730000
            04 FILLER PIC X(20) VALUE '                   C'.           03740000
            04 FILLER PIC X(16) VALUE 'APACITE ATTRIB :'.               03750000
            04 FILLER PIC X(01) VALUE SPACES.                           03760000
            04 ZON16 PIC ZZZZZ9.                                        03770000
            04 FILLER PIC X(01) VALUE SPACES.                           03780000
            04 FILLER PIC X(20) VALUE SPACES.                           03790000
            04 FILLER PIC X(20) VALUE SPACES.                           03800000
            04 FILLER PIC X(20) VALUE ' !                  '.           03810000
            04 FILLER PIC X(7) VALUE SPACES.                            03820000
            04 FILLER PIC X(01) VALUE SPACES.                           03830000
      ***************************************************************** 03840000
          02 DETAIL12.                                                  03850000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03860000
            04 FILLER PIC X(20) VALUE SPACES.                           03870000
            04 FILLER PIC X(16) VALUE '        TOTALE :'.               03880000
            04 FILLER PIC X(01) VALUE SPACES.                           03890000
            04 ZON17 PIC ZZZZZ9.                                        03900000
            04 FILLER PIC X(01) VALUE SPACES.                           03910000
            04 FILLER PIC X(20) VALUE SPACES.                           03920000
            04 FILLER PIC X(20) VALUE SPACES.                           03930000
            04 FILLER PIC X(20) VALUE ' !                  '.           03940000
            04 FILLER PIC X(7) VALUE SPACES.                            03950000
            04 FILLER PIC X(01) VALUE SPACES.                           03960000
      ***************************************************************** 03970000
          02 DETAIL13.                                                  03980000
            04 FILLER PIC X(20) VALUE '    !         !     '.           03990000
            04 FILLER PIC X(20) VALUE SPACES.                           04000000
            04 FILLER PIC X(20) VALUE SPACES.                           04010000
            04 FILLER PIC X(20) VALUE SPACES.                           04020000
            04 FILLER PIC X(20) VALUE SPACES.                           04030000
            04 FILLER PIC X(20) VALUE '     !              '.           04040000
            04 FILLER PIC X(11) VALUE SPACES.                           04050000
            04 FILLER PIC X(01) VALUE SPACES.                           04060000
      ***************************************************************** 04070000
          02 DETAIL14.                                                  04080000
            04 FILLER PIC X(20) VALUE '    !         ! RATI'.           04090000
            04 FILLER PIC X(20) VALUE 'OS OCCUPATION       '.           04100000
            04 FILLER PIC X(20) VALUE '                  MO'.           04110000
            04 FILLER PIC X(20) VALUE 'Y    MIN    1QU    M'.           04120000
            04 FILLER PIC X(20) VALUE 'ED    3QU    MAX    '.           04130000
            04 FILLER PIC X(20) VALUE '     !              '.           04140000
            04 FILLER PIC X(11) VALUE SPACES.                           04150000
            04 FILLER PIC X(01) VALUE SPACES.                           04160000
      ***************************************************************** 04170000
          02 DETAIL15.                                                  04180000
            04 FILLER PIC X(20) VALUE '    !         !     '.           04190000
            04 FILLER PIC X(20) VALUE '                   T'.           04200000
            04 FILLER PIC X(16) VALUE 'AUX OCCUPATION :'.               04210000
            04 FILLER PIC X(01) VALUE SPACES.                           04220000
            04 ZON18 PIC ZZZ9.                                          04230000
            04 FILLER PIC X(01) VALUE SPACES.                           04240000
            04 FILLER PIC X(1) VALUE SPACES.                            04250000
            04 FILLER PIC X(01) VALUE SPACES.                           04260000
            04 ZON19 PIC ZZZ9.                                          04270000
            04 FILLER PIC X(01) VALUE SPACES.                           04280000
            04 FILLER PIC X(1) VALUE SPACES.                            04290000
            04 FILLER PIC X(01) VALUE SPACES.                           04300000
            04 ZON20 PIC ZZZ9 BLANK WHEN ZERO.                          04310000
            04 FILLER PIC X(01) VALUE SPACES.                           04320000
            04 FILLER PIC X(1) VALUE SPACES.                            04330000
            04 FILLER PIC X(01) VALUE SPACES.                           04340000
            04 ZON21 PIC ZZZ9 BLANK WHEN ZERO.                          04350000
            04 FILLER PIC X(01) VALUE SPACES.                           04360000
            04 FILLER PIC X(1) VALUE SPACES.                            04370000
            04 FILLER PIC X(01) VALUE SPACES.                           04380000
            04 ZON22 PIC ZZZ9 BLANK WHEN ZERO.                          04390000
            04 FILLER PIC X(01) VALUE SPACES.                           04400000
            04 FILLER PIC X(1) VALUE SPACES.                            04410000
            04 FILLER PIC X(01) VALUE SPACES.                           04420000
            04 ZON23 PIC ZZZ9.                                          04430000
            04 FILLER PIC X(01) VALUE SPACES.                           04440000
            04 FILLER PIC X(20) VALUE '        !           '.           04450000
            04 FILLER PIC X(14) VALUE SPACES.                           04460000
            04 FILLER PIC X(01) VALUE SPACES.                           04470000
      ***************************************************************** 04480000
          02 DETAIL16.                                                  04490000
            04 FILLER PIC X(20) VALUE '    !         !     '.           04500000
            04 FILLER PIC X(20) VALUE '                   N'.           04510000
            04 FILLER PIC X(16) VALUE 'BP/EMPLACNT    :'.               04520000
            04 FILLER PIC X(01) VALUE SPACES.                           04530000
            04 ZON24 PIC ZZZ9.                                          04540000
            04 FILLER PIC X(01) VALUE SPACES.                           04550000
            04 FILLER PIC X(1) VALUE SPACES.                            04560000
            04 FILLER PIC X(01) VALUE SPACES.                           04570000
            04 ZON25   PIC ZZZ9.                                        04580000
            04 FILLER PIC X(01) VALUE SPACES.                           04590000
            04 FILLER PIC X(1) VALUE SPACES.                            04600000
            04 FILLER PIC X(01) VALUE SPACES.                           04610000
            04 ZON26   PIC ZZZ9 BLANK WHEN ZERO.                        04620000
            04 FILLER PIC X(01) VALUE SPACES.                           04630000
            04 FILLER PIC X(1) VALUE SPACES.                            04640000
            04 FILLER PIC X(01) VALUE SPACES.                           04650000
            04 ZON27   PIC ZZZ9 BLANK WHEN ZERO.                        04660000
            04 FILLER PIC X(01) VALUE SPACES.                           04670000
            04 FILLER PIC X(1) VALUE SPACES.                            04680000
            04 FILLER PIC X(01) VALUE SPACES.                           04690000
            04 ZON28   PIC ZZZ9 BLANK WHEN ZERO.                        04700000
            04 FILLER PIC X(01) VALUE SPACES.                           04710000
            04 FILLER PIC X(1) VALUE SPACES.                            04720000
            04 FILLER PIC X(01) VALUE SPACES.                           04730000
            04 ZON29   PIC ZZZ9.                                        04740000
            04 FILLER PIC X(01) VALUE SPACES.                           04750000
            04 FILLER PIC X(20) VALUE '        !           '.           04760000
            04 FILLER PIC X(14) VALUE SPACES.                           04770000
            04 FILLER PIC X(01) VALUE SPACES.                           04780000
      ***************************************************************** 04790000
          02 DETAIL17.                                                  04800000
            04 FILLER PIC X(20) VALUE '    !         !     '.           04810000
            04 FILLER PIC X(20) VALUE '                   N'.           04820000
            04 FILLER PIC X(16) VALUE 'BP/REFERENCE   :'.               04830000
            04 FILLER PIC X(01) VALUE SPACES.                           04840000
            04 ZON30   PIC ZZZ9.                                        04850000
            04 FILLER PIC X(01) VALUE SPACES.                           04860000
            04 FILLER PIC X(1) VALUE SPACES.                            04870000
            04 FILLER PIC X(01) VALUE SPACES.                           04880000
            04 ZON31   PIC ZZZ9.                                        04890000
            04 FILLER PIC X(01) VALUE SPACES.                           04900000
            04 FILLER PIC X(1) VALUE SPACES.                            04910000
            04 FILLER PIC X(01) VALUE SPACES.                           04920000
            04 ZON32   PIC ZZZ9 BLANK WHEN ZERO.                        04930000
            04 FILLER PIC X(01) VALUE SPACES.                           04940000
            04 FILLER PIC X(1) VALUE SPACES.                            04950000
            04 FILLER PIC X(01) VALUE SPACES.                           04960000
            04 ZON33   PIC ZZZ9 BLANK WHEN ZERO.                        04970000
            04 FILLER PIC X(01) VALUE SPACES.                           04980000
            04 FILLER PIC X(1) VALUE SPACES.                            04990000
            04 FILLER PIC X(01) VALUE SPACES.                           05000000
            04 ZON34   PIC ZZZ9 BLANK WHEN ZERO.                        05010000
            04 FILLER PIC X(01) VALUE SPACES.                           05020000
            04 FILLER PIC X(1) VALUE SPACES.                            05030000
            04 FILLER PIC X(01) VALUE SPACES.                           05040000
            04 ZON35   PIC ZZZ9.                                        05050000
            04 FILLER PIC X(01) VALUE SPACES.                           05060000
            04 FILLER PIC X(20) VALUE '        !           '.           05070000
            04 FILLER PIC X(14) VALUE SPACES.                           05080000
            04 FILLER PIC X(01) VALUE SPACES.                           05090000
      ***************************************************************** 05100000
          02 DETAIL18.                                                  05110000
            04 FILLER PIC X(20) VALUE '    !         !     '.           05120000
            04 FILLER PIC X(20) VALUE '                   N'.           05130000
            04 FILLER PIC X(16) VALUE 'BE/REFERENCE   :'.               05140000
            04 FILLER PIC X(01) VALUE SPACES.                           05150000
            04 ZON36   PIC ZZZ9.                                        05160000
            04 FILLER PIC X(01) VALUE SPACES.                           05170000
            04 FILLER PIC X(1) VALUE SPACES.                            05180000
            04 FILLER PIC X(01) VALUE SPACES.                           05190000
            04 ZON37   PIC ZZZ9.                                        05200000
            04 FILLER PIC X(01) VALUE SPACES.                           05210000
            04 FILLER PIC X(1) VALUE SPACES.                            05220000
            04 FILLER PIC X(01) VALUE SPACES.                           05230000
            04 ZON38   PIC ZZZ9 BLANK WHEN ZERO.                        05240000
            04 FILLER PIC X(01) VALUE SPACES.                           05250000
            04 FILLER PIC X(1) VALUE SPACES.                            05260000
            04 FILLER PIC X(01) VALUE SPACES.                           05270000
            04 ZON39   PIC ZZZ9 BLANK WHEN ZERO.                        05280000
            04 FILLER PIC X(01) VALUE SPACES.                           05290000
            04 FILLER PIC X(1) VALUE SPACES.                            05300000
            04 FILLER PIC X(01) VALUE SPACES.                           05310000
            04 ZON40   PIC ZZZ9 BLANK WHEN ZERO.                        05320000
            04 FILLER PIC X(01) VALUE SPACES.                           05330000
            04 FILLER PIC X(1) VALUE SPACES.                            05340000
            04 FILLER PIC X(01) VALUE SPACES.                           05350000
            04 ZON41   PIC ZZZ9.                                        05360000
            04 FILLER PIC X(01) VALUE SPACES.                           05370000
            04 FILLER PIC X(20) VALUE '        !           '.           05380000
            04 FILLER PIC X(14) VALUE SPACES.                           05390000
            04 FILLER PIC X(01) VALUE SPACES.                           05400000
      ***************************************************************** 05410000
          02 HENTET19.                                                  05420000
            04 FILLER PIC X(20) VALUE '    ----------------'.           05430000
            04 FILLER PIC X(20) VALUE '--------------------'.           05440000
            04 FILLER PIC X(20) VALUE '--------------------'.           05450000
            04 FILLER PIC X(20) VALUE '--------------------'.           05460000
            04 FILLER PIC X(20) VALUE '--------------------'.           05470000
            04 FILLER PIC X(20) VALUE '------              '.           05480000
            04 FILLER PIC X(11) VALUE SPACES.                           05490000
            04 FILLER PIC X(01) VALUE SPACES.                           05500000
      ***************************************************************** 05510000
          02 DETAIL20.                                                  05520000
            04 FILLER PIC X(20) VALUE '    ! RACK    !     '.           05530000
            04 FILLER PIC X(20) VALUE SPACES.                           05540000
            04 FILLER PIC X(20) VALUE '                 PIC'.           05550000
            04 FILLER PIC X(20) VALUE 'KING         STOCKAG'.           05560000
            04 FILLER PIC X(20) VALUE 'E           TOTAL   '.           05570000
            04 FILLER PIC X(20) VALUE '     !              '.           05580000
            04 FILLER PIC X(11) VALUE SPACES.                           05590000
            04 FILLER PIC X(01) VALUE SPACES.                           05600000
      ***************************************************************** 05610000
          02 DETAIL21.                                                  05620000
            04 FILLER PIC X(20) VALUE '    !         !     '.           05630000
            04 FILLER PIC X(20) VALUE SPACES.                           05640000
            04 FILLER PIC X(20) VALUE SPACES.                           05650000
            04 FILLER PIC X(20) VALUE SPACES.                           05660000
            04 FILLER PIC X(20) VALUE SPACES.                           05670000
            04 FILLER PIC X(20) VALUE '     !              '.           05680000
            04 FILLER PIC X(11) VALUE SPACES.                           05690000
            04 FILLER PIC X(01) VALUE SPACES.                           05700000
      ***************************************************************** 05710000
          02 DETAIL22.                                                  05720000
            04 FILLER PIC X(20) VALUE '    !         ! NOMB'.           05730000
            04 FILLER PIC X(20) VALUE 'RE EMPLACEMENTS  TOT'.           05740000
            04 FILLER PIC X(14) VALUE 'AL           :'.                 05750000
            04 FILLER PIC X(01) VALUE SPACES.                           05760000
            04 ZON42   PIC ZZZZ9.                                       05770000
            04 FILLER PIC X(01) VALUE SPACES.                           05780000
            04 FILLER PIC X(10) VALUE SPACES.                           05790000
            04 FILLER PIC X(01) VALUE SPACES.                           05800000
            04 ZON43   PIC ZZZZ9.                                       05810000
            04 FILLER PIC X(01) VALUE SPACES.                           05820000
            04 FILLER PIC X(10) VALUE SPACES.                           05830000
            04 FILLER PIC X(01) VALUE SPACES.                           05840000
            04 ZON44   PIC ZZZZ9.                                       05850000
            04 FILLER PIC X(01) VALUE SPACES.                           05860000
            04 FILLER PIC X(20) VALUE '          !         '.           05870000
            04 FILLER PIC X(16) VALUE SPACES.                           05880000
            04 FILLER PIC X(01) VALUE SPACES.                           05890000
      ***************************************************************** 05900000
          02 DETAIL23.                                                  05910000
            04 FILLER PIC X(20) VALUE '    !         !     '.           05920000
            04 FILLER PIC X(20) VALUE '                 ATT'.           05930000
            04 FILLER PIC X(14) VALUE 'RIBUE        :'.                 05940000
            04 FILLER PIC X(01) VALUE SPACES.                           05950000
            04 ZON45   PIC ZZZZ9.                                       05960000
            04 FILLER PIC X(01) VALUE SPACES.                           05970000
            04 ZON46  PIC ZZ9,99.                                       05980000
            04 FILLER PIC X(01) VALUE SPACES.                           05990000
            04 FILLER PIC X(3) VALUE '%  '.                             06000000
            04 FILLER PIC X(01) VALUE SPACES.                           06010000
            04 ZON47   PIC ZZZZ9.                                       06020000
            04 FILLER PIC X(01) VALUE SPACES.                           06030000
            04 ZON48  PIC ZZ9,99.                                       06040000
            04 FILLER PIC X(01) VALUE SPACES.                           06050000
            04 FILLER PIC X(3) VALUE '%  '.                             06060000
            04 FILLER PIC X(01) VALUE SPACES.                           06070000
            04 ZON49   PIC ZZZZ9.                                       06080000
            04 FILLER PIC X(01) VALUE SPACES.                           06090000
            04 ZON50  PIC ZZ9,99.                                       06100000
            04 FILLER PIC X(01) VALUE SPACES.                           06110000
            04 FILLER PIC X(20) VALUE '%  !                '.           06120000
            04 FILLER PIC X(9) VALUE SPACES.                            06130000
            04 FILLER PIC X(01) VALUE SPACES.                           06140000
      ***************************************************************** 06150000
          02 DETAIL24.                                                  06160000
            04 FILLER PIC X(20) VALUE '    !         !     '.           06170000
            04 FILLER PIC X(20) VALUE '                 DIS'.           06180000
            04 FILLER PIC X(14) VALUE 'PONIBLE      :'.                 06190000
            04 FILLER PIC X(01) VALUE SPACES.                           06200000
            04 ZON51   PIC ZZZZ9.                                       06210000
            04 FILLER PIC X(01) VALUE SPACES.                           06220000
            04 ZON52  PIC ZZ9,99.                                       06230000
            04 FILLER PIC X(01) VALUE SPACES.                           06240000
            04 FILLER PIC X(3) VALUE '%  '.                             06250000
            04 FILLER PIC X(01) VALUE SPACES.                           06260000
            04 ZON53   PIC ZZZZ9.                                       06270000
            04 FILLER PIC X(01) VALUE SPACES.                           06280000
            04 ZON54  PIC ZZ9,99.                                       06290000
            04 FILLER PIC X(01) VALUE SPACES.                           06300000
            04 FILLER PIC X(3) VALUE '%  '.                             06310000
            04 FILLER PIC X(01) VALUE SPACES.                           06320000
            04 ZON55   PIC ZZZZ9.                                       06330000
            04 FILLER PIC X(01) VALUE SPACES.                           06340000
            04 ZON56  PIC ZZ9,99.                                       06350000
            04 FILLER PIC X(01) VALUE SPACES.                           06360000
            04 FILLER PIC X(20) VALUE '%  !                '.           06370000
            04 FILLER PIC X(9) VALUE SPACES.                            06380000
            04 FILLER PIC X(01) VALUE SPACES.                           06390000
      ***************************************************************** 06400000
          02 DETAIL25.                                                  06410000
            04 FILLER PIC X(20) VALUE '    !         !     '.           06420000
            04 FILLER PIC X(20) VALUE SPACES.                           06430000
            04 FILLER PIC X(20) VALUE SPACES.                           06440000
            04 FILLER PIC X(20) VALUE SPACES.                           06450000
            04 FILLER PIC X(20) VALUE SPACES.                           06460000
            04 FILLER PIC X(20) VALUE '     !              '.           06470000
            04 FILLER PIC X(11) VALUE SPACES.                           06480000
            04 FILLER PIC X(01) VALUE SPACES.                           06490000
      ***************************************************************** 06500000
          02 DETAIL26.                                                  06510000
            04 FILLER PIC X(20) VALUE '    !         ! NB D'.           06520000
            04 FILLER PIC X(20) VALUE 'E REFERENCES        '.           06530000
            04 FILLER PIC X(14) VALUE '             :'.                 06540000
            04 FILLER PIC X(01) VALUE SPACES.                           06550000
            04 ZON57   PIC ZZZZ9.                                       06560000
            04 FILLER PIC X(01) VALUE SPACES.                           06570000
            04 FILLER PIC X(10) VALUE SPACES.                           06580000
            04 FILLER PIC X(01) VALUE SPACES.                           06590000
            04 ZON58   PIC ZZZZ9.                                       06600000
            04 FILLER PIC X(01) VALUE SPACES.                           06610000
            04 FILLER PIC X(10) VALUE SPACES.                           06620000
            04 FILLER PIC X(01) VALUE SPACES.                           06630000
            04 ZON59   PIC ZZZZ9.                                       06640000
            04 FILLER PIC X(01) VALUE SPACES.                           06650000
            04 FILLER PIC X(20) VALUE '          !         '.           06660000
            04 FILLER PIC X(16) VALUE SPACES.                           06670000
            04 FILLER PIC X(01) VALUE SPACES.                           06680000
      ***************************************************************** 06690000
          02 DETAIL27.                                                  06700000
            04 FILLER PIC X(20) VALUE '    !         !     '.           06710000
            04 FILLER PIC X(20) VALUE SPACES.                           06720000
            04 FILLER PIC X(20) VALUE SPACES.                           06730000
            04 FILLER PIC X(20) VALUE SPACES.                           06740000
            04 FILLER PIC X(20) VALUE SPACES.                           06750000
            04 FILLER PIC X(20) VALUE '     !              '.           06760000
            04 FILLER PIC X(11) VALUE SPACES.                           06770000
            04 FILLER PIC X(01) VALUE SPACES.                           06780000
      ***************************************************************** 06790000
          02 DETAIL28.                                                  06800000
            04 FILLER PIC X(20) VALUE '    !         ! CAPA'.           06810000
            04 FILLER PIC X(20) VALUE 'CITE NB PIECES   STO'.           06820000
            04 FILLER PIC X(14) VALUE 'CK           :'.                 06830000
            04 FILLER PIC X(01) VALUE SPACES.                           06840000
            04 ZON60   PIC ZZZZZZ9.                                     06850000
            04 FILLER PIC X(01) VALUE SPACES.                           06860000
            04 FILLER PIC X(8) VALUE SPACES.                            06870000
            04 FILLER PIC X(01) VALUE SPACES.                           06880000
            04 ZON61   PIC ZZZZZZ9.                                     06890000
            04 FILLER PIC X(01) VALUE SPACES.                           06900000
            04 FILLER PIC X(8) VALUE SPACES.                            06910000
            04 FILLER PIC X(01) VALUE SPACES.                           06920000
            04 ZON62   PIC ZZZZZZ9.                                     06930000
            04 FILLER PIC X(01) VALUE SPACES.                           06940000
            04 FILLER PIC X(20) VALUE '        !           '.           06950000
            04 FILLER PIC X(14) VALUE SPACES.                           06960000
            04 FILLER PIC X(01) VALUE SPACES.                           06970000
      ***************************************************************** 06980000
          02 DETAIL29.                                                  06990000
            04 FILLER PIC X(20) VALUE '    !         !     '.           07000000
            04 FILLER PIC X(20) VALUE '                 CAP'.           07010000
            04 FILLER PIC X(14) VALUE 'ACITE ATTRIB :'.                 07020000
            04 FILLER PIC X(01) VALUE SPACES.                           07030000
            04 ZON63   PIC ZZZZZZ9.                                     07040000
            04 FILLER PIC X(01) VALUE SPACES.                           07050000
            04 FILLER PIC X(08) VALUE SPACES.                           07060000
            04 FILLER PIC X(01) VALUE SPACES.                           07070000
            04 ZON64   PIC ZZZZZZ9.                                     07080000
            04 FILLER PIC X(01) VALUE SPACES.                           07090000
            04 FILLER PIC X(08) VALUE SPACES.                           07100000
            04 FILLER PIC X(01) VALUE SPACES.                           07110000
            04 ZON65   PIC ZZZZZZ9.                                     07120000
            04 FILLER PIC X(01) VALUE SPACES.                           07130000
            04 FILLER PIC X(20) VALUE '        !           '.           07140000
            04 FILLER PIC X(14) VALUE SPACES.                           07150000
            04 FILLER PIC X(01) VALUE SPACES.                           07160000
      ***************************************************************** 07170000
          02 DETAIL30.                                                  07180000
            04 FILLER PIC X(20) VALUE '    !         !     '.           07190000
            04 FILLER PIC X(20) VALUE SPACES.                           07200000
            04 FILLER PIC X(14) VALUE '      TOTALE :'.                 07210000
            04 FILLER PIC X(01) VALUE SPACES.                           07220000
            04 ZON66   PIC ZZZZZZ9.                                     07230000
            04 FILLER PIC X(01) VALUE SPACES.                           07240000
            04 FILLER PIC X(8) VALUE SPACES.                            07250000
            04 FILLER PIC X(01) VALUE SPACES.                           07260000
            04 ZON67   PIC ZZZZZZ9.                                     07270000
            04 FILLER PIC X(01) VALUE SPACES.                           07280000
            04 FILLER PIC X(8) VALUE SPACES.                            07290000
            04 FILLER PIC X(01) VALUE SPACES.                           07300000
            04 ZON68   PIC ZZZZZZ9.                                     07310000
            04 FILLER PIC X(01) VALUE SPACES.                           07320000
            04 FILLER PIC X(20) VALUE '        !           '.           07330000
            04 FILLER PIC X(14) VALUE SPACES.                           07340000
            04 FILLER PIC X(01) VALUE SPACES.                           07350000
      ***************************************************************** 07360000
          02 DETAIL31.                                                  07370000
            04 FILLER PIC X(20) VALUE '    !         !     '.           07380000
            04 FILLER PIC X(20) VALUE SPACES.                           07390000
            04 FILLER PIC X(20) VALUE SPACES.                           07400000
            04 FILLER PIC X(20) VALUE SPACES.                           07410000
            04 FILLER PIC X(20) VALUE SPACES.                           07420000
            04 FILLER PIC X(20) VALUE '     !              '.           07430000
            04 FILLER PIC X(11) VALUE SPACES.                           07440000
            04 FILLER PIC X(01) VALUE SPACES.                           07450000
      ***************************************************************** 07460000
          02 DETAIL32.                                                  07470000
            04 FILLER PIC X(20) VALUE '    !         ! CAPA'.           07480000
            04 FILLER PIC X(20) VALUE 'CITE NB M3       STO'.           07490000
            04 FILLER PIC X(14) VALUE 'CK           :'.                 07500000
            04 FILLER PIC X(01) VALUE SPACES.                           07510000
            04 ZON69   PIC ZZZZZZ9.                                     07520000
            04 FILLER PIC X(01) VALUE SPACES.                           07530000
            04 FILLER PIC X(8) VALUE SPACES.                            07540000
            04 FILLER PIC X(01) VALUE SPACES.                           07550000
            04 ZON70   PIC ZZZZZZ9.                                     07560000
            04 FILLER PIC X(01) VALUE SPACES.                           07570000
            04 FILLER PIC X(8) VALUE SPACES.                            07580000
            04 FILLER PIC X(01) VALUE SPACES.                           07590000
            04 ZON71   PIC ZZZZZZ9.                                     07600000
            04 FILLER PIC X(01) VALUE SPACES.                           07610000
            04 FILLER PIC X(20) VALUE '        !           '.           07620000
            04 FILLER PIC X(14) VALUE SPACES.                           07630000
            04 FILLER PIC X(01) VALUE SPACES.                           07640000
      ***************************************************************** 07650000
          02 DETAIL33.                                                  07660000
            04 FILLER PIC X(20) VALUE '    !         !     '.           07670000
            04 FILLER PIC X(20) VALUE '                 CAP'.           07680000
            04 FILLER PIC X(14) VALUE 'ACITE ATTRIB :'.                 07690000
            04 FILLER PIC X(01) VALUE SPACES.                           07700000
            04 ZON72   PIC ZZZZZZ9.                                     07710000
            04 FILLER PIC X(01) VALUE SPACES.                           07720000
            04 FILLER PIC X(8) VALUE SPACES.                            07730000
            04 FILLER PIC X(01) VALUE SPACES.                           07740000
            04 ZON73   PIC ZZZZZZ9.                                     07750000
            04 FILLER PIC X(01) VALUE SPACES.                           07760000
            04 FILLER PIC X(8) VALUE SPACES.                            07770000
            04 FILLER PIC X(01) VALUE SPACES.                           07780000
            04 ZON74   PIC ZZZZZZ9.                                     07790000
            04 FILLER PIC X(01) VALUE SPACES.                           07800000
            04 FILLER PIC X(20) VALUE '        !           '.           07810000
            04 FILLER PIC X(14) VALUE SPACES.                           07820000
            04 FILLER PIC X(01) VALUE SPACES.                           07830000
      ***************************************************************** 07840000
          02 DETAIL34.                                                  07850000
            04 FILLER PIC X(20) VALUE '    !         !     '.           07860000
            04 FILLER PIC X(20) VALUE SPACES.                           07870000
            04 FILLER PIC X(14) VALUE '      TOTALE :'.                 07880000
            04 FILLER PIC X(01) VALUE SPACES.                           07890000
            04 ZON75   PIC ZZZZZZ9.                                     07900000
            04 FILLER PIC X(01) VALUE SPACES.                           07910000
            04 FILLER PIC X(8) VALUE SPACES.                            07920000
            04 FILLER PIC X(01) VALUE SPACES.                           07930000
            04 ZON76   PIC ZZZZZZ9.                                     07940000
            04 FILLER PIC X(01) VALUE SPACES.                           07950000
            04 FILLER PIC X(8) VALUE SPACES.                            07960000
            04 FILLER PIC X(01) VALUE SPACES.                           07970000
            04 ZON77   PIC ZZZZZZ9.                                     07980000
            04 FILLER PIC X(01) VALUE SPACES.                           07990000
            04 FILLER PIC X(20) VALUE '        !           '.           08000000
            04 FILLER PIC X(14) VALUE SPACES.                           08010000
            04 FILLER PIC X(01) VALUE SPACES.                           08020000
      ***************************************************************** 08030000
          02 DETAIL35.                                                  08040000
            04 FILLER PIC X(20) VALUE '    !         !     '.           08050000
            04 FILLER PIC X(20) VALUE SPACES.                           08060000
            04 FILLER PIC X(20) VALUE SPACES.                           08070000
            04 FILLER PIC X(20) VALUE SPACES.                           08080000
            04 FILLER PIC X(20) VALUE SPACES.                           08090000
            04 FILLER PIC X(20) VALUE '     !              '.           08100000
            04 FILLER PIC X(11) VALUE SPACES.                           08110000
            04 FILLER PIC X(01) VALUE SPACES.                           08120000
      ***************************************************************** 08130000
          02 DETAIL36.                                                  08140000
            04 FILLER PIC X(20) VALUE '    !         ! RATI'.           08150000
            04 FILLER PIC X(20) VALUE 'OS OCCUPATION TOUS E'.           08160000
            04 FILLER PIC X(20) VALUE 'MPLNTS            MO'.           08170000
            04 FILLER PIC X(20) VALUE 'Y    MIN    1QU    M'.           08180000
            04 FILLER PIC X(20) VALUE 'ED    3QU    MAX    '.           08190000
            04 FILLER PIC X(20) VALUE '     !              '.           08200000
            04 FILLER PIC X(11) VALUE SPACES.                           08210000
            04 FILLER PIC X(01) VALUE SPACES.                           08220000
      ***************************************************************** 08230000
          02 DETAIL37.                                                  08240000
            04 FILLER PIC X(20) VALUE '    !         !     '.           08250000
            04 FILLER PIC X(20) VALUE '                   T'.           08260000
            04 FILLER PIC X(16) VALUE 'AUX OCCUPATION :'.               08270000
            04 FILLER PIC X(01) VALUE SPACES.                           08280000
            04 ZON78   PIC ZZZ9.                                        08290000
            04 FILLER PIC X(01) VALUE SPACES.                           08300000
            04 FILLER PIC X(1) VALUE SPACES.                            08310000
            04 FILLER PIC X(01) VALUE SPACES.                           08320000
            04 ZON79   PIC ZZZ9.                                        08330000
            04 FILLER PIC X(01) VALUE SPACES.                           08340000
            04 FILLER PIC X(1) VALUE SPACES.                            08350000
            04 FILLER PIC X(01) VALUE SPACES.                           08360000
            04 ZON80  PIC ZZZ9  BLANK WHEN ZERO.                        08370000
            04 FILLER PIC X(01) VALUE SPACES.                           08380000
            04 FILLER PIC X(1) VALUE SPACES.                            08390000
            04 FILLER PIC X(01) VALUE SPACES.                           08400000
            04 ZON81   PIC ZZZ9 BLANK WHEN ZERO.                        08410000
            04 FILLER PIC X(01) VALUE SPACES.                           08420000
            04 FILLER PIC X(1) VALUE SPACES.                            08430000
            04 FILLER PIC X(01) VALUE SPACES.                           08440000
            04 ZON82   PIC ZZZ9 BLANK WHEN ZERO.                        08450000
            04 FILLER PIC X(01) VALUE SPACES.                           08460000
            04 FILLER PIC X(1) VALUE SPACES.                            08470000
            04 FILLER PIC X(01) VALUE SPACES.                           08480000
            04 ZON83   PIC ZZZ9.                                        08490000
            04 FILLER PIC X(01) VALUE SPACES.                           08500000
            04 FILLER PIC X(20) VALUE '        !           '.           08510000
            04 FILLER PIC X(14) VALUE SPACES.                           08520000
            04 FILLER PIC X(01) VALUE SPACES.                           08530000
      ***************************************************************** 08540000
          02 DETAIL38.                                                  08550000
            04 FILLER PIC X(20) VALUE '    !         !     '.           08560000
            04 FILLER PIC X(20) VALUE '                   N'.           08570000
            04 FILLER PIC X(16) VALUE 'BP/EMPLACNT    :'.               08580000
            04 FILLER PIC X(01) VALUE SPACES.                           08590000
            04 ZON84   PIC ZZZ9.                                        08600000
            04 FILLER PIC X(01) VALUE SPACES.                           08610000
            04 FILLER PIC X(1) VALUE SPACES.                            08620000
            04 FILLER PIC X(01) VALUE SPACES.                           08630000
            04 ZON85   PIC ZZZ9.                                        08640000
            04 FILLER PIC X(01) VALUE SPACES.                           08650000
            04 FILLER PIC X(1) VALUE SPACES.                            08660000
            04 FILLER PIC X(01) VALUE SPACES.                           08670000
            04 ZON86   PIC ZZZ9  BLANK WHEN ZERO.                       08680000
            04 FILLER PIC X(01) VALUE SPACES.                           08690000
            04 FILLER PIC X(1) VALUE SPACES.                            08700000
            04 FILLER PIC X(01) VALUE SPACES.                           08710000
            04 ZON87   PIC ZZZ9 BLANK WHEN ZERO.                        08720000
            04 FILLER PIC X(01) VALUE SPACES.                           08730000
            04 FILLER PIC X(1) VALUE SPACES.                            08740000
            04 FILLER PIC X(01) VALUE SPACES.                           08750000
            04 ZON88   PIC ZZZ9 BLANK WHEN ZERO.                        08760000
            04 FILLER PIC X(01) VALUE SPACES.                           08770000
            04 FILLER PIC X(1) VALUE SPACES.                            08780000
            04 FILLER PIC X(01) VALUE SPACES.                           08790000
            04 ZON89   PIC ZZZ9.                                        08800000
            04 FILLER PIC X(01) VALUE SPACES.                           08810000
            04 FILLER PIC X(20) VALUE '        !           '.           08820000
            04 FILLER PIC X(14) VALUE SPACES.                           08830000
            04 FILLER PIC X(01) VALUE SPACES.                           08840000
      ***************************************************************** 08850000
          02 DETAIL39.                                                  08860000
            04 FILLER PIC X(20) VALUE '    !         !     '.           08870000
            04 FILLER PIC X(20) VALUE '                   N'.           08880000
            04 FILLER PIC X(16) VALUE 'BP/REFERENCE   :'.               08890000
            04 FILLER PIC X(01) VALUE SPACES.                           08900000
            04 ZON90   PIC ZZZ9.                                        08910000
            04 FILLER PIC X(01) VALUE SPACES.                           08920000
            04 FILLER PIC X(1) VALUE SPACES.                            08930000
            04 FILLER PIC X(01) VALUE SPACES.                           08940000
            04 ZON91   PIC ZZZ9.                                        08950000
            04 FILLER PIC X(01) VALUE SPACES.                           08960000
            04 FILLER PIC X(1) VALUE SPACES.                            08970000
            04 FILLER PIC X(01) VALUE SPACES.                           08980000
            04 ZON92   PIC ZZZ9 BLANK WHEN ZERO.                        08990000
            04 FILLER PIC X(01) VALUE SPACES.                           09000000
            04 FILLER PIC X(1) VALUE SPACES.                            09010000
            04 FILLER PIC X(01) VALUE SPACES.                           09020000
            04 ZON93   PIC ZZZ9 BLANK WHEN ZERO.                        09030000
            04 FILLER PIC X(01) VALUE SPACES.                           09040000
            04 FILLER PIC X(1) VALUE SPACES.                            09050000
            04 FILLER PIC X(01) VALUE SPACES.                           09060000
            04 ZON94   PIC ZZZ9 BLANK WHEN ZERO.                        09070000
            04 FILLER PIC X(01) VALUE SPACES.                           09080000
            04 FILLER PIC X(1) VALUE SPACES.                            09090000
            04 FILLER PIC X(01) VALUE SPACES.                           09100000
            04 ZON95   PIC ZZZ9.                                        09110000
            04 FILLER PIC X(01) VALUE SPACES.                           09120000
            04 FILLER PIC X(20) VALUE '        !           '.           09130000
            04 FILLER PIC X(14) VALUE SPACES.                           09140000
            04 FILLER PIC X(01) VALUE SPACES.                           09150000
      ***************************************************************** 09160000
          02 DETAIL40.                                                  09170000
            04 FILLER PIC X(20) VALUE '    !         !     '.           09180000
            04 FILLER PIC X(20) VALUE '                   N'.           09190000
            04 FILLER PIC X(16) VALUE 'BE/REFERENCE   :'.               09200000
            04 FILLER PIC X(01) VALUE SPACES.                           09210000
            04 ZON96   PIC ZZZ9.                                        09220000
            04 FILLER PIC X(01) VALUE SPACES.                           09230000
            04 FILLER PIC X(1) VALUE SPACES.                            09240000
            04 FILLER PIC X(01) VALUE SPACES.                           09250000
            04 ZON97   PIC ZZZ9.                                        09260000
            04 FILLER PIC X(01) VALUE SPACES.                           09270000
            04 FILLER PIC X(1) VALUE SPACES.                            09280000
            04 FILLER PIC X(01) VALUE SPACES.                           09290000
            04 ZON98   PIC ZZZ9 BLANK WHEN ZERO.                        09300000
            04 FILLER PIC X(01) VALUE SPACES.                           09310000
            04 FILLER PIC X(1) VALUE SPACES.                            09320000
            04 FILLER PIC X(01) VALUE SPACES.                           09330000
            04 ZON99   PIC ZZZ9 BLANK WHEN ZERO.                        09340000
            04 FILLER PIC X(01) VALUE SPACES.                           09350000
            04 FILLER PIC X(1) VALUE SPACES.                            09360000
            04 FILLER PIC X(01) VALUE SPACES.                           09370000
            04 ZON100  PIC ZZZ9 BLANK WHEN ZERO.                        09380000
            04 FILLER PIC X(01) VALUE SPACES.                           09390000
            04 FILLER PIC X(1) VALUE SPACES.                            09400000
            04 FILLER PIC X(01) VALUE SPACES.                           09410000
            04 ZON101  PIC ZZZ9.                                        09420000
            04 FILLER PIC X(01) VALUE SPACES.                           09430000
            04 FILLER PIC X(20) VALUE '        !           '.           09440000
            04 FILLER PIC X(14) VALUE SPACES.                           09450000
            04 FILLER PIC X(01) VALUE SPACES.                           09460000
      ***************************************************************** 09470000
          02 DETAIL41.                                                  09480000
            04 FILLER PIC X(20) VALUE '    !         !     '.           09490000
            04 FILLER PIC X(20) VALUE '                   N'.           09500000
            04 FILLER PIC X(16) VALUE 'B M3/EMPLACNT  :'.               09510000
            04 FILLER PIC X(01) VALUE SPACES.                           09520000
            04 ZON102  PIC ZZZ9.                                        09530000
            04 FILLER PIC X(01) VALUE SPACES.                           09540000
            04 FILLER PIC X(1) VALUE SPACES.                            09550000
            04 FILLER PIC X(01) VALUE SPACES.                           09560000
            04 ZON103  PIC ZZZ9.                                        09570000
            04 FILLER PIC X(01) VALUE SPACES.                           09580000
            04 FILLER PIC X(1) VALUE SPACES.                            09590000
            04 FILLER PIC X(01) VALUE SPACES.                           09600000
            04 ZON104  PIC ZZZ9 BLANK WHEN ZERO.                        09610000
            04 FILLER PIC X(01) VALUE SPACES.                           09620000
            04 FILLER PIC X(1) VALUE SPACES.                            09630000
            04 FILLER PIC X(01) VALUE SPACES.                           09640000
            04 ZON105  PIC ZZZ9 BLANK WHEN ZERO.                        09650000
            04 FILLER PIC X(01) VALUE SPACES.                           09660000
            04 FILLER PIC X(1) VALUE SPACES.                            09670000
            04 FILLER PIC X(01) VALUE SPACES.                           09680000
            04 ZON106  PIC ZZZ9 BLANK WHEN ZERO.                        09690000
            04 FILLER PIC X(01) VALUE SPACES.                           09700000
            04 FILLER PIC X(1) VALUE SPACES.                            09710000
            04 FILLER PIC X(01) VALUE SPACES.                           09720000
            04 ZON107  PIC ZZZ9.                                        09730000
            04 FILLER PIC X(01) VALUE SPACES.                           09740000
            04 FILLER PIC X(20) VALUE '        !           '.           09750000
            04 FILLER PIC X(14) VALUE SPACES.                           09760000
            04 FILLER PIC X(01) VALUE SPACES.                           09770000
      ***************************************************************** 09780000
          02 DETAIL42.                                                  09790000
            04 FILLER PIC X(20) VALUE '    !         !     '.           09800000
            04 FILLER PIC X(20) VALUE '                   N'.           09810000
            04 FILLER PIC X(16) VALUE 'B M3/REFERENCE :'.               09820000
            04 FILLER PIC X(01) VALUE SPACES.                           09830000
            04 ZON108  PIC ZZZ9.                                        09840000
            04 FILLER PIC X(01) VALUE SPACES.                           09850000
            04 FILLER PIC X(1) VALUE SPACES.                            09860000
            04 FILLER PIC X(01) VALUE SPACES.                           09870000
            04 ZON109  PIC ZZZ9.                                        09880000
            04 FILLER PIC X(01) VALUE SPACES.                           09890000
            04 FILLER PIC X(1) VALUE SPACES.                            09900000
            04 FILLER PIC X(01) VALUE SPACES.                           09910000
            04 ZON110  PIC ZZZ9 BLANK WHEN ZERO.                        09920000
            04 FILLER PIC X(01) VALUE SPACES.                           09930000
            04 FILLER PIC X(1) VALUE SPACES.                            09940000
            04 FILLER PIC X(01) VALUE SPACES.                           09950000
            04 ZON111  PIC ZZZ9 BLANK WHEN ZERO.                        09960000
            04 FILLER PIC X(01) VALUE SPACES.                           09970000
            04 FILLER PIC X(1) VALUE SPACES.                            09980000
            04 FILLER PIC X(01) VALUE SPACES.                           09990000
            04 ZON112  PIC ZZZ9 BLANK WHEN ZERO.                        10000000
            04 FILLER PIC X(01) VALUE SPACES.                           10010000
            04 FILLER PIC X(1) VALUE SPACES.                            10020000
            04 FILLER PIC X(01) VALUE SPACES.                           10030000
            04 ZON113  PIC ZZZ9.                                        10040000
            04 FILLER PIC X(01) VALUE SPACES.                           10050000
            04 FILLER PIC X(20) VALUE '        !           '.           10060000
            04 FILLER PIC X(14) VALUE SPACES.                           10070000
            04 FILLER PIC X(01) VALUE SPACES.                           10080000
      ***************************************************************** 10090000
          02 HENTET43.                                                  10100000
            04 FILLER PIC X(20) VALUE '    ----------------'.           10110000
            04 FILLER PIC X(20) VALUE '--------------------'.           10120000
            04 FILLER PIC X(20) VALUE '--------------------'.           10130000
            04 FILLER PIC X(20) VALUE '--------------------'.           10140000
            04 FILLER PIC X(20) VALUE '--------------------'.           10150000
            04 FILLER PIC X(20) VALUE '------              '.           10160000
            04 FILLER PIC X(11) VALUE SPACES.                           10170000
            04 FILLER PIC X(01) VALUE SPACES.                           10180000
      ***************************************************************** 10190000
          02 DETAIL44.                                                  10200000
            04 FILLER PIC X(20) VALUE '    !         !     '.           10210000
            04 FILLER PIC X(20) VALUE SPACES.                           10220000
            04 FILLER PIC X(20) VALUE SPACES.                           10230000
            04 FILLER PIC X(20) VALUE SPACES.                           10240000
            04 FILLER PIC X(20) VALUE SPACES.                           10250000
            04 FILLER PIC X(20) VALUE '     !              '.           10260000
            04 FILLER PIC X(11) VALUE SPACES.                           10270000
            04 FILLER PIC X(01) VALUE SPACES.                           10280000
      ***************************************************************** 10290000
          02 DETAIL45.                                                  10300000
            04 FILLER PIC X(20) VALUE '    ! VRAC    ! NOMB'.           10310000
            04 FILLER PIC X(20) VALUE 'RE D''EMPLACEMENTS  T'.          10320000
            04 FILLER PIC X(16) VALUE 'OTAL           :'.               10330000
            04 FILLER PIC X(01) VALUE SPACES.                           10340000
            04 ZON114  PIC ZZZZ9.                                       10350000
            04 FILLER PIC X(01) VALUE SPACES.                           10360000
            04 FILLER PIC X(20) VALUE SPACES.                           10370000
            04 FILLER PIC X(20) VALUE SPACES.                           10380000
            04 FILLER PIC X(20) VALUE '  !                 '.           10390000
            04 FILLER PIC X(8) VALUE SPACES.                            10400000
            04 FILLER PIC X(01) VALUE SPACES.                           10410000
      ***************************************************************** 10420000
          02 DETAIL46.                                                  10430000
            04 FILLER PIC X(20) VALUE '    !         !     '.           10440000
            04 FILLER PIC X(20) VALUE '                   A'.           10450000
            04 FILLER PIC X(16) VALUE 'TTRIBUE        :'.               10460000
            04 FILLER PIC X(01) VALUE SPACES.                           10470000
            04 ZON115  PIC ZZZZ9.                                       10480000
            04 FILLER PIC X(01) VALUE SPACES.                           10490000
            04 ZON116 PIC ZZ9,99.                                       10500000
            04 FILLER PIC X(01) VALUE SPACES.                           10510000
            04 FILLER PIC X(20) VALUE '%                   '.           10520000
            04 FILLER PIC X(20) VALUE '               !    '.           10530000
            04 FILLER PIC X(20) VALUE SPACES.                           10540000
            04 FILLER PIC X(1) VALUE SPACES.                            10550000
            04 FILLER PIC X(01) VALUE SPACES.                           10560000
      ***************************************************************** 10570000
          02 DETAIL47.                                                  10580000
            04 FILLER PIC X(20) VALUE '    !         !     '.           10590000
            04 FILLER PIC X(20) VALUE '                   D'.           10600000
            04 FILLER PIC X(16) VALUE 'ISPONIBLE      :'.               10610000
            04 FILLER PIC X(01) VALUE SPACES.                           10620000
            04 ZON117  PIC ZZZZ9.                                       10630000
            04 FILLER PIC X(01) VALUE SPACES.                           10640000
            04 ZON118 PIC ZZ9,99.                                       10650000
            04 FILLER PIC X(01) VALUE SPACES.                           10660000
            04 FILLER PIC X(20) VALUE '%                   '.           10670000
            04 FILLER PIC X(20) VALUE '               !    '.           10680000
            04 FILLER PIC X(20) VALUE SPACES.                           10690000
            04 FILLER PIC X(1) VALUE SPACES.                            10700000
            04 FILLER PIC X(01) VALUE SPACES.                           10710000
      ***************************************************************** 10720000
          02 DETAIL48.                                                  10730000
            04 FILLER PIC X(20) VALUE '    !         !     '.           10740000
            04 FILLER PIC X(20) VALUE SPACES.                           10750000
            04 FILLER PIC X(20) VALUE SPACES.                           10760000
            04 FILLER PIC X(20) VALUE SPACES.                           10770000
            04 FILLER PIC X(20) VALUE SPACES.                           10780000
            04 FILLER PIC X(20) VALUE '     !              '.           10790000
            04 FILLER PIC X(11) VALUE SPACES.                           10800000
            04 FILLER PIC X(01) VALUE SPACES.                           10810000
      ***************************************************************** 10820000
          02 DETAIL49.                                                  10830000
            04 FILLER PIC X(20) VALUE '    !         ! RATI'.           10840000
            04 FILLER PIC X(20) VALUE 'OS OCCUPATION      N'.           10850000
            04 FILLER PIC X(16) VALUE 'B REFERENCES   :'.               10860000
            04 FILLER PIC X(01) VALUE SPACES.                           10870000
            04 ZON119  PIC ZZZZ9.                                       10880000
            04 FILLER PIC X(01) VALUE SPACES.                           10890000
            04 FILLER PIC X(20) VALUE SPACES.                           10900000
            04 FILLER PIC X(20) VALUE SPACES.                           10910000
            04 FILLER PIC X(20) VALUE '  !                 '.           10920000
            04 FILLER PIC X(8) VALUE SPACES.                            10930000
            04 FILLER PIC X(01) VALUE SPACES.                           10940000
      ***************************************************************** 10950000
          02 DETAIL50.                                                  10960000
            04 FILLER PIC X(20) VALUE '    !         !     '.           10970000
            04 FILLER PIC X(20) VALUE '                   S'.           10980000
            04 FILLER PIC X(16) VALUE 'TOCK           :'.               10990000
            04 FILLER PIC X(01) VALUE SPACES.                           11000000
            04 ZON120  PIC ZZZZZ9.                                      11010000
            04 FILLER PIC X(01) VALUE SPACES.                           11020000
            04 FILLER PIC X(20) VALUE SPACES.                           11030000
            04 FILLER PIC X(20) VALUE SPACES.                           11040000
            04 FILLER PIC X(20) VALUE ' !                  '.           11050000
            04 FILLER PIC X(7) VALUE SPACES.                            11060000
            04 FILLER PIC X(01) VALUE SPACES.                           11070000
      ***************************************************************** 11080000
          02 DETAIL51.                                                  11090000
            04 FILLER PIC X(20) VALUE '    !         !     '.           11100000
            04 FILLER PIC X(20) VALUE SPACES.                           11110000
            04 FILLER PIC X(20) VALUE SPACES.                           11120000
            04 FILLER PIC X(20) VALUE SPACES.                           11130000
            04 FILLER PIC X(20) VALUE SPACES.                           11140000
            04 FILLER PIC X(20) VALUE '     !              '.           11150000
            04 FILLER PIC X(11) VALUE SPACES.                           11160000
            04 FILLER PIC X(01) VALUE SPACES.                           11170000
      ***************************************************************** 11180000
          02 DETAIL52.                                                  11190000
            04 FILLER PIC X(20) VALUE '    !         !     '.           11200000
            04 FILLER PIC X(20) VALUE SPACES.                           11210000
            04 FILLER PIC X(20) VALUE '                  MO'.           11220000
            04 FILLER PIC X(20) VALUE 'Y    MIN    1QU    M'.           11230000
            04 FILLER PIC X(20) VALUE 'ED    3QU    MAX    '.           11240000
            04 FILLER PIC X(20) VALUE '     !              '.           11250000
            04 FILLER PIC X(11) VALUE SPACES.                           11260000
            04 FILLER PIC X(01) VALUE SPACES.                           11270000
      ***************************************************************** 11280000
          02 DETAIL53.                                                  11290000
            04 FILLER PIC X(20) VALUE '    !         !     '.           11300000
            04 FILLER PIC X(20) VALUE '                   N'.           11310000
            04 FILLER PIC X(16) VALUE 'BP/EMPLACNT    :'.               11320000
            04 FILLER PIC X(01) VALUE SPACES.                           11330000
            04 ZON121  PIC ZZZ9.                                        11340000
            04 FILLER PIC X(01) VALUE SPACES.                           11350000
            04 FILLER PIC X(1) VALUE SPACES.                            11360000
            04 FILLER PIC X(01) VALUE SPACES.                           11370000
            04 ZON122  PIC ZZZ9.                                        11380000
            04 FILLER PIC X(01) VALUE SPACES.                           11390000
            04 FILLER PIC X(1) VALUE SPACES.                            11400000
            04 FILLER PIC X(01) VALUE SPACES.                           11410000
            04 ZON123  PIC ZZZ9 BLANK WHEN ZERO.                        11420000
            04 FILLER PIC X(01) VALUE SPACES.                           11430000
            04 FILLER PIC X(1) VALUE SPACES.                            11440000
            04 FILLER PIC X(01) VALUE SPACES.                           11450000
            04 ZON124  PIC ZZZ9 BLANK WHEN ZERO.                        11460000
            04 FILLER PIC X(01) VALUE SPACES.                           11470000
            04 FILLER PIC X(1) VALUE SPACES.                            11480000
            04 FILLER PIC X(01) VALUE SPACES.                           11490000
            04 ZON125  PIC ZZZ9 BLANK WHEN ZERO.                        11500000
            04 FILLER PIC X(01) VALUE SPACES.                           11510000
            04 FILLER PIC X(1) VALUE SPACES.                            11520000
            04 FILLER PIC X(01) VALUE SPACES.                           11530000
            04 ZON126  PIC ZZZ9.                                        11540000
            04 FILLER PIC X(01) VALUE SPACES.                           11550000
            04 FILLER PIC X(20) VALUE '        !           '.           11560000
            04 FILLER PIC X(14) VALUE SPACES.                           11570000
            04 FILLER PIC X(01) VALUE SPACES.                           11580000
      ***************************************************************** 11590000
          02 DETAIL54.                                                  11600000
            04 FILLER PIC X(20) VALUE '    !         !     '.           11610000
            04 FILLER PIC X(20) VALUE '                   N'.           11620000
            04 FILLER PIC X(16) VALUE 'BP/REFERENCE   :'.               11630000
            04 FILLER PIC X(01) VALUE SPACES.                           11640000
            04 ZON127  PIC ZZZ9.                                        11650000
            04 FILLER PIC X(01) VALUE SPACES.                           11660000
            04 FILLER PIC X(1) VALUE SPACES.                            11670000
            04 FILLER PIC X(01) VALUE SPACES.                           11680000
            04 ZON128  PIC ZZZ9.                                        11690000
            04 FILLER PIC X(01) VALUE SPACES.                           11700000
            04 FILLER PIC X(1) VALUE SPACES.                            11710000
            04 FILLER PIC X(01) VALUE SPACES.                           11720000
            04 ZON129  PIC ZZZ9 BLANK WHEN ZERO.                        11730000
            04 FILLER PIC X(01) VALUE SPACES.                           11740000
            04 FILLER PIC X(1) VALUE SPACES.                            11750000
            04 FILLER PIC X(01) VALUE SPACES.                           11760000
            04 ZON130  PIC ZZZ9 BLANK WHEN ZERO.                        11770000
            04 FILLER PIC X(01) VALUE SPACES.                           11780000
            04 FILLER PIC X(1) VALUE SPACES.                            11790000
            04 FILLER PIC X(01) VALUE SPACES.                           11800000
            04 ZON131 PIC ZZZ9 BLANK WHEN ZERO.                         11810000
            04 FILLER PIC X(01) VALUE SPACES.                           11820000
            04 FILLER PIC X(1) VALUE SPACES.                            11830000
            04 FILLER PIC X(01) VALUE SPACES.                           11840000
            04 ZON132  PIC ZZZ9.                                        11850000
            04 FILLER PIC X(01) VALUE SPACES.                           11860000
            04 FILLER PIC X(20) VALUE '        !           '.           11870000
            04 FILLER PIC X(14) VALUE SPACES.                           11880000
            04 FILLER PIC X(01) VALUE SPACES.                           11890000
      ***************************************************************** 11900000
          02 DETAIL55.                                                  11910000
            04 FILLER PIC X(20) VALUE '    !         !     '.           11920000
            04 FILLER PIC X(20) VALUE '                   N'.           11930000
            04 FILLER PIC X(16) VALUE 'BE/REFERENCE   :'.               11940000
            04 FILLER PIC X(01) VALUE SPACES.                           11950000
            04 ZON133  PIC ZZZ9.                                        11960000
            04 FILLER PIC X(01) VALUE SPACES.                           11970000
            04 FILLER PIC X(1) VALUE SPACES.                            11980000
            04 FILLER PIC X(01) VALUE SPACES.                           11990000
            04 ZON134  PIC ZZZ9.                                        12000000
            04 FILLER PIC X(01) VALUE SPACES.                           12010000
            04 FILLER PIC X(1) VALUE SPACES.                            12020000
            04 FILLER PIC X(01) VALUE SPACES.                           12030000
            04 ZON135  PIC ZZZ9 BLANK WHEN ZERO.                        12040000
            04 FILLER PIC X(01) VALUE SPACES.                           12050000
            04 FILLER PIC X(1) VALUE SPACES.                            12060000
            04 FILLER PIC X(01) VALUE SPACES.                           12070000
            04 ZON136  PIC ZZZ9 BLANK WHEN ZERO.                        12080000
            04 FILLER PIC X(01) VALUE SPACES.                           12090000
            04 FILLER PIC X(1) VALUE SPACES.                            12100000
            04 FILLER PIC X(01) VALUE SPACES.                           12110000
            04 ZON137  PIC ZZZ9 BLANK WHEN ZERO.                        12120000
            04 FILLER PIC X(01) VALUE SPACES.                           12130000
            04 FILLER PIC X(1) VALUE SPACES.                            12140000
            04 FILLER PIC X(01) VALUE SPACES.                           12150000
            04 ZON138  PIC ZZZ9.                                        12160000
            04 FILLER PIC X(01) VALUE SPACES.                           12170000
            04 FILLER PIC X(20) VALUE '        !           '.           12180000
            04 FILLER PIC X(14) VALUE SPACES.                           12190000
            04 FILLER PIC X(01) VALUE SPACES.                           12200000
      ***************************************************************** 12210000
          02 DETAIL56.                                                  12220000
            04 FILLER PIC X(20) VALUE '    !         !     '.           12230000
            04 FILLER PIC X(20) VALUE SPACES.                           12240000
            04 FILLER PIC X(20) VALUE SPACES.                           12250000
            04 FILLER PIC X(20) VALUE SPACES.                           12260000
            04 FILLER PIC X(20) VALUE SPACES.                           12270000
            04 FILLER PIC X(20) VALUE '     !              '.           12280000
            04 FILLER PIC X(11) VALUE SPACES.                           12290000
            04 FILLER PIC X(01) VALUE SPACES.                           12300000
      ***************************************************************** 12310000
          02 HENTET57.                                                  12320000
            04 FILLER PIC X(20) VALUE '    ----------------'.           12330000
            04 FILLER PIC X(20) VALUE '--------------------'.           12340000
            04 FILLER PIC X(20) VALUE '--------------------'.           12350000
            04 FILLER PIC X(20) VALUE '--------------------'.           12360000
            04 FILLER PIC X(20) VALUE '--------------------'.           12370000
            04 FILLER PIC X(20) VALUE '------              '.           12380000
            04 FILLER PIC X(11) VALUE SPACES.                           12390000
            04 FILLER PIC X(01) VALUE SPACES.                           12400000
      *                                                                 12410000
       PROCEDURE DIVISION.                                              12420000
      *                                                                 12430000
      *                                                                 12440000
      ***************************************************************** 12450000
      *              T R A M E   DU   P R O G R A M M E               * 12460000
      ***************************************************************** 12470000
      *                                                                 12480000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 12490000
      *                                                               * 12500000
      *           ------------------------------------------          * 12510000
      *           --MODULE DE BASE DU PROGRAMME  BGE002   --          * 12520000
      *           ------------------------------------------          * 12530000
      *                               I                               * 12540000
      *      ----------------------------------------------           * 12550000
      *      I                        I                   I           * 12560000
      * -------------         -----------------     ---------------   * 12570000
      * - DEBUT-TRT -         -  TRAITEMENT   -     -   FIN-TRT   -   * 12580000
      * -------------         -----------------     ---------------   * 12590000
      *                                                               * 12600000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 12610000
      *                                                                 12620000
           PERFORM DEBUT-TRT   THRU FIN-DEBUT-TRT.                      12630000
           PERFORM TRAITEMENT  THRU FIN-TRAITEMENT                      12640000
                                   UNTIL FIN-FICHIER OR NOT-OK.         12650000
           PERFORM FIN-TRT  THRU FIN-FIN-TRT.                           12660000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *            STOP RUN.                                            12670000
      *--                                                                       
                   EXIT PROGRAM.                                                
      *}                                                                        
               EJECT                                                    12680000
      *                                                                 12690000
      *                                                                 12700000
      ***************************************************************** 12710000
      *              DEBUT       DU      TRAITEMENT                   * 12720000
      ***************************************************************** 12730000
       DEBUT-TRT.                                                       12740000
                                                                        12750000
           DISPLAY '************************'                           12760000
           DISPLAY '    PROGRAMME BGE002    '                           12770000
           DISPLAY '************************'                           12780000
           DISPLAY '      DEBUT NORMAL      '                           12790000
           DISPLAY '************************'                           12800000
                                                                        12810000
           OPEN INPUT FENTREE OUTPUT IMPR.                              12820000
           ACCEPT W-DATE9 FROM  DATE.                                   12830000
           MOVE   W-JJ9   TO W-JJ.                                      12840000
           MOVE   W-MM9   TO W-MM.                                      12850000
           MOVE   W-AA9   TO W-AA.                                      12860000
                                                                        12870000
           READ FENTREE INTO W-ENR                                      12880000
                    AT END SET FIN-FICHIER TO TRUE.                     12890000
      *----> 1ERE LECTURE                                               12900000
      *---->   ==>  ON A UN ENR TYPE 10 OU 20 OU 30                     12910000
           IF NOT FIN-FICHIER                                           12911000
              ADD 1 TO CTRLEC                                           12920000
           END-IF.                                                      12921000
                                                                        12930000
       FIN-DEBUT-TRT. EXIT.                                             12940000
               EJECT                                                    12950000
      *                                                                 12960000
      *                                                                 12970000
      ***************************************************************** 12980000
      *                   TRAITEMENT    PRINCIPAL                     * 12990000
      ***************************************************************** 13000000
       TRAITEMENT.                                                      13010000
                                                                        13020000
           PERFORM TRT-ENTETES THRU FIN-TRT-ENTETES.                    13030000
                                                                        13040000
           IF OK                                                        13050000
              PERFORM TRT-SOL  THRU FIN-TRT-SOL.                        13060000
                                                                        13070000
           IF OK                                                        13080000
              PERFORM TRT-RACK THRU FIN-TRT-RACK.                       13090000
                                                                        13100000
           IF OK AND ENC-FICHIER                                        13110000
              PERFORM TRT-VRAC THRU FIN-TRT-VRAC.                       13120000
                                                                        13130000
                                                                        13140000
           IF ENC-FICHIER AND LECT-A-FAIRE AND OK                       13150000
               READ FENTREE INTO W-ENR                                  13160000
                    AT END SET FIN-FICHIER TO TRUE                      13170000
      *------> ON A UN ENR TYPE 10 OU 20 OU 30                          13180000
               ADD 1 TO CTRLEC                                          13190000
           END-IF.                                                      13200000
                                                                        13210000
       FIN-TRAITEMENT. EXIT.                                            13220000
               EJECT                                                    13230000
      *                                                                 13240000
      ***************************************************************** 13250000
      *              FIN         DU      TRAITEMENT                   * 13260000
      ***************************************************************** 13270000
       FIN-TRT.                                                         13280000
           CLOSE FENTREE IMPR.                                          13290000
           IF OK                                                        13300000
                 DISPLAY '************************'                     13310000
                 DISPLAY ' NOMBRE D''ENR LUS    :  ' CTRLEC             13320000
                 DISPLAY ' NOMBRE D''ENR ECRITS :  ' CTRECR             13320100
                 DISPLAY '************************'                     13320200
                 DISPLAY '       FIN NORMALE      '                     13320300
                 DISPLAY '************************'                     13320400
           ELSE                                                         13320500
                 DISPLAY '************************'                     13320600
                 DISPLAY '      FIN ANORMALE      '                     13320700
                 DISPLAY '*********************** '                     13320800
           END-IF.                                                      13320900
                                                                        13321000
       FIN-FIN-TRT. EXIT.                                               13322000
               EJECT                                                    13323000
      *                                                                 13324000
      ***************************************************************** 13325000
      *              TRT       ENTETES                                * 13326000
      ***************************************************************** 13327000
       TRT-ENTETES.                                                     13328000
                                                                        13329000
            MOVE ENR-ENTREPOT    TO   ZON1.                             13330000
            ADD 1 TO CTR-PAGE MOVE CTR-PAGE TO ZON2.                    13340000
            MOVE W-DATE       TO  ZON3.                                 13350000
            WRITE LIGNE      FROM  HENTET01                             13360000
                   AFTER ADVANCING SAUT                                 13370000
            WRITE LIGNE      FROM  HENTET02                             13380000
            WRITE LIGNE      FROM  HENTET03                             13390000
            WRITE LIGNE      FROM  HENTET04                             13400000
            WRITE LIGNE      FROM  HENTET05                             13410000
            WRITE LIGNE      FROM  HENTET06.                            13420000
            ADD 6 TO CTRECR.                                            13430000
                                                                        13440000
       FIN-TRT-ENTETES. EXIT.                                           13450000
      ***************************************************************** 13460000
      ***************************************************************** 13470000
      ***************************************************************** 13480000
      *                                                               * 13490000
      *              TRT       SOL                                    * 13500000
      *                                                               * 13510000
      ***************************************************************** 13520000
      ***************************************************************** 13530000
      ***************************************************************** 13540000
       TRT-SOL.                                                         13550000
                                                                        13560000
           IF ENR-TYPE = '10'                                           13570000
              PERFORM TRT-10  THRU FIN-TRT-10                           13580000
              IF OK                                                     13590000
                 PERFORM TRT-11 THRU FIN-TRT-11                         13600000
              END-IF                                                    13610000
              IF OK                                                     13620000
                 PERFORM TRT-12 THRU FIN-TRT-12                         13630000
              END-IF                                                    13640000
              IF OK                                                     13650000
                 PERFORM TRT-13 THRU FIN-TRT-13                         13660000
              END-IF                                                    13670000
              IF OK                                                     13680000
                 PERFORM TRT-14 THRU FIN-TRT-14                         13690000
                 SET LECT-A-FAIRE TO TRUE                               13700000
              END-IF                                                    13710000
           ELSE                                                         13720000
              SET LECT-DEJA-FAITE TO TRUE                               13730000
           END-IF.                                                      13740000
                                                                        13750000
       FIN-TRT-SOL. EXIT.                                               13760000
      ***************************************************************** 13770000
      *              TRT       10                                     * 13780000
      ***************************************************************** 13790000
       TRT-10.                                                          13800000
                                                                        13810000
           MOVE CTR-SURF-TOT      TO ZON4                               13820000
           MOVE CTR-SURF-ATT      TO ZON5                               13830000
           COMPUTE CTR6 ROUNDED = (CTR-SURF-ATT * 100) / CTR-SURF-TOT   13840000
           MOVE CTR6              TO ZON6                               13850000
           COMPUTE CTR7 = (CTR-SURF-TOT - CTR-SURF-ATT)                 13860000
           MOVE CTR7              TO ZON7                               13870000
           COMPUTE CTR6 ROUNDED = (CTR7 * 100) / CTR-SURF-TOT           13880000
           MOVE CTR6              TO ZON8.                              13890000
                                                                        13900000
           MOVE CTR-NBEMPL-TOT      TO ZON9                             13910000
           MOVE CTR-NBEMPL-ATT      TO ZON10 CTR10                      13920000
           COMPUTE CTR6 ROUNDED = (CTR-NBEMPL-ATT * 100)                13930000
                                    / CTR-NBEMPL-TOT                    13940000
           MOVE CTR6              TO ZON11                              13950000
           COMPUTE CTR7 = (CTR-NBEMPL-TOT - CTR-NBEMPL-ATT)             13960000
           MOVE CTR7              TO ZON12                              13970000
           COMPUTE CTR6 ROUNDED = (CTR7 * 100) / CTR-NBEMPL-TOT         13980000
           MOVE CTR6              TO ZON13.                             13990000
                                                                        14000000
           MOVE CTR-NB-REF      TO ZON14 CTR14                          14010000
           MOVE CTR-STOCK       TO ZON15                                14020000
           MOVE CTR-CAP-ATT     TO ZON16                                14030000
           MOVE CTR-CAP-TOT     TO ZON17.                               14040000
                                                                        14050000
           WRITE LIGNE      FROM  DETAIL01                              14060000
           WRITE LIGNE      FROM  DETAIL02                              14070000
           WRITE LIGNE      FROM  DETAIL03                              14080000
           WRITE LIGNE      FROM  DETAIL04                              14090000
           WRITE LIGNE      FROM  DETAIL05                              14100000
           WRITE LIGNE      FROM  DETAIL06                              14110000
           WRITE LIGNE      FROM  DETAIL07                              14120000
           WRITE LIGNE      FROM  DETAIL08                              14130000
           WRITE LIGNE      FROM  DETAIL09                              14140000
           WRITE LIGNE      FROM  DETAIL10                              14150000
           WRITE LIGNE      FROM  DETAIL11                              14160000
           WRITE LIGNE      FROM  DETAIL12                              14170000
           WRITE LIGNE      FROM  DETAIL13.                             14180000
           ADD 13 TO CTRECR.                                            14190000
                                                                        14200000
       FIN-TRT-10. EXIT.                                                14210000
      ***************************************************************** 14220000
      *              TRT       11                                     * 14230000
      ***************************************************************** 14240000
       TRT-11.                                                          14250000
                                                                        14260000
           READ FENTREE INTO W-ENR                                      14270000
      *--> LECTURE DU 1ER ENR DE TYPE 11 ( POUR L'ENTREPOT EN COURS )   14280000
      *--> SI NON-TROUVE ==> ARRET DU PG                                14290000
                    AT END SET FIN-FICHIER TO TRUE                      14300000
                           SET NOT-OK TO TRUE                           14310000
                           DISPLAY ' PAS D''ENR TYPE 11 12 13 ET 14 '   14320000
                           GO TO FIN-TRT-11.                            14330000
           ADD 1 TO CTRLEC.                                             14340000
           IF ENR-TYPE NOT = '11'                                       14350000
                SET NOT-OK TO TRUE                                      14360000
                DISPLAY ' PAS D''ENR TYPE 11 '                          14370000
                GO TO FIN-TRT-11.                                       14380000
                                                                        14390000
           MOVE ENR-ZON-RATIO  TO  ZON19.                               14400000
           MOVE ZERO           TO  CUMUL-MOY.                           14410000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           14420000
                                                                        14430000
      *--> QUOTIENT = NBEMPL-ATT / QUARTILE                             14440000
           DIVIDE CTR10 BY 4 GIVING QUOTIENT                            14450000
                                                                        14460000
           MOVE     QUOT-5  TO    W1QUART                               14470000
           COMPUTE  WMED     =    W1QUART * 2                           14480000
           COMPUTE  W3QUART  =    W1QUART * 3                           14490000
                                                                        14500000
           MOVE ZERO TO ZON20                                           14510000
                        ZON21                                           14520000
                        ZON22                                           14530000
           IF QUOT-5 < 2                                                14540000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR10            14550000
                 READ FENTREE INTO W-ENR                                14560000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        14570000
                 ADD 1 TO CTRLEC                                        14580000
              END-PERFORM                                               14590000
           ELSE                                                         14600000
              IF QUOT-2  = ZERO                                         14610000
      *----------> SI CTR10 EST MULTIPLE DE 4                           14620000
                  PERFORM TRT-11-A THRU FIN-TRT-11-A                    14630000
                               VARYING I FROM 2 BY 1                    14640000
                                          UNTIL I > CTR10               14650000
              ELSE                                                      14660000
                  PERFORM TRT-11-B THRU FIN-TRT-11-B                    14670000
                               VARYING I FROM 2 BY 1                    14680000
                                          UNTIL I > CTR10.              14690000
                                                                        14700000
           MOVE  ENR-ZON-RATIO TO ZON23                                 14710000
           COMPUTE CTR7 = CUMUL-MOY / CTR10                             14720000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      14730000
           MOVE    CTR7 TO ZON18.                                       14740000
           WRITE LIGNE FROM DETAIL14.                                   14750000
           WRITE LIGNE FROM DETAIL15.                                   14760000
           ADD  2 TO CTRECR.                                            14770000
                                                                        14780000
       FIN-TRT-11. EXIT.                                                14790000
      ***************************************************************** 14800000
      *              TRT       11 A                                   * 14810000
      ***************************************************************** 14820000
       TRT-11-A.                                                        14830000
                                                                        14840000
           READ FENTREE INTO W-ENR                                      14850000
      *--> LECTURE SUIVANTE ENR DE TYPE 11                              14860000
      *--> SI NON-TROUVE ==> ARRET DU PG                                14870000
                    AT END SET FIN-FICHIER TO TRUE                      14880000
                           SET NOT-OK TO TRUE                           14890000
                           DISPLAY ' ERREUR LECT ENR TYPE 11 '          14900000
                           GO TO FIN-TRT-11-A.                          14910000
           ADD 1 TO CTRLEC.                                             14920000
           IF ENR-TYPE NOT = '11'                                       14930000
                SET NOT-OK TO TRUE                                      14940000
                DISPLAY ' PAS D''ENR TYPE 11 '                          14950000
                GO TO FIN-TRT-11-A.                                     14960000
                                                                        14970000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             14980000
                                                                        14990000
           IF   I = W1QUART                                             15000000
              MOVE  ENR-ZON-RATIO TO ZON20                              15010000
           ELSE                                                         15020000
              IF   I  = WMED                                            15030000
                 MOVE  ENR-ZON-RATIO TO ZON21                           15040000
              ELSE                                                      15050000
                 IF   I  = W3QUART                                      15060000
                     MOVE  ENR-ZON-RATIO TO ZON22.                      15070000
                                                                        15080000
       FIN-TRT-11-A. EXIT.                                              15090000
      ***************************************************************** 15100000
      *              TRT       11 B                                   * 15110000
      ***************************************************************** 15120000
       TRT-11-B.                                                        15130000
                                                                        15140000
           READ FENTREE INTO W-ENR                                      15150000
      *--> LECTURE SUIVANTE ENR DE TYPE 11                              15160000
      *--> SI NON-TROUVE ==> ARRET DU PG                                15170000
                    AT END SET FIN-FICHIER TO TRUE                      15180000
                           SET NOT-OK TO TRUE                           15190000
                           DISPLAY ' ERREUR LECT ENR TYPE 11 '          15200000
                           GO TO FIN-TRT-11-B.                          15210000
           ADD 1 TO CTRLEC.                                             15220000
           IF ENR-TYPE NOT = '11'                                       15230000
                SET NOT-OK TO TRUE                                      15240000
                DISPLAY ' PAS D''ENR TYPE 11 '                          15250000
                GO TO FIN-TRT-11-B.                                     15260000
                                                                        15270000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             15280000
                                                                        15290000
           IF   I = W1QUART                                             15300000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        15310000
              MOVE    CTR7  TO ZON20                                    15320000
           ELSE                                                         15330000
              IF   I  = WMED                                            15340000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     15350000
                 MOVE  CTR7  TO ZON21                                   15360000
              ELSE                                                      15370000
                 IF   I  = W3QUART                                      15380000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 15390000
                     MOVE  CTR7  TO ZON22.                              15400000
                                                                        15410000
                                                                        15420000
       FIN-TRT-11-B. EXIT.                                              15430000
      ***************************************************************** 15440000
      *              TRT       12                                     * 15450000
      ***************************************************************** 15460000
       TRT-12.                                                          15470000
                                                                        15480000
           READ FENTREE INTO W-ENR                                      15490000
      *--> LECTURE DU 1ER ENR DE TYPE 12 ( POUR L'ENTREPOT EN COURS )   15500000
      *--> SI NON-TROUVE ==> ARRET DU PG                                15510000
                    AT END SET FIN-FICHIER TO TRUE                      15520000
                           SET NOT-OK TO TRUE                           15530000
                           DISPLAY ' PAS D''ENR TYPE 12 13 ET 14 '      15540000
                           GO TO FIN-TRT-12.                            15550000
           ADD 1 TO CTRLEC.                                             15560000
           IF ENR-TYPE NOT = '12'                                       15570000
                SET NOT-OK TO TRUE                                      15580000
                DISPLAY ' PAS D''ENR TYPE 12 '                          15590000
                GO TO FIN-TRT-12.                                       15600000
                                                                        15610000
           MOVE ENR-ZON-RATIO  TO  ZON25.                               15620000
           MOVE ZERO           TO  CUMUL-MOY.                           15630000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           15640000
                                                                        15650000
      *--> QUOTIENT = NBEMPL-ATT / QUARTILE                             15660000
           DIVIDE CTR10 BY 4 GIVING QUOTIENT                            15670000
                                                                        15680000
           MOVE     QUOT-5  TO    W1QUART                               15690000
           COMPUTE  WMED     =    W1QUART * 2                           15700000
           COMPUTE  W3QUART  =    W1QUART * 3                           15710000
                                                                        15720000
           MOVE ZERO  TO ZON26 ZON27 ZON28                              15730000
                                                                        15740000
           IF QUOT-5 < 2                                                15750000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR10            15760000
                 READ FENTREE INTO W-ENR                                15770000
                 ADD  1 TO CTRLEC                                       15780000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        15790000
              END-PERFORM                                               15800000
           ELSE                                                         15810000
              IF QUOT-2  = ZERO                                         15820000
      *---------> SI CTR10 EST MULTIPLE DE 4                            15830000
                 PERFORM TRT-12-A  THRU  FIN-TRT-12-A                   15840000
                                 VARYING I FROM 2 BY 1                  15850000
                                             UNTIL I > CTR10            15860000
              ELSE                                                      15870000
                  PERFORM TRT-12-B  THRU FIN-TRT-12-B                   15880000
                                 VARYING I FROM 2 BY 1                  15890000
                                             UNTIL I > CTR10.           15900000
                                                                        15910000
           MOVE  ENR-ZON-RATIO TO ZON29                                 15920000
           COMPUTE CTR7 = CUMUL-MOY / CTR10                             15930000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      15940000
           MOVE    CTR7 TO ZON24.                                       15950000
           WRITE LIGNE FROM DETAIL16.                                   15960000
           ADD  1 TO CTRECR.                                            15970000
                                                                        15980000
       FIN-TRT-12. EXIT.                                                15990000
      ***************************************************************** 16000000
      *              TRT       12 A                                   * 16010000
      ***************************************************************** 16020000
       TRT-12-A.                                                        16030000
                                                                        16040000
           READ FENTREE INTO W-ENR                                      16050000
      *--> LECTURE SUIVANTE ENR DE TYPE 12                              16060000
      *--> SI NON-TROUVE ==> ARRET DU PG                                16070000
                    AT END SET FIN-FICHIER TO TRUE                      16080000
                           SET NOT-OK TO TRUE                           16090000
                           DISPLAY ' ERREUR LECT ENR TYPE 12 '          16100000
                           GO TO FIN-TRT-12-A.                          16110000
           ADD  1 TO CTRLEC.                                            16120000
           IF ENR-TYPE NOT = '12'                                       16130000
                SET NOT-OK TO TRUE                                      16140000
                DISPLAY ' PAS D''ENR TYPE 12 '                          16150000
                GO TO FIN-TRT-12-A.                                     16160000
                                                                        16170000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             16180000
                                                                        16190000
           IF   I = W1QUART                                             16200000
              MOVE  ENR-ZON-RATIO TO ZON26                              16210000
           ELSE                                                         16220000
              IF   I  = WMED                                            16230000
                 MOVE  ENR-ZON-RATIO TO ZON27                           16240000
              ELSE                                                      16250000
                 IF   I  = W3QUART                                      16260000
                     MOVE  ENR-ZON-RATIO TO ZON28.                      16270000
                                                                        16280000
       FIN-TRT-12-A. EXIT.                                              16290000
      ***************************************************************** 16300000
      *              TRT       12 B                                   * 16310000
      ***************************************************************** 16320000
       TRT-12-B.                                                        16330000
                                                                        16340000
           READ FENTREE INTO W-ENR                                      16350000
      *--> LECTURE SUIVANTE ENR DE TYPE 12                              16360000
      *--> SI NON-TROUVE ==> ARRET DU PG                                16370000
                    AT END SET FIN-FICHIER TO TRUE                      16380000
                           SET NOT-OK TO TRUE                           16390000
                           DISPLAY ' ERREUR LECT ENR TYPE 12 '          16400000
                           GO TO FIN-TRT-12-B.                          16410000
           ADD  1 TO CTRLEC.                                            16420000
           IF ENR-TYPE NOT = '12'                                       16430000
                SET NOT-OK TO TRUE                                      16440000
                DISPLAY ' PAS D''ENR TYPE 12 '                          16450000
                GO TO FIN-TRT-12-B.                                     16460000
                                                                        16470000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             16480000
                                                                        16490000
           IF   I = W1QUART                                             16500000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        16510000
              MOVE    CTR7  TO ZON26                                    16520000
           ELSE                                                         16530000
              IF   I  = WMED                                            16540000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     16550000
                 MOVE  CTR7  TO ZON27                                   16560000
              ELSE                                                      16570000
                 IF   I  = W3QUART                                      16580000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 16590000
                     MOVE  CTR7  TO ZON28.                              16600000
                                                                        16610000
                                                                        16620000
       FIN-TRT-12-B. EXIT.                                              16630000
      ***************************************************************** 16640000
      *              TRT       13                                     * 16650000
      ***************************************************************** 16660000
       TRT-13.                                                          16670000
                                                                        16680000
                                                                        16690000
           READ FENTREE INTO W-ENR                                      16700000
      *--> LECTURE DU 1ER ENR DE TYPE 13 ( POUR L'ENTREPOT EN COURS )   16710000
      *--> SI NON-TROUVE ==> ARRET DU PG                                16720000
                    AT END SET FIN-FICHIER TO TRUE                      16730000
                           SET NOT-OK TO TRUE                           16740000
                           DISPLAY ' PAS D''ENR TYPE  13 ET 14 '        16750000
                           GO TO FIN-TRT-13.                            16760000
           ADD  1 TO CTRLEC.                                            16770000
           IF ENR-TYPE NOT = '13'                                       16780000
                SET NOT-OK TO TRUE                                      16790000
                DISPLAY ' PAS D''ENR TYPE 13 '                          16800000
                GO TO FIN-TRT-13.                                       16810000
                                                                        16820000
           MOVE ENR-ZON-RATIO  TO  ZON31.                               16830000
           MOVE ZERO           TO  CUMUL-MOY.                           16840000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           16850000
                                                                        16860000
      *--> QUOTIENT = NB DE REF  / QUARTILE                             16870000
           DIVIDE CTR14 BY 4 GIVING QUOTIENT                            16880000
                                                                        16890000
           MOVE     QUOT-5  TO    W1QUART                               16900000
           COMPUTE  WMED     =    W1QUART * 2                           16910000
           COMPUTE  W3QUART  =    W1QUART * 3                           16920000
                                                                        16930000
           MOVE ZERO  TO ZON32 ZON33 ZON34                              16940000
                                                                        16950000
           IF QUOT-5 < 2                                                16960000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR14            16970000
                 READ FENTREE INTO W-ENR                                16980000
                 ADD  1 TO CTRLEC                                       16990000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        17000000
              END-PERFORM                                               17010000
           ELSE                                                         17020000
              IF QUOT-2  = ZERO                                         17030000
      *---------> SI CTR14 EST MULTIPLE DE 4                            17040000
                  PERFORM TRT-13-A THRU FIN-TRT-13-A                    17050000
                            VARYING I FROM 2 BY 1                       17060000
                                 UNTIL I > CTR14                        17070000
              ELSE                                                      17080000
                  PERFORM TRT-13-B THRU FIN-TRT-13-B                    17090000
                            VARYING I FROM 2 BY 1                       17100000
                                 UNTIL I > CTR14.                       17110000
                                                                        17120000
           MOVE  ENR-ZON-RATIO TO ZON35                                 17130000
           COMPUTE CTR7 = CUMUL-MOY / CTR14                             17140000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      17150000
           MOVE    CTR7 TO ZON30.                                       17160000
           WRITE LIGNE FROM DETAIL17.                                   17170000
           ADD  1 TO CTRECR.                                            17180000
                                                                        17190000
       FIN-TRT-13. EXIT.                                                17200000
      ***************************************************************** 17210000
      *              TRT       13 A                                   * 17220000
      ***************************************************************** 17230000
       TRT-13-A.                                                        17240000
                                                                        17250000
           READ FENTREE INTO W-ENR                                      17260000
      *--> LECTURE SUIVANTE ENR DE TYPE 13                              17270000
      *--> SI NON-TROUVE ==> ARRET DU PG                                17280000
                    AT END SET FIN-FICHIER TO TRUE                      17290000
                           SET NOT-OK TO TRUE                           17300000
                           DISPLAY ' ERREUR LECT ENR TYPE 13 '          17310000
                           GO TO FIN-TRT-13-A.                          17320000
           ADD  1 TO CTRLEC.                                            17330000
           IF ENR-TYPE NOT = '13'                                       17340000
                SET NOT-OK TO TRUE                                      17350000
                DISPLAY ' PAS D''ENR TYPE 13 '                          17360000
                GO TO FIN-TRT-13-A.                                     17370000
                                                                        17380000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             17390000
                                                                        17400000
           IF   I = W1QUART                                             17410000
              MOVE  ENR-ZON-RATIO TO ZON32                              17420000
           ELSE                                                         17430000
              IF   I  = WMED                                            17440000
                 MOVE  ENR-ZON-RATIO TO ZON33                           17450000
              ELSE                                                      17460000
                 IF   I  = W3QUART                                      17470000
                     MOVE  ENR-ZON-RATIO TO ZON34.                      17480000
                                                                        17490000
       FIN-TRT-13-A. EXIT.                                              17500000
      ***************************************************************** 17510000
      *              TRT       13 B                                   * 17520000
      ***************************************************************** 17530000
       TRT-13-B.                                                        17540000
                                                                        17550000
           READ FENTREE INTO W-ENR                                      17560000
      *--> LECTURE SUIVANTE ENR DE TYPE 13                              17570000
      *--> SI NON-TROUVE ==> ARRET DU PG                                17580000
                    AT END SET FIN-FICHIER TO TRUE                      17590000
                           SET NOT-OK TO TRUE                           17600000
                           DISPLAY ' ERREUR LECT ENR TYPE 13 '          17610000
                           GO TO FIN-TRT-13-B.                          17620000
           ADD  1 TO CTRLEC.                                            17630000
           IF ENR-TYPE NOT = '13'                                       17640000
                SET NOT-OK TO TRUE                                      17650000
                DISPLAY ' PAS D''ENR TYPE 13 '                          17660000
                GO TO FIN-TRT-13-B.                                     17670000
                                                                        17680000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             17690000
                                                                        17700000
           IF   I = W1QUART                                             17710000
              PERFORM ROUTINE1  THRU FIN-ROUTINE1                       17720000
              MOVE    CTR7  TO ZON32                                    17730000
           ELSE                                                         17740000
              IF   I  = WMED                                            17750000
                 PERFORM ROUTINE1  THRU FIN-ROUTINE1                    17760000
                 MOVE  CTR7  TO ZON33                                   17770000
              ELSE                                                      17780000
                 IF   I  = W3QUART                                      17790000
                     PERFORM ROUTINE1  THRU FIN-ROUTINE1                17800000
                     MOVE  CTR7  TO ZON34.                              17810000
                                                                        17820000
                                                                        17830000
       FIN-TRT-13-B. EXIT.                                              17840000
      ***************************************************************** 17850000
      *              TRT       14                                     * 17860000
      ***************************************************************** 17870000
       TRT-14.                                                          17880000
                                                                        17890000
           READ FENTREE INTO W-ENR                                      17900000
      *--> LECTURE DU 1ER ENR DE TYPE 14 ( POUR L'ENTREPOT EN COURS )   17910000
      *--> SI NON-TROUVE ==> ARRET DU PG                                17920000
                    AT END SET FIN-FICHIER TO TRUE                      17930000
                           SET NOT-OK TO TRUE                           17940000
                           DISPLAY ' PAS D''ENR TYPE   14 '             17950000
                           GO TO FIN-TRT-14.                            17960000
           ADD  1 TO CTRLEC.                                            17970000
           IF ENR-TYPE NOT = '14'                                       17980000
                SET NOT-OK TO TRUE                                      17990000
                DISPLAY ' PAS D''ENR TYPE 14 '                          18000000
                GO TO FIN-TRT-14.                                       18010000
                                                                        18020000
           MOVE ENR-ZON-RATIO  TO  ZON37.                               18030000
           MOVE ZERO           TO  CUMUL-MOY.                           18040000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           18050000
                                                                        18060000
      *--> QUOTIENT = NB DE REF  / QUARTILE                             18070000
           DIVIDE CTR14 BY 4 GIVING QUOTIENT                            18080000
                                                                        18090000
           MOVE     QUOT-5  TO    W1QUART                               18100000
           COMPUTE  WMED     =    W1QUART * 2                           18110000
           COMPUTE  W3QUART  =    W1QUART * 3                           18120000
                                                                        18130000
           MOVE ZERO TO ZON38 ZON39 ZON40                               18140000
                                                                        18150000
           IF QUOT-5 < 2                                                18160000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR14            18170000
                 READ FENTREE INTO W-ENR                                18180000
                 ADD  1 TO CTRLEC                                       18190000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        18200000
              END-PERFORM                                               18210000
           ELSE                                                         18220000
              IF QUOT-2  = ZERO                                         18230000
      *-------> SI CTR14 EST MULTIPLE DE 4                              18240000
                    PERFORM TRT-14-A  THRU FIN-TRT-14-A                 18250000
                             VARYING I FROM 2 BY 1                      18260000
                                UNTIL I > CTR14                         18270000
              ELSE                                                      18280000
                    PERFORM TRT-14-B  THRU FIN-TRT-14-B                 18290000
                             VARYING I FROM 2 BY 1                      18300000
                                 UNTIL I > CTR14.                       18310000
                                                                        18320000
           MOVE  ENR-ZON-RATIO TO ZON41                                 18330000
           COMPUTE CTR7 = CUMUL-MOY / CTR14                             18340000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      18350000
           MOVE    CTR7 TO ZON36.                                       18360000
           WRITE LIGNE FROM DETAIL18.                                   18370000
           WRITE LIGNE FROM HENTET19.                                   18380000
           ADD  2 TO CTRECR.                                            18390000
                                                                        18400000
       FIN-TRT-14. EXIT.                                                18410000
      ***************************************************************** 18420000
      *              TRT       14 A                                   * 18430000
      ***************************************************************** 18440000
       TRT-14-A.                                                        18450000
                                                                        18460000
           READ FENTREE INTO W-ENR                                      18470000
      *--> LECTURE SUIVANTE ENR DE TYPE 14                              18480000
      *--> SI NON-TROUVE ==> ARRET DU PG                                18490000
                    AT END SET FIN-FICHIER TO TRUE                      18500000
                           SET NOT-OK TO TRUE                           18510000
                           DISPLAY ' ERREUR LECT ENR TYPE 14 '          18520000
                           GO TO FIN-TRT-14-A.                          18530000
           ADD  1 TO CTRLEC.                                            18540000
           IF ENR-TYPE NOT = '14'                                       18550000
                SET NOT-OK TO TRUE                                      18560000
                DISPLAY ' PAS D''ENR TYPE 14 '                          18570000
                GO TO FIN-TRT-14-A.                                     18580000
                                                                        18590000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             18600000
                                                                        18610000
           IF   I = W1QUART                                             18620000
              MOVE  ENR-ZON-RATIO TO ZON38                              18630000
           ELSE                                                         18640000
              IF   I  = WMED                                            18650000
                 MOVE  ENR-ZON-RATIO TO ZON39                           18660000
              ELSE                                                      18670000
                 IF   I  = W3QUART                                      18680000
                     MOVE  ENR-ZON-RATIO TO ZON40.                      18690000
                                                                        18700000
       FIN-TRT-14-A. EXIT.                                              18710000
      ***************************************************************** 18720000
      *              TRT       14 B                                   * 18730000
      ***************************************************************** 18740000
       TRT-14-B.                                                        18750000
                                                                        18760000
           READ FENTREE INTO W-ENR                                      18770000
      *--> LECTURE SUIVANTE ENR DE TYPE 14                              18780000
      *--> SI NON-TROUVE ==> ARRET DU PG                                18790000
                    AT END SET FIN-FICHIER TO TRUE                      18800000
                           SET NOT-OK TO TRUE                           18810000
                           DISPLAY ' ERREUR LECT ENR TYPE 14 '          18820000
                           GO TO FIN-TRT-14-B.                          18830000
           ADD  1 TO CTRLEC.                                            18840000
           IF ENR-TYPE NOT = '14'                                       18850000
                SET NOT-OK TO TRUE                                      18860000
                DISPLAY ' PAS D''ENR TYPE 14 '                          18870000
                GO TO FIN-TRT-14-B.                                     18880000
                                                                        18890000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             18900000
                                                                        18910000
           IF   I = W1QUART                                             18920000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        18930000
              MOVE    CTR7  TO ZON38                                    18940000
           ELSE                                                         18950000
              IF   I  = WMED                                            18960000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     18970000
                 MOVE  CTR7  TO ZON39                                   18980000
              ELSE                                                      18990000
                 IF   I  = W3QUART                                      19000000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 19010000
                     MOVE  CTR7  TO ZON40.                              19020000
                                                                        19030000
                                                                        19040000
       FIN-TRT-14-B. EXIT.                                              19050000
      ***************************************************************** 19060000
      ***************************************************************** 19070000
      ***************************************************************** 19080000
      *                                                                 19090000
      *              TRT       RACK                                   * 19100000
      *                                                                 19110000
      ***************************************************************** 19120000
      ***************************************************************** 19130000
      ***************************************************************** 19140000
       TRT-RACK.                                                        19150000
                                                                        19160000
           IF LECT-A-FAIRE                                              19170000
              READ FENTREE INTO W-ENR                                   19180000
                    AT END SET FIN-FICHIER TO TRUE                      19190000
                           GO TO FIN-TRT-RACK                           19200000
              ADD  1 TO CTRLEC                                          19210000
           END-IF.                                                      19220000
           IF ENR-TYPE NOT = '20'                                       19230000
              SET LECT-DEJA-FAITE TO TRUE                               19240000
              GO TO  FIN-TRT-RACK                                       19250000
           END-IF.                                                      19260000
                                                                        19270000
           PERFORM TRT-20 THRU FIN-TRT-20.                              19280000
           IF OK                                                        19290000
              PERFORM TRT-21 THRU FIN-TRT-21.                           19300000
           IF OK                                                        19310000
              PERFORM TRT-22 THRU FIN-TRT-22.                           19320000
           IF OK                                                        19330000
              PERFORM TRT-23 THRU FIN-TRT-23.                           19340000
           IF OK                                                        19350000
              PERFORM TRT-24 THRU FIN-TRT-24.                           19360000
           IF OK                                                        19370000
              PERFORM TRT-25 THRU FIN-TRT-25.                           19380000
           IF OK                                                        19390000
              PERFORM TRT-26 THRU FIN-TRT-26                            19400000
              SET LECT-A-FAIRE TO TRUE.                                 19410000
                                                                        19420000
                                                                        19430000
       FIN-TRT-RACK. EXIT.                                              19440000
      ***************************************************************** 19450000
      *              TRT       20                                     * 19460000
      ***************************************************************** 19470000
       TRT-20.                                                          19480000
                                                                        19490000
      *---->PICKING                                                     19500000
           MOVE CTR-NBEMPL-TOT-PICK   TO ZON42                          19510000
           MOVE CTR-NBEMPL-ATT-PICK   TO ZON45                          19520000
           IF  CTR-NBEMPL-TOT-PICK NOT = ZERO                           19530000
              COMPUTE CTR6 ROUNDED = (CTR-NBEMPL-ATT-PICK * 100)        19540000
                                        /  CTR-NBEMPL-TOT-PICK          19550000
           ELSE                                                         19560000
              MOVE ZERO  TO CTR6                                        19570000
           END-IF                                                       19580000
                                                                        19590000
           MOVE CTR6              TO ZON46                              19600000
           COMPUTE CTR7 = (CTR-NBEMPL-TOT-PICK - CTR-NBEMPL-ATT-PICK)   19610000
           MOVE CTR7              TO ZON51                              19620000
           IF  CTR-NBEMPL-TOT-PICK NOT = ZERO                           19630000
             COMPUTE CTR6 ROUNDED = (CTR7 * 100) / CTR-NBEMPL-TOT-PICK  19640000
           ELSE                                                         19650000
              MOVE ZERO  TO CTR6                                        19660000
           END-IF                                                       19670000
                                                                        19680000
           MOVE CTR6              TO ZON52.                             19690000
                                                                        19700000
      *---->STOCKAGE                                                    19710000
           MOVE CTR-NBEMPL-TOT-STOC   TO ZON43                          19720000
           MOVE CTR-NBEMPL-ATT-STOC   TO ZON47                          19730000
           IF  CTR-NBEMPL-TOT-STOC NOT = ZERO                           19740000
             COMPUTE CTR6 ROUNDED = (CTR-NBEMPL-ATT-STOC * 100)         19750000
                                        /  CTR-NBEMPL-TOT-STOC          19760000
           ELSE                                                         19770000
              MOVE ZERO  TO CTR6                                        19780000
           END-IF                                                       19790000
                                                                        19800000
           MOVE CTR6              TO ZON48                              19810000
           COMPUTE CTR7 = (CTR-NBEMPL-TOT-STOC - CTR-NBEMPL-ATT-STOC)   19820000
           MOVE CTR7              TO ZON53                              19830000
           IF  CTR-NBEMPL-TOT-STOC NOT = ZERO                           19840000
             COMPUTE CTR6 ROUNDED = (CTR7 * 100) / CTR-NBEMPL-TOT-STOC  19850000
           ELSE                                                         19860000
              MOVE ZERO  TO CTR6                                        19870000
           END-IF                                                       19880000
                                                                        19890000
           MOVE CTR6              TO ZON54.                             19900000
      *---->TOTAL                                                       19910000
           COMPUTE CTR44 = CTR-NBEMPL-TOT-PICK + CTR-NBEMPL-TOT-STOC    19920000
           COMPUTE CTR49 = CTR-NBEMPL-ATT-PICK + CTR-NBEMPL-ATT-STOC    19930000
           MOVE CTR44   TO ZON44                                        19940000
           MOVE CTR49   TO ZON49                                        19950000
           COMPUTE CTR6 ROUNDED = (CTR49 * 100) /  CTR44                19960000
           MOVE CTR6              TO ZON50                              19970000
           COMPUTE CTR7 = (CTR44 - CTR49)                               19980000
           MOVE CTR7              TO ZON55                              19990000
           COMPUTE CTR6 ROUNDED = (CTR7 * 100) / CTR44                  20000000
           MOVE CTR6              TO ZON56.                             20010000
                                                                        20020000
           MOVE CTR-NB-REF-PICK   TO ZON57                              20030000
           MOVE CTR-NB-REF-STOC   TO ZON58                              20040000
           MOVE CTR-NB-REF-TOT    TO ZON59                              20050000
           COMPUTE CTR58 = CTR-NB-REF-PICK + CTR-NB-REF-STOC            20060000
                                                                        20070000
           MOVE CTR-STOCK-NBP-PICK  TO ZON60                            20080000
           MOVE CTR-STOCK-NBP-STOC  TO ZON61                            20090000
           COMPUTE WZON9-7 =                                            20100000
                CTR-STOCK-NBP-PICK + CTR-STOCK-NBP-STOC                 20110000
           MOVE WZON9-7             TO ZON62                            20120000
                                                                        20130000
                                                                        20140000
           MOVE CTR-NBP-CAP-ATT-PICK  TO ZON63                          20150000
           MOVE CTR-NBP-CAP-ATT-STOC  TO ZON64                          20160000
           COMPUTE WZON9-7 =                                            20170000
                CTR-NBP-CAP-ATT-PICK + CTR-NBP-CAP-ATT-STOC             20180000
           MOVE WZON9-7             TO ZON65                            20190000
                                                                        20200000
           MOVE CTR-NBP-CAP-TOT-PICK  TO ZON66                          20210000
           MOVE CTR-NBP-CAP-TOT-STOC  TO ZON67                          20220000
           COMPUTE WZON9-7 =                                            20230000
                CTR-NBP-CAP-TOT-PICK + CTR-NBP-CAP-TOT-STOC             20240000
           MOVE WZON9-7             TO ZON68                            20250000
                                                                        20260000
           MOVE CTR-STOCK-M3-PICK  TO ZON69                             20270000
           MOVE CTR-STOCK-M3-STOC  TO ZON70                             20280000
           COMPUTE WZON9-7 =                                            20290000
                CTR-STOCK-M3-PICK + CTR-STOCK-M3-STOC                   20300000
           MOVE WZON9-7             TO ZON71                            20310000
                                                                        20320000
                                                                        20330000
           MOVE CTR-M3-CAP-ATT-PICK  TO ZON72                           20340000
           MOVE CTR-M3-CAP-ATT-STOC  TO ZON73                           20350000
           COMPUTE WZON9-7 =                                            20360000
                CTR-M3-CAP-ATT-PICK + CTR-M3-CAP-ATT-STOC               20370000
           MOVE WZON9-7             TO ZON74                            20380000
                                                                        20390000
           MOVE CTR-M3-CAP-TOT-PICK  TO ZON75                           20400000
           MOVE CTR-M3-CAP-TOT-STOC  TO ZON76                           20410000
           COMPUTE WZON9-7 =                                            20420000
                CTR-M3-CAP-TOT-PICK + CTR-M3-CAP-TOT-STOC               20430000
           MOVE WZON9-7             TO ZON77.                           20440000
                                                                        20450000
                                                                        20460000
           WRITE LIGNE      FROM  DETAIL20                              20470000
           WRITE LIGNE      FROM  DETAIL21                              20480000
           WRITE LIGNE      FROM  DETAIL22                              20490000
           WRITE LIGNE      FROM  DETAIL23                              20500000
           WRITE LIGNE      FROM  DETAIL24                              20510000
           WRITE LIGNE      FROM  DETAIL25                              20520000
           WRITE LIGNE      FROM  DETAIL26                              20530000
           WRITE LIGNE      FROM  DETAIL27                              20540000
           WRITE LIGNE      FROM  DETAIL28                              20550000
           WRITE LIGNE      FROM  DETAIL29                              20560000
           WRITE LIGNE      FROM  DETAIL30                              20570000
           WRITE LIGNE      FROM  DETAIL31                              20580000
           WRITE LIGNE      FROM  DETAIL32                              20590000
           WRITE LIGNE      FROM  DETAIL33                              20600000
           WRITE LIGNE      FROM  DETAIL34                              20610000
           WRITE LIGNE      FROM  DETAIL35                              20620000
           WRITE LIGNE      FROM  DETAIL36.                             20630000
           ADD 17 TO CTRECR.                                            20640000
                                                                        20650000
       FIN-TRT-20. EXIT.                                                20660000
      ***************************************************************** 20670000
      *              TRT       21                                     * 20680000
      ***************************************************************** 20690000
       TRT-21.                                                          20700000
                                                                        20710000
           READ FENTREE INTO W-ENR                                      20720000
      *--> LECTURE DU 1ER ENR DE TYPE 21 ( POUR L'ENTREPOT EN COURS )   20730000
      *--> SI NON-TROUVE ==> ARRET DU PG                                20740000
                    AT END SET FIN-FICHIER TO TRUE                      20750000
                           SET NOT-OK TO TRUE                           20760000
                           DISPLAY ' PAS D''ENR TYPE 21 A     26 '      20770000
                           GO TO FIN-TRT-21.                            20780000
           ADD  1 TO CTRLEC.                                            20790000
           IF ENR-TYPE NOT = '21'                                       20800000
                SET NOT-OK TO TRUE                                      20810000
                DISPLAY ' PAS D''ENR TYPE 21 '                          20820000
                GO TO FIN-TRT-21.                                       20830000
                                                                        20840000
           MOVE ENR-ZON-RATIO  TO  ZON79.                               20850000
           MOVE ZERO           TO  CUMUL-MOY.                           20860000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           20870000
                                                                        20880000
      *--> QUOTIENT = NBEMPL-ATT / QUARTILE                             20890000
           DIVIDE CTR49 BY 4 GIVING QUOTIENT                            20900000
                                                                        20910000
           MOVE     QUOT-5  TO    W1QUART                               20920000
           COMPUTE  WMED     =    W1QUART * 2                           20930000
           COMPUTE  W3QUART  =    W1QUART * 3                           20940000
                                                                        20950000
           MOVE ZERO TO ZON80  ZON81  ZON82                             20960000
           IF QUOT-5 < 2                                                20970000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR49            20980000
                 READ FENTREE INTO W-ENR                                20990000
                 ADD  1 TO CTRLEC                                       21000000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        21010000
              END-PERFORM                                               21020000
           ELSE                                                         21030000
              IF QUOT-2  = ZERO                                         21040000
      *----------> SI CTR49  EST MULTIPLE DE 4                          21050000
                  PERFORM TRT-21-A THRU FIN-TRT-21-A                    21060000
                                      VARYING I FROM 2 BY 1             21070000
                                          UNTIL I > CTR49               21080000
              ELSE                                                      21090000
                  PERFORM TRT-21-B THRU FIN-TRT-21-B                    21100000
                                   VARYING I FROM 2 BY 1                21110000
                                          UNTIL I > CTR49.              21120000
                                                                        21130000
           MOVE  ENR-ZON-RATIO TO ZON83                                 21140000
           COMPUTE CTR7 = CUMUL-MOY / CTR49                             21150000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      21160000
           MOVE    CTR7 TO ZON78.                                       21170000
           WRITE LIGNE FROM DETAIL37.                                   21180000
           ADD  1 TO CTRECR.                                            21190000
                                                                        21200000
       FIN-TRT-21. EXIT.                                                21210000
      ***************************************************************** 21220000
      *              TRT       21 A                                   * 21230000
      ***************************************************************** 21240000
       TRT-21-A.                                                        21250000
                                                                        21260000
           READ FENTREE INTO W-ENR                                      21270000
      *--> LECTURE SUIVANTE ENR DE TYPE 21                              21280000
      *--> SI NON-TROUVE ==> ARRET DU PG                                21290000
                    AT END SET FIN-FICHIER TO TRUE                      21300000
                           SET NOT-OK TO TRUE                           21310000
                           DISPLAY ' ERREUR LECT ENR TYPE 21 '          21320000
                           GO TO FIN-TRT-21-A.                          21330000
           ADD  1 TO CTRLEC.                                            21340000
           IF ENR-TYPE NOT = '21'                                       21350000
                SET NOT-OK TO TRUE                                      21360000
                DISPLAY ' PAS D''ENR TYPE 21 '                          21370000
                GO TO FIN-TRT-21-A.                                     21380000
                                                                        21390000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             21400000
                                                                        21410000
           IF   I = W1QUART                                             21420000
              MOVE  ENR-ZON-RATIO TO ZON80                              21430000
           ELSE                                                         21440000
              IF   I  = WMED                                            21450000
                 MOVE  ENR-ZON-RATIO TO ZON81                           21460000
              ELSE                                                      21470000
                 IF   I  = W3QUART                                      21480000
                     MOVE  ENR-ZON-RATIO TO ZON82.                      21490000
                                                                        21500000
       FIN-TRT-21-A. EXIT.                                              21510000
      ***************************************************************** 21520000
      *              TRT       21 B                                   * 21530000
      ***************************************************************** 21540000
       TRT-21-B.                                                        21550000
                                                                        21560000
           READ FENTREE INTO W-ENR                                      21570000
      *--> LECTURE SUIVANTE ENR DE TYPE 21                              21580000
      *--> SI NON-TROUVE ==> ARRET DU PG                                21590000
                    AT END SET FIN-FICHIER TO TRUE                      21600000
                           SET NOT-OK TO TRUE                           21610000
                           DISPLAY ' ERREUR LECT ENR TYPE 31 '          21620000
                           GO TO FIN-TRT-21-B.                          21630000
           ADD  1 TO CTRLEC.                                            21640000
           IF ENR-TYPE NOT = '21'                                       21650000
                SET NOT-OK TO TRUE                                      21660000
                DISPLAY ' PAS D''ENR TYPE 21 '                          21670000
                GO TO FIN-TRT-21-B.                                     21680000
                                                                        21690000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             21700000
                                                                        21710000
           IF   I = W1QUART                                             21720000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        21730000
              MOVE    CTR7  TO ZON80                                    21740000
           ELSE                                                         21750000
              IF   I  = WMED                                            21760000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     21770000
                 MOVE  CTR7  TO ZON81                                   21780000
              ELSE                                                      21790000
                 IF   I  = W3QUART                                      21800000
                     PERFORM ROUTINE1  THRU FIN-ROUTINE1                21810000
                     MOVE  CTR7  TO ZON82.                              21820000
                                                                        21830000
                                                                        21840000
       FIN-TRT-21-B. EXIT.                                              21850000
      ***************************************************************** 21860000
      *              TRT       22                                     * 21870000
      ***************************************************************** 21880000
       TRT-22.                                                          21890000
                                                                        21900000
           READ FENTREE INTO W-ENR                                      21910000
      *--> LECTURE DU 1ER ENR DE TYPE 22 ( POUR L'ENTREPOT EN COURS )   21920000
      *--> SI NON-TROUVE ==> ARRET DU PG                                21930000
                    AT END SET FIN-FICHIER TO TRUE                      21940000
                           SET NOT-OK TO TRUE                           21950000
                           DISPLAY ' PAS D''ENR TYPE  22 A  26 '        21960000
                           GO TO FIN-TRT-22.                            21970000
           ADD  1 TO CTRLEC.                                            21980000
           IF ENR-TYPE NOT = '22'                                       21990000
                SET NOT-OK TO TRUE                                      22000000
                DISPLAY ' PAS D''ENR TYPE 22 '                          22010000
                GO TO FIN-TRT-22.                                       22020000
                                                                        22030000
           MOVE ENR-ZON-RATIO  TO  ZON85.                               22040000
           MOVE ZERO           TO  CUMUL-MOY.                           22050000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           22060000
                                                                        22070000
      *--> QUOTIENT = NB REF   / QUARTILE                               22080000
           DIVIDE CTR49  BY 4 GIVING QUOTIENT                           22090000
                                                                        22100000
           MOVE     QUOT-5  TO    W1QUART                               22110000
           COMPUTE  WMED     =    W1QUART * 2                           22120000
           COMPUTE  W3QUART  =    W1QUART * 3                           22130000
                                                                        22140000
           MOVE ZERO TO ZON86  ZON87  ZON88                             22150000
           IF QUOT-5 < 2                                                22160000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR49            22170000
                 READ FENTREE INTO W-ENR                                22180000
                 ADD  1 TO CTRLEC                                       22190000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        22200000
              END-PERFORM                                               22210000
           ELSE                                                         22220000
              IF QUOT-2  = ZERO                                         22230000
      *----------> SI CTR49  EST MULTIPLE DE 4                          22240000
                  PERFORM TRT-22-A THRU FIN-TRT-22-A                    22250000
                                      VARYING I FROM 2 BY 1             22260000
                                          UNTIL I > CTR49               22270000
              ELSE                                                      22280000
                  PERFORM TRT-22-B THRU FIN-TRT-22-B                    22290000
                                      VARYING I FROM 2 BY 1             22300000
                                          UNTIL I > CTR49.              22310000
                                                                        22320000
           MOVE  ENR-ZON-RATIO TO ZON89                                 22330000
           COMPUTE CTR7 = CUMUL-MOY / CTR49                             22340000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      22350000
           MOVE    CTR7 TO ZON84.                                       22360000
           WRITE LIGNE FROM DETAIL38.                                   22370000
           ADD  1 TO CTRECR.                                            22380000
                                                                        22390000
       FIN-TRT-22. EXIT.                                                22400000
      ***************************************************************** 22410000
      *              TRT       22 A                                   * 22420000
      ***************************************************************** 22430000
       TRT-22-A.                                                        22440000
                                                                        22450000
           READ FENTREE INTO W-ENR                                      22460000
      *--> LECTURE SUIVANTE ENR DE TYPE 22                              22470000
      *--> SI NON-TROUVE ==> ARRET DU PG                                22480000
                    AT END SET FIN-FICHIER TO TRUE                      22490000
                           SET NOT-OK TO TRUE                           22500000
                           DISPLAY ' ERREUR LECT ENR TYPE 22 '          22510000
                           GO TO FIN-TRT-22-A.                          22520000
           ADD  1 TO CTRLEC.                                            22530000
           IF ENR-TYPE NOT = '22'                                       22540000
                SET NOT-OK TO TRUE                                      22550000
                DISPLAY ' PAS D''ENR TYPE 22 '                          22560000
                GO TO FIN-TRT-22-A.                                     22570000
                                                                        22580000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             22590000
                                                                        22600000
           IF   I = W1QUART                                             22610000
              MOVE  ENR-ZON-RATIO TO ZON86                              22620000
           ELSE                                                         22630000
              IF   I  = WMED                                            22640000
                 MOVE  ENR-ZON-RATIO TO ZON87                           22650000
              ELSE                                                      22660000
                 IF   I  = W3QUART                                      22670000
                     MOVE  ENR-ZON-RATIO TO ZON88.                      22680000
                                                                        22690000
       FIN-TRT-22-A. EXIT.                                              22700000
      ***************************************************************** 22710000
      *              TRT       22 B                                   * 22720000
      ***************************************************************** 22730000
       TRT-22-B.                                                        22740000
                                                                        22750000
           READ FENTREE INTO W-ENR                                      22760000
      *--> LECTURE SUIVANTE ENR DE TYPE 22                              22770000
      *--> SI NON-TROUVE ==> ARRET DU PG                                22780000
                    AT END SET FIN-FICHIER TO TRUE                      22790000
                           SET NOT-OK TO TRUE                           22800000
                           DISPLAY ' ERREUR LECT ENR TYPE 22 '          22810000
                           GO TO FIN-TRT-22-B.                          22820000
           ADD  1 TO CTRLEC.                                            22830000
           IF ENR-TYPE NOT = '22'                                       22840000
                SET NOT-OK TO TRUE                                      22850000
                DISPLAY ' PAS D''ENR TYPE 22 '                          22860000
                GO TO FIN-TRT-22-B.                                     22870000
                                                                        22880000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             22890000
                                                                        22900000
           IF   I = W1QUART                                             22910000
              PERFORM ROUTINE1  THRU FIN-ROUTINE1                       22920000
              MOVE    CTR7  TO ZON86                                    22930000
           ELSE                                                         22940000
              IF   I  = WMED                                            22950000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     22960000
                 MOVE  CTR7  TO ZON87                                   22970000
              ELSE                                                      22980000
                 IF   I  = W3QUART                                      22990000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 23000000
                     MOVE  CTR7  TO ZON88.                              23010000
                                                                        23020000
                                                                        23030000
       FIN-TRT-22-B. EXIT.                                              23040000
      ***************************************************************** 23050000
      *              TRT       23                                     * 23060000
      ***************************************************************** 23070000
       TRT-23.                                                          23080000
                                                                        23090000
           READ FENTREE INTO W-ENR                                      23100000
      *--> LECTURE DU 1ER ENR DE TYPE 23 ( POUR L'ENTREPOT EN COURS )   23110000
      *--> SI NON-TROUVE ==> ARRET DU PG                                23120000
                    AT END SET FIN-FICHIER TO TRUE                      23130000
                           SET NOT-OK TO TRUE                           23140000
                           DISPLAY ' PAS D''ENR TYPE   23 A 26 '        23150000
                           GO TO FIN-TRT-23.                            23160000
           ADD  1 TO CTRLEC.                                            23170000
           IF ENR-TYPE NOT = '23'                                       23180000
                SET NOT-OK TO TRUE                                      23190000
                DISPLAY ' PAS D''ENR TYPE 23 '                          23200000
                GO TO FIN-TRT-23.                                       23210000
                                                                        23220000
           MOVE ENR-ZON-RATIO  TO  ZON91.                               23230000
           MOVE ZERO           TO  CUMUL-MOY.                           23240000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           23250000
                                                                        23260000
      *--> QUOTIENT = NB REF   / QUARTILE                               23270000
           DIVIDE CTR58  BY 4 GIVING QUOTIENT                           23280000
                                                                        23290000
           MOVE     QUOT-5  TO    W1QUART                               23300000
           COMPUTE  WMED     =    W1QUART * 2                           23310000
           COMPUTE  W3QUART  =    W1QUART * 3                           23320000
                                                                        23330000
           MOVE ZERO TO ZON92  ZON93  ZON94                             23340000
           IF QUOT-5 < 2                                                23350000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR58            23360000
                 READ FENTREE INTO W-ENR                                23370000
                 ADD  1 TO CTRLEC                                       23380000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        23390000
              END-PERFORM                                               23400000
           ELSE                                                         23410000
              IF QUOT-2  = ZERO                                         23420000
      *----------> SI CTR58  EST MULTIPLE DE 4                          23430000
                  PERFORM TRT-23-A THRU FIN-TRT-23-A                    23440000
                                      VARYING I FROM 2 BY 1             23450000
                                          UNTIL I > CTR58               23460000
              ELSE                                                      23470000
                  PERFORM TRT-23-B THRU FIN-TRT-23-B                    23480000
                                      VARYING I FROM 2 BY 1             23490000
                                          UNTIL I > CTR58.              23500000
                                                                        23510000
           MOVE  ENR-ZON-RATIO TO ZON95                                 23520000
           COMPUTE CTR7 = CUMUL-MOY / CTR58                             23530000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      23540000
           MOVE    CTR7 TO ZON90.                                       23550000
           WRITE LIGNE FROM DETAIL39.                                   23560000
           ADD  1 TO CTRECR.                                            23570000
                                                                        23580000
       FIN-TRT-23. EXIT.                                                23590000
      ***************************************************************** 23600000
      *              TRT       23 A                                   * 23610000
      ***************************************************************** 23620000
       TRT-23-A.                                                        23630000
                                                                        23640000
           READ FENTREE INTO W-ENR                                      23650000
      *--> LECTURE SUIVANTE ENR DE TYPE 23                              23660000
      *--> SI NON-TROUVE ==> ARRET DU PG                                23670000
                    AT END SET FIN-FICHIER TO TRUE                      23680000
                           SET NOT-OK TO TRUE                           23690000
                           DISPLAY ' ERREUR LECT ENR TYPE 23 '          23700000
                           GO TO FIN-TRT-23-A.                          23710000
           ADD  1 TO CTRLEC.                                            23720000
           IF ENR-TYPE NOT = '23'                                       23730000
                SET NOT-OK TO TRUE                                      23740000
                DISPLAY ' PAS D''ENR TYPE 23 '                          23750000
                GO TO FIN-TRT-23-A.                                     23760000
                                                                        23770000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             23780000
                                                                        23790000
           IF   I = W1QUART                                             23800000
              MOVE  ENR-ZON-RATIO TO ZON92                              23810000
           ELSE                                                         23820000
              IF   I  = WMED                                            23830000
                 MOVE  ENR-ZON-RATIO TO ZON93                           23840000
              ELSE                                                      23850000
                 IF   I  = W3QUART                                      23860000
                     MOVE  ENR-ZON-RATIO TO ZON94.                      23870000
                                                                        23880000
       FIN-TRT-23-A. EXIT.                                              23890000
      ***************************************************************** 23900000
      *              TRT       23 B                                   * 23910000
      ***************************************************************** 23920000
       TRT-23-B.                                                        23930000
                                                                        23940000
           READ FENTREE INTO W-ENR                                      23950000
      *--> LECTURE SUIVANTE ENR DE TYPE 23                              23960000
      *--> SI NON-TROUVE ==> ARRET DU PG                                23970000
                    AT END SET FIN-FICHIER TO TRUE                      23980000
                           SET NOT-OK TO TRUE                           23990000
                           DISPLAY ' ERREUR LECT ENR TYPE 23 '          24000000
                           GO TO FIN-TRT-23-B.                          24010000
           ADD  1 TO CTRLEC.                                            24020000
           IF ENR-TYPE NOT = '23'                                       24030000
                SET NOT-OK TO TRUE                                      24040000
                DISPLAY ' PAS D''ENR TYPE 23 '                          24050000
                GO TO FIN-TRT-23-B.                                     24060000
                                                                        24070000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             24080000
                                                                        24090000
           IF   I = W1QUART                                             24100000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        24110000
              MOVE    CTR7  TO ZON92                                    24120000
           ELSE                                                         24130000
              IF   I  = WMED                                            24140000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     24150000
                 MOVE  CTR7  TO ZON93                                   24160000
              ELSE                                                      24170000
                 IF   I  = W3QUART                                      24180000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 24190000
                     MOVE  CTR7  TO ZON94.                              24200000
                                                                        24210000
       FIN-TRT-23-B. EXIT.                                              24220000
      *                                                                 24230000
      ***************************************************************** 24240000
      *              TRT       24                                     * 24250000
      ***************************************************************** 24260000
       TRT-24.                                                          24270000
                                                                        24280000
           READ FENTREE INTO W-ENR                                      24290000
      *--> LECTURE DU 1ER ENR DE TYPE 24 ( POUR L'ENTREPOT EN COURS )   24300000
      *--> SI NON-TROUVE ==> ARRET DU PG                                24310000
                    AT END SET FIN-FICHIER TO TRUE                      24320000
                           SET NOT-OK TO TRUE                           24330000
                           DISPLAY ' PAS D''ENR TYPE   24 A 26 '        24340000
                           GO TO FIN-TRT-24.                            24350000
           ADD  1 TO CTRLEC.                                            24360000
           IF ENR-TYPE NOT = '24'                                       24370000
                SET NOT-OK TO TRUE                                      24380000
                DISPLAY ' PAS D''ENR TYPE 24 '                          24390000
                GO TO FIN-TRT-24.                                       24400000
                                                                        24410000
           MOVE ENR-ZON-RATIO  TO  ZON97.                               24420000
           MOVE ZERO           TO  CUMUL-MOY.                           24430000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           24440000
                                                                        24450000
      *--> QUOTIENT = NB REF   / QUARTILE                               24460000
           DIVIDE CTR58  BY 4 GIVING QUOTIENT                           24470000
                                                                        24480000
           MOVE     QUOT-5  TO    W1QUART                               24490000
           COMPUTE  WMED     =    W1QUART * 2                           24500000
           COMPUTE  W3QUART  =    W1QUART * 3                           24510000
                                                                        24520000
           MOVE ZERO TO ZON98  ZON99  ZON100                            24530000
           IF QUOT-5 < 2                                                24540000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR58            24550000
                 READ FENTREE INTO W-ENR                                24560000
                 ADD  1 TO CTRLEC                                       24570000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        24580000
              END-PERFORM                                               24590000
           ELSE                                                         24600000
              IF QUOT-2  = ZERO                                         24610000
      *----------> SI CTR58  EST MULTIPLE DE 4                          24620000
                  PERFORM TRT-24-A THRU FIN-TRT-24-A                    24630000
                                      VARYING I FROM 2 BY 1             24640000
                                          UNTIL I > CTR58               24650000
              ELSE                                                      24660000
                  PERFORM TRT-24-B THRU FIN-TRT-24-B                    24670000
                                      VARYING I FROM 2 BY 1             24680000
                                          UNTIL I > CTR58.              24690000
                                                                        24700000
           MOVE  ENR-ZON-RATIO TO ZON101                                24710000
           COMPUTE CTR7 = CUMUL-MOY / CTR58                             24720000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      24730000
           MOVE    CTR7 TO ZON96.                                       24740000
           WRITE LIGNE FROM DETAIL40.                                   24750000
           ADD  1 TO CTRECR.                                            24760000
                                                                        24770000
       FIN-TRT-24. EXIT.                                                24780000
      ***************************************************************** 24790000
      *              TRT       24 A                                   * 24800000
      ***************************************************************** 24810000
       TRT-24-A.                                                        24820000
                                                                        24830000
           READ FENTREE INTO W-ENR                                      24840000
      *--> LECTURE SUIVANTE ENR DE TYPE 24                              24850000
      *--> SI NON-TROUVE ==> ARRET DU PG                                24860000
                    AT END SET FIN-FICHIER TO TRUE                      24870000
                           SET NOT-OK TO TRUE                           24880000
                           DISPLAY ' ERREUR LECT ENR TYPE 24 '          24890000
                           GO TO FIN-TRT-24-A.                          24900000
           ADD  1 TO CTRLEC.                                            24910000
           IF ENR-TYPE NOT = '24'                                       24920000
                SET NOT-OK TO TRUE                                      24930000
                DISPLAY ' PAS D''ENR TYPE 24 '                          24940000
                GO TO FIN-TRT-24-A.                                     24950000
                                                                        24960000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             24970000
                                                                        24980000
           IF   I = W1QUART                                             24990000
              MOVE  ENR-ZON-RATIO TO ZON98                              25000000
           ELSE                                                         25010000
              IF   I  = WMED                                            25020000
                 MOVE  ENR-ZON-RATIO TO ZON99                           25030000
              ELSE                                                      25040000
                 IF   I  = W3QUART                                      25050000
                     MOVE  ENR-ZON-RATIO TO ZON100.                     25060000
                                                                        25070000
       FIN-TRT-24-A. EXIT.                                              25080000
      ***************************************************************** 25090000
      *              TRT       24 B                                   * 25100000
      ***************************************************************** 25110000
       TRT-24-B.                                                        25120000
                                                                        25130000
           READ FENTREE INTO W-ENR                                      25140000
      *--> LECTURE SUIVANTE ENR DE TYPE 24                              25150000
      *--> SI NON-TROUVE ==> ARRET DU PG                                25160000
                    AT END SET FIN-FICHIER TO TRUE                      25170000
                           SET NOT-OK TO TRUE                           25180000
                           DISPLAY ' ERREUR LECT ENR TYPE 24 '          25190000
                           GO TO FIN-TRT-24-B.                          25200000
           ADD  1 TO CTRLEC.                                            25210000
           IF ENR-TYPE NOT = '24'                                       25220000
                SET NOT-OK TO TRUE                                      25230000
                DISPLAY ' PAS D''ENR TYPE 24 '                          25240000
                GO TO FIN-TRT-24-B.                                     25250000
                                                                        25260000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             25270000
                                                                        25280000
           IF   I = W1QUART                                             25290000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        25300000
              MOVE    CTR7  TO ZON98                                    25310000
           ELSE                                                         25320000
              IF   I  = WMED                                            25330000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     25340000
                 MOVE  CTR7  TO ZON99                                   25350000
              ELSE                                                      25360000
                 IF   I  = W3QUART                                      25370000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 25380000
                     MOVE  CTR7  TO ZON100.                             25390000
                                                                        25400000
       FIN-TRT-24-B. EXIT.                                              25410000
      *                                                                 25420000
      ***************************************************************** 25430000
      *              TRT       25                                     * 25440000
      ***************************************************************** 25450000
       TRT-25.                                                          25460000
                                                                        25470000
           READ FENTREE INTO W-ENR                                      25480000
      *--> LECTURE DU 1ER ENR DE TYPE 25 ( POUR L'ENTREPOT EN COURS )   25490000
      *--> SI NON-TROUVE ==> ARRET DU PG                                25500000
                    AT END SET FIN-FICHIER TO TRUE                      25510000
                           SET NOT-OK TO TRUE                           25520000
                           DISPLAY ' PAS D''ENR TYPE   25 A 26 '        25530000
                           GO TO FIN-TRT-25.                            25540000
           ADD  1 TO CTRLEC.                                            25550000
           IF ENR-TYPE NOT = '25'                                       25560000
                SET NOT-OK TO TRUE                                      25570000
                DISPLAY ' PAS D''ENR TYPE 25 '                          25580000
                GO TO FIN-TRT-25.                                       25590000
                                                                        25600000
           MOVE ENR-ZON-RATIO  TO  ZON103.                              25610000
           MOVE ZERO           TO  CUMUL-MOY.                           25620000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           25630000
                                                                        25640000
      *--> QUOTIENT = NB EMPL  / QUARTILE                               25650000
           DIVIDE CTR49  BY 4 GIVING QUOTIENT                           25660000
                                                                        25670000
           MOVE     QUOT-5  TO    W1QUART                               25680000
           COMPUTE  WMED     =    W1QUART * 2                           25690000
           COMPUTE  W3QUART  =    W1QUART * 3                           25700000
                                                                        25710000
           MOVE ZERO TO ZON104 ZON105 ZON106                            25720000
           IF QUOT-5 < 2                                                25730000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR49            25740000
                 READ FENTREE INTO W-ENR                                25750000
                 ADD  1 TO CTRLEC                                       25760000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        25770000
              END-PERFORM                                               25780000
           ELSE                                                         25790000
              IF QUOT-2  = ZERO                                         25800000
      *----------> SI CTR49  EST MULTIPLE DE 4                          25810000
                  PERFORM TRT-25-A THRU FIN-TRT-25-A                    25820000
                                      VARYING I FROM 2 BY 1             25830000
                                          UNTIL I > CTR49               25840000
              ELSE                                                      25850000
                  PERFORM TRT-25-B THRU FIN-TRT-25-B                    25860000
                                      VARYING I FROM 2 BY 1             25870000
                                          UNTIL I > CTR49.              25880000
                                                                        25890000
           MOVE  ENR-ZON-RATIO TO ZON107                                25900000
           COMPUTE CTR7 = CUMUL-MOY / CTR49                             25910000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      25920000
           MOVE    CTR7 TO ZON102.                                      25930000
           WRITE LIGNE FROM DETAIL41.                                   25940000
           ADD  1 TO CTRECR.                                            25950000
                                                                        25960000
       FIN-TRT-25. EXIT.                                                25970000
      ***************************************************************** 25980000
      *              TRT       25 A                                   * 25990000
      ***************************************************************** 26000000
       TRT-25-A.                                                        26010000
                                                                        26020000
           READ FENTREE INTO W-ENR                                      26030000
      *--> LECTURE SUIVANTE ENR DE TYPE 25                              26040000
      *--> SI NON-TROUVE ==> ARRET DU PG                                26050000
                    AT END SET FIN-FICHIER TO TRUE                      26060000
                           SET NOT-OK TO TRUE                           26070000
                           DISPLAY ' ERREUR LECT ENR TYPE 25 '          26080000
                           GO TO FIN-TRT-25-A.                          26090000
           ADD  1 TO CTRLEC.                                            26100000
           IF ENR-TYPE NOT = '25'                                       26110000
                SET NOT-OK TO TRUE                                      26120000
                DISPLAY ' PAS D''ENR TYPE 25 '                          26130000
                GO TO FIN-TRT-25-A.                                     26140000
                                                                        26150000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             26160000
                                                                        26170000
           IF   I = W1QUART                                             26180000
              MOVE  ENR-ZON-RATIO TO ZON104                             26190000
           ELSE                                                         26200000
              IF   I  = WMED                                            26210000
                 MOVE  ENR-ZON-RATIO TO ZON105                          26220000
              ELSE                                                      26230000
                 IF   I  = W3QUART                                      26240000
                     MOVE  ENR-ZON-RATIO TO ZON106.                     26250000
                                                                        26260000
       FIN-TRT-25-A. EXIT.                                              26270000
      ***************************************************************** 26280000
      *              TRT       25 B                                   * 26290000
      ***************************************************************** 26300000
       TRT-25-B.                                                        26310000
                                                                        26320000
           READ FENTREE INTO W-ENR                                      26330000
      *--> LECTURE SUIVANTE ENR DE TYPE 25                              26340000
      *--> SI NON-TROUVE ==> ARRET DU PG                                26350000
                    AT END SET FIN-FICHIER TO TRUE                      26360000
                           SET NOT-OK TO TRUE                           26370000
                           DISPLAY ' ERREUR LECT ENR TYPE 25 '          26380000
                           GO TO FIN-TRT-25-B.                          26390000
           ADD  1 TO CTRLEC.                                            26400000
           IF ENR-TYPE NOT = '25'                                       26410000
                SET NOT-OK TO TRUE                                      26420000
                DISPLAY ' PAS D''ENR TYPE 25 '                          26430000
                GO TO FIN-TRT-25-B.                                     26440000
                                                                        26450000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             26460000
                                                                        26470000
           IF   I = W1QUART                                             26480000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        26490000
              MOVE    CTR7  TO ZON104                                   26500000
           ELSE                                                         26510000
              IF   I  = WMED                                            26520000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     26530000
                 MOVE  CTR7  TO ZON105                                  26540000
              ELSE                                                      26550000
                 IF   I  = W3QUART                                      26560000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 26570000
                     MOVE  CTR7  TO ZON106.                             26580000
                                                                        26590000
       FIN-TRT-25-B. EXIT.                                              26600000
      *                                                                 26610000
      ***************************************************************** 26620000
      *              TRT       26                                     * 26630000
      ***************************************************************** 26640000
       TRT-26.                                                          26650000
                                                                        26660000
           READ FENTREE INTO W-ENR                                      26670000
      *--> LECTURE DU 1ER ENR DE TYPE 26 ( POUR L'ENTREPOT EN COURS )   26680000
      *--> SI NON-TROUVE ==> ARRET DU PG                                26690000
                    AT END SET FIN-FICHIER TO TRUE                      26700000
                           SET NOT-OK TO TRUE                           26710000
                           DISPLAY ' PAS D''ENR TYPE    26 '            26720000
                           GO TO FIN-TRT-26.                            26730000
           ADD  1 TO CTRLEC.                                            26740000
           IF ENR-TYPE NOT = '26'                                       26750000
                SET NOT-OK TO TRUE                                      26760000
                DISPLAY ' PAS D''ENR TYPE 26 '                          26770000
                GO TO FIN-TRT-26.                                       26780000
                                                                        26790000
           MOVE ENR-ZON-RATIO  TO  ZON109.                              26800000
           MOVE ZERO           TO  CUMUL-MOY.                           26810000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           26820000
                                                                        26830000
      *--> QUOTIENT = NB REF   / QUARTILE                               26840000
           DIVIDE CTR58  BY 4 GIVING QUOTIENT                           26850000
                                                                        26860000
           MOVE     QUOT-5  TO    W1QUART                               26870000
           COMPUTE  WMED     =    W1QUART * 2                           26880000
           COMPUTE  W3QUART  =    W1QUART * 3                           26890000
                                                                        26900000
           MOVE ZERO TO ZON110 ZON111 ZON112                            26910000
           IF QUOT-5 < 2                                                26920000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR58            26930000
                 READ FENTREE INTO W-ENR                                26940000
                 ADD  1 TO CTRLEC                                       26950000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        26960000
              END-PERFORM                                               26970000
           ELSE                                                         26980000
              IF QUOT-2  = ZERO                                         26990000
      *----------> SI CTR58  EST MULTIPLE DE 4                          27000000
                  PERFORM TRT-26-A THRU FIN-TRT-26-A                    27010000
                                      VARYING I FROM 2 BY 1             27020000
                                          UNTIL I > CTR58               27030000
              ELSE                                                      27040000
                  PERFORM TRT-26-B THRU FIN-TRT-26-B                    27050000
                                      VARYING I FROM 2 BY 1             27060000
                                          UNTIL I > CTR58.              27070000
                                                                        27080000
           MOVE  ENR-ZON-RATIO TO ZON113                                27090000
           COMPUTE CTR7 = CUMUL-MOY / CTR58                             27100000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      27110000
           MOVE    CTR7 TO ZON108.                                      27120000
           WRITE LIGNE FROM DETAIL42.                                   27130000
           WRITE LIGNE FROM HENTET43.                                   27140000
           ADD  2 TO CTRECR.                                            27141000
                                                                        27142000
       FIN-TRT-26. EXIT.                                                27143000
      ***************************************************************** 27144000
      *              TRT       26 A                                   * 27145000
      ***************************************************************** 27146000
       TRT-26-A.                                                        27147000
                                                                        27148000
           READ FENTREE INTO W-ENR                                      27149000
      *--> LECTURE SUIVANTE ENR DE TYPE 26                              27150000
      *--> SI NON-TROUVE ==> ARRET DU PG                                27160000
                    AT END SET FIN-FICHIER TO TRUE                      27170000
                           SET NOT-OK TO TRUE                           27180000
                           DISPLAY ' ERREUR LECT ENR TYPE 26 '          27190000
                           GO TO FIN-TRT-26-A.                          27200000
           ADD  1 TO CTRLEC.                                            27210000
           IF ENR-TYPE NOT = '26'                                       27220000
                SET NOT-OK TO TRUE                                      27230000
                DISPLAY ' PAS D''ENR TYPE 26 '                          27240000
                GO TO FIN-TRT-26-A.                                     27250000
                                                                        27260000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             27270000
                                                                        27280000
           IF   I = W1QUART                                             27290000
              MOVE  ENR-ZON-RATIO TO ZON110                             27300000
           ELSE                                                         27310000
              IF   I  = WMED                                            27320000
                 MOVE  ENR-ZON-RATIO TO ZON111                          27330000
              ELSE                                                      27340000
                 IF   I  = W3QUART                                      27350000
                     MOVE  ENR-ZON-RATIO TO ZON112.                     27360000
                                                                        27370000
       FIN-TRT-26-A. EXIT.                                              27380000
      ***************************************************************** 27390000
      *              TRT       26 B                                   * 27400000
      ***************************************************************** 27410000
       TRT-26-B.                                                        27420000
                                                                        27430000
           READ FENTREE INTO W-ENR                                      27440000
      *--> LECTURE SUIVANTE ENR DE TYPE 26                              27450000
      *--> SI NON-TROUVE ==> ARRET DU PG                                27460000
                    AT END SET FIN-FICHIER TO TRUE                      27470000
                           SET NOT-OK TO TRUE                           27480000
                           DISPLAY ' ERREUR LECT ENR TYPE 26 '          27490000
                           GO TO FIN-TRT-26-B.                          27500000
           ADD  1 TO CTRLEC.                                            27510000
           IF ENR-TYPE NOT = '26'                                       27520000
                SET NOT-OK TO TRUE                                      27530000
                DISPLAY ' PAS D''ENR TYPE 26 '                          27540000
                GO TO FIN-TRT-26-B.                                     27550000
                                                                        27560000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             27570000
                                                                        27580000
           IF   I = W1QUART                                             27590000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        27600000
              MOVE    CTR7  TO ZON110                                   27610000
           ELSE                                                         27620000
              IF   I  = WMED                                            27630000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     27640000
                 MOVE  CTR7  TO ZON111                                  27650000
              ELSE                                                      27660000
                 IF   I  = W3QUART                                      27670000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 27680000
                     MOVE  CTR7  TO ZON112.                             27690000
                                                                        27700000
       FIN-TRT-26-B. EXIT.                                              27710000
      *                                                                 27720000
      *                                                                 27730000
      ***************************************************************** 27740000
      ***************************************************************** 27750000
      ***************************************************************** 27760000
      *                                                               * 27770000
      *              TRT      VRAC                                    * 27780000
      *                                                               * 27790000
      ***************************************************************** 27800000
      ***************************************************************** 27810000
      ***************************************************************** 27820000
       TRT-VRAC.                                                        27830000
                                                                        27840000
           IF LECT-A-FAIRE                                              27850000
              READ FENTREE INTO W-ENR                                   27860000
                    AT END SET FIN-FICHIER TO TRUE                      27870000
                           GO TO FIN-TRT-VRAC                           27880000
              ADD  1 TO CTRLEC                                          27890000
           END-IF.                                                      27900000
           IF ENR-TYPE NOT = '30'                                       27910000
              SET LECT-DEJA-FAITE TO TRUE                               27920000
              GO TO  FIN-TRT-VRAC                                       27930000
           END-IF.                                                      27940000
                                                                        27950000
           PERFORM TRT-30 THRU FIN-TRT-30.                              27960000
           IF OK                                                        27970000
              PERFORM TRT-31 THRU FIN-TRT-31.                           27980000
           IF OK                                                        27990000
              PERFORM TRT-32 THRU FIN-TRT-32.                           28000000
           IF OK                                                        28010000
              PERFORM TRT-33 THRU FIN-TRT-33                            28020000
              SET LECT-A-FAIRE TO TRUE.                                 28030000
                                                                        28040000
       FIN-TRT-VRAC. EXIT.                                              28050000
      ***************************************************************** 28060000
      *              TRT       30                                     * 28070000
      ***************************************************************** 28080000
       TRT-30.                                                          28090000
                                                                        28100000
           MOVE CTR-NBEMPL-TOT-V    TO ZON114                           28110000
           MOVE CTR-NBEMPL-ATT-V    TO ZON115 CTR115                    28120000
           COMPUTE CTR6 ROUNDED = (CTR-NBEMPL-ATT-V * 100)              28130000
                                        / CTR-NBEMPL-TOT-V              28140000
           MOVE CTR6              TO ZON116                             28150000
           COMPUTE CTR7 = (CTR-NBEMPL-TOT-V  - CTR-NBEMPL-ATT-V)        28160000
           MOVE CTR7              TO ZON117                             28170000
           COMPUTE CTR6 ROUNDED = (CTR7 * 100) / CTR-NBEMPL-TOT-V       28180000
           MOVE CTR6              TO ZON118.                            28190000
                                                                        28200000
           MOVE CTR-NB-REF-V    TO ZON119 CTR119                        28210000
           MOVE CTR-STOCK-V     TO ZON120                               28220000
                                                                        28230000
           WRITE LIGNE      FROM  DETAIL44                              28240000
           WRITE LIGNE      FROM  DETAIL45                              28250000
           WRITE LIGNE      FROM  DETAIL46                              28260000
           WRITE LIGNE      FROM  DETAIL47                              28270000
           WRITE LIGNE      FROM  DETAIL48                              28280000
           WRITE LIGNE      FROM  DETAIL49                              28290000
           WRITE LIGNE      FROM  DETAIL50                              28300000
           WRITE LIGNE      FROM  DETAIL51                              28310000
           WRITE LIGNE      FROM  DETAIL52.                             28320000
           ADD  9 TO CTRECR.                                            28330000
                                                                        28340000
       FIN-TRT-30. EXIT.                                                28350000
      ***************************************************************** 28360000
      *              TRT       31                                     * 28370000
      ***************************************************************** 28380000
       TRT-31.                                                          28390000
                                                                        28400000
           READ FENTREE INTO W-ENR                                      28410000
      *--> LECTURE DU 1ER ENR DE TYPE 31 ( POUR L'ENTREPOT EN COURS )   28420000
      *--> SI NON-TROUVE ==> ARRET DU PG                                28430000
                    AT END SET FIN-FICHIER TO TRUE                      28440000
                           SET NOT-OK TO TRUE                           28450000
                           DISPLAY ' PAS D''ENR TYPE 31 32 ET 33 '      28460000
                           GO TO FIN-TRT-31.                            28470000
           ADD  1 TO CTRLEC.                                            28480000
           IF ENR-TYPE NOT = '31'                                       28490000
                SET NOT-OK TO TRUE                                      28500000
                DISPLAY ' PAS D''ENR TYPE 31 '                          28510000
                GO TO FIN-TRT-31.                                       28520000
                                                                        28530000
           MOVE ENR-ZON-RATIO  TO  ZON122.                              28540000
           MOVE ZERO           TO  CUMUL-MOY.                           28550000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           28560000
                                                                        28570000
      *--> QUOTIENT = NBEMPL-ATT / QUARTILE                             28580000
           DIVIDE CTR115 BY 4 GIVING QUOTIENT                           28590000
                                                                        28600000
           MOVE     QUOT-5  TO    W1QUART                               28610000
           COMPUTE  WMED     =    W1QUART * 2                           28620000
           COMPUTE  W3QUART  =    W1QUART * 3                           28630000
                                                                        28640000
           MOVE ZERO TO ZON123 ZON124 ZON125                            28650000
           IF QUOT-5 < 2                                                28660000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR115           28670000
                 READ FENTREE INTO W-ENR                                28680000
                 ADD  1 TO CTRLEC                                       28690000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        28700000
              END-PERFORM                                               28710000
           ELSE                                                         28720000
              IF QUOT-2  = ZERO                                         28730000
      *----------> SI CTR119 EST MULTIPLE DE 4                          28740000
                  PERFORM TRT-31-A THRU FIN-TRT-31-A                    28750000
                                      VARYING I FROM 2 BY 1             28760000
                                          UNTIL I > CTR115              28770000
              ELSE                                                      28780000
                  PERFORM TRT-31-B THRU FIN-TRT-31-B                    28790000
                                   VARYING I FROM 2 BY 1                28800000
                                          UNTIL I > CTR115.             28810000
                                                                        28820000
           MOVE  ENR-ZON-RATIO TO ZON126                                28830000
           COMPUTE CTR7 = CUMUL-MOY / CTR115                            28840000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      28850000
           MOVE    CTR7 TO ZON121.                                      28860000
           WRITE LIGNE FROM DETAIL53.                                   28870000
           ADD  1 TO CTRECR.                                            28871000
                                                                        28872000
       FIN-TRT-31. EXIT.                                                28873000
      ***************************************************************** 28874000
      *              TRT       31 A                                   * 28875000
      ***************************************************************** 28876000
       TRT-31-A.                                                        28877000
                                                                        28878000
           READ FENTREE INTO W-ENR                                      28879000
      *--> LECTURE SUIVANTE ENR DE TYPE 31                              28880000
      *--> SI NON-TROUVE ==> ARRET DU PG                                28890000
                    AT END SET FIN-FICHIER TO TRUE                      28900000
                           SET NOT-OK TO TRUE                           28910000
                           DISPLAY ' ERREUR LECT ENR TYPE 31 '          28920000
                           GO TO FIN-TRT-31-A.                          28930000
           ADD  1 TO CTRLEC.                                            28940000
           IF ENR-TYPE NOT = '31'                                       28950000
                SET NOT-OK TO TRUE                                      28960000
                DISPLAY ' PAS D''ENR TYPE 31 '                          28970000
                GO TO FIN-TRT-31-A.                                     28980000
                                                                        28990000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             29000000
                                                                        29010000
           IF   I = W1QUART                                             29020000
              MOVE  ENR-ZON-RATIO TO ZON123                             29030000
           ELSE                                                         29040000
              IF   I  = WMED                                            29050000
                 MOVE  ENR-ZON-RATIO TO ZON124                          29060000
              ELSE                                                      29070000
                 IF   I  = W3QUART                                      29080000
                     MOVE  ENR-ZON-RATIO TO ZON125.                     29090000
                                                                        29100000
       FIN-TRT-31-A. EXIT.                                              29110000
      ***************************************************************** 29120000
      *              TRT       31 B                                   * 29130000
      ***************************************************************** 29140000
       TRT-31-B.                                                        29150000
                                                                        29160000
           READ FENTREE INTO W-ENR                                      29170000
      *--> LECTURE SUIVANTE ENR DE TYPE 31                              29180000
      *--> SI NON-TROUVE ==> ARRET DU PG                                29190000
                    AT END SET FIN-FICHIER TO TRUE                      29200000
                           SET NOT-OK TO TRUE                           29210000
                           DISPLAY ' ERREUR LECT ENR TYPE 31 '          29220000
                           GO TO FIN-TRT-31-B.                          29230000
           ADD  1 TO CTRLEC.                                            29240000
           IF ENR-TYPE NOT = '31'                                       29250000
                SET NOT-OK TO TRUE                                      29260000
                DISPLAY ' PAS D''ENR TYPE 31 '                          29270000
                GO TO FIN-TRT-31-B.                                     29280000
                                                                        29290000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             29300000
                                                                        29310000
           IF   I = W1QUART                                             29320000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        29330000
              MOVE    CTR7  TO ZON123                                   29340000
           ELSE                                                         29350000
              IF   I  = WMED                                            29360000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     29370000
                 MOVE  CTR7  TO ZON124                                  29380000
              ELSE                                                      29390000
                 IF   I  = W3QUART                                      29400000
                     PERFORM ROUTINE1  THRU FIN-ROUTINE1                29410000
                     MOVE  CTR7  TO ZON125.                             29420000
                                                                        29430000
                                                                        29440000
       FIN-TRT-31-B. EXIT.                                              29450000
      ***************************************************************** 29460000
      *              TRT       32                                     * 29470000
      ***************************************************************** 29480000
       TRT-32.                                                          29490000
                                                                        29500000
           READ FENTREE INTO W-ENR                                      29510000
      *--> LECTURE DU 1ER ENR DE TYPE 32 ( POUR L'ENTREPOT EN COURS )   29520000
      *--> SI NON-TROUVE ==> ARRET DU PG                                29530000
                    AT END SET FIN-FICHIER TO TRUE                      29540000
                           SET NOT-OK TO TRUE                           29550000
                           DISPLAY ' PAS D''ENR TYPE  32 ET 33 '        29560000
                           GO TO FIN-TRT-32.                            29570000
           ADD  1 TO CTRLEC.                                            29580000
           IF ENR-TYPE NOT = '32'                                       29590000
                SET NOT-OK TO TRUE                                      29600000
                DISPLAY ' PAS D''ENR TYPE 32 '                          29610000
                GO TO FIN-TRT-32.                                       29620000
                                                                        29630000
           MOVE ENR-ZON-RATIO  TO  ZON128.                              29640000
           MOVE ZERO           TO  CUMUL-MOY.                           29650000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           29660000
                                                                        29670000
      *--> QUOTIENT = NB REF   / QUARTILE                               29680000
           DIVIDE CTR119 BY 4 GIVING QUOTIENT                           29690000
                                                                        29700000
           MOVE     QUOT-5  TO    W1QUART                               29710000
           COMPUTE  WMED     =    W1QUART * 2                           29720000
           COMPUTE  W3QUART  =    W1QUART * 3                           29730000
                                                                        29740000
           MOVE ZERO TO ZON129 ZON130 ZON131                            29750000
           IF QUOT-5 < 2                                                29760000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR119           29770000
                 READ FENTREE INTO W-ENR                                29780000
                 ADD  1 TO CTRLEC                                       29790000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        29800000
              END-PERFORM                                               29810000
           ELSE                                                         29820000
              IF QUOT-2  = ZERO                                         29830000
      *----------> SI CTR119 EST MULTIPLE DE 4                          29840000
                  PERFORM TRT-32-A THRU FIN-TRT-32-A                    29850000
                                      VARYING I FROM 2 BY 1             29860000
                                          UNTIL I > CTR119              29870000
              ELSE                                                      29880000
                  PERFORM TRT-32-B THRU FIN-TRT-32-B                    29890000
                                      VARYING I FROM 2 BY 1             29900000
                                          UNTIL I > CTR119.             29910000
                                                                        29920000
           MOVE  ENR-ZON-RATIO TO ZON132                                29930000
           COMPUTE CTR7 = CUMUL-MOY / CTR119                            29940000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      29950000
           MOVE    CTR7 TO ZON127.                                      29960000
           WRITE LIGNE FROM DETAIL54.                                   29970000
           ADD  1 TO CTRECR.                                            29980000
                                                                        29990000
       FIN-TRT-32. EXIT.                                                30000000
      ***************************************************************** 30010000
      *              TRT       32 A                                   * 30020000
      ***************************************************************** 30030000
       TRT-32-A.                                                        30040000
                                                                        30050000
           READ FENTREE INTO W-ENR                                      30060000
      *--> LECTURE SUIVANTE ENR DE TYPE 32                              30070000
      *--> SI NON-TROUVE ==> ARRET DU PG                                30080000
                    AT END SET FIN-FICHIER TO TRUE                      30090000
                           SET NOT-OK TO TRUE                           30100000
                           DISPLAY ' ERREUR LECT ENR TYPE 32 '          30110000
                           GO TO FIN-TRT-32-A.                          30120000
           ADD  1 TO CTRLEC.                                            30130000
           IF ENR-TYPE NOT = '32'                                       30140000
                SET NOT-OK TO TRUE                                      30150000
                DISPLAY ' PAS D''ENR TYPE 32 '                          30160000
                GO TO FIN-TRT-32-A.                                     30170000
                                                                        30180000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             30190000
                                                                        30200000
           IF   I = W1QUART                                             30210000
              MOVE  ENR-ZON-RATIO TO ZON129                             30220000
           ELSE                                                         30230000
              IF   I  = WMED                                            30240000
                 MOVE  ENR-ZON-RATIO TO ZON130                          30250000
              ELSE                                                      30260000
                 IF   I  = W3QUART                                      30270000
                     MOVE  ENR-ZON-RATIO TO ZON131.                     30280000
                                                                        30290000
       FIN-TRT-32-A. EXIT.                                              30300000
      ***************************************************************** 30310000
      *              TRT       32 B                                   * 30320000
      ***************************************************************** 30330000
       TRT-32-B.                                                        30340000
                                                                        30350000
           READ FENTREE INTO W-ENR                                      30360000
      *--> LECTURE SUIVANTE ENR DE TYPE 32                              30370000
      *--> SI NON-TROUVE ==> ARRET DU PG                                30380000
                    AT END SET FIN-FICHIER TO TRUE                      30390000
                           SET NOT-OK TO TRUE                           30400000
                           DISPLAY ' ERREUR LECT ENR TYPE 32 '          30410000
                           GO TO FIN-TRT-32-B.                          30420000
           ADD  1 TO CTRLEC.                                            30430000
           IF ENR-TYPE NOT = '32'                                       30440000
                SET NOT-OK TO TRUE                                      30450000
                DISPLAY ' PAS D''ENR TYPE 32 '                          30460000
                GO TO FIN-TRT-32-B.                                     30470000
                                                                        30480000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             30490000
                                                                        30500000
           IF   I = W1QUART                                             30510000
              PERFORM ROUTINE1  THRU FIN-ROUTINE1                       30520000
              MOVE    CTR7  TO ZON129                                   30530000
           ELSE                                                         30540000
              IF   I  = WMED                                            30550000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     30560000
                 MOVE  CTR7  TO ZON130                                  30570000
              ELSE                                                      30580000
                 IF   I  = W3QUART                                      30590000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 30600000
                     MOVE  CTR7  TO ZON131.                             30610000
                                                                        30620000
                                                                        30630000
       FIN-TRT-32-B. EXIT.                                              30640000
      ***************************************************************** 30650000
      *              TRT       33                                     * 30660000
      ***************************************************************** 30670000
       TRT-33.                                                          30680000
                                                                        30690000
           READ FENTREE INTO W-ENR                                      30700000
      *--> LECTURE DU 1ER ENR DE TYPE 33 ( POUR L'ENTREPOT EN COURS )   30710000
      *--> SI NON-TROUVE ==> ARRET DU PG                                30720000
                    AT END SET FIN-FICHIER TO TRUE                      30730000
                           SET NOT-OK TO TRUE                           30740000
                           DISPLAY ' PAS D''ENR TYPE   33 '             30750000
                           GO TO FIN-TRT-33.                            30760000
           ADD  1 TO CTRLEC.                                            30770000
           IF ENR-TYPE NOT = '33'                                       30780000
                SET NOT-OK TO TRUE                                      30790000
                DISPLAY ' PAS D''ENR TYPE 33 '                          30800000
                GO TO FIN-TRT-33.                                       30810000
                                                                        30820000
           MOVE ENR-ZON-RATIO  TO  ZON134.                              30830000
           MOVE ZERO           TO  CUMUL-MOY.                           30840000
           ADD  ENR-ZON-RATIO  TO  CUMUL-MOY.                           30850000
                                                                        30860000
      *--> QUOTIENT = NB REF   / QUARTILE                               30870000
           DIVIDE CTR119 BY 4 GIVING QUOTIENT                           30880000
                                                                        30890000
           MOVE     QUOT-5  TO    W1QUART                               30900000
           COMPUTE  WMED     =    W1QUART * 2                           30910000
           COMPUTE  W3QUART  =    W1QUART * 3                           30920000
                                                                        30930000
           MOVE ZERO TO ZON135 ZON136 ZON137                            30940000
           IF QUOT-5 < 2                                                30950000
              PERFORM  VARYING I FROM 2 BY 1 UNTIL I > CTR119           30960000
                 READ FENTREE INTO W-ENR                                30970000
                 ADD  1 TO CTRLEC                                       30971000
                 ADD  ENR-ZON-RATIO TO CUMUL-MOY                        30972000
              END-PERFORM                                               30973000
           ELSE                                                         30974000
              IF QUOT-2  = ZERO                                         30975000
      *----------> SI CTR119 EST MULTIPLE DE 4                          30976000
                  PERFORM TRT-33-A THRU FIN-TRT-33-A                    30977000
                                      VARYING I FROM 2 BY 1             30978000
                                          UNTIL I > CTR119              30979000
              ELSE                                                      30980000
                  PERFORM TRT-33-B THRU FIN-TRT-33-B                    30990000
                                      VARYING I FROM 2 BY 1             31000000
                                          UNTIL I > CTR119.             31010000
                                                                        31020000
           MOVE  ENR-ZON-RATIO TO ZON138                                31030000
           COMPUTE CTR7 = CUMUL-MOY / CTR119                            31040000
                  ON SIZE ERROR MOVE ZERO TO CTR7.                      31050000
           MOVE    CTR7 TO ZON133.                                      31060000
           WRITE LIGNE FROM DETAIL55                                    31070000
           WRITE LIGNE FROM DETAIL56                                    31080000
           WRITE LIGNE FROM HENTET57.                                   31090000
           ADD  3 TO CTRECR.                                            31100000
                                                                        31110000
       FIN-TRT-33. EXIT.                                                31120000
      ***************************************************************** 31130000
      *              TRT       33 A                                   * 31140000
      ***************************************************************** 31150000
       TRT-33-A.                                                        31160000
                                                                        31170000
           READ FENTREE INTO W-ENR                                      31180000
      *--> LECTURE SUIVANTE ENR DE TYPE 33                              31190000
      *--> SI NON-TROUVE ==> ARRET DU PG                                31200000
                    AT END SET FIN-FICHIER TO TRUE                      31210000
                           SET NOT-OK TO TRUE                           31220000
                           DISPLAY ' ERREUR LECT ENR TYPE 33 '          31230000
                           GO TO FIN-TRT-33-A.                          31240000
           ADD  1 TO CTRLEC.                                            31250000
           IF ENR-TYPE NOT = '33'                                       31260000
                SET NOT-OK TO TRUE                                      31270000
                DISPLAY ' PAS D''ENR TYPE 33 '                          31280000
                GO TO FIN-TRT-33-A.                                     31290000
                                                                        31300000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             31310000
                                                                        31320000
           IF   I = W1QUART                                             31330000
              MOVE  ENR-ZON-RATIO TO ZON135                             31340000
           ELSE                                                         31350000
              IF   I  = WMED                                            31360000
                 MOVE  ENR-ZON-RATIO TO ZON136                          31370000
              ELSE                                                      31380000
                 IF   I  = W3QUART                                      31390000
                     MOVE  ENR-ZON-RATIO TO ZON137.                     31400000
                                                                        31410000
       FIN-TRT-33-A. EXIT.                                              31420000
      ***************************************************************** 31430000
      *              TRT       33 B                                   * 31440000
      ***************************************************************** 31450000
       TRT-33-B.                                                        31460000
                                                                        31470000
           READ FENTREE INTO W-ENR                                      31480000
      *--> LECTURE SUIVANTE ENR DE TYPE 33                              31490000
      *--> SI NON-TROUVE ==> ARRET DU PG                                31500000
                    AT END SET FIN-FICHIER TO TRUE                      31510000
                           SET NOT-OK TO TRUE                           31520000
                           DISPLAY ' ERREUR LECT ENR TYPE 33 '          31530000
                           GO TO FIN-TRT-33-B.                          31540000
           ADD  1 TO CTRLEC.                                            31550000
           IF ENR-TYPE NOT = '33'                                       31560000
                SET NOT-OK TO TRUE                                      31570000
                DISPLAY ' PAS D''ENR TYPE 33 '                          31580000
                GO TO FIN-TRT-33-B.                                     31590000
                                                                        31600000
           ADD  ENR-ZON-RATIO TO CUMUL-MOY.                             31610000
                                                                        31620000
           IF   I = W1QUART                                             31630000
              PERFORM ROUTINE1 THRU FIN-ROUTINE1                        31640000
              MOVE    CTR7  TO ZON135                                   31650000
           ELSE                                                         31660000
              IF   I  = WMED                                            31670000
                 PERFORM ROUTINE1 THRU FIN-ROUTINE1                     31680000
                 MOVE  CTR7  TO ZON136                                  31690000
              ELSE                                                      31700000
                 IF   I  = W3QUART                                      31710000
                     PERFORM ROUTINE1 THRU FIN-ROUTINE1                 31720000
                     MOVE  CTR7  TO ZON137.                             31730000
                                                                        31740000
       FIN-TRT-33-B. EXIT.                                              31750000
      *                                                                 31760000
      ******************************************************************31770000
      *                                                                 31780000
      *                                                                 31790000
      *                                                                 31800000
      *               ROUTINES    DIVERSES                              31810000
      *                                                                 31820000
      *                                                                 31830000
      *                                                                 31840000
      *                                                                 31850000
      ******************************************************************31860000
      *                                                                 31870000
      *                                                                 31880000
       ROUTINE1.                                                        31890000
                                                                        31900000
              MOVE  ZERO          TO WZON1                              31910000
              MOVE  QUOT-2        TO WZON2                              31920000
              MOVE  ENR-ZON-RATIO TO WRES-RATIO.                        31930000
              READ FENTREE INTO W-ENR                                   31940000
              ADD  1 TO CTRLEC.                                         31950000
              ADD  ENR-ZON-RATIO TO CUMUL-MOY                           31960000
              ADD 1 TO I                                                31970000
              COMPUTE CTR7 ROUNDED  = WRES-RATIO + ((ENR-ZON-RATIO -    31980000
                                WRES-RATIO ) * WZON1  ).                31990000
                                                                        32000000
       FIN-ROUTINE1. EXIT.                                              32010000
