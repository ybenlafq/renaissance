      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.                     BIV137.                          00020000
       AUTHOR. PROJET AIDA.                                             00030000
      *                                                                 00040000
      ******************************************************************00050000
      * PROJET: GESTION INVENTAIRES / EDITION DES ECARTS INVENTAIRE PAR 00060000
      *         RAYON TOUS MAGASINS CONFONDUS                           00070000
      * DATE CREATION: 26/06/90                                         00080000
      * BUT DU PROGRAMME :                                              00090000
      *   EDITION LISTE DES ECARTS D'INVENTAIRE PAR FAMILLE / RAYON /   00100000
      *   POUR TS LES MAGASINS                                          00110000
      *   A PARTIR DU FICHIER FIV130                                    00120000
      *   ET TRIE SUR :  - CODE SOCIETE                                 00130000
      *                  - NUMERO DE SEQUENCE RAYON                     00140000
      *                  - NUMERO DE SEQUENCE FAMILLE                   00150000
      *                                                                 00160000
       ENVIRONMENT DIVISION.                                            00170000
       CONFIGURATION SECTION.                                           00180000
       SPECIAL-NAMES.                                                   00190000
           DECIMAL-POINT IS COMMA.                                      00200000
       INPUT-OUTPUT SECTION.                                            00210000
      *                                                                 00220000
       FILE-CONTROL.                                                    00230000
      *                                                                 00240000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPARM  ASSIGN TO FPARM.                              00250000
      *--                                                                       
            SELECT FPARM  ASSIGN TO FPARM                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00260000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FIV130 ASSIGN TO FIV130.                             00270000
      *--                                                                       
            SELECT FIV130 ASSIGN TO FIV130                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00280000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IIV137 ASSIGN TO IIV137.                             00290000
      *--                                                                       
            SELECT IIV137 ASSIGN TO IIV137                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00300000
       DATA DIVISION.                                                   00310000
       FILE SECTION.                                                    00320000
      *                                                                 00330000
       FD  FIV130                                                       00340000
           RECORDING F                                                  00350000
           BLOCK CONTAINS 0 RECORDS                                     00360000
           LABEL RECORD STANDARD                                        00370000
           DATA RECORD W-FIN130.                                        00380000
      *                                                                 00390000
      **************************************************************    00400000
      * ****************       DESCRIPTION     ******************* *    00410000
      *                 *******   FIV130  *******                  *    00420000
      *              FICHIER DES ECARTS D'INVENTAIRE               *    00430000
      *                 PAR ARTICLE , RAYON,  LIEU DE TRT ,        *    00440000
      *                   SOUS LIEU , MAGASIN                      *    00450000
      *          POUR TRT EDITION DES ETATS D'INVENTAIRES NCG      *    00460000
      *------------------------------------------------------------*    00470000
      *                  FICHIER SAM, LONGUEUR 100                 *    00480000
      **************************************************************    00490000
      *                                                                 00500000
       COPY FIV130.                                                     00510000
      *                                                                 00520000
       FD  IIV137                                                       00530000
           BLOCK CONTAINS 0 RECORDS                                     00540000
           LABEL RECORD STANDARD                                        00550000
           DATA RECORD ENR-IIV137.                                      00560000
      *                                                                 00570000
       01  ENR-IIV137.                                                  00580000
           02  SAUT-IIV137 PIC X(01).                                   00590000
           02  LIG-IIV137  PIC X(132).                                  00600000
      *                                                                 00610000
       FD  FPARM                                                        00620000
           RECORDING F                                                  00630000
           BLOCK CONTAINS 0 RECORDS                                     00640000
           LABEL RECORD STANDARD                                        00650000
           DATA RECORD ENR-FPARM.                                       00660000
      *                                                                 00670000
       01  ENR-FPARM.                                                   00680000
           02  FPARM-TRT   PIC X(03).                                   00690000
           02  FILLER      PIC X(77).                                   00700000
      ***************************************************************** 00710000
       WORKING-STORAGE SECTION.                                         00720000
      ***************************************************************** 00730000
      *                         ZONES   SPECISCQUES   AU   PROGRAMME  * 00740000
      ***************************************************************** 00750000
      *                                                               * 00760000
       77  FILLER           PIC X(20) VALUE '**DEBUT DE WORKING**'.     00770000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  WRET               PIC S9(04) COMP  VALUE ZERO.              00780000
      *--                                                                       
       77  WRET               PIC S9(04) COMP-5  VALUE ZERO.                    
      *}                                                                        
       77  DATHEUR            PIC S9(13) COMP-3 VALUE ZERO.             00790000
      *                                       POURCENTAGE MAXI          00800000
       01  W-PMAX  PIC  9(04)V9(6)         VALUE 0999,999999.           00810000
      *--- VARIABLE TAMPON                                              00800000
PM0305 01  W-TAMPON PIC  S9(11)V9(4) COMP-3 VALUE ZERO.                 00810000
      *                                                                 00820000
       01  W-DONNEES.                                                   00830000
      *                                                                 00840000
      *------------------------------ NB LIGNE PAGE                     00850000
             02 W-LIG-PAG                   PIC 9(2) VALUE ZERO.        00860000
      *------------------------------ NB PAGE                           00870000
             02 W-NUM-PAGE                  PIC 9(3) VALUE ZERO.        00880000
      *                                                                 00890000
      ***************************************************************** 00900000
      *                                                                 00910000
      *------------------------------ CODE SOCIETE MAGASIN              00920000
             02 W-NSOCMAG-REF               PIC X(03) VALUE SPACE.      00930000
      *------------------------------ CODE RAYON                        00940000
             02 W-CRAYON-REF                PIC X(05) VALUE SPACE.      00950000
      *------------------------------ CODE FAMILLE                      00960000
             02 W-CFAM-REF                  PIC X(05) VALUE SPACE.      00970000
      *                                                                 00980000
      ***************************************************************** 00990000
      *        TOTALISATION MAGASIN                                     01000000
      *-------------------------------------  QUANTITE STOCK AVANT      01010000
      *                                       INVENTAIRE                01020000
             02 W-QSTOCKAV-MAG  PIC S9(09)    COMP-3 VALUE ZERO.        01030000
      *-------------------------------------  QUANTITE STOCK APRES N    01040000
      *                                       INVENTAIRE                01050000
             02 W-QSTOCKAP-MAG PIC S9(09)     COMP-3 VALUE ZERO.        01060000
      *-------------------------------------  QUANTITE STOCK EN ECART   01070000
             02 W-QSTOCKEC-MAG PIC S9(09)     COMP-3 VALUE ZERO.        01080000
      *-------------------------------------  VALEUR STOCK AVANT        01090000
      *                                       INVENTAIRE                01100000
             02 W-VSTOCKAV-MAG PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01110000
      *-------------------------------------  VALEUR STOCK APRES        01120000
      *                                       INVENTAIRE                01130000
             02 W-VSTOCKAP-MAG PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01140000
      *-------------------------------------  VALEUR STOCK ECART        01150000
      *                                       INVENTAIRE                01160000
             02 W-VSTOCKEC-MAG PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01170000
      *-------------------------------------  POURCENTAGE ECART         01180000
      *                                       QUANTITE                  01190000
             02 W-PQSTOCKEC-MAG PIC S9(04)V9(06) COMP-3 VALUE ZERO.     01200000
      *-------------------------------------  POURCENTAGE ECART         01210000
      *                                       VALORISE                  01220000
             02 W-PVSTOCKEC-MAG PIC S9(04)V9(06) COMP-3 VALUE ZERO.     01230000
      *-------------------------------------                            01240000
      *        TOTALISATION RAYON                                       01250000
      *-------------------------------------  QUANTITE STOCK AVANT      01260000
      *                                       INVENTAIRE                01270000
             02 W-QSTOCKAV-RAY  PIC S9(09)    COMP-3 VALUE ZERO.        01280000
      *-------------------------------------  QUANTITE STOCK APRES N    01290000
      *                                       INVENTAIRE                01300000
             02 W-QSTOCKAP-RAY PIC S9(09)     COMP-3 VALUE ZERO.        01310000
      *-------------------------------------  QUANTITE STOCK EN ECART   01320000
             02 W-QSTOCKEC-RAY PIC S9(09)     COMP-3 VALUE ZERO.        01330000
      *-------------------------------------  VALEUR STOCK AVANT        01340000
      *                                       INVENTAIRE                01350000
             02 W-VSTOCKAV-RAY PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01360000
      *-------------------------------------  VALEUR STOCK APRES        01370000
      *                                       INVENTAIRE                01380000
             02 W-VSTOCKAP-RAY PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01390000
      *-------------------------------------  VALEUR STOCK ECART        01400000
      *                                       INVENTAIRE                01410000
             02 W-VSTOCKEC-RAY PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01420000
      *-------------------------------------  POURCENTAGE ECART         01430000
      *                                       QUANTITE                  01440000
             02 W-PQSTOCKEC-RAY PIC S9(04)V9(06) COMP-3 VALUE ZERO.     01450000
      *-------------------------------------  POURCENTAGE ECART         01460000
      *                                       VALORISE                  01470000
             02 W-PVSTOCKEC-RAY PIC S9(04)V9(06) COMP-3 VALUE ZERO.     01480000
      *-------------------------------------                            01490000
      *        TOTALISATION FAMILLE                                     01500000
      *-------------------------------------  QUANTITE STOCK AVANT      01510000
      *                                       INVENTAIRE                01520000
             02 W-QSTOCKAV-FAM  PIC S9(09)    COMP-3 VALUE ZERO.        01530000
      *-------------------------------------  QUANTITE STOCK APRES N    01540000
      *                                       INVENTAIRE                01550000
             02 W-QSTOCKAP-FAM PIC S9(09)     COMP-3 VALUE ZERO.        01560000
      *-------------------------------------  QUANTITE STOCK EN ECART   01570000
             02 W-QSTOCKEC-FAM PIC S9(09)     COMP-3 VALUE ZERO.        01580000
      *-------------------------------------  VALEUR STOCK AVANT        01590000
      *                                       INVENTAIRE                01600000
             02 W-VSTOCKAV-FAM PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01610000
      *-------------------------------------  VALEUR STOCK APRES        01620000
      *                                       INVENTAIRE                01630000
             02 W-VSTOCKAP-FAM PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01640000
      *-------------------------------------  VALEUR STOCK ECART        01650000
      *                                       INVENTAIRE                01660000
             02 W-VSTOCKEC-FAM PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01670000
      *-------------------------------------  POURCENTAGE ECART         01680000
      *                                       QUANTITE                  01690000
             02 W-PQSTOCKEC-FAM PIC S9(04)V9(06) COMP-3 VALUE ZERO.     01700000
      *-------------------------------------  POURCENTAGE ECART         01710000
      *                                       VALORISE                  01720000
             02 W-PVSTOCKEC-FAM PIC S9(04)V9(06) COMP-3 VALUE ZERO.     01730000
      *                                                                 01740000
      *-------------------------------------  VALEUR STOCK AVANT        01750000
      *                                       INVENTAIRE                01760000
             02 W-VSTOCKAV     PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01770000
      *-------------------------------------  VALEUR STOCK APRES        01780000
      *                                       INVENTAIRE                01790000
             02 W-VSTOCKAP     PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01800000
      *-------------------------------------  VALEUR STOCK ECART        01810000
      *                                       INVENTAIRE                01820000
             02 W-VSTOCKEC     PIC S9(11)V9(06) COMP-3 VALUE ZERO.      01830000
      *                                                                 01840000
      **************************************************************    01850000
      *                                                                 01860000
      ***************************************************************** 01870000
      ***************************************************************** 01880000
      *                                                               * 01890000
       01  I                            PIC 9(02) VALUE ZERO.           01900000
       01  W-IND-LIG                    PIC 9(02) VALUE ZERO.           01910000
       01  W-IND-PAG                    PIC 9(02) VALUE ZERO.           01920000
      *                                                               * 01930000
       01  W-DATE.                                                      01940000
           02  W-DATE-SS                PIC X(02).                      01950000
           02  W-DATE-AA                PIC X(02).                      01960000
           02  W-DATE-MM                PIC X(02).                      01970000
           02  W-DATE-JJ                PIC X(02).                      01980000
      *                                                               * 01990000
       01  W-DATS.                                                      02000000
           02  W-DATS-AA                PIC X(02).                      02010000
           02  W-DATS-MM                PIC X(02).                      02020000
           02  W-DATS-JJ                PIC X(02).                      02030000
      *                                                               * 02040000
       01  W-DATJ.                                                      02050000
           02  W-DATJ-JJ                PIC X(02).                      02060000
           02  FILLER                   PIC X(01) VALUE '/'.            02070000
           02  W-DATJ-MM                PIC X(02).                      02080000
           02  FILLER                   PIC X(01) VALUE '/'.            02090000
           02  W-DATJ-AA                PIC X(02).                      02100000
      *                                                               * 02110000
      *                                                               * 02120000
       01  W-DJOUR.                                                     02130000
           02  W-DJOUR-JJ               PIC X(02).                      02140000
           02  FILLER                   PIC X(01) VALUE '/'.            02150000
           02  W-DJOUR-MM               PIC X(02).                      02160000
           02  FILLER                   PIC X(01) VALUE '/'.            02170000
           02  W-DJOUR-AA               PIC X(02).                      02180000
      *                                                               * 02190000
       01  W-DAT2.                                                      02200000
           02  W-DAT2-AA                 PIC X(02).                     02210000
           02  W-DAT2-MM                 PIC X(02).                     02220000
           02  W-DAT2-JJ                 PIC X(02).                     02230000
      *                                                               * 02240000
       01  W-LIG-LIBTOTSOC  PIC X(17) VALUE 'TOTAL SOCIETE !  '.        02250000
       01  W-LIG-LIBTOTSOC1 PIC X(17) VALUE 'TOTAL SOC HNA !  '.        02250100
      *                                                               * 02260000
       01  W-LIGNE.                                                     02270000
           02  W-LIG-SAUT               PIC X(01).                      02280000
           02  W-LIG-DONNEE             PIC X(132).                     02290000
      *                                                               * 02300000
       01  W-MESSAGE.                                                   02310000
           02  W-MESSAGE1   PIC X(40).                                  02320000
           02  W-MESSAGE2.                                              02330000
               03  W-MESSAGE21  PIC X(07).                              02340000
               03  W-MESSAGE22  PIC X(07).                              02350000
               03  W-MESSAGE23  PIC X(07).                              02360000
               03  W-MESSAGE24  PIC X(07).                              02370000
               03  W-MESSAGE25  PIC X(07).                              02380000
      *                                                               * 02390000
      ***************************************************************** 02400000
      *                 ZONE COMPTEURS                                * 02410000
      ***************************************************************** 02420000
      *                                                               * 02430000
       01  COMPTEURS.                                                   02440000
           02 W-LUS-FIV130            PIC S9(6) COMP-3 VALUE  +0.       02450000
      *                                                               * 02460000
       01  W-FIV130              PIC X(01)  VALUE '0'.                  02470000
           88 OK-FIV130                     VALUE '1'.                  02480000
           88 FIN-FAMILLE                   VALUE '2'.                  02490000
           88 FIN-FIV130                    VALUE '3'.                  02500000
      *                                                               * 02510000
       01  W-RAYON               PIC X(01)  VALUE '0'.                  02520000
           88 OK-RAYON                      VALUE '1'.                  02530000
      *                                                               * 02540000
       01  W-RUPTSOC             PIC X(01)  VALUE '0'.                  02550000
           88 OK-RUPTSOC                    VALUE '1'.                  02560000
           88 NOK-RUPTSOC                   VALUE '2'.                  02570000
      *                                                               * 02580000
      * -AIDA ********************************************************* 02590000
      *  DESCRIPTION DE L ETAT DES ECARTS INVENTAIRE /MAG RAYON FAMILLE 02600000
      ***************************************************************** 02610000
      *                                                                 02620000
       01  W-LIG-IIV137.                                                02630000
      ***************************************************************** 02640000
      *  ENTETE PHYSIQUE DE PAGE                                        02650000
      ***************************************************************** 02660000
          02 W-LIG-ENTETE1.                                             02670000
            04 FILLER PIC X(02) VALUE SPACES.                           02680000
            04 FILLER PIC X(6) VALUE 'IIV137'.                          02690000
            04 FILLER PIC X(34) VALUE SPACES.                           02700000
            04 FILLER PIC X(20) VALUE ' E T A B L I S S E M'.           02710000
            04 FILLER PIC X(20) VALUE ' E N T S    D A R T '.           02720000
            04 FILLER PIC X(17) VALUE 'Y                '.              02730000
            04 FILLER PIC X(19) VALUE ' E D I T E    L E  '.            02740000
            04 FILLER PIC X(02) VALUE SPACES.                           02750000
            04 W-LIG-DATE PIC X(8).                                     02760000
            04 FILLER PIC X(03) VALUE SPACES.                           02770000
      ***************************************************************** 02780000
          02 W-LIG-ENTETE2.                                             02790000
            04 FILLER PIC X(20) VALUE SPACES.                           02800000
            04 FILLER PIC X(20) VALUE SPACES.                           02810000
            04 FILLER PIC X(20) VALUE SPACES.                           02820000
            04 FILLER PIC X(20) VALUE SPACES.                           02830000
            04 FILLER PIC X(20) VALUE SPACES.                           02840000
            04 FILLER PIC X(20) VALUE SPACES.                           02850000
            04 FILLER PIC X(11) VALUE SPACES.                           02860000
            04 FILLER PIC X(01) VALUE SPACES.                           02870000
      ***************************************************************** 02880000
          02 W-LIG-ENTETE3.                                             02890000
            04 FILLER PIC X(20) VALUE SPACES.                           02900000
            04 FILLER PIC X(21) VALUE SPACES.                           02910000
            04 FILLER PIC X(18) VALUE '    ETAT DES ECART'.             02920000
            04 FILLER PIC X(22) VALUE 'S D INVENTAIRE FAMILLE'.         02930000
            04 FILLER PIC X(20) VALUE SPACES.                           02940000
            04 FILLER PIC X(23) VALUE '                PAGE : '.        02950000
            04 FILLER PIC X(01) VALUE SPACES.                           02960000
            04 W-LIG-PAGE PIC ZZ9.                                      02970000
            04 FILLER PIC X(04) VALUE SPACES.                           02980000
      ***************************************************************** 02990000
          02 W-LIG-ENTETE4.                                             03090000
            04 FILLER   PIC X(20) VALUE SPACES.                         03100000
            04 FILLER   PIC X(20) VALUE SPACES.                         03110000
            04 FILLER   PIC X(16) VALUE SPACES.                         03120000
            04 LIB-ENT4 PIC X(48) VALUE '   SOCIETE   '.                03130000
            04 FILLER   PIC X(20) VALUE SPACES.                         03160000
            04 FILLER   PIC X(08) VALUE SPACES.                         03170000
      ***************************************************************** 03180000
          02 W-LIG-ENTETE5.                                             03190000
            04 FILLER PIC X(02) VALUE '  '.                             03200000
            04 FILLER PIC X(18) VALUE 'DATE INVENTAIRE : '.             03210000
            04 W-LIG-DJOUR     PIC X(8).                                03220000
      ***************************************************************** 03230000
          02 W-LIG-ENTETE6.                                             03240000
            04 FILLER PIC X(02) VALUE '  '.                             03250000
            04 FILLER PIC X(18) VALUE 'SOCIETE         : '.             03260000
            04 W-LIG-NSOCMAG   PIC X(3).                                03270000
      ***************************************************************** 03280000
          02 W-LIG-ENTETE9.                                             03290000
            04 FILLER PIC X(20) VALUE '    +---------------'.           03300000
            04 FILLER PIC X(20) VALUE '--------------------'.           03310000
            04 FILLER PIC X(20) VALUE '--------------------'.           03320000
            04 FILLER PIC X(20) VALUE '--------------------'.           03330000
            04 FILLER PIC X(20) VALUE '--------------------'.           03340000
            04 FILLER PIC X(28) VALUE '---------------------------+'.   03350000
      ***************************************************************** 03360000
          02 W-LIG-ENTETE10.                                            03370000
            04 FILLER PIC X(20) VALUE '    !  RAYON  FAMILL'.           03380000
            04 FILLER PIC X(20) VALUE 'E  !   QTE    VALEUR'.           03390000
            04 FILLER PIC X(20) VALUE ' AVANT    !   QTE   '.           03400000
            04 FILLER PIC X(20) VALUE ' VALEUR APRES    !  '.           03410000
            04 FILLER PIC X(20) VALUE '  ECART    ECART    '.           03420000
            04 FILLER PIC X(28) VALUE '!      ECART       ECART   !'.   03430000
      ***************************************************************** 03440000
          02 W-LIG-ENTETE11.                                            03450000
            04 FILLER PIC X(20) VALUE '    !               '.           03460000
            04 FILLER PIC X(20) VALUE '   !  AVANT    INVEN'.           03470000
            04 FILLER PIC X(20) VALUE 'TAIRE     !  APRES  '.           03480000
            04 FILLER PIC X(20) VALUE '  INVENTAIRE     !  '.           03490000
            04 FILLER PIC X(20) VALUE '   QTE     QTE %    '.           03500000
            04 FILLER PIC X(28) VALUE '!    VALORISE      VALO %  !'.   03510000
      ***************************************************************** 03520000
          02 W-LIG-ENTETE12.                                            03530000
            04 FILLER PIC X(20) VALUE '    !---------------'.           03540000
            04 FILLER PIC X(20) VALUE '---!----------------'.           03550000
            04 FILLER PIC X(20) VALUE '----------!---------'.           03560000
            04 FILLER PIC X(20) VALUE '-----------------!--'.           03570000
            04 FILLER PIC X(20) VALUE '--------------------'.           03580000
            04 FILLER PIC X(28) VALUE '!--------------------------!'.   03590000
      ***************************************************************** 03600000
          02 W-LIG-ENTETE13.                                            03610000
            04 FILLER PIC X(20) VALUE '    !               '.           03620000
            04 FILLER PIC X(20) VALUE '   !                '.           03630000
            04 FILLER PIC X(20) VALUE '          !         '.           03640000
            04 FILLER PIC X(20) VALUE '                 !  '.           03650000
            04 FILLER PIC X(20) VALUE '                    '.           03660000
            04 FILLER PIC X(28) VALUE '!                          !'.   03670000
      ***************************************************************** 03680000
      ***************************************************************** 03690000
          02 W-LIG-DETAIL.                                              03700000
            04 FILLER PIC X(09) VALUE '    !    '.                      03710000
            04 W-LIG-CRAYON         PIC X(05).                          03720000
            04 FILLER PIC X(02) VALUE '  '.                             03730000
            04 W-LIG-CFAM           PIC X(05).                          03740000
            04 FILLER PIC X(03) VALUE '  !'.                            03750000
            04 W-LIG-QSTOCKAV-FAM   PIC -------9.                       03760000
            04 FILLER PIC X(01) VALUE SPACES.                           03770000
            04 W-LIG-VSTOCKAV-FAM   PIC -----------9,99.                03780000
            04 FILLER PIC X(03) VALUE '  !'.                            03790000
            04 W-LIG-QSTOCKAP-FAM   PIC -------9.                       03800000
            04 FILLER PIC X(01) VALUE SPACES.                           03810000
            04 W-LIG-VSTOCKAP-FAM   PIC -----------9,99.                03820000
            04 FILLER PIC X(04) VALUE '  !'.                            03830000
            04 W-LIG-QSTOCKEC-FAM   PIC -------9.                       03840000
            04 FILLER PIC X(02) VALUE SPACES.                           03850000
            04 W-LIG-PQSTOCKEC-FAM  PIC ----9,99.                       03860000
            04 FILLER PIC X(04) VALUE '   !'.                           03870000
            04 W-LIG-VSTOCKEC-FAM   PIC ----------9,99.                 03880000
            04 FILLER PIC X(02) VALUE SPACES.                           03890000
            04 W-LIG-PVSTOCKEC-FAM  PIC ----9,99.                       03900000
            04 FILLER PIC X(03) VALUE '  !'.                            03910000
      ***************************************************************** 03920000
      *                                                                 03930000
          02 W-LIG-RAYON.                                               03940000
            04 FILLER PIC X(09) VALUE '    !    '.                      03950000
            04 FILLER PIC X(07) VALUE 'TOTAL  '.                        03960000
            04 W-LIG-CRAYONTOT      PIC X(05).                          03970000
            04 FILLER PIC X(03) VALUE '  !'.                            03980000
            04 W-LIG-QSTOCKAV-RAY   PIC -------9.                       03990000
            04 FILLER PIC X(01) VALUE SPACES.                           04000000
            04 W-LIG-VSTOCKAV-RAY   PIC -----------9,99.                04010000
            04 FILLER PIC X(03) VALUE '  !'.                            04020000
            04 W-LIG-QSTOCKAP-RAY   PIC -------9.                       04030000
            04 FILLER PIC X(01) VALUE SPACES.                           04040000
            04 W-LIG-VSTOCKAP-RAY   PIC -----------9,99.                04050000
            04 FILLER PIC X(03) VALUE '  !'.                            04060000
            04 W-LIG-QSTOCKEC-RAY   PIC -------9.                       04070000
            04 FILLER PIC X(03) VALUE SPACES.                           04080000
            04 W-LIG-PQSTOCKEC-RAY  PIC ----9,99.                       04090000
            04 FILLER PIC X(04) VALUE '   !'.                           04100000
            04 W-LIG-VSTOCKEC-RAY   PIC ----------9,99.                 04110000
            04 FILLER PIC X(02) VALUE SPACES.                           04120000
            04 W-LIG-PVSTOCKEC-RAY  PIC ----9,99.                       04130000
            04 FILLER PIC X(03) VALUE '  !'.                            04140000
      ***************************************************************** 04150000
          02 W-LIG-MAGASIN.                                             04160000
            04 FILLER PIC X(09) VALUE '    !    '.                      04170000
            04 W-LIG-LIBTOTMAG  PIC X(15) VALUE 'TOTAL MAGASIN !'.      04180000
            04 W-LIG-QSTOCKAV-MAG   PIC -------9.                       04190000
            04 FILLER PIC X(01) VALUE SPACES.                           04200000
            04 W-LIG-VSTOCKAV-MAG   PIC -----------9,99.                04210000
            04 FILLER PIC X(03) VALUE '  !'.                            04220000
            04 W-LIG-QSTOCKAP-MAG   PIC -------9.                       04230000
            04 FILLER PIC X(01) VALUE SPACES.                           04240000
            04 W-LIG-VSTOCKAP-MAG   PIC -----------9,99.                04250000
            04 FILLER PIC X(04) VALUE '  !'.                            04260000
            04 W-LIG-QSTOCKEC-MAG   PIC -------9.                       04270000
            04 FILLER PIC X(02) VALUE SPACES.                           04280000
            04 W-LIG-PQSTOCKEC-MAG  PIC ----9,99.                       04290000
            04 FILLER PIC X(04) VALUE '   !'.                           04300000
            04 W-LIG-VSTOCKEC-MAG   PIC ----------9,99.                 04310000
            04 FILLER PIC X(02) VALUE SPACES.                           04320000
            04 W-LIG-PVSTOCKEC-MAG  PIC ----9,99.                       04330000
            04 FILLER PIC X(03) VALUE '  !'.                            04340000
      *                                                                 04350000
      *                                                                 04360000
      ***************************************************************** 04370000
      *   ZONES PLANTAGES                                             * 04380000
      ***************************************************************** 04390000
           COPY ABENDCOP.                                               04400000
      *                                                                 04410000
      * -AIDA ********************************************************* 04420000
      *   ZONES GENERALES OBLIGATOIRES                                * 04430000
      ***************************************************************** 04440000
      *                                                                 04450000
       01   FILLER    PIC X(16)    VALUE      '*** Z-INOUT ****'.       04460000
       01   Z-INOUT   PIC X(4096)  VALUE      SPACES.                   04470000
      *                                                                 04480000
      ***************************************************************** 04490000
      *     ZONE  DE  GESTION  DES  ERREURS                           * 04500000
      ***************************************************************** 04510000
      *                                                               * 04520000
           COPY SYBWERRO.                                               04530000
      *                                                                 04540000
           COPY  SYBWDIV0.                                              04550000
      *                                                                 04560000
           COPY WORKDATC.                                               04570000
      *                                                               * 04580000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04590000
      *                                                                 04600000
       PROCEDURE DIVISION.                                              04610000
      *                                                                 04620000
      ***************************************************************** 04630000
      *              T R A M E   DU   P R O G R A M M E               * 04640000
      ***************************************************************** 04650000
      *                                                                 04660000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04670000
      *                                                               * 04680000
      *           -----------------------------------------           * 04690000
      *           - MODULE DE BASE DE LA TRANSACTION GD00 -           * 04700000
      *           -----------------------------------------           * 04710000
      *                               I                               * 04720000
      *           -----------------------------------------           * 04730000
      *           I                   I                   I           * 04740000
      *   -----------------   -----------------   -----------------   * 04750000
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   * 04760000
      *   -----------------   -----------------   -----------------   * 04770000
      *                                                               * 04780000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 04790000
      *                                                                 04800000
       MODULE-BIV137                  SECTION.                          04810000
      *                                                                 04820000
           PERFORM MODULE-ENTREE.                                       04830000
      *                                                                 04840000
           PERFORM MODULE-TRAITEMENT.                                   04850000
      *                                                                 04860000
           PERFORM MODULE-SORTIE.                                       04870000
      *                                                                 04880000
       FIN-MODULE-BIV137.  EXIT.                                        04890000
      *                                                                 04900000
      * -AIDA ********************************************************* 04910000
      *                                                               * 04920000
      *  EEEEEEEEE NN     NN TTTTTTTTT RRRRRRRR  EEEEEEEEE EEEEEEEEE  * 04930000
      *  EEEEEEEEE NNN    NN TTTTTTTTT RRRRRRRRR EEEEEEEEE EEEEEEEEE  * 04940000
      *  EEE       NNN    NN    TTT    RRR   RRR EEE       EEE        * 04950000
      *  EEE       NNNN   NN    TTT    RRR   RRR EEE       EEE        * 04960000
      *  EEEEEEEEE NN NN  NN    TTT    RRRRRRRRR EEEEEEEEE EEEEEEEEE  * 04970000
      *  EEEEEEEEE NN  NN NN    TTT    RRRRRRRR  EEEEEEEEE EEEEEEEEE  * 04980000
      *  EEE       NN   NNNN    TTT    RRR RRRR  EEE       EEE        * 04990000
      *  EEE       NN    NNN    TTT    RRR   RR  EEE       EEE        * 05000000
      *  EEEEEEEEE NN    NNN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  * 05010000
      *  EEEEEEEEE NN     NN    TTT    RRR    RR EEEEEEEEE EEEEEEEEE  * 05020000
      *                                                               * 05030000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05040000
      *                                                               * 05050000
      *                    -----------------------                    * 05060000
      *                    -   MODULE D'ENTREE   -                    * 05070000
      *                    -----------------------                    * 05080000
      *                               -                               * 05090000
      *           -----------------------------------------           * 05100000
      *                                                               * 05110000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05120000
       MODULE-ENTREE              SECTION.                              05130000
      *                                                                 05140000
           MOVE   'BIV137'     TO   ABEND-PROG.                         05150000
           OPEN INPUT FIV130.                                           05160000
           OPEN INPUT FPARM.                                            05170000
           OPEN OUTPUT IIV137.                                          05180000
      *                                                                 05190000
           ACCEPT W-DATS    FROM DATE.                                  05200000
           MOVE W-DATS-JJ  TO W-DATJ-JJ.                                05210000
           MOVE W-DATS-MM  TO W-DATJ-MM.                                05220000
           MOVE W-DATS-AA  TO W-DATJ-AA.                                05230000
           INITIALIZE W-MESSAGE.                                        05240000
           INITIALIZE W-DONNEES.                                        05250000
           READ FPARM.                                                  05260000
           IF FPARM-TRT = 'MAG'                                         05260100
                 MOVE 'TOUS MAGASINS'         TO LIB-ENT4               05260200
           ELSE                                                         05260300
              IF FPARM-TRT = 'HNA'                                      05260500
                 MOVE 'SOCIETE HORS DEPOTS NATIONAUX'    TO LIB-ENT4    05260600
                 MOVE W-LIG-LIBTOTSOC1 TO W-LIG-LIBTOTMAG               05260601
              ELSE                                                      05260602
                 MOVE W-LIG-LIBTOTSOC TO W-LIG-LIBTOTMAG                05260610
              END-IF                                                    05260700
           END-IF.                                                      05260800
      *                                                                 05270000
       FIN-MODULE-ENTREE.   EXIT.                                       05280000
      *                                                                 05290000
      * -AIDA ********************************************************* 05300000
      *                                                               * 05310000
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  * 05320000
      *  TTTTTTTTT RRRRRRRR  AAAAAAAAA IIIIIIIII TTTTTTTTT EEEEEEEEE  * 05330000
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        * 05340000
      *     TTT    RRR   RRR AAA   AAA    III       TTT    EEE        * 05350000
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  * 05360000
      *     TTT    RRRRRRRR  AAAAAAAAA    III       TTT    EEEEEEEEE  * 05370000
      *     TTT    RRR RRRR  AAA   AAA    III       TTT    EEE        * 05380000
      *     TTT    RRR   RR  AAA   AAA    III       TTT    EEE        * 05390000
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  * 05400000
      *     TTT    RRR   RRR AAA   AAA IIIIIIIII    TTT    EEEEEEEEE  * 05410000
      *                                                               * 05420000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05430000
      * MODULE DE TRAITEMENT                                          * 05440000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05450000
      *                                                                 05460000
       MODULE-TRAITEMENT          SECTION.                              05470000
      *                                                                 05480000
           PERFORM TRT-LISTE-FIV130 UNTIL FIN-FIV130.                   05490000
      *                                                                 05500000
       FIN-MODULE-TRAITEMENT. EXIT.                                     05510000
      *                                                                 05520000
      *================================================================ 05530000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05540000
      * LECTURE MVTS ECARTS D'INVENTAIRE PAR ARTICLE FIV130             05550000
      * MISE EN PAGE ET EDITION LISTE DES ECARTS PAR FAMILLE / RAYON  * 05560000
      *    CLASSES PAR : CODE SOCIETE         ( AVEC RUPTURE PAGE )   * 05570000
      *                  N� SEQUENCE RAYON   ( AVEC RUPTURE LIGNE )   * 05580000
      *                  N� SEQUENCE FAMILLE                          * 05590000
      * MISE EN PAGE ET EDITION TOTAUX PAR FAMILLE                    * 05600000
      * MISE EN PAGE ET EDITION TOTAUX PAR RAYON                      * 05610000
      * MISE EN PAGE ET EDITION TOTAUX POUR TOUS MAGASIN              * 05620000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05630000
      *                                                                 05640000
       TRT-LISTE-FIV130               SECTION.                          05650000
      *                                                                 05660000
           PERFORM TRT-LECT-FIV130 UNTIL (FIN-FAMILLE OR FIN-FIV130).   05670000
      *                                                                 05680000
           IF FIN-FAMILLE                                               05690000
              PERFORM TRT-EDIT-LIGFAM                                   05700000
              PERFORM TRT-EDIT-ENTETE                                   05710000
           END-IF.                                                      05720000
      *                                                                 05730000
           IF FIN-FIV130                                                05740000
              PERFORM TRT-LIGNE-FAMILLE                                 05750000
              PERFORM TRT-FIN-FAMILLE                                   05760000
              PERFORM TRT-EDIT-TOTRAY                                   05770000
              PERFORM TRT-EDIT-TOTMAG                                   05780000
              PERFORM TRT-ENTETE-LIGNE9                                 05790000
              PERFORM TRT-EDIT-LIGNE                                    05800000
           END-IF.                                                      05810000
      *                                                                 05820000
       FIN-TRT-LISTE-FIV130. EXIT.                                      05830000
      *                                                                 05840000
      *================================================================ 05850000
      *  ==>  TRT-LISTE-FIV130                                          05860000
      *                                                               * 05870000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05880000
      * LECTURE   FIV130  ET CUMUL PAR FAMILLE                        * 05890000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 05900000
      *                                                                 05910000
       TRT-LECT-FIV130              SECTION.                            05920000
      *                                                                 05930000
           READ FIV130                                                  05940000
                AT END MOVE '3' TO W-FIV130.                            05950000
      *                                                                 05960000
           IF W-FIV130 NOT = '3'                                        05970000
              ADD 1 TO W-LUS-FIV130                                     05980000
              IF F130-RAYON = SPACE OR LOW-VALUE                        05990000
                 MOVE 'XXXXX' TO F130-RAYON                             06000000
              END-IF                                                    06010000
              IF F130-CFAM = SPACE OR LOW-VALUE                         06020000
                 MOVE 'XXXXX' TO F130-CFAM                              06030000
              END-IF                                                    06040000
      *       DISPLAY ' MAGASIN ' F130-NSOCMAG                          06050000
      *                       ' ' F130-NMAG                             06060000
      *               ' RAYON '   F130-CRAYON                           06070000
      *               ' FAMILLE ' F130-CFAM                             06080000
      *               ' CODIC '   F130-NCODIC                           06090000
              IF (F130-CFAM NOT = W-CFAM-REF                            06100000
                  AND W-CFAM-REF NOT = SPACE )                          06110000
                 OR ( F130-NSOCMAG NOT = W-NSOCMAG-REF                  06120000
                     AND W-NSOCMAG-REF NOT = SPACE )                    06130000
                    OR ( F130-RAYON NOT = W-CRAYON-REF                  06140000
                      AND W-CRAYON-REF NOT = SPACE )                    06150000
                 MOVE '2' TO W-FIV130                                   06160000
              ELSE                                                      06170000
                 IF W-CRAYON-REF = SPACE                                06180000
                    MOVE '1' TO W-RAYON                                 06190000
                 END-IF                                                 06200000
                 PERFORM TRT-CUMUL-FAMILLE                              06210000
              END-IF                                                    06220000
           END-IF.                                                      06230000
      *                                                                 06240000
           IF W-LUS-FIV130 = 1                                          06250000
              MOVE F130-DINVENTAIRE TO W-DATE                           06260000
              MOVE W-DATE-JJ        TO  W-DJOUR-JJ                      06270000
              MOVE W-DATE-MM        TO  W-DJOUR-MM                      06280000
              MOVE W-DATE-AA        TO  W-DJOUR-AA                      06290000
              PERFORM TRT-EDIT-ENTETE                                   06300000
           END-IF.                                                      06310000
      *                                                                 06320000
       FIN-TRT-LECT-FIV130. EXIT.                                       06330000
      *                                                               * 06340000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06350000
      * TRAITEMENT EDITION ENTETE (RUPTURE MAGASIN)                   * 06360000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06370000
      *                                                                 06380000
       TRT-EDIT-ENTETE                SECTION.                          06390000
      *                                                                 06400000
      *                                                                 06410000
           IF ( F130-NSOCMAG NOT = W-NSOCMAG-REF                        06420000
                AND W-NSOCMAG-REF NOT = SPACE )                         06430000
                          IF W-LIG-PAG = ZERO                           06440000
                             PERFORM TRT-EDIT-ENTETE1                   06450000
                          END-IF                                        06460000
                          PERFORM TRT-EDIT-TOTMAG                       06470000
           END-IF.                                                      06480000
      *                                                                 06490000
      *                                                                 06500000
           IF F130-NSOCMAG NOT = W-NSOCMAG-REF                          06510000
              OR  W-NSOCMAG-REF = SPACE                                 06520000
                                   PERFORM TRT-RUPT-MAG                 06530000
           END-IF.                                                      06540000
      *                                                                 06550000
           IF OK-RUPTSOC                                                06560000
                  PERFORM TRT-EDIT-ENTETE1                              06570000
           END-IF.                                                      06580000
      *                                                               * 06590000
       FIN-TRT-EDIT-ENTETE. EXIT.                                       06600000
      *                                                               * 06610000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06620000
      * TRAITEMENT EDITION LIGNE FAMILLE ET TOTAUX RAYON              * 06630000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 06640000
      *                                                                 06650000
       TRT-EDIT-LIGFAM               SECTION.                           06660000
      *                                                                 06670000
           IF W-LIG-PAG = ZERO                                          06680000
                  PERFORM TRT-EDIT-ENTETE1                              06690000
           END-IF.                                                      06700000
      *                                                                 06710000
           PERFORM TRT-LIGNE-FAMILLE.                                   06720000
      *                                                                 06730000
           PERFORM TRT-FIN-FAMILLE.                                     06740000
      *                                                                 06750000
           IF ( F130-RAYON NOT = W-CRAYON-REF                           06760000
                 AND W-CRAYON-REF NOT = SPACE )                         06770000
              OR ( F130-NSOCMAG NOT = W-NSOCMAG-REF                     06780000
                   AND W-NSOCMAG-REF NOT = SPACE )                      06790000
                          IF W-LIG-PAG = ZERO                           06800000
                             PERFORM TRT-EDIT-TOTRAY                    06810000
                             PERFORM TRT-EDIT-ENTETE1                   06820000
                          ELSE                                          06830000
                             IF F130-NSOCMAG = W-NSOCMAG-REF            06840000
                                PERFORM TRT-EDIT-TOTRAY                 06850000
                                PERFORM TRT-EDIT-ENTETE1                06860000
                             ELSE                                       06870000
                                PERFORM TRT-EDIT-TOTRAY                 06880000
                             END-IF                                     06890000
                          END-IF                                        06900000
           END-IF.                                                      06910000
      *                                                                 06920000
           PERFORM TRT-CUMUL-FAMILLE.                                   06930000
      *                                                                 06940000
       FIN-TRT-EDIT-LIGFAM. EXIT.                                       06950000
      *                                                               * 06960000
      *                                                               * 06970000
      *================================================================ 06980000
      *                                                               * 06990000
      * ==>    TRT-EDIT-ENTETE                                          07000000
      *                                                                 07010000
      *================================================================ 07020000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07030000
      * RUPTURE   MAGASINS        ( EDITION TOTAUX SOCIETE PRECEDENTE * 07040000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07050000
       TRT-EDIT-TOTMAG    SECTION.                                      07060000
      *                                                                 07070000
      *                                                                 07080000
           PERFORM TRT-MAGASIN-LIGNE.                                   07090000
           PERFORM TRT-EDIT-LIGNE.                                      07100000
      *                                                                 07110000
           IF W-LIG-PAG > 55                                            07120000
              MOVE ZERO TO W-LIG-PAG                                    07130000
           END-IF.                                                      07140000
      *                                                                 07150000
       FIN-TRT-EDIT-TOTMAG. EXIT.                                       07160000
      *                                                                 07170000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07180000
      *                                                                 07190000
       TRT-MAGASIN-LIGNE        SECTION.                                07200000
      *                                                                 07210000
PM0305     MOVE 0               TO W-TAMPON.                            07330000
           MOVE SPACE           TO W-LIGNE.                             07220000
           MOVE W-QSTOCKAV-MAG  TO W-LIG-QSTOCKAV-MAG.                  07230000
           MOVE W-QSTOCKAP-MAG  TO W-LIG-QSTOCKAP-MAG.                  07240000
           MOVE W-QSTOCKEC-MAG  TO W-LIG-QSTOCKEC-MAG.                  07250000
           MOVE W-VSTOCKAV-MAG  TO W-LIG-VSTOCKAV-MAG.                  07260000
           MOVE W-VSTOCKAP-MAG  TO W-LIG-VSTOCKAP-MAG.                  07270000
           MOVE W-VSTOCKEC-MAG  TO W-LIG-VSTOCKEC-MAG.                  07280000
           IF W-QSTOCKAV-MAG NOT = ZERO                                 07290000
              COMPUTE W-PQSTOCKEC-MAG = W-QSTOCKEC-MAG /                07300000
                                        ( W-QSTOCKAV-MAG / 100)         07310000
           END-IF.                                                      07320000
PM0305     MOVE W-VSTOCKAV-MAG   TO W-TAMPON.                           07330000
  "        IF W-TAMPON NOT = ZERO                                       07330000
PM0305*    IF W-VSTOCKAV-MAG NOT = ZERO                                 07330000
              COMPUTE W-PVSTOCKEC-MAG = W-VSTOCKEC-MAG /                07340000
PM0305                                  ( W-TAMPON / 100)               07350000
  "   *                                 ( W-VSTOCKAV-MAG / 100)         07350000
  "        ELSE                                                         07360000
PM0305        MOVE 0              TO W-PVSTOCKEC-MAG                    07360000
           END-IF.                                                      07360000
           IF W-QSTOCKAV-MAG < ZERO                                     07370000
              COMPUTE W-PQSTOCKEC-MAG = W-QSTOCKEC-MAG /                07380000
                                      ( ( W-QSTOCKAV-MAG * -1 ) / 100)  07390000
           END-IF.                                                      07400000
           IF W-VSTOCKAV-MAG < ZERO                                     07410000
              COMPUTE W-PVSTOCKEC-MAG = W-VSTOCKEC-MAG /                07420000
                                      ( ( W-VSTOCKAV-MAG * -1 ) / 100)  07430000
           END-IF.                                                      07440000
           IF W-QSTOCKAV-MAG = ZERO                                     07450000
              AND W-QSTOCKAP-MAG = ZERO                                 07460000
                  MOVE ZERO TO W-PQSTOCKEC-MAG                          07470000
                  MOVE ZERO TO W-PVSTOCKEC-MAG                          07480000
           END-IF.                                                      07490000
           MOVE W-PQSTOCKEC-MAG TO W-LIG-PQSTOCKEC-MAG.                 07500000
           MOVE W-PVSTOCKEC-MAG TO W-LIG-PVSTOCKEC-MAG.                 07510000
           IF W-QSTOCKAV-MAG = ZERO                                     07520000
              AND W-QSTOCKAP-MAG NOT = ZERO                             07530000
                  MOVE W-PMAX TO W-LIG-PQSTOCKEC-MAG                    07540000
                  MOVE W-PMAX TO W-LIG-PVSTOCKEC-MAG                    07550000
           END-IF.                                                      07560000
           ADD 1                TO W-LIG-PAG.                           07600000
           MOVE ' '             TO W-LIG-SAUT.                          07610000
           MOVE W-LIG-MAGASIN   TO W-LIG-DONNEE.                        07620000
      *                                                                 07630000
           INITIALIZE              W-QSTOCKAV-MAG                       07640000
                                   W-QSTOCKAP-MAG                       07650000
                                   W-QSTOCKEC-MAG                       07660000
                                   W-VSTOCKAV-MAG                       07670000
                                   W-VSTOCKAP-MAG                       07680000
                                   W-VSTOCKEC-MAG                       07690000
                                   W-PQSTOCKEC-MAG                      07700000
                                   W-PVSTOCKEC-MAG.                     07710000
      *                                                                 07720000
       FIN-TRT-MAGASIN-LIGNE. EXIT.                                     07730000
      *                                                                 07740000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07750000
      * RUPTURE   SOCIETE                                             * 07760000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07770000
      *                                                                 07780000
       TRT-RUPT-MAG                SECTION.                             07790000
      *                                                                 07800000
           IF F130-NSOCMAG NOT = W-NSOCMAG-REF                          07810000
              OR  W-NSOCMAG-REF = SPACE                                 07820000
              MOVE F130-NSOCMAG TO W-NSOCMAG-REF                        07830000
              MOVE '1'          TO W-RUPTSOC                            07840000
           END-IF.                                                      07850000
      * --------------------------------  FIN DE PAGE                   07860000
      *                                                                 07870000
           IF W-LIG-PAG > ZERO                                          07880000
              PERFORM TRT-ENTETE-LIGNE9                                 07890000
              PERFORM TRT-EDIT-LIGNE                                    07900000
           END-IF.                                                      07910000
      *                                                                 07920000
           MOVE ZERO        TO W-LIG-PAG.                               07930000
      *                                                                 07940000
       FIN-TRT-RUPT-MAG. EXIT.                                          07950000
      *                                                                 07960000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 07970000
      *================================================================ 07980000
      *================================================================ 07990000
      * ==>    TRT-EDIT-LIGFAM                                          08000000
      *================================================================ 08010000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 08020000
      * RUPTURE   RAYON                                               * 08030000
      * EDITION LIGNE  RAYON                                          * 08040000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 08050000
      *                                                                 08060000
       TRT-EDIT-TOTRAY    SECTION.                                      08070000
      *                                                                 08080000
           PERFORM TRT-ENTETE-LIGNE13.                                  08090000
           PERFORM TRT-EDIT-LIGNE.                                      08100000
           PERFORM TRT-ENTETE-LIGNE12.                                  08110000
           PERFORM TRT-EDIT-LIGNE.                                      08120000
      *                                                                 08130000
           PERFORM TRT-RAYON-LIGNE.                                     08140000
           PERFORM TRT-EDIT-LIGNE.                                      08150000
      *                                                                 08160000
           PERFORM TRT-ENTETE-LIGNE12.                                  08170000
           PERFORM TRT-EDIT-LIGNE.                                      08180000
      *                                                                 08190000
           IF W-LIG-PAG > 55                                            08200000
              MOVE ZERO TO W-LIG-PAG                                    08210000
           END-IF.                                                      08220000
      *                                                                 08230000
       FIN-TRT-EDIT-TOTRAY. EXIT.                                       08240000
      *                                                                 08250000
      *                                                                 08260000
       TRT-RAYON-LIGNE        SECTION.                                  08270000
      *                                                                 08280000
           ADD  W-QSTOCKAV-RAY       TO W-QSTOCKAV-MAG.                 08290000
           ADD  W-QSTOCKAP-RAY       TO W-QSTOCKAP-MAG.                 08300000
           ADD  W-QSTOCKEC-RAY       TO W-QSTOCKEC-MAG.                 08310000
      *                                                                 08320000
           ADD  W-VSTOCKAV-RAY       TO W-VSTOCKAV-MAG.                 08330000
           ADD  W-VSTOCKAP-RAY       TO W-VSTOCKAP-MAG.                 08340000
           ADD  W-VSTOCKEC-RAY       TO W-VSTOCKEC-MAG.                 08350000
      *                                                                 08360000
PM0305     MOVE 0               TO W-TAMPON.                            07330000
           MOVE SPACE           TO W-LIGNE.                             08370000
           IF W-CRAYON-REF NOT = 'XXXXX'                                08380000
              MOVE W-CRAYON-REF    TO W-LIG-CRAYONTOT                   08390000
           ELSE                                                         08400000
              MOVE SPACE           TO W-LIG-CRAYONTOT                   08410000
           END-IF.                                                      08420000
           MOVE W-QSTOCKAV-RAY  TO W-LIG-QSTOCKAV-RAY.                  08430000
           MOVE W-QSTOCKAP-RAY  TO W-LIG-QSTOCKAP-RAY.                  08440000
           MOVE W-QSTOCKEC-RAY  TO W-LIG-QSTOCKEC-RAY.                  08450000
           MOVE W-VSTOCKAV-RAY  TO W-LIG-VSTOCKAV-RAY.                  08460000
           MOVE W-VSTOCKAP-RAY  TO W-LIG-VSTOCKAP-RAY.                  08470000
           MOVE W-VSTOCKEC-RAY  TO W-LIG-VSTOCKEC-RAY.                  08480000
           IF W-QSTOCKAV-RAY NOT = ZERO                                 08490000
              COMPUTE W-PQSTOCKEC-RAY = W-QSTOCKEC-RAY /                08500000
                                        ( W-QSTOCKAV-RAY / 100)         08510000
           END-IF.                                                      08520000
PM0305     MOVE W-VSTOCKAV-RAY   TO W-TAMPON.                           07330000
  "        IF W-TAMPON NOT = ZERO                                       07330000
PM0305*    IF W-VSTOCKAV-RAY NOT = ZERO                                 08530000
              COMPUTE W-PVSTOCKEC-RAY = W-VSTOCKEC-RAY /                08540000
PM0305*                                 ( W-VSTOCKAV-RAY / 100)         08550000
  "                                     ( W-TAMPON / 100)               07350000
  "        ELSE                                                         07360000
PM0305        MOVE 0              TO W-PVSTOCKEC-RAY                    07360000
           END-IF.                                                      08560000
           IF W-QSTOCKAV-RAY < ZERO                                     08570000
              COMPUTE W-PQSTOCKEC-RAY = W-QSTOCKEC-RAY /                08580000
                                      ( ( W-QSTOCKAV-RAY * -1 ) / 100)  08590000
           END-IF.                                                      08600000
           IF W-VSTOCKAV-RAY < ZERO                                     08610000
              COMPUTE W-PVSTOCKEC-RAY = W-VSTOCKEC-RAY /                08620000
                                      ( ( W-VSTOCKAV-RAY * -1 ) / 100)  08630000
           END-IF.                                                      08640000
           IF W-QSTOCKAV-RAY = ZERO                                     08650000
              AND W-QSTOCKAP-RAY = ZERO                                 08660000
                  MOVE ZERO TO W-PQSTOCKEC-RAY                          08670000
                  MOVE ZERO TO W-PVSTOCKEC-RAY                          08680000
           END-IF.                                                      08690000
           MOVE W-PQSTOCKEC-RAY TO W-LIG-PQSTOCKEC-RAY.                 08700000
           MOVE W-PVSTOCKEC-RAY TO W-LIG-PVSTOCKEC-RAY.                 08710000
           IF W-QSTOCKAV-RAY = ZERO                                     08720000
              AND W-QSTOCKAP-RAY NOT = ZERO                             08730000
                  MOVE W-PMAX TO W-LIG-PQSTOCKEC-RAY                    08740000
                  MOVE W-PMAX TO W-LIG-PVSTOCKEC-RAY                    08750000
           END-IF.                                                      08760000
           ADD 1                TO W-LIG-PAG.                           08770000
           MOVE ' '             TO W-LIG-SAUT.                          08780000
           MOVE W-LIG-RAYON     TO W-LIG-DONNEE.                        08790000
      *                                                                 08800000
           MOVE F130-RAYON             TO W-CRAYON-REF.                 08810000
           MOVE '1'                    TO W-RAYON.                      08820000
      *                                                                 08830000
           INITIALIZE              W-QSTOCKAV-RAY                       08840000
                                   W-QSTOCKAP-RAY                       08850000
                                   W-QSTOCKEC-RAY                       08860000
                                   W-VSTOCKAV-RAY                       08870000
                                   W-VSTOCKAP-RAY                       08880000
                                   W-VSTOCKEC-RAY                       08890000
                                   W-PQSTOCKEC-RAY                      08900000
                                   W-PVSTOCKEC-RAY.                     08910000
      *                                                                 08920000
       FIN-TRT-RAYON-LIGNE. EXIT.                                       08930000
      *                                                                 08940000
      *================================================================ 08950000
      * ==>    TRT-EDIT-LIGFAM                                          08960000
      *     -->    TRT-LIGNE-FAMILLE                                    08970000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 08980000
      * EDITION LIGNE FAMILLE                                         * 08990000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 09000000
      *                                                               * 09010000
       TRT-LIGNE-FAMILLE          SECTION.                              09020000
      *                                                                 09030000
           PERFORM TRT-FAMILLE-LIGNE.                                   09040000
           PERFORM TRT-EDIT-LIGNE.                                      09050000
      *                                                                 09060000
       FIN-TRT-LIGNE-FAMILLE. EXIT.                                     09070000
      *                                                                 09080000
      *                                                                 09090000
       TRT-FAMILLE-LIGNE     SECTION.                                   09100000
      *                                                                 09110000
           MOVE SPACE             TO W-LIGNE.                           09120000
           MOVE W-QSTOCKAV-FAM  TO W-LIG-QSTOCKAV-FAM.                  09130000
           MOVE W-QSTOCKAP-FAM  TO W-LIG-QSTOCKAP-FAM.                  09140000
           MOVE W-QSTOCKEC-FAM  TO W-LIG-QSTOCKEC-FAM.                  09150000
           MOVE W-VSTOCKAV-FAM  TO W-LIG-VSTOCKAV-FAM.                  09160000
           MOVE W-VSTOCKAP-FAM  TO W-LIG-VSTOCKAP-FAM.                  09170000
           MOVE W-VSTOCKEC-FAM  TO W-LIG-VSTOCKEC-FAM.                  09180000
           IF W-QSTOCKAV-FAM = ZERO                                     09190000
              AND W-QSTOCKAP-FAM = ZERO                                 09200000
                  MOVE ZERO TO W-PQSTOCKEC-FAM                          09210000
                  MOVE ZERO TO W-PVSTOCKEC-FAM                          09220000
           END-IF.                                                      09230000
           MOVE W-PQSTOCKEC-FAM TO W-LIG-PQSTOCKEC-FAM.                 09240000
           MOVE W-PVSTOCKEC-FAM TO W-LIG-PVSTOCKEC-FAM.                 09250000
           IF W-QSTOCKAV-FAM = ZERO                                     09260000
              AND W-QSTOCKAP-FAM NOT = ZERO                             09270000
                  MOVE W-PMAX TO W-LIG-PQSTOCKEC-FAM                    09280000
                  MOVE W-PMAX TO W-LIG-PVSTOCKEC-FAM                    09290000
           END-IF.                                                      09300000
           IF OK-RAYON                                                  09310000
              IF W-CRAYON-REF NOT = 'XXXXX'                             09320000
                 MOVE W-CRAYON-REF      TO W-LIG-CRAYON                 09330000
              ELSE                                                      09340000
                 MOVE SPACE             TO W-LIG-CRAYON                 09350000
              END-IF                                                    09360000
              MOVE '0'               TO W-RAYON                         09370000
           ELSE                                                         09380000
              MOVE SPACE             TO W-LIG-CRAYON                    09390000
           END-IF.                                                      09400000
           IF W-CFAM-REF NOT = 'XXXXX'                                  09410000
              MOVE W-CFAM-REF        TO W-LIG-CFAM                      09420000
           ELSE                                                         09430000
              MOVE SPACE             TO W-LIG-CFAM                      09440000
           END-IF.                                                      09450000
      *                                                                 09460000
           ADD 1                  TO W-LIG-PAG.                         09470000
           MOVE ' '               TO W-LIG-SAUT.                        09480000
           MOVE W-LIG-DETAIL      TO W-LIG-DONNEE.                      09490000
           IF W-LIG-PAG > 55                                            09500000
              MOVE ZERO TO W-LIG-PAG                                    09510000
           END-IF.                                                      09520000
      *                                                                 09530000
       FIN-TRT-FAMILLE-LIGNE. EXIT.                                     09540000
      *                                                                 09550000
       TRT-FIN-FAMILLE              SECTION.                            09560000
      *                                                                 09570000
           ADD  W-QSTOCKAV-FAM       TO W-QSTOCKAV-RAY.                 09580000
           ADD  W-QSTOCKAP-FAM       TO W-QSTOCKAP-RAY.                 09590000
           ADD  W-QSTOCKEC-FAM       TO W-QSTOCKEC-RAY.                 09600000
      *                                                                 09610000
           ADD  W-VSTOCKAV-FAM       TO W-VSTOCKAV-RAY.                 09620000
           ADD  W-VSTOCKAP-FAM       TO W-VSTOCKAP-RAY.                 09630000
           ADD  W-VSTOCKEC-FAM       TO W-VSTOCKEC-RAY.                 09640000
      *                                                                 09650000
           IF NOT FIN-FIV130                                            09660000
              MOVE '0'                  TO W-FIV130                     09670000
              MOVE '0'                  TO W-RUPTSOC                    09680000
              MOVE F130-CFAM            TO W-CFAM-REF                   09690000
           END-IF.                                                      09700000
      *                                                                 09710000
           INITIALIZE              W-QSTOCKAV-FAM                       09720000
                                   W-QSTOCKAP-FAM                       09730000
                                   W-QSTOCKEC-FAM                       09740000
                                   W-VSTOCKAV-FAM                       09750000
                                   W-VSTOCKAP-FAM                       09760000
                                   W-VSTOCKEC-FAM                       09770000
                                   W-PQSTOCKEC-FAM                      09780000
                                   W-PVSTOCKEC-FAM.                     09790000
      *                                                                 09800000
       FIN-TRT-FIN-FAMILLE. EXIT.                                       09810000
      *                                                               * 09820000
      *                                                                 09830000
       TRT-CUMUL-FAMILLE        SECTION.                                09840000
      *                                                                 09850000
           MOVE F130-CFAM               TO W-CFAM-REF.                  09860000
           MOVE F130-RAYON              TO W-CRAYON-REF.                09870000
      *                                                                 09880000
           COMPUTE W-QSTOCKAV-FAM = W-QSTOCKAV-FAM + F130-QSTOCK.       09890000
           COMPUTE W-QSTOCKEC-FAM = W-QSTOCKEC-FAM + F130-QSTOCKEC1.    09900000
           COMPUTE W-QSTOCKAP-FAM = W-QSTOCKAV-FAM + W-QSTOCKEC-FAM.    09910000
           IF F130-PRMP NOT = ZERO                                      09920000
              COMPUTE W-VSTOCKAV = F130-QSTOCK * F130-PRMP              09930000
              COMPUTE W-VSTOCKAP =                                      09940000
                         ( F130-QSTOCK + F130-QSTOCKEC1 ) * F130-PRMP   09950000
              COMPUTE W-VSTOCKEC = F130-QSTOCKEC1 * F130-PRMP           09960000
           END-IF.                                                      09970000
           IF F130-PRMP NOT = ZERO                                      09980000
              COMPUTE W-VSTOCKAV-FAM = W-VSTOCKAV-FAM + W-VSTOCKAV      09990000
              COMPUTE W-VSTOCKAP-FAM = W-VSTOCKAP-FAM + W-VSTOCKAP      10000000
              COMPUTE W-VSTOCKEC-FAM = W-VSTOCKEC-FAM + W-VSTOCKEC      10010000
           END-IF.                                                      10020000
      *                                                                 10030000
           IF W-QSTOCKAV-FAM NOT = ZERO                                 10040000
              COMPUTE W-PQSTOCKEC-FAM = W-QSTOCKEC-FAM /                10050000
                                        ( W-QSTOCKAV-FAM / 100)         10060000
           END-IF.                                                      10070000
PM0305     MOVE 0               TO W-TAMPON.                            07330000
  "        MOVE W-VSTOCKAV-FAM  TO W-TAMPON.                            07330000
  "        IF W-TAMPON NOT = ZERO                                       07330000
PM0305*    IF W-VSTOCKAV-FAM NOT = ZERO                                 10080000
              COMPUTE W-PVSTOCKEC-FAM = W-VSTOCKEC-FAM /                10090000
PM0305*                                 ( W-VSTOCKAV-FAM / 100)         10100000
  "                                     ( W-TAMPON / 100)               07350000
  "        ELSE                                                         07360000
PM0305        MOVE 0              TO W-PVSTOCKEC-FAM                    07360000
           END-IF.                                                      10110000
           IF W-QSTOCKAV-FAM < ZERO                                     10120000
              COMPUTE W-PQSTOCKEC-FAM = W-QSTOCKEC-FAM /                10130000
                                      ( ( W-QSTOCKAV-FAM * -1 ) / 100)  10140000
           END-IF.                                                      10150000
           IF W-VSTOCKAV-FAM < ZERO                                     10160000
              COMPUTE W-PVSTOCKEC-FAM = W-VSTOCKEC-FAM /                10170000
                                      ( ( W-VSTOCKAV-FAM * -1 )  / 100) 10180000
           END-IF.                                                      10190000
      *                                                                 10200000
      *                                                                 10210000
       FIN-TRT-CUMUL-FAMILLE. EXIT.                                     10220000
      *                                                                 10230000
      *                                                                 10240000
      *================================================================ 10250000
      *                                                                 10260000
       TRT-EDIT-LIGNE        SECTION.                                   10270000
      *                                                                 10280000
            MOVE W-LIG-SAUT        TO SAUT-IIV137.                      10290000
            MOVE W-LIG-DONNEE      TO LIG-IIV137.                       10300000
            WRITE ENR-IIV137.                                           10310000
      *                                                                 10320000
       FIN-TRT-EDIT-LIGNE. EXIT.                                        10330000
      *                                                                 10340000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 10350000
      *================================================================ 10360000
      *                                                               * 10370000
      * ==>    TRT-EDIT-ENTETE                                          10380000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 10390000
      * RUPTURE   PAGE       SUR SOCIETE                              * 10400000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 10410000
      *                                                                 10420000
       TRT-EDIT-ENTETE1           SECTION.                              10430000
      *                                                                 10440000
           PERFORM TRT-ENTETE-LIGNE VARYING W-IND-LIG                   10450000
                                    FROM 1 BY 1 UNTIL W-IND-LIG > 16.   10460000
      *                                                                 10470000
       FIN-EDIT-ENTETE1. EXIT.                                          10480000
      *                                                                 10490000
       TRT-ENTETE-LIGNE           SECTION.                              10500000
      *                                                                 10510000
           IF W-IND-LIG = 1                                             10520000
              PERFORM TRT-ENTETE-LIGNE1                                 10530000
           END-IF.                                                      10540000
           IF W-IND-LIG = 2                                             10550000
              PERFORM TRT-ENTETE-LIGNE2                                 10560000
           END-IF.                                                      10570000
           IF W-IND-LIG = 3                                             10580000
              PERFORM TRT-ENTETE-LIGNE3                                 10590000
           END-IF.                                                      10600000
           IF W-IND-LIG = 4                                             10610000
              PERFORM TRT-ENTETE-LIGNE4                                 10620000
           END-IF.                                                      10630000
           IF W-IND-LIG = 5                                             10640000
              PERFORM TRT-ENTETE-LIGNE2                                 10650000
           END-IF.                                                      10660000
           IF W-IND-LIG = 6                                             10670000
              PERFORM TRT-ENTETE-LIGNE5                                 10680000
           END-IF.                                                      10690000
           IF W-IND-LIG = 7                                             10700000
              PERFORM TRT-ENTETE-LIGNE6                                 10710000
           END-IF.                                                      10720000
           IF W-IND-LIG = 8                                             10730000
              PERFORM TRT-ENTETE-LIGNE2                                 10740000
           END-IF.                                                      10750000
           IF W-IND-LIG = 9                                             10760000
              PERFORM TRT-ENTETE-LIGNE2                                 10770000
           END-IF.                                                      10780000
           IF W-IND-LIG = 10                                            10790000
              PERFORM TRT-ENTETE-LIGNE2                                 10800000
           END-IF.                                                      10810000
           IF W-IND-LIG = 11                                            10820000
              PERFORM TRT-ENTETE-LIGNE2                                 10830000
           END-IF.                                                      10840000
           IF W-IND-LIG = 12                                            10850000
              PERFORM TRT-ENTETE-LIGNE9                                 10860000
           END-IF.                                                      10870000
           IF W-IND-LIG = 13                                            10880000
              PERFORM TRT-ENTETE-LIGNE10                                10890000
           END-IF.                                                      10900000
           IF W-IND-LIG = 14                                            10910000
              PERFORM TRT-ENTETE-LIGNE11                                10920000
           END-IF.                                                      10930000
           IF W-IND-LIG = 15                                            10940000
              PERFORM TRT-ENTETE-LIGNE12                                10950000
           END-IF.                                                      10960000
           IF W-IND-LIG = 16                                            10970000
              PERFORM TRT-ENTETE-LIGNE13                                10980000
           END-IF.                                                      10990000
           PERFORM TRT-EDIT-LIGNE.                                      11000000
      *                                                                 11010000
       FIN-ENTETE-LIGNE. EXIT.                                          11020000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 11030000
      *                                                                 11040000
       TRT-ENTETE-LIGNE1          SECTION.                              11050000
      *                                                                 11060000
           MOVE SPACE                TO W-LIGNE.                        11070000
           MOVE ZERO                 TO W-LIG-PAG.                      11080000
           MOVE W-DATJ               TO W-LIG-DATE.                     11090000
           MOVE W-DATE-AA            TO W-DAT2-AA.                      11100000
           MOVE W-DATE-MM            TO W-DAT2-MM.                      11110000
           MOVE W-DATE-JJ            TO W-DAT2-JJ.                      11120000
           ADD 1                     TO W-LIG-PAG.                      11130000
           MOVE '1'                  TO W-LIG-SAUT.                     11140000
           MOVE W-LIG-ENTETE1        TO W-LIG-DONNEE.                   11150000
      *                                                                 11160000
       FIN-ENTETE-LIGNE1. EXIT.                                         11170000
      *                                                                 11180000
      *                                                                 11190000
       TRT-ENTETE-LIGNE2          SECTION.                              11200000
      *                                                                 11210000
           MOVE SPACE                TO W-LIGNE.                        11220000
           ADD 1                     TO W-LIG-PAG.                      11230000
           MOVE ' '                  TO W-LIG-SAUT.                     11240000
           MOVE W-LIG-ENTETE2        TO W-LIG-DONNEE.                   11250000
      *                                                                 11260000
       FIN-ENTETE-LIGNE2. EXIT.                                         11270000
      *                                                                 11280000
       TRT-ENTETE-LIGNE3          SECTION.                              11290000
      *                                                                 11300000
           MOVE SPACE                TO W-LIGNE.                        11310000
           ADD 1                     TO W-NUM-PAGE.                     11320000
           MOVE W-NUM-PAGE           TO W-LIG-PAGE.                     11330000
           ADD 1                     TO W-LIG-PAG.                      11340000
           MOVE ' '                  TO W-LIG-SAUT.                     11350000
           MOVE W-LIG-ENTETE3        TO W-LIG-DONNEE.                   11360000
      *                                                                 11370000
       FIN-ENTETE-LIGNE3. EXIT.                                         11380000
      *                                                                 11390000
       TRT-ENTETE-LIGNE4          SECTION.                              11400000
      *                                                                 11410000
           MOVE SPACE                TO W-LIGNE.                        11420000
           ADD 1                     TO W-LIG-PAG.                      11430000
           MOVE ' '                  TO W-LIG-SAUT.                     11440000
           MOVE W-LIG-ENTETE4        TO W-LIG-DONNEE.                   11460000
      *                                                                 11500000
       FIN-ENTETE-LIGNE4. EXIT.                                         11510000
      *                                                                 11520000
       TRT-ENTETE-LIGNE5          SECTION.                              11530000
      *                                                                 11540000
           MOVE SPACE                TO W-LIGNE.                        11550000
           MOVE W-DJOUR              TO W-LIG-DJOUR.                    11560000
           ADD 1                     TO W-LIG-PAG.                      11570000
           MOVE ' '                  TO W-LIG-SAUT.                     11580000
           MOVE W-LIG-ENTETE5        TO W-LIG-DONNEE.                   11590000
      *                                                                 11600000
       FIN-ENTETE-LIGNE5. EXIT.                                         11610000
      *                                                                 11620000
      *                                                                 11630000
       TRT-ENTETE-LIGNE6          SECTION.                              11640000
      *                                                                 11650000
           MOVE SPACE                TO W-LIGNE.                        11660000
           MOVE W-NSOCMAG-REF        TO W-LIG-NSOCMAG.                  11670000
           ADD 1                     TO W-LIG-PAG.                      11680000
           MOVE ' '                  TO W-LIG-SAUT.                     11690000
           MOVE W-LIG-ENTETE6        TO W-LIG-DONNEE.                   11700000
      *                                                                 11710000
       FIN-ENTETE-LIGNE6. EXIT.                                         11720000
      *                                                                 11730000
      *                                                                 11740000
       TRT-ENTETE-LIGNE9         SECTION.                               11750000
      *                                                                 11760000
           MOVE SPACE                TO W-LIGNE.                        11770000
           ADD 1                     TO W-LIG-PAG.                      11780000
           MOVE ' '                  TO W-LIG-SAUT.                     11790000
           MOVE W-LIG-ENTETE9        TO W-LIG-DONNEE.                   11800000
      *                                                                 11810000
       FIN-ENTETE-LIGNE9. EXIT.                                         11820000
      *                                                                 11830000
       TRT-ENTETE-LIGNE10        SECTION.                               11840000
      *                                                                 11850000
           MOVE SPACE                TO W-LIGNE.                        11860000
           ADD 1                     TO W-LIG-PAG.                      11870000
           MOVE ' '                  TO W-LIG-SAUT.                     11880000
           MOVE W-LIG-ENTETE10       TO W-LIG-DONNEE.                   11890000
      *                                                                 11900000
       FIN-ENTETE-LIGNE10. EXIT.                                        11910000
      *                                                                 11920000
       TRT-ENTETE-LIGNE11         SECTION.                              11930000
      *                                                                 11940000
           MOVE SPACE                TO W-LIGNE.                        11950000
           ADD 1                     TO W-LIG-PAG.                      11960000
           MOVE ' '                  TO W-LIG-SAUT.                     11970000
           MOVE W-LIG-ENTETE11       TO W-LIG-DONNEE.                   11980000
      *                                                                 11990000
       FIN-ENTETE-LIGNE11. EXIT.                                        12000000
      *                                                                 12010000
       TRT-ENTETE-LIGNE12         SECTION.                              12020000
      *                                                                 12030000
           MOVE SPACE                TO W-LIGNE.                        12040000
           ADD 1                     TO W-LIG-PAG.                      12050000
           MOVE ' '                  TO W-LIG-SAUT.                     12060000
           MOVE W-LIG-ENTETE12       TO W-LIG-DONNEE.                   12070000
      *                                                                 12080000
       FIN-ENTETE-LIGNE12. EXIT.                                        12090000
      *                                                                 12100000
      *                                                                 12110000
       TRT-ENTETE-LIGNE13         SECTION.                              12120000
      *                                                                 12130000
           MOVE SPACE                TO W-LIGNE.                        12140000
           ADD 1                     TO W-LIG-PAG.                      12150000
           MOVE ' '                  TO W-LIG-SAUT.                     12160000
           MOVE W-LIG-ENTETE13       TO W-LIG-DONNEE.                   12170000
      *                                                                 12180000
       FIN-ENTETE-LIGNE13. EXIT.                                        12190000
      *                                                                 12200000
      * -AIDA ********************************************************* 12210000
      *                                                               * 12220000
      *  SSSSSSSS   OOOOOOO  RRRRRRRR  TTTTTTTTT IIIIIIIII EEEEEEEEE  * 12230000
      *  SSSSSSSS  OOOOOOOOO RRRRRRRRR TTTTTTTTT IIIIIIIII EEEEEEEEE  * 12240000
      *  SSSS      OOO   OOO RRR   RRR    TTT       III    EEE        * 12250000
      *   SSSS     OOO   OOO RRR   RRR    TTT       III    EEE        * 12260000
      *    SSSS    OOO   OOO RRR   RRR    TTT       III    EEEEEEEEE  * 12270000
      *     SSSS   OOO   OOO RRRRRRRRR    TTT       III    EEEEEEEEE  * 12280000
      *      SSSS  OOO   OOO RRRRRRRR     TTT       III    EEE        * 12290000
      *      SSSS  OOO   OOO RRR   RR     TTT       III    EEE        * 12300000
      *  SSSSSSSS  OOOOOOOOO RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  * 12310000
      *  SSSSSSSS   OOOOOOO  RRR   RRR    TTT    IIIIIIIII EEEEEEEEE  * 12320000
      *                                                               * 12330000
      ***************************************************************** 12340000
      *                                                                 12350000
       MODULE-SORTIE              SECTION.                              12360000
      *                                                                 12370000
      *                                                                 12380000
           PERFORM DISP-COMPTEURS.                                      12390000
           CLOSE FPARM.                                                 12400000
           CLOSE FIV130.                                                12410000
           CLOSE IIV137.                                                12420000
           GOBACK.                                                      12430000
      *                                                                 12440000
       FIN-MODULE-SORTIE. EXIT.                                         12450000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   12460000
      *                 *      COMPTEURS                                12470000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   12480000
      *                                                                 12490000
       DISP-COMPTEURS SECTION.                                          12500000
      *                                                                 12510000
            DISPLAY '************************************************'. 12520000
            DISPLAY '*****  EDITION DES COMPTEURS  PGM :BIV137  *****'. 12530000
            DISPLAY '************************************************'. 12540000
            DISPLAY 'DATE DE TRAITEMENT : ' W-DAT2.                     12550000
            DISPLAY '******     NB CODICS LUS DANS FIV130         = '   12560000
                                             W-LUS-FIV130.              12570000
            DISPLAY '************************************************'. 12580000
      *                                                                 12590000
       FIN-DISP-COMPTEURS. EXIT.                                        12600000
      *                                                                 12610000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   12620000
      *                 *      FIN ANORMALE                             12630000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   12640000
      *                                                                 12650000
       ABEND-PROGRAMME SECTION.                                         12660000
      *                                                                 12670000
           PERFORM DISP-COMPTEURS.                                      12680000
           CLOSE FIV130, IIV137.                                        12690000
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    12700000
      *                                                                 12710000
       FIN-ABEND-PROGRAMME. EXIT.                                       12720000
      *                                                                 12730000
       IG3-COPY SECTION. CONTINUE. COPY SYBCERRO.                       12740000
