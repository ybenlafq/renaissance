      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BRM015.                                                      
      *===============================================================*         
      *                                                                         
      *   REAPPROVISIONNEMENT DES STOCKS MAGASINS                               
      *                                                                         
      * ETAPE 3               12/98                                             
      *                                                                         
      * HERVE AMANT                                                             
      *===============================================================*         
      *                                                                         
      *   CONSTITUTION D UN FICHIER A L IMAGE DE RTRM85                         
      *                                                                         
      *===============================================================*         
      *                                                                         
      *   MODIFICATION JA  LE 140499 : RAJOUT DE QV4SER DANS RTRM85             
      *                                                                         
      *===============================================================*         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRQV4S   ASSIGN TO FRQV4S.                                   
      *--                                                                       
            SELECT FRQV4S   ASSIGN TO FRQV4S                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRQV4SER ASSIGN TO FRQV4SER.                                 
      *--                                                                       
            SELECT FRQV4SER ASSIGN TO FRQV4SER                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRQPV    ASSIGN TO FRQPV.                                    
      *--                                                                       
            SELECT FRQPV    ASSIGN TO FRQPV                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSR4S    ASSIGN TO FSR4S.                                    
      *--                                                                       
            SELECT FSR4S    ASSIGN TO FSR4S                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSR4SER  ASSIGN TO FSR4SER.                                  
      *--                                                                       
            SELECT FSR4SER  ASSIGN TO FSR4SER                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FSRQPV   ASSIGN TO FSRQPV.                                   
      *--                                                                       
            SELECT FSRQPV   ASSIGN TO FSRQPV                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FRM85    ASSIGN TO FRM85.                                    
      *--                                                                       
            SELECT FRM85    ASSIGN TO FRM85                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *---                                                                      
       FD  FRQV4S    RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FRQV4S-ENR             PIC X(80).                                    
      *---                                                                      
       FD  FRQV4SER  RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FRQV4SER-ENR           PIC X(80).                                    
      *---                                                                      
       FD  FRQPV     RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FRQPV-ENR              PIC X(80).                                    
      *---                                                                      
       FD  FSR4S     RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FSR4S-ENR              PIC X(80).                                    
      *---                                                                      
       FD  FSR4SER   RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FSR4SER-ENR            PIC X(80).                                    
      *---                                                                      
       FD  FSRQPV    RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FSRQPV-ENR             PIC X(80).                                    
       FD  FRM85     RECORDING F BLOCK 0 RECORDS LABEL RECORD STANDARD.         
       01  FRM85-ENR             PIC X(90).                                     
        WORKING-STORAGE SECTION.                                                
       01  FRQV4S-DSECT.                                                        
           02 FRQV4S-NSOCIETE          PIC X(03).                               
           02 FRQV4S-CGROUP            PIC X(05).                               
           02 FRQV4S-CFAM              PIC X(05).                               
           02 FRQV4S-LAGREGATED        PIC X(20).                               
           02 FRQV4S-NCODIC            PIC X(07).                               
           02 FRQV4S-QV4S              PIC S9(7)      COMP-3.                   
           02 FRQV4S-RANG-REEL         PIC S9(5)      COMP-3.                   
           02 FRQV4S-8020-REEL         PIC X.                                   
           02 FRQV4S-POIDS-REEL        PIC S9(3)V99   COMP-3.                   
           02 FRQV4S-CVDESCRIPT1       PIC X(5).                                
           02 FRQV4S-CVDESCRIPT2       PIC X(5).                                
           02 FRQV4S-CVDESCRIPT3       PIC X(5).                                
           02 FILLER                   PIC X(14).                               
       01  FRQV4SER-DSECT.                                                      
           02 FRQV4SER-NSOCIETE        PIC X(03).                               
           02 FRQV4SER-CGROUP          PIC X(05).                               
           02 FRQV4SER-CFAM            PIC X(05).                               
           02 FRQV4SER-LAGREGATED      PIC X(20).                               
           02 FRQV4SER-NCODIC          PIC X(07).                               
           02 FRQV4SER-QV4SER          PIC S9(7)      COMP-3.                   
           02 FRQV4SER-RANG-AJUSTE     PIC S9(5)      COMP-3.                   
           02 FRQV4SER-8020-AJUSTE     PIC X.                                   
           02 FRQV4SER-POIDS-AJUSTE    PIC S9(3)V99   COMP-3.                   
           02 FRQV4SER-CVDESCRIPT1     PIC X(5).                                
           02 FRQV4SER-CVDESCRIPT2     PIC X(5).                                
           02 FRQV4SER-CVDESCRIPT3     PIC X(5).                                
           02 FILLER                   PIC X(14).                               
       01  FRQPV-DSECT.                                                         
           02 FRQPV-NSOCIETE           PIC X(03).                               
           02 FRQPV-CGROUP             PIC X(05).                               
           02 FRQPV-CFAM               PIC X(05).                               
           02 FRQPV-LAGREGATED         PIC X(20).                               
           02 FRQPV-NCODIC             PIC X(07).                               
           02 FRQPV-QPV                PIC S9(7)      COMP-3.                   
           02 FRQPV-RANG-PREV          PIC S9(5)      COMP-3.                   
           02 FRQPV-8020-PREV          PIC X.                                   
           02 FRQPV-POIDS-PREV         PIC S9(3)V99   COMP-3.                   
           02 FRQPV-CVDESCRIPT1        PIC X(5).                                
           02 FRQPV-CVDESCRIPT2        PIC X(5).                                
           02 FRQPV-CVDESCRIPT3        PIC X(5).                                
           02 FILLER                   PIC X(14).                               
       01  FSR4S-DSECT.                                                         
           02 FSR4S-NSOCIETE         PIC X(03).                                 
           02 FSR4S-CGROUP           PIC X(05).                                 
           02 FSR4S-CFAM             PIC X(05).                                 
           02 FSR4S-LAGREGATED       PIC X(20).                                 
           02 FSR4S-8020-NBP-REEL    PIC S9(5)      COMP-3.                     
           02 FILLER                   PIC X(44).                               
       01  FSR4SER-DSECT.                                                       
           02 FSR4SER-NSOCIETE         PIC X(03).                               
           02 FSR4SER-CGROUP           PIC X(05).                               
           02 FSR4SER-CFAM             PIC X(05).                               
           02 FSR4SER-LAGREGATED       PIC X(20).                               
           02 FSR4SER-8020-NBP-AJUSTE  PIC S9(5)      COMP-3.                   
           02 FILLER                   PIC X(44).                               
       01  FSRQPV-DSECT.                                                        
           02 FSRQPV-NSOCIETE          PIC X(03).                               
           02 FSRQPV-CGROUP            PIC X(05).                               
           02 FSRQPV-CFAM              PIC X(05).                               
           02 FSRQPV-LAGREGATED        PIC X(20).                               
           02 FSRQPV-8020-NBP-PREV     PIC S9(5)      COMP-3.                   
           02 FILLER                   PIC X(44).                               
JA         COPY RVRM8501.                                                       
      ****************************************************************          
      *    COMPTEURS                                                            
      ****************************************************************          
       01  W-FRQV4S        PIC X VALUE '0'.                                     
           88 OK-FRQV4S          VALUE '0'.                                     
           88 FIN-FRQV4S         VALUE '1'.                                     
       01  W-FRQV4SER      PIC X VALUE '0'.                                     
           88 OK-FRQV4SER        VALUE '0'.                                     
           88 FIN-FRQV4SER       VALUE '1'.                                     
       01  W-FRQPV         PIC X VALUE '0'.                                     
           88 OK-FRQPV           VALUE '0'.                                     
           88 FIN-FRQPV          VALUE '1'.                                     
       01  W-FSR4SER       PIC X VALUE '0'.                                     
           88 OK-FSR4SER         VALUE '0'.                                     
           88 FIN-FSR4SER        VALUE '1'.                                     
       01  W-FSR4S         PIC X VALUE '0'.                                     
           88 OK-FSR4S           VALUE '0'.                                     
           88 FIN-FSR4S          VALUE '1'.                                     
       01  W-FSRQPV        PIC X VALUE '0'.                                     
           88 OK-FSRQPV          VALUE '0'.                                     
           88 FIN-FSRQPV         VALUE '1'.                                     
      ***************************************************************           
      * ZONE D APPEL AU MODULE D ABEND                                          
      ***************************************************************           
           COPY  ABENDCOP.                                                      
           COPY SYBWDIV0.                                                       
           COPY SYKWSQ10.                                                       
      *                                                                         
      *================================================================*        
      *--->  PARAMETRES D'APPEL  SPDATDB2                                       
      *================================================================*        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-RET                         PIC S9(4) COMP.                        
      *--                                                                       
       01  W-RET                         PIC S9(4) COMP-5.                      
      *}                                                                        
       01  DATHEUR                       PIC S9(13)   COMP-3.                   
       01  W-MESS-SPDATDB2.                                                     
           05 W-MESS-SPDAT               PIC X(32)  VALUE                       
              'RETOUR SPDATDB2 ANORMAL, CODE : '.                               
           05 WRET-PIC                   PIC 9(04).                             
      *****************************************************************         
      *  DESCRIPTION DES ZONES D'ABEND                                *         
      *****************************************************************         
       01  FILLER    PIC X(16)    VALUE  '*** Z-INOUT ****'.                    
       01  Z-INOUT   PIC X(4096)  VALUE  SPACE.                                 
      *****************************************************************         
           COPY SYBWERRO.                                                       
      *===============================================================*         
       PROCEDURE DIVISION.                                                      
      *===============================================================*         
       MODULE-BRM015                  SECTION.                                  
           PERFORM ENTREE.                                                      
           PERFORM TRAITEMENT.                                                  
           PERFORM SORTIE.                                                      
       FIN-MODULE-BRM015.   EXIT.                                               
      *===============================================================*         
      *    - LECTURE DES PARAMETRES                                             
      *===============================================================*         
       ENTREE SECTION.                                                          
           OPEN INPUT FRQV4S FRQV4SER FRQPV FSR4S FSR4SER FSRQPV.               
           OPEN OUTPUT FRM85.                                                   
       FIN-ENTREE. EXIT.                                                        
      *****************************************************************         
      *    TRAITEMENT                                                           
      *****************************************************************         
       TRAITEMENT SECTION.                                                      
           PERFORM LECTURE-FRQV4S.                                              
           PERFORM LECTURE-FRQV4SER.                                            
           PERFORM LECTURE-FRQPV.                                               
           PERFORM UNTIL FIN-FRQV4S                                             
              PERFORM TRAITEMENT-RM85                                           
              PERFORM LECTURE-FRQV4S                                            
              PERFORM LECTURE-FRQV4SER                                          
              PERFORM LECTURE-FRQPV                                             
           END-PERFORM.                                                         
       FIN-TRAITEMENT. EXIT.                                                    
       TRAITEMENT-RM85                SECTION.                                  
           INITIALIZE RVRM8501.                                                 
           MOVE FRQV4S-CGROUP            TO RM85-CGROUP.                        
           MOVE FRQV4S-CFAM              TO RM85-CFAM.                          
           MOVE FRQV4S-LAGREGATED        TO RM85-LAGREGATED.                    
           MOVE FRQV4S-NCODIC            TO RM85-NCODIC.                        
           MOVE FRQV4S-RANG-REEL         TO RM85-QRANG.                         
           MOVE FRQV4SER-RANG-AJUSTE     TO RM85-QRANGHER.                      
           MOVE FRQPV-RANG-PREV          TO RM85-QRANGP.                        
           MOVE FRQV4S-POIDS-REEL        TO RM85-QPOIDS.                        
           MOVE FRQV4SER-POIDS-AJUSTE    TO RM85-QPOIDSR.                       
           MOVE FRQPV-POIDS-PREV         TO RM85-QPOIDSP.                       
JA         MOVE FRQV4SER-QV4SER          TO RM85-QV4SER.                        
           MOVE FRQV4S-CVDESCRIPT1       TO RM85-CVDESCRIPT1.                   
           MOVE FRQV4S-CVDESCRIPT2       TO RM85-CVDESCRIPT2.                   
           MOVE FRQV4S-CVDESCRIPT3       TO RM85-CVDESCRIPT3.                   
           PERFORM LECTURE-FSR4S UNTIL                                          
                   FSR4S-CGROUP   > FRQV4S-CGROUP                               
               OR (FSR4S-CGROUP   = FRQV4S-CGROUP AND                           
                   FSR4S-CFAM     > FRQV4S-CFAM     )                           
               OR (FSR4S-CGROUP   = FRQV4S-CGROUP AND                           
                   FSR4S-CFAM     = FRQV4S-CFAM     AND                         
                   FSR4S-LAGREGATED NOT < FRQV4S-LAGREGATED )                   
               OR  FIN-FSR4S.                                                   
           IF FSR4S-LAGREGATED = FRQV4S-LAGREGATED                              
              MOVE FSR4S-8020-NBP-REEL     TO RM85-QNBREF                       
           ELSE                                                                 
              MOVE 1                       TO RM85-QNBREF                       
           END-IF.                                                              
           PERFORM LECTURE-FSR4SER UNTIL                                        
                   FSR4SER-CGROUP   > FRQV4SER-CGROUP                           
               OR (FSR4SER-CGROUP   = FRQV4SER-CGROUP AND                       
                   FSR4SER-CFAM     > FRQV4SER-CFAM     )                       
               OR (FSR4SER-CGROUP   = FRQV4SER-CGROUP AND                       
                   FSR4SER-CFAM     = FRQV4SER-CFAM     AND                     
                   FSR4SER-LAGREGATED NOT < FRQV4SER-LAGREGATED )               
               OR  FIN-FSR4SER.                                                 
           IF FSR4SER-LAGREGATED = FRQV4SER-LAGREGATED                          
              MOVE FSR4SER-8020-NBP-AJUSTE TO RM85-QNBREFR                      
           ELSE                                                                 
              MOVE 1                       TO RM85-QNBREFR                      
           END-IF.                                                              
           PERFORM LECTURE-FSRQPV UNTIL                                         
                   FSRQPV-CGROUP    > FRQPV-CGROUP                              
               OR (FSRQPV-CGROUP    = FRQPV-CGROUP AND                          
                   FSRQPV-CFAM      > FRQPV-CFAM )                              
               OR (FSRQPV-CGROUP    = FRQPV-CGROUP AND                          
                   FSRQPV-CFAM      = FRQPV-CFAM AND                            
                   FSRQPV-LAGREGATED NOT < FRQPV-LAGREGATED )                   
               OR  FIN-FSRQPV.                                                  
           IF FSRQPV-LAGREGATED = FRQPV-LAGREGATED                              
              MOVE FSRQPV-8020-NBP-PREV   TO RM85-QNBREFP                       
           ELSE                                                                 
              MOVE 1                      TO RM85-QNBREFP                       
           END-IF.                                                              
           PERFORM CHARGEMENT-DSYST.                                            
           MOVE DATHEUR                   TO RM85-DSYST.                        
           PERFORM ECRITURE-FRM85.                                              
       FIN-TRAITEMENT-RM85. EXIT.                                               
      ****************************************************************          
      *    ECRITURE DE FRM85                                                    
      ****************************************************************          
       ECRITURE-FRM85  SECTION.                                                 
JA         WRITE FRM85-ENR FROM RVRM8501.                                       
       FIN-ECRITURE-FRM85.  EXIT.                                               
      *===============================================================*         
       SORTIE      SECTION.                                                     
      *===============================================================*         
           CLOSE                                                                
                  FRQV4S FRQV4SER FRQPV FSR4S FSR4SER FSRQPV FRM85.             
           DISPLAY '       FIN TRT BRM015              '.                       
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-SORTIE. EXIT.                                                        
      *****************************************************************         
      *   LECTURE DE FRQV4S                                                     
      *****************************************************************         
       LECTURE-FRQV4S             SECTION.                                      
           READ FRQV4S                                                          
                AT END DISPLAY 'FIN FCQV4S'                                     
                SET FIN-FRQV4S TO TRUE.                                         
           MOVE FRQV4S-ENR TO FRQV4S-DSECT.                                     
       FIN-LECTURE-FRQV4S.  EXIT.                                               
      *****************************************************************         
      *   LECTURE DE FRQV4SER                                                   
      *****************************************************************         
       LECTURE-FRQV4SER           SECTION.                                      
           READ FRQV4SER                                                        
                AT END DISPLAY 'FIN FRQV4SER'                                   
                SET FIN-FRQV4SER TO TRUE.                                       
           MOVE FRQV4SER-ENR TO FRQV4SER-DSECT.                                 
       FIN-LECTURE-FRQV4SER. EXIT.                                              
      *****************************************************************         
      *   LECTURE DE FRQPV                                                      
      *****************************************************************         
       LECTURE-FRQPV              SECTION.                                      
           READ FRQPV                                                           
                AT END DISPLAY 'FIN FRQPV'                                      
                SET FIN-FRQPV TO TRUE.                                          
           MOVE FRQPV-ENR TO FRQPV-DSECT.                                       
       FIN-LECTURE-FRQPV. EXIT.                                                 
      *****************************************************************         
      *   LECTURE DE FSR4S                                                      
      *****************************************************************         
       LECTURE-FSR4S           SECTION.                                         
           READ FSR4S                                                           
                AT END DISPLAY 'FIN FSR4S'                                      
                SET FIN-FSR4S TO TRUE.                                          
           MOVE FSR4S-ENR TO FSR4S-DSECT.                                       
       FIN-LECTURE-FSR4S. EXIT.                                                 
      *****************************************************************         
      *   LECTURE DE FSR4SER                                                    
      *****************************************************************         
       LECTURE-FSR4SER           SECTION.                                       
           READ FSR4SER                                                         
                AT END DISPLAY 'FIN FSR4SER'                                    
                SET FIN-FSR4SER TO TRUE.                                        
           MOVE FSR4SER-ENR TO FSR4SER-DSECT.                                   
       FIN-LECTURE-FSR4SER. EXIT.                                               
      *****************************************************************         
      *   LECTURE DE FSRQPV                                                     
      *****************************************************************         
       LECTURE-FSRQPV              SECTION.                                     
           READ FSRQPV                                                          
                AT END DISPLAY 'FIN FSRQPV'                                     
                SET FIN-FSRQPV TO TRUE.                                         
           MOVE FSRQPV-ENR TO FSRQPV-DSECT.                                     
       FIN-LECTURE-FSRQPV. EXIT.                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * CHARGEMENT DE LA DATE SYSTEME                                 *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       CHARGEMENT-DSYST           SECTION.                                      
           CALL 'SPDATDB2' USING W-RET DATHEUR.                                 
           IF W-RET NOT = ZERO                                                  
              MOVE W-RET TO WRET-PIC                                            
              MOVE W-MESS-SPDATDB2 TO ABEND-MESS                                
              PERFORM SECTION-ABANDON-TACHE                                     
           END-IF.                                                              
      *                                                                         
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
      *                                                                         
