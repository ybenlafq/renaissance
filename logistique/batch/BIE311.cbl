      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
      *                                                                 00000020
       PROGRAM-ID.  BIE311.                                             00000030
       AUTHOR.  DSA014.                                                 00000040
       DATE-WRITTEN.  12/12/91.                                         00000050
      *                                                                 00000060
      *************************************************************     00000070
      *                                                           *     00000080
      *   EDITION DE LA SITUATION D'INVENTAIRE DES LIEUX DE       *     00000090
      *   TRAITEMENT INVENTORIES NON DETAILLES DANS JIE311        *     00000100
      *                                                           *     00000110
      *************************************************************     00000120
      * CORRECTION AD1 : DANS REMPLIR-TABLE-LIEUX ON NE TRAITAIT  *     00000120
      *                  PAS LE PREMIER ENREG                     *     00000120
      *************************************************************     00000120
      *                                                                 00000130
       ENVIRONMENT DIVISION.                                            00000140
       CONFIGURATION SECTION.                                           00000150
       SPECIAL-NAMES.                                                   00000160
           DECIMAL-POINT IS COMMA.                                      00000170
      *                                                                 00000180
       INPUT-OUTPUT SECTION.                                            00000190
       FILE-CONTROL.                                                    00000200
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FIE310  ASSIGN TO FIE310.                            00000210
      *--                                                                       
            SELECT FIE310  ASSIGN TO FIE310                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IIE311  ASSIGN TO IIE311.                            00000220
      *--                                                                       
            SELECT IIE311  ASSIGN TO IIE311                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00000230
       FILE SECTION.                                                    00000240
      *                                                                 00000250
      *                                                                 00000260
       FD  FIE310                                                       00000270
           RECORDING F                                                  00000280
           BLOCK 0 RECORDS                                              00000290
           LABEL RECORD STANDARD.                                       00000300
       COPY FIE310.                                                     00000310
      *                                                                 00000320
       FD  IIE311                                                       00000330
           RECORDING F                                                  00000340
           BLOCK 0 RECORDS                                              00000350
           LABEL RECORD STANDARD.                                       00000360
       01  ENR-IIE311                 PIC X(133).                       00000370
      *                                                                 00000380
      ***************************************************************** 00000390
      *                 W O R K I N G  -  S T O R A G E               * 00000400
      ***************************************************************** 00000410
       WORKING-STORAGE SECTION.                                         00000420
      *------------------------                                         00000430
      *                                                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-LIGNES-FIE310-LUES      PIC S9(7) COMP VALUE +0.           00000450
      *--                                                                       
       77  W-LIGNES-FIE310-LUES      PIC S9(7) COMP-5 VALUE +0.                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-LIGNES-IIE311-ECRITES   PIC S9(7) COMP VALUE +0.           00000460
      *--                                                                       
       77  W-LIGNES-IIE311-ECRITES   PIC S9(7) COMP-5 VALUE +0.                 
      *}                                                                        
       77  ABEND-PROG                PIC X(6) VALUE 'BIE311'.           00000470
       77  ABEND-MESS                PIC X(30).                         00000480
                                                                        00000490
       01  ETAT-FIE310               PIC X VALUE '1'.                   00000500
           88  FIN-FIE310               VALUE '0'.                      00000510
                                                                        00000520
       01  W-FLAG-MESS1              PIC X VALUE SPACES.                00000521
           88  STOCK-THEO               VALUE '1'.                      00000522
           88  STOCK-THEO-NUL           VALUE '0'.                      00000523
                                                                        00000524
       01  W-FLAG-MESS2              PIC X VALUE SPACES.                00000525
           88  STOCK-INVE               VALUE '1'.                      00000526
           88  STOCK-INVE-NUL           VALUE '0'.                      00000527
                                                                        00000528
       01  REF-NSOCDEPOT             PIC X(3)  VALUE SPACES.            00000530
       01  REF-NDEPOT                PIC X(3)  VALUE SPACES.            00000540
       01  REF-NSSLIEU               PIC X(3)  VALUE SPACES.            00000550
       01  REF-CFAM                  PIC X(5)  VALUE SPACES.            00000560
       01  REF-NCODIC                PIC X(7)  VALUE SPACES.            00000570
       01  REF-CMARQ                 PIC X(5)  VALUE SPACES.            00000571
       01  REF-LREFFOURN             PIC X(20) VALUE SPACES.            00000572
                                                                        00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-NLIGNES                 PIC 9(4) COMP VALUE 0.             00000590
      *--                                                                       
       01  W-NLIGNES                 PIC 9(4) COMP-5 VALUE 0.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-NLIGNES-MAX             PIC 9(4) COMP VALUE 8.             00000600
      *--                                                                       
       01  W-NLIGNES-MAX             PIC 9(4) COMP-5 VALUE 8.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-NBLIEU                  PIC 9(2) COMP.                     00000610
      *--                                                                       
       01  W-NBLIEU                  PIC 9(2) COMP-5.                           
      *}                                                                        
       01  W-NPAGE                   PIC 9(4) VALUE 0.                  00000620
                                                                        00000630
       01  DATE0.                                                       00000640
           02  SS1                   PIC XX.                            00000650
           02  DATE1.                                                   00000660
               05  AA1               PIC XX.                            00000670
               05  MM1               PIC XX.                            00000680
               05  JJ1               PIC XX.                            00000690
                                                                        00000700
       01  DATE2.                                                       00000710
           02  JJ2                   PIC XX.                            00000720
           02  FILLER                PIC X VALUE '/'.                   00000730
           02  MM2                   PIC XX.                            00000740
           02  FILLER                PIC X VALUE '/'.                   00000750
           02  AA2                   PIC XX.                            00000760
                                                                        00000770
       01  TIME1.                                                       00000780
           02  HH1                   PIC XX.                            00000790
           02  MN1                   PIC XX.                            00000800
                                                                        00000810
       01  TIME2.                                                       00000820
           02  HH2                   PIC XX.                            00000830
           02  FILLER                PIC X VALUE 'H'.                   00000840
           02  MN2                   PIC XX.                            00000850
                                                                        00000860
       01  W-TABLE-QSTOCK.                                              00000870
           02  W-TAB-QSTOCK  OCCURS 14.                                 00000880
               05  W-QSTOCKAV        PIC S9(5) COMP-3.                  00000890
               05  W-QSTOCKAP        PIC S9(5) COMP-3.                  00000900
               05  W-ECART           PIC S9(5) COMP-3.                  00000910
       01  W-TABLE-FAMILLE-QSTOCK.                                      00000920
           02  W-TAB-FAMILLE-QSTOCK  OCCURS 14.                         00000930
               05  W-TOTAL-FAMILLE-QSTOCKAV  PIC S9(7) COMP-3.          00000940
               05  W-TOTAL-FAMILLE-QSTOCKAP  PIC S9(7) COMP-3.          00000950
               05  W-TOTAL-FAMILLE-ECART     PIC S9(7) COMP-3.          00000960
       01  W-TABLE-ENTREPOT-QSTOCK.                                     00000970
           02  W-TAB-ENTREPOT-QSTOCK  OCCURS 14.                        00000980
               05  W-TOTAL-ENTREPOT-QSTOCKAV PIC S9(7) COMP-3.          00000990
               05  W-TOTAL-ENTREPOT-QSTOCKAP PIC S9(7) COMP-3.          00001000
               05  W-TOTAL-ENTREPOT-ECART    PIC S9(7) COMP-3.          00001010
                                                                        00001020
       01  W-TOTAL-LIGNE.                                               00001030
           02  W-TOTAL-LIGNE-QSTOCKAV        PIC S9(7) COMP-3.          00001040
           02  W-TOTAL-LIGNE-QSTOCKAP        PIC S9(7) COMP-3.          00001050
           02  W-TOTAL-LIGNE-ECART           PIC S9(7) COMP-3.          00001060
                                                                        00001070
       01  W-TOTAL-FAMILLE-LIGNE             PIC S9(7) COMP-3.          00001080
       01  W-TOTAL-ENTREPOT-LIGNE            PIC S9(7) COMP-3.          00001090
                                                                        00001100
       01  W-TABLE-LIEUX.                                               00001110
           02  W-CLIEUTRT  OCCURS 14   PIC X(5).                        00001120
                                                                        00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I                        PIC 9(2) COMP.                      00001140
      *                                                                         
      *--                                                                       
       01  I                        PIC 9(2) COMP-5.                            
                                                                        00001150
      *}                                                                        
       01  LIGNE.                                                       00001160
           02  SAUT-PAGE                PIC X.                          00001170
           02  LIGNE1                   PIC X(132).                     00001180
                                                                        00001190
       01  LIGNE-VIDE               PIC X(132) VALUE SPACES.            00001200
                                                                        00001210
       01  LIGNE-ENTETE-01.                                             00001220
           02  FILLER         PIC X(7) VALUE 'JIE311B'.                 00001230
           02  FILLER         PIC X(49) VALUE SPACES.                   00001240
           02  FILLER         PIC X(20) VALUE 'ETABLISSEMENTS DARTY'.   00001250
           02  FILLER         PIC X(46) VALUE SPACES.                   00001260
           02  FILLER         PIC X(7) VALUE 'PAGE : '.                 00001270
           02  ED-NPAGE       PIC ZZZ.                                  00001280
                                                                        00001290
       01  LIGNE-ENTETE-02.                                             00001300
           02  FILLER    PIC X(36) VALUE SPACES.                        00001310
           02  FILLER    PIC X(23) VALUE 'SITUATION D''INVENTAIRE '.    00001320
           02  FILLER    PIC X(24) VALUE 'DES LIEUX DE TRAITEMENT '.    00001330
           02  FILLER    PIC X(11) VALUE 'INVENTORIES'.                 00001340
           02  FILLER    PIC X(38) VALUE SPACES.                        00001350
                                                                        00001360
       01  LIGNE-ENTETE-03.                                             00001370
           02  FILLER     PIC X(50) VALUE SPACES.                       00001380
           02  FILLER     PIC X(19) VALUE 'NON DETAILLES DANS '.        00001390
           02  FILLER     PIC X(13) VALUE 'L''ETAT JIE311'.             00001400
           02  FILLER     PIC X(50) VALUE SPACES.                       00001410
                                                                        00001420
       01  LIGNE-ENTETE-04.                                             00001430
           02  FILLER     PIC X(12) VALUE 'DATE      : '.               00001440
           02  ED-DATE    PIC X(8).                                     00001450
           02  FILLER     PIC X(4) VALUE SPACES.                        00001460
           02  FILLER     PIC X(8) VALUE 'HEURE : '.                    00001470
           02  ED-HEURE   PIC X(5).                                     00001480
           02  FILLER     PIC X(67) VALUE SPACES.                       00001490
           02  FILLER     PIC X(20) VALUE 'DATE D''INVENTAIRE : '.      00001500
           02  ED-DATEINV PIC X(8).                                     00001510
                                                                        00001520
       01  LIGNE-ENTETE-05.                                             00001530
           02  FILLER   PIC X(12) VALUE 'ENTREPOT : '.                  00001540
           02  ED-NSOCDEPOT PIC X(3).                                   00001550
           02  FILLER    PIC X VALUE SPACE.                             00001560
           02  ED-NDEPOT PIC X(3).                                      00001570
           02  FILLER    PIC X(113) VALUE SPACES.                       00001580
                                                                        00001590
       01  LIGNE-ENTETE-06.                                             00001600
           02  FILLER   PIC X(12) VALUE 'SOUS-LIEU : '.                 00001610
           02  ED-NSSLIEU PIC X(120).                                   00001620
                                                                        00001630
       01  LIGNE-ENTETE-07.                                             00001640
           02  FILLER   PIC X(132) VALUE ALL '-'.                       00001660
                                                                        00001680
       01  LIGNE-ENTETE-08.                                             00001690
           02  FILLER   PIC X(40) VALUE SPACES.                         00001710
           02  FILLER   PIC X VALUE '!'.                                00001720
           02  FILLER   PIC X(32) VALUE SPACES.                         00001730
           02  FILLER   PIC X(19) VALUE 'LIEUX DE TRAITEMENT'.          00001800
           02  FILLER   PIC X(32) VALUE SPACES.                         00001810
           02  FILLER   PIC X VALUE '!'.                                00001820
           02  FILLER   PIC X(07) VALUE SPACES.                         00001830
                                                                        00001850
       01  LIGNE-ENTETE-09.                                             00001860
           02  FILLER   PIC X(6) VALUE 'FAM.'.                          00001880
           02  FILLER   PIC X(6) VALUE 'MARQ.'.                         00001880
           02  FILLER   PIC X(21) VALUE 'REFERENCE'.                    00001920
           02  FILLER   PIC X(7) VALUE 'CODIC'.                         00001940
           02  FILLER   PIC X VALUE '!'.                                00001950
           02  FILLER   PIC X(83) VALUE ALL '-'.                        00001960
           02  FILLER   PIC XX VALUE '!'.                               00001970
           02  FILLER   PIC X(6) VALUE 'TOTAL'.                         00001980
                                                                        00002000
       01  LIGNE-ENTETE-10.                                             00002010
           02  FILLER   PIC X(40) VALUE SPACES.                         00002030
           02  ED-TABLE-CLIEUTRT OCCURS 14.                             00002110
               05  ED-CLIEUTRT-FILL1    PIC X.                          00002120
               05  ED-CLIEUTRT          PIC X(5).                       00002130
           02  FILLER   PIC X(8) VALUE '!'.                             00002160
                                                                        00002170
       01  LIGNE-ENTETE-11.                                             00002180
           02  FILLER   PIC X(40) VALUE SPACES.                         00002200
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(6) VALUE '! '.                            00002210
           02  FILLER   PIC X(8) VALUE '! '.                            00002210
                                                                        00002390
       01  LIGNE-ENTETE-12.                                             00002400
           02  FILLER   PIC X(40) VALUE ALL '-'.                        00002420
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(6) VALUE '!-----'.                        00002440
           02  FILLER   PIC X(8) VALUE '!-------'.                      00002440
                                                                        00002700
       01  LIGNE-DETAIL.                                                00002710
           02  LIGNE-DETAIL1.                                           00002720
               05  ED-CFAM   PIC X(6).                                  00002740
               05  ED-CMARQ  PIC X(6).                                  00002760
               05  ED-LREFFOURN  PIC X(20).                             00002780
           02  W-MESS-SM     REDEFINES LIGNE-DETAIL1.                   00002781
               05  W-MESS    PIC X(32).                                 00002783
           02  LIGNE-DETAIL2 REDEFINES LIGNE-DETAIL1.                   00002790
               05  ED-LIBELLE-DETAIL2  PIC X(16).                       00002800
               05  ED-CFAM-DETAIL2     PIC X(05).                       00002810
           02  ED-NCODIC     PIC X(7) VALUE SPACES.                     00002830
           02  FILLER        PIC XX VALUE ' !'.                         00002840
           02  ED-TABLE-CLIEUTRT OCCURS 14.                             00002850
               05  ED-QSTOCK            PIC ----.                       00002860
               05  ED-QSTOCK-FILL       PIC X(2).                       00002870
           02  ED-QSTOCK-TOTAL          PIC -----9.                     00002890
           02  FILLER                   PIC X VALUE ' '.                00002900
      ***************************************************************** 00002940
      *             P R O C E D U R E   D I V I S I O N               * 00002950
      ***************************************************************** 00002960
       PROCEDURE DIVISION.                                              00002980
      *---------------------------------------------------------------* 00003000
      *              D E B U T   B I E 3 1 0                          * 00003010
      *---------------------------------------------------------------* 00003020
       MODULE-BIE311               SECTION.                             00003060
      *---------------                                                  00003070
           PERFORM  DEBUT.                                              00003090
           PERFORM  REMPLIR-TABLE-LIEUX.                                00003100
           PERFORM  TRAITEMENT  UNTIL FIN-FIE310.                       00003110
           PERFORM  FIN.                                                00003120
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00003130
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                 00003140
       FIN-MODULE-BIE311.  EXIT.                                        00003150
              EJECT                                                     00003160
      *                                                                 00003170
      *===============================================================* 00003180
      *                     D E B U T                                 * 00003190
      *===============================================================* 00003200
      *                                                                 00003210
       DEBUT                 SECTION.                                   00003220
      *------------------------------                                   00003230
      *                                                                 00003240
           DISPLAY  '************************************************'. 00003250
           DISPLAY  '**                                            **'. 00003260
           DISPLAY  '**   PROGRAMME  :  BIE311                     **'. 00003270
           DISPLAY  '**   EDITION DE LA SITUATION D''INVENTAIRE    **'. 00003280
           DISPLAY  '**   POUR LES SOUS-LIEUX ET LIEUX CONTENU     **'. 00003290
           DISPLAY  '**   DANS LA SOUS-TABLE IE60I.                **'. 00003300
           DISPLAY  '************************************************'. 00003310
           DISPLAY  '***     PROGRAMME DEBUTE NORMALEMENT         ***'. 00003320
      *                                                                 00003330
           OPEN INPUT  FIE310.                                          00003340
           OPEN OUTPUT IIE311.                                          00003350
      *                                                                 00003360
           PERFORM LECTURE-FIE310.                                      00003370
      *                                                                 00003380
           INITIALIZE W-TABLE-FAMILLE-QSTOCK.                           00003390
           INITIALIZE W-TABLE-ENTREPOT-QSTOCK.                          00003400
           INITIALIZE W-TOTAL-LIGNE.                                    00003410
      *                                                                 00003420
           ACCEPT DATE1 FROM DATE.                                      00003430
           MOVE JJ1 TO JJ2.                                             00003440
           MOVE MM1 TO MM2.                                             00003450
           MOVE AA1 TO AA2.                                             00003460
           MOVE DATE2 TO ED-DATE.                                       00003470
      *                                                                 00003480
           ACCEPT TIME1 FROM TIME.                                      00003490
           MOVE HH1 TO HH2.                                             00003500
           MOVE MN1 TO MN2.                                             00003510
           MOVE TIME2 TO ED-HEURE.                                      00003520
      *                                                                 00003530
           MOVE FIE310-DATEINV TO DATE0.                                00003540
           MOVE JJ1 TO JJ2.                                             00003550
           MOVE MM1 TO MM2.                                             00003560
           MOVE AA1 TO AA2.                                             00003570
           MOVE DATE2 TO ED-DATEINV.                                    00003580
      *                                                                 00003590
       FIN-DEBUT.                 EXIT.                                 00003600
      *                                                                 00003610
      *===============================================================* 00003620
      *       F I N   N O R M A L E   D U   P R O G R A M M E         * 00003630
      *===============================================================* 00003640
      *                                                                 00003650
       FIN                        SECTION.                              00003660
      *-----------------------------------                              00003670
      *                                                                 00003680
      *                                                                 00003690
           DISPLAY  '************************************************'. 00003700
           DISPLAY  '***       FIN  NORMALE  DU PROGRAMME         ***'. 00003710
           DISPLAY  '***                 BIE311                   ***'. 00003720
           DISPLAY  '************************************************'. 00003730
           DISPLAY  '** NBR DE LIGNES FIE310 LUES          : '          00003740
                    W-LIGNES-FIE310-LUES.                               00003750
           DISPLAY  '** NBR DE LIGNES IIE311 ECRITES       : '          00003760
                    W-LIGNES-IIE311-ECRITES.                            00003770
      *                                                                 00003780
           CLOSE FIE310 IIE311.                                         00003790
      *                                                                 00003800
       FIN-FIN.        EXIT.                                            00003810
      *                                                                 00003820
       REMPLIR-TABLE-LIEUX                SECTION.                      00003830
      *                                                                 00003840
           MOVE 0 TO W-NBLIEU                                           00003850
           INITIALIZE W-TABLE-LIEUX.                                    00003860
AD1   *    PERFORM LECTURE-FIE310.                                      00003870
           PERFORM UNTIL  FIE310-DSECT = HIGH-VALUE                     00003880
                MOVE 1 TO I                                             00003890
                PERFORM  UNTIL  I > 14                                  00003900
                           OR  (I > W-NBLIEU AND                        00003910
                                I NOT = 1)                              00003920
                            OR  FIE310-CLIEUTRT = W-CLIEUTRT (I)        00003930
                   ADD 1 TO I                                           00003940
                END-PERFORM                                             00003950
                IF  I NOT > 14 AND I > W-NBLIEU                         00003960
                    ADD 1 TO W-NBLIEU                                   00003970
                    MOVE FIE310-CLIEUTRT TO W-CLIEUTRT (W-NBLIEU)       00003980
                END-IF                                                  00003990
                PERFORM LECTURE-FIE310                                  00004000
           END-PERFORM.                                                 00004010
      *                                                                 00004020
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > 14                   00004030
                MOVE W-CLIEUTRT (I) TO ED-CLIEUTRT (I)                  00004040
                MOVE '!'            TO ED-CLIEUTRT-FILL1 (I)            00004050
           END-PERFORM.                                                 00004070
      *                                                                 00004080
           CLOSE FIE310.                                                00004090
           OPEN INPUT FIE310.                                           00004100
           PERFORM LECTURE-FIE310.                                      00004110
      *                                                                 00004120
       FIN-REMPLIR-TABLE-LIEUX.         EXIT.                           00004130
      *                                                                 00004140
       ECRIRE-ENTETE                SECTION.                            00004150
      *                                                                 00004160
           ADD 1 TO W-NPAGE.                                            00004170
           MOVE W-NPAGE TO ED-NPAGE.                                    00004180
           MOVE FIE310-NSOCDEPOT TO ED-NSOCDEPOT.                       00004190
           MOVE FIE310-NDEPOT    TO ED-NDEPOT.                          00004200
           MOVE FIE310-NSSLIEU   TO ED-NSSLIEU.                         00004210
           MOVE '1' TO SAUT-PAGE.                                       00004220
           MOVE LIGNE-ENTETE-01 TO LIGNE1.                              00004230
           PERFORM ECRIRE-IIE311.                                       00004240
           MOVE ' ' TO SAUT-PAGE.                                       00004250
           MOVE LIGNE-VIDE      TO LIGNE1.                              00004260
           PERFORM ECRIRE-IIE311.                                       00004270
           MOVE LIGNE-ENTETE-02 TO LIGNE1.                              00004280
           PERFORM ECRIRE-IIE311.                                       00004290
           MOVE LIGNE-ENTETE-03 TO LIGNE1.                              00004300
           PERFORM ECRIRE-IIE311.                                       00004310
           MOVE LIGNE-ENTETE-04 TO LIGNE1.                              00004320
           PERFORM ECRIRE-IIE311.                                       00004330
           MOVE LIGNE-ENTETE-05 TO LIGNE1.                              00004340
           PERFORM ECRIRE-IIE311.                                       00004350
           MOVE LIGNE-ENTETE-06 TO LIGNE1.                              00004360
           PERFORM ECRIRE-IIE311.                                       00004370
           MOVE LIGNE-VIDE      TO LIGNE1.                              00004380
           PERFORM ECRIRE-IIE311.                                       00004390
           MOVE LIGNE-ENTETE-07 TO LIGNE1.                              00004400
           PERFORM ECRIRE-IIE311.                                       00004410
           MOVE LIGNE-ENTETE-08 TO LIGNE1.                              00004420
           PERFORM ECRIRE-IIE311.                                       00004430
           MOVE LIGNE-ENTETE-09 TO LIGNE1.                              00004440
           PERFORM ECRIRE-IIE311.                                       00004450
           MOVE LIGNE-ENTETE-10 TO LIGNE1.                              00004460
           PERFORM ECRIRE-IIE311.                                       00004470
           MOVE LIGNE-ENTETE-12 TO LIGNE1.                              00004480
           PERFORM ECRIRE-IIE311.                                       00004490
      *                                                                 00004500
       FIN-ECRIRE-ENTETE.          EXIT.                                00004510
      *                                                                 00004520
      *==============================================================*  00004530
      *                  T R A I T E M E N T                         *  00004540
      *==============================================================*  00004550
       TRAITEMENT    SECTION.                                           00004560
      *                                                                 00004570
           IF  FIE310-DSECT = HIGH-VALUE                                00004580
              SET FIN-FIE310 TO TRUE                                    00004590
           END-IF.                                                      00004600
           MOVE SPACES TO ED-CFAM                                       00004610
                          ED-CMARQ                                      00004620
                          ED-LREFFOURN                                  00004630
                          ED-NCODIC.                                    00004640
           IF  FIE310-CFAM      NOT = REF-CFAM AND                      00004650
               W-NPAGE NOT = 0                                          00004660
                 PERFORM ECRIRE-TOTAL-FAMILLE                           00004670
           END-IF.                                                      00004680
           IF (FIE310-NSOCDEPOT NOT = REF-NSOCDEPOT OR                  00004690
               FIE310-NDEPOT NOT = REF-NDEPOT) AND                      00004700
               W-NPAGE NOT = 0                                          00004710
                 PERFORM ECRIRE-TOTAL-ENTREPOT                          00004720
           END-IF.                                                      00004730
           IF (FIE310-NSOCDEPOT NOT = REF-NSOCDEPOT OR                  00004740
               FIE310-NDEPOT    NOT = REF-NDEPOT    OR                  00004750
               FIE310-NSSLIEU   NOT = REF-NSSLIEU   OR                  00004760
               W-NLIGNES        > W-NLIGNES-MAX) AND                    00004770
               NOT FIN-FIE310                                           00004780
                 MOVE  FIE310-NSOCDEPOT TO REF-NSOCDEPOT                00004790
                 MOVE  FIE310-NDEPOT    TO REF-NDEPOT                   00004800
                 MOVE  FIE310-NSSLIEU   TO REF-NSSLIEU                  00004810
                 MOVE  SPACES           TO REF-CFAM                     00004820
                 IF  W-NPAGE NOT = 0                                    00004830
                   MOVE LIGNE-ENTETE-12 TO LIGNE1                       00004840
                   PERFORM ECRIRE-IIE311                                00004850
                 END-IF                                                 00004860
                 PERFORM ECRIRE-ENTETE                                  00004870
                 MOVE 0 TO W-NLIGNES                                    00004880
           END-IF.                                                      00004890
      *                                                                 00004900
           IF  FIE310-CFAM NOT = REF-CFAM                               00004910
               MOVE FIE310-CFAM TO ED-CFAM                              00004920
                                   REF-CFAM                             00004930
           ELSE                                                         00004940
               MOVE SPACES TO ED-CFAM                                   00004950
           END-IF.                                                      00004960
           MOVE FIE310-CMARQ            TO ED-CMARQ REF-CMARQ.          00004970
           MOVE FIE310-LREFFOURN        TO ED-LREFFOURN REF-LREFFOURN.  00004980
           MOVE FIE310-NCODIC           TO ED-NCODIC.                   00004990
                                                                        00005000
           ADD 1 TO W-NLIGNES.                                          00005010
           IF  FIE310-DSECT NOT = HIGH-VALUE AND                        00005020
               W-NLIGNES NOT > W-NLIGNES-MAX                            00005030
             INITIALIZE W-TABLE-QSTOCK                                  00005040
             MOVE ZERO TO W-TOTAL-LIGNE-QSTOCKAV                        00005050
                          W-TOTAL-LIGNE-QSTOCKAP                        00005060
                          W-TOTAL-LIGNE-ECART                           00005070
             MOVE FIE310-NCODIC TO REF-NCODIC                           00005080
             PERFORM UNTIL FIE310-DSECT = HIGH-VALUE OR                 00005090
                         W-NLIGNES > W-NLIGNES-MAX OR                   00005100
                         FIE310-NSOCDEPOT NOT = REF-NSOCDEPOT OR        00005110
                         FIE310-NDEPOT NOT = REF-NDEPOT OR              00005120
                         FIE310-NSSLIEU NOT = REF-NSSLIEU OR            00005130
                         FIE310-NCODIC NOT = REF-NCODIC                 00005140
               PERFORM RECHERCHE-LIEU                                   00005150
               MOVE FIE310-QSTOCKAV TO W-QSTOCKAV (I)                   00005160
               MOVE FIE310-QSTOCKAP TO W-QSTOCKAP (I)                   00005170
               MOVE FIE310-ECART    TO W-ECART (I)                      00005180
               ADD  FIE310-QSTOCKAV TO W-TOTAL-LIGNE-QSTOCKAV           00005190
               ADD  FIE310-QSTOCKAP TO W-TOTAL-LIGNE-QSTOCKAP           00005200
               ADD  FIE310-ECART    TO W-TOTAL-LIGNE-ECART              00005210
               ADD  FIE310-QSTOCKAV TO W-TOTAL-FAMILLE-QSTOCKAV (I)     00005220
               ADD  FIE310-QSTOCKAP TO W-TOTAL-FAMILLE-QSTOCKAP (I)     00005230
               ADD  FIE310-ECART    TO W-TOTAL-FAMILLE-ECART (I)        00005240
               ADD  FIE310-QSTOCKAV TO W-TOTAL-ENTREPOT-QSTOCKAV (I)    00005250
               ADD  FIE310-QSTOCKAP TO W-TOTAL-ENTREPOT-QSTOCKAP (I)    00005260
               ADD  FIE310-ECART    TO W-TOTAL-ENTREPOT-ECART (I)       00005270
               PERFORM LECTURE-FIE310                                   00005280
             END-PERFORM                                                00005290
             SET STOCK-THEO-NUL TO TRUE                                 00005291
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > 14                 00005300
               IF W-QSTOCKAV (I) NOT = 0                                00005301
                  SET STOCK-THEO TO TRUE                                00005302
               END-IF                                                   00005303
               MOVE W-QSTOCKAV (I) TO ED-QSTOCK (I)                     00005310
               MOVE ' !'           TO ED-QSTOCK-FILL (I)                00005320
             END-PERFORM                                                00005330
             IF STOCK-THEO-NUL                                          00005331
                 MOVE SPACES TO ED-CFAM                                 00005332
                                ED-CMARQ                                00005333
                                ED-LREFFOURN                            00005334
                                ED-NCODIC                               00005335
                 MOVE '  ** STOCK THEORIQUE INEXISTANT **  ' TO W-MESS  00005336
             END-IF                                                     00005337
             MOVE W-TOTAL-LIGNE-QSTOCKAV TO ED-QSTOCK-TOTAL             00005340
             MOVE LIGNE-DETAIL TO LIGNE1                                00005350
             PERFORM ECRIRE-IIE311                                      00005360
             MOVE SPACES TO W-MESS                                      00005361
                                                                        00005370
             IF STOCK-THEO-NUL                                          00005371
                MOVE REF-CFAM      TO ED-CFAM                           00005380
                MOVE REF-CMARQ     TO ED-CMARQ                          00005390
                MOVE REF-LREFFOURN TO ED-LREFFOURN                      00005400
                MOVE REF-NCODIC    TO ED-NCODIC                         00005410
             ELSE                                                       00005411
                 MOVE SPACES TO ED-CFAM                                 00005412
                                ED-CMARQ                                00005413
                                ED-LREFFOURN                            00005414
                                ED-NCODIC                               00005415
             END-IF                                                     00005416
             SET STOCK-INVE-NUL TO TRUE                                 00005417
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > 14                 00005420
               IF W-QSTOCKAP (I) NOT = 0                                00005421
                  SET STOCK-INVE TO TRUE                                00005422
               END-IF                                                   00005423
               MOVE W-QSTOCKAP (I) TO ED-QSTOCK (I)                     00005430
             END-PERFORM                                                00005440
             IF STOCK-INVE-NUL                                          00005441
                 MOVE '  ** STOCK INVENTORIE INEXISTANT ** ' TO W-MESS  00005443
             END-IF                                                     00005444
             MOVE W-TOTAL-LIGNE-QSTOCKAP TO ED-QSTOCK-TOTAL             00005450
             MOVE LIGNE-DETAIL TO LIGNE1                                00005460
             PERFORM ECRIRE-IIE311                                      00005470
             MOVE SPACES TO W-MESS                                      00005471
                                                                        00005480
             MOVE SPACES TO ED-CFAM                                     00005481
                            ED-CMARQ                                    00005482
                            ED-LREFFOURN                                00005483
                            ED-NCODIC                                   00005484
             PERFORM VARYING I FROM 1 BY 1 UNTIL I > 14                 00005490
               MOVE W-ECART    (I) TO ED-QSTOCK (I)                     00005500
             END-PERFORM                                                00005510
             MOVE W-TOTAL-LIGNE-ECART    TO ED-QSTOCK-TOTAL             00005520
             MOVE LIGNE-DETAIL TO LIGNE1                                00005530
             PERFORM ECRIRE-IIE311                                      00005540
             MOVE LIGNE-ENTETE-11 TO LIGNE1                             00005550
             PERFORM ECRIRE-IIE311                                      00005560
           END-IF.                                                      00005570
                                                                        00005580
      *                                                                 00005590
       FIN-TRAITEMENT.              EXIT.                               00005600
      *                                                                 00005610
       RECHERCHE-LIEU            SECTION.                               00005620
      *                                                                 00005630
           MOVE 1 TO I.                                                 00005640
           PERFORM UNTIL  I > 14                                        00005650
                       OR W-CLIEUTRT (I) = FIE310-CLIEUTRT              00005660
              ADD 1 TO I                                                00005670
           END-PERFORM.                                                 00005680
           IF  I > W-NBLIEU                                             00005690
             ADD 1 TO W-NBLIEU                                          00005700
             IF  W-NBLIEU > 14                                          00005710
                DISPLAY 'TABLE TROP PETITE ' FIE310-CLIEUTRT                    
                MOVE 14 TO W-NBLIEU                                             
      *         MOVE 'TABLE DES LIEUX TROP PETITE ' TO ABEND-MESS       00005720
      *         PERFORM ABANDON-PROGRAMME                               00005730
      *      ELSE                                                       00005740
                MOVE W-NBLIEU TO I                                      00005750
                MOVE FIE310-CLIEUTRT TO W-CLIEUTRT (I)                  00005760
      *      END-IF                                                     00005770
           END-IF.                                                      00005780
      *                                                                 00005790
       FIN-RECHERCHE-LIEU.           EXIT.                              00005800
      *                                                                 00005810
       ECRIRE-TOTAL-FAMILLE          SECTION.                           00005820
      *                                                                 00005830
           MOVE '  TOTAL FAMILLE ' TO ED-LIBELLE-DETAIL2.               00005840
           MOVE REF-CFAM TO ED-CFAM-DETAIL2.                            00005850
      *                                                                 00005860
           MOVE 0 TO W-TOTAL-FAMILLE-LIGNE.                             00005870
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > W-NBLIEU             00005880
              MOVE W-TOTAL-FAMILLE-QSTOCKAV (I) TO ED-QSTOCK (I)        00005890
              ADD W-TOTAL-FAMILLE-QSTOCKAV (I) TO W-TOTAL-FAMILLE-LIGNE 00005900
           END-PERFORM.                                                 00005910
           MOVE W-TOTAL-FAMILLE-LIGNE TO ED-QSTOCK-TOTAL.               00005920
           MOVE LIGNE-DETAIL TO LIGNE1.                                 00005930
           PERFORM ECRIRE-IIE311.                                       00005940
      *                                                                 00005950
           MOVE '                    ' TO ED-LIBELLE-DETAIL2.           00005960
           MOVE '     '                TO ED-CFAM-DETAIL2.              00005970
           MOVE 0 TO W-TOTAL-FAMILLE-LIGNE.                             00005980
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > W-NBLIEU             00005990
              MOVE W-TOTAL-FAMILLE-QSTOCKAP (I) TO ED-QSTOCK (I)        00006000
              ADD W-TOTAL-FAMILLE-QSTOCKAP (I) TO W-TOTAL-FAMILLE-LIGNE 00006010
           END-PERFORM.                                                 00006020
           MOVE W-TOTAL-FAMILLE-LIGNE TO ED-QSTOCK-TOTAL.               00006030
           MOVE LIGNE-DETAIL TO LIGNE1.                                 00006040
           PERFORM ECRIRE-IIE311.                                       00006050
      *                                                                 00006060
           MOVE 0 TO W-TOTAL-FAMILLE-LIGNE.                             00006070
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > W-NBLIEU             00006080
              MOVE W-TOTAL-FAMILLE-ECART    (I) TO ED-QSTOCK (I)        00006090
              ADD W-TOTAL-FAMILLE-ECART    (I) TO W-TOTAL-FAMILLE-LIGNE 00006100
           END-PERFORM.                                                 00006110
           MOVE W-TOTAL-FAMILLE-LIGNE TO ED-QSTOCK-TOTAL.               00006120
           MOVE LIGNE-DETAIL TO LIGNE1.                                 00006130
           PERFORM ECRIRE-IIE311.                                       00006140
           IF  W-NLIGNES NOT > W-NLIGNES-MAX                            00006150
              MOVE LIGNE-ENTETE-12 TO LIGNE1                            00006160
              PERFORM ECRIRE-IIE311                                     00006170
           END-IF.                                                      00006180
      *                                                                 00006190
           INITIALIZE W-TABLE-FAMILLE-QSTOCK.                           00006200
      *                                                                 00006210
       FIN-ECRIRE-TOTAL-FAMILLE.    EXIT.                               00006220
      *                                                                 00006230
      *                                                                 00006240
       ECRIRE-TOTAL-ENTREPOT         SECTION.                           00006250
      *                                                                 00006260
           MOVE '  TOTAL ENTREPOT' TO ED-LIBELLE-DETAIL2.               00006270
           MOVE SPACES   TO ED-CFAM-DETAIL2.                            00006280
      *                                                                 00006290
           MOVE 0 TO W-TOTAL-ENTREPOT-LIGNE.                            00006300
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > W-NBLIEU             00006310
            MOVE W-TOTAL-ENTREPOT-QSTOCKAV (I) TO ED-QSTOCK (I)         00006320
            ADD W-TOTAL-ENTREPOT-QSTOCKAV (I) TO W-TOTAL-ENTREPOT-LIGNE 00006330
           END-PERFORM.                                                 00006340
           MOVE W-TOTAL-ENTREPOT-LIGNE TO ED-QSTOCK-TOTAL.              00006350
           MOVE LIGNE-DETAIL TO LIGNE1.                                 00006360
           PERFORM ECRIRE-IIE311.                                       00006370
      *                                                                 00006380
           MOVE '                    ' TO ED-LIBELLE-DETAIL2.           00006390
           MOVE '     '                TO ED-CFAM-DETAIL2.              00006400
           MOVE 0 TO W-TOTAL-ENTREPOT-LIGNE.                            00006410
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > W-NBLIEU             00006420
            MOVE W-TOTAL-ENTREPOT-QSTOCKAP (I) TO ED-QSTOCK (I)         00006430
            ADD W-TOTAL-ENTREPOT-QSTOCKAP (I) TO W-TOTAL-ENTREPOT-LIGNE 00006440
           END-PERFORM.                                                 00006450
           MOVE W-TOTAL-ENTREPOT-LIGNE TO ED-QSTOCK-TOTAL.              00006460
           MOVE LIGNE-DETAIL TO LIGNE1.                                 00006470
           PERFORM ECRIRE-IIE311.                                       00006480
      *                                                                 00006490
           MOVE 0 TO W-TOTAL-ENTREPOT-LIGNE.                            00006500
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > W-NBLIEU             00006510
            MOVE W-TOTAL-ENTREPOT-ECART    (I) TO ED-QSTOCK (I)         00006520
            ADD W-TOTAL-ENTREPOT-ECART    (I) TO W-TOTAL-ENTREPOT-LIGNE 00006530
           END-PERFORM.                                                 00006540
           MOVE W-TOTAL-ENTREPOT-LIGNE TO ED-QSTOCK-TOTAL.              00006550
           MOVE LIGNE-DETAIL TO LIGNE1.                                 00006560
           PERFORM ECRIRE-IIE311.                                       00006570
      *                                                                 00006580
           MOVE LIGNE-ENTETE-12 TO LIGNE1.                              00006590
           PERFORM ECRIRE-IIE311.                                       00006600
      *                                                                 00006610
           INITIALIZE W-TABLE-ENTREPOT-QSTOCK.                          00006620
      *                                                                 00006630
       FIN-ECRIRE-TOTAL-ENTREPOT.   EXIT.                               00006640
      *                                                                 00006650
      *                                                                 00006660
       LECTURE-FIE310           SECTION.                                00006670
      *                                                                 00006680
           READ FIE310 AT END                                           00006690
              MOVE HIGH-VALUE TO FIE310-DSECT                           00006700
           END-READ.                                                    00006710
           IF FIE310-DSECT NOT = HIGH-VALUE                             00006720
              ADD 1 TO W-LIGNES-FIE310-LUES                             00006730
              IF FIE310-CLIEUTRT (1:2) = 'IR'                                   
                 MOVE 'IRXXX' TO FIE310-CLIEUTRT                                
              END-IF                                                            
              IF FIE310-NSOCDEPOT = '961'                                       
                 IF  FIE310-CLIEUTRT (1:3) = 'SAV'                              
                 AND FIE310-CLIEUTRT (4:2) > '00'                               
                    MOVE 'SAVXX' TO FIE310-CLIEUTRT                             
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                      00006740
      *                                                                 00006750
       FIN-LECTURE-FIE310.      EXIT.                                   00006760
      *                                                                 00006770
      *                                                                 00006780
       ECRIRE-IIE311           SECTION.                                 00006790
      *                                                                 00006800
           WRITE ENR-IIE311 FROM LIGNE.                                 00006810
           ADD 1 TO W-LIGNES-IIE311-ECRITES.                            00006820
      *                                                                 00006830
      *                                                                 00006840
       FIN-ECRIRE-IIE311.      EXIT.                                    00006850
      *                                                                 00006860
      *---------------------------------------------------------------* 00006870
      *             A B A N D O N - P R O G R A M M E                 * 00006880
      *---------------------------------------------------------------* 00006890
      *                                                                 00006900
       ABANDON-PROGRAMME        SECTION.                                00006910
      *---------------------------------                                00006920
      *                                                                 00006930
           CLOSE FIE310                                                 00006940
                 IIE311.                                                00006950
      *                                                                 00006960
           DISPLAY '***************************************'.           00006970
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.           00006980
           DISPLAY '***************************************'.           00006990
      *                                                                 00007000
           MOVE  'BIE311'  TO    ABEND-PROG.                            00007010
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                00007020
      *                                                                 00007030
       FIN-ABANDON-PROGRAMME. EXIT.                                     00007040
          EJECT                                                         00007050
      *                                                                 00007060
