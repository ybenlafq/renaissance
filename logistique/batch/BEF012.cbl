      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                BEF012.                                       
       AUTHOR.                    DSA011-FC.                                    
      *================================================================*        
      *                                                                *        
      *       EDITION DE VALORISATION DE L'ENCOURS FOURNISSEUR         *        
      *          VALO GLOBALE AU PRMP     (ETAT IEF012)                *        
      *                                                                *        
      *================================================================*        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN TO FDATE.                                     
      *--                                                                       
            SELECT FDATE   ASSIGN TO FDATE                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FEF010  ASSIGN TO FEF010.                                    
      *--                                                                       
            SELECT FEF010  ASSIGN TO FEF010                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IEF012  ASSIGN TO IEF012.                                    
      *--                                                                       
            SELECT IEF012  ASSIGN TO IEF012                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *     F I C H I E R   D A T E   J O U R                                   
      * -------------------------------------------------------                 
       FD   FDATE                                                               
            RECORDING F LABEL RECORD STANDARD.                                  
       01  FDATE-ENREG                 PIC  X(80).                              
      *     F I C H I E R   D E S   A N O M A L I E S                           
      * ----------------------------------------------                          
       FD  FEF010                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FEF010-ENREG PIC X(120).                                             
      *                                                                         
      *     F I C H I E R   I M P R E S S I O N                                 
      * ---------------------------------------                                 
       FD  IEF012                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  IEF012-ENREG    PIC X(133).                                          
      *                                                                         
      *================================================================*        
       WORKING-STORAGE            SECTION.                                      
      *================================================================*        
      *                                                                 00660000
      * MODULES APPELES PAR CALL                                        00670000
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *77  ABEND                   PIC X(08) VALUE 'ABEND   '.          00680000
      *--                                                                       
       77  MW-ABEND                   PIC X(08) VALUE 'ABEND   '.               
      *}                                                                        
       77  BETDATC                 PIC X(08) VALUE 'BETDATC '.          00680000
      *                                                                 00660000
       01  VARIABLES-NUM.                                                       
           05  I                   PIC S9(3) COMP-3.                            
           05  N                   PIC S9(3) COMP-3.                            
           05  W-L                 PIC S9(3) COMP-3.                            
           05  W-P                 PIC S9(3) COMP-3.                            
           05  W-LUS-EF            PIC S9(7) COMP-3.                            
           05  W-TAB-MT.                                                        
               07 W-NBAPP          PIC S9(11)    OCCURS 5.                      
               07 W-VALO           PIC S9(13)V99 OCCURS 5.                      
       01  VARIABLES-ALPHA.                                                     
      *    05  W-NSOC              PIC X(3).                                    
      *    05  W-NLIEU             PIC X(3).                                    
           05  W-CTRAIT            PIC X(5).                                    
           05  W-LTRAIT            PIC X(20).                                   
           05  W-CTIERS            PIC X(5).                                    
           05  W-LTIERS            PIC X(20).                                   
           05  W-CRENDU            PIC X(5).                                    
           05  W-MENVOI            PIC X(6).                                    
           05  W-MMSSAA            PIC X(7).                                    
       01  VARIABLES-TEST.                                                      
           05  W-FIN-EF            PIC 9.                               00660000
             88  FIN-EF VALUE 1.                                                
      *================================================================*        
       COPY SWEF010.                                                            
       COPY ABENDCOP.                                                           
       COPY WORKDATC.                                                           
      *================================================================*        
      *---------------------------------------------------------------          
       01      IEF012-DSECT.                                                    
           03  IEF012-ASA           PIC  X(01).                         00000270
           03  IEF012-LIG           PIC  X(132).                        00000320
                                                                        00000390
       01  E00.                                                                 
           05 FILLER PIC X(30) VALUE 'I E F 0 1 2'.                             
           05 FILLER PIC X(30) VALUE 'V A L O R I S A T I O N     D '.          
           05 FILLER PIC X(30) VALUE ' E     L '' E N C O U R S     F'.         
           05 FILLER PIC X(30) VALUE ' O U R N I S S E U R          '.          
           05 FILLER PIC X(07) VALUE ' PAGE :'.                                 
           05 E00-PAGE PIC Z(4)9.                                               
       01  E01.                                                                 
           05 FILLER PIC X(43) VALUE ' '.                                       
           05 FILLER PIC X(30) VALUE 'E N C O U R S    T O T A L    '.          
           05 FILLER PIC X(30) VALUE 'V A L O R I S E               '.          
       01  E02.                                                                 
           05 FILLER   PIC X(18) VALUE 'DATE D''ENCOURS  :'.                    
           05 E02-DATE PIC X(10).                                               
       01  E03.                                                                 
           05 FILLER PIC X(16) VALUE ALL ' '.                                   
           05 FILLER PIC X(30) VALUE '+----------------------------+'.          
           05 FILLER PIC X(30) VALUE '------------------------------'.          
           05 FILLER PIC X(30) VALUE '------------------------------'.          
           05 FILLER PIC X(10) VALUE '---------+'.                              
       01  E04.                                                                 
           05 FILLER PIC X(16) VALUE ALL ' '.                                   
           05 FILLER PIC X(30) VALUE '!   FOURNISSEUR              !'.          
           05 FILLER PIC X(30) VALUE ' PERIODE   CENTRE ENVOI   NUME'.          
           05 FILLER PIC X(30) VALUE 'RO   DATE       NBRE D''      V'.         
           05 FILLER PIC X(10) VALUE 'ALEUR    !'.                              
       01  E05.                                                                 
           05 FILLER PIC X(16) VALUE ALL ' '.                                   
           05 FILLER PIC X(30) VALUE '!                            !'.          
           05 FILLER PIC X(30) VALUE ' ENVOI                    ENVO'.          
           05 FILLER PIC X(30) VALUE 'I    ENVOI      APPAREILS    P'.          
           05 FILLER PIC X(10) VALUE 'P.R.M.P  !'.                              
       01  E06       PIC X(133).                                                
       01  E07.                                                                 
           05 FILLER PIC X(16) VALUE ALL ' '.                                   
           05 FILLER PIC X(30) VALUE '!                            !'.          
           05 FILLER PIC X(30) VALUE '                              '.          
           05 FILLER PIC X(30) VALUE '                ---------    -'.          
           05 FILLER PIC X(10) VALUE '-------- !'.                              
       01  E08.                                                                 
           05 FILLER PIC X(16) VALUE ALL ' '.                                   
           05 FILLER PIC X(30) VALUE '!                            !'.          
           05 FILLER PIC X(30) VALUE '------------------------------'.          
           05 FILLER PIC X(30) VALUE '------------------------------'.          
           05 FILLER PIC X(10) VALUE '---------!'.                              
       01  E09       PIC X(133).                                                
       01  E10       PIC X(133).                                                
       01  D01.                                                                 
           05 FILLER     PIC X(18).                                             
           05 D01-CTIERS PIC X(06).                                             
           05 D01-LTIERS PIC X(20).                                             
           05 FILLER     PIC X(03).                                             
           05 D01-LIB.                                                          
            7 D01-MENVOI PIC X(10).                                             
      *     7 D01-NSOC   PIC X(04).                                             
      *     7 D01-NLIEU  PIC X(04).                                             
            7 D01-CTRAIT PIC X(15).                                             
            7 D01-NENVOI PIC X(09).                                             
            7 D01-DENVOI PIC X(10).                                             
            7 D01-NBAPP  PIC Z(09)9.                                            
            7 D01-VALO   PIC Z(12)9.                                            
           05 FILLER     PIC X(02).                                             
      *                                                                         
      *                                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *                                                                         
      *                                                                         
           PERFORM DEBUT                                                        
           PERFORM TRAITEMENT-EF UNTIL FIN-EF                                   
           PERFORM FIN-NORMALE                                                  
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP    RUN.                                                         
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *===============================================================*         
       DEBUT SECTION.                                                           
      *===============================================================*         
           OPEN INPUT   FDATE                                                   
                        FEF010                                                  
                OUTPUT  IEF012                                                  
           DISPLAY '*=============================================*'            
           DISPLAY '*          DEBUT DU PROGRAMME BEF012          *'            
           DISPLAY '*=============================================*'            
      *--- INIT GENERALES WORK                                                  
           INITIALIZE VARIABLES-NUM                                             
                      VARIABLES-ALPHA                                           
                      VARIABLES-TEST                                            
      *--- VERIFICATION DATE DE TRAITEMENT                                      
           READ FDATE   INTO GFJJMMSSAA END                                     
                MOVE '*** FDATE VIDE *** ' TO ABEND-MESS                        
                PERFORM FIN-ANORMALE                                            
           END-READ                                                             
           MOVE '1' TO GFDATA                                                   
           CALL BETDATC USING WORK-BETDATC                                      
           IF   GFVDAT NOT = '1'                                                
                MOVE 'FDATE REJETEE PAR BETDATC' TO ABEND-MESS                  
                PERFORM FIN-ANORMALE                                            
           END-IF                                                               
           DISPLAY '*  DATE DE TRAITEMENT : ' GFJMSA-5                          
           DISPLAY '*'                                                          
      *--- PRECHARGEMENT DES ZONES D'ENTETE                                     
           MOVE     GFJMSA-5   TO E02-DATE                                      
           MOVE     E03        TO E06 E09 E10                                   
           INSPECT  E06  REPLACING ALL '-' BY  ' '                              
           INSPECT  E06  REPLACING ALL '+' BY  '!'                              
           INSPECT  E09  REPLACING ALL '-' BY  '='                              
           INSPECT  E09  REPLACING ALL '+' BY  '!'                              
           INSPECT  E10  REPLACING ALL '-' BY  '*'                              
           INSPECT  E10  REPLACING ALL '+' BY  '!'                              
           MOVE     E06        TO D01                                           
           PERFORM  SAUT-PAGE                                                   
      *--- LECTURE INITIALE FEF010                                              
           READ FEF010 INTO FEF010-DSECT                                        
                END     SET FIN-EF TO TRUE                                      
                        MOVE 'AUCUN PRODUIT ENCOURS-FOURNISSEUR'                
                             TO D01-LTIERS                                      
                        MOVE D01 TO IEF012-LIG                                  
                        PERFORM     IEF012-WRITE                                
                        MOVE E03 TO IEF012-LIG                                  
                        PERFORM     IEF012-WRITE                                
           END-READ.                                                            
      *===============================================================*         
      *===============================================================*         
       TRAITEMENT-EF SECTION.                                                   
           PERFORM    UNTIL FIN-EF                                              
              MOVE       FEF010-CTIERS    TO    W-CTIERS D01-CTIERS             
              MOVE       FEF010-LTIERS    TO    W-LTIERS D01-LTIERS             
              PERFORM UNTIL FIN-EF                                              
                      OR FEF010-CTIERS    NOT = W-CTIERS                        
                 MOVE    FEF010-DENVOI    TO    W-MENVOI                        
                 MOVE    FEF010-DENVOI(5:2) TO  W-MMSSAA                        
                 MOVE    '/'                TO  W-MMSSAA(3:1)                   
                 MOVE    FEF010-DENVOI(1:4) TO  W-MMSSAA(4:4)                   
                 MOVE    W-MMSSAA           TO  D01-MENVOI                      
                 PERFORM UNTIL FIN-EF                                           
                      OR FEF010-CTIERS       NOT = W-CTIERS                     
                      OR FEF010-DENVOI(1:6)  NOT = W-MENVOI                     
      *             MOVE FEF010-NSOCENV      TO    W-NSOC   D01-NSOC            
      *             MOVE FEF010-NLIEUENV     TO    W-NLIEU  D01-NLIEU           
                    MOVE FEF010-CTRAIT       TO    W-CTRAIT D01-CTRAIT          
                    MOVE FEF010-LTRAIT       TO    W-LTRAIT                     
                    PERFORM UNTIL FIN-EF                                        
                       OR FEF010-CTIERS      NOT = W-CTIERS                     
                       OR FEF010-DENVOI(1:6) NOT = W-MENVOI                     
      *                OR FEF010-NSOCENV     NOT = W-NSOC                       
      *                OR FEF010-NLIEUENV    NOT = W-NLIEU                      
                       OR FEF010-CTRAIT      NOT = W-CTRAIT                     
                       MOVE FEF010-NENVOI       TO D01-NENVOI                   
                       MOVE FEF010-DENVOI(7:2)  TO D01-DENVOI                   
                       MOVE '/'                 TO D01-DENVOI(3:1)              
                       MOVE W-MMSSAA            TO D01-DENVOI(4:7)              
                       MOVE 1 TO N                                              
                       MOVE FEF010-QTENV        TO   W-NBAPP(N)                 
                       MOVE FEF010-PRMP         TO   W-VALO (N)                 
                       PERFORM EDIT-D1                                          
                       ADD  1 TO W-LUS-EF                                       
                       READ FEF010 INTO FEF010-DSECT                            
                            END SET FIN-EF TO TRUE                              
                       END-READ                                                 
                    END-PERFORM                                                 
                    MOVE    2         TO N                                      
                    MOVE   11         TO I                                      
      *             STRING 'TOTAL ' W-NSOC ' ' W-NLIEU ' ' W-CTRAIT             
                    STRING 'TOTAL ' W-CTRAIT                                    
                           DELIMITED SIZE INTO D01-LIB WITH POINTER I           
                    PERFORM EDIT-D1                                             
                 END-PERFORM                                                    
                 MOVE    3         TO N                                         
                 STRING 'TOTAL '  W-MMSSAA DELIMITED SIZE INTO D01-LIB          
                 PERFORM EDIT-D1                                                
              END-PERFORM                                                       
              MOVE   4          TO N                                            
              MOVE   'TOTAL'    TO D01-CTIERS                                   
              MOVE    W-LTIERS  TO D01-LTIERS                                   
              PERFORM EDIT-D1                                                   
           END-PERFORM                                                          
           MOVE    5         TO N                                               
           MOVE    'TOTAL'   TO D01-CTIERS                                      
           MOVE   'GENERAL'  TO D01-LTIERS                                      
           PERFORM EDIT-D1.                                                     
      *                                                                         
      *                                                                         
      *                                                                         
       EDIT-D1 SECTION.                                                         
           IF (N = 1 AND W-L > 62)                                              
           OR (N > 1 AND W-L > 58 AND N < 5)                                    
              PERFORM SAUT-PAGE                                                 
           END-IF                                                               
           MOVE W-NBAPP(N)  TO D01-NBAPP                                        
           COMPUTE             D01-VALO ROUNDED = W-VALO (N)                    
           ADD  1 N GIVING I                                                    
           IF   N < 5                                                           
             ADD  W-NBAPP(N) TO W-NBAPP(I)                                      
             ADD  W-VALO (N) TO W-VALO (I)                                      
             MOVE 0          TO W-NBAPP(N)                                      
             MOVE 0          TO W-VALO (N)                                      
           END-IF                                                               
           EVALUATE N                                                           
              WHEN 2       MOVE E07  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
              WHEN 3       MOVE E08  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
              WHEN 4       MOVE E09  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
              WHEN 5       MOVE E10  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
           END-EVALUATE                                                         
           MOVE D01        TO IEF012-LIG                                        
           PERFORM            IEF012-WRITE                                      
           MOVE E06        TO D01                                               
           EVALUATE N                                                           
              WHEN 3       MOVE E08  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
              WHEN 4       MOVE E09  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
              WHEN 5       MOVE E10  TO IEF012-LIG                              
                           PERFORM      IEF012-WRITE                            
           END-EVALUATE                                                         
           IF N > 1 AND N < 5                                                   
              MOVE E06  TO IEF012-LIG                                           
              PERFORM      IEF012-WRITE                                         
           END-IF.                                                              
      *                                                                         
      *                                                                         
      *                                                                         
       SAUT-PAGE SECTION.                                                       
           IF  W-P > 0                                                          
               MOVE E03 TO IEF012-LIG                                           
               PERFORM     IEF012-WRITE                                         
           END-IF                                                               
           ADD 1 TO W-P                                                         
           MOVE     W-P TO E00-PAGE                                             
           MOVE '1'     TO IEF012-ASA                                           
           MOVE E00     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE '0'     TO IEF012-ASA                                           
           MOVE E01     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE ' '     TO IEF012-ASA                                           
           MOVE E02     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE '-'     TO IEF012-ASA                                           
           MOVE E03     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE ' '     TO IEF012-ASA                                           
           MOVE E04     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE E05     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE E03     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           MOVE E06     TO IEF012-LIG                                           
           PERFORM         IEF012-WRITE                                         
           IF N < 4                                                             
              MOVE       W-CTIERS TO D01-CTIERS                                 
              MOVE       W-LTIERS TO D01-LTIERS                                 
              IF N < 3                                                          
                 MOVE    W-MMSSAA TO D01-MENVOI                                 
                 IF N < 2                                                       
      *             MOVE W-NSOC   TO D01-NSOC                                   
      *             MOVE W-NLIEU  TO D01-NLIEU                                  
                    MOVE W-CTRAIT TO D01-CTRAIT                                 
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *===============================================================*         
      *===============================================================*         
       IEF012-WRITE SECTION.                                                    
      *===============================================================*         
           WRITE    IEF012-ENREG FROM IEF012-DSECT                              
           EVALUATE IEF012-ASA                                                  
               WHEN '1'        MOVE 1 TO W-L                                    
               WHEN ' '        ADD  1 TO W-L                                    
               WHEN '0'        ADD  2 TO W-L                                    
               WHEN '-'        ADD  3 TO W-L                                    
           END-EVALUATE.                                                        
      *===============================================================*         
      *===============================================================*         
       FIN-NORMALE SECTION.                                                     
      *===============================================================*         
           PERFORM FERMETURE-FICHIERS                                           
           DISPLAY '*'                                                          
           DISPLAY '*'                                                          
           DISPLAY '*================================================='         
           DISPLAY '*      FIN NORMALE DU PROGRAMME BEF012'                     
           DISPLAY '*================================================='         
           DISPLAY '*'                                                          
           DISPLAY '* LIGNES LUES DANS FEF010      : ' W-LUS-EF                 
           DISPLAY '*'                                                          
           DISPLAY '* NB PAGES DANS IEF012         : ' W-P                      
           DISPLAY '*'                                                          
           DISPLAY '*================================================='.        
      *================================================================*        
      *===============================================================*         
       FIN-ANORMALE SECTION.                                                    
      *===============================================================*         
           PERFORM FERMETURE-FICHIERS                                           
           DISPLAY '*'                                                          
           DISPLAY '*================================================='         
           DISPLAY '*'                                                          
           DISPLAY '*** ANOMALIE PENDANT EXECUTION DU BEF012 ***'               
           DISPLAY '*** ' ABEND-MESS                                            
           DISPLAY '*'                                                          
           DISPLAY '*================================================='         
           MOVE   'BEF012'  TO ABEND-PROG                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL    ABEND USING ABEND-PROG ABEND-MESS.                           
      *--                                                                       
           CALL    MW-ABEND USING ABEND-PROG ABEND-MESS.                        
      *}                                                                        
      *================================================================*        
      *================================================================*        
       FERMETURE-FICHIERS SECTION.                                              
      *================================================================*        
           CLOSE FDATE                                                          
                 FEF010                                                         
                 IEF012.                                                        
      *================================================================*        
      * THAT'S ALL FOLKS                                                        
