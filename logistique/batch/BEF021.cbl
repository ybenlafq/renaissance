      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID         DIVISION.                                             00000020
       PROGRAM-ID.  BEF021.                                             00000040
       AUTHOR.   DSA045-CP.                                             00000050
       DATE-COMPILED.                                                   00000060
      ******************************************************************00000080
      * 12.06.2007                                                     *00000090
      * ==========                                                     *00000090
      *                                                                *00000090
      * EXTRACTION DU FICHIER SEQUENTIEL A PARTIR DU FICHIER VSAM :    *00000090
      *       -  LISTE ENCOURS FOURNISSEUR   (JEF021)                  *00000090
      *         (CUMUL DU 1ER AU DIMANCHE ET MOIS)                     *00000090
      *----------------------------------------------------------------*00000080
      * 22.11.2007                                                     *        
      * DSA043 : AJOUT DE L'ECO PARTICIPATION (221107)                 *        
      ******************************************************************00000080
      *                                                                 00000080
       ENVIRONMENT DIVISION.                                            00000150
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *****************************************************************         
      * FICHIERS EN ENTREE :                                          *         
      *****************************************************************         
      * FJEF21E : LISTE ENCOURS FOURNISSEUR                           *         
      *****************************************************************         
           SELECT FJEF21E    ASSIGN   TO    FJEF21E                     00000250
                             ORGANIZATION   INDEXED                             
                             ACCESS MODE    DYNAMIC                             
                             RECORD KEY FJEF21E-CLE                             
                             FILE STATUS ST-FJEF21E.                            
      *                                                                 00000190
      *****************************************************************         
      * FDATE   : FICHIER DATE D'EXTRACTION                           *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE      ASSIGN   TO     FDATE                      00000250
      *                      FILE STATUS  ST-FDATE.                             
      *--                                                                       
           SELECT FDATE      ASSIGN   TO     FDATE                              
                             FILE STATUS  ST-FDATE                              
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *****************************************************************         
      * FICHIERS CSV EN SORTIE :                                      *         
      *****************************************************************         
      * FJEF21S : FICHIER D'EDITION LISTE DE DESTOCKAGE               *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FJEF21S    ASSIGN   TO    FJEF21S                     00000250
      *                      FILE STATUS ST-FJEF21S.                            
      *--                                                                       
           SELECT FJEF21S    ASSIGN   TO    FJEF21S                             
                             FILE STATUS ST-FJEF21S                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE  SECTION.                                                   00000290
      *                                                                 00000190
       FD  FDATE                                                        00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FDATE-ENREG               PIC X(80).                                 
      *                                                                 00000190
       FD  FJEF21E.                                                     00000440
       01  FJEF21E-ENREG.                                                       
           05  FJEF21E-CLE.                                                     
               10  FJEF21E-ETAT      PIC X(006).                                
               10  FJEF21E-DATE      PIC X(008).                                
               10  FJEF21E-USER      PIC X(008).                                
               10  FJEF21E-NSEQ      PIC 9(005).                                
           05  FJEF21E-RESTES        PIC X(223).                                
      *                                                                 00000190
       FD  FJEF21S                                                      00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
      *01  FJEF21S-ENREG             PIC X(188).                                
221107 01  FJEF21S-ENREG             PIC X(197).                                
      *                                                                         
      *****************************************************************         
      *    W O R K I N G   S T O R A G E   S E C T I O N              *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                         00000710
       77  ST-FDATE                  PIC X(02).                                 
       77  ST-FJEF21E                PIC X(02).                                 
       77  ST-FJEF21S                PIC X(02).                                 
       77  WRK-RESTES                PIC 9(02).                                 
       77  WRK-NBRLUS                PIC 9(05)  COMP-3.                         
       77  WRK-JEFECR                PIC 9(05)  COMP-3.                         
      *                                                                         
       01  WRK-PARAMETRE                        VALUE SPACES.                   
           05  WRK-DAT-TRAITD        PIC X(08).                                 
           05  FILLER                REDEFINES  WRK-DAT-TRAITD.                 
               10  WRK-DAT-TRTSAD.                                              
                   15  WRK-DAT-TRTSD PIC X(02).                                 
                   15  WRK-DAT-TRTAD PIC X(02).                                 
               10  WRK-DAT-TRTMMD    PIC X(02).                                 
               10  WRK-DAT-TRTJJD    PIC X(02).                                 
           05  WRK-DAT-TRAITF        PIC X(08).                                 
           05  FILLER                REDEFINES  WRK-DAT-TRAITF.                 
               10  WRK-DAT-TRTSAF.                                              
                   15  WRK-DAT-TRTSF PIC X(02).                                 
                   15  WRK-DAT-TRTAF PIC X(02).                                 
               10  WRK-DAT-TRTMMF    PIC X(02).                                 
               10  WRK-DAT-TRTJJF    PIC X(02).                                 
       01  WRK-DAT-SSAAMMJJ          PIC X(08).                                 
       01  FILLER                    REDEFINES  WRK-DAT-SSAAMMJJ.               
           05  WRK-DAT-MACSSAA.                                                 
               10  WRK-DAT-MACSS     PIC X(02).                                 
               10  WRK-DAT-MACAA     PIC X(02).                                 
               10  WRK-DAT-MACAA-R   REDEFINES  WRK-DAT-MACAA                   
                                     PIC 9(02).                                 
           05  WRK-DAT-MACSSAA-R     REDEFINES  WRK-DAT-MACSSAA                 
                                     PIC 9(04).                                 
           05  WRK-DAT-MACMOIS       PIC X(02).                                 
           05  WRK-DAT-MACMOIS-R     REDEFINES  WRK-DAT-MACMOIS                 
                                     PIC 9(02).                                 
           05  WRK-DAT-MACJOUR       PIC X(02).                                 
       01  WRK-DAT-SSAAMMJJ-R        REDEFINES  WRK-DAT-SSAAMMJJ                
                                     PIC 9(08).                                 
      *                                                                 00000600
       01  WRK-FLAGS                 PIC X(01)  VALUE SPACES.                   
           88  WRK-FINTRT            VALUE '1'.                                 
      *                                                                         
       01  TB-TABLEM.                                                           
           05  FILLER                PIC X(02)  VALUE '31'.                     
           05  FILLER                PIC X(02)  VALUE '00'.                     
           05  FILLER                PIC X(02)  VALUE '31'.                     
           05  FILLER                PIC X(02)  VALUE '30'.                     
           05  FILLER                PIC X(02)  VALUE '31'.                     
           05  FILLER                PIC X(02)  VALUE '30'.                     
           05  FILLER                PIC X(02)  VALUE '31'.                     
           05  FILLER                PIC X(02)  VALUE '31'.                     
           05  FILLER                PIC X(02)  VALUE '30'.                     
           05  FILLER                PIC X(02)  VALUE '31'.                     
           05  FILLER                PIC X(02)  VALUE '30'.                     
           05  FILLER                PIC X(02)  VALUE '31'.                     
       01  FILLER                    REDEFINES  TB-TABLEM.                      
           05  TB-TBMOIS             OCCURS 12.                                 
               10  TB-NB-JOUR        PIC 9(02).                                 
      *                                                                         
       01  WRK-DATEXTR               PIC X(08).                                 
       01  FILLER                    REDEFINES  WRK-DATEXTR.                    
           05  WRK-DATE-JJ           PIC 9(02).                                 
           05  WRK-DATE-MM           PIC 9(02).                                 
           05  WRK-DATE-SA.                                                     
               10  WRK-DATE-SS       PIC 9(02).                                 
               10  WRK-DATE-AA       PIC 9(02).                                 
           05  WRK-DATE-SA-R         REDEFINES  WRK-DATE-SA                     
                                     PIC 9(04).                                 
       01  WRK-DATEXTR-R             REDEFINES  WRK-DATEXTR                     
                                     PIC 9(08).                                 
       01  WRK-DAT-CALCULEE          PIC 9(08).                                 
      *                                                                         
       01  WRK-CALCUL.                                                          
           05  WRK-DAT-CALSSAA.                                                 
               10  WRK-DAT-CALSS     PIC X(02).                                 
               10  WRK-DAT-CALAA     PIC X(02).                                 
               10  WRK-DAT-CALAA-R   REDEFINES  WRK-DAT-CALAA                   
                                     PIC 9(02).                                 
           05  WRK-DAT-CALSSAA-R     REDEFINES  WRK-DAT-CALSSAA                 
                                     PIC 9(04).                                 
           05  WRK-DAT-CALMOIS       PIC X(02).                                 
           05  WRK-DAT-CALMOIS-R     REDEFINES  WRK-DAT-CALMOIS                 
                                     PIC 9(02).                                 
           05  WRK-DAT-CALJOUR       PIC X(02).                                 
           05  WRK-DAT-CALJOUR-R     REDEFINES  WRK-DAT-CALJOUR                 
                                     PIC 9(02).                                 
       01  WRK-CALCUL-R              REDEFINES  WRK-CALCUL                      
                                     PIC 9(08).                                 
      *                                                                 00000600
       01  WRK-PARAMETRE.                                                       
           05  WRK-CLEFIC.                                                      
               10  WRK-ETAT              PIC X(06)  VALUE 'JEF021'.             
               10  WRK-DATED             PIC X(08).                             
               10  FILLER                REDEFINES  WRK-DATED.                  
                   15  WRK-DAT-DEBSSAA.                                         
                       20  WRK-DAT-DEBSS PIC X(02).                             
                       20  WRK-DAT-DEBAA PIC X(02).                             
                       20  WRK-DAT-DEBAA-R   REDEFINES WRK-DAT-DEBAA            
                                         PIC 9(02).                             
                   15  WRK-DAT-DEBSSAA-R REDEFINES  WRK-DAT-DEBSSAA             
                                         PIC 9(04).                             
                   15  WRK-DAT-DEBMOIS   PIC X(02).                             
                   15  WRK-DAT-DEBMOIS-R REDEFINES  WRK-DAT-DEBMOIS             
                                         PIC 9(02).                             
                   15  WRK-DAT-DEBJOUR   PIC X(02).                             
                   15  WRK-DAT-DEBJOUR-R REDEFINES  WRK-DAT-DEBJOUR             
                                         PIC 9(02).                             
               10  WRK-DATED-R           REDEFINES  WRK-DATED                   
                                         PIC 9(08).                             
           05  WRK-DATEF                 PIC X(08).                             
           05  FILLER                    REDEFINES  WRK-DATEF.                  
               10  WRK-DAT-FINSSAA.                                             
                   15  WRK-DAT-FINSS     PIC X(02).                             
                   15  WRK-DAT-FINAA     PIC X(02).                             
               10  WRK-DAT-FINSSAA-R     REDEFINES  WRK-DAT-FINSSAA             
                                         PIC 9(04).                             
               10  WRK-DAT-FINMOIS       PIC X(02).                             
               10  WRK-DAT-FINMOIS-R     REDEFINES  WRK-DAT-FINMOIS             
                                         PIC 9(02).                             
               10  WRK-DAT-FINJOUR       PIC X(02).                             
               10  WRK-DAT-FINJOUR-R     REDEFINES  WRK-DAT-FINJOUR             
                                         PIC 9(02).                             
           05  WRK-DATEF-R               REDEFINES  WRK-DATEF                   
                                         PIC 9(08).                             
      *                                                                 00000600
           COPY SYKWF021.                                                       
      *                                                                 00000600
      *                                                                 00000600
           COPY SWEFF021.                                                       
      *                                                                 00000600
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
       PROCEDURE  DIVISION.                                             00002160
       TRT-GENERAL  SECTION.                                                    
           PERFORM  MODULE-ENTREE.                                              
           PERFORM  MODULE-INITIAL  UNTIL WRK-FINTRT.                           
           PERFORM  MODULE-SORTIE.                                              
           GOBACK.                                                              
       TRT-GENERAL-F. EXIT.                                                     
      *                                                                         
      *----------------------------------------------------------------*        
      *    OUVERTURE DES FICHIERS FDATE FJEF21E FJEF21S                *        
      *----------------------------------------------------------------*        
       MODULE-ENTREE  SECTION.                                                  
           INITIALIZE  WRK-FLAGS.                                               
           MOVE ZEROS                TO WRK-JEFECR                              
                                        WRK-NBRLUS.                             
           OPEN  INPUT FDATE.                                                   
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FDATE  = ' ST-FDATE            
               GOBACK                                                           
           END-IF.                                                              
           OPEN  INPUT FJEF21E.                                                 
           IF  ST-FJEF21E        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJEF21E = ' ST-FJEF21E         
               CLOSE  FDATE                                                     
               GOBACK                                                           
           END-IF.                                                              
           OPEN OUTPUT FJEF21S.                                                 
           IF  ST-FJEF21S        NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJEF21S = ' ST-FJEF21S         
               CLOSE  FDATE FJEF21E                                             
               GOBACK                                                           
           END-IF.                                                              
      *    ACCEPT WRK-DATEF        FROM DATE YYYYMMDD.                          
           ACCEPT WRK-DAT-SSAAMMJJ FROM DATE YYYYMMDD.                          
      *---------------------------------------------------------                
      *--- LECTURE DU FICHIER DATE JJMMSSAA                                     
      *---------------------------------------------------------                
           READ FDATE    INTO WRK-DATEXTR.                                      
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY 'FICHIER FDATE VIDE'                                     
               DISPLAY 'DATE DU JOUR PRISE PAR DEFAUT'                          
               MOVE WRK-DAT-MACSSAA  TO WRK-DATE-SA                             
               MOVE WRK-DAT-MACMOIS  TO WRK-DATE-MM                             
               MOVE WRK-DAT-MACJOUR  TO WRK-DATE-JJ                             
           ELSE                                                                 
               MOVE WRK-DATE-SA      TO WRK-DAT-MACSSAA                         
               MOVE WRK-DATE-MM      TO WRK-DAT-MACMOIS                         
               MOVE WRK-DATE-JJ      TO WRK-DAT-MACJOUR                         
           END-IF.                                                              
           MOVE WRK-DAT-SSAAMMJJ     TO WRK-DATED                               
                                        WRK-DATEF.                              
           MOVE '01'                 TO WRK-DAT-DEBJOUR.                        
           IF  WRK-DAT-MACMOIS       < '01' OR                                  
               WRK-DAT-MACMOIS       > '12'                                     
               DISPLAY '*B E F 0 2 1'                                           
               DISPLAY '*MOIS ERRONEE = ' WRK-DAT-MACMOIS                       
               CLOSE FDATE FJEF21E FJEF21S                                      
               GOBACK                                                           
           END-IF.                                                              
           PERFORM  MODULE-MEF-DATES.                                           
           MOVE WRK-DAT-MACJOUR      TO WRK-DAT-TRTJJF.                         
           IF  WRK-DAT-MACJOUR   NOT =  TB-NB-JOUR(WRK-DAT-MACMOIS-R)           
               DISPLAY '*  TRAITEMENT HEBDOMADAIRE'                             
           ELSE                                                                 
               DISPLAY '*  TRAITEMENT  M E N S U E L'                           
           END-IF.                                                              
           DISPLAY '*'.                                                         
           DISPLAY '*  PARAMETRE EN ENTREE = ' WRK-ETAT                         
                                           ' ' WRK-DATED                        
                                           ' ' WRK-DATEF                        
           DISPLAY '*'.                                                         
           MOVE WRK-CLEFIC           TO FJEF21E-CLE.                            
      *--- SI CODE RETOUR=23 ----> PAS D'ENREGISTREMENT TROUVE                  
           START FJEF21E KEY IS      >  FJEF21E-CLE                             
                 INVALID KEY                                                    
                 SET WRK-FINTRT      TO TRUE.                                   
           IF  NOT WRK-FINTRT                                                   
               PERFORM  MODULE-READN-FJEF21E                                    
           END-IF.                                                              
       MODULE-ENTREE-F. EXIT.                                                   
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *    CREATION DU FICHIER FJEF21S                                 *        
      *----------------------------------------------------------------*        
      *                                                                 00002950
       MODULE-INITIAL  SECTION.                                         00002330
           ADD 1                     TO WRK-NBRLUS.                             
           IF  WRK-ETAT              =  FJEF21E-ETAT  AND                       
              (FJEF21E-DATE      NOT <  WRK-DATED AND                           
               FJEF21E-DATE      NOT >  WRK-DATEF)                              
               MOVE FJEF21E-ENREG    TO F021-ENREG                              
               PERFORM  MODULE-REMPL-FJEF21S                                    
           END-IF.                                                              
           IF FJEF21E-ETAT           >  WRK-ETAT                                
              SET WRK-FINTRT         TO TRUE                                    
           ELSE                                                                 
               PERFORM MODULE-READN-FJEF21E                                     
           END-IF.                                                              
       MODULE-INITIAL-F. EXIT.                                          00002620
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *      FERMETURE DU FICHIER                                      *        
      *----------------------------------------------------------------*        
       MODULE-SORTIE  SECTION.                                          00002330
           CLOSE  FDATE  FJEF21E  FJEF21S.                                      
           IF  ST-FDATE              = '00' AND                                 
               ST-FJEF21E            = '00' AND                                 
               ST-FJEF21S            = '00'                                     
               DISPLAY '* BEF021 : CREATION FJEF21S O.K ***'                    
           ELSE                                                                 
               DISPLAY '* BEF021 : PROBLEME FERMETURE FICHIERS '                
               DISPLAY '* ------   FICHIER FDATE  = ' ST-FDATE                  
               DISPLAY '*          FICHIER FJEF21E = ' ST-FJEF21E               
               DISPLAY '*          FICHIER FJEF21S = ' ST-FJEF21S               
           END-IF.                                                              
           DISPLAY '*'.                                                         
           DISPLAY '*          EXTRACTION SUR L'' ETAT = ' WRK-ETAT.            
           DISPLAY '*          DATE DEBUT             = ' WRK-DATED.            
           DISPLAY '*          DATE FIN               = ' WRK-DATEF.            
           DISPLAY '*'.                                                         
           DISPLAY '*          NBR ENREG. LUS FJEF21E = ' WRK-NBRLUS.           
           DISPLAY '*          NBR ENREG. ECR FJEF21S = ' WRK-JEFECR.           
       MODULE-SORTIE-F. EXIT.                                           00002620
      *                                                                         
      *                                                                         
       MODULE-READN-FJEF21E SECTION.                                            
           READ FJEF21E NEXT.                                                   
           IF  ST-FJEF21E        NOT =  '00'                                    
                                 AND    '10'                                    
               DISPLAY '* BEF021 : PB LECTURE FJEF21E = ' ST-FJEF21E            
               CLOSE FDATE FJEF21E FJEF21S                                      
               GOBACK                                                           
           END-IF.                                                              
           IF  ST-FJEF21E            = '10'                                     
               SET WRK-FINTRT        TO TRUE                                    
           END-IF.                                                              
       MODULE-READN-FJEF21E-F. EXIT.                                            
      *                                                                         
      *                                                                         
       MODULE-REMPL-FJEF21S  SECTION.                                           
           MOVE WRK-ETAT             TO F021-CSV-NOMETAT.                       
           MOVE WRK-DATED            TO F021-CSV-DATED.                         
           MOVE WRK-DATEF            TO F021-CSV-DATEF.                         
           MOVE F021-NRENDU          TO F021-CSV-NRENDU.                        
           MOVE F021-DRENDU          TO F021-CSV-DRENDU.                        
           MOVE F021-MRENDU          TO F021-CSV-MRENDU.                        
           MOVE F021-LTIERS          TO F021-CSV-LTIERS.                        
           MOVE F021-CTIERS          TO F021-CSV-CTIERS.                        
           MOVE F021-CODE            TO F021-CSV-CODE.                          
           MOVE F021-QTRDU           TO F021-CSV-QTRDU.                         
           MOVE F021-NCODIC          TO F021-CSV-NCODIC.                        
           MOVE F021-CFAM            TO F021-CSV-CFAM.                          
           MOVE F021-CMARQ           TO F021-CSV-CMARQ.                         
           MOVE F021-LREF            TO F021-CSV-LREF.                          
           MOVE F021-NLIEUHS         TO F021-CSV-NLIEUHS.                       
           MOVE F021-NHS             TO F021-CSV-NHS.                           
           MOVE F021-NENVOI          TO F021-CSV-NENVOI.                        
           MOVE F021-DENVOI          TO F021-CSV-DENVOI.                        
           MOVE F021-QTENV           TO F021-CSV-QTENV.                         
           MOVE F021-PRMP-R          TO F021-CSV-PRMP-R.                        
           MOVE F021-PVTTC-R         TO F021-CSV-PVTTC-R.                       
221107     IF  F021-ECOPA-R          =  SPACES  OR                              
  "                                     LOW-VALUES                              
  "            MOVE 0                TO F021-CSV-ECOPA                          
  "        ELSE                                                                 
  "            MOVE F021-ECOPA-R     TO F021-CSV-ECOPA-R                        
221107     END-IF.                                                              
           WRITE FJEF21S-ENREG  FROM F021-CSV-ENREG.                            
           IF  ST-FJEF21S        NOT =  '00'                                    
               DISPLAY '* BJHE61 : PB ECRITURE FJEF21S = ' ST-FJEF21S           
               CLOSE FDATE  FJEF21E FJEF21S                                     
               GOBACK                                                           
           END-IF.                                                              
           ADD 1                     TO WRK-JEFECR.                             
       MODULE-REMPL-FJEF21S-F. EXIT.                                            
      *                                                                         
      *                                                                         
       MODULE-MEF-DATES  SECTION.                                               
           IF  WRK-DAT-MACMOIS-R     =  2                                       
               IF  WRK-DAT-MACAA-R   =  ZEROS                                   
                   DIVIDE WRK-DAT-MACSSAA-R BY 4  GIVING WRK-RESTES             
                                               REMAINDER WRK-RESTES             
               ELSE                                                             
                   DIVIDE WRK-DAT-MACAA-R BY 4    GIVING WRK-RESTES             
                                               REMAINDER WRK-RESTES             
               END-IF                                                           
               IF  WRK-RESTES        =  ZEROS                                   
                   MOVE 29           TO TB-NB-JOUR (WRK-DAT-MACMOIS-R)          
               ELSE                                                             
                   MOVE 28           TO TB-NB-JOUR (WRK-DAT-MACMOIS-R)          
               END-IF                                                           
           END-IF.                                                              
       MODULE-MEF-DATES-F. EXIT.                                                
      *                                                                 00002950
      *********** F I N  D E  'B E F 0 2 1' ****************************00002950
