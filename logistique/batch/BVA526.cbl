      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                  BVA526.                                     
       AUTHOR.    GILLES E.                                                     
      *-----------------------------------------------------------------        
      * EDITION DE L'ETAT IVA540                                                
      *-----------------------------------------------------------------        
      * MODIFICATION LE 30 05 2001 PAR DSA022 NT                                
      * AJOUT DE L'EURO POUR IVA540 PRODUITS DARTY (IVA540DP)                   
      *-----------------------------------------------------------------        
       ENVIRONMENT DIVISION.                                                    
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                                   
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                           
      *    DECIMAL-POINT IS COMMA.                                              
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FVA540I ASSIGN TO FVA540I.                                    
      *--                                                                       
           SELECT FVA540I ASSIGN TO FVA540I                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FVA540O ASSIGN TO FVA540O.                                    
      *--                                                                       
           SELECT FVA540O ASSIGN TO FVA540O                                     
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *---------------------------------------------------------------*         
       FD  FVA540I                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FVA540I-ENR.                                                         
           02 NOMETAT       PIC X(6).                                           
           02 NSOCVALO      PIC X(3).                                           
           02 CSEQRAYON     PIC X(2).                                           
           02 NLIEUVALO     PIC X(3).                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SEQ           PIC S9(4) COMP.                                     
      *--                                                                       
           02 SEQ           PIC S9(4) COMP-5.                                   
      *}                                                                        
           02 CRAYON1       PIC X(5).                                           
NT         02 DEVISE        PIC X(3).                                           
           02 LLIEU         PIC X(20).                                          
           02 P1            PIC S9(9)V9(6) COMP-3.                              
           02 P2            PIC S9(9)V9(6) COMP-3.                              
           02 P3            PIC S9(9)V9(6) COMP-3.                              
           02 P4            PIC S9(9)V9(6) COMP-3.                              
           02 Q1            PIC S9(11)     COMP-3.                              
           02 Q2            PIC S9(11)     COMP-3.                              
           02 Q3            PIC S9(11)     COMP-3.                              
           02 Q4            PIC S9(11)     COMP-3.                              
NT         02 TXEURO        PIC S9(01)V9(05) COMP-3.                            
NT         02 FILLER        PIC X(408).                                         
      *    02 FILLER        PIC X(415).                                         
       FD  FVA540O                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FVA540O-ENR  PIC X(512).                                             
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY ABENDCOP.                                                       
       01  FILLER.                                                              
           02 ETAT-FVA540I           PIC  9  VALUE 0.                           
              88 EOF                         VALUE 1.                           
           COPY IVA540.                                                         
       PROCEDURE DIVISION.                                                      
      *---------------------------------------------------------------*         
       MODULE-BVA526.                                                           
           DISPLAY 'DEBUT BVA526'.                                              
           OPEN INPUT FVA540I OUTPUT FVA540O.                                   
           PERFORM LECTURE-FVA540I.                                             
           PERFORM     TRT       UNTIL EOF.                                     
           CLOSE FVA540I FVA540O.                                               
           DISPLAY 'FIN BVA526'.                                                
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *---------------------------------------------------------------*         
       TRT.                                                                     
           MOVE  NOMETAT   TO          NOMETAT-IVA540                           
           MOVE  NSOCVALO  TO          IVA540-NSOCVALO                          
      *    MOVE  CSEQRAYON TO          IVA540-CSEQRAYON                         
           MOVE  NLIEUVALO TO          IVA540-NLIEUVALO                         
           MOVE  SEQ       TO          IVA540-SEQUENCE                          
           MOVE  CRAYON1   TO          IVA540-CRAYON1                           
           MOVE  CRAYON1   TO          IVA540-TLMELA                            
           MOVE  CRAYON1(4:2) TO       IVA540-BL-BR                             
           MOVE  LLIEU     TO          IVA540-LLIEU                             
           MOVE  P1        TO          IVA540-PSTOCKENTREE                      
           MOVE  P2        TO          IVA540-PSTOCKFINAL                       
           MOVE  P3        TO          IVA540-PSTOCKINIT                        
           MOVE  P4        TO          IVA540-PSTOCKSORTIE                      
           MOVE  Q1        TO          IVA540-QSTOCKENTREE                      
           MOVE  Q2        TO          IVA540-QSTOCKFINAL                       
           MOVE  Q3        TO          IVA540-QSTOCKINIT                        
           MOVE  Q4        TO          IVA540-QSTOCKSORTIE                      
NT         MOVE  DEVISE (1:1) TO       IVA540-LIB1 IVA540-LIB2                  
NT                                     IVA540-LIB4                              
NT                                     IVA540-LIB11 IVA540-LIB21                
NT                                     IVA540-LIB41                             
NT         MOVE  DEVISE    TO          IVA540-DEVISE                            
NT         MOVE  TXEURO    TO          IVA540-TXEURO                            
           WRITE FVA540O-ENR FROM DSECT-IVA540.                                 
           PERFORM LECTURE-FVA540I.                                             
      *---------------------------------------------------------------*         
       LECTURE-FVA540I.                                                         
           READ FVA540I AT END SET EOF TO TRUE.                                 
      *    READ FVA540I INTO DSECT-IVA540 AT END SET EOF TO TRUE.               
      *---------------------------------------------------------------*         
       PLANTAGE.                                                                
           CLOSE FVA540I FVA540O.                                               
           MOVE 'BVA526' TO ABEND-PROG                                          
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                            
