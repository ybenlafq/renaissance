      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BGS871                                                    
       AUTHOR. DSA079.                                                          
      *------------------------------------------------------------*            
      *                PROGRAMME D'EDITION DE L'ETAT               *            
      *                   ANALYSE DES STOCKS DEPOT                 *            
      *                                                            *            
      * DATE CREATION : 21/12/01                                   *            
      *------------------------------------------------------------*            
PM0205* MODIFICATION : - VENTILER POUR LES DEPOTS ET LES FAMILLES               
      *                PARAMETRES DANS LA SS-TABLE RVPARME LES DONNEES          
      *                DE L'ETAT EN FONCTION DU MODE DE STOCKAGE                
      *                - AFFICHER LE MODE DE STOCKAGE EN QUESTION               
      *       AUTEUR : DSA054 - PH MAILLARD                                     
      *------------------------------------------------------------*            
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGS870  ASSIGN TO FGS870.                                     
      *--                                                                       
           SELECT FGS870  ASSIGN TO FGS870                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IGS870 ASSIGN TO IGS870.                                      
      *--                                                                       
           SELECT IGS870 ASSIGN TO IGS870                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IGS871 ASSIGN TO IGS871.                                      
      *--                                                                       
           SELECT IGS871 ASSIGN TO IGS871                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F D A T E            *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FDATE-ENREG.                                                 00000030
           02 FDATE-JJ              PIC X(02).                                  
           02 FDATE-MM              PIC X(02).                                  
           02 FDATE-SS              PIC X(02).                                  
           02 FDATE-AA              PIC X(02).                                  
           02 FILLER                PIC X(72).                                  
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    F G P 8 8 9          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  FGS870                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
PM0205*01  ENREG-FGS870 PIC X(300).                                             
       01  ENREG-FGS870 PIC X(305).                                             
      *                                                                         
      *---------------------------------------------------------------*         
      *  D E F I N I T I O N    F I C H I E R    I G P 8 7 0          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FD  IGS870                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-IGS870 PIC X(133).                                             
      *    02 SAUT       PIC X.                                                 
      *    02 LIGNE      PIC X(132).                                            
           EJECT                                                                
      *                                                                         
       FD  IGS871                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  LIGNE-IGS871 PIC X(133).                                             
      *    02 SAUT       PIC X.                                                 
      *    02 LIGNE      PIC X(132).                                            
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
       77  W-ZONE    PIC XX.                                                    
      *                                                                         
        COPY ABENDCOP.                                                          
      *                                                                         
        COPY WORKDATC.                                                          
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    Z O N E S         *         
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
       01  LIGNE-ETAT.                                                          
           02 SAUT       PIC X.                                                 
           02 LIGNE      PIC X(132).                                            
       01  W-DATE.                                                              
           03  DATJMSA.                                                         
               05 JJ                  PIC  XX.                                  
               05 MM                  PIC  XX.                                  
               05 SS                  PIC  XX.                                  
               05 AA                  PIC  XX.                                  
           03  W-TIME.                                                          
               05 HH                  PIC  XX.                                  
               05 MN                  PIC  XX.                                  
               05 FILLER              PIC  X(14).                               
      *                                                                         
       01  RUPTURE        PIC 99 VALUE 15.                                      
      *                                                                         
       01  CTR-FGS870  PIC 9(9) VALUE 0.                                        
      *                                                                         
       01  FLAG-FICHIER PIC X VALUE SPACE.                                      
           88  EOF      VALUE 'F'.                                              
       01  FLAG-IMPRI   PIC X VALUE SPACE.                                      
           88  SURIMPRESSION     VALUE '+'.                                     
           88  IMPRESSION-NORMAL VALUE ' '.                                     
       01  FLAG-APPRO   PIC X VALUE SPACE.                                      
           88  APPRO-SENSI       VALUE 'V'.                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  STOCK-VEND PIC S9(8) COMP.                                           
      *--                                                                       
       01  STOCK-VEND PIC S9(8) COMP-5.                                         
      *}                                                                        
       01  W-VOLUME   PIC S9(10)V9 COMP-3.                                      
       01  TOT-RAYON.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-APP-TOT-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  R-APP-TOT-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-APP-STK-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  R-APP-STK-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-CQE-TOT-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  R-CQE-TOT-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-CQE-STK-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  R-CQE-STK-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-EPU-STK-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  R-EPU-STK-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKDIS        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKDIS        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKRES        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKRES        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKTOT        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKTOT        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKEPU        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKEPU        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKC          PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKC          PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKTOTDEP     PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKTOTDEP     PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKTOTDEPMAG  PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKTOTDEPMAG  PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKMAG        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKMAG        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKMAGHS      PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKMAGHS      PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKMAGEPF     PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKMAGEPF     PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKCDE        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKCDE        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKGENGRO     PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKGENGRO     PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKPTF        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKPTF        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKP          PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKP          PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-STKHS         PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  R-STKHS         PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-VOLUME        PIC S9(15) COMP.                                 
      *--                                                                       
           05  R-VOLUME        PIC S9(15) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  R-PALETTE       PIC S9(05) COMP.                                 
      *--                                                                       
           05  R-PALETTE       PIC S9(05) COMP-5.                               
      *}                                                                        
       01  TOT-DEPOT.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-APP-TOT-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  D-APP-TOT-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-APP-STK-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  D-APP-STK-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-CQE-TOT-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  D-CQE-TOT-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-CQE-STK-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  D-CQE-STK-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-EPU-STK-REF   PIC S9(05) COMP.                                 
      *--                                                                       
           05  D-EPU-STK-REF   PIC S9(05) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKDIS        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKDIS        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKRES        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKRES        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKTOT        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKTOT        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKEPU        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKEPU        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKC          PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKC          PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKTOTDEP     PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKTOTDEP     PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKMAG        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKMAG        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKMAGHS      PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKMAGHS      PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKMAGEPF     PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKMAGEPF     PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKTOTDEPMAG  PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKTOTDEPMAG  PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKCDE        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKCDE        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKGENGRO     PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKGENGRO     PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKPTF        PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKPTF        PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKP          PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKP          PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-STKHS         PIC S9(11)V9(06) COMP.                           
      *--                                                                       
           05  D-STKHS         PIC S9(11)V9(06) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-VOLUME        PIC S9(15) COMP.                                 
      *--                                                                       
           05  D-VOLUME        PIC S9(15) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  D-PALETTE       PIC S9(05) COMP.                                 
      *--                                                                       
           05  D-PALETTE       PIC S9(05) COMP-5.                               
      *}                                                                        
      *                                                                         
      *ENREGISTREMENT COURANT                                                   
       01 FGS870-ENTREPOT PIC X(06).                                            
       COPY FGS870 REPLACING ==:FGPXXX:== BY ==FGS870==.                        
      *                                                                         
      *ENREGISTREMENT PRECEDENT                                                 
       COPY FGS870 REPLACING ==:FGPXXX:== BY ==W-FGS870==.                      
      *                                                                         
       01  CTR-PAGE     PIC 9(4) VALUE 0.                                       
      *                                                                         
      *MASQUE D EDITION                                                         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-MAX         PIC S9(11) COMP.                                       
      *--                                                                       
       01  W-MAX         PIC S9(11) COMP-5.                                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-NBRE        PIC S9(11) COMP.                                       
      *--                                                                       
       01  W-NBRE        PIC S9(11) COMP-5.                                     
      *}                                                                        
       01  W-CODE.                                                              
           02 W-LCODE    PIC X.                                                 
           02 W-SIGNE    PIC X.                                                 
      *                                                                         
       01  W-NUMED2        PIC Z9.                                              
       01  W-NUMED3        PIC ZZ9.                                             
       01  W-NUMED4        PIC ZZZ9.                                            
       01  W-NUMED5        PIC ZZZZ9.                                           
       01  W-NUMED5S       PIC -ZZZ9.                                           
       01  W-NUMED6        PIC ZZZZZ9.                                          
       01  W-NUMED6S       PIC -ZZZZ9.                                          
       01  W-NUMED7        PIC ZZZZZZ9.                                         
       01  W-NUMED7S       PIC -ZZZZZ9.                                         
       01  W-NUMED8        PIC ZZZZZZZ9.                                        
       01  W-NUMED8S       PIC -ZZZZZZ9.                                        
       01  W-NUMED9        PIC ZZZZZZZZ9.                                       
       01  W-NUMED9S       PIC -ZZZZZZZ9.                                       
      *                                                                         
       01  EDITX.                                                               
           05 EDIT3.                                                            
              10 W-EDIT3     PIC XX.                                            
              10 W-MILLIERS3 PIC X.                                             
           05 EDIT4.                                                            
              10 W-EDIT4     PIC XXX.                                           
              10 W-MILLIERS4 PIC X.                                             
           05 EDIT5.                                                            
              10 W-EDIT5     PIC XXXX.                                          
              10 W-MILLIERS5 PIC X.                                             
           05 EDIT6.                                                            
              10 W-EDIT6     PIC XXXXX.                                         
              10 W-MILLIERS6 PIC X.                                             
           05 EDIT7.                                                            
              10 W-EDIT7     PIC XXXXXX.                                        
              10 W-MILLIERS7 PIC X.                                             
           05 EDIT8.                                                            
              10 W-EDIT8     PIC XXXXXXX.                                       
              10 W-MILLIERS8 PIC X.                                             
           05 EDIT9.                                                            
              10 W-EDIT9     PIC XXXXXXXX.                                      
              10 W-MILLIERS9 PIC X.                                             
      *                                                                         
      *                                                                         
      * VARIABLE PERMETTANT UNE EDITION DES NOMBRES EN MILLIERS                 
      * PAR EXEMPLE  104000 --> 104K                                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  QUOTIENT   PIC S9(9) COMP.                                           
      *--                                                                       
       01  QUOTIENT   PIC S9(9) COMP-5.                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  RESTE      PIC 9(6) COMP.                                            
      *--                                                                       
       01  RESTE      PIC 9(6) COMP-5.                                          
      *}                                                                        
       01  CTR-LIGNE    PIC 99   VALUE 0.                                       
       01  MAX-LIGNE    PIC 99   VALUE 60.                                      
           EJECT                                                                
      *                                                                         
      *                                                                         
       01  IGS870-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  IGS870-E00.                                                      
               10  IGS870-E00-CETAT               PIC X(06).                    
               10  FILLER                         PIC X(52) VALUE               
           '                                          ANALYSE DE'.              
               10  FILLER                         PIC X(47) VALUE               
           'S STOCKS PAR FAMILLE                  EDITE LE '.                   
               10  IGS870-E00-DATEDITE.                                         
                   15 JJ                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 MM                          PIC XX.                       
                   15 FILLER                      PIC X VALUE '/'.              
                   15 SS                          PIC XX.                       
                   15 AA                          PIC XX.                       
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  IGS870-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  IGS870-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  IGS870-E00-NOPAGE              PIC ZZZ.                      
      *--------------------------- LIGNE E05  --------------------------        
           05  IGS870-E05.                                                      
               10  FILLER                         PIC X(11) VALUE               
           'ENTREPOT : '.                                                       
               10  IGS870-E05-NSOCIETE            PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-E05-NDEPOT              PIC X(03).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IGS870-E05-LLIEU               PIC X(20).                    
               10  FILLER                         PIC X(19) VALUE               
           '                EN '.                                               
               10  IGS870-E05-TITRE               PIC X(08).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(06) VALUE               
           '      '.                                                            
      *--------------------------- LIGNE E10  --------------------------        
           05  IGS870-E10.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE E40  --------------------------        
           05  IGS870-E40.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------- '.                                                  
      *--------------------------- LIGNE E45  --------------------------        
           05  IGS870-E45.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'FAMILLE !  NB CODIC PAR STATUT  !         STOCK VENDABLE  '.        
               10  FILLER                         PIC X(58) VALUE               
           '         ! STK  NON !   STOCK   !  STOCK !   STOCK MAGASIN'.        
               10  FILLER                         PIC X(16) VALUE               
           '  !    STOCK    '.                                                  
      *--------------------------- LIGNE E50  --------------------------        
           05  IGS870-E50.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '        !                       ! DISPO   DT    RES  MISDE'.        
               10  FILLER                         PIC X(58) VALUE               
           '  TOTAL  ! VENDABLE !  AFFILIE  !  TOTAL ! TOTAL   DT    D'.        
               10  FILLER                         PIC X(16) VALUE               
           'T !   MAGASIN   '.                                                  
      *--------------------------- LIGNE E52  --------------------------        
           05  IGS870-E52.                                                      
               10  FILLER                         PIC X(58) VALUE               
           ' RAYON  !  APP     CQE & Z   EPU!        EPUIS        COTE'.        
               10  FILLER                         PIC X(58) VALUE               
           '         ! AUTRE HS ! DEPO  PTF !  DEPOT !  MAG   EPUIS  H'.        
               10  FILLER                         PIC X(16) VALUE               
           'S !   + DEPOT   '.                                                  
      *--------------------------- LIGNE E55  --------------------------        
           05  IGS870-E55.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------- '.                                                  
      *--------------------------- LIGNE D05  --------------------------        
           05  IGS870-D05.                                                      
               10  IGS870-D05-CFAM                PIC X(05).                    
PM0205         10  FILLER                         PIC X(01) VALUE               
  "        ' '.                                                                 
  "            10  IGS870-D05-CMODSTOCK           PIC X(02).                    
  "            10  FILLER                         PIC X(01) VALUE               
PM0205     ' '.                                                                 
               10  IGS870-D05-APP-STK-REF         PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           '/'.                                                                 
               10  IGS870-D05-APP-TOT-REF         PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-D05-CQE-STK-REF         PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           '/'.                                                                 
               10  IGS870-D05-CQE-TOT-REF         PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-D05-EPU-STK-REF         PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKDISPO          PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKEPU            PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKRES            PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKC              PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKVD             PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKPRET           PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKHS             PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-D05-STOCKGENGRO         PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKPTF            PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKTOTDEP         PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKMAG            PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKMAGEPF         PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-D05-STOCKMAGHS          PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-D05-STOCKDEPMAG         PIC X(09).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
      *--------------------------- LIGNE D10  --------------------------        
           05  IGS870-D10.                                                      
               10  IGS870-D10-CFAM                PIC X(05).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(11) VALUE               
           '           '.                                                       
      *--------------------------- LIGNE T05  --------------------------        
           05  IGS870-T05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '         --------  --------  ---  ------ ----- -----  ----'.        
               10  FILLER                         PIC X(58) VALUE               
           ' --------  ---  ---  ----- ----- -------- ------- ----- --'.        
               10  FILLER                         PIC X(16) VALUE               
           '--  ---------   '.                                                  
      *--------------------------- LIGNE T10  --------------------------        
           05  IGS870-T10.                                                      
               10  FILLER                         PIC X(01) VALUE               
           '*'.                                                                 
               10  IGS870-T10-RAYON               PIC X(07).                    
               10  IGS870-T10-RAPP-STK-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           '/'.                                                                 
               10  IGS870-T10-RAPP-TOT-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RCQE-STK-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           '/'.                                                                 
               10  IGS870-T10-RCQE-TOT-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-REPU-STK-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKDISPO         PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKEPU           PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKRES           PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKC             PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKVD            PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKPRET          PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKHS            PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-T10-RSTOCKGENGRO        PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKPTF           PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKTOTDEP        PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKMAG           PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKMAGEPF        PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T10-RSTOCKMAGHS         PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-T10-RSTOCKDEPMAG        PIC X(09).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
      *--------------------------- LIGNE T15  --------------------------        
           05  IGS870-T15.                                                      
               10  FILLER                         PIC X(08) VALUE               
           'DEPOT   '.                                                          
               10  IGS870-T15-DAPP-STK-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           '/'.                                                                 
               10  IGS870-T15-DAPP-TOT-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DCQE-STK-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           '/'.                                                                 
               10  IGS870-T15-DCQE-TOT-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DEPU-STK-REF        PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKDISPO         PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKEPU           PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKRES           PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKC             PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKVD            PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKPRET          PIC X(04).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKHS            PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-T15-DSTOCKGENGRO        PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKPTF           PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKTOTDEP        PIC X(08).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKMAG           PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKMAGEPF        PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IGS870-T15-DSTOCKMAGHS         PIC X(04).                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IGS870-T15-DSTOCKMAGDEP        PIC X(09).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
      *--------------------------- LIGNE B05  --------------------------        
           05  IGS870-B05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '--------------- '.                                                  
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *           D E S C R I P T I O N    D E S    T A B L E S       *         
      *---------------------------------------------------------------*         
      *                                                                         
      *                                                                         
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           ------------------------------------------          *         
      *           --  MODULE DE BASE DU PROGRAMME  BGS871 --          *         
      *           ------------------------------------------          *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BGS871 SECTION.                                                   
      *                                                                         
           PERFORM  DEBUT-BGS871.                                               
           PERFORM  TRAIT-BGS871.                                               
           PERFORM  FIN-BGS871.                                                 
      *                                                                         
       F-MODULE-BGS871. EXIT.                                                   
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    D E B U T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       DEBUT-BGS871 SECTION.                                                    
      *                                                                         
           DISPLAY '*** DEBUT BGS871 ***'.                                      
           PERFORM OUVERTURE-FICHIERS.                                          
           PERFORM TRAIT-DATE.                                                  
      *                                                                         
       F-DEBUT-BGS871.                                                          
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *                    T R A I T        B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       TRAIT-BGS871 SECTION.                                                    
      *                                                                         
           INITIALIZE TOT-RAYON TOT-DEPOT.                                      
           PERFORM LECTURE-FGS870 .                                             
EOF        PERFORM UNTIL EOF                                                    
             PERFORM RUPTURE-ENTREPOT                                           
             PERFORM ECRITURE-ENTETE                                            
DEPOT        PERFORM UNTIL                                                      
                FGS870-NSOCDEPOT NOT = W-FGS870-NSOCDEPOT                       
             OR FGS870-NDEPOT    NOT = W-FGS870-NDEPOT                          
             OR EOF                                                             
               PERFORM RUPTURE-RAYON                                            
RAYON          PERFORM UNTIL                                                    
                  FGS870-NSOCDEPOT NOT = W-FGS870-NSOCDEPOT                     
               OR FGS870-NDEPOT    NOT = W-FGS870-NDEPOT                        
               OR FGS870-RAYON     NOT = W-FGS870-RAYON                         
               OR EOF                                                           
                 PERFORM RUPTURE-FAMILLE                                        
FAM              PERFORM UNTIL                                                  
                    FGS870-NSOCDEPOT NOT = W-FGS870-NSOCDEPOT                   
                 OR FGS870-NDEPOT    NOT = W-FGS870-NDEPOT                      
                 OR FGS870-WSEQFAM   NOT = W-FGS870-WSEQFAM                     
                 OR EOF                                                         
                   PERFORM CUMUL-RAYON                                          
                   PERFORM ECRITURE-DETAIL                                      
                   PERFORM LECTURE-FGS870                                       
FAM              END-PERFORM                                                    
RAYON          END-PERFORM                                                      
               PERFORM LIGNE-RAYON                                              
DEPOT        END-PERFORM                                                        
             PERFORM LIGNE-DEPOT                                                
EOF        END-PERFORM.                                                         
       FIN-TRAIT-BGS871. EXIT.                                                  
      *                                                                         
       FIN-BGS871 SECTION.                                                      
      *                                                                         
           CLOSE FDATE IGS870 IGS871 FGS870.                                    
           DISPLAY '**********************************************'.            
           DISPLAY '*    PROGRAMME BGS871  TERMINE NORMALEMENT   *'.            
           DISPLAY '*                                            *'.            
           DISPLAY '*  NOMBRE D''ENREG TRAITES SUR FGS870 = ' CTR-FGS870        
                   ' *'.                                                        
           DISPLAY '*                                            *'.            
           DISPLAY '**********************************************'.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-FIN-BGS871. EXIT.                                                    
           EJECT                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    D E B U T  B G P 8 9 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       OUVERTURE-FICHIERS SECTION.                                              
      *-------------------                                                      
      *                                                                         
           OPEN INPUT FDATE FGS870.                                             
           OPEN OUTPUT IGS870.                                                  
           OPEN OUTPUT IGS871.                                                  
       FIN-OUVERTURE-FICHIERS. EXIT.                                            
      *                                                                         
       TRAIT-DATE SECTION.                                                      
      *-----------                                                              
           READ FDATE                                                           
           AT END MOVE 'FICHIER FDATE VIDE ' TO ABEND-MESS                      
                  PERFORM FIN-ANORMALE.                                         
           MOVE '1'                 TO GFDATA.                                  
           MOVE FDATE-ENREG         TO GFJJMMSSAA.                              
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              DISPLAY 'DATE DE TRAITEMENT INVALIDE '                            
              MOVE '** DATE DE TRAITEMENT INVALIDE **' TO ABEND-MESS            
              PERFORM FIN-ANORMALE                                              
           ELSE                                                                 
              DISPLAY '** FDATE  ** ' GFJMSA-5                                  
           END-IF.                                                              
           COMPUTE GFQNT0 = GFQNT0 + 1 .                                        
           MOVE '3' TO GFDATA.                                                  
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE 'DATE CARTE PARAMETRE ERRONEE ' TO ABEND-MESS                
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE GFJJMMSSAA       TO DATJMSA.                                    
           DISPLAY 'DATE RECEPTION ETAT DANS BOITE EOS : ' DATJMSA.             
           ACCEPT W-TIME FROM TIME.                                             
       FIN-TRAIT-DATE. EXIT.                                                    
      *                                                                         
      *---------------------------------------------------------------*         
      *         M O D U L E S    T R A I T  B G S 8 7 0               *         
      *---------------------------------------------------------------*         
      *                                                                         
       LECTURE-FGS870 SECTION.                                                  
      *----------------                                                         
           MOVE FGS870-ENREG TO W-FGS870-ENREG.                                 
           INITIALIZE FGS870-ENREG.                                             
           READ FGS870                                                          
                INTO FGS870-ENREG                                               
                AT END SET EOF TO TRUE                                          
           END-READ                                                             
           IF NOT EOF                                                           
              IF CTR-FGS870 = 0                                                 
                 MOVE FGS870-ENREG TO W-FGS870-ENREG                            
              END-IF                                                            
              ADD 1 TO CTR-FGS870                                               
              PERFORM NUMERO-RUPTURE                                            
           END-IF.                                                              
       FIN-LECTURE-FGS870. EXIT.                                                
      *---------------- RUPTURE DE L ETAT ----------------------------          
       NUMERO-RUPTURE SECTION.                                                  
      *-----------------                                                        
           IF  FGS870-NSOCDEPOT = W-FGS870-NSOCDEPOT                            
           AND FGS870-NDEPOT   = W-FGS870-NDEPOT                                
               MOVE 3  TO RUPTURE                                               
               IF FGS870-RAYON    = W-FGS870-RAYON                              
                  MOVE 2  TO RUPTURE                                            
                  IF FGS870-WSEQFAM  = W-FGS870-WSEQFAM                         
                     MOVE 1  TO RUPTURE                                         
                  END-IF                                                        
               END-IF                                                           
           ELSE                                                                 
            MOVE 4  TO RUPTURE                                                  
           END-IF.                                                              
           IF CTR-FGS870 = 1 MOVE 4  TO RUPTURE.                                
       FIN-NUMERO-RUPTURE. EXIT.                                                
      *                                                                         
      *                                                                         
       RUPTURE-ENTREPOT SECTION.                                                
           INITIALIZE CTR-PAGE.                                                 
           MOVE FGS870-NSOCDEPOT TO W-FGS870-NSOCDEPOT.                         
           MOVE FGS870-NDEPOT    TO W-FGS870-NDEPOT   .                         
       FIN-RUPTURE-ENTREPOT. EXIT.                                              
      *                                                                         
       RUPTURE-RAYON    SECTION.                                                
           MOVE FGS870-NSOCDEPOT TO W-FGS870-NSOCDEPOT.                         
           MOVE FGS870-NDEPOT    TO W-FGS870-NDEPOT   .                         
           MOVE FGS870-RAYON     TO W-FGS870-RAYON    .                         
       FIN-RUPTURE-RAYON. EXIT.                                                 
      *                                                                         
       RUPTURE-FAMILLE  SECTION.                                                
           MOVE FGS870-NSOCDEPOT TO W-FGS870-NSOCDEPOT.                         
           MOVE FGS870-NDEPOT    TO W-FGS870-NDEPOT   .                         
           MOVE FGS870-WSEQFAM   TO W-FGS870-WSEQFAM.                           
       FIN-RUPTURE-FAMILLE. EXIT.                                               
      *                                                                         
       ECRITURE-ENTETE SECTION.                                                 
      *-----------------                                                        
           ADD 1              TO CTR-PAGE.                                      
           MOVE 0             TO CTR-LIGNE.                                     
           MOVE CORRESPONDING DATJMSA TO IGS870-E00-DATEDITE.                   
           MOVE FGS870-CETAT          TO IGS870-E00-CETAT.                      
           MOVE HH OF W-TIME          TO IGS870-E00-HEUREDITE.                  
           MOVE MN OF W-TIME          TO IGS870-E00-MINUEDITE.                  
           MOVE CTR-PAGE              TO IGS870-E00-NOPAGE.                     
           MOVE '1'                   TO SAUT.                                  
           MOVE IGS870-E00            TO LIGNE.                                 
           PERFORM ECRITURE-FICHIER.                                            
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE FGS870-NSOCDEPOT   TO IGS870-E05-NSOCIETE.                      
           MOVE FGS870-NDEPOT      TO IGS870-E05-NDEPOT.                        
           MOVE FGS870-LLIEU       TO IGS870-E05-LLIEU .                        
           EVALUATE FGS870-CETAT                                                
             WHEN 'IGS870' MOVE 'QUANTITE' TO IGS870-E05-TITRE                  
             WHEN 'IGS871' MOVE 'VALEUR  ' TO IGS870-E05-TITRE                  
             WHEN OTHER CONTINUE                                                
           END-EVALUATE.                                                        
           MOVE ' '                TO SAUT.                                     
           MOVE IGS870-E05         TO LIGNE.                                    
           PERFORM ECRITURE-FICHIER.                                            
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGS870-E10         TO LIGNE.                                    
           PERFORM ECRITURE-FICHIER.                                            
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE ' '               TO SAUT.                                      
           MOVE IGS870-E40        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E45        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E50        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E52        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E55        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
       FIN-ECRITURE-ENTETE. EXIT.                                               
      *                                                                         
       ECRITURE-ENTETE-PRE SECTION.                                             
      *-----------------                                                        
           ADD 1              TO CTR-PAGE.                                      
           MOVE 0             TO CTR-LIGNE.                                     
           MOVE CORRESPONDING DATJMSA TO IGS870-E00-DATEDITE.                   
           MOVE W-FGS870-CETAT        TO IGS870-E00-CETAT.                      
           MOVE HH OF W-TIME          TO IGS870-E00-HEUREDITE.                  
           MOVE MN OF W-TIME          TO IGS870-E00-MINUEDITE.                  
           MOVE CTR-PAGE              TO IGS870-E00-NOPAGE.                     
           MOVE '1'                   TO SAUT.                                  
           MOVE IGS870-E00            TO LIGNE.                                 
           PERFORM ECRITURE-FICHIER.                                            
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE W-FGS870-NSOCDEPOT   TO IGS870-E05-NSOCIETE.                    
           MOVE W-FGS870-NDEPOT      TO IGS870-E05-NDEPOT.                      
           MOVE W-FGS870-LLIEU       TO IGS870-E05-LLIEU.                       
           MOVE ' '                TO SAUT.                                     
           MOVE IGS870-E05         TO LIGNE.                                    
           PERFORM ECRITURE-FICHIER.                                            
           ADD 1 TO CTR-LIGNE.                                                  
           MOVE IGS870-E10         TO LIGNE.                                    
           PERFORM ECRITURE-FICHIER.                                            
           ADD 1 TO CTR-LIGNE.                                                  
      *                                                                         
           MOVE ' '               TO SAUT.                                      
           MOVE IGS870-E40        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E45        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E50        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E52        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
           MOVE IGS870-E55        TO LIGNE.                                     
           PERFORM ECRITURE-1-LIGNE.                                            
       FIN-ECRITURE-ENTETE-PRE. EXIT.                                           
       ECRITURE-DETAIL SECTION.                                                 
      *-----------------                                                        
      * LIGNE QUANTITE EN STOCK                                                 
           INITIALIZE STOCK-VEND.                                               
           MOVE FGS870-CFAM              TO IGS870-D05-CFAM.                    
PM0205     MOVE FGS870-CMODSTOCK         TO IGS870-D05-CMODSTOCK.               
           MOVE '7S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED                                               
           = FGS870-APP-QSTOCKDIS + FGS870-CQE-QSTOCK                           
           + FGS870-EPU-QSTOCK .                                                
           ADD W-NBRE TO STOCK-VEND.                                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7                    TO IGS870-D05-STOCKDISPO.              
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-EPU-QSTOCK                           
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-D05-STOCKEPU.                
           MOVE '5'                      TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCKC                              
           ADD W-NBRE TO STOCK-VEND.                                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-D05-STOCKC.                  
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED                                               
           = FGS870-APP-QSTOCKRES + FGS870-CQE-QSTOCKRES                        
           + FGS870-EPU-QSTOCKRES .                                             
           ADD W-NBRE TO STOCK-VEND.                                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-D05-STOCKRES.                
           MOVE '8S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED                                               
           = FGS870-APP-QSTOCKDIS + FGS870-CQE-QSTOCK                           
           + FGS870-EPU-QSTOCK    + FGS870-QSTOCKC                              
           + FGS870-APP-QSTOCKRES + FGS870-CQE-QSTOCKRES                        
           + FGS870-EPU-QSTOCKRES                                               
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8                    TO IGS870-D05-STOCKVD.                 
      *    COMPUTE IGS870-D05-VOLUME                                            
      *    = FGS870-VOLUME / 1000000 .                                          
      *    DIVIDE 1000000 INTO FGS870-VOLUME                                    
      *    GIVING W-VOLUME ROUNDED.                                             
      *    MOVE W-VOLUME TO IGS870-D05-VOLUME.                                  
      *    MOVE '4'                      TO W-CODE.                             
      *    MOVE FGS870-NBPALETTE         TO W-NBRE                              
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT4                    TO IGS870-D05-NBPALETTE.               
           MOVE '7S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCKMAG                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7                    TO IGS870-D05-STOCKMAG.                
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCKMAGEPF                         
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-D05-STOCKMAGEPF.             
           MOVE '4S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCKMAGHS                          
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-D05-STOCKMAGHS.              
      *    MOVE '6'                      TO W-CODE.                             
      *    COMPUTE W-NBRE ROUNDED = FGS870-TOTQCDE                              
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT6                    TO IGS870-D05-QCDE.                    
           MOVE '4S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCKP                              
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-D05-STOCKPRET.               
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCKHS                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-D05-STOCKHS.                 
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCK-GENGRO                        
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-D05-STOCKGENGRO.             
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = FGS870-QSTOCK-PTF                           
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-D05-STOCKPTF.                
           MOVE '8S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED                                               
           = FGS870-QSTOCK-PTF + FGS870-QSTOCK-GENGRO                           
           + FGS870-APP-QSTOCKDIS + FGS870-CQE-QSTOCK                           
           + FGS870-EPU-QSTOCK    + FGS870-QSTOCKC                              
           + FGS870-APP-QSTOCKRES + FGS870-CQE-QSTOCKRES                        
           + FGS870-EPU-QSTOCKRES                                               
           + FGS870-QSTOCKP + FGS870-QSTOCKHS .                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8                    TO IGS870-D05-STOCKTOTDEP.             
           MOVE '9S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED                                               
           = FGS870-QSTOCK-PTF + FGS870-QSTOCK-GENGRO                           
           + FGS870-APP-QSTOCKDIS + FGS870-CQE-QSTOCK                           
           + FGS870-EPU-QSTOCK    + FGS870-QSTOCKC                              
           + FGS870-APP-QSTOCKRES + FGS870-CQE-QSTOCKRES                        
           + FGS870-EPU-QSTOCKRES                                               
           + FGS870-QSTOCKP + FGS870-QSTOCKHS                                   
           + FGS870-QSTOCKMAG .                                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT9                    TO IGS870-D05-STOCKDEPMAG.             
      * BLOC  NBRE DE CODIC                                                     
           MOVE '3'                      TO W-CODE.                             
           MOVE FGS870-APP-QSTKDIS-REF   TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT3                    TO IGS870-D05-APP-STK-REF.             
           MOVE FGS870-CQE-QSTK-REF      TO W-NBRE.                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT3                    TO IGS870-D05-CQE-STK-REF.             
           MOVE FGS870-EPU-QSTK-REF      TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT3                    TO IGS870-D05-EPU-STK-REF.             
           MOVE '4'                      TO W-CODE.                             
           MOVE FGS870-APP-TOT-REF       TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-D05-APP-TOT-REF.             
           MOVE FGS870-CQE-TOT-REF       TO W-NBRE.                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-D05-CQE-TOT-REF.             
           MOVE ' '                TO SAUT                                      
           MOVE IGS870-D05         TO LIGNE                                     
           PERFORM ECRITURE-FICHIER.                                            
      * SURIMPRESSION DU CFAM SI LA FAMILLE N EST PAS AFFILIE AU DEPOT          
           IF FGS870-WGENGRO = 'INA'                                            
              MOVE FGS870-CFAM        TO IGS870-D10-CFAM                        
              MOVE '+'                TO SAUT                                   
              MOVE IGS870-D10         TO LIGNE                                  
              PERFORM ECRITURE-1-LIGNE                                          
              PERFORM ECRITURE-1-LIGNE                                          
           END-IF.                                                              
           ADD 1 TO CTR-LIGNE.                                                  
           IF CTR-LIGNE >= MAX-LIGNE                                            
              PERFORM ECRITURE-ENTETE                                           
           END-IF.                                                              
       FIN-ECRITURE-DETAIL. EXIT.                                               
      *                                                                         
       CUMUL-RAYON SECTION.                                                     
      *-------------                                                            
           ADD FGS870-APP-TOT-REF   TO R-APP-TOT-REF                            
                                       D-APP-TOT-REF.                           
           ADD FGS870-APP-QSTKDIS-REF  TO R-APP-STK-REF                         
                                       D-APP-STK-REF.                           
           ADD FGS870-CQE-TOT-REF   TO R-CQE-TOT-REF                            
                                       D-CQE-TOT-REF.                           
           ADD FGS870-CQE-QSTK-REF  TO R-CQE-STK-REF                            
                                       D-CQE-STK-REF.                           
           ADD FGS870-EPU-QSTK-REF  TO R-EPU-STK-REF                            
                                       D-EPU-STK-REF.                           
           ADD FGS870-VOLUME        TO R-VOLUME                                 
                                       D-VOLUME.                                
           ADD FGS870-NBPALETTE     TO R-PALETTE                                
                                       D-PALETTE.                               
           ADD FGS870-QSTOCKC       TO R-STKC                                   
                                       D-STKC .                                 
           COMPUTE R-STKDIS                                                     
           = R-STKDIS + FGS870-EPU-QSTOCK                                       
           + FGS870-APP-QSTOCKDIS + FGS870-CQE-QSTOCK .                         
           COMPUTE R-STKRES                                                     
           = R-STKRES + FGS870-EPU-QSTOCKRES                                    
           + FGS870-APP-QSTOCKRES + FGS870-CQE-QSTOCKRES .                      
           COMPUTE R-STKTOT                                                     
           = R-STKRES + R-STKDIS + R-STKC .                                     
           COMPUTE R-STKEPU                                                     
           = R-STKEPU + FGS870-EPU-QSTOCK .                                     
           COMPUTE D-STKDIS                                                     
           = D-STKDIS + FGS870-EPU-QSTOCK                                       
           + FGS870-APP-QSTOCKDIS + FGS870-CQE-QSTOCK .                         
           COMPUTE D-STKRES                                                     
           = D-STKRES + FGS870-EPU-QSTOCKRES                                    
           + FGS870-APP-QSTOCKRES + FGS870-CQE-QSTOCKRES .                      
           COMPUTE D-STKTOT                                                     
           = D-STKRES + D-STKDIS + D-STKC .                                     
           COMPUTE D-STKEPU                                                     
           = D-STKEPU + FGS870-EPU-QSTOCK .                                     
           ADD FGS870-QSTOCKMAG             TO R-STKMAG                         
                                               D-STKMAG .                       
           ADD FGS870-QSTOCKMAGHS           TO R-STKMAGHS                       
                                               D-STKMAGHS.                      
           ADD FGS870-QSTOCKMAGEPF          TO R-STKMAGEPF                      
                                               D-STKMAGEPF.                     
           ADD FGS870-TOTQCDE               TO R-STKCDE                         
                                               D-STKCDE .                       
           ADD FGS870-QSTOCK-GENGRO         TO R-STKGENGRO                      
                                               D-STKGENGRO .                    
           ADD FGS870-QSTOCK-PTF            TO R-STKPTF                         
                                               D-STKPTF .                       
           ADD FGS870-QSTOCKP               TO R-STKP                           
                                               D-STKP .                         
           ADD FGS870-QSTOCKHS              TO R-STKHS                          
                                               D-STKHS .                        
           COMPUTE R-STKTOTDEP                                                  
           = R-STKTOT + R-STKHS + R-STKP + R-STKGENGRO + R-STKPTF.              
           COMPUTE D-STKTOTDEP                                                  
           = D-STKTOT + D-STKHS + D-STKP + D-STKGENGRO + D-STKPTF.              
           COMPUTE R-STKTOTDEPMAG                                               
           = R-STKTOTDEP + R-STKMAG.                                            
           COMPUTE D-STKTOTDEPMAG                                               
           = D-STKTOTDEP + D-STKMAG.                                            
       FIN-CUMUL-RAYON. EXIT.                                                   
       LIGNE-RAYON     SECTION.                                                 
      *-----------------                                                        
           MOVE ' ' TO SAUT.                                                    
           MOVE IGS870-T05 TO LIGNE.                                            
           PERFORM ECRITURE-LIGNE-SUI.                                          
           MOVE W-FGS870-RAYON TO IGS870-T10-RAYON .                            
           MOVE '7S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKDIS                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7          TO IGS870-T10-RSTOCKDISPO.                       
           MOVE '5S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKRES                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5          TO IGS870-T10-RSTOCKRES.                         
           MOVE '8S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKTOT                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8          TO IGS870-T10-RSTOCKVD.                          
           MOVE '5S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKEPU                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5          TO IGS870-T10-RSTOCKEPU.                         
      *    COMPUTE IGS870-T10-RVOLUME                                           
      *    = R-VOLUME / 1000000 .                                               
      *    MOVE '4'                      TO W-CODE.                             
      *    MOVE R-PALETTE                TO W-NBRE                              
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT4                    TO IGS870-T10-RNBPALETTE.              
           MOVE '7S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKMAG                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7          TO IGS870-T10-RSTOCKMAG.                         
           MOVE '5S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKMAGEPF                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5          TO IGS870-T10-RSTOCKMAGEPF.                      
           MOVE '4S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKMAGHS                                  
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4          TO IGS870-T10-RSTOCKMAGHS.                       
      *    MOVE '6'                      TO W-CODE.                             
      *    COMPUTE W-NBRE ROUNDED = R-STKCDE                                    
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT6                    TO IGS870-T10-RQCDE.                   
           MOVE '5'                      TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = R-STKC                                      
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-T10-RSTOCKC.                 
           MOVE '4S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = R-STKP                                      
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-RSTOCKPRET.              
           COMPUTE W-NBRE ROUNDED = R-STKHS                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-RSTOCKHS.                
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = R-STKGENGRO                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-T10-RSTOCKGENGRO.            
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = R-STKPTF                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-T10-RSTOCKPTF.               
           MOVE '8S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKTOTDEP                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8          TO IGS870-T10-RSTOCKTOTDEP.                      
           MOVE '9S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = R-STKTOTDEPMAG                              
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT9          TO IGS870-T10-RSTOCKDEPMAG.                      
      * BLOC  NBRE DE CODIC                                                     
           MOVE '4'                      TO W-CODE.                             
           MOVE R-APP-TOT-REF            TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-RAPP-TOT-REF.            
           MOVE R-APP-STK-REF            TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-RAPP-STK-REF.            
           MOVE R-CQE-TOT-REF            TO W-NBRE.                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-RCQE-TOT-REF.            
           MOVE R-CQE-STK-REF            TO W-NBRE.                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-RCQE-STK-REF.            
           MOVE R-EPU-STK-REF            TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T10-REPU-STK-REF.            
           MOVE ' '                TO SAUT                                      
           MOVE IGS870-T10         TO LIGNE                                     
           PERFORM ECRITURE-LIGNE-SUI.                                          
           MOVE ' '                TO SAUT                                      
           MOVE IGS870-B05         TO LIGNE                                     
           PERFORM ECRITURE-LIGNE-SUI.                                          
           INITIALIZE TOT-RAYON .                                               
       FIN-LIGNE-RAYON. EXIT.                                                   
       LIGNE-DEPOT     SECTION.                                                 
      *-----------------                                                        
           MOVE '7S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKDIS                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7          TO IGS870-T15-DSTOCKDISPO.                       
           MOVE '5S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKRES                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5          TO IGS870-T15-DSTOCKRES.                         
           MOVE '8S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKTOT                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8          TO IGS870-T15-DSTOCKVD.                          
           MOVE '8S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKTOTDEP                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT8          TO IGS870-T15-DSTOCKTOTDEP.                      
           MOVE '9S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKTOTDEPMAG                              
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT9          TO IGS870-T15-DSTOCKMAGDEP.                      
           MOVE '5S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKEPU                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5          TO IGS870-T15-DSTOCKEPU.                         
      *    COMPUTE IGS870-T15-DVOLUME                                           
      *    = D-VOLUME / 1000000 .                                               
      *    MOVE '4'                      TO W-CODE.                             
      *    MOVE D-PALETTE                TO W-NBRE                              
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT4                    TO IGS870-T15-DNBPALETTE.              
           MOVE '7S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKMAG                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT7          TO IGS870-T15-DSTOCKMAG.                         
           MOVE '5S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKMAGEPF                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5          TO IGS870-T15-DSTOCKMAGEPF.                      
           MOVE '4S'           TO W-CODE.                                       
           COMPUTE W-NBRE ROUNDED = D-STKMAGHS                                  
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4          TO IGS870-T15-DSTOCKMAGHS.                       
      *    MOVE '6'                      TO W-CODE.                             
      *    COMPUTE W-NBRE ROUNDED = D-STKCDE                                    
      *    PERFORM TEST-CADRAGE.                                                
      *    MOVE EDIT6                    TO IGS870-T15-DQCDE.                   
           MOVE '5'                      TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = D-STKC                                      
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-T15-DSTOCKC.                 
           MOVE '4S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = D-STKP                                      
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DSTOCKPRET.              
           COMPUTE W-NBRE ROUNDED = D-STKHS                                     
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DSTOCKHS.                
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = D-STKGENGRO                                 
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-T15-DSTOCKGENGRO.            
           MOVE '5S'                     TO W-CODE.                             
           COMPUTE W-NBRE ROUNDED = D-STKPTF                                    
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT5                    TO IGS870-T15-DSTOCKPTF.               
      * BLOC  NBRE DE CODIC                                                     
           MOVE '4'                      TO W-CODE.                             
           MOVE D-APP-TOT-REF            TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DAPP-TOT-REF.            
           MOVE D-APP-STK-REF            TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DAPP-STK-REF.            
           MOVE D-CQE-TOT-REF            TO W-NBRE.                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DCQE-TOT-REF.            
           MOVE D-CQE-STK-REF            TO W-NBRE.                             
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DCQE-STK-REF.            
           MOVE D-EPU-STK-REF            TO W-NBRE .                            
           PERFORM TEST-CADRAGE.                                                
           MOVE EDIT4                    TO IGS870-T15-DEPU-STK-REF.            
           MOVE ' '                TO SAUT                                      
           MOVE IGS870-T15         TO LIGNE                                     
           PERFORM ECRITURE-LIGNE-SUI.                                          
           MOVE ' '                TO SAUT                                      
           MOVE IGS870-B05         TO LIGNE                                     
           PERFORM ECRITURE-LIGNE-SUI.                                          
           INITIALIZE TOT-DEPOT .                                               
       FIN-LIGNE-DEPOT. EXIT.                                                   
      *                                                                         
       ECRITURE-LIGNE-SUI SECTION.                                              
      *-------------------                                                      
           PERFORM ECRITURE-FICHIER.                                            
           EVALUATE SAUT                                                        
             WHEN ' ' ADD 1 TO CTR-LIGNE                                        
             WHEN '0' ADD 2 TO CTR-LIGNE                                        
             WHEN OTHER CONTINUE                                                
           END-EVALUATE.                                                        
           IF CTR-LIGNE >= MAX-LIGNE                                            
              PERFORM ECRITURE-ENTETE-PRE                                       
           END-IF.                                                              
       FIN-ECRITURE-LIGNE-SUI. EXIT.                                            
      *                                                                         
       ECRITURE-1-LIGNE SECTION.                                                
      *-----------------                                                        
           PERFORM ECRITURE-FICHIER.                                            
           EVALUATE SAUT                                                        
             WHEN ' ' ADD 1 TO CTR-LIGNE                                        
             WHEN '0' ADD 2 TO CTR-LIGNE                                        
             WHEN OTHER CONTINUE                                                
           END-EVALUATE.                                                        
           IF CTR-LIGNE >= MAX-LIGNE                                            
              PERFORM ECRITURE-ENTETE                                           
           END-IF.                                                              
       FIN-ECRITURE-1-LIGNE. EXIT.                                              
      *                                                                         
      *                                                                         
      * CETTE PROCEDURE EVITE LA TRONCATURE A GAUCHE DES NBRES                  
      *                                                                         
      * EN ENTREE : W-LCODE -> NBRE DE CARACTERES MAXIMUM EDITES                
      *             W-SIGNE -> S POUR UN NOMBRE SIGNE (AUTREMENT SPACE)         
      *             W-NBRE  -> LE NOMBRE QUE L ON VEUT EDITER                   
      * EN SORTIE : EDIT?   -> LE NOMBRE SOUS LA FORME PIC X                    
      *             SI LE NOMBRE EST PLUS GRAND QUE LA ZONE D EDITION           
      *             ON SUPPRIME LES CHIFFRES DES MILLIERS QUI SONT              
      *             REMPLACES PAR UN 'K'                                        
      *             SI LE NOMBRE EST TOUJOURS TROP GRAND                        
      *             ON SUPPRIME LES CHIFFRES DES MILLIONS QUI SONT              
      *             REMPLACES PAR UN 'M'                                        
       TEST-CADRAGE       SECTION.                                              
           EVALUATE W-LCODE                                                     
             WHEN '3'                                                           
                  MOVE 999      TO W-MAX                                        
             WHEN '4'                                                           
                  MOVE 9999     TO W-MAX                                        
             WHEN '5'                                                           
                  MOVE 99999    TO W-MAX                                        
             WHEN '6'                                                           
                  MOVE 999999   TO W-MAX                                        
             WHEN '7'                                                           
                  MOVE 9999999  TO W-MAX                                        
             WHEN '8'                                                           
                  MOVE 99999999 TO W-MAX                                        
             WHEN '9'                                                           
                  MOVE 999999999 TO W-MAX                                       
             WHEN OTHER                                                         
                  MOVE 99999    TO W-MAX                                        
           END-EVALUATE                                                         
           IF W-NBRE > W-MAX                                                    
              EVALUATE W-LCODE                                                  
                WHEN '3'                                                        
                     MOVE 99      TO W-MAX                                      
                WHEN '4'                                                        
                     MOVE 999     TO W-MAX                                      
                WHEN '5'                                                        
                     MOVE 9999    TO W-MAX                                      
                WHEN '6'                                                        
                     MOVE 99999   TO W-MAX                                      
                WHEN '7'                                                        
                     MOVE 999999  TO W-MAX                                      
                WHEN '8'                                                        
                     MOVE 9999999 TO W-MAX                                      
                WHEN '9'                                                        
                     MOVE 99999999 TO W-MAX                                     
                WHEN OTHER                                                      
                     MOVE 99999    TO W-MAX                                     
              END-EVALUATE                                                      
              DIVIDE W-NBRE BY 1000 GIVING QUOTIENT REMAINDER RESTE             
              IF QUOTIENT > W-MAX                                               
                DIVIDE W-NBRE BY 1000000 GIVING QUOTIENT REMAINDER RESTE        
                EVALUATE W-LCODE                                                
                  WHEN '3'                                                      
                       MOVE QUOTIENT  TO W-NUMED2                               
                       MOVE W-NUMED2  TO W-EDIT3                                
                       MOVE 'M'       TO W-MILLIERS3                            
                  WHEN '4'                                                      
                       MOVE QUOTIENT  TO W-NUMED3                               
                       MOVE W-NUMED3  TO W-EDIT4                                
                       MOVE 'M'       TO W-MILLIERS4                            
                  WHEN '5'                                                      
                       MOVE QUOTIENT  TO W-NUMED4                               
                       MOVE W-NUMED4  TO W-EDIT5                                
                       MOVE 'M'       TO W-MILLIERS5                            
                  WHEN '6'                                                      
                       MOVE QUOTIENT  TO W-NUMED5                               
                       MOVE W-NUMED5  TO W-EDIT6                                
                       MOVE 'M'       TO W-MILLIERS6                            
                  WHEN '7'                                                      
                       MOVE QUOTIENT  TO W-NUMED6                               
                       MOVE W-NUMED6  TO W-EDIT7                                
                       MOVE 'M'       TO W-MILLIERS7                            
                  WHEN '8'                                                      
                       MOVE QUOTIENT  TO W-NUMED7                               
                       MOVE W-NUMED7  TO W-EDIT8                                
                       MOVE 'M'       TO W-MILLIERS8                            
                  WHEN '9'                                                      
                       MOVE QUOTIENT  TO W-NUMED8                               
                       MOVE W-NUMED8  TO W-EDIT9                                
                       MOVE 'M'       TO W-MILLIERS9                            
                  WHEN OTHER                                                    
                       MOVE '*******' TO EDITX                                  
                END-EVALUATE                                                    
              ELSE                                                              
                EVALUATE W-LCODE                                                
                  WHEN '3'                                                      
                       MOVE QUOTIENT  TO W-NUMED2                               
                       MOVE W-NUMED2  TO W-EDIT3                                
                       MOVE 'K'       TO W-MILLIERS3                            
                  WHEN '4'                                                      
                       MOVE QUOTIENT  TO W-NUMED3                               
                       MOVE W-NUMED3  TO W-EDIT4                                
                       MOVE 'K'       TO W-MILLIERS4                            
                  WHEN '5'                                                      
                       MOVE QUOTIENT  TO W-NUMED4                               
                       MOVE W-NUMED4  TO W-EDIT5                                
                       MOVE 'K'       TO W-MILLIERS5                            
                  WHEN '6'                                                      
                       MOVE QUOTIENT  TO W-NUMED5                               
                       MOVE W-NUMED5  TO W-EDIT6                                
                       MOVE 'K'       TO W-MILLIERS6                            
                  WHEN '7'                                                      
                       MOVE QUOTIENT  TO W-NUMED6                               
                       MOVE W-NUMED6  TO W-EDIT7                                
                       MOVE 'K'       TO W-MILLIERS7                            
                  WHEN '8'                                                      
                       MOVE QUOTIENT  TO W-NUMED7                               
                       MOVE W-NUMED7  TO W-EDIT8                                
                       MOVE 'K'       TO W-MILLIERS8                            
                  WHEN '9'                                                      
                       MOVE QUOTIENT  TO W-NUMED8                               
                       MOVE W-NUMED8  TO W-EDIT9                                
                       MOVE 'K'       TO W-MILLIERS9                            
                  WHEN OTHER                                                    
                       MOVE '*******' TO EDITX                                  
                END-EVALUATE                                                    
              END-IF                                                            
           ELSE                                                                 
              IF W-NBRE < 0                                                     
                 EVALUATE W-CODE                                                
                   WHEN '3'                                                     
                        MOVE W-NBRE    TO W-NUMED3                              
                        MOVE W-NUMED3  TO EDIT3                                 
                   WHEN '4'                                                     
                        MOVE W-NBRE    TO W-NUMED4                              
                        MOVE W-NUMED4  TO EDIT4                                 
                   WHEN '5'                                                     
                        MOVE W-NBRE    TO W-NUMED5                              
                        MOVE W-NUMED5  TO EDIT5                                 
                   WHEN '5S'                                                    
                        MOVE W-NBRE    TO W-NUMED5S                             
                        MOVE W-NUMED5S TO EDIT5                                 
                   WHEN '6S'                                                    
                        MOVE W-NBRE    TO W-NUMED6S                             
                        MOVE W-NUMED6S TO EDIT6                                 
                   WHEN '7S'                                                    
                        MOVE W-NBRE    TO W-NUMED7S                             
                        MOVE W-NUMED7S TO EDIT7                                 
                   WHEN '8S'                                                    
                        MOVE W-NBRE    TO W-NUMED8S                             
                        MOVE W-NUMED8S TO EDIT8                                 
                   WHEN '9S'                                                    
                        MOVE W-NBRE    TO W-NUMED9S                             
                        MOVE W-NUMED9S TO EDIT9                                 
                   WHEN OTHER                                                   
                        MOVE '*******'  TO EDITX                                
                 END-EVALUATE                                                   
              ELSE                                                              
                 EVALUATE W-LCODE                                               
                   WHEN '3'                                                     
                        MOVE W-NBRE    TO W-NUMED3                              
                        MOVE W-NUMED3  TO EDIT3                                 
                   WHEN '4'                                                     
                        MOVE W-NBRE    TO W-NUMED4                              
                        MOVE W-NUMED4  TO EDIT4                                 
                   WHEN '5'                                                     
                        MOVE W-NBRE    TO W-NUMED5                              
                        MOVE W-NUMED5  TO EDIT5                                 
                   WHEN '6'                                                     
                        MOVE W-NBRE    TO W-NUMED6                              
                        MOVE W-NUMED6  TO EDIT6                                 
                   WHEN '7'                                                     
                        MOVE W-NBRE    TO W-NUMED7                              
                        MOVE W-NUMED7  TO EDIT7                                 
                   WHEN '8'                                                     
                        MOVE W-NBRE    TO W-NUMED8                              
                        MOVE W-NUMED8  TO EDIT8                                 
                   WHEN '9'                                                     
                        MOVE W-NBRE    TO W-NUMED9                              
                        MOVE W-NUMED9  TO EDIT9                                 
                   WHEN OTHER                                                   
                        MOVE '*******'  TO EDITX                                
                 END-EVALUATE                                                   
              END-IF                                                            
           END-IF.                                                              
           IF EDITX = SPACES                                                    
              MOVE '0' TO EDITX                                                 
           END-IF.                                                              
       FIN-TEST-CADRAGE.       EXIT.                                            
      *                                                                         
       ECRITURE-FICHIER SECTION.                                                
      *-------------                                                            
           EVALUATE IGS870-E00-CETAT                                            
             WHEN 'IGS870' MOVE LIGNE-ETAT TO LIGNE-IGS870                      
                         WRITE LIGNE-IGS870                                     
             WHEN 'IGS871' MOVE LIGNE-ETAT TO LIGNE-IGS871                      
                         WRITE LIGNE-IGS871                                     
             WHEN OTHER DISPLAY IGS870-E00-CETAT                                
                             '*** CODE ETAT IN CONNU **'                        
           END-EVALUATE.                                                        
      *                                                                         
       F-FIN-ECRITURE-FICHIER.                                                  
           EXIT.                                                                
      *                                                                         
      *---------------------------------------------------------------*         
      *         P A R A G R A P H E   F I N  A N O R M A L E          *         
      *---------------------------------------------------------------*         
      *                                                                         
       FIN-ANORMALE SECTION.                                                    
      *-------------                                                            
           DISPLAY FGS870-LFAM                                                  
           MOVE 'BGS871' TO ABEND-PROG.                                         
           CALL 'ABEND' USING                                                   
                        ABEND-PROG                                              
                        ABEND-MESS.                                             
      *                                                                         
       F-FIN-ANORMALE.                                                          
           EXIT.                                                                
      *                                                                         
           EJECT                                                                
