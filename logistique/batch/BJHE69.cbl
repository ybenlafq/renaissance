      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID        DIVISION.                                              00000020
       PROGRAM-ID. BJHE69.                                              00000040
       AUTHOR.  DSA045-CP.                                              00000050
       DATE-COMPILED.                                                   00000060
      ******************************************************************00000080
      * 05.09.2007                                                     *00000090
      * ==========                                                     *00000090
      *                                                                *00000090
      * PURGE DES MOUVEMENTS DU FICHIER VSAM COMMUN AUX ETATS JHC011,  *00000090
      *                   JHE601 ET JEF021 JUSQU'A MOIS - 1.           *00000090
      *               (ON GARDE LES MOUVEMENTS PENDANT 31 JOURS)       *00000090
      *                                                                *00000090
      * FREQUENCE : HEBDOMADAIRE (DIMANCHE)                            *00000090
      ******************************************************************00000080
      *                                                                 00000080
       ENVIRONMENT DIVISION.                                            00000150
       INPUT-OUTPUT SECTION.                                            00000200
       FILE-CONTROL.                                                    00000210
      *****************************************************************         
      * FICHIERS EN ENTREE                                            *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE    ASSIGN   TO     FDATE                        00000250
      *                    FILE STATUS  ST-FDATE.                               
      *--                                                                       
           SELECT FDATE    ASSIGN   TO     FDATE                                
                           FILE STATUS  ST-FDATE                                
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
           SELECT FJHE69E  ASSIGN   TO   FJHE69E                        00000250
                           ORGANIZATION  INDEXED                                
                           ACCESS MODE   DYNAMIC                                
                           RECORD KEY  HE69E-CLE                                
                           FILE STATUS  ST-HE69E.                               
      *                                                                 00000190
      *****************************************************************         
      * FICHIER EN SORTIE                                             *         
      *****************************************************************         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FJHE69S  ASSIGN   TO   FJHE69S                        00000250
      *                    FILE STATUS  ST-HE69S.                               
      *--                                                                       
           SELECT FJHE69S  ASSIGN   TO   FJHE69S                                
                           FILE STATUS  ST-HE69S                                
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000190
      *                                                                 00000190
       DATA DIVISION.                                                   00000270
       FILE  SECTION.                                                   00000290
      *                                                                 00000190
       FD  FDATE                                                        00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FDATE-ENREG               PIC X(80).                                 
      *                                                                 00000190
       FD  FJHE69E.                                                     00000440
       01  FJHE69E-ENREG.                                                       
           05  HE69E-CLE.                                                       
               10  FJHE69E-ETAT      PIC X(006).                                
               10  FJHE69E-DATE      PIC X(008).                                
               10  FJHE69E-USER      PIC X(008).                                
               10  FJHE69E-NSEQ      PIC 9(005).                                
           05  FJHE69E-RESTES        PIC X(223).                                
      *                                                                 00000190
       FD  FJHE69S                                                      00000440
           RECORDING MODE      F                                        00000470
           LABEL RECORD STANDARD.                                       00000470
       01  FJHE69S-ENREG             PIC X(250).                                
      *                                                                 00000190
      *****************************************************************         
      *    W O R K I N G   S T O R A G E   S E C T I O N              *         
      *****************************************************************         
       WORKING-STORAGE SECTION.                                         00000710
       77  ST-FDATE                  PIC X(02).                                 
       77  ST-HE69E                  PIC X(02).                                 
       77  ST-HE69S                  PIC X(02).                                 
       77  WRK-CLEFIC                PIC X(06)  VALUE 'AAAAAA'.                 
       77  WRK-RESTE                 PIC 9(02).                                 
       77  WRK-SAV-MOIS              PIC 9(02).                                 
       77  WRK-NBRPRG                PIC S9(9)  COMP-3.                         
       77  WRK-NBR69E                PIC S9(9)  COMP-3.                         
       77  WRK-NBR69S                PIC S9(9)  COMP-3.                         
      *                                                                 00000600
       01  WRK-FLAGS                 PIC X(01)  VALUE SPACES.                   
           88  WRK-FINTRT            VALUE '1'.                                 
      *                                                                 00000600
      *-----------------------------------------------------------------        
      *--- VALEUR DATE PARAMETRE EN ENTREE                                      
      *-----------------------------------------------------------------        
       01  WRK-DAT-PARAM.                                                       
           05  WRK-DAT-JJ            PIC X(02).                                 
           05  WRK-DAT-MM            PIC X(02).                                 
           05  WRK-DAT-SSAA.                                                    
               10 WRK-DAT-SS         PIC X(02).                                 
               10 WRK-DAT-AA         PIC X(02).                                 
      *                                                                 00000600
      *-----------------------------------------------------------------        
      *--- VALEUR DATE LIMITE DE PURGE                                          
      *-----------------------------------------------------------------        
       01  WRK-TRT-FDATE.                                                       
           05  WRK-TRT-SSAA.                                                    
               10 WRK-TRT-SS         PIC X(02).                                 
               10 WRK-TRT-SS9        REDEFINES  WRK-TRT-SS                      
                                     PIC 9(02).                                 
               10 WRK-TRT-AA         PIC X(02).                                 
               10 WRK-TRT-AA9        REDEFINES  WRK-TRT-AA                      
                                     PIC 9(02).                                 
           05  WRK-TRT-SSAA9         REDEFINES  WRK-TRT-SSAA                    
                                     PIC 9(04).                                 
           05  WRK-TRT-MOIS          PIC X(02).                                 
           05  WRK-TRT-MOIS9         REDEFINES  WRK-TRT-MOIS                    
                                     PIC 9(02).                                 
           05  WRK-TRT-JOUR          PIC X(02).                                 
           05  WRK-TRT-JOUR9         REDEFINES  WRK-TRT-JOUR                    
                                     PIC 9(02).                                 
      *                                                                 00000600
      *-----------------------------------------------------------------        
      *--- TABLE DES MOIS                                                       
      *-----------------------------------------------------------------        
       01  WRK-TAB-MOIS.                                                        
           03  FILLER                  PIC X(11) VALUE '31JANVIER'.             
           03  FILLER                  PIC X(11) VALUE '00FEVRIER'.             
           03  FILLER                  PIC X(11) VALUE '31MARS'.                
           03  FILLER                  PIC X(11) VALUE '30AVRIL'.               
           03  FILLER                  PIC X(11) VALUE '31MAI'.                 
           03  FILLER                  PIC X(11) VALUE '30JUIN'.                
           03  FILLER                  PIC X(11) VALUE '31JUILLET'.             
           03  FILLER                  PIC X(11) VALUE '31AOUT'.                
           03  FILLER                  PIC X(11) VALUE '30SEPTEMBRE'.           
           03  FILLER                  PIC X(11) VALUE '31OCTOBRE'.             
           03  FILLER                  PIC X(11) VALUE '30NOVEMBRE'.            
           03  FILLER                  PIC X(11) VALUE '31DECEMBRE'.            
       01  FILLER                      REDEFINES WRK-TAB-MOIS.                  
           03  WRK-TBMOIS              OCCURS 12.                               
               05  WRK-NBJ-MOIS        PIC 9(02).                               
               05  WRK-LIB-MOIS        PIC X(09).                               
      *                                                                 00000600
      *-----------------------------------------------------------------        
      * PROCEDURE DIVISION.                                                     
      *-----------------------------------------------------------------        
      *                                                                 00000600
       PROCEDURE   DIVISION.                                            00002160
       TRT-GENERAL  SECTION.                                                    
           PERFORM  MODULE-ENTREE.                                              
           PERFORM  MODULE-TRAITEM  UNTIL WRK-FINTRT.                           
           PERFORM  MODULE-SORTIE.                                              
           GOBACK.                                                              
       TRT-GENERAL-F. EXIT.                                                     
      *                                                                         
      *----------------------------------------------------------------*        
      *    OUVERTURE DES FICHIERS FDATE  -  FJHE69E  - FJHE69S         *        
      *----------------------------------------------------------------*        
       MODULE-ENTREE  SECTION.                                                  
           INITIALIZE  WRK-FLAGS.                                               
           MOVE ZEROS                TO WRK-NBR69E                              
                                        WRK-NBR69S                              
                                        WRK-NBRPRG.                             
           OPEN  INPUT FDATE.                                                   
           IF  ST-FDATE          NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FDATE  = ' ST-FDATE            
               GOBACK                                                           
           END-IF.                                                              
           OPEN  INPUT FJHE69E.                                                 
           IF  ST-HE69E          NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHE69E = ' ST-HE69E           
               CLOSE  FDATE                                                     
               GOBACK                                                           
           END-IF.                                                              
           OPEN OUTPUT FJHE69S.                                                 
           IF  ST-HE69S          NOT = '00'                                     
               DISPLAY '*** PB OUVERTURE FICHIER FJHE69S = ' ST-HE69S           
               CLOSE  FDATE FJHE69E                                             
               GOBACK                                                           
           END-IF.                                                              
      *-----------------------------------------------------------------        
      *--- LECTURE DU FICHIER FDATE (JJMMSSAA)                                  
      *-----------------------------------------------------------------        
           READ FDATE INTO WRK-DAT-PARAM                                        
                AT END                                                          
                DISPLAY 'FICHIER FDATE VIDE'                                    
                DISPLAY 'ABANDON PROGRAMME D''EXTRACTION'                       
                CLOSE  FDATE FJHE69E FJHE69S                                    
                GOBACK                                                          
           END-READ.                                                            
      *--- CALCUL DATE FIN DE PURGE                                             
           PERFORM  MODULE-CALC-DATEFIN.                                        
           MOVE WRK-CLEFIC           TO HE69E-CLE.                              
      *--- SI CODE RETOUR=23 ----> PAS D'ENREGISTREMENT TROUVE                  
           START FJHE69E KEY IS      >  HE69E-CLE                               
               INVALID KEY                                                      
                   SET WRK-FINTRT    TO TRUE.                                   
           IF  NOT WRK-FINTRT                                                   
               PERFORM  MODULE-READN-FJHE69E                                    
           END-IF.                                                              
       MODULE-ENTREE-F. EXIT.                                                   
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *    TRAITEMENT CREATION FICHIER FJHE69S                         *        
      *----------------------------------------------------------------*        
       MODULE-TRAITEM  SECTION.                                         00002330
           ADD 1                     TO WRK-NBR69E.                             
      *--- REPRISE DES MVTS DONT DATE SUPERIEURE A LA DATE CALCULEE     00002950
           IF  FJHE69E-DATE          >  WRK-TRT-FDATE OR                        
               FJHE69E-ETAT          =  WRK-CLEFIC                              
               PERFORM  MODULE-ECRI-FJHE69S                                     
           ELSE                                                                 
               ADD 1                 TO WRK-NBRPRG                              
           END-IF.                                                              
           PERFORM  MODULE-READN-FJHE69E.                                       
       MODULE-TRAITEM-F. EXIT.                                          00002620
      *                                                                 00002950
      *----------------------------------------------------------------*        
      *      FERMETURE DU FICHIER                                      *        
      *----------------------------------------------------------------*        
       MODULE-SORTIE  SECTION.                                          00002330
           CLOSE  FDATE  FJHE69E  FJHE69S.                                      
           IF  ST-FDATE              = '00' AND                                 
               ST-HE69E              = '00' AND                                 
               ST-HE69S              = '00'                                     
               DISPLAY '*'                                                      
               DISPLAY '* BJHE69 : CREATION FJHE69S O.K ***'                    
           ELSE                                                                 
               DISPLAY '*'                                                      
               DISPLAY '* BJHE69 : PROBLEME FERMETURE FICHIERS '                
               DISPLAY '* ------   FICHIER FDATE   = ' ST-FDATE                 
               DISPLAY '*          FICHIER FJHE69E = ' ST-HE69E                 
               DISPLAY '*          FICHIER FJHE69S = ' ST-HE69S                 
           END-IF.                                                              
           DISPLAY '*'.                                                         
           DISPLAY '*          PURGE DU FICHIER FHJE601 '                       
           DISPLAY '*          DATE TRAITEMENT        = ' WRK-DAT-JJ            
                                                      '.' WRK-DAT-MM            
                                                      '.' WRK-DAT-SSAA.         
           DISPLAY '*          DATE FIN PURGE         = ' WRK-TRT-JOUR          
                                                      '.' WRK-TRT-MOIS          
                                                      '.' WRK-TRT-SSAA.         
           DISPLAY '*'.                                                         
           DISPLAY '*          NBR ENREG. LUS FJHE69E = ' WRK-NBR69E.           
           DISPLAY '*          NBR ENREG. ECR FJHE69S = ' WRK-NBR69S.           
           DISPLAY '*          NBR ENREG. PURGES      = ' WRK-NBRPRG.           
       MODULE-SORTIE-F. EXIT.                                           00002620
      *-----------------------------------------------------------------        
      *    RECHERCHE DE DATE LIMITE DE PURGE : M - 1.                           
      *-----------------------------------------------------------------        
       MODULE-CALC-DATEFIN  SECTION.                                            
           MOVE WRK-DAT-SSAA         TO WRK-TRT-SSAA.                           
           MOVE WRK-DAT-MM           TO WRK-TRT-MOIS.                           
           MOVE WRK-DAT-JJ           TO WRK-TRT-JOUR.                           
      *--- RECHERCHE ANNEE BISSEXTILE                                           
           IF  WRK-TRT-AA9           =  0                                       
               DIVIDE WRK-TRT-SS9    BY 4 GIVING WRK-RESTE                      
                                       REMAINDER WRK-RESTE                      
           ELSE                                                                 
               DIVIDE WRK-TRT-AA9    BY 4 GIVING WRK-RESTE                      
                                       REMAINDER WRK-RESTE                      
           END-IF.                                                              
           IF  WRK-RESTE             =  ZEROS                                   
               MOVE 29               TO WRK-NBJ-MOIS (2)                        
           ELSE                                                                 
               MOVE 28               TO WRK-NBJ-MOIS (2)                        
           END-IF.                                                              
      *--- CALCUL DATE LIMITE D'EXTRACTION : 1 MOIS DANS LE FICHIER             
           MOVE WRK-TRT-MOIS9        TO WRK-SAV-MOIS.                           
           SUBTRACT 1              FROM WRK-TRT-MOIS9.                          
           IF  WRK-TRT-MOIS9         <  1                                       
               MOVE 12               TO WRK-TRT-MOIS9                           
               SUBTRACT 1          FROM WRK-TRT-SSAA9                           
           END-IF.                                                              
      *--- SI NB JOURS MOIS PARAMETRE > NB JOURS MOIS PRECEDENT,                
      *---    RECHERCHE DU DERNIER JOUR DE PURGE (ON GARDE 31 JOURS)            
           IF  WRK-NBJ-MOIS (WRK-SAV-MOIS) >                                    
                                        WRK-NBJ-MOIS (WRK-TRT-MOIS9)            
               SUBTRACT WRK-TRT-JOUR9                                           
                                   FROM WRK-NBJ-MOIS (WRK-SAV-MOIS)             
                                 GIVING WRK-RESTE                               
               SUBTRACT WRK-RESTE  FROM WRK-NBJ-MOIS (WRK-TRT-MOIS9)            
                                 GIVING WRK-TRT-JOUR9                           
           END-IF.                                                              
       MODULE-CALC-DATEFIN-F. EXIT.                                             
      *                                                                         
      *                                                                         
       MODULE-READN-FJHE69E  SECTION.                                           
           READ FJHE69E NEXT.                                                   
           IF  ST-HE69E          NOT =  '00'                                    
                                 AND    '10'                                    
               DISPLAY '* BJHE69 : PB LECTURE FJHE69E = ' ST-HE69E              
               CLOSE FDATE FJHE69E FJHE69S                                      
               GOBACK                                                           
           END-IF.                                                              
           IF  ST-HE69E              = '10'                                     
               SET WRK-FINTRT        TO TRUE                                    
           END-IF.                                                              
       MODULE-READN-FJHE69E-F. EXIT.                                            
      *                                                                         
      *                                                                         
       MODULE-ECRI-FJHE69S  SECTION.                                            
           WRITE FJHE69S-ENREG  FROM FJHE69E-ENREG.                             
           IF  ST-HE69S          NOT =  '00'                                    
               DISPLAY '* BJHE69 : PB ECRITURE FJHE69S = ' ST-HE69S             
               CLOSE FDATE FJHE69E FJHE69S                                      
               GOBACK                                                           
           END-IF.                                                              
           ADD 1                     TO WRK-NBR69S.                             
       MODULE-REMPL-FJHE69S-F. EXIT.                                            
      *                                                                 00002950
      *********** F I N  D E  'B J H E 6 9' ****************************00002950
