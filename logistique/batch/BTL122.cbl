      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BTL122.                                                     
       AUTHOR.      DSA026.                                             00030005
                                                                        00040005
      *----------------------------------------------------------- *    00050005
      *                                                            *    00060005
      *    PROGRAMME      : BTL122                                 *    00070005
      *    FONCTIONS      : PRODUCTION D'UN FICHIERS               *    00070005
      *    CONCERNANT LES STATISTIQUES DES TOURNEES DE LIVRAISON   *    00080005
      *    � DESTINATION DE COPERNIC                               *    00120005
      *    DATE DE CREAT. : 30/04/2008                             *    00110005
      *                                                            *    00120005
      *----------------------------------------------------------- *    00130005
0211  * DSA015 FEVRIER 2011 : AJOUT DE LA FILIALE ET DU MAG DE     *            
      *                       VENTE                                *            
      *----------------------------------------------------------- *    00130005
                                                                        00140005
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FMOIS     ASSIGN  TO  FMOIS.                                 
      *--                                                                       
           SELECT  FMOIS     ASSIGN  TO  FMOIS                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FTL111B   ASSIGN  TO  FTL111B.                               
      *--                                                                       
           SELECT  FTL111B   ASSIGN  TO  FTL111B                                
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT  FTL122    ASSIGN  TO  FTL122.                                
      *--                                                                       
           SELECT  FTL122    ASSIGN  TO  FTL122                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
                                                                        00391011
       FD  FTL111B                                                              
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENR-FTL111B PIC X(120).                                              
      *                                                                         
       FD   FMOIS                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FMOIS.                                                          
            03  FMOIS-MMSSAA.                                                   
                05  FMOIS-MM      PIC X(02).                                    
                05  FMOIS-SS      PIC X(02).                                    
                05  FMOIS-AA      PIC X(02).                                    
            03  FILLER            PIC X(74).                                    
      *                                                                         
       FD  FTL122                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENR-FTL122 PIC X(80).                                                
       WORKING-STORAGE SECTION.                                                 
       77  NCGFC-BTL15           PIC X.                                         
       77  NCGFC-TL112           PIC X.                                         
       77  BETDATC               PIC X(08) VALUE 'BETDATC '.                    
       77  W-QTERPI              PIC 9(07)        VALUE 0.                      
       77  W-LUS-FTL111B         PIC 9(07) COMP-3 VALUE ZERO.                   
       77  W-ECR-FTL122          PIC 9(07) COMP-3 VALUE ZERO.                   
      *                                                                         
       01   W-COPER-ENREG.                                                      
            05  COPER-NSOC         PIC X(03).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-PER          PIC X(04).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-NLIEU        PIC X(03).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-INDIC        PIC X(03).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-TEQUIP       PIC X(05).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-EQUIG        PIC X(05).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-COMPOEQ      PIC X(01).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-CRETOUR      PIC X(02).                                   
            05  FILLER             PIC X(01)   VALUE ';'.                       
            05  COPER-QTE          PIC 9(7).99.                                 
            05  FILLER             PIC X(01)   VALUE ';'.                       
0211        05  COPER-NSOCVTE      PIC X(03)   VALUE SPACES.                    
0211        05  FILLER             PIC X(01)   VALUE ';'.                       
0211        05  COPER-NLIEUVTE     PIC X(03)   VALUE SPACES.                    
0211        05  FILLER             PIC X(01)   VALUE ';'.                       
0211        05  FILLER             PIC X(21)   VALUE SPACES.                    
      *     05  FILLER             PIC X(29)   VALUE SPACES.                    
      *                                                                         
       01      ETAT-FTL111B      PIC X(01) VALUE '0'.                   00699509
           88  FIN-FTL111B                 VALUE '1'.                   00699709
       01  CODE-RETOUR           PIC X(01) VALUE '0'.                   00699509
           88  WRETOUR                     VALUE '1'.                   00699709
           88  WLIVRAISON                  VALUE '0'.                   00699809
      *                                                                         
      *----------------------------------------------------------------         
           COPY WORKDATC.                                                       
           COPY SWTL112.                                                        
           COPY ABENDCOP.                                                       
      ***************************************************************           
      *            P   R   O   C   E   D   U   R   E                *           
      ***************************************************************           
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
           PERFORM DEBUT                                                        
           PERFORM LECTURE-FTL111B.                                             
           PERFORM TRAIT-FTL111B UNTIL FIN-FTL111B                              
           PERFORM COMPTE-RENDU                                                 
           PERFORM FERMETURE-FICHIERS                                           
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *----------------------------------------------------------------         
       DEBUT                       SECTION.                                     
      *----------------------------------------------------------------         
           OPEN INPUT  FTL111B FMOIS                                            
                OUTPUT FTL122                                                   
      *                                                                         
           MOVE SPACE  TO FTL112-DSECT                                          
           READ FMOIS END                                                       
                MOVE 'PAS DE MOIS A TRAITER' TO ABEND-MESS                      
                PERFORM FIN-ANORMALE                                            
           END-READ                                                             
           STRING '01' FMOIS-MMSSAA                                             
                  DELIMITED SIZE INTO GFJJMMSSAA                                
           MOVE '1' TO GFDATA                                                   
           CALL BETDATC USING WORK-BETDATC                                      
           IF GFVDAT NOT = '1'                                                  
              MOVE 'FMOIS REJETE PAR BETDATC' TO ABEND-MESS                     
              PERFORM FIN-ANORMALE                                              
           END-IF                                                               
           .                                                                    
      *----------------------------------------------------------------         
       LECTURE-FTL111B SECTION.                                                 
      *----------------------------------------------------------------         
           READ FTL111B INTO    FTL112-DSECT                                    
                END     SET FIN-FTL111B TO TRUE                                 
                NOT END ADD 1 TO W-LUS-FTL111B                                  
           END-READ.                                                            
      *                                                                         
      *----------------------------------------------------------------         
       TRAIT-FTL111B SECTION.                                                   
      *----------------------------------------------------------------         
           MOVE FTL112-NSOC    TO COPER-NSOC                                    
0211                              COPER-NSOCVTE                                 
           MOVE FTL112-NDEPOT  TO COPER-NLIEU                                   
           MOVE FTL112-CEQUIP  TO COPER-TEQUIP                                  
           MOVE FTL112-CEQUIPE TO COPER-EQUIG                                   
           MOVE FTL112-COMPOS  TO COPER-COMPOEQ                                 
0211       MOVE FTL112-NMAG    TO COPER-NLIEUVTE                                
           MOVE FMOIS-AA       TO COPER-PER                                     
           MOVE FMOIS-MM       TO COPER-PER (3:)                                
           IF FTL112-CRETOUR NOT = SPACES AND LOW-VALUE                         
              SET WRETOUR TO TRUE                                               
              MOVE FTL112-CRETOUR TO COPER-CRETOUR                              
           END-IF                                                               
           IF FTL112-QBLDEP > 0                                                 
              IF WRETOUR                                                        
                 MOVE 'RBL' TO COPER-INDIC                                      
                 MOVE FTL112-QBLDEP TO COPER-QTE                                
                 WRITE ENR-FTL122 FROM W-COPER-ENREG                            
                 MOVE 'DBL' TO COPER-INDIC                                      
                 MOVE FTL112-QBLDEP TO COPER-QTE                                
                 MOVE SPACES TO COPER-CRETOUR                                   
                 WRITE ENR-FTL122 FROM W-COPER-ENREG                            
              ELSE                                                              
                 MOVE 'DBL' TO COPER-INDIC                                      
                 MOVE FTL112-QBLDEP TO COPER-QTE                                
                 WRITE ENR-FTL122 FROM W-COPER-ENREG                            
              END-IF                                                            
           END-IF                                                               
           IF  FTL112-QPIECESDEP > 0                                            
              IF  WRETOUR                                                       
                 MOVE 'RPI' TO COPER-INDIC                                      
                 COMPUTE W-QTERPI = FTL112-QPIECESRETIMP +                      
                                    FTL112-QPIECESRETNIMP                       
                 MOVE  W-QTERPI     TO COPER-QTE                                
                 MOVE FTL112-CRETOUR TO COPER-CRETOUR                           
                 WRITE ENR-FTL122 FROM W-COPER-ENREG                            
                 ADD 1 TO W-ECR-FTL122                                          
                 MOVE 'DPI' TO COPER-INDIC                                      
                 MOVE SPACES TO COPER-CRETOUR                                   
                 MOVE  FTL112-QPIECESDEP TO COPER-QTE                           
                 WRITE ENR-FTL122 FROM W-COPER-ENREG                            
                 ADD 1 TO W-ECR-FTL122                                          
              ELSE                                                              
                 MOVE 'DPI' TO COPER-INDIC                                      
                 MOVE  FTL112-QPIECESDEP TO COPER-QTE                           
                 WRITE ENR-FTL122 FROM W-COPER-ENREG                            
                 ADD 1 TO W-ECR-FTL122                                          
              END-IF                                                            
           END-IF                                                               
           IF FTL112-QEQUIP > 0                                                 
              MOVE 'NEQ' TO COPER-INDIC                                         
              MOVE  FTL112-QEQUIP     TO COPER-QTE                              
              MOVE SPACES TO COPER-CRETOUR                                      
              WRITE ENR-FTL122 FROM W-COPER-ENREG                               
              ADD 1 TO W-ECR-FTL122                                             
           END-IF                                                               
           PERFORM LECTURE-FTL111B.                                             
           SET WLIVRAISON TO TRUE                                               
           MOVE SPACES         TO COPER-NSOC                                    
                                  COPER-NLIEU                                   
                                  COPER-TEQUIP                                  
                                  COPER-EQUIG                                   
                                  COPER-COMPOEQ                                 
                                  COPER-PER                                     
                                  COPER-CRETOUR                                 
0211                              COPER-NSOCVTE                                 
0211                              COPER-NLIEUVTE                                
           .                                                                    
       FIN-TRAIT-FTL111B. EXIT.                                         00097100
      *                                                                         
      *                                                                         
      *                                                                         
      *----------------------------------------------------------------         
       COMPTE-RENDU SECTION.                                                    
      *----------------------------------------------------------------         
           DISPLAY  ' '                                                         
           DISPLAY  '*-------------------------------------------------'        
           DISPLAY  '*'                                                         
           DISPLAY  '  BTL122 : FIN NORMALE DU TRAITEMENT '                     
           DISPLAY  '*'                                                         
           DISPLAY  '*-------------------------------------------------'        
           DISPLAY  '*'                                                         
           DISPLAY  '* ' W-LUS-FTL111B ' LUS    DANS FICHIER FTL111B'           
           DISPLAY  '* ' W-ECR-FTL122 ' ECRITS DANS FICHIER FTL122'             
           DISPLAY  '*'                                                         
           DISPLAY  ' '.                                                        
       FIN-COMPTE-RENDU. EXIT.                                                  
      *                                                                         
      *                                                                         
       FERMETURE-FICHIERS SECTION.                                              
           CLOSE FTL111B FMOIS                                                  
                 FTL122                                                         
           .                                                                    
       FIN-FERMETURE-FICHIERS. EXIT.                                            
      *                                                                         
      *                                                                         
      *----------------------------------------------------------------         
       FIN-ANORMALE       SECTION.                                              
      *----------------------------------------------------------------         
           MOVE  'BTL122'     TO   ABEND-PROG.                                  
           CALL  'ABEND'   USING   ABEND-PROG  ABEND-MESS.                      
       FIN-FIN-ANORMALE.          EXIT.                                         
      *                                                                         
