      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
      *                                                                         
      *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*         
      *                                                               *         
      *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*         
      *                                                                         
       PROGRAM-ID.                  BRM035.                                     
       AUTHOR. DSA015.                                                          
      *                                                                         
      *****************************************************************         
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *         
      *****************************************************************         
      *                                                               *         
      *  PROJET     :                                                 *         
      *  PROGRAMME  : BRM035                                          *         
      *  CREATION   : 27 JUILLET 1999                                 *         
      *  PERIODICITE: ...                                             *         
      *                                                               *         
      *  FONCTION   : CE PROGRAMME  PERMET  L'EDITION DE  L'ETAT      *         
      *               D'ANALYSE DES STOCKS OBJECTIFS PAR SEGMENT      *         
      *               DETAIL PAR MAGASIN (IRM035).                    *         
      *                                                               *         
      *****************************************************************         
      *---------------------------------------------------------------*         
      *  DATE       : 18 02 2003  DSA022 NT                           *         
      *  OBJET      : CHGT POUR QUE LE PGM NE PLANTE PLUS QUAND       *         
      *               FICHIER FRM035 EST VIDE                         *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
      *SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.                            
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      * -AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * DESCRIPTION DES SELECTS DES FICHIERS SAM.                     *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
      *===============================================================*         
      * FICHIERS EN ENTREE:                                           *         
      *===============================================================*         
      * FDATE   : FICHIER PARAMETRE CONTENANT LA DATE DE TRAITEMENT.  *         
      *---------------------------------------------------------------*         
      * FNSOC   : FICHIER PARAMETRE CONTENANT LE NUMERO DE SOCIETE A  *         
      *         : TRAITER.                                            *         
      *---------------------------------------------------------------*         
      *---------------------------------------------------------------*         
      * FRM035  : FICHIER DES DONNEES A EDITER.                       *         
      *    COPY : IRM035.                                             *         
      *    LONG : 512 CAR.                                            *         
      *===============================================================*         
      * FICHIER  EN SORTIE:                                           *         
      *===============================================================*         
      * IRM035  : ETAT IRM035.                                        *         
      *    COPY : DESCRIPTION EN WORKING.                             *         
      *    LONG : 133 CAR.                                            *         
      *===============================================================*         
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IRM035  ASSIGN TO IRM035.                                     
      *--                                                                       
           SELECT IRM035  ASSIGN TO IRM035                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FRM035  ASSIGN TO FRM035.                                     
      *--                                                                       
           SELECT FRM035  ASSIGN TO FRM035                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      * ==============================================================*         
      *    D E F I N I T I O N   F I C H I E R S   P A R A M R T R E S*         
      * ==============================================================*         
       FD  FDATE                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FILE-DATE.                                                           
           03  FILLER      PIC X(80).                                           
      *===============================================================*         
      *             DEFINITION DE FRM035 (FICHIER ENTREE)             *         
      *===============================================================*         
       FD  FRM035                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD OMITTED.                                                
       01  FILE-FRM035.                                                         
           03  FILLER                     PIC X(512).                           
      *===============================================================*         
      *             DEFINITION DE IRM035 (FICHIER SORTIE)             *         
      *===============================================================*         
       FD  IRM035                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  IRM035-ENREG.                                                        
           03  FILLER      PIC X(132).                                          
      *---------------------------------------------------------------*         
      *==============================================================*          
      *****************************************************************         
      *                                                              *          
      *   W O R K I N G - S T O R A G E   S E C T I O N               *         
      *                                                              *          
      *****************************************************************         
      *==============================================================*          
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
      *===============================================================*         
      *              DESCRIPTION DE FRM035 (FICHIER EN ENTREE)        *         
      *===============================================================*         
           COPY IRM035.                                                         
      *===============================================================*         
      *              DESCRIPTION DE FDATE                             *         
      *===============================================================*         
       01  FDATE-ENREG.                                                         
           02 F-JOUR   PIC X(2).                                                
           02 F-MOIS   PIC X(2).                                                
           02 F-ANNEE  PIC X(4).                                                
           02  FILLER      PIC X(72).                                           
      *===============================================================*         
      *              LIGNES DE L'ETAT IRM035                          *         
      *===============================================================*         
       01  IRM035-LIGNES.                                                       
      *--------------------------- LIGNE E00  --------------------------        
           05  IRM035-E00.                                                      
               10  FILLER                         PIC X(58) VALUE               
           'IRM035                       ANALYSE DES STOCKS OBJECTIFS '.        
               10  FILLER                         PIC X(46) VALUE               
           'PAR SEGMENT                          EDITE LE '.                    
               10  IRM035-E00-DATEDITE            PIC X(10).                    
               10  FILLER                         PIC X(03) VALUE               
           ' A '.                                                               
               10  IRM035-E00-HEUREDITE           PIC 99.                       
               10  FILLER                         PIC X VALUE 'H'.              
               10  IRM035-E00-MINUEDITE           PIC 99.                       
               10  FILLER                         PIC X(06) VALUE               
           ' PAGE '.                                                            
               10  IRM035-E00-NOPAGE              PIC ZZZZ.                     
      *--------------------------- LIGNE E01  --------------------------        
           05  IRM035-E01.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                        DETAIL PAR MAGASIN'.        
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(16) VALUE               
           '                '.                                                  
      *--------------------------- LIGNE E08  --------------------------        
           05  IRM035-E08.                                                      
               10  FILLER                         PIC X(15) VALUE               
           'CHEF PRODUIT : '.                                                   
               10  IRM035-E08-CHEFPROD            PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IRM035-E08-LCHEFPROD           PIC X(20).                    
               10  FILLER                         PIC X(13) VALUE               
           '   FAMILLE : '.                                                     
               10  IRM035-E08-LFAM                PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
      *--------------------------- LIGNE E09  --------------------------        
           05  IRM035-E09.                                                      
               10  FILLER                         PIC X(10) VALUE               
           'SEGMENT : '.                                                        
               10  IRM035-E09-LAGREGATED          PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(44) VALUE               
           '                                            '.                      
      *--------------------------- LIGNE E10  --------------------------        
           05  IRM035-E10.                                                      
               10  FILLER                         PIC X(10) VALUE               
           'MAGASIN : '.                                                        
               10  IRM035-E10-NSOCIETE            PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IRM035-E10-NLIEU               PIC X(03).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IRM035-E10-LMAG                PIC X(20).                    
               10  FILLER                         PIC X(58) VALUE               
           '                                                          '.        
               10  FILLER                         PIC X(36) VALUE               
           '                                    '.                              
      *--------------------------- LIGNE E15  --------------------------        
           05  IRM035-E15.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '-----------------------------------+---------------+------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------------+------------------+---------+-'.        
               10  FILLER                         PIC X(16) VALUE               
           '------+---------'.                                                  
      *--------------------------- LIGNE E25  --------------------------        
           05  IRM035-E25.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '                                   ! VENTES EMP.8S ! STOCK'.        
               10  FILLER                         PIC X(58) VALUE               
           'S EN COURS  STOCK CALCULES !  ASSOR      QTE  ! STOCK + ! '.        
               10  FILLER                         PIC X(16) VALUE               
           'INDIS.!  INDIS. '.                                                  
      *--------------------------- LIGNE E30  --------------------------        
           05  IRM035-E30.                                                      
               10  FILLER                         PIC X(58) VALUE               
           ' CODIC   MQE       REFERENCE       ! REEL.  PREV.  !  OBJ '.        
               10  FILLER                         PIC X(58) VALUE               
           '    MAX      OBJ     MAX   ! STD MAG  EXPO LS ! MUT ATT ! '.        
               10  FILLER                         PIC X(16) VALUE               
           ' MAG  ! ENTREPOT'.                                                  
      *--------------------------- LIGNE E35  --------------------------        
           05  IRM035-E35.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '-----------------------------------+---------------+------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------------+------------------+---------+-'.        
               10  FILLER                         PIC X(16) VALUE               
           '------+---------'.                                                  
      *--------------------------- LIGNE D05  --------------------------        
           05  IRM035-D05.                                                      
               10  IRM035-D05-NCODIC              PIC X(07).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IRM035-D05-CMARQ               PIC X(05).                    
               10  FILLER                         PIC X(01) VALUE               
           ' '.                                                                 
               10  IRM035-D05-LREFFOURN           PIC X(20).                    
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IRM035-D05-QV8SR               PIC ZZZZZ.                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IRM035-D05-QV8SC               PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  IRM035-D05-QSOS                PIC ZZZZZ.                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-D05-QSMS                PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-D05-QSO                 PIC ZZZZZ.                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-D05-QSM                 PIC ZZZZZ.                    
               10  FILLER                         PIC X(05) VALUE               
           '  !  '.                                                             
               10  IRM035-D05-WASSORTSTD          PIC X(01).                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-D05-WASSORTMAG          PIC X(01).                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-D05-QEXPO               PIC Z.                        
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-D05-QLS                 PIC Z.                        
               10  FILLER                         PIC X(04) VALUE               
           '  ! '.                                                              
               10  IRM035-D05-QSTOCK              PIC ZZZZZZZ.                  
               10  FILLER                         PIC X(03) VALUE               
           ' ! '.                                                               
               10  IRM035-D05-QINDCOD                                           
                   PIC ZZ9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(04) VALUE               
           ' !  '.                                                              
               10  IRM035-D05-QINDENT                                           
                   PIC ZZ9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
      *--------------------------- LIGNE T04  --------------------------        
           05  IRM035-T04.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '-----------------------------------+---------------+------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------------+------------------+---------+-'.        
               10  FILLER                         PIC X(16) VALUE               
           '------+---------'.                                                  
      *--------------------------- LIGNE T05  --------------------------        
           05  IRM035-T05.                                                      
               10  FILLER                         PIC X(31) VALUE               
           '                         TOTAL '.                                   
               10  IRM035-T05-AFF8020             PIC ZZ.                       
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T05-QV8SR8020           PIC ZZZZZ.                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IRM035-T05-QV8SC8020           PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T05-QSOS8020            PIC ZZZZZ.                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-T05-QSMS8020            PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T05-QSO8020             PIC ZZZZZ.                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-T05-QSM8020             PIC ZZZZZ.                    
               10  FILLER                         PIC X(23) VALUE               
           '                       '.                                           
               10  IRM035-T05-QSTOCK8020          PIC ZZZZZZZ.                  
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-T05-QINDCOD8020                                       
                   PIC ZZ9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T05-QINDENT8020                                       
                   PIC ZZ9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
      *--------------------------- LIGNE T06  --------------------------        
           05  IRM035-T06.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '-----------------------------------+---------------+------'.        
               10  FILLER                         PIC X(58) VALUE               
           '---------------------------+------------------+---------+-'.        
               10  FILLER                         PIC X(16) VALUE               
           '------+---------'.                                                  
      *--------------------------- LIGNE T10  --------------------------        
           05  IRM035-T10.                                                      
               10  FILLER                         PIC X(37) VALUE               
           '                         TOTAL       '.                             
               10  IRM035-T10-QV8SRSEG            PIC ZZZZZ.                    
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
               10  IRM035-T10-QV8SCSEG            PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T10-QSOSSEG             PIC ZZZZZ.                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-T10-QSMSSEG             PIC ZZZZZ.                    
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T10-QSOSEG              PIC ZZZZZ.                    
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-T10-QSMSEG              PIC ZZZZZ.                    
               10  FILLER                         PIC X(23) VALUE               
           '                       '.                                           
               10  IRM035-T10-QSTOCKEG            PIC ZZZZZZZ.                  
               10  FILLER                         PIC X(03) VALUE               
           '   '.                                                               
               10  IRM035-T10-QINDCODEG                                         
                   PIC ZZ9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(04) VALUE               
           '    '.                                                              
               10  IRM035-T10-QINDENTEG                                         
                   PIC ZZ9,9                          BLANK WHEN ZERO.          
               10  FILLER                         PIC X(02) VALUE               
           '  '.                                                                
      *--------------------------- LIGNE B05  --------------------------        
           05  IRM035-B05.                                                      
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(58) VALUE               
           '----------------------------------------------------------'.        
               10  FILLER                         PIC X(16) VALUE               
           '----------------'.                                                  
      *===============================================================*         
      *        VARIABLES DE TOTAUX                                    *         
      *===============================================================*         
      *                                                                         
       01 TOTAL-8020.                                                           
          05 W-QINDCOD-8020     PIC S9(3)V9(4) COMP-3 VALUE +0.                 
          05 W-QINDENT-8020     PIC S9(3)V9(4) COMP-3 VALUE +0.                 
          05 W-QV8SR-8020       PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QV8SC-8020       PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSOS-8020        PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSMS-8020        PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSO-8020         PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSM-8020         PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSTOCK-8020      PIC S9(7) COMP-3 VALUE +0.                      
      *                                                                         
       01 TOTAL-SEG.                                                            
          05 W-QINDCOD-SEG      PIC S9(3)V9(4) COMP-3 VALUE +0.                 
          05 W-QINDENT-SEG      PIC S9(3)V9(4) COMP-3 VALUE +0.                 
          05 W-QV8SR-SEG        PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QV8SC-SEG        PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSOS-SEG         PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSMS-SEG         PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSO-SEG          PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSM-SEG          PIC S9(7) COMP-3 VALUE +0.                      
          05 W-QSTOCK-SEG       PIC S9(7) COMP-3 VALUE +0.                      
      *                                                                         
      *===============================================================*         
      *                 VARIABLES DIVERSES                            *         
      *===============================================================*         
       01  W-TIME.                                                              
           05 HH                PIC XX VALUE SPACE.                             
           05 MM                PIC XX VALUE SPACE.                             
           05 SS                PIC XX VALUE SPACE.                             
      *                                                                         
       01  W-DATE-ED            PIC X(10).                                      
      *                                                                         
       01  W-NOLIGNE                 PIC 9(2).                                  
       01  W-NOLIGNE-INIT            PIC 9(2) VALUE 9.                          
       01  W-NBLIGNE-MAX             PIC 9(2) VALUE 56.                         
       01  W-PAGE                    PIC 9(4) VALUE 0.                          
       01  WK-SOCIETE                PIC X(3).                                  
       01  W-CALCUL                  PIC S9(7)V9(2).                            
       01  W-DATE-TRAITEMENT         PIC X(8).                                  
       01  W-SAUVE-CHEFPROD          PIC X(5).                                  
       01  W-SAUVE-CFAM              PIC X(5).                                  
       01  W-SAUVE-LAGREGATED        PIC X(20).                                 
       01  W-SAUVE-CGROUP            PIC X(5).                                  
       01  W-SAUVE-NSOCIETE          PIC X(3).                                  
       01  W-SAUVE-NLIEU             PIC X(3).                                  
       01  W-SAUVE-W8020-RUP         PIC X(1).                                  
       01  W-SAUVE-QRANG             PIC S9(5) COMP-3.                          
       01  W-SAUVE-AFF8020           PIC X(2).                                  
       01  W-FRM035-LUS          PIC S9(9) COMP-3  VALUE +0.                    
       01  W-RTAN00-ECRITS       PIC S9(9) COMP-3  VALUE +0.                    
       01  W-IRM035-ECRITS       PIC S9(9) COMP-3  VALUE +0.                    
      *                                                                         
       01  ETAT-FRM035                    PIC X.                                
           88  FRM035-FINI               VALUE '0'.                             
           88  FRM035-NON-FINI           VALUE '1'.                             
           COPY ABENDCOP.                                                       
           COPY WORKDATC.                                                       
      *****************************************************************         
      *                                                               *         
      *             P R O C E D U R E   D I V I S I O N               *         
      *                                                               *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
       MODULE-BRM035 SECTION.                                                   
      *****************************************************************         
      *                                                                         
           PERFORM DEBUT-BRM035.                                                
           PERFORM TRAITEMENT UNTIL FRM035-FINI                                 
           WRITE IRM035-ENREG FROM IRM035-B05                                   
           PERFORM  FIN-BRM035.                                                 
      *                                                                         
       FIN-MODULE-BRM035. EXIT.                                                 
      *===============================================================*         
      *===============================================================*         
      *                     D E B U T                                 *         
      *===============================================================*         
      *===============================================================*         
       DEBUT-BRM035 SECTION.                                                    
      *                                                                         
           DISPLAY  '************************************************'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '**   PROGRAMME  :  BRM035                     **'.         
           DISPLAY  '**   EDITION IRM035                           **'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '************************************************'.         
      D    DISPLAY  '***     PROGRAMME DEBUTE NORMALEMENT         ***'.         
           OPEN  INPUT  FRM035.                                                 
      D    DISPLAY '  OPEN  FICHIER FRM035'.                                    
           OPEN OUTPUT  IRM035.                                                 
      D    DISPLAY '  OPEN  FICHIER IRM035'.                                    
           PERFORM RECUP-DATE.                                                  
           ACCEPT W-TIME FROM TIME                                              
           SET FRM035-NON-FINI TO TRUE.                                         
           PERFORM LIRE-FRM035.                                                 
           IF FRM035-NON-FINI                                                   
               IF DSECT-IRM035 = LOW-VALUES                                     
                   DISPLAY '*** 1ER ENREG DE FRM035 = LOW-VALUES  ***'          
                   DISPLAY '*** DONC AUCUN CODIC ECRIT PAR BRM035 ***'          
                   PERFORM FIN-BRM035                                           
               END-IF                                                           
           ELSE                                                                 
               MOVE '*** FICHIER FRM035 VIDE ' TO ABEND-MESS                    
               MOVE 4 TO ABEND-CODE                                             
NT    *        GO TO FIN-ANORMALE                                               
NT             GO TO FIN-DEBUT-BRM035                                           
           END-IF.                                                              
      *                                                                         
           PERFORM ECRIRE-ENTETE-PAGE.                                          
      *                                                                         
       FIN-DEBUT-BRM035. EXIT.                                                  
      *                                                                         
      *****************************************************************         
       RECUP-DATE SECTION.                                                      
      *****************************************************************         
      *                                                                         
           OPEN INPUT FDATE.                                                    
           READ   FDATE  INTO   FDATE-ENREG AT END                              
                MOVE  '*** CARTE PARAMETRE DATE ABSENTE '                       
                      TO   ABEND-MESS                                           
                MOVE 1 TO   ABEND-CODE                                          
                CLOSE FDATE                                                     
                GO TO FIN-ANORMALE                                              
           END-READ.                                                            
           MOVE FDATE-ENREG TO GFJJMMSSAA.                                      
           MOVE '1' TO GFDATA.                                                  
           CALL 'BETDATC' USING WORK-BETDATC.                                   
           IF GFVDAT NOT = '1'                                                  
               MOVE 'PB DATE (FDATE)' TO ABEND-MESS                             
               MOVE '8' TO ABEND-MESS                                           
               GO TO FIN-ANORMALE                                               
           ELSE                                                                 
               MOVE GFJMSA-5          TO W-DATE-ED                              
           END-IF.                                                              
      D    DISPLAY 'ENTETE-DATE-ED'   ENTETE-DATE-ED.                           
           DISPLAY 'FDATE       : ' FDATE-ENREG.                                
           CLOSE FDATE.                                                         
      *                                                                         
       FIN-RECUP-DATE. EXIT.                                                    
      *                                                                         
      *****************************************************************         
       TRAITEMENT SECTION.                                                      
      *****************************************************************         
      *                                                                         
           MOVE IRM035-CHEFPROD    TO W-SAUVE-CHEFPROD                          
           MOVE IRM035-CFAM        TO W-SAUVE-CFAM                              
           MOVE IRM035-LAGREGATED  TO W-SAUVE-LAGREGATED                        
           MOVE IRM035-CGROUP      TO W-SAUVE-CGROUP                            
           MOVE IRM035-NSOCIETE    TO W-SAUVE-NSOCIETE                          
           MOVE IRM035-NLIEU       TO W-SAUVE-NLIEU                             
           PERFORM EDIT-MAG UNTIL FRM035-FINI                                   
               OR IRM035-CHEFPROD NOT = W-SAUVE-CHEFPROD                        
               OR IRM035-CFAM NOT= W-SAUVE-CFAM                                 
               OR IRM035-LAGREGATED NOT = W-SAUVE-LAGREGATED                    
               OR IRM035-CGROUP NOT = W-SAUVE-CGROUP                            
               OR IRM035-NSOCIETE NOT = W-SAUVE-NSOCIETE                        
               OR IRM035-NLIEU NOT = W-SAUVE-NLIEU                              
      *                                                                         
           MOVE W-QV8SR-SEG    TO IRM035-T10-QV8SRSEG                           
           MOVE W-QV8SC-SEG    TO IRM035-T10-QV8SCSEG                           
           MOVE W-QSOS-SEG     TO IRM035-T10-QSOSSEG                            
           MOVE W-QSMS-SEG     TO IRM035-T10-QSMSSEG                            
           MOVE W-QSO-SEG      TO IRM035-T10-QSOSEG                             
           MOVE W-QSM-SEG      TO IRM035-T10-QSMSEG                             
           MOVE W-QSTOCK-SEG   TO IRM035-T10-QSTOCKEG                           
      *    MOVE W-QINDCOD-SEG  TO IRM035-T10-QINDCODEG                          
           COMPUTE IRM035-T10-QINDCODEG ROUNDED = W-QINDCOD-SEG                 
      *    MOVE W-QINDENT-SEG  TO IRM035-T10-QINDENTEG                          
           COMPUTE IRM035-T10-QINDENTEG ROUNDED = W-QINDENT-SEG                 
           WRITE IRM035-ENREG FROM IRM035-T10                                   
           INITIALIZE TOTAL-SEG                                                 
           ADD 1 TO W-NOLIGNE W-IRM035-ECRITS                                   
           IF FRM035-NON-FINI                                                   
              PERFORM ECRIRE-ENTETE-PAGE                                        
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT. EXIT.                                                    
      *                                                                         
      *****************************************************************         
       EDIT-MAG SECTION.                                                        
      *****************************************************************         
      *                                                                         
           MOVE IRM035-W8020-RUP   TO W-SAUVE-W8020-RUP                         
           MOVE IRM035-AFF8020     TO W-SAUVE-AFF8020                           
           PERFORM EDIT-CODIC UNTIL FRM035-FINI                                 
               OR IRM035-CHEFPROD NOT = W-SAUVE-CHEFPROD                        
               OR IRM035-CFAM NOT= W-SAUVE-CFAM                                 
               OR IRM035-LAGREGATED NOT = W-SAUVE-LAGREGATED                    
               OR IRM035-CGROUP NOT = W-SAUVE-CGROUP                            
               OR IRM035-NSOCIETE NOT = W-SAUVE-NSOCIETE                        
               OR IRM035-NLIEU NOT = W-SAUVE-NLIEU                              
               OR IRM035-W8020-RUP NOT = W-SAUVE-W8020-RUP.                     
      *                                                                         
           MOVE W-SAUVE-AFF8020      TO IRM035-T05-AFF8020                      
           MOVE W-QV8SR-8020   TO IRM035-T05-QV8SR8020                          
           MOVE W-QV8SC-8020   TO IRM035-T05-QV8SC8020                          
           MOVE W-QSOS-8020    TO IRM035-T05-QSOS8020                           
           MOVE W-QSMS-8020    TO IRM035-T05-QSMS8020                           
           MOVE W-QSO-8020     TO IRM035-T05-QSO8020                            
           MOVE W-QSM-8020     TO IRM035-T05-QSM8020                            
           MOVE W-QSTOCK-8020  TO IRM035-T05-QSTOCK8020                         
      *    MOVE W-QINDCOD-8020 TO IRM035-T05-QINDCOD8020                        
           COMPUTE IRM035-T05-QINDCOD8020 ROUNDED = W-QINDCOD-8020              
      *    MOVE W-QINDENT-8020 TO IRM035-T05-QINDENT8020                        
           COMPUTE IRM035-T05-QINDENT8020 ROUNDED = W-QINDENT-8020              
           WRITE IRM035-ENREG FROM IRM035-T04                                   
           WRITE IRM035-ENREG FROM IRM035-T05                                   
           WRITE IRM035-ENREG FROM IRM035-T06                                   
           INITIALIZE TOTAL-8020                                                
           ADD 3 TO W-NOLIGNE W-IRM035-ECRITS.                                  
      *                                                                         
       FIN-EDIT-MAG. EXIT.                                                      
      *                                                                         
      *****************************************************************         
       EDIT-CODIC SECTION.                                                      
      *****************************************************************         
      *                                                                         
           MOVE IRM035-NCODIC      TO IRM035-D05-NCODIC                         
           MOVE IRM035-CMARQ       TO IRM035-D05-CMARQ                          
           MOVE IRM035-LREFFOURN   TO IRM035-D05-LREFFOURN                      
           MOVE IRM035-QV8SR       TO IRM035-D05-QV8SR                          
           MOVE IRM035-QV8SC       TO IRM035-D05-QV8SC                          
           MOVE IRM035-QSOS        TO IRM035-D05-QSOS                           
           MOVE IRM035-QSMS        TO IRM035-D05-QSMS                           
           MOVE IRM035-QSO         TO IRM035-D05-QSO                            
           MOVE IRM035-QSM         TO IRM035-D05-QSM                            
           MOVE IRM035-WASSORTSTD  TO IRM035-D05-WASSORTSTD                     
           MOVE IRM035-WASSORTMAG  TO IRM035-D05-WASSORTMAG                     
           MOVE IRM035-QEXPO       TO IRM035-D05-QEXPO                          
           MOVE IRM035-QLS         TO IRM035-D05-QLS                            
           MOVE IRM035-QSTOCK      TO IRM035-D05-QSTOCK                         
           MOVE IRM035-QINDCOD     TO IRM035-D05-QINDCOD                        
           MOVE IRM035-QINDENT     TO IRM035-D05-QINDENT                        
           WRITE IRM035-ENREG FROM IRM035-D05                                   
           ADD 1 TO W-NOLIGNE W-IRM035-ECRITS                                   
           ADD IRM035-QIND-SEG-MAG TO W-QINDCOD-8020 W-QINDCOD-SEG              
           ADD IRM035-QIND-SEG-ENT TO W-QINDENT-8020 W-QINDENT-SEG              
           ADD IRM035-QV8SR        TO W-QV8SR-8020 W-QV8SR-SEG                  
           ADD IRM035-QV8SC        TO W-QV8SC-8020 W-QV8SC-SEG                  
           ADD IRM035-QSOS-MAG     TO W-QSOS-8020 W-QSOS-SEG                    
           ADD IRM035-QSMS-MAG     TO W-QSMS-8020 W-QSMS-SEG                    
           ADD IRM035-QSO-MAG      TO W-QSO-8020 W-QSO-SEG                      
           ADD IRM035-QSM-MAG      TO W-QSM-8020 W-QSM-SEG                      
           ADD IRM035-QSTOCK       TO W-QSTOCK-8020 W-QSTOCK-SEG                
           PERFORM LIRE-FRM035                                                  
           IF FRM035-NON-FINI AND (W-NOLIGNE >= 56)                             
              AND IRM035-CHEFPROD = W-SAUVE-CHEFPROD                            
              AND IRM035-CFAM = W-SAUVE-CFAM                                    
              AND IRM035-LAGREGATED  = W-SAUVE-LAGREGATED                       
              AND IRM035-CGROUP = W-SAUVE-CGROUP                                
              AND IRM035-NSOCIETE = W-SAUVE-NSOCIETE                            
              AND IRM035-NLIEU = W-SAUVE-NLIEU                                  
              PERFORM ECRIRE-ENTETE-PAGE                                        
           END-IF.                                                              
      *                                                                         
       FIN-EDIT-CODIC. EXIT.                                                    
      *                                                                         
      *****************************************************************         
       ECRIRE-ENTETE-PAGE SECTION.                                              
      *****************************************************************         
      *                                                                         
           IF W-PAGE NOT = 0                                                    
              WRITE IRM035-ENREG FROM IRM035-B05                                
              ADD 1 TO W-IRM035-ECRITS                                          
           END-IF.                                                              
           ADD 1 TO W-PAGE.                                                     
           MOVE W-PAGE             TO IRM035-E00-NOPAGE                         
           MOVE W-DATE-ED          TO IRM035-E00-DATEDITE                       
           MOVE HH OF W-TIME       TO IRM035-E00-HEUREDITE                      
           MOVE MM OF W-TIME       TO IRM035-E00-MINUEDITE                      
           MOVE IRM035-CHEFPROD    TO IRM035-E08-CHEFPROD                       
           MOVE IRM035-LCHEFPROD   TO IRM035-E08-LCHEFPROD                      
           MOVE IRM035-LFAM        TO IRM035-E08-LFAM                           
           MOVE IRM035-LAGREGATED  TO IRM035-E09-LAGREGATED                     
           MOVE IRM035-NSOCIETE    TO IRM035-E10-NSOCIETE                       
           MOVE IRM035-NLIEU       TO IRM035-E10-NLIEU                          
           MOVE IRM035-LMAG        TO IRM035-E10-LMAG                           
           MOVE W-NOLIGNE-INIT TO W-NOLIGNE.                                    
           WRITE IRM035-ENREG FROM IRM035-E00 AFTER PAGE.                       
           WRITE IRM035-ENREG FROM IRM035-E01.                                  
           WRITE IRM035-ENREG FROM IRM035-E08.                                  
           IF IRM035-WE9 NOT = SPACE                                            
              WRITE IRM035-ENREG FROM IRM035-E09                                
           ELSE                                                                 
              SUBTRACT 1 FROM W-NOLIGNE                                         
           END-IF                                                               
           WRITE IRM035-ENREG FROM IRM035-E10.                                  
           WRITE IRM035-ENREG FROM IRM035-E15 AFTER 2 LINES.                    
           WRITE IRM035-ENREG FROM IRM035-E25.                                  
           WRITE IRM035-ENREG FROM IRM035-E30.                                  
           WRITE IRM035-ENREG FROM IRM035-E35.                                  
           ADD W-NOLIGNE TO W-IRM035-ECRITS.                                    
      *                                                                         
       FIN-ECRIRE-ENTETE-PAGE. EXIT.                                            
      *                                                                         
      *****************************************************************         
       LIRE-FRM035 SECTION.                                                     
      *****************************************************************         
           READ   FRM035 INTO  DSECT-IRM035 AT END                              
                SET FRM035-FINI TO TRUE.                                        
           IF FRM035-NON-FINI                                                   
                ADD 1 TO W-FRM035-LUS                                           
           END-IF.                                                              
      *                                                                         
       FIN-LIRE-FRM035. EXIT.                                                   
      *                                                                         
      *============================================================*            
      *    FIN NORMALE DU PROGRAMME                                *            
      *============================================================*            
       FIN-BRM035 SECTION.                                                      
           DISPLAY  '************************************************'.         
           DISPLAY  '***       FIN  NORMALE  DU PROGRAMME         ***'.         
           DISPLAY  '***                 BRM035                   ***'.         
           DISPLAY  '************************************************'.         
           DISPLAY  '** NBR DE LIGNES FRM035 LUES          : '                  
                    W-FRM035-LUS.                                               
           DISPLAY  '** NBR DE LIGNES IRM035 ECRITES       : '                  
                    W-IRM035-ECRITS                                             
           CLOSE  FRM035.                                                       
           CLOSE  IRM035.                                                       
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BRM035. EXIT.                                                    
      *=============================================================*           
      *   F I N  A N O R M A L E                                    *           
      *=============================================================*           
       FIN-ANORMALE SECTION.                                                    
           CLOSE  FRM035.                                                       
           CLOSE  IRM035.                                                       
           DISPLAY '***************************************'.                   
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.                   
           DISPLAY '***************************************'.                   
           MOVE  'BRM035'  TO    ABEND-PROG.                                    
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
       FIN-FIN-ANORMALE. EXIT.                                                  
