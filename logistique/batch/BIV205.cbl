      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.            BIV205.                                   00020001
       AUTHOR.      JOHANN.                                             00030001
      *                                                                 00040000
      * ============================================================= * 00050000
      *                                                               * 00060000
      *       PROJET        :  EDITION DES ETIQUETTES                 * 00070001
      *       PROGRAMME     :  BIV205.                                * 00080001
      *       PERIODICITE   :  EDITION A LA DEMANDE                   * 00090001
      *       FONCTION      :  CREATION DES ETIQUETTES                * 00100001
      *                        A PARTIR DES EDITIONS PRE-INVENTAIRES  * 00110001
      *       DATE CREATION :  15/12/2003.                            * 00120001
      *                                                               * 00130000
      * ============================================================= * 00140000
      *                                                                 00150000
       ENVIRONMENT DIVISION.                                            00160000
       CONFIGURATION SECTION.                                           00170000
       SPECIAL-NAMES.                                                   00180000
           DECIMAL-POINT IS COMMA.                                      00190000
       INPUT-OUTPUT SECTION.                                            00200000
      *                                                                 00210000
       FILE-CONTROL.                                                    00220000
      *                                                                 00230000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FIV201        ASSIGN  TO  FIV201.                   00240002
      *--                                                                       
            SELECT  FIV201        ASSIGN  TO  FIV201                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  IIV205        ASSIGN  TO  IIV205.                   00250001
      *--                                                                       
            SELECT  IIV205        ASSIGN  TO  IIV205                            
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FDATE         ASSIGN  TO  FDATE.                    00260001
      *--                                                                       
            SELECT  FDATE         ASSIGN  TO  FDATE                             
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00270000
                                                                        00280000
      *                                                                 00290000
       DATA DIVISION.                                                   00300000
       FILE SECTION.                                                    00310000
      *                                                                 00320000
      * ============================================================= * 00330000
      *  D E F I N I T I O N    F I C H I E R    E X T R A C T I O N  * 00340000
      * ============================================================= * 00350000
      *                                                                 00360000
       FD   FIV201                                                      00370002
            RECORDING F                                                 00380000
            BLOCK 0 RECORDS                                             00390000
            LABEL RECORD STANDARD.                                      00400000
                                                                        00410000
       01  FIV201-RECORD.                                               00420002
           03   FIV201-CLEF.                                            00430002
                05   FIV201-TYPE          PIC  X(3).                    00440002
                05   FIV201-LIEU          PIC  X(3).                    00450002
                05   FIV201-MODSTOCK      PIC  X(5).                    00460002
                05   FIV201-EMPL12        PIC  X(5).                    00470002
                05   FIV201-SEQFAM        PIC  9(5) COMP-3.             00480002
           03   FIV201-MARQUE             PIC  X(5).                    00490002
           03   FIV201-NCODIC             PIC  X(07).                   00500002
           03   FIV201-CFAM               PIC  X(5).                    00510002
           03   FIV201-LIBFAM             PIC  X(20).                   00520002
           03   FIV201-REFERENCE          PIC  X(20).                   00530002
           03   FIV201-NBETIQ             PIC  9(5) COMP-3.             00540002
           03   FIV201-DATEINV            PIC  X(8).                    00550002
           03   FIV201-EMPL3              PIC  X(3).                    00560002
           03   FIV201-NEAN               PIC  X(13).                   00570003
           03   FILLER                    PIC  X(17).                   00580002
      *                                                                 00590000
       FD   IIV205                                                      00600001
            RECORDING F                                                 00610001
            BLOCK 0 RECORDS                                             00620001
            LABEL RECORD STANDARD.                                      00630001
                                                                        00640001
       01  IIV205-RECORD.                                               00650001
           03   IIV205-LIGNE              PIC  X(133).                  00660001
      *                                                                 00670000
       FD   FDATE                                                       00680001
            RECORDING F                                                 00690001
            BLOCK 0 RECORDS                                             00700001
            LABEL RECORD STANDARD.                                      00710001
                                                                        00720001
       01  FILE-DATE.                                                   00730001
           03  F-DATE.                                                  00740001
               06   JJ     PIC X(2).                                    00750001
               06   MM     PIC X(2).                                    00760001
               06   SS     PIC X(2).                                    00770001
               06   AA     PIC X(2).                                    00780001
           03  FILLER PIC X(72).                                        00790001
      * ============================================================= * 00800000
      *                 W O R K I N G  -  S T O R A G E               * 00810000
      * ============================================================= * 00820000
      *                                                                 00830000
       WORKING-STORAGE SECTION.                                         00840000
      *                                                                 00850000
       77  W-MODSTOCK              PIC  X(05) VALUE SPACE.              00860002
       77  W-MAG                   PIC  X(03) VALUE SPACE.              00870001
       77  W-EMPL12                PIC  X(05).                          00880002
       77  W-SEQFAM                PIC  9(5) COMP-3.                    00890001
       77  W-NBETIQ                PIC  9(5) COMP-3.                    00900001
      *                                                                 00910000
       77  I-COL                  PIC  9.                               00920001
       77  I-LIGNE                PIC  9.                               00930001
      *                                                                 00940000
      *                                                                 00950000
       77  NB-FAM-MAX             PIC  9(5) COMP-3 VALUE 500.           00960000
       77  NB-FAM                 PIC  9(5) COMP-3.                     00970000
       77  I-FAM                  PIC  9(5) COMP-3.                     00980000
       77  IND1                   PIC  9(5) COMP-3.                     00990000
       77  I                      PIC  9(5) COMP-3.                     01000000
      *                                                                 01010000
       01  W-MARQ-EAN.                                                  01020001
           05  W-MARQ    PIC X(6).                                      01030001
           05  W-NEAN    PIC X(14).                                     01040001
      *                                                                 01050000
       01  ZONE-JOU.                                                    01060000
           05  Z-DATE    PIC X(8).                                      01070000
           05  FILLER    PIC X  VALUE '%'.                              01080000
      *                                                                 01090000
       01  DATE-JOU.                                                    01100000
           05  DATE-SS   PIC 99.                                        01110000
           05  DATE-AN   PIC 99.                                        01120000
           05  DATE-MO   PIC 99.                                        01130000
           05  DATE-JO   PIC 99.                                        01140000
      *                                                                 01150000
       01  W-INVDATE.                                                   01160001
           05  INV-SS    PIC XX.                                        01170001
           05  INV-AA    PIC XX.                                        01180001
           05  INV-MM    PIC XX.                                        01190001
           05  INV-JJ    PIC XX.                                        01200001
                                                                        01210001
       01  W-FORMAT-DATE.                                               01220001
           05  FORMAT-JJ    PIC XX.                                     01230001
           05  FORMAT1      PIC X VALUE '/'.                            01240001
           05  FORMAT-MM    PIC XX.                                     01250001
           05  FORMAT2      PIC X VALUE '/'.                            01260001
           05  FORMAT-AA    PIC XX.                                     01270001
       01  SYS-DATE.                                                    01280000
           03  SYS-SS              PIC  X(02).                          01290000
           03  SYS-AA              PIC  X(02).                          01300000
           03  SYS-MM              PIC  X(02).                          01310000
           03  SYS-JJ              PIC  X(02).                          01320000
      *                                                                 01330000
       01  EDT-DATE.                                                    01340000
           03  EDT-JJ              PIC  X(02).                          01350000
           03  FILLER              PIC  X(01)  VALUE '/'.               01360000
           03  EDT-MM              PIC  X(02).                          01370000
           03  FILLER              PIC  X(01)  VALUE '/'.               01380000
           03  EDT-SS              PIC  X(02).                          01390000
           03  EDT-AA              PIC  X(02).                          01400000
      *                                                                 01410000
       01  W-CMODDEL.                                                   01420000
           03  W-MOD1              PIC X(01).                           01430000
           03  W-MOD2              PIC X(02).                           01440000
      *                                                                 01450000
       01  FIN-FICHIER         PIC X  VALUE  '0'.                       01460001
      *                                                                 01470000
      *                                                                 01480000
      *                                                                 01490000
       01  CODE-RETOUR             PIC  X(01)  VALUE  '0'.              01500000
           88  TROUVE              VALUE  '0'.                          01510000
           88  NORMAL              VALUE  '0'.                          01520000
           88  NON-TROUVE          VALUE  '1'.                          01530000
           88  ANORMAL             VALUE  '1'.                          01540000
           88  EXISTE-DEJA         VALUE  '2'.                          01550000
           88  DOUBLE              VALUE  '7'.                          01560000
      *                                                                 01570000
       01  W-FIV201-LUES         PIC  S9(09)  COMP-3  VALUE  +0.        01580002
       01  W-IIV205-ECRITS       PIC  S9(09)  COMP-3  VALUE  +0.        01590001
      *                                                                 01600000
      ****************************************************************  01610001
      *                   DEFINITION DES LIGNES ETATS                *  01620001
      ****************************************************************  01630001
                                                                        01640001
       01  SAUT-PAGE.                                                   01650001
           05  SAUTPG        PIC X VALUE '1'.                           01660001
       01  SAUT-2LIGNES.                                                01670001
           05  SAUTLG2       PIC X VALUE '-'.                           01680001
       01  SAUT-LIGNE.                                                  01690001
           05  SAUTLG1       PIC X VALUE SPACES.                        01700001
                                                                        01710001
       01  LIGNE-ETAT1.                                                 01720001
           05  W-SAUT            PIC  X  VALUE '-'.                     01730001
           05  POSTE-FAM OCCURS 4 .                                     01740001
               10  W-EMPLACEMENT     PIC  X(05).                        01750001
               10  W-LIB1            PIC  X(05).                        01760001
               10  W-LIB-FAM         PIC  X(20).                        01770001
               10  W-LIEU-DISP       PIC  X(3).                         01780001
      *                                                                 01790000
       01  LIGNE-ETAT2.                                                 01800001
           05  FILLER            PIC  X  VALUE SPACE.                   01810001
           05  POSTE-MARQ OCCURS 4 .                                    01820001
               10  W-LIB2            PIC  X(10).                        01830001
               10  W-LIB-MARQ        PIC  X(20).                        01840001
               10  FILLER            PIC  X(3).                         01850001
      *                                                                 01860001
       01  LIGNE-ETAT3.                                                 01870001
           05  FILLER            PIC  X  VALUE SPACE.                   01880001
           05  POSTE-REF OCCURS 4 .                                     01890001
               10  W-LIB3            PIC  X(10).                        01900001
               10  W-LIB-REF         PIC  X(20).                        01910001
               10  FILLER            PIC  X(3).                         01920001
      *                                                                 01930001
       01  LIGNE-ETAT4.                                                 01940001
           05  FILLER            PIC  X  VALUE SPACE.                   01950001
           05  POSTE-CODIC OCCURS 4 .                                   01960001
               10  W-LIB4            PIC  X(10).                        01970001
               10  W-LIB-CODIC       PIC  X(7).                         01980001
               10  FILLER            PIC  X(5).                         01990001
               10  W-DATEINV         PIC  X(8).                         02000001
               10  FILLER            PIC  X(3).                         02010001
      *                                                                 02020000
       01  INTITULE.                                                    02030001
           05  FILLER     PIC X(4).                                     02040001
           05  W-LIBELLE  PIC X(13).                                    02050001
           05  FILLER     PIC X(3).                                     02060001
                                                                        02070001
       01  CADRE.                                                       02080001
           05  FILLER     PIC X(9).                                     02090001
           05  W-VALEUR   PIC X(5).                                     02100001
           05  FILLER     PIC X(6).                                     02110001
                                                                        02120001
      *                                                                 02130000
           EJECT                                                        02140000
      *                                                                 02150000
      * ============================================================= * 02160000
      *        D E S C R I P T I O N      D E S      Z O N E S        * 02170000
      *           D ' A P P E L    M O D U L E    A B E N D           * 02180000
      * ============================================================= * 02190000
      *                                                                 02200000
           COPY  ABENDCOP.                                              02210000
      *                                                                 02220000
      *                                                                 02230000
      * ============================================================= * 02240000
      *     D E S C R I P T I O N    R E C O R D    B I V 1 0 5       * 02250001
      * ============================================================= * 02260000
      *                                                                 02270000
      *                                                                 02280000
           EJECT                                                        02290000
      *                                                                 02300000
       PROCEDURE DIVISION.                                              02310000
      *                                                                 02320000
      * ============================================================= * 02330000
      *              T R A M E   DU   P R O G R A M M E               * 02340000
      * ============================================================= * 02350000
      *                                                               * 02360000
      * ============================================================= * 02370000
      *                                                               * 02380000
      *           ------------------------------------------          * 02390000
      *           --  MODULE DE BASE DU PROGRAMME  BIV205 --          * 02400001
      *           ------------------------------------------          * 02410000
      *                               I                               * 02420000
      *           -----------------------------------------           * 02430000
      *           I                   I                   I           * 02440000
      *   -----------------   -----------------   -----------------   * 02450000
      *   -    DEBUT      -   -  TRAITEMENT   -   -      FIN      -   * 02460000
      *   -----------------   -----------------   -----------------   * 02470000
      *                                                               * 02480000
      * ============================================================= * 02490000
      *                                                                 02500000
      *                                                                 02510000
           PERFORM  DEBUT-BIV205.                                       02520001
           PERFORM  FIRST-LECT-FIV201.                                  02530002
           PERFORM  TRAIT-ETIQUETTES UNTIL FIN-FICHIER = '1'.           02540001
           PERFORM  FIN-BIV205.                                         02550001
      *                                                                 02560000
           EJECT                                                        02570000
      *                                                                 02580000
      * ============================================================= * 02590000
      *                  D E B U T        B I V 1 0 5                 * 02600001
      * ============================================================= * 02610000
      *                                                                 02620000
       DEBUT-BIV205      SECTION.                                       02630001
             DISPLAY 'DEBUT-BIV205      SECTION'.                       02640001
      *                                                                 02650000
           PERFORM  OUVERTURE-FICHIERS.                                 02660000
      * ON INITIALISE LA DATE DU JOUR                                   02670000
           PERFORM INIT-DAT.                                            02680000
           MOVE 0    TO W-FIV201-LUES .                                 02690002
           MOVE 0    TO W-IIV205-ECRITS.                                02700001
           MOVE 1    TO I-COL.                                          02710001
           MOVE SPACES  TO W-MAG.                                       02720001
           MOVE SPACES  TO W-MODSTOCK.                                  02730002
           MOVE SPACES  TO W-EMPL12.                                    02740002
           INITIALIZE  LIGNE-ETAT1.                                     02750001
           MOVE FIV201-LIEU        TO W-LIEU-DISP(1)                    02760002
           INITIALIZE  LIGNE-ETAT2.                                     02770001
           INITIALIZE  LIGNE-ETAT3.                                     02780001
           INITIALIZE  LIGNE-ETAT4.                                     02790001
      *                                                                 02800000
       FIN-DEBUT-BIV205.  EXIT.                                         02810001
           EJECT                                                        02820001
                                                                        02830001
      * ============================================================= * 02840000
      *          M O D U L E S    D E B U T    B I V 1 0 5            * 02850001
      * ============================================================= * 02860000
      *                                                                 02870000
      *====>  TRAITEMENT  POUR RECUPERER LA DATE DU JOUR                02880000
      *                                                                 02890000
       INIT-DAT                SECTION.                                 02900000
                  DISPLAY 'INIT-DAT    SECTION'.                        02910000
           OPEN  INPUT   FDATE                                          02920000
           READ FDATE AT END                                            02930000
             MOVE  '**  PAS  DE  DATE  EN  ENTREE  **'                  02940000
                                           TO  ABEND-MESS               02950000
             PERFORM  FIN-ANORMALE                                      02960000
           END-READ.                                                    02970000
           MOVE SS TO DATE-SS EDT-SS.                                   02980000
           MOVE AA TO DATE-AN EDT-AA.                                   02990000
           MOVE MM TO DATE-MO EDT-MM.                                   03000000
           MOVE JJ TO DATE-JO EDT-JJ.                                   03010000
           CLOSE  FDATE .                                               03020000
       FIN-INIT-DAT.      EXIT.                                         03030000
      *                                                                 03040000
      *====>  O U V E R T U R E  -  F I C H I E R S .                   03050000
      *                                                                 03060000
       OUVERTURE-FICHIERS     SECTION.                                  03070000
                                                                        03080000
           OPEN  INPUT   FIV201.                                        03090002
           OPEN  OUTPUT  IIV205.                                        03100001
                                                                        03110000
       FIN-OUVERTURE-FICHIERS.     EXIT.                                03120000
      *                                                                 03130000
      *                                                                 03140000
      *                                                                 03150000
      * ============================================================= * 03160000
      *          M O D U L E S    T R A I T    B I V 1 0 5            * 03170001
      * ============================================================= * 03180000
                                                                        03190000
       FIRST-LECT-FIV201       SECTION.                                 03200002
                  DISPLAY 'FIRST-LECT  SECTION'.                        03210001
           READ FIV201 AT END                                           03220002
                   MOVE '1'   TO FIN-FICHIER                            03230001
           END-READ.                                                    03240001
                                                                        03250001
           IF FIN-FICHIER NOT = '1'                                     03260001
              MOVE   FIV201-LIEU          TO W-MAG                      03270002
              MOVE   FIV201-MODSTOCK      TO W-MODSTOCK                 03280002
              MOVE   FIV201-EMPL12        TO W-EMPL12                   03290002
              MOVE   FIV201-SEQFAM        TO W-SEQFAM                   03300002
              MOVE   FIV201-NBETIQ        TO W-NBETIQ                   03310002
              ADD   1                     TO W-FIV201-LUES              03320002
              PERFORM ECRITURE-ENTETE                                   03330001
           ELSE                                                         03340001
              DISPLAY 'PAS DE DEMANDE D''EDITION D''ETIQUETTES'         03350001
           END-IF.                                                      03360001
                                                                        03370001
       FIN-FIRST-LECT-FIV201. EXIT.                                     03380002
            EJECT                                                       03390001
      *----------------------------------------------------------------*03400001
      *   TRAITEMENT GENERAL DE L'EDITION DES ETIQUETTES               *03410001
      *----------------------------------------------------------------*03420001
                                                                        03430001
       TRAIT-ETIQUETTES   SECTION.                                      03440001
                                                                        03450001
           IF FIV201-LIEU = W-MAG AND FIV201-MODSTOCK = W-MODSTOCK  AND 03460002
              FIV201-EMPL12 = W-EMPL12                                  03470002
              PERFORM EDIT-ARTICLE-ETIQ                                 03480001
              PERFORM LECT-SUIV-FIV201                                  03490002
           ELSE                                                         03500001
              IF I-COL NOT EQUAL 1                                      03510001
                 MOVE '-'    TO W-SAUT                                  03520001
                 PERFORM ECRITURE-LIGNE-ETAT                            03530001
              END-IF                                                    03540001
              PERFORM ECRITURE-ENTETE                                   03550001
              PERFORM RECUP-DERNIERES-ZONES                             03560001
           END-IF.                                                      03570001
                                                                        03580001
       FIN-TRAIT-ETIQUETTES.   EXIT.                                    03590001
                                                                        03600001
      *----------------------------------------------------------------*03610001
      *   ECRITURE D'AUTANT D'ETIQUETTES DEMANDEES PAR MODE STOCK/EMPLA*03620002
      *   GESTION DES SAUTS LE LIGNE DANS LE CAS OU 4 ETIQUETTES ONT   *03630001
      *                        ETE ECRITES                             *03640001
      *----------------------------------------------------------------*03650001
       EDIT-ARTICLE-ETIQ  SECTION.                                      03660001
           PERFORM VARYING  W-NBETIQ FROM W-NBETIQ BY -1                03670001
                                  UNTIL W-NBETIQ < 1                    03680001
              MOVE FIV201-EMPL12      TO W-EMPLACEMENT (I-COL)          03690002
              MOVE ' FAM:'            TO W-LIB1(I-COL)                  03700001
              MOVE FIV201-LIBFAM      TO W-LIB-FAM(I-COL)               03710002
              IF I-COL = 1                                              03720001
                 MOVE FIV201-LIEU        TO W-LIEU-DISP(I-COL)          03730002
              ELSE                                                      03740001
                 MOVE SPACE              TO W-LIEU-DISP(I-COL)          03750001
              END-IF                                                    03760001
              MOVE 'MARQUE  : '       TO W-LIB2(I-COL)                  03770001
              MOVE FIV201-MARQUE      TO W-MARQ                         03780002
              MOVE FIV201-NEAN        TO W-NEAN                         03790002
              MOVE W-MARQ-EAN         TO W-LIB-MARQ(I-COL)              03800001
              MOVE 'REFER   : '       TO W-LIB3(I-COL)                  03810001
              MOVE FIV201-REFERENCE   TO W-LIB-REF(I-COL)               03820002
              MOVE 'CODIC   : '       TO W-LIB4(I-COL)                  03830001
              MOVE FIV201-NCODIC      TO W-LIB-CODIC(I-COL)             03840002
              MOVE FIV201-DATEINV     TO W-INVDATE                      03850002
              MOVE INV-JJ             TO FORMAT-JJ                      03860001
              MOVE INV-MM             TO FORMAT-MM                      03870001
              MOVE INV-AA             TO FORMAT-AA                      03880001
              MOVE W-FORMAT-DATE      TO W-DATEINV(I-COL)               03890001
              ADD 1 TO I-COL                                            03900001
              IF I-COL > 4                                              03910001
                 MOVE '-'    TO W-SAUT                                  03920001
                 PERFORM ECRITURE-LIGNE-ETAT                            03930001
              END-IF                                                    03940001
           END-PERFORM.                                                 03950001
                                                                        03960001
       FIN-EDIT-ARTICLE-ETIQ. EXIT.                                     03970001
           EJECT                                                        03980001
      *----------------------------------------------------------------*03990001
      *         LECTURE SUIVANTE SANS SAUVEGARDE DES ZONES LUES        *04000001
      *----------------------------------------------------------------*04010001
       LECT-SUIV-FIV201     SECTION.                                    04020002
           READ FIV201 AT END                                           04030002
                   MOVE '1'   TO FIN-FICHIER                            04040001
           END-READ.                                                    04050001
                                                                        04060001
           IF FIN-FICHIER NOT = '1'                                     04070001
              ADD   1                     TO W-FIV201-LUES              04080002
              MOVE   FIV201-NBETIQ        TO W-NBETIQ                   04090002
           ELSE                                                         04100001
              IF I-COL > 1                                              04110001
                 MOVE '-'    TO W-SAUT                                  04120001
                 PERFORM ECRITURE-LIGNE-ETAT                            04130001
              END-IF                                                    04140001
           END-IF.                                                      04150001
                                                                        04160001
       FIN-LECT-SUIV-FIV201. EXIT.                                      04170002
            EJECT                                                       04180001
      *----------------------------------------------------------------*04190001
      *         SAUVEGARDE DES ZONES LUES PRECEDEMMENT DANS LE CAS DE  *04200001
      *         RUPTURE CE PROCEDE PERMET D'AVOIR UNE LECTURE D'AVANCE *04210001
      *----------------------------------------------------------------*04220001
       RECUP-DERNIERES-ZONES       SECTION.                             04230001
                                                                        04240001
           MOVE   FIV201-LIEU          TO W-MAG.                        04250002
           MOVE   FIV201-MODSTOCK      TO W-MODSTOCK.                   04260002
           MOVE   FIV201-EMPL12        TO W-EMPL12.                     04270002
           MOVE   FIV201-SEQFAM        TO W-SEQFAM.                     04280002
                                                                        04290001
       FIN-RECUP-DERNIERES-ZONES. EXIT.                                 04300001
          EJECT                                                         04310001
      *----------------------------------------------------------------*04320001
      *         ECRITURE DE L'ENTETE (MAGASIN MODSTOCK / EMPLACEMENT ) *04330002
      *                      DANS LE CAS D'UNE RUPTURE                 *04340001
      *----------------------------------------------------------------*04350001
       ECRITURE-ENTETE     SECTION.                                     04360001
                                                                        04370001
           INITIALIZE  LIGNE-ETAT1.                                     04380001
           MOVE FIV201-LIEU        TO W-LIEU-DISP(1)                    04390002
           INITIALIZE  LIGNE-ETAT2.                                     04400001
           INITIALIZE  LIGNE-ETAT3.                                     04410001
           INITIALIZE  LIGNE-ETAT4.                                     04420001
           MOVE 1                TO   I-COL.                            04430001
           MOVE 'D E P O T'      TO   W-LIBELLE.                        04440001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04450001
           MOVE FIV201-LIEU      TO   W-VALEUR.                         04460002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04470001
           ADD 1                 TO   I-COL.                            04480001
           MOVE 'MODE STOCKAGE'  TO   W-LIBELLE.                        04490001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04500001
           MOVE FIV201-MODSTOCK  TO   W-VALEUR.                         04510002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04520001
           ADD 1                 TO   I-COL.                            04530001
           EVALUATE FIV201-MODSTOCK                                     04540002
              WHEN 'RACK' MOVE 'ALLEE/NIVEAU '  TO   W-LIBELLE          04550001
              WHEN 'VRAC' MOVE 'ZONE/SECTEUR '  TO   W-LIBELLE          04560001
              WHEN 'SOL'  MOVE 'AIRE/COTE    '  TO   W-LIBELLE          04570001
              WHEN OTHER  MOVE SPACES           TO   W-LIBELLE          04580001
           END-EVALUATE                                                 04590001
                                                                        04600001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04610001
           MOVE FIV201-EMPL12    TO   W-VALEUR.                         04620002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04630001
           ADD 1                 TO   I-COL.                            04640001
           MOVE '   T Y P E   '  TO   W-LIBELLE.                        04650001
           MOVE INTITULE         TO   W-LIB-MARQ(I-COL).                04660001
           MOVE FIV201-TYPE      TO   W-VALEUR.                         04670002
           MOVE CADRE            TO   W-LIB-REF(I-COL).                 04680001
                                                                        04690001
           MOVE '1'      TO W-SAUT.                                     04700001
           PERFORM ECRITURE-LIGNE-ETAT.                                 04710001
           MOVE '-'    TO W-SAUT.                                       04720001
                                                                        04730001
       FIN-ECRITURE-ENTETE.  EXIT.                                      04740001
           EJECT                                                        04750001
      *----------------------------------------------------------------*04760001
      *               ECRITURE DE LES LIGNES D'ETAT                    *04770001
      *----------------------------------------------------------------*04780001
                                                                        04790001
       ECRITURE-LIGNE-ETAT     SECTION.                                 04800001
                                                                        04810001
           WRITE IIV205-RECORD    FROM  LIGNE-ETAT1.                    04820001
           WRITE IIV205-RECORD    FROM  LIGNE-ETAT2.                    04830001
           WRITE IIV205-RECORD    FROM  LIGNE-ETAT3.                    04840001
           WRITE IIV205-RECORD    FROM  LIGNE-ETAT4.                    04850001
           INITIALIZE  LIGNE-ETAT1.                                     04860001
           MOVE FIV201-LIEU        TO W-LIEU-DISP(1)                    04870002
           INITIALIZE  LIGNE-ETAT2.                                     04880001
           INITIALIZE  LIGNE-ETAT3.                                     04890001
           INITIALIZE  LIGNE-ETAT4.                                     04900001
           MOVE 1   TO I-COL .                                          04910001
           ADD  4                  TO W-IIV205-ECRITS.                  04920001
       FIN-ECRITURE-LIGNE-ETAT. EXIT.                                   04930001
          EJECT                                                         04940001
                                                                        04950001
      *                                                                 04960000
      *                                                                 04970000
      * ============================================================= * 04980000
      *                      F I N        B I V 1 0 5                 * 04990001
      * ============================================================= * 05000000
      *                                                                 05010000
       FIN-BIV205     SECTION.                                          05020001
                                                                        05030000
           PERFORM  COMPTE-RENDU.                                       05040000
           PERFORM  FERMETURE-FICHIERS.                                 05050000
           PERFORM  FIN-PROGRAMME.                                      05060000
                                                                        05070000
       FIN-FIN-BIV205.   EXIT.                                          05080001
      * ============================================================= * 05090000
      *          M O D U L E S      F I N      B I V 1 0 5            * 05100001
      * ============================================================= * 05110000
      *                                                                 05120000
      *====>  F I N  -  A N O R M A L E.                                05130000
      *                                                                 05140000
       FIN-ANORMALE    SECTION.                                         05150000
                                                                        05160000
           PERFORM  FERMETURE-FICHIERS.                                 05170000
           MOVE  'BIV205'                  TO  ABEND-PROG.              05180001
                                                                        05190000
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                05200000
                                                                        05210000
       FIN-FIN-ANORMALE.    EXIT.                                       05220000
      *                                                                 05230000
      *====>  C O M P T E  -  R E N D U.                                05240000
      *                                                                 05250000
       COMPTE-RENDU      SECTION.                                       05260000
                                                                        05270000
           DISPLAY  '**'.                                               05280000
           DISPLAY  '**           B I V 1 0 5 '.                        05290001
           DISPLAY  '**'.                                               05300000
           DISPLAY  '** DATE  PARAMETRE                         : '     05310000
                     EDT-DATE.                                          05320000
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  ECRITS         : '     05330000
                    W-IIV205-ECRITS.                                    05340001
           DISPLAY  '** NOMBRE  ENREGISTREMENTS  LUS SUR FIV201 : '     05350002
                    W-FIV201-LUES.                                      05360002
                                                                        05370000
       FIN-COMPTE-RENDU.        EXIT.                                   05380000
      *                                                                 05390000
      *====>  F E R M E T U R E    F I C H I E R S.                     05400000
      *                                                                 05410000
       FERMETURE-FICHIERS    SECTION.                                   05420000
                                                                        05430000
           CLOSE  FIV201.                                               05440002
           CLOSE  IIV205.                                               05450001
                                                                        05460000
       F-FERMETURE-FICHIERS.    EXIT.                                   05470000
      *                                                                 05480000
      *====>  F I N    P R O G R A M M E.                               05490000
      *                                                                 05500000
       FIN-PROGRAMME        SECTION.                                    05510000
                                                                        05520000
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP  RUN.                                                   05530000
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        05540000
       F-FIN-PROGRAMME.      EXIT.                                      05550000
           EJECT                                                        05560000
                                                                        05570000
