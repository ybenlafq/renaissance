      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID DIVISION.                                                             
       PROGRAM-ID. BGP102.                                                      
      *                                                                         
      *    PROGRAMME PASSE SOUS SCLM (ECOM)                                     
      *    02 AVRIL 2013                                                        
      *                                                                         
      *   COQUILLE VIDE POUR MAJ LIBRARIAN                                      
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *****************************************************************         
      *                                                               *         
      *     EXTRACTION QUOTIDIENNE VENTES / STOCKS DARTY.COM          *         
      *                                                               *         
      *     PROG 2/2 : ETAT                                           *         
      *                                                               *         
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT  SECTION.                                                   
       FILE-CONTROL.                                                            
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
       PROCEDURE DIVISION.                                                      
           PERFORM TRAITEMENT.                                                  
      *                                                                         
      ******************************************************************        
       TRAITEMENT                   SECTION.                                    
      *****************************************************************         
      *                                                                         
           GOBACK.                                                              
       FIN-TRAITEMENT. EXIT.                                                    
      *                                                                         
      *                                                                         
