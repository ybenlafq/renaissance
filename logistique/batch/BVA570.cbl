      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.    BVA570.                                                   
       AUTHOR.        APSIDE.                                                   
       ENVIRONMENT    DIVISION.                                                 
       CONFIGURATION  SECTION.                                                  
      ***************************************************************           
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E *           
      ***************************************************************           
      *                                                                         
      *  PROJET     : DSA0                                                      
      *  PROGRAMME  : BVA570                                                    
      *  CREATION   : 93/02/26                                                  
      *  FONCTION   : EDITION DE L'ETAT IVA570                                  
      *                                                                         
      *****************************************************************         
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT   SECTION.                                                  
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT IVA570  ASSIGN  TO  IVA570.                                   
      *--                                                                       
           SELECT IVA570  ASSIGN  TO  IVA570                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FVA570  ASSIGN  TO  FVA570.                                   
      *--                                                                       
           SELECT FVA570  ASSIGN  TO  FVA570                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FVA571  ASSIGN  TO  FVA571.                                   
      *--                                                                       
           SELECT FVA571  ASSIGN  TO  FVA571                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      ***  FICHIER TOTAL FVA570                                                 
       FD  FVA570                                                               
           BLOCK  CONTAINS 0   RECORDS                                          
           LABEL RECORD STANDARD.                                               
       01  MW-FILLER              PIC X(80).                                    
      ***  FICHIER DETAIL FVA571                                                
       FD  FVA571                                                               
           BLOCK  CONTAINS 0   RECORDS                                          
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER              PIC X(71).                                    
      ***  ETAT IVA570                                                          
       FD  IVA570                                                               
           BLOCK  CONTAINS 0   RECORDS                                          
           LABEL RECORD STANDARD.                                               
       01  LIGNE-IVA570        PIC X(512).                                      
      *****************************************************************         
      *                 W O R K I N G  S T O R A G E                            
      *****************************************************************         
       WORKING-STORAGE SECTION.                                                 
      *------------------------                                                 
       01  DONNEES.                                                             
           05 CPT-FVA570       PIC 9(06)      VALUE 0.                          
           05 CPT-FVA571       PIC 9(06)      VALUE 0.                          
      *                                                                         
       01  TOP-ECRITURE-DSECT      PIC X(1) VALUE '1'.                          
           88 ECRIT                VALUE '1'.                                   
           88 NON-ECRIT            VALUE '2'.                                   
      *                                                                         
       01  TOP-LECTURE-FVA570      PIC X(1) VALUE '1'.                          
           88 DEBUT-FVA570         VALUE '1'.                                   
           88 FIN-FVA570           VALUE '2'.                                   
      *                                                                         
       01  TOP-LECTURE-FVA571      PIC X(1) VALUE '1'.                          
           88 DEBUT-FVA571         VALUE '1'.                                   
           88 FIN-FVA571           VALUE '2'.                                   
      *                                                                         
       01  ABEND-ZONE.                                                          
           05 ABEND-PROG           PIC X(08).                                   
           05 ABEND-MESS           PIC X(55).                                   
      *****************************************************************         
      *     DSECT DES FICHIERS D'EXTRACTION                                     
      *****************************************************************         
           COPY FVA570.                                                         
           COPY FVA571.                                                         
           COPY IVA570.                                                         
      *****************************************************************         
      *     P R O C E D U R E   D I V I S I O N                       *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BVA570.                                                           
      *--------------                                                           
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT UNTIL FIN-FVA570.                          
           PERFORM MODULE-SORTIE.                                               
      *--------------------------                                               
       MODULE-ENTREE     SECTION.                                               
      *--------------------------                                               
           DISPLAY '*** EXECUTION STARTEE NORMALEMENT ***'.                     
           OPEN  INPUT  FVA570 FVA571.                                          
           OPEN  OUTPUT IVA570.                                                 
           PERFORM LECTURE-FVA570.                                              
           PERFORM LECTURE-FVA571.                                              
      *----------------------------                                             
       MODULE-TRAITEMENT   SECTION.                                             
      *----------------------------                                             
           INITIALIZE DSECT-IVA570.                                     22040399
           SET NON-ECRIT                 TO TRUE.                               
           MOVE 'IVA570'                 TO NOMETAT-IVA570.                     
           MOVE 1                        TO IVA570-SEQUENCE.                    
           MOVE FVA570-NSOC              TO IVA570-NSOC.                        
           MOVE FVA570-CSEQ              TO IVA570-CSEQ.                        
           MOVE FVA570-NCONSO            TO IVA570-NCONSO.                      
           MOVE FVA570-LCONSO            TO IVA570-LCONSO.                      
           MOVE FVA570-CRAYON            TO IVA570-CRAYON.                      
           MOVE FVA570-QSTOCKINIT        TO IVA570-QSTOCKINIT.                  
           MOVE FVA570-QSTOCKFINAL       TO IVA570-QSTOCKFINAL.                 
           MOVE FVA570-PSTOCKINIT        TO IVA570-PSTOCKINIT.                  
           MOVE FVA570-PSTOCKFINAL       TO IVA570-PSTOCKFINAL.                 
           MOVE FVA570-PRECYCLENTREE     TO IVA570-PRECYCLENTREE.               
           MOVE FVA570-PRECYCLSORTIE     TO IVA570-PRECYCLSORTIE.               
           PERFORM UNTIL FVA571-NSOC  NOT = IVA570-NSOC                         
                      OR FVA571-CSEQ  NOT = IVA570-CSEQ                         
                      OR FVA571-NCONSO NOT = IVA570-NCONSO                      
                      OR FIN-FVA571                                             
              MOVE FVA571-NSOCMVT            TO IVA570-NSOCMVT                  
              MOVE FVA571-NLIEUMVT           TO IVA570-NLIEUMVT                 
              MOVE FVA571-LLIEU              TO IVA570-LLIEU                    
              MOVE FVA571-CTYPMVT            TO IVA570-CTYPMVT                  
              EVALUATE FVA571-CTYPMVT                                           
              WHEN 'E'                                                          
                 MOVE 'MVTS EXTERNES'        TO IVA570-LTYPMVT                  
              WHEN 'R'                                                          
                 MOVE 'MVTS DE RETROCESSION' TO IVA570-LTYPMVT                  
              WHEN 'M'                                                          
                 MOVE 'MVTS DE MUTATION'     TO IVA570-LTYPMVT                  
              WHEN OTHER                                                        
                 STRING 'TYPE MVT ' FVA571-CTYPMVT ' INCONNU'                   
                    DELIMITED BY SIZE INTO ABEND-MESS                           
                 END-STRING                                                     
                 PERFORM PLANTAGE                                               
              END-EVALUATE                                                      
              MOVE FVA571-QSTOCKENTREE   TO IVA570-QSTOCKENTREE                 
              MOVE FVA571-QSTOCKSORTIE   TO IVA570-QSTOCKSORTIE                 
              MOVE FVA571-PSTOCKENTREE   TO IVA570-PSTOCKENTREE                 
              MOVE FVA571-PSTOCKSORTIE   TO IVA570-PSTOCKSORTIE                 
              DISPLAY 'LCONSO ' FVA570-LCONSO ' ' IVA570-LCONSO                 
              DISPLAY 'LLIEU ' FVA571-LLIEU ' ' IVA570-LLIEU                    
              WRITE LIGNE-IVA570 FROM DSECT-IVA570                              
              SET ECRIT                  TO TRUE                                
              PERFORM LECTURE-FVA571                                            
           END-PERFORM.                                                         
           IF NON-ECRIT                                                         
              WRITE LIGNE-IVA570 FROM DSECT-IVA570                              
           END-IF.                                                              
           PERFORM LECTURE-FVA570.                                              
      *----------------------------                                             
       LECTURE-FVA570      SECTION.                                             
      *----------------------------                                             
           READ FVA570 INTO DSECT-FVA570                                        
             AT END SET FIN-FVA570 TO TRUE.                                     
           IF DEBUT-FVA570                                                      
              ADD 1 TO CPT-FVA570                                               
           END-IF.                                                              
      *----------------------------                                             
       LECTURE-FVA571      SECTION.                                             
      *----------------------------                                             
           READ FVA571 INTO DSECT-FVA571                                        
             AT END SET FIN-FVA571 TO TRUE.                                     
           IF DEBUT-FVA571                                                      
              ADD 1 TO CPT-FVA571                                               
           END-IF.                                                              
      *------------------------------                                           
       PLANTAGE              SECTION.                                           
      *------------------------------                                           
           PERFORM  FIN-ANORMALE.                                               
           MOVE  'BVA570' TO ABEND-PROG.                                        
           DISPLAY '*** ABEND ***' ABEND-MESS                                   
           CALL  'ABEND' USING ABEND-PROG  ABEND-MESS.                          
      *------------------------------                                           
       FIN-ANORMALE          SECTION.                                           
      *------------------------------                                           
           DISPLAY  '***********************************************'.          
           DISPLAY  '***        INTERRUPTION DU PROGRAMME        ***'.          
           DISPLAY  '***                 BVA570                  ***'.          
           DISPLAY  '***********************************************'.          
           PERFORM  COMPTE-RENDU.                                               
           CLOSE  FVA570 FVA571 IVA570.                                         
      *------------------------------                                           
       MODULE-SORTIE         SECTION.                                           
      *------------------------------                                           
           DISPLAY  '***********************************************'.          
           DISPLAY  '***      EXECUTION TERMINEE NORMALEMENT     ***'.          
           DISPLAY  '***                 BVA570                  ***'.          
           DISPLAY  '***********************************************'.          
           PERFORM  COMPTE-RENDU.                                               
           CLOSE  FVA570 FVA571 IVA570.                                         
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *------------------------------                                           
       COMPTE-RENDU          SECTION.                                           
      *------------------------------                                           
           DISPLAY  '***********************************************'.          
           DISPLAY  '***  LECTURE DU FICHIER FVA570  : ' CPT-FVA570.            
           DISPLAY  '***  LECTURE DU FICHIER FVA571  : ' CPT-FVA571.            
           DISPLAY  '***********************************************'.          
