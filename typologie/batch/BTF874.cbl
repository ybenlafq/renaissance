      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00000020
       PROGRAM-ID.                  BTF874.                             00000030
       AUTHOR.    DSA008.                                               00000040
      *------------------------------------------------------------*    00000050
      *                   PROGRAMME                                *    00000060
      *                  DE MISE A JOUR                            *    00000070
      *               DU FICHIER FTF874 (FICHIER DE PURGE AS400    *    00000080
      *               DES VENTES ARCHIVEES SUR LE HOST)            *    00000080
      * DATE CREATION : 10/01/11                                   *    00000100
      *------------------------------------------------------------*    00000110
       ENVIRONMENT DIVISION.                                            00000120
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00000130
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES.                                                   00000140
      *    DECIMAL-POINT IS COMMA.                                      00000150
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA                                               
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                            00000160
       FILE-CONTROL.                                                    00000170
      *                                                                 00000180
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FPV874 ASSIGN TO FPV874.                              00000190
      *--                                                                       
           SELECT FPV874 ASSIGN TO FPV874                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTF874 ASSIGN TO FTF874.                              00000200
      *--                                                                       
           SELECT FTF874 ASSIGN TO FTF874                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNSOC  ASSIGN TO FNSOC.                               00000200
      *--                                                                       
           SELECT FNSOC  ASSIGN TO FNSOC                                        
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                 00000230
       DATA DIVISION.                                                   00000240
       FILE SECTION.                                                    00000250
      *---------------------------------------------------------------* 00000260
      *  D E F I N I T I O N    F I C H I E R    F T F 2 0 0          * 00000270
      *---------------------------------------------------------------* 00000280
      *                                                                 00000290
       FD  FPV874                                                       00000360
           RECORDING F                                                  00000370
           BLOCK 0 RECORDS                                              00000380
           LABEL RECORD STANDARD.                                       00000390
       01 DSECT-FPV874.                                                 00000400
          03 FPV874-NSOCIETE             PIC X(0003).                           
          03 FPV874-NLIEU                PIC X(0003).                           
          03 FPV874-NVENTE               PIC X(0007).                           
          03 FPV874-FILLER               PIC X(0007).                           
      *                                                                 00000350
       FD  FTF874                                                       00000360
           RECORDING F                                                  00000370
           BLOCK 0 RECORDS                                              00000380
           LABEL RECORD STANDARD.                                       00000390
       01  FTF874-ENR   PIC X(385).                                     00000400
      *                                                                 00000410
      *                                                                         
       FD   FNSOC                                                               
            RECORDING F                                                         
            BLOCK 0  RECORDS                                                    
            LABEL RECORD STANDARD.                                              
       01   ENR-FNSOC            PIC X(80).                                     
      *                                                                 00000550
      *---------------------------------------------------------------* 00000560
      *                 W O R K I N G  -  S T O R A G E               * 00000570
      *---------------------------------------------------------------* 00000580
      *                                                                 00000590
       WORKING-STORAGE SECTION.                                         00000600
      *                                                                 00000620
      *---------------------------------------------------------------* 00000630
      *           D E S C R I P T I O N    D E S    Z O N E S         * 00000640
      *  S P E C I F I Q U E S    A U   F I C H I E R   F T F 2 0 0   * 00000650
      *---------------------------------------------------------------* 00000660
      *                                                                 00000670
           COPY FTF600C.                                                00000680
      *                                                                 00000700
      *=================================================================00000710
      * DESCRIPTION DE FDATE                                            00000720
      *=================================================================00000730
      *                                                                 00000740
      *                                                                 00000910
           COPY ABENDCOP.                                               00000920
           COPY WORKDATC.                                               00000920
           EJECT                                                        00000930
      *                                                                 00000940
      *                                                                 00000940
      *---------------------------------------------------------------* 00000950
      *           D E S C R I P T I O N    D E S    Z O N E S         * 00000960
      *        S P E C I F I Q U E S    A U    P R O G R A M M E      * 00000970
      *---------------------------------------------------------------* 00000980
      *                                                                 00000990
      *                                                                 00001000
       01  COMPTEURS.                                                   00001010
           03  CTR-FPV874             PIC  S9(05) COMP-3 VALUE +0.      00001020
           03  CTR-FTF874             PIC  S9(05) COMP-3 VALUE +0.      00001030
           03  NOMBRE                 PIC  ZZZZ9.                       00001050
      *                                                                         
       01 ETAT-FICHIER-FPV874 PIC X VALUE '1'.                          00001180
           88 NON-FIN-FPV874  VALUE '1'.                                00001190
           88 FIN-FPV874      VALUE '0'.                                00001200
      *                                                                         
       01 W-FTF874-CLE.                                                         
          05 W-FTF874-CLE-NSOCIETE PIC X(3).                                    
          05 W-FTF874-CLE-NLIEU    PIC X(3).                                    
          05 W-FTF874-CLE-NVENTE   PIC X(7).                                    
      *                                                                         
       01  FNSOC-REC               PIC X(03).                                   
      *                                                                         
       01 W-FTF874-DATA.                                                        
          05 W-FTF874-NSOCIETE     PIC X(3).                                    
          05 W-FTF874-NLIEU        PIC X(3).                                    
          05 W-FTF874-NVENTE       PIC X(7).                                    
      *                                                                 00001210
       PROCEDURE DIVISION.                                              00001260
      *                                                                 00001270
       MODULE-BTF874 SECTION.                                           00001280
      *                                                                 00001290
           PERFORM  DEBUT                                               00001300
           PERFORM  TRAITEMENT UNTIL FIN-FPV874                         00001310
           PERFORM  FIN                                                 00001320
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    00001330
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-MODULE-BTF874. EXIT.                                                 
      *                                                                 00001360
      *---------------------------------------------------------------* 00001370
      *                    D E B U T                                  * 00001380
      *---------------------------------------------------------------* 00001390
      *                                                                 00001400
       DEBUT SECTION.                                                   00001410
      *======                                                           00001420
           OPEN OUTPUT FTF874.                                          00001430
           OPEN  INPUT  FNSOC.                                                  
           READ FNSOC INTO FNSOC-REC AT END                                     
                MOVE 'BTF874 : FICHIER FNSOC VIDE' TO ABEND-MESS                
                PERFORM FIN-ANORMALE                                            
           END-READ.                                                            
           CLOSE FNSOC.                                                         
           OPEN INPUT FPV874.                                           00001440
           PERFORM LECTURE-FPV874.                                      00001450
       FIN-DEBUT. EXIT.                                                         
      *                                                                 00001700
      *---------------------------------------------------------------* 00001710
      *                     T R A I T E M E N T                       * 00001720
      *---------------------------------------------------------------* 00001730
      *                                                                 00001740
       TRAITEMENT SECTION.                                              00001750
      *===========                                                      00001760
      *                                                                 00001770
           MOVE  SPACES                TO TF600-ENREG.                          
           MOVE 'PURGE '               TO TF600-FICHIER                         
           MOVE FNSOC-REC              TO TF600-SOC                             
           MOVE FPV874-NSOCIETE        TO W-FTF874-CLE-NSOCIETE                 
           MOVE FPV874-NLIEU           TO W-FTF874-CLE-NLIEU                    
           MOVE FPV874-NVENTE          TO W-FTF874-CLE-NVENTE                   
           MOVE W-FTF874-CLE           TO TF600-CLE                             
           MOVE W-FTF874-CLE-NSOCIETE  TO W-FTF874-NSOCIETE                     
           MOVE W-FTF874-CLE-NLIEU     TO W-FTF874-NLIEU                        
           MOVE W-FTF874-CLE-NVENTE    TO W-FTF874-NVENTE                       
           MOVE W-FTF874-DATA          TO TF600-DATA                            
           WRITE FTF874-ENR FROM TF600-ENREG                               00002
           END-WRITE                                                       00002
           ADD 1 TO CTR-FTF874                                             00002
           PERFORM LECTURE-FPV874.                                              
       FIN-TRAITEMENT. EXIT.                                                    
      *                                                                 00002420
       LECTURE-FPV874 SECTION.                                          00002430
      *===============                                                  00002440
      *                                                                 00002450
           READ FPV874                                                  00002460
                INTO W-FTF874-DATA                                      00002470
                AT END SET FIN-FPV874 TO TRUE                           00002480
           END-READ.                                                    00002490
           IF NON-FIN-FPV874                                            00002500
               ADD 1 TO CTR-FPV874                                      00002510
           END-IF.                                                      00002520
       FIN-LECTURE-FPV874. EXIT.                                                
      *                                                                         
      *                                                                 00002420
      *---------------------------------------------------------------* 00002870
      *                             F I N                             * 00002880
      *---------------------------------------------------------------* 00002890
      *                                                                 00002900
       FIN SECTION.                                                     00002910
      *====                                                             00002920
           CLOSE FPV874 FTF874.                                         00002930
           DISPLAY '**********************************************'     00002940
           DISPLAY '*    PROGRAMME BTF874 TERMINE NORMALEMENT    *'     00002950
           DISPLAY '*                                            *'     00002960
           MOVE CTR-FPV874 TO NOMBRE.                                   00002970
           DISPLAY '*  NOMBRE DE LIGNES LUES SUR FPV874 = ' NOMBRE      00002980
                   '  *'.                                               00002990
           MOVE CTR-FTF874 TO NOMBRE.                                   00003030
           DISPLAY '*  NOMBRE D''ENREG ECRITS DS FTF874  = ' NOMBRE     00003040
                   '  *'.                                               00003050
           DISPLAY '*                                            *'     00003060
           DISPLAY '**********************************************'.    00003070
       FIN-FIN. EXIT.                                                           
      *                                                                 00003080
      *-----------------------------------------------------------------        
       FIN-ANORMALE SECTION.                                                    
      *-----------------------------------------------------------------        
           DISPLAY  '***********************************************'.          
           DISPLAY  '*** INTERRUPTION ANORMALE DU PROGRAMME      ***'.          
           DISPLAY  '***               BTF874                    ***'.          
           DISPLAY  '***********************************************'.          
           CLOSE    FTF874.                                                     
           MOVE 'BTF874' TO ABEND-PROG.                                         
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                            
       FIN-FIN-ANORMALE. EXIT.                                                  
      *                                                                         
      *                                                                         
