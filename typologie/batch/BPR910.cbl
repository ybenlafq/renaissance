      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BPR910.                                                 
       AUTHOR.          DSA017.                                                 
      *                                                                         
      ******************************************************************        
      *   F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E  *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : EPURATION FICHIER HISTO FFS052                  *        
      *  PROGRAMME   : BPR910                                          *        
      *  CREATION    : 17/06/1998                                      *        
      *  PERIODICITE : MENSUELLE                                       *        
      *  FONCTIONS   :                                                 *        
      *                                                                *        
      *    EPURATION DES MOUVEMENTS DE PLUS DE 2 MOIS                  *        
      *                                                                *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FMOIS  ASSIGN TO FMOIS.                                      
      *--                                                                       
            SELECT FMOIS  ASSIGN TO FMOIS                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FFS052 ASSIGN TO FFS052.                                     
      *--                                                                       
            SELECT FFS052 ASSIGN TO FFS052                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FPR910 ASSIGN TO FPR910.                                     
      *--                                                                       
            SELECT FPR910 ASSIGN TO FPR910                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      ******************************************************************        
      *    FMOIS  : FICHIER EN ENTREE CONTENANT LA DATE DE TRAITEMENT  *        
      ******************************************************************        
       FD  FMOIS                                                                
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  ENR-FMOIS.                                                           
           05 FMOIS-MMSSAA           PIC X(06).                                 
           05 FILLER                 PIC X(74).                                 
      *                                                                         
      ******************************************************************        
      *    FFS052 : FICHIER EN ENTREE VENTES DE PRESTATIONS MENSUELLES *        
      ******************************************************************        
       FD  FFS052                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
           COPY FFS052.                                                         
      *                                                                         
      ******************************************************************        
      *    FPR910 : FICHIER EN SORTIE VENTES DE PRESTATIONS MENSUELLES *        
      ******************************************************************        
       FD  FPR910                                                               
           RECORDING F                                                          
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FPR910-ENR            PIC X(300).                                    
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
           COPY  SYBWDIV0.                                                      
           COPY  WORKDATC.                                                      
      ******************************************************************        
      *    ZONES   SPECIFIQUES   AU   PROGRAMME                        *        
      ******************************************************************        
       01  FILLER                        PIC X(10)      VALUE 'WORKING'.        
      *                                                                         
       01  WS-FMOIS-AASSMMJJ             PIC X(08)         VALUE SPACES.        
       01  WS-DDEBMOIS                   PIC X(08)         VALUE SPACES.        
       01  WS-DFINMOIS                   PIC X(08)         VALUE SPACES.        
       01  WS-DMOISPAYE                  PIC X(06)         VALUE SPACES.        
      *                                                                         
       01  WS-COMPTEURS.                                                        
           05 WS-CPT-FFS052              PIC  9(06)        VALUE ZEROES.        
           05 WS-CPT-FPR910              PIC  9(06)        VALUE ZEROES.        
      *                                                                         
       01  ETAT-FFS052                   PIC 9                  VALUE 0.        
           88 PAS-FIN-FFS052                                    VALUE 0.        
           88 FIN-FFS052                                        VALUE 1.        
      *                                                                         
      ******************************************************************        
      *    ZONES DE GESTION DES ERREURS                                *        
      ******************************************************************        
      *                                                                         
           COPY SYKWERRO.                                                       
           COPY ABENDCOP.                                                       
       PROCEDURE DIVISION.                                                      
      *----------------------------------------------------------------*        
      *              T R A M E   DU   P R O G R A M M E                *        
      *----------------------------------------------------------------*        
      *----------------------------------------                                 
       MODULE-BPR910                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
      *----------------------------------------                                 
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY  '************************************************'          
           DISPLAY  '**                                            **'          
           DISPLAY  '**              PROGRAMME BPR910              **'          
           DISPLAY  '**              ----------------              **'          
           DISPLAY  '************************************************'          
           DISPLAY  '**                                            **'          
           DISPLAY  '**        PROGRAMME DEBUTE NORMALEMENT        **'          
           DISPLAY  '**                                            **'          
      * INITIALISATIONS                                                         
           MOVE 'BPR910'               TO ABEND-PROG                            
           OPEN INPUT  FFS052 FMOIS                                             
           OPEN OUTPUT FPR910                                                   
           INITIALIZE ETAT-FFS052                                               
           PERFORM LECTURE-FFS052                                               
           PERFORM LECTURE-FMOIS.                                               
      *                                                                         
      *----------------------------------------                                 
       MODULE-TRAITEMENT               SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM UNTIL FIN-FFS052                                             
             IF  FFS052-DDELIV > WS-DFINMOIS                                    
                WRITE FPR910-ENR   FROM DSECT-FFS052                            
                ADD 1  TO WS-CPT-FPR910                                         
             END-IF                                                             
             PERFORM LECTURE-FFS052                                             
           END-PERFORM.                                                         
      *                                                                         
      *----------------------------------------                                 
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY  '************************************************'          
           DISPLAY  '**        FIN  NORMALE  DU PROGRAMME          **'          
           DISPLAY  '**                  BPR910                    **'          
           DISPLAY  '************************************************'          
           DISPLAY  '**                                            **'          
           DISPLAY  '**   NOMBRE ENREG. LUS FFS052   : '                        
                     WS-CPT-FFS052  '      **'                                  
           DISPLAY  '**                                            **'          
           DISPLAY  '************************************************'          
           DISPLAY  '**   NOMBRE ENREG. ECRITS  IPR910 :  '                     
                     WS-CPT-FPR910  '   **'                                     
           DISPLAY  '************************************************'          
           CLOSE  FPR910 FFS052                                                 
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *----------------------------------------------------------------*        
      *                 LECTURE DES FICHIERS EN ENTREE                 *        
      *----------------------------------------------------------------*        
      *----------------------------------------                                 
       LECTURE-FMOIS                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           READ FMOIS AT END                                                    
                MOVE  '*** CARTE PARAMETRE FMOIS ABSENTE '                      
                                       TO ABEND-MESS  MESS                      
                MOVE 1                 TO ABEND-CODE                            
                CLOSE FMOIS                                                     
                GO TO ABANDON-BATCH                                             
           END-READ.                                                            
           STRING FMOIS-MMSSAA(3:4)                                             
                  FMOIS-MMSSAA(1:2)                                             
                  '01'                                                          
                  DELIMITED BY SIZE INTO WS-FMOIS-AASSMMJJ                      
           END-STRING.                                                          
           DISPLAY '**   DATE DE TRAITEMENT : ' FMOIS-MMSSAA                    
                   '            **'.                                            
      *--> PASSAGE A 1 MOIS EN ARRIERE                                          
           MOVE '01'          TO GFJJMMSSAA (1:2)                               
           MOVE FMOIS-MMSSAA  TO GFJJMMSSAA (3:6)                               
           MOVE   'B'         TO GFDATA.                                        
           MOVE  '01'         TO GFAJOUX.                                       
           PERFORM APPEL-BETDATC                                                
           MOVE GFSAMJ-0      TO WS-DFINMOIS                                    
           DISPLAY '**   DATE D EPURATION   : ' GFJMSA-5                        
                   '          **'                                               
           CLOSE FMOIS.                                                         
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FFS052                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           READ FFS052 AT END                                                   
                SET FIN-FFS052 TO TRUE                                          
           END-READ.                                                            
           IF NOT FIN-FFS052                                                    
              ADD 1 TO WS-CPT-FFS052                                            
           END-IF.                                                              
      *                                                                         
      *---------------------------------------                                  
       APPEL-BETDATC                  SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           CALL  'BETDATC' USING WORK-BETDATC                                   
           IF GFVDAT NOT = '1'                                                  
              MOVE GF-MESS-ERR  TO TRACE-MESS                                   
              PERFORM ABANDON-BATCH                                             
           END-IF.                                                              
      *                                                                         
      ******************************************************************        
      *    MODULE D' ABANDON                                           *        
      ******************************************************************        
      *                                                                         
       99-COPY SECTION. CONTINUE. COPY SYBCERRO.                                
      *                                                                         
      *                                                                         
