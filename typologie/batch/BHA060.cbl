      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00020040
       PROGRAM-ID.                BHA060.                               00030040
       AUTHOR.                    DSA030.                               00040040
      * ************************************************************** +00050040
      *                 AJOUT PRIMES OPERATEURS SUR RTPV00             *00060040
      * ************************************************************** +00070040
      *  PROJET     : MANTIS 10047  CREATION  ETAT POUR GATEWAY        *00080040
      *  PROGRAMME  : BHA060                                           *00090040
      *  CREATION   : 09/06/11  J.BICHY                                *00100040
      *  FONCTION   : DEMANDE D'EXTRACT �QUIVALENTE IHA030             *00110040
      ******************************************************************00120040
634998* E-CARE N� 634998 :  - INCIDENT - 907640 " VENTES -  - STOCK    *00121042
634998* N'APPARAIT PAS SUR LE LISTING                                  *00121142
      ******************************************************************00122042
       ENVIRONMENT DIVISION.                                            00130040
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00140040
      *{ Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                      
      *SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00150040
      *--                                                                       
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA                                    
           COPY MW-COLSEQ.                                                      
      *}                                                                        
      *}                                                                        
       INPUT-OUTPUT SECTION.                                            00160040
                                                                        00170040
       FILE-CONTROL.                                                    00180040
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IHA030  ASSIGN TO IHA030  FILE STATUS IS ST-IHA030.  00190040
      *--                                                                       
            SELECT IHA030  ASSIGN TO IHA030  FILE STATUS IS ST-IHA030           
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FIHA060 ASSIGN TO FIHA060 FILE STATUS IS ST-FIHA060. 00200040
      *                                                                         
      *--                                                                       
            SELECT FIHA060 ASSIGN TO FIHA060 FILE STATUS IS ST-FIHA060          
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00210040
      *}                                                                        
       DATA DIVISION.                                                   00220040
       FILE SECTION.                                                    00230040
                                                                        00240040
       FD  IHA030                                                       00250040
           RECORDING MODE IS F                                          00260040
           BLOCK  CONTAINS 0                                            00270040
           LABEL RECORD STANDARD.                                       00280040
       01  IHA030-RECORD           PIC X(512).                          00290040
                                                                        00300040
       FD  FIHA060                                                      00310040
           RECORDING F                                                  00320040
           BLOCK 0 RECORDS                                              00330040
           LABEL RECORD STANDARD.                                       00340040
       01  FIHA060-RECORD.                                              00350040
           05  FIHA060-LIGNE.                                           00360040
               10  FILLER        PIC X(255).                            00370040
                                                                        00380040
                                                                        00390040
       WORKING-STORAGE SECTION.                                         00400040
      *========================                                         00410040
                                                                        00420040
       77 FILLER                    PIC X(10) VALUE '$WORKING$'.        00430040
       77 FILLER                    PIC X(10) VALUE '$COMPTEU$'.        00440040
       77 W-CPT-LEC-IHA30           PIC 9(06) VALUE 0.                  00450040
       77 W-CPT-ECRITURES           PIC 9(06) VALUE 0.                  00460040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  WRET                      PIC S9(4) COMP.                    00470040
      *--                                                                       
       77  WRET                      PIC S9(4) COMP-5.                          
      *}                                                                        
       77 WC-BETDATC                 PIC X(08) VALUE 'BETDATC'.         00480040
       77 WS-WHEN-COMPILED           PIC X(16) VALUE SPACE.             00490040
      *                                                                 00500040
      ***************************************************************** 00510040
       01 WS-DATE-COMPILE.                                              00520040
          05 WS-COMPILE-JJ        PIC XX.                               00530040
          05 FILLER               PIC X     VALUE '/'.                  00540040
          05 WS-COMPILE-MM        PIC XX.                               00550040
          05 FILLER               PIC XXX   VALUE '/20'.                00560040
          05 WS-COMPILE-AA        PIC XX.                               00570040
          05 FILLER               PIC X(2).                             00580040
          05 WS-COMPILE-HE        PIC X(2).                             00590040
          05 FILLER               PIC X(1)  VALUE ':'.                  00600040
          05 WS-COMPILE-MI        PIC X(2).                             00610040
          05 FILLER               PIC X(1)  VALUE ':'.                  00620040
          05 WS-COMPILE-SE        PIC X(2).                             00630040
                                                                        00640040
       01 WN-DATHEUR              PIC 9(13).                            00650040
       01 WS-DATHEUR REDEFINES WN-DATHEUR.                              00660040
          05 WS-DATHEUR-D         PIC X(5).                             00670040
          05 WS-DATHEUR-H         PIC X(6).                             00680040
          05 FILLER               PIC X(2).                             00690040
                                                                        00700040
       01 WS-TRANSCO9             PIC    ZZZZ9.                         00710040
                                                                        00720040
       01 WS-RUPTURES-IHA030.                                           00730040
           05 WS-NSOCLAB            PIC X(03)       VALUE SPACE.        00740040
           05 WS-NLAB               PIC X(03)       VALUE SPACE.        00750040
           05 WS-NSOCIETE           PIC X(03)       VALUE SPACE.        00760040
           05 WS-NDEPOT             PIC X(03)       VALUE SPACE.        00770040
                                                                        00780040
       01  WS-CPT-ECRIT-IHA         PIC S9(7)       COMP-3 VALUE ZERO.  00790040
                                                                        00800040
           COPY IHA030.                                                 00810040
       01  ST-IHA030                 PIC 99         VALUE ZERO.         00820040
                                                                        00830040
           COPY FBHA060.                                                00840040
       01  ST-FIHA060                PIC 99         VALUE ZERO.         00850040
                                                                        00860040
           COPY WORKDATC.                                               00870040
                                                                        00880040
       01 ETAT-FICHIER-IHA030 PIC X VALUE '1'.                          00890040
           88 NON-FIN-IHA030        VALUE '1'.                          00900040
           88 FIN-IHA030            VALUE '0'.                          00910040
                                                                        00920040
                                                                        00930040
       01 Z-INOUT                    PIC X(236) VALUE SPACES.           00940040
           COPY SYBWERRO.                                               00950040
           COPY ABENDCOP.                                               00960040
                                                                        00970040
      ****************************************************************  00980040
      *                   DEBUT DU PROGRAMME                         *  00990040
      ****************************************************************  01000040
       PROCEDURE DIVISION.                                              01010040
      *===================                                              01020040
                                                                        01030040
           PERFORM INITIALISATION                                       01040040
              THRU FIN-INITIALISATION.                                  01050040
                                                                        01060040
           PERFORM TRAITEMENT-IHA030  UNTIL FIN-IHA030.                 01070040
                                                                        01080040
           PERFORM TRAITEMENT-SORTIE                                    01090040
              THRU FIN-TRAITEMENT-SORTIE.                               01100040
                                                                        01110040
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    01120040
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        01130040
      * //////////////////////////////////////////////////////////// *  01140040
      *                  SECTIONS DE LA BOUCLE PRINCIPALE            *  01150040
      * //////////////////////////////////////////////////////////// *  01160040
       INITIALISATION SECTION.                                          01170040
      *=======================                                          01180040
                                                                        01190040
           DISPLAY '***************************************************'01200040
           DISPLAY '*       DEBUT PROGRAMME BHA060                    *'01210040
           DISPLAY '***************************************************'01220040
           MOVE WHEN-COMPILED            TO WS-WHEN-COMPILED            01230040
           MOVE WS-WHEN-COMPILED(4:2)    TO WS-COMPILE-JJ               01240040
           MOVE WS-WHEN-COMPILED(7:2)    TO WS-COMPILE-AA               01250040
           MOVE WS-WHEN-COMPILED(1:2)    TO WS-COMPILE-MM               01260040
           MOVE WS-WHEN-COMPILED(9:2)    TO WS-COMPILE-HE               01270040
           MOVE WS-WHEN-COMPILED(12:2)   TO WS-COMPILE-MI               01280040
           MOVE WS-WHEN-COMPILED(15:2)   TO WS-COMPILE-SE               01290040
           DISPLAY 'COMPILE DU ' WS-DATE-COMPILE                        01300040
           DISPLAY '     '                                              01310040
           MOVE 'BHA060'                 TO ABEND-PROG                  01320040
                                                                        01330040
                                                                        01340040
           OPEN INPUT  IHA030.                                          01350040
           OPEN OUTPUT FIHA060.                                         01360040
                                                                        01370040
           SET NON-FIN-IHA030 TO TRUE                                   01380040
      *    1ERE LECTURE SUR IHA030                                      01390040
           PERFORM LECTURE-IHA030 .                                     01400040
           IF ST-IHA030 = 10                                            01410040
              DISPLAY 'FICHIER IHA030 VIDE : SORTIE PGM'                01420040
              PERFORM TRAITEMENT-SORTIE                                 01430040
           END-IF.                                                      01440040
                                                                        01450040
       FIN-INITIALISATION.  EXIT.                                       01460040
      *-------------------                                              01470040
                                                                        01480040
                                                                        01490040
       TRAITEMENT-IHA030 SECTION.                                       01500040
      *==========================                                       01510040
           MOVE RUPTURES-IHA030(1:12)     TO   WS-RUPTURES-IHA030       01520040
           PERFORM    ECRITURE-ENTETE-FIHA060                           01530040
           PERFORM UNTIL RUPTURES-IHA030(1:12) NOT = WS-RUPTURES-IHA030 01540040
           OR FIN-IHA030                                                01550040
              PERFORM    ECRITURE-FIHA060                               01560040
              PERFORM    LECTURE-IHA030                                 01570040
                                                                        01580040
           END-PERFORM.                                                 01590040
                                                                        01600040
       FIN-TRAITEMENT-IHA030.   EXIT.                                   01610040
      *----------------------                                           01620040
                                                                        01630040
                                                                        01640040
                                                                        01650040
       LECTURE-IHA030 SECTION.                                          01660040
      *=======================                                          01670040
                                                                        01680040
           READ IHA030 INTO DSECT-IHA030                                01690040
                AT END SET FIN-IHA030 TO TRUE.                          01700040
           ADD 1  TO W-CPT-LEC-IHA30 .                                  01710040
                                                                        01720040
       FIN-LECTURE-FPV500 . EXIT.                                       01730040
      *--------------------                                             01740040
                                                                        01750040
                                                                        01760040
       ECRITURE-ENTETE-FIHA060 SECTION.                                 01770040
      *==========================                                       01780040
           INITIALIZE    W-ENR-FBHA06                                   01790040
           MOVE  ' FAMILLE '              TO FBHA06-FAMILLE             01800040
           MOVE  'MARQUE'                 TO FBHA06-MARQUE              01810040
           MOVE  ' CODIC '                TO FBHA06-CODIC               01820040
           MOVE  '    REFERENCE        '  TO FBHA06-REF                 01830040
           MOVE  ' STA '                  TO FBHA06-STA                 01840040
           MOVE  'MUTATION'               TO FBHA06-MUTATION            01850040
           MOVE  'DATE DE 1ERE RECEPTION' TO FBHA06-1ERE-RECEP          01860040
           MOVE  'RECEPTION EFFECTUEE'    TO FBHA06-RECEP-EFF           01870040
           MOVE  'STOCK DISPO '           TO FBHA06-STK-DISPO           01880040
           MOVE  'QTE MIS DE COTE      '  TO FBHA06-RESERVE             01890040
           MOVE  'DEPOT'                  TO FBHA06-ENTREPOT            01900040
           MOVE  WS-NSOCIETE              TO FBHA06-SOCIETE             01910040
           MOVE  WS-NDEPOT                TO FBHA06-DEPOT               01920040
           WRITE FIHA060-RECORD FROM W-ENR-FBHA06                       01930040
           ADD 1 TO W-CPT-ECRITURES.                                    01940040
                                                                        01950040
       FIN-ECRITURE-ENTETE-FIHA060. EXIT.                               01960040
      *---------------------                                            01970040
                                                                        01980040
       ECRITURE-FIHA060 SECTION.                                        01990040
      *==========================                                       02000040
                                                                        02010040
           INITIALIZE    W-ENR-FBHA06                                   02020040
           MOVE  IHA030-CFAM             TO FBHA06-FAMILLE              02030040
           MOVE  IHA030-CMARQ            TO FBHA06-MARQUE               02040040
           MOVE  IHA030-NCODIC           TO FBHA06-CODIC                02050040
           MOVE  IHA030-LREFFOURN        TO FBHA06-REF                  02060040
           MOVE  IHA030-LSTATCOMP        TO FBHA06-STA                  02070040
           IF    IHA030-QSTOCKV          >  ZERO                        02071043
                 MOVE  IHA030-QSTOCKV    TO WS-TRANSCO9                 02080043
                 MOVE  WS-TRANSCO9       TO FBHA06-STK-DISPO            02090043
           ELSE                                                         02090143
                 MOVE  SPACES            TO FBHA06-STK-DISPO            02090243
           END-IF                                                       02091043
           IF    IHA030-QSTOCKC          >  ZERO                        02092043
                 MOVE  IHA030-QSTOCKC    TO WS-TRANSCO9                 02100043
                 MOVE  WS-TRANSCO9       TO FBHA06-RESERVE              02101043
           ELSE                                                         02101143
                 MOVE  SPACES            TO FBHA06-RESERVE              02101243
           END-IF                                                       02102043
                                                                        02109040
      *  POUR UN PRODUIT DONT LA DATE DE RECEPTION EST FUTURE           02110040
           IF    IHA030-DATE3         > SPACE                           02120040
                 MOVE  SPACES         TO FBHA06-MUTATION                02130040
                 MOVE  SPACES         TO FBHA06-1ERE-RECEP              02140040
                 MOVE  SPACES         TO FBHA06-RECEP-EFF               02150040
                 MOVE  SPACES         TO FBHA06-STK-DISPO               02160040
                 MOVE  SPACES         TO FBHA06-RESERVE                 02170040
           END-IF                                                       02180040
                                                                        02181040
      *POUR 1 PRODUIT DONT LA DATE EST PASSEE ET RECEPTION NON EFFECTUEE02182040
           IF    IHA030-DATE1         >  SPACE                          02183040
      *    AND   IHA030-FLAG    NOT   =  'R'                            02184042
634998     AND   IHA030-QSTOCKC       =  0                              02184142
634998     AND   IHA030-QSTOCKV       =  0                              02184242
                 MOVE  SPACES         TO FBHA06-MUTATION                02185040
                 MOVE  IHA030-DATE1   TO FBHA06-1ERE-RECEP              02186040
                 MOVE  SPACES         TO FBHA06-RECEP-EFF               02187040
                 MOVE  SPACES         TO FBHA06-STK-DISPO               02188040
                 MOVE  SPACES         TO FBHA06-RESERVE                 02189040
           END-IF                                                       02189140
                                                                        02189240
      *  POUR UN PRODUIT DONT LA DATE EST PASSEE ET RECEPTION EFFECTUEE 02189340
      *  ET STOCK  OU MIS DE COTE                                       02189440
           IF    IHA030-DATE1         > SPACE                           02189540
           AND   IHA030-FLAG          = 'R'                             02189640
                 MOVE  'O'            TO FBHA06-MUTATION                02189740
                 MOVE  IHA030-DATE1   TO FBHA06-1ERE-RECEP              02189840
                 MOVE  'O'            TO FBHA06-RECEP-EFF               02189940
                 MOVE  IHA030-QSTOCKV TO WS-TRANSCO9                    02190040
                 MOVE  WS-TRANSCO9    TO FBHA06-STK-DISPO               02190140
                 MOVE  IHA030-QSTOCKC TO WS-TRANSCO9                    02190240
                 MOVE  WS-TRANSCO9    TO FBHA06-RESERVE                 02190340
           END-IF                                                       02190440
                                                                        02190540
      *  POUR UN PRODUIT DONT LA DATE EST PASSEE ET RECEPTION EFFECTUEE 02190640
      *  PAS DE STOCK  PAS DE MIS DE COTE                               02190740
           IF    IHA030-DATE1         > SPACE                           02190840
           AND   IHA030-FLAG          = 'R'                             02190940
           AND   IHA030-QSTOCKV       =  ZERO                           02191040
           AND   IHA030-QSTOCKC       =  ZERO                           02191140
                 MOVE  'O'            TO FBHA06-MUTATION                02191240
                 MOVE  IHA030-DATE1   TO FBHA06-1ERE-RECEP              02191340
                 MOVE  'O'            TO FBHA06-RECEP-EFF               02191440
                 MOVE  SPACES         TO FBHA06-STK-DISPO               02191540
                 MOVE  SPACES         TO FBHA06-RESERVE                 02191640
           END-IF                                                       02191740
                                                                        02191840
           WRITE FIHA060-RECORD  FROM W-ENR-FBHA06                      02240040
           ADD 1 TO W-CPT-ECRITURES.                                    02250040
                                                                        02260040
       FIN-ECRITURE-FIHA060. EXIT.                                      02270040
      *---------------------                                            02280040
                                                                        02290040
                                                                        02300040
       TRAITEMENT-SORTIE SECTION.                                       02310040
      *==========================                                       02320040
                                                                        02330040
           CLOSE IHA030 FIHA060.                                        02340040
                                                                        02350040
           DISPLAY '***************************************************'02360040
           DISPLAY '*        FIN D''EXECUTION BHA060                  *'02370040
           DISPLAY '***************************************************'02380040
           DISPLAY ' -  NBRE DE LECTURE IHA030    : ' W-CPT-LEC-IHA30.  02390040
           DISPLAY ' -  NBRE D''ECRITURES FIHA060  : ' W-CPT-ECRITURES. 02400040
                                                                        02410040
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    02420040
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
                                                                        02430040
       FIN-TRAITEMENT-SORTIE.     EXIT.                                 02440040
      *----------------------                                           02450040
                                                                        02460040
                                                                        02470040
