      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                         00000010
       PROGRAM-ID.       BBS203.                                        00000020
       DATE-COMPILED.                                                   00000030
       AUTHOR.           DSA046.                                        00000040
      *-----------------------------------------------------------------00000050
      * F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E     00000060
      *-----------------------------------------------------------------00000070
      *  PROJET     : BIENS ET SERVICES                                 00000080
      *  PROGRAMME  : BBS203                                            00000090
      *  CREATION   : JUILLET 2003                                      00000100
      *  FONCTION   : POUR CHAQUE OFFRE / SOCIETE / ZONE DE PRIX :      00000110
      *               - SI UNE LISTE DE CODICS FAIT PARTIE DE L'OFFRE:  00000120
      *                 GARDER LE CODIC AVEC (PRIX - COMM OPERATEUR) MAX00000130
      *               - CALCULS DE :                                    00000140
      *                 * STOCK MAG (STOCK MAG MINI PARMI LES CODICS    00000150
      *                              CONSTITUTIFS DE L'OFFRE  )         00000160
      *                 * STOCK DEPOT (STOCK DEPOT MINI PARMI LES CODICS00000170
      *                              CONSTITUTIFS DE L'OFFRE  )         00000180
      *                 * PRA, PRMP DE L'OFFRE : SOMME DES PRA ET PRMP  00000190
      *                   DES CODICS CONSTITUTIFS DE L'OFFRE            00000200
      *-----------------------------------------------------------------00000210
      *    M O D I F I C A T I O N S / M A I N T E N A N C E S          00000220
      *-----------------------------------------------------------------00000230
      *  DATE       : ...                                               00000240
      *  AUTEUR     : ...                                               00000250
      *  REPAIRE    : ...                                               00000260
      *  OBJET      : ...                                               00000270
      *-----------------------------------------------------------------00000280
       ENVIRONMENT DIVISION.                                            00000290
       CONFIGURATION SECTION.                                           00000300
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                           00000310
       INPUT-OUTPUT SECTION.                                            00000320
       FILE-CONTROL.                                                    00000330
                                                                        00000340
      ***************************************************************** 00000350
      * FBS202  : FICHIER CONTENANT LES INFOS SUR LES OFFRES EN ENTREE* 00000360
      * FBS203  : FICHIER EN SORTIE                                   * 00000370
      ***************************************************************** 00000380
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FBS202   ASSIGN TO FBS202   FILE STATUS IS ST-FBS202.00000390
      *--                                                                       
            SELECT FBS202   ASSIGN TO FBS202   FILE STATUS IS ST-FBS202         
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FBS203   ASSIGN TO FBS203   FILE STATUS IS ST-FBS203.00000400
      *                                                                         
      *--                                                                       
            SELECT FBS203   ASSIGN TO FBS203   FILE STATUS IS ST-FBS203         
              ORGANIZATION LINE SEQUENTIAL.                                     
                                                                        00000410
      *}                                                                        
      *-----------------------------------------------------------------00000420
                                                                        00000430
       DATA DIVISION.                                                   00000440
       FILE SECTION.                                                    00000450
                                                                        00000460
                                                                        00000470
       FD  FBS203                                                       00000480
           RECORDING MODE IS F                                          00000490
           BLOCK 0 RECORDS                                              00000500
           LABEL RECORD STANDARD.                                       00000510
       01  FBS203-ENR       PIC X(360).                                 00000520
                                                                        00000530
       FD  FBS202                                                       00000540
           RECORDING MODE IS F                                          00000550
           BLOCK 0 RECORDS                                              00000560
           LABEL RECORD STANDARD.                                       00000570
       01  FBS202-ENR       PIC X(270).                                 00000580
                                                                        00000590
      *-----------------------------------------------------------------00000600
                                                                        00000610
       WORKING-STORAGE SECTION.                                         00000620
                                                                        00000630
      *--  GESTION APPEL MODULE                                         00000640
                                                                        00000650
       01  WNMODULE                  PIC X(8).                          00000660
                                                                        00000670
      *--  GESTION DES DISPLAYS                                         00000680
                                                                        00000690
       01  WEDITIO9                  PIC Z(06)9          VALUE ZERO.    00000700
       01  WEDITIOX                  PIC X(8)            VALUE SPACE.   00000710
       01  WEDITIOX-42               PIC X(42)           VALUE SPACE.   00000720
                                                                        00000730
      *--  GESTION AFFICHAGE TEMPS                                      00000740
                                                                        00000750
       01  W-TIME                    PIC 9(8)            VALUE ZERO.    00000760
       01  W-ETAPE                   PIC X(15)           VALUE SPACE.   00000770
                                                                        00000780
                                                                        00000790
      *--  GESTION CARTE FBS202                                         00000800
           COPY FBS202.                                                 00000810
                                                                        00000820
      *--  GESTION CARTE FBS203                                         00000830
           COPY FBS203.                                                 00000840
                                                                        00000850
      *--  VARIABLES DE TRAVAIL                                         00000860
                                                                        00000870
       01  W-CLE.                                                       00000880
           05 W-OFFRE         PIC X(7).                                 00000890
           05 W-NSOCIETE      PIC XXX.                                  00000900
           05 W-ZPRIX         PIC XX.                                   00000910
                                                                        00000920
       77  W-CLE-SAVE         PIC X(12).                                00000930
       77  W-NOM-TAB          PIC X(04).                                00000940
       01  W-VAR-SAVE.                                                  00000950
           05 W-NSEQENT-SAVE   PIC S9(3) COMP-3.                        00000960
           05 W-NENTMAX-SAVE   PIC S9(3) COMP-3.                        00000970
           05 W-RESULTAT-SAVE  PIC S9(5)V99 COMP-3.                     00000980
                                                                        00000990
       01  W-VAR-STOCK.                                                 00001000
           05 W-QSTOCKDEP       PIC S9(5) COMP-3.                       00001010
           05 W-QSTOCKRES       PIC S9(5) COMP-3.                       00001020
           05 W-DCDE            PIC S9(5) COMP-3.                       00001030
           05 W-QCDE            PIC S9(5) COMP-3.                       00001040
           05 W-QCDERES         PIC S9(5) COMP-3.                       00001050
           05 W-TOTQCDE         PIC S9(5) COMP-3.                       00001060
           05 W-TOTQCDERES      PIC S9(5) COMP-3.                       00001070
           05 W-QSTOCKMAG       PIC S9(5) COMP-3.                       00001080
           05 W-QMUTATT         PIC S9(5) COMP-3.                       00001090
           05 W-NSOCDEPOT       PIC X(3).                               00001100
           05 W-NDEPOT          PIC X(3).                               00001110
       01  W-VAR-SOMME.                                                 00001120
           05 W-PRA            PIC S9(7)V99   COMP-3.                   00001130
           05 W-PRMP           PIC S9(7)V9(6) COMP-3.                   00001140
           05 W-SRP            PIC S9(7)V9(6) COMP-3.                   00001150
           05 W-MONTANT        PIC S9(5)V9(2) COMP-3.                   00001160
           05 W-MONTANT-MAX    PIC S9(5)V9(2) COMP-3.                   00001170
           05 W-REFI           PIC S9(5)V9(2) COMP-3.                   00001180
      * ON GARDE CES DONN�ES POUR LE PREMIER CODIC                      00001190
       01  W-VAR-NCODIC.                                                00001200
           05 W-NCODIC-1        PIC X(7).                               00001210
           05 W-PRA-1           PIC S9(7)V99   COMP-3.                  00001220
           05 W-PRMP-1          PIC S9(7)V9(6) COMP-3.                  00001230
           05 W-SRP-1           PIC S9(7)V9(6) COMP-3.                  00001240
           05 W-PREF1-1         PIC S9(5)V9(2) COMP-3.                  00001250
           05 W-QSTOCKDEP-1     PIC S9(5) COMP-3.                       00001260
           05 W-QSTOCKRES-1     PIC S9(5) COMP-3.                       00001270
           05 W-DCDE-1          PIC S9(5) COMP-3.                       00001280
           05 W-QCDE-1          PIC S9(5) COMP-3.                       00001290
           05 W-QCDERES-1       PIC S9(5) COMP-3.                       00001300
           05 W-TOTQCDE-1       PIC S9(5) COMP-3.                       00001310
           05 W-TOTQCDERES-1    PIC S9(5) COMP-3.                       00001320
           05 W-QSTOCKMAG-1     PIC S9(5) COMP-3.                       00001330
           05 W-QMUTATT-1       PIC S9(5) COMP-3.                       00001340
           05 W-QPIECES0-HV04-1 PIC S9(5) COMP-3.                       00001350
           05 W-QPIECES1-HV04-1 PIC S9(5) COMP-3.                       00001360
           05 W-QPIECES2-HV04-1 PIC S9(5) COMP-3.                       00001370
           05 W-QPIECES3-HV04-1 PIC S9(5) COMP-3.                       00001380
           05 W-QPIECES4-HV04-1 PIC S9(5) COMP-3.                       00001390
           05 W-QPIECES0-HV12-1 PIC S9(5) COMP-3.                       00001400
           05 W-QPIECES1-HV12-1 PIC S9(5) COMP-3.                       00001410
           05 W-QPIECES2-HV12-1 PIC S9(5) COMP-3.                       00001420
           05 W-QPIECES3-HV12-1 PIC S9(5) COMP-3.                       00001430
           05 W-QPIECES4-HV12-1 PIC S9(5) COMP-3.                       00001440
           05 W-PCOMMART-1      PIC S9(5)V99   COMP-3.                  00001450
                                                                        00001460
       01  W-FLAG-BOUCLE      PIC X.                                    00001470
           88  BOUCLE-OK      VALUE 'O'.                                00001480
           88  BOUCLE-KO      VALUE 'N'.                                00001490
                                                                        00001500
       01  W-FLAG-WSENSAPPRO  PIC X.                                    00001510
           88  WSENSAPPRO-N   VALUE 'O'.                                00001520
           88  WSENSAPPRO-O   VALUE 'Y'.                                00001530
                                                                        00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-NBR-LEC-FBS202   PIC S9(4) COMP.                           00001550
      *--                                                                       
       77  W-NBR-LEC-FBS202   PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-NBR-ECR-FBS203   PIC S9(4) COMP.                           00001560
      *                                                                         
      *--                                                                       
       77  W-NBR-ECR-FBS203   PIC S9(4) COMP-5.                                 
                                                                        00001570
      *}                                                                        
      *--  GESTION EN TABLEAU DES ELEMENTS DE LA LISTE                  00001580
                                                                        00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  MAX-203-NBP                PIC S9(08) COMP   VALUE 100.      00001600
      *--                                                                       
       01  MAX-203-NBP                PIC S9(08) COMP-5   VALUE 100.            
      *}                                                                        
       01  TABLEAU-FBS203.                                              00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 I-203                   PIC S9(04) COMP   VALUE 0.        00001620
      *--                                                                       
           05 I-203                   PIC S9(04) COMP-5   VALUE 0.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IND-203-NBP             PIC S9(04) COMP   VALUE 0.        00001630
      *--                                                                       
           05 IND-203-NBP             PIC S9(04) COMP-5   VALUE 0.              
      *}                                                                        
           05 TAB-203 OCCURS 100.                                       00001640
            07 T-203-DONNEES.                                           00001650
              10 T-203-NCODIC                   PIC X(07).              00001660
              10 T-203-OFFRE                    PIC X(07).              00001670
              10 T-203-PREF1                    PIC S9(5)V99   COMP-3.  00001680
              10 T-203-PREF2                    PIC S9(5)V99   COMP-3.  00001690
              10 T-203-WOA                      PIC X(01).              00001700
              10 T-203-INFOS.                                           00001710
                 15 T-203-LREFFOURN             PIC X(20).              00001720
                 15 T-203-CFAM                  PIC X(05).              00001730
                 15 T-203-CMARQ                 PIC X(05).              00001740
                 15 T-203-LSTATCOMP             PIC X(03).              00001750
                 15 T-203-CHEFPROD              PIC X(05).              00001760
                 15 T-203-CTAUXTVA              PIC X(05).              00001770
                 15 T-203-DEFSTATUT             PIC X(08).              00001780
                 15 T-203-WSEQED                PIC X(02).              00001790
                 15 T-203-NAGREGATED            PIC X(02).              00001800
                 15 T-203-LAGREGATED            PIC X(20).              00001810
                 15 T-203-CPROFASS              PIC X(05).              00001820
                 15 T-203-200-FILLER            PIC X(13).              00001830
                 15 T-203-NSEQENT               PIC S9(3)      COMP-3.  00001840
                 15 T-203-NENTMIN               PIC S9(3)      COMP-3.  00001850
                 15 T-203-NENTMAX               PIC S9(3)      COMP-3.  00001860
                 15 T-203-WPRIME                PIC X(01).              00001870
                 15 T-203-WPRIX                 PIC X(01).              00001880
                 15 T-203-TYPE                  PIC XX.                 00001890
                 15 T-203-MONTANT               PIC S9(5)V99   COMP-3.  00001900
                 15 T-203-MONTANT-MAX           PIC S9(5)V99   COMP-3.  00001910
                 15 T-203-REFI                  PIC S9(5)V99   COMP-3.  00001920
                 15 T-203-WIMPERATIF            PIC X(1).               00001930
                 15 T-203-WBLOQUE               PIC X(1).               00001940
                 15 T-203-201-FILLER            PIC X(09).              00001950
              10 T-203-VAR-NSOCIETE.                                    00001960
                 15 T-203-NSOCIETE              PIC X(03).              00001970
                 15 T-203-PRA                   PIC S9(7)V99   COMP-3.  00001980
                 15 T-203-PRMP                  PIC S9(7)V9(6) COMP-3.  00001990
                 15 T-203-SRP                   PIC S9(7)V9(6) COMP-3.  00002000
                 15 T-203-VAR-ZPRIX.                                    00002010
                    20 T-203-ZPRIX              PIC X(02).              00002020
                    20 T-203-NSOCDEPOT          PIC X(03).              00002030
                    20 T-203-NDEPOT             PIC X(03).              00002040
                    20 T-203-QPIECES0-HV04      PIC S9(5)      COMP-3.  00002050
                    20 T-203-QPIECES1-HV04      PIC S9(5)      COMP-3.  00002060
                    20 T-203-QPIECES2-HV04      PIC S9(5)      COMP-3.  00002070
                    20 T-203-QPIECES3-HV04      PIC S9(5)      COMP-3.  00002080
                    20 T-203-QPIECES4-HV04      PIC S9(5)      COMP-3.  00002090
                    20 T-203-QPIECES0-HV12      PIC S9(5)      COMP-3.  00002100
                    20 T-203-QPIECES1-HV12      PIC S9(5)      COMP-3.  00002110
                    20 T-203-QPIECES2-HV12      PIC S9(5)      COMP-3.  00002120
                    20 T-203-QPIECES3-HV12      PIC S9(5)      COMP-3.  00002130
                    20 T-203-QPIECES4-HV12      PIC S9(5)      COMP-3.  00002140
                    20 T-203-QSTOCKMAGD         PIC S9(5)      COMP-3.  00002150
                    20 T-203-QMUTATT            PIC S9(5)      COMP-3.  00002160
                    20 T-203-WSENSAPPRO         PIC X(01).              00002170
                    20 T-203-QSTOCKDEP          PIC S9(5)      COMP-3.  00002180
                    20 T-203-QSTOCKRES          PIC S9(5)      COMP-3.  00002190
                    20 T-203-DCDE               PIC X(08).              00002200
                    20 T-203-QCDE               PIC S9(5)      COMP-3.  00002210
                    20 T-203-QCDERES            PIC S9(5)      COMP-3.  00002220
                    20 T-203-TOTQCDE            PIC S9(5)      COMP-3.  00002230
                    20 T-203-TOTQCDERES         PIC S9(5)      COMP-3.  00002240
                    20 T-203-PSTDTTC            PIC S9(7)V99   COMP-3.  00002250
                    20 T-203-PCOMMART           PIC S9(5)V99   COMP-3.  00002260
                    20 T-203-PCOMMVOL           PIC S9(5)V99   COMP-3.  00002270
                    20 T-203-CPT-PEXPTTC        PIC S9(5)      COMP-3.  00002280
            07 T-203-FLAG                       PIC X.                  00002290
              88   T-203-CODIC-OK                     VALUE 'O'.        00002300
              88   T-203-CODIC-KO                     VALUE 'K'.        00002310
                                                                        00002320
      *--  TABLEAU DE TRAVAIL                                           00002330
                                                                        00002340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 MAX-TRV-NBP                 PIC S9(08) COMP   VALUE 999.      00002350
      *--                                                                       
       01 MAX-TRV-NBP                 PIC S9(08) COMP-5   VALUE 999.            
      *}                                                                        
       01  TABLEAU-TRV.                                                 00002360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 I-TRV                  PIC S9(04) COMP   VALUE 0.         00002370
      *--                                                                       
           05 I-TRV                  PIC S9(04) COMP-5   VALUE 0.               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05 IND-TRV-NBP             PIC S9(04) COMP   VALUE 0.        00002380
      *--                                                                       
           05 IND-TRV-NBP             PIC S9(04) COMP-5   VALUE 0.              
      *}                                                                        
           05 TAB-TRV OCCURS 999.                                       00002390
              10 T-TRV-NCODIC           PIC X(7).                       00002400
              10 T-TRV-RESULTAT         PIC S9(5)V99 COMP-3.            00002410
                                                                        00002420
      *--  GESTION FIN DE FICHIER                                       00002430
       01 W-FLAG                      PIC X VALUE SPACES.               00002440
          88 FIN-FBS202               VALUE 'O'.                        00002450
                                                                        00002460
      *--  GESTION AIDA                                                 00002470
                                                                        00002480
           COPY  ABENDCOP.                                              00002490
           COPY  WORKDATC.                                              00002500
       01  Z-INOUT                   PIC X(236)          VALUE SPACES.  00002510
           COPY  SYBWERRO.                                              00002520
           COPY  SYBWDIV0.                                              00002530
                                                                        00002540
       LINKAGE SECTION.                                                 00002550
       PROCEDURE DIVISION.                                              00002560
                                                                        00002570
      *---------------------------------------                          00002580
       MODULE-BBS203                  SECTION.                          00002590
      *---------------------------------------                          00002600
                                                                        00002610
           PERFORM MODULE-ENTREE.                                       00002620
           PERFORM MODULE-TRAITEMENT.                                   00002630
           PERFORM MODULE-SORTIE.                                       00002640
                                                                        00002650
       FIN-MODULE-BBS203. EXIT.                                         00002660
                                                                        00002670
      *---------------------------------------                          00002680
       MODULE-ENTREE                  SECTION.                          00002690
      *---------------------------------------                          00002700
                                                                        00002710
           DISPLAY  '************************************************'. 00002720
           DISPLAY  '**                                            **'. 00002730
           DISPLAY  '**              PROGRAMME BBS203              **'. 00002740
           DISPLAY  '**                                            **'. 00002750
           MOVE WHEN-COMPILED   TO Z-WHEN-COMPILED.                     00002760
           STRING Z-WHEN-COMPILED(4:2) '/' Z-WHEN-COMPILED(1:2) '/'     00002770
                  Z-WHEN-COMPILED(7:2) DELIMITED BY SIZE INTO WEDITIOX  00002780
           DISPLAY                                                      00002790
           '**      DATE DE COMPILE (JJ/MM/AA): '   WEDITIOX    '  **'. 00002800
           DISPLAY  '**                                            **'. 00002810
           DISPLAY  '************************************************'. 00002820
                                                                        00002830
           OPEN INPUT  FBS202                                           00002840
           OPEN OUTPUT FBS203                                           00002850
                                                                        00002860
           PERFORM LECTURE-FBS202                                       00002870
           .                                                            00002880
       FIN-MODULE-ENTREE. EXIT.                                         00002890
                                                                        00002900
      *---------------------------------------                          00002910
       MODULE-TRAITEMENT              SECTION.                          00002920
      *---------------------------------------                          00002930
                                                                        00002940
           MOVE 'DEB TRT  ' TO W-ETAPE    PERFORM AFF-TEMPS.            00002950
           PERFORM UNTIL FIN-FBS202                                     00002960
              IF W-CLE NOT = W-CLE-SAVE                                 00002970
                 IF W-CLE-SAVE > SPACES                                 00002980
                    PERFORM TRAITEMENT-PRINCIPAL                        00002990
                 END-IF                                                 00003000
                 INITIALIZE TABLEAU-FBS203                              00003010
                 SET WSENSAPPRO-N TO TRUE                               00003020
                 MOVE W-CLE TO W-CLE-SAVE                               00003030
              END-IF                                                    00003040
              PERFORM FILL-TABLEAU-FBS203                               00003050
              PERFORM LECTURE-FBS202                                    00003060
           END-PERFORM                                                  00003070
           IF W-CLE-SAVE > SPACES                                       00003080
              PERFORM TRAITEMENT-PRINCIPAL                              00003090
           END-IF                                                       00003100
           MOVE 'FIN TRT '  TO W-ETAPE    PERFORM AFF-TEMPS.            00003110
                                                                        00003120
       FIN-MODULE-TRAITEMENT. EXIT.                                     00003130
                                                                        00003140
      *---------------------------------------                          00003150
       LECTURE-FBS202                 SECTION.                          00003160
      *---------------------------------------                          00003170
                                                                        00003180
           READ FBS202 INTO W-ENR-FBS202                                00003190
           IF ST-FBS202 = 10                                            00003200
              SET FIN-FBS202 TO TRUE                                    00003210
           ELSE                                                         00003220
              MOVE FBS202-OFFRE    TO W-OFFRE                           00003230
              MOVE FBS202-NSOCIETE TO W-NSOCIETE                        00003240
              MOVE FBS202-ZPRIX    TO W-ZPRIX                           00003250
              ADD 1 TO W-NBR-LEC-FBS202                                 00003260
           END-IF.                                                      00003270
                                                                        00003280
       FIN-LECTURE-FBS202 . EXIT.                                       00003290
                                                                        00003300
      *---------------------------------------                          00003310
       TRAITEMENT-PRINCIPAL           SECTION.                          00003320
      *---------------------------------------                          00003330
           PERFORM ECREMAGE-LISTE                                       00003340
           PERFORM INIT-VAR                                             00003350
           PERFORM VARYING I-203 FROM 1 BY 1                            00003360
            UNTIL I-203 > IND-203-NBP                                   00003370
              IF T-203-TYPE (I-203) = 'CO'                              00003380
      *--  POUR LES STOCKS DEPOT ET MAGASIN                             00003390
      *--  ON RELEVE LES PLUS PETITES VALEURS DE STOCKS DES CODICS      00003400
      *--  QUI PEUVENT CONSTITUER L'OFFRE                               00003410
                 PERFORM RECHERCHE-STOCK                                00003420
              END-IF                                                    00003430
              IF T-203-CODIC-OK (I-203)                                 00003440
      *--  POUR LE CALCUL DE PRMP, SRP, PRA                             00003450
      *--  ON NE TIENT PAS COMPTE DES CODICS ELIMINES DANS LE �         00003460
      *--  ECREMAGE-LISTE                                               00003470
                 PERFORM CALCUL-PRMP-PRA-SRP                            00003480
      *--  INITIALISATION DU CHAMP WSENSAPPRO DE LA LIGNE OFFRE         00003490
      *--  SI AU MOINS 1 DES CODICS CONSTITUANT L'OFFRE A UN            00003500
      *--  WSENSAPPRO A 'O', --> WSENSAPPRO DE L'OFFRE A 'O'            00003510
      *--                        SINON 'N'                              00003520
                 IF T-203-WSENSAPPRO (I-203) = 'O'                      00003530
                    SET WSENSAPPRO-O TO TRUE                            00003540
                 END-IF                                                 00003550
              END-IF                                                    00003560
           END-PERFORM                                                  00003570
           IF I-203 = 2                                                 00003580
      *--  DANS LE CAS D' OFFRE SANS CODIC, ON REINITIALISE LES STOCKS  00003590
              MOVE ZERO TO W-QSTOCKDEP W-QSTOCKMAG                      00003600
           END-IF                                                       00003610
           PERFORM VARYING I-203 FROM 1 BY 1                            00003620
            UNTIL I-203 > IND-203-NBP                                   00003630
              PERFORM ECRITURE-FBS203                                   00003640
           END-PERFORM                                                  00003650
           .                                                            00003660
       FIN-TRAITEMENT-PRINCIPAL. EXIT.                                  00003670
                                                                        00003680
      *------------------------------------*                            00003690
       FILL-TABLEAU-FBS203          SECTION.                            00003700
      *------------------------------------*                            00003710
           ADD 1 TO IND-203-NBP                                         00003720
           IF IND-203-NBP > MAX-203-NBP                                 00003730
              MOVE '203' TO W-NOM-TAB                                   00003740
              PERFORM DEPASSEMENT-TAB                                   00003750
           END-IF                                                       00003760
           MOVE W-ENR-FBS202  TO T-203-DONNEES  (IND-203-NBP)           00003770
           .                                                            00003780
       FIN-FILL-TABLEAU-FBS203. EXIT.                                   00003790
                                                                        00003800
      *---------------------------------------                          00003810
       ECREMAGE-LISTE                 SECTION.                          00003820
      *---------------------------------------                          00003830
           INITIALIZE W-VAR-SAVE                                        00003840
           PERFORM VARYING I-203 FROM 1 BY 1                            00003850
            UNTIL I-203 > IND-203-NBP                                   00003860
              IF T-203-TYPE    (I-203)  = 'CO'                          00003870
      *-CE � NE S'APPLIQUE QU'AUX CODICS CONSTITUTIFS DE L'OFFRE        00003880
                 IF T-203-NSEQENT (I-203) NOT = W-NSEQENT-SAVE          00003890
                    IF W-NSEQENT-SAVE > ZERO                            00003900
                       PERFORM ELIMINATION-CODICS                       00003910
                    END-IF                                              00003920
                    MOVE T-203-NSEQENT (I-203) TO W-NSEQENT-SAVE        00003930
                    INITIALIZE TABLEAU-TRV                              00003940
                    MOVE T-203-NENTMAX (I-203) TO W-NENTMAX-SAVE        00003950
                    IF W-NENTMAX-SAVE > MAX-TRV-NBP                     00003960
                       MOVE 'TRV' TO W-NOM-TAB                          00003970
                       PERFORM DEPASSEMENT-TAB                          00003980
                    END-IF                                              00003990
      *--INITIALISATION DU TABLEAU TRV                                  00004000
                    PERFORM VARYING I-TRV FROM 1 BY 1                   00004010
                     UNTIL I-TRV > W-NENTMAX-SAVE                       00004020
                       ADD 1 TO IND-TRV-NBP                             00004030
                       MOVE -99999 TO T-TRV-RESULTAT (IND-TRV-NBP)      00004040
                    END-PERFORM                                         00004050
                 END-IF                                                 00004060
                 SET BOUCLE-OK TO TRUE                                  00004070
                 SUBTRACT T-203-MONTANT (I-203) FROM T-203-PRMP (I-203) 00004080
                 GIVING W-RESULTAT-SAVE                                 00004090
                 PERFORM VARYING I-TRV FROM 1 BY 1                      00004100
                  UNTIL I-TRV > IND-TRV-NBP                             00004110
                  OR BOUCLE-KO                                          00004120
                    IF W-RESULTAT-SAVE > T-TRV-RESULTAT (I-TRV)         00004130
      *--ON STOCKE DANS LE TABLEAU TRV LES NENTMAX CODICS QUI           00004140
      *--   ONT LE PLUS FORT (PRIX - COMMISSION OPERATEUR)              00004150
                       MOVE T-203-NCODIC (I-203)                        00004160
                                            TO T-TRV-NCODIC   (I-TRV)   00004170
                       MOVE W-RESULTAT-SAVE TO T-TRV-RESULTAT (I-TRV)   00004180
                       SET BOUCLE-KO TO TRUE                            00004190
                    END-IF                                              00004200
                 END-PERFORM                                            00004210
              END-IF                                                    00004220
           END-PERFORM                                                  00004230
           IF W-NSEQENT-SAVE > ZERO                                     00004240
              PERFORM ELIMINATION-CODICS                                00004250
           END-IF                                                       00004260
           .                                                            00004270
       FIN-ECREMAGE-LISTE. EXIT.                                        00004280
                                                                        00004290
                                                                        00004300
      *---------------------------------------                          00004310
       ELIMINATION-CODICS            SECTION.                           00004320
      *---------------------------------------                          00004330
           MOVE ZERO TO I-203                                           00004340
      *{ reorder-array-condition 1.1                                            
      *    PERFORM UNTIL T-203-NSEQENT (I-203) = W-NSEQENT-SAVE         00004350
      *     OR I-203 > IND-203-NBP                                      00004360
      *--                                                                       
           PERFORM UNTIL I-203 > IND-203-NBP                                    
            OR T-203-NSEQENT (I-203) = W-NSEQENT-SAVE                           
      *}                                                                        
               ADD 1 TO I-203                                           00004370
           END-PERFORM                                                  00004380
           SET BOUCLE-OK TO TRUE                                        00004390
      *{ reorder-array-condition 1.1                                            
      *    PERFORM UNTIL T-203-NSEQENT (I-203) NOT = W-NSEQENT-SAVE     00004400
      *     OR I-203 > IND-203-NBP                                      00004410
      *--                                                                       
           PERFORM UNTIL I-203 > IND-203-NBP                                    
            OR T-203-NSEQENT (I-203) NOT = W-NSEQENT-SAVE                       
      *}                                                                        
              SET BOUCLE-OK TO TRUE                                     00004420
              PERFORM VARYING I-TRV FROM 1 BY 1                         00004430
               UNTIL I-TRV > IND-TRV-NBP                                00004440
               OR BOUCLE-KO                                             00004450
                 IF T-TRV-NCODIC (I-TRV) = T-203-NCODIC (I-203)         00004460
                    SET BOUCLE-KO TO TRUE                               00004470
                 END-IF                                                 00004480
              END-PERFORM                                               00004490
                                                                        00004500
              IF BOUCLE-KO                                              00004510
      *-- LE CODIC EST DANS LE TABLEAU-TRV :                            00004520
      *--    IL FAIT DONC PARTIE DES NENTMAX CODICS DONT (PRIX - COMM   00004530
      *--    OPERATEUR) EST MAX : LE CODIC EST RETENU POUR LE CALCUL    00004540
      *--    DES ZONES DE L'OFFRE                                       00004550
                 SET T-203-CODIC-OK (I-203) TO TRUE                     00004560
              ELSE                                                      00004570
                 SET T-203-CODIC-KO (I-203) TO TRUE                     00004580
              END-IF                                                    00004590
              ADD 1 TO I-203                                            00004600
           END-PERFORM                                                  00004610
           .                                                            00004620
       FIN-ELIMINATION-CODICS. EXIT.                                    00004630
                                                                        00004640
      *---------------------------------------                          00004650
       DEPASSEMENT-TAB                SECTION.                          00004660
      *---------------------------------------                          00004670
           STRING 'PB DEPASSEMENT TABLEAU ' W-NOM-TAB                   00004680
            DELIMITED BY SIZE                                           00004690
            INTO ABEND-MESS                                             00004700
            PERFORM FIN-ANORMALE                                        00004710
           .                                                            00004720
       FIN-DEPASSEMENT-TAB. EXIT.                                       00004730
                                                                        00004740
      *---------------------------------------                          00004750
       INIT-VAR                       SECTION.                          00004760
      *---------------------------------------                          00004770
                                                                        00004780
           INITIALIZE W-VAR-SOMME W-VAR-NCODIC                          00004790
                      W-VAR-STOCK                                       00004800
           MOVE 99999 TO W-QSTOCKDEP W-QSTOCKMAG                        00004810
           .                                                            00004820
       FIN-INIT-VAR. EXIT.                                              00004830
                                                                        00004840
      *---------------------------------------                          00004850
       RECHERCHE-STOCK                SECTION.                          00004860
      *---------------------------------------                          00004870
           IF T-203-QSTOCKDEP (I-203) < W-QSTOCKDEP                     00004880
              MOVE T-203-QSTOCKDEP (I-203) TO W-QSTOCKDEP               00004890
              MOVE T-203-QSTOCKRES (I-203) TO W-QSTOCKRES               00004900
              MOVE T-203-DCDE      (I-203) TO W-DCDE                    00004910
              MOVE T-203-QCDE      (I-203) TO W-QCDE                    00004920
              MOVE T-203-QCDERES   (I-203) TO W-QCDERES                 00004930
              MOVE T-203-TOTQCDE   (I-203) TO W-TOTQCDE                 00004940
              MOVE T-203-TOTQCDERES(I-203) TO W-TOTQCDERES              00004950
           END-IF                                                       00004960
           IF T-203-QSTOCKMAGD (I-203) < W-QSTOCKMAG                    00004970
              MOVE T-203-QSTOCKMAGD (I-203) TO W-QSTOCKMAG              00004980
              MOVE T-203-QMUTATT    (I-203) TO W-QMUTATT                00004990
           END-IF                                                       00005000
           .                                                            00005010
       FIN-RECHERCHE-STOCK. EXIT.                                       00005020
                                                                        00005030
      *---------------------------------------                          00005040
       CALCUL-PRMP-PRA-SRP            SECTION.                          00005050
      *---------------------------------------                          00005060
           IF W-NCODIC-1 NOT > SPACE                                    00005070
              MOVE T-203-NCODIC        (I-203) TO W-NCODIC-1            00005080
              MOVE T-203-NSOCDEPOT     (I-203) TO W-NSOCDEPOT           00005090
              MOVE T-203-NDEPOT        (I-203) TO W-NDEPOT              00005100
              MOVE T-203-PRA           (I-203) TO W-PRA-1               00005110
              MOVE T-203-PRMP          (I-203) TO W-PRMP-1              00005120
              MOVE T-203-SRP           (I-203) TO W-SRP-1               00005130
              MOVE T-203-PREF1         (I-203) TO W-PREF1-1             00005140
              MOVE T-203-QSTOCKDEP     (I-203) TO W-QSTOCKDEP-1         00005150
              MOVE T-203-QSTOCKRES     (I-203) TO W-QSTOCKRES-1         00005160
              MOVE T-203-DCDE          (I-203) TO W-DCDE-1              00005170
              MOVE T-203-QCDE          (I-203) TO W-QCDE-1              00005180
              MOVE T-203-QCDERES       (I-203) TO W-QCDERES-1           00005190
              MOVE T-203-TOTQCDE       (I-203) TO W-TOTQCDE-1           00005200
              MOVE T-203-TOTQCDERES    (I-203) TO W-TOTQCDERES-1        00005210
              MOVE T-203-QSTOCKMAGD    (I-203) TO W-QSTOCKMAG-1         00005220
              MOVE T-203-QMUTATT       (I-203) TO W-QMUTATT-1           00005230
              MOVE T-203-QPIECES0-HV04 (I-203) TO W-QPIECES0-HV04-1     00005240
              MOVE T-203-QPIECES1-HV04 (I-203) TO W-QPIECES1-HV04-1     00005250
              MOVE T-203-QPIECES2-HV04 (I-203) TO W-QPIECES2-HV04-1     00005260
              MOVE T-203-QPIECES3-HV04 (I-203) TO W-QPIECES3-HV04-1     00005270
              MOVE T-203-QPIECES4-HV04 (I-203) TO W-QPIECES4-HV04-1     00005280
              MOVE T-203-QPIECES0-HV12 (I-203) TO W-QPIECES0-HV12-1     00005290
              MOVE T-203-QPIECES1-HV12 (I-203) TO W-QPIECES1-HV12-1     00005300
              MOVE T-203-QPIECES2-HV12 (I-203) TO W-QPIECES2-HV12-1     00005310
              MOVE T-203-QPIECES3-HV12 (I-203) TO W-QPIECES3-HV12-1     00005320
              MOVE T-203-QPIECES4-HV12 (I-203) TO W-QPIECES4-HV12-1     00005330
              MOVE T-203-PCOMMART      (I-203) TO W-PCOMMART-1          00005340
           END-IF                                                       00005350
           ADD T-203-PRMP        (I-203) TO W-PRMP                      00005360
           ADD T-203-PRA         (I-203) TO W-PRA                       00005370
           ADD T-203-SRP         (I-203) TO W-SRP                       00005380
           ADD T-203-MONTANT     (I-203) TO W-MONTANT                   00005390
           ADD T-203-MONTANT-MAX (I-203) TO W-MONTANT-MAX               00005400
           ADD T-203-REFI        (I-203) TO W-REFI                      00005410
           .                                                            00005420
       FIN-CALCUL-PRMP-PRA-SRP. EXIT.                                   00005430
                                                                        00005440
      *---------------------------------------                          00005450
       ECRITURE-FBS203                SECTION.                          00005460
      *---------------------------------------                          00005470
           IF T-203-TYPE (I-203) = 'OF'                                 00005480
              IF WSENSAPPRO-O                                           00005490
                 MOVE 'O'      TO T-203-WSENSAPPRO (I-203)              00005500
              ELSE                                                      00005510
                 MOVE 'N'      TO T-203-WSENSAPPRO (I-203)              00005520
              END-IF                                                    00005530
              MOVE W-NSOCDEPOT   TO T-203-NSOCDEPOT  (I-203)            00005540
              MOVE W-NDEPOT      TO T-203-NDEPOT     (I-203)            00005550
              MOVE W-QSTOCKDEP   TO T-203-QSTOCKDEP  (I-203)            00005560
              MOVE W-QSTOCKRES   TO T-203-QSTOCKRES  (I-203)            00005570
              MOVE W-QSTOCKMAG   TO T-203-QSTOCKMAGD (I-203)            00005580
              MOVE W-QMUTATT     TO T-203-QMUTATT    (I-203)            00005590
              MOVE W-PRA         TO T-203-PRA        (I-203)            00005600
              MOVE W-PRMP        TO T-203-PRMP       (I-203)            00005610
              MOVE W-SRP         TO T-203-SRP        (I-203)            00005620
              ADD  W-MONTANT     TO T-203-MONTANT    (I-203)            00005630
              ADD  W-MONTANT-MAX TO T-203-MONTANT-MAX(I-203)            00005640
              ADD  W-REFI        TO T-203-REFI       (I-203)            00005650
              MOVE T-203-DONNEES (I-203) TO FBS203-VAR-OFFRE            00005660
              MOVE W-VAR-NCODIC          TO FBS203-VAR-NCODIC           00005670
              WRITE FBS203-ENR FROM W-ENR-FBS203                        00005680
              ADD 1 TO W-NBR-ECR-FBS203                                 00005690
           END-IF                                                       00005700
           .                                                            00005710
       FIN-ECRITURE-FBS203. EXIT.                                       00005720
                                                                        00005730
      *---------------------------------------                          00005740
       AFF-TEMPS                      SECTION.                          00005750
      *---------------------------------------                          00005760
                                                                        00005770
           IF W-ETAPE NOT > SPACE MOVE '????????' TO W-ETAPE END-IF.    00005780
           ACCEPT W-TIME FROM TIME                                      00005790
           DISPLAY 'TIME (' W-ETAPE ') --> ' W-TIME(1:2) ' H '          00005800
                                             W-TIME(3:2) ' S '          00005810
                                             W-TIME(5:4).               00005820
                                                                        00005830
       FIN-AFF-TEMPS. EXIT.                                             00005840
                                                                        00005850
      *---------------------------------------                          00005860
       MODULE-SORTIE                  SECTION.                          00005870
      *---------------------------------------                          00005880
                                                                        00005890
           CLOSE FBS202.                                                00005900
           CLOSE FBS203.                                                00005910
                                                                        00005920
           MOVE W-NBR-LEC-FBS202 TO WEDITIO9.  DISPLAY                  00005930
           '**   NB LECTURES  FBS202          : '    WEDITIO9 '   **'.  00005940
           MOVE W-NBR-ECR-FBS203 TO WEDITIO9.  DISPLAY                  00005950
           '**   NB ECRITURES FBS203          : '    WEDITIO9 '   **'.  00005960
           DISPLAY '**                                            **'.  00005970
           DISPLAY '************************************************'.  00005980
           DISPLAY '**                                            **'.  00005990
           DISPLAY '**  BBS203 : EXECUTION TERMINEE NORMALEMENT   **'.  00006000
           DISPLAY '**                                            **'.  00006010
           DISPLAY '************************************************'.  00006020
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN                                                     00006030
      *--                                                                       
           EXIT PROGRAM                                                         
      *}                                                                        
           .                                                            00006040
       FIN-MODULE-SORTIE. EXIT.                                         00006050
                                                                        00006060
      *---------------------------------------                          00006070
       FIN-ANORMALE                   SECTION.                          00006080
      *---------------------------------------                          00006090
                                                                        00006100
           MOVE 'ABEND    ' TO W-ETAPE    PERFORM AFF-TEMPS.            00006110
           DISPLAY '************************************************'.  00006120
           DISPLAY '**                                            **'.  00006130
           DISPLAY '**  BBS203 : EXECUTION TERMINEE ANORMALEMENT  **'.  00006140
           DISPLAY '**                                            **'.  00006150
           DISPLAY '************************************************'.  00006160
           DISPLAY '**                                            **'.  00006170
           DISPLAY '** MESSAGE ABEND-MESS (--> CALL ABEND) :      **'.  00006180
           DISPLAY '**                                            **'.  00006190
           DISPLAY '**   ' ABEND-MESS                           ' **'.  00006200
           DISPLAY '**                                            **'.  00006210
           DISPLAY '************************************************'.  00006220
           MOVE 'BBS203'    TO ABEND-PROG.                              00006230
           MOVE 'ABEND'     TO WNMODULE.                                00006240
           CALL 'ABEND'  USING ABEND-PROG ABEND-MESS.                   00006250
                                                                        00006260
       FIN-FIN-ANORMALE. EXIT.                                          00006270
                                                                        00006280
      *-----------------------------------------------------------------00006290
                                                                        00006300
