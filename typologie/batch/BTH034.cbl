      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BTH034.                                                 
       AUTHOR.     AD - APSIDE.                                                 
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : THEMIS/NSKEP : INTERFACE NCG - NSKEP            *        
      *  INTERFACE   : /                                               *        
      *  PERIODICITE : HEBDOMADAIRE                                    *        
      *  PROGRAMME   : BTH034                                          *        
      *  CREATION    : 09/08/2014                                      *        
      *  FONCTION    : COMPARER LE FICHIERS                            *        
      *                DES REMPLACES / REMPLACANT DU SAMEDI PRECEDENT  *        
      *                AVEC LE SAMEDI EN COURS POUR ENVOYER LES        *        
      *                SUPPRESSIONS                                    *        
      *                                                                *        
      *  EN ENTREE   :                                                 *        
      *                FTH034J  : FICHIER DU JOUR (SAMEDI)             *        
      *                FTH034J1 : FICHIER DU SAMEDI PRECEDENT          *        
      *                                                                *        
      *  EN SORTIE   :                                                 *        
      *                FTH034O  : LISTE DES SUPPRESSIONS DE LIENS      *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTH034J1  ASSIGN TO  FTH034J1.                                
      *--                                                                       
           SELECT FTH034J1  ASSIGN TO  FTH034J1                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTH034J   ASSIGN TO  FTH034J.                                 
      *--                                                                       
           SELECT FTH034J   ASSIGN TO  FTH034J                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FTH034O   ASSIGN TO  FTH034O.                                 
      *--                                                                       
           SELECT FTH034O   ASSIGN TO  FTH034O                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *--- FICHIER EN ENTREE                                                    
      *                                                                         
      *------ FICHIER DES LIENS DU SAMEDI PRECEDENT                             
       FD  FTH034J1                                                             
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FTH034J1-ENR PIC X(22).                                              
      *                                                                         
      *                                                                         
      *------ FICHIER DES LIENS DU JOUR                                         
       FD  FTH034J                                                              
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FTH034J-ENR PIC X(22).                                               
      *                                                                         
      *                                                                         
      *--- FICHIER EN SORTIE                                                    
      *                                                                         
      *------ FICHIER EN SORTIE                                                 
       FD  FTH034O                                                              
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FTH034O-ENR PIC X(15).                                               
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *--------------------------------------------------------------*          
      *  ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL     *          
      *--------------------------------------------------------------*          
           COPY SYKWSQ10.                                                       
           COPY ZLIBERRG.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES PROGRAMME                                 *          
      *--------------------------------------------------------------*          
      *                                                                         
       01  FTH034J1-ENREGISTREMENT.                                             
          03 FTH034J1-CLE.                                                      
      *----- CODIC                                                              
1            05 FTH034J1-NCODIC          PIC X(07).                             
      *----- CODIC LIE                                                          
8            05 FTH034J1-NCODICLIE       PIC X(07).                             
15        03 FILLER                      PIC X(08).                             
      *                                                                         
       01  FTH034J-ENREGISTREMENT.                                              
          03 FTH034J-CLE.                                                       
      *----- CODIC                                                              
1            05 FTH034J-NCODIC           PIC X(07).                             
      *----- CODIC LIE                                                          
8            05 FTH034J-NCODICLIE        PIC X(07).                             
15        03 FILLER                      PIC X(08).                             
      *                                                                         
       01  FTH034O-ENREGISTREMENT.                                              
      *----- CODIC                                                              
1            05 FTH034O-NCODIC           PIC X(07).                             
8            05 FILLER                   PIC X(01) VALUE ';'.                   
      *----- CODIC LIE                                                          
9            05 FTH034O-NCODICLIE        PIC X(07).                             
      *                                                                         
      *----COMPTEURS D'ENREGISTREMENTS                                          
      *                                                                         
       01 CPT-GENERAL.                                                          
          03 CPT-FTH034J1-LU   PIC 9(08) COMP-3   VALUE ZERO.                   
          03 CPT-FTH034J-LU    PIC 9(08) COMP-3   VALUE ZERO.                   
          03 CPT-FTH034O       PIC 9(08) COMP-3   VALUE ZERO.                   
      *                                                                         
       01  PIC-EDIT            PIC Z(07)9.                                      
      *                                                                         
      *--- STATISTIQUES DE TRAITEMENT                                           
      *                                                                         
       01 CR-STAT.                                                              
          03 CR-TIME.                                                           
             05 CR-HEURE         PIC 9(2).                                      
             05 CR-MINUTE        PIC 9(2).                                      
             05 CR-SECONDE       PIC 9(2).                                      
             05 FILLER           PIC 9(2).                                      
          03 CR-DUREE            PIC 9(8).                                      
          03 CR-DATE             PIC 9(6).                                      
          03 CR-JOUR-DEBUT       PIC 9(5).                                      
          03 CR-JOUR-FIN         PIC 9(5).                                      
      *                                                                         
      *--- FLAG                                                                 
      ************************************************* ANCIEN J-1              
       01 F-FTH034J1         PIC 9     VALUE 0.                                 
          88 DEBUT-FTH034J1            VALUE 0.                                 
          88 FIN-FTH034J1              VALUE 1.                                 
      ************************************************* NOUVEAU J               
       01 F-FTH034J          PIC 9     VALUE 0.                                 
          88 DEBUT-FTH034J             VALUE 0.                                 
          88 FIN-FTH034J               VALUE 1.                                 
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
       01 FILLER  PIC X(16)   VALUE '*** Z-INOUT ****'.                         
       01 Z-INOUT PIC X(4096) VALUE SPACE.                                      
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                *          
      *--------------------------------------------------------------*          
           COPY SYBWERRO.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND / BETDATC / BFTNPER                            *          
      *--------------------------------------------------------------*          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND  PIC X(8) VALUE 'ABEND'.                                       
      *--                                                                       
       01  MW-ABEND  PIC X(8) VALUE 'ABEND'.                                    
      *}                                                                        
           COPY ABENDCOP.                                                       
      *                                                                         
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BTH034.                                                           
      *--------------                                                           
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM TRAITEMENT                                                   
           PERFORM AFFICHAGE-COMPTEUR                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BTH034.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-          BIENVENUE DANS LE PROGRAMME          -'          
           DISPLAY '-                    BTH034                     -'          
           DISPLAY '-------------------------------------------------'          
      *                                                                         
           MOVE 'BTH034' TO  ABEND-PROG                                         
      *                                                                         
           ACCEPT CR-DATE FROM DATE                                             
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-JOUR-DEBUT FROM DAY                                        
      *                                                                         
           COMPUTE CR-DUREE  = CR-HEURE * 3600                                  
                             + CR-MINUTE * 60 + CR-SECONDE                      
      *                                                                         
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-                                               -'          
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT   FTH034J1   FTH034J.                                     
           OPEN OUTPUT  FTH034O.                                                
      *                                                                         
       FIN-MODULE-ENTREE.                 EXIT.                                 
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *                                                                         
      *----------------------------------------                                 
       TRAITEMENT                      SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      * ON COMPARE DEUX FICHIERS: LA SITUATION DU JOUR FTH034J                  
      * ET LA SITUATION DU SAMEDI PRECEDENT                             R       
      *                                                                         
      * SI LA LIGNE N'EST PLUS DANS LE NOUVEAU FICHIER ON L'ECRIT               
      * DANS LE FICHIER EN SORTIE                                               
      *                                                                         
           INITIALIZE CPT-GENERAL                                               
           SET DEBUT-FTH034J1 TO TRUE                                           
           SET DEBUT-FTH034J TO TRUE                                            
           INITIALIZE FTH034J-ENREGISTREMENT                                    
      *                                                                         
           PERFORM LECTURE-FTH034J1                                             
           PERFORM LECTURE-FTH034J                                              
      *                                                                         
           PERFORM UNTIL FIN-FTH034J1                                           
      *                                                                         
              IF FTH034J-CLE = FTH034J1-CLE THEN                                
                 PERFORM LECTURE-FTH034J1                                       
                 PERFORM LECTURE-FTH034J                                        
              END-IF                                                            
              IF FTH034J-CLE > FTH034J1-CLE THEN                                
                 MOVE FTH034J1-NCODIC     TO FTH034O-NCODIC                     
                 MOVE FTH034J1-NCODICLIE  TO FTH034O-NCODICLIE                  
                 PERFORM ECRITURE-FTH034O                                       
                 PERFORM LECTURE-FTH034J1                                       
              END-IF                                                            
              IF FTH034J-CLE < FTH034J1-CLE THEN                                
                 IF FIN-FTH034J THEN                                            
                    IF NOT FIN-FTH034J1 THEN                                    
                       MOVE FTH034J1-NCODIC     TO FTH034O-NCODIC               
                       MOVE FTH034J1-NCODICLIE  TO FTH034O-NCODICLIE            
                       PERFORM ECRITURE-FTH034O                                 
                       PERFORM LECTURE-FTH034J1                                 
                    END-IF                                                      
                 ELSE                                                           
                    PERFORM LECTURE-FTH034J                                     
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAITEMENT.                    EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       AFFICHAGE-COMPTEUR              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '- AFFICHAGE DES COMPTEURS :                     -'          
           MOVE  CPT-FTH034J1-LU  TO  PIC-EDIT                                  
           DISPLAY '-  NBRE DE LIGNES LUES J-1       '                          
                   PIC-EDIT '       -'                                          
           MOVE  CPT-FTH034J-LU   TO  PIC-EDIT                                  
           DISPLAY '-  NBRE DE LIGNES LUES J         '                          
                   PIC-EDIT '       -'.                                         
           MOVE  CPT-FTH034O     TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LIGNES INSEREES       '                          
                   PIC-EDIT '       -'.                                         
      *                                                                         
       FIN-AFFICHAGE-COMPTEUR.            EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FTH034J1                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           READ FTH034J1 INTO FTH034J1-ENREGISTREMENT                           
             AT END                                                             
                SET FIN-FTH034J1 TO TRUE                                        
                MOVE HIGH-VALUE TO  FTH034J1-ENREGISTREMENT                     
             NOT END                                                            
                COMPUTE CPT-FTH034J1-LU = CPT-FTH034J1-LU + 1                   
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FTH034J1.              EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FTH034J                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           READ FTH034J INTO FTH034J-ENREGISTREMENT                             
             AT END                                                             
                SET FIN-FTH034J TO  TRUE                                        
                MOVE LOW-VALUE TO  FTH034J-ENREGISTREMENT                       
             NOT END                                                            
                COMPUTE CPT-FTH034J-LU = CPT-FTH034J-LU + 1                     
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FTH034J.               EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       ECRITURE-FTH034O                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           ADD  1  TO  CPT-FTH034O                                              
      *                                                                         
           WRITE FTH034O-ENR FROM FTH034O-ENREGISTREMENT                        
           INITIALIZE FTH034O-ENREGISTREMENT.                                   
      *                                                                         
       FIN-ECRITURE-FTH034O.              EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           CLOSE FTH034J1  FTH034J.                                             
      *                                                                         
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-DATE FROM DATE                                             
           DISPLAY '-                                               -'          
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           COMPUTE CR-DUREE  = CR-DUREE - CR-HEURE * 3600                       
                             - CR-MINUTE * 60 - CR-SECONDE                      
           ACCEPT CR-JOUR-FIN FROM DAY                                          
           IF CR-JOUR-FIN > CR-JOUR-DEBUT                                       
              COMPUTE CR-DUREE  = CR-DUREE                                      
                                + (CR-JOUR-FIN - CR-JOUR-DEBUT) * 86400         
           END-IF                                                               
           DIVIDE CR-DUREE  BY 3600 GIVING CR-HEURE                             
                                                   REMAINDER CR-MINUTE          
           DIVIDE CR-MINUTE BY 60 GIVING CR-MINUTE REMAINDER CR-SECONDE         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       DUREE TOTALE DU PROGRAMME '                         
                   CR-HEURE ':' CR-MINUTE ':' CR-SECONDE '      -'              
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       CETTE NUIT TOUT S''EST BIEN PASSE        -'         
           DISPLAY '-------------------------------------------------'          
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       SORTIE-ANORMALE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY '><                    ' ABEND-PROG(1:6)                     
                                               '                  ><'           
           DISPLAY '><     FIN ANORMALE PROVOQUEE PAR PROGRAMME   ><'           
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY ABEND-MESS                                                   
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.               EXIT.                                 
      *                                                                         
      *                                                                         
       FIN-ANORMALE        SECTION.                                             
      *----------------------------*                                            
           CLOSE FTH034J1  FTH034J  FTH034O                                     
           DISPLAY '**************************************************'         
           DISPLAY '**                                              **'         
           DISPLAY '**   BTH034 : EXECUTION TERMINEE ANORMALEMENT   **'         
           DISPLAY '**                                              **'         
           DISPLAY '**************************************************'         
           DISPLAY '**                                              **'         
           DISPLAY '**   MESSAGE ABEND-MESS (--> CALL ABEND) :      **'         
           DISPLAY '**                                              **'         
           DISPLAY '**     ' ABEND-MESS                           ' **'         
           DISPLAY '**                                              **'         
           DISPLAY '**************************************************'         
           MOVE 'BTH034'    TO ABEND-PROG.                                      
           CALL 'ABEND'  USING ABEND-PROG ABEND-MESS.                           
      ******************************************************************        
      ******************************************************************        
      **                       FIN                                    **        
      ******************************************************************        
      ******************************************************************        
