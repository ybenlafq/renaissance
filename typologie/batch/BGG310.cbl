      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.  BGG310.                                                     
       AUTHOR.  THIERRY GRAFF                                                   
       DATE-WRITTEN.  06/03/91.                                                 
      *************************************************                         
      *                                               *                         
      *   EDITION DES ECARTS PRA / PRMP DANS LA NCG   *                         
      *                                               *                         
      *************************************************                         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE   ASSIGN TO FDATE.                                      
      *--                                                                       
           SELECT FDATE   ASSIGN TO FDATE                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FGG300 ASSIGN TO FGG300.                                      
      *--                                                                       
           SELECT FGG300 ASSIGN TO FGG300                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT JGG310  ASSIGN TO JGG310.                                     
      *--                                                                       
           SELECT JGG310  ASSIGN TO JGG310                                      
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE FDATE                          * 00099058
      *---------------------------------------------------------------* 00100055
       FD   FDATE                                                               
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01   ENR-FDATE.                                                          
            03  FILLER      PIC X(80).                                          
                                                                        00097055
      *---------------------------------------------------------------* 00098055
      *                  DEFINITION DE FGG300                         * 00099058
      *---------------------------------------------------------------* 00100055
       FD   FGG300                                                              
            RECORDING F                                                         
            BLOCK 0 RECORDS                                                     
            LABEL RECORD STANDARD.                                              
       01  ENR-FGG300.                                                          
           03 FILLER PIC X(75).                                                 
                                                                        00097055
      *===============================================================* 00098055
      *                  DEFINITION DE JGG310                         * 00099058
      *===============================================================* 00100055
       FD   JGG310                                                              
            LABEL RECORD STANDARD.                                              
       01   JGG310-ENREG.                                                       
            03  FILLER      PIC X(133).                                         
      *===============================================================*         
      *****************************************************************         
      *                 W O R K I N G  -  S T O R A G E               *         
      *****************************************************************         
      *===============================================================*         
       WORKING-STORAGE SECTION.                                                 
      *===============================================================*         
      *                 DESCRIPTION DE FDATE                          *         
      *===============================================================*         
       01  FDATE-ENREG.                                                         
            02  FDATE-JJ       PIC X(02).                                       
            02  FDATE-MM       PIC X(02).                                       
            02  FDATE-SS       PIC X(02).                                       
            02  FDATE-AA       PIC X(02).                                       
            02  FILLER         PIC X(72).                                       
      *===============================================================*         
      *                 DESCRIPTION DE FGG300                         *         
      *===============================================================*         
       COPY FGG300.                                                             
       01  ETAT-FGG300                   PIC X.                                 
           88  FGG300-FINI               VALUE '0'.                             
           88  FGG300-NON-FINI           VALUE '1'.                             
       01  W-LIGNES-FGG300-LUES          PIC S9(9) COMP-3  VALUE +0.            
       01  W-LIGNES-JGG310-ECRITES       PIC S9(9) COMP-3  VALUE +0.            
      *===============================================================*         
      *              LIGNES DE L'ETAT JGG310                          *         
      *===============================================================*         
       01  SAUT-PAGE                 PIC X VALUE '1'.                           
       01  SAUT-LIGNE                PIC X VALUE '-'.                           
       01  LIGNE-TIRETS.                                                        
           02  FILLER                PIC X      VALUE '!'.                      
           02  FILLER                PIC X(127) VALUE ALL '-'.                  
           02  FILLER                PIC X      VALUE '!'.                      
       01  LIGNE-RESULTAT.                                                      
           02  FILLER                 PIC X(2) VALUE '! '.                      
           02  ETAT-CFAMILLE          PIC X(5).                                 
           02  FILLER                 PIC X(3) VALUE SPACES.                    
           02  ETAT-CMARQ             PIC X(5).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-NCODIC            PIC X(7).                                 
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  ETAT-LREFFOURN         PIC X(20).                                
           02  FILLER                 PIC X(4) VALUE '  ! '.                    
           02  ETAT-DATEMAJ           PIC X(8).                                 
           02  FILLER                 PIC X(3) VALUE '  !'.                     
           02  ETAT-PRA               PIC -(7)9,99.                             
           02  FILLER                 PIC X    VALUE SPACES.                    
           02  ETAT-PRMP              PIC -(7)9,99.                             
           02  FILLER                 PIC XX   VALUE ' !'.                      
           02  ETAT-ECART-UNITAIRE    PIC -(7)9,99.                             
           02  FILLER                 PIC XX   VALUE SPACES.                    
           02  ETAT-POURCENTAGE       PIC -(3)9,99.                             
           02  FILLER                 PIC XX   VALUE SPACES.                    
           02  ETAT-STOCK             PIC -(6)9.                                
           02  FILLER                 PIC XX   VALUE SPACES.                    
           02  ETAT-VALEUR-ECART      PIC -(7)9,99.                             
           02  FILLER                 PIC X    VALUE '!'.                       
       01  LIGNE-ENTETE1.                                                       
           02  FILLER                 PIC X VALUE SPACE.                        
           02  FILLER                 PIC X(6) VALUE 'JGG310'.                  
           02  FILLER                 PIC X(47) VALUE SPACES.                   
           02  FILLER                 PIC X(30)                                 
                 VALUE 'ECARTS PRMP / PRA             '.                        
           02  FILLER                 PIC X(11) VALUE SPACES.                   
           02  FILLER                 PIC X(11) VALUE 'EDITE LE : '.            
           02  ENTETE-DATE            PIC X(8).                                 
           02  FILLER                 PIC X(5).                                 
           02  FILLER                 PIC X(7) VALUE 'PAGE : '.                 
           02  ENTETE-PAGE            PIC X(3).                                 
           02  FILLER                 PIC X(4) VALUE SPACES.                    
       01  LIGNE-ENTETE2.                                                       
           02  FILLER                 PIC X(2)  VALUE '! '.                     
           02  FILLER                 PIC X(8)  VALUE 'FAMILLE '.               
           02  FILLER                 PIC X(6)  VALUE 'MARQUE'.                 
           02  FILLER                 PIC X(10) VALUE ' CODIC  '.               
           02  FILLER                 PIC X(22)                                 
                      VALUE 'REFERENCE FOURNISSEUR '.                           
           02  FILLER                 PIC X(11) VALUE '!   DATE   '.            
           02  FILLER                 PIC X(12) VALUE ' !    PRA   '.           
           02  FILLER                 PIC X(13) VALUE '      PRMP   '.          
           02  FILLER                 PIC X(14) VALUE ' ! ECART UNIT.'.         
           02  FILLER                 PIC X(9)  VALUE '  POURC. '.              
           02  FILLER                 PIC X(9)  VALUE '  STOCK  '.              
           02  FILLER                 PIC X(12) VALUE '  VAL. ECART'.           
           02  FILLER                 PIC X     VALUE '!'.                      
       01  LIGNE-VIDE.                                                          
           02  FILLER                 PIC X(4)  VALUE '!   '.                   
           02  FILLER                 PIC X(43) VALUE SPACES.                   
           02  FILLER                 PIC X(2)  VALUE ' !'.                     
           02  FILLER                 PIC X(10) VALUE SPACES.                   
           02  FILLER                 PIC X(2)  VALUE ' !'.                     
           02  FILLER                 PIC X(23) VALUE SPACES.                   
           02  FILLER                 PIC X(2)  VALUE ' !'.                     
           02  FILLER                 PIC X(40) VALUE SPACES.                   
           02  FILLER                 PIC X(4)  VALUE '  !'.                    
       01  LIGNE-TOTAL-FAMILLE.                                                 
           02  FILLER                 PIC X(4)  VALUE '!   '.                   
           02  FILLER                 PIC X(23)                                 
                      VALUE ' TOTAL  FAMILLE        '.                          
           02  TOTAL-CFAMILLE         PIC X(5).                                 
           02  FILLER                 PIC X(76) VALUE SPACES.                   
           02  TOTAL-FAM-STOCK        PIC -(6)9.                                
           02  FILLER                 PIC X(2) VALUE SPACES.                    
           02  TOTAL-FAM-ECART        PIC -(7)9,99.                             
           02  FILLER                 PIC X     VALUE '!'.                      
       01  LIGNE-TOTAL-GENERAL.                                                 
           02  FILLER                 PIC X(4)  VALUE '!   '.                   
           02  FILLER                 PIC X(23)                                 
                      VALUE ' TOTAL  GENERAL        '.                          
           02  FILLER                 PIC X(81) VALUE SPACES.                   
           02  TOTAL-GEN-STOCK        PIC -(6)9.                                
           02  FILLER                 PIC X(2)  VALUE SPACES.                   
           02  TOTAL-GEN-ECART        PIC -(7)9,99.                             
           02  FILLER                 PIC X     VALUE '!'.                      
      *==============================================================*          
      *    VARIABLES DE TOTAUX                                       *          
      *==============================================================*          
       01  WT-FAM-ECART              PIC S9(9)V9(2) COMP-3 VALUE 0.             
       01  WT-TOT-ECART              PIC S9(9)V9(2) COMP-3 VALUE 0.             
       01  WT-FAM-STOCK              PIC S9(7) COMP-3      VALUE 0.             
       01  WT-TOT-STOCK              PIC S9(7) COMP-3      VALUE 0.             
      *==============================================================*          
      *    VARIABLES DIVERSES                                        *          
      *==============================================================*          
       01  W-NOLIGNE                 PIC 9(2).                                  
       01  W-NOLIGNE-INIT            PIC 9(2) VALUE 9.                          
       01  W-NBLIGNE-MAX             PIC 9(2) VALUE 55.                         
       01  W-PAGE                    PIC 9(3) VALUE 0.                          
       01  W-SAUVE-WSEQED            PIC S9(5) COMP-3.                          
       01  W-SAUVE-CFAMILLE          PIC X(5).                                  
      *==============================================================*          
      *    VARIABLES DE DATES                                        *          
      *==============================================================*          
      *** ZONE CORRESPONDANT AU FORMAT D'EDITION ***                            
       01  W2-DATE.                                                             
           02  W2-JJ                     PIC XX.                                
           02  FILLER                    PIC X VALUE '/'.                       
           02  W2-MM                     PIC XX.                                
           02  FILLER                    PIC X VALUE '/'.                       
           02  W2-AA                     PIC XX.                                
      ***  ZONE RECEPTRICE DE DATES DE FORMAT AAAAMMJJ ***                      
       01  W1-DATEMAJ.                                                          
           03  W1-AAAA.                                                         
               05  W1-AA1                PIC XX.                                
               05  W1-AA2                PIC XX.                                
           03  W1-MM                     PIC XX.                                
           03  W1-JJ                     PIC XX.                                
           COPY ABENDCOP.                                                       
      *==============================================================*          
      *--------------------------------------------------------------*          
      ****************************************************************          
      *             P R O C E D U R E   D I V I S I O N              *          
      ****************************************************************          
      *--------------------------------------------------------------*          
      *==============================================================*          
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BGG310 THRU FIN-DEBUT-BGG310.                         
           PERFORM TRAITER-FAMILLE THRU FIN-TRAITER-FAMILLE                     
                                     UNTIL FGG300-FINI.                         
           PERFORM  FIN-BGG310 THRU FIN-FIN-BGG310.                             
              EJECT                                                             
      *===============================================================*         
      *                     D E B U T                                 *         
      *===============================================================*         
       DEBUT-BGG310.                                                            
           DISPLAY  '************************************************'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '**   PROGRAMME  :  BGG310                     **'.         
           DISPLAY  '**   EDITION DES ECARTS PRMP / PRA            **'.         
           DISPLAY  '**                                            **'.         
           DISPLAY  '************************************************'.         
           DISPLAY  '***     PROGRAMME DEBUTE NORMALEMENT         ***'.         
           OPEN  INPUT  FGG300.                                                 
           OPEN OUTPUT JGG310.                                                  
           OPEN INPUT FDATE.                                                    
           READ   FDATE  INTO   FDATE-ENREG   AT END                            
                MOVE  'CARTE PARAMETRE FDATE ABSENTE   '                        
                               TO   ABEND-MESS                                  
                MOVE  01       TO   ABEND-CODE                                  
                CLOSE FDATE                                                     
                GO TO FIN-ANORMALE.                                             
           MOVE FDATE-ENREG TO W1-DATEMAJ.                                      
           DISPLAY 'FDATE : ' W1-DATEMAJ.                                       
           MOVE FDATE-JJ TO W2-JJ.                                              
           MOVE FDATE-MM TO W2-MM.                                              
           MOVE FDATE-AA TO W2-AA.                                              
           MOVE W2-DATE  TO ENTETE-DATE.                                        
           CLOSE FDATE.                                                         
           SET FGG300-NON-FINI TO TRUE.                                         
           PERFORM LIRE-FGG300 THRU FIN-LIRE-FGG300.                            
           IF FGG300-NON-FINI                                                   
               IF FGG300-ENREG = LOW-VALUES                                     
                   DISPLAY '*** 1ER ENREG DE FGG300 = LOW-VALUES  ***'          
                   DISPLAY '*** DONC AUCUN CODIC ECRIT PAR BGG300 ***'          
                   CLOSE FGG300                                                 
      *{ Ba-Stop-Run-Statement 1.1                                              
      *            STOP RUN                                                     
      *--                                                                       
                   EXIT PROGRAM                                                 
      *}                                                                        
               ELSE                                                             
                   PERFORM ADD-TOT-FAMILLE THRU FIN-ADD-TOT-FAMILLE             
                   PERFORM ADD-TOT-GENERAL THRU FIN-ADD-TOT-GENERAL             
                   PERFORM ECRIRE-ENTETE-PAGE                                   
                         THRU FIN-ECRIRE-ENTETE-PAGE                            
               END-IF                                                           
           ELSE                                                                 
               MOVE '*** FICHIER FGG300 VIDE ***' TO ABEND-MESS                 
               GO TO FIN-ANORMALE                                               
           END-IF.                                                              
       FIN-DEBUT-BGG310.                                                        
      *===============================================================*         
      *     PROCEDURE : TRAITER-FAMILLE                               *         
      *===============================================================*         
       TRAITER-FAMILLE.                                                         
           MOVE FGG300-WSEQED TO W-SAUVE-WSEQED.                                
           MOVE FGG300-CFAMILLE TO W-SAUVE-CFAMILLE.                            
           IF W-NOLIGNE > W-NBLIGNE-MAX - 1                                     
               WRITE JGG310-ENREG FROM LIGNE-TIRETS                             
               PERFORM ECRIRE-ENTETE-PAGE THRU FIN-ECRIRE-ENTETE-PAGE           
           ELSE                                                                 
               WRITE JGG310-ENREG FROM LIGNE-VIDE                               
               ADD 1 TO W-NOLIGNE                                               
           END-IF.                                                              
      ***************************************                                   
      ***  APPEL BOUCLE DE TRAITER-CODIC  ***                                   
      ***************************************                                   
           PERFORM TRAITER-CODIC THRU FIN-TRAITER-CODIC                         
                        UNTIL FGG300-FINI                                       
                           OR FGG300-WSEQED NOT = W-SAUVE-WSEQED.               
      *******************************                                           
      ***  ECRITURE TOTAL FAMILLE ***                                           
      *******************************                                           
           IF W-NOLIGNE > W-NBLIGNE-MAX - 3                                     
               WRITE JGG310-ENREG FROM LIGNE-TIRETS                             
               PERFORM ECRIRE-ENTETE-PAGE THRU FIN-ECRIRE-ENTETE-PAGE           
           END-IF.                                                              
           MOVE W-SAUVE-CFAMILLE TO TOTAL-CFAMILLE.                             
           MOVE WT-FAM-ECART TO TOTAL-FAM-ECART.                                
           MOVE WT-FAM-STOCK TO TOTAL-FAM-STOCK.                                
           WRITE JGG310-ENREG FROM LIGNE-TIRETS.                                
           WRITE JGG310-ENREG FROM LIGNE-TOTAL-FAMILLE.                         
           WRITE JGG310-ENREG FROM LIGNE-TIRETS.                                
           ADD 3 TO W-NOLIGNE.                                                  
           PERFORM INIT-TOT-FAMILLE THRU FIN-INIT-TOT-FAMILLE.                  
           PERFORM ADD-TOT-FAMILLE  THRU FIN-ADD-TOT-FAMILLE.                   
       FIN-TRAITER-FAMILLE.                                                     
      *==========================================================*              
      *        PROCEDURE : TRAITER-CODIC                         *              
      *==========================================================*              
       TRAITER-CODIC.                                                           
           MOVE FGG300-CFAMILLE       TO ETAT-CFAMILLE.                         
           MOVE FGG300-CMARQ          TO ETAT-CMARQ.                            
           MOVE FGG300-NCODIC         TO ETAT-NCODIC.                           
           MOVE FGG300-LREFFOURN      TO ETAT-LREFFOURN.                        
           MOVE FGG300-DATEMAJ        TO W1-DATEMAJ.                            
           MOVE W1-AA2                TO W2-AA.                                 
           MOVE W1-MM                 TO W2-MM.                                 
           MOVE W1-JJ                 TO W2-JJ.                                 
           MOVE W2-DATE               TO ETAT-DATEMAJ.                          
           MOVE FGG300-PRA            TO ETAT-PRA.                              
           MOVE FGG300-PRMP           TO ETAT-PRMP.                             
           MOVE FGG300-ECART-UNITAIRE TO ETAT-ECART-UNITAIRE.                   
           MOVE FGG300-POURCENTAGE    TO ETAT-POURCENTAGE.                      
           MOVE FGG300-STOCK          TO ETAT-STOCK.                            
           MOVE FGG300-VALEUR-ECART   TO ETAT-VALEUR-ECART.                     
           IF W-NOLIGNE > W-NBLIGNE-MAX - 1                                     
               WRITE JGG310-ENREG FROM LIGNE-TIRETS                             
               PERFORM ECRIRE-ENTETE-PAGE THRU FIN-ECRIRE-ENTETE-PAGE           
           END-IF.                                                              
           WRITE JGG310-ENREG FROM LIGNE-RESULTAT.                              
           ADD 1 TO W-LIGNES-JGG310-ECRITES.                                    
           ADD 1 TO W-NOLIGNE.                                                  
           PERFORM LIRE-FGG300 THRU FIN-LIRE-FGG300.                            
           IF FGG300-NON-FINI                                                   
               PERFORM ADD-TOT-GENERAL THRU FIN-ADD-TOT-GENERAL                 
               IF W-SAUVE-WSEQED = FGG300-WSEQED                                
                   PERFORM ADD-TOT-FAMILLE THRU FIN-ADD-TOT-FAMILLE             
               END-IF                                                           
           END-IF.                                                              
       FIN-TRAITER-CODIC.                                                       
      *===============================================================*         
      *        PROCEDURE : ECRIRE-ENTETE-PAGE                         *         
      *===============================================================*         
       ECRIRE-ENTETE-PAGE.                                                      
           ADD 1 TO W-PAGE.                                                     
           MOVE W-PAGE TO ENTETE-PAGE.                                          
           WRITE JGG310-ENREG FROM LIGNE-ENTETE1 AFTER PAGE.                    
           WRITE JGG310-ENREG FROM LIGNE-TIRETS AFTER 3.                        
           WRITE JGG310-ENREG FROM LIGNE-VIDE.                                  
           WRITE JGG310-ENREG FROM LIGNE-ENTETE2.                               
           WRITE JGG310-ENREG FROM LIGNE-VIDE.                                  
           WRITE JGG310-ENREG FROM LIGNE-TIRETS.                                
           MOVE W-NOLIGNE-INIT TO W-NOLIGNE.                                    
       FIN-ECRIRE-ENTETE-PAGE.                                                  
      *===============================================================*         
      *        PROCEDURE : LIRE-FGG300                                *         
      *===============================================================*         
       LIRE-FGG300.                                                             
           READ   FGG300 INTO    FGG300-ENREG   AT END                          
                SET FGG300-FINI TO TRUE.                                        
      *    DISPLAY '============ LIRE-LIGNE ============'.                      
      *    DISPLAY '*** FGG300-WSEQED =' FGG300-WSEQED.                         
           IF FGG300-NON-FINI                                                   
                ADD 1 TO W-LIGNES-FGG300-LUES                                   
           END-IF.                                                              
       FIN-LIRE-FGG300.                                                         
       ADD-TOT-FAMILLE.                                                         
           ADD FGG300-VALEUR-ECART TO WT-FAM-ECART.                             
           ADD FGG300-STOCK        TO WT-FAM-STOCK.                             
       FIN-ADD-TOT-FAMILLE.                                                     
       ADD-TOT-GENERAL.                                                         
           ADD FGG300-VALEUR-ECART TO WT-TOT-ECART.                             
           ADD FGG300-STOCK        TO WT-TOT-STOCK.                             
       FIN-ADD-TOT-GENERAL.                                                     
       INIT-TOT-FAMILLE.                                                        
           MOVE 0  TO WT-FAM-ECART.                                             
           MOVE 0  TO WT-FAM-STOCK.                                             
       FIN-INIT-TOT-FAMILLE.                                                    
       INIT-TOT-GENERAL.                                                        
           MOVE 0  TO WT-TOT-ECART.                                             
           MOVE 0  TO WT-TOT-STOCK.                                             
       FIN-INIT-TOT-GENERAL.                                                    
      *===============================================================*         
      *       F I N   N O R M A L E   D U   P R O G R A M M E         *         
      *===============================================================*         
       FIN-BGG310.                                                              
           DISPLAY  '************************************************'.         
           DISPLAY  '***       FIN  NORMALE  DU PROGRAMME         ***'.         
           DISPLAY  '***                 BGG310                   ***'.         
           DISPLAY  '************************************************'.         
           WRITE JGG310-ENREG FROM LIGNE-TIRETS.                                
           ADD 1 TO W-NOLIGNE.                                                  
           MOVE WT-TOT-ECART TO TOTAL-GEN-ECART.                                
           MOVE WT-TOT-STOCK TO TOTAL-GEN-STOCK.                                
           IF FGG300-ENREG NOT = LOW-VALUE                                      
               WRITE JGG310-ENREG FROM LIGNE-TOTAL-GENERAL                      
               WRITE JGG310-ENREG FROM LIGNE-TIRETS                             
           END-IF.                                                              
           DISPLAY  '** NBR DE LIGNES FGG300 LUES          : '          07480000
                    W-LIGNES-FGG300-LUES.                               07490000
           DISPLAY  '** NBR DE LIGNES JGG310 ECRITES       : '          07500000
                    W-LIGNES-JGG310-ECRITES                             07510000
           CLOSE  FGG300.                                                       
           CLOSE  JGG310.                                                       
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       FIN-FIN-BGG310.                                                          
      *===============================================================*         
      *             F I N   A N O R M A L E                           *         
      *===============================================================*         
       FIN-ANORMALE.                                                            
           CLOSE  FGG300.                                                       
           CLOSE  JGG310.                                                       
           DISPLAY '***************************************'.                   
           DISPLAY '*** PROGRAMME TERMINE ANORMALEMENT  ***'.                   
           DISPLAY '***************************************'.                   
           MOVE  'BGG300'  TO    ABEND-PROG.                                    
           CALL  'ABEND'  USING  ABEND-PROG  ABEND-MESS.                        
       FIN-FIN-ANORMALE.                                                        
          EJECT                                                                 
