      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.                     BRX016.                                  
       AUTHOR. DSA013.                                                          
      ******************************************************************        
      *                                                                *        
      *      EXTRACTION DES ARTICLES DONT LE PRIX EST INFERIEUR AUX    *        
      *                  CONCURRENTS DE SA ZONE DE PRIX                *        
      *                                                                *        
      ******************************************************************        
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *        
      ******************************************************************        
      *  DATE       : 15/05/2001                                      *         
      *  AUTEUR     : DSA051                                          *         
      *  REPAIRE    : GC                                              *         
      *  OBJET      :                                                           
      *****************************************************************         
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                            00000180
       FILE-CONTROL.                                                    00000190
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IRX010   ASSIGN TO IRX010.                                   
      *--                                                                       
            SELECT IRX010   ASSIGN TO IRX010                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT IRX016   ASSIGN TO IRX016.                                   
      *--                                                                       
            SELECT IRX016   ASSIGN TO IRX016                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                   00000240
       FILE SECTION.                                                    00000250
      *                                                                 00000260
       FD  IRX010                                                       00000270
           RECORDING F                                                  00000280
           BLOCK 0 RECORDS                                              00000290
           LABEL RECORD STANDARD.                                       00000300
       01  IRX010-ENR        PIC X(512).                                00000310
      *                                                                 00000260
       FD  IRX016                                                       00000270
           RECORDING F                                                  00000280
           BLOCK 0 RECORDS                                              00000290
           LABEL RECORD STANDARD.                                       00000300
       01  IRX016-ENR        PIC X(512).                                00000310
      *                                                                 00000260
       WORKING-STORAGE SECTION.                                                 
                  COPY  IRX010.                                                 
                  COPY  IRX016.                                                 
                  COPY  SYKWDIV0.                                               
                  COPY  ABENDCOP.                                               
                  COPY  WORKDATC.                                               
           EJECT                                                                
      *****************************************************************         
      *   DECRIRE   ICI   LES   ZONES   SPECIFIQUES   AU   PROGRAMME  *         
      *****************************************************************         
      *                                                                         
       01  W-FLAG.                                                              
           05  FLAG-IRX010         PIC X VALUE 'O'.                             
               88 NON-FIN-IRX010         VALUE 'O'.                             
               88 FIN-IRX010             VALUE 'N'.                             
      *                                                                         
      *01  RUPTURE-TAMPON.                                                      
      *    05 TAMPON-NSOCIETE           PIC X(03).                      007  003
      *    05 TAMPON-CHEFPROD           PIC X(05).                      010  005
      *    05 TAMPON-WSEQFAM            PIC S9(05)      COMP-3.         015  003
      *    05 TAMPON-LVMARKET1          PIC X(20).                      018  020
      *    05 TAMPON-LVMARKET2          PIC X(20).                      038  020
      *    05 TAMPON-LVMARKET3          PIC X(20).                      058  020
      *    05 TAMPON-MARQREF            PIC X(05).                      078  005
      *    05 TAMPON-MARQGR             PIC X(07).                      078  005
      *    05 TAMPON-FOURNREF           PIC X(20).                      168  020
      *    05 TAMPON-NZONPRIX           PIC X(02).                      110  002
GCC   *    05 TAMPON-PVCONC             PIC S9(07)V9(2) COMP-3.         110  002
      *                                                                         
      *01  RUPTURE-ACTUEL.                                                      
      *    05 ACTUEL-NSOCIETE           PIC X(03).                      007  003
      *    05 ACTUEL-CHEFPROD           PIC X(05).                      010  005
      *    05 ACTUEL-WSEQFAM            PIC S9(05)      COMP-3.         015  003
      *    05 ACTUEL-LVMARKET1          PIC X(20).                      018  020
      *    05 ACTUEL-LVMARKET2          PIC X(20).                      038  020
      *    05 ACTUEL-LVMARKET3          PIC X(20).                      058  020
      *    05 ACTUEL-MARQREF            PIC X(05).                      078  005
      *    05 ACTUEL-MARQGR             PIC X(07).                      078  005
      *    05 ACTUEL-FOURNREF           PIC X(20).                      168  020
      *    05 ACTUEL-NZONPRIX           PIC X(02).                      110  002
GCC   *    05 ACTUEL-PVCONC             PIC S9(07)V9(2) COMP-3.         110  002
      *                                                                         
       01  CTR-IRX010 PIC 9(6) VALUE ZERO.                                      
       01  CTR-IRX016 PIC 9(6) VALUE ZERO.                                      
      *                                                                 00019300
       PROCEDURE DIVISION.                                                      
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *           -----------------------------------------           *         
      *           -   MODULE DE BASE DU PROGRAMME BRX016  -           *         
      *           -----------------------------------------           *         
      *                               I                               *         
      *           -----------------------------------------           *         
      *           I                   I                   I           *         
      *   -----------------   -----------------   -----------------   *         
      *   -    ENTREE     -   -  TRAITEMENT   -   -     SORTIE    -   *         
      *   -----------------   -----------------   -----------------   *         
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                                         
       MODULE-BRX016                  SECTION.                                  
      *                                                                         
           DISPLAY '>>>>>> DEBUT'                                               
           PERFORM MODULE-ENTREE.                                               
           DISPLAY '>>>>>> TRAITEMENT'                                          
           PERFORM MODULE-TRAITEMENT UNTIL FIN-IRX010.                          
           DISPLAY '>>>>>> SORTIE'                                              
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BRX016.  EXIT.                                                
                    EJECT                                                       
      *                                                                         
       MODULE-ENTREE              SECTION.                                      
      *                                                                         
           OPEN INPUT  IRX010.                                                  
           OPEN OUTPUT IRX016.                                                  
           PERFORM LECTURE-IRX010.                                              
      *                                                                         
      *                                                                         
       FIN-MODULE-ENTREE.   EXIT.                                               
                    EJECT                                                       
      ***********************************************************               
       MODULE-TRAITEMENT          SECTION.                                      
      ***********************************************************               
            IF IRX010-ECART = '<' AND IRX010-PVCONC NOT = 0                     
              PERFORM TRAITEMENT-ECRITURE                                       
            ELSE                                                                
              PERFORM TRAITEMENT-LECTURE                                        
            END-IF.                                                             
      *                                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
       TRAITEMENT-LECTURE   SECTION.                                            
                   PERFORM LECTURE-IRX010.                                      
       FIN-TRAITEMENT-LECT. EXIT.                                               
      *                                                                         
      *                                                                         
       TRAITEMENT-ECRITURE  SECTION.                                            
                   PERFORM ECRITURE-IRX016                                      
                   PERFORM LECTURE-IRX010.                                      
       FIN-TRAITEMENT-ECRI. EXIT.                                               
      *                                                                         
       ECRITURE-IRX016    SECTION.                                              
      *                                                                         
           INITIALIZE DSECT-IRX016.                                             
           MOVE 'IRX016'         TO NOMETAT-IRX016.                             
           MOVE  RUPTURES-IRX010 TO RUPTURES-IRX016                             
           MOVE  CHAMPS-IRX010   TO  CHAMPS-IRX016                              
           WRITE IRX016-ENR   FROM DSECT-IRX016                                 
           ADD 1 TO CTR-IRX016.                                                 
      *                                                                         
       FIN-ECRITURE-IRX016. EXIT.                                               
      *                                                                         
       LECTURE-IRX010  SECTION.                                                 
      *                                                                         
           READ IRX010 INTO DSECT-IRX010 AT END                                 
              SET FIN-IRX010 TO TRUE                                            
           END-READ.                                                            
           IF NON-FIN-IRX010                                                    
              ADD 1 TO CTR-IRX010                                               
      *       MOVE IRX010-NSOCIETE   TO ACTUEL-NSOCIETE                 007  003
      *       MOVE IRX010-CHEFPROD   TO ACTUEL-CHEFPROD                 010  005
      *       MOVE IRX010-WSEQFAM    TO ACTUEL-WSEQFAM                  015  003
      *       MOVE IRX010-LVMARKET1  TO ACTUEL-LVMARKET1                018  020
      *       MOVE IRX010-LVMARKET2  TO ACTUEL-LVMARKET2                038  020
      *       MOVE IRX010-LVMARKET3  TO ACTUEL-LVMARKET3                058  020
      *       MOVE IRX010-MARQREF    TO ACTUEL-MARQREF                  078  005
      *       MOVE IRX010-MARQGR     TO ACTUEL-MARQGR                   078  005
      *       MOVE IRX010-FOURNREF   TO ACTUEL-FOURNREF                 168  020
      *       MOVE IRX010-NZONPRIX   TO ACTUEL-NZONPRIX                 110  002
GCC   *       MOVE IRX010-PVCONC     TO ACTUEL-PVCONC                   110  002
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-IRX010. EXIT.                                                
      *                                                                         
       MODULE-SORTIE              SECTION.                                      
      *                                                                         
           CLOSE IRX010 IRX016.                                                 
           DISPLAY '****************************'.                              
           DISPLAY ' IRX010 LUS    = ' CTR-IRX010.                              
           DISPLAY ' IRX016 ECRITS = ' CTR-IRX016.                              
           DISPLAY '     FIN NORMALE BRX016     '.                              
           DISPLAY '****************************'.                              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
                    EJECT                                                       
      *                                                                 00039300
      ** AIDA * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00039400
      *     M O D U L E     D ' A B A N D O N                         * 00039500
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00039600
      *                                                                 00039700
