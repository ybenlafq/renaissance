      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BNV921.                                                 
       AUTHOR.     FVM        .                                                 
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : INNOVENTE - REQUETE DE CONTROLE                 *        
      *  PROGRAMME   : BNV921                                          *        
      *  CREATION    : 06/11/2009                                      *        
      *  FONCTION    : EXTRACTION DES PRIX POUR INNOVENTE              *        
      *                ON RECUPERE LE FICHIER D'UNLOAD DES TABLES      *        
      *                RTNV18          ET ON LE MET AU FORMAT CSV      *        
      *                                                                *        
      *  EN ENTREE   :                                                 *        
      *                FDATE                                           *        
      *                FNV921I : FICHIER D'UNLOAD                      *        
      *                FNV388I : ISSUE DE BNV388                       *        
      *                FNV390I : ISSUE DE BNV390                       *        
      *                                                                *        
      *  EN SORTIE   : FNV921PX  FICHIER AU FORMAT CSV                 *        
      *              : FNV921PM  FICHIER AU FORMAT CSV                 *        
      *              : FNV921EC  FICHIER AU FORMAT CSV                 *        
      *              : FNV921SR  FICHIER AU FORMAT CSV                 *        
      *              : FNV921MP  FICHIER AU FORMAT CSV                 *        
BF2   *              : FNV921PR  FICHIER AU FORMAT CSV                 *        
      *                                                                *        
      ******************************************************************        
      * 30/05/2013 BF1                                                 *        
      *            GERER LES TAXES COMPLEMENTAIRES                     *        
      ******************************************************************        
      * 02/06/2015 BF2                                                 *        
      *            AJOUT PRMP POUR HUB CATALOGUE                       *        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FDATE     ASSIGN TO  FDATE.                                   
      *--                                                                       
           SELECT FDATE     ASSIGN TO  FDATE                                    
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV921I   ASSIGN TO  FNV921.                                  
      *--                                                                       
           SELECT FNV921I   ASSIGN TO  FNV921                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV388I   ASSIGN TO  FNV388.                                  
      *--                                                                       
           SELECT FNV388I   ASSIGN TO  FNV388                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV390I   ASSIGN TO  FNV390.                                  
      *--                                                                       
           SELECT FNV390I   ASSIGN TO  FNV390                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV921PX  ASSIGN TO  FNV921PX.                                
      *--                                                                       
           SELECT FNV921PX  ASSIGN TO  FNV921PX                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV921PM  ASSIGN TO  FNV921PM.                                
      *--                                                                       
           SELECT FNV921PM  ASSIGN TO  FNV921PM                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV921EC  ASSIGN TO  FNV921EC.                                
      *--                                                                       
           SELECT FNV921EC  ASSIGN TO  FNV921EC                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV921SR  ASSIGN TO  FNV921SR.                                
      *--                                                                       
           SELECT FNV921SR  ASSIGN TO  FNV921SR                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV921MP  ASSIGN TO  FNV921MP.                                
      *--                                                                       
           SELECT FNV921MP  ASSIGN TO  FNV921MP                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
BF2   *    SELECT FNV921PR  ASSIGN TO  FNV921PR.                                
      *--                                                                       
           SELECT FNV921PR  ASSIGN TO  FNV921PR                                 
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *--- FICHIER EN ENTREE                                                    
      *                                                                         
      *------ FICHIER FDATE                                                     
       FD  FDATE                                                                
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER      PIC X(80).                                            
      *                                                                         
      *------ FICHIER FNV921I                                                   
       FD  FNV921I                                                              
           RECORDING MODE F                                                     
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FNV921I-ENREGISTREMENT.                                              
      *                                                                         
1         03 FNV921I-NCODIC              PIC X(07).                             
8         03 FNV921I-NSOCIETE            PIC X(03).                             
11        03 FNV921I-NLIEU               PIC X(03).                             
14        03 FNV921I-CTYPE               PIC X(02).                             
16        03 FNV921I-PMONTANT            PIC S9(7)V99 COMP-3.                   
25        03 FNV921I-DSYST               PIC 9(13) COMP-3.                      
38    *                                                                         
      *--- FICHIER SITUATION DES PRIX ARTICLES A J                              
       FD  FNV388I                                                              
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FNV388-ENREGISTREMENT.                                               
          03 FNV388-NCODIC               PIC X(07).                             
          03 FNV388-NSOCIETE             PIC X(03).                             
          03 FNV388-NLIEU                PIC X(03).                             
          03 FNV388-SRP                  PIC S9(7)V9(2) COMP-3.                 
      *                                                                         
      *--- FICHIER SITUATION DES PRIX ARTICLES A J                              
       FD  FNV390I                                                              
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01  FNV390-ENREGISTREMENT.                                               
          03 FNV390-NCODIC               PIC X(07).                             
          03 FNV390-NSOCIETE             PIC X(03).                             
          03 FNV390-NLIEU                PIC X(03).                             
          03 FNV390-SRP                  PIC S9(7)V9(2) COMP-3.                 
      *                                                                         
      *--- FICHIER EN SORTIE                                                    
      *                                                                         
      *------ FICHIER FNV921PX                                                  
       FD  FNV921PX                                                             
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01 FNV921PX-ENREGISTREMENT PIC X(30).                                    
      *------ FICHIER FNV921PM                                                  
       FD  FNV921PM                                                             
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01 FNV921PM-ENREGISTREMENT PIC X(30).                                    
      *------ FICHIER FNV921EC                                                  
       FD  FNV921EC                                                             
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01 FNV921EC-ENREGISTREMENT PIC X(30).                                    
      *------ FICHIER FNV921SR                                                  
       FD  FNV921SR                                                             
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01 FNV921SR-ENREGISTREMENT PIC X(30).                                    
      *------ FICHIER FNV921MP                                                  
       FD  FNV921MP                                                             
           RECORDING MODE IS F                                                  
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
       01 FNV921MP-ENREGISTREMENT PIC X(30).                                    
      *------ FICHIER FNV921PR                                                  
BF2    FD  FNV921PR                                                             
BF2        RECORDING MODE IS F                                                  
BF2        BLOCK 0 RECORDS                                                      
BF2        LABEL RECORD STANDARD.                                               
BF2    01 FNV921PR-ENREGISTREMENT PIC X(30).                                    
      *                                                                         
      *---------------------------------------------------------------*         
      *                 W O R K I N G  -  S T O R A G E               *         
      *---------------------------------------------------------------*         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *--------------------------------------------------------------*          
      *  ZONES D'INTERFACE POUR LES ACCES AUX DATA BASES DB2/SQL     *          
      *--------------------------------------------------------------*          
      *    COPY SYKWSQ10.                                                       
      *    COPY ZLIBERRG.                                                       
      *                                                                         
      *    EXEC SQL INCLUDE SQLCA        END-EXEC.                              
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES PROGRAMME                                 *          
      *--------------------------------------------------------------*          
      *                                                                         
      *--- VARIABLE FICHIERS                                                    
1      01    W-NCODIC.                                                          
             05 W-BLANC                PIC XX.                                  
             05 W-SERVICE              PIC X(5).                                
       01  W-FNV921O-ENREGISTREMENT.                                            
1         03 W-FNV921O-NCODIC            PIC X(7).                              
8         03 FILLER                      PIC X(01) VALUE ';'.                   
9         03 W-FNV921O-NSOCIETE          PIC X(03).                             
12        03 FILLER                      PIC X(01) VALUE ';'.                   
13        03 W-FNV921O-NLIEU             PIC X(03).                             
16        03 FILLER                      PIC X(01) VALUE ';'.                   
17        03 W-FNV921O-CTYPE             PIC X(02).                             
19        03 FILLER                      PIC X(01) VALUE ';'.                   
20        03 W-FNV921O-PMONTANT          PIC ------9,9(02).                     
30        03 FILLER                      PIC X(01) VALUE ';'.                   
31    *                                                                         
      *----COMPTEURS D'ENREGISTREMENTS                                          
      *                                                                         
       01 CPT-GENERAL.                                                          
          03 CPT-FNV921-LU     PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV388-LU     PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV390-LU     PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV921-EC     PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV921PX-EC   PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV921PM-EC   PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV921EC-EC   PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV921SR-EC   PIC 9(10) COMP-3   VALUE ZERO.                   
          03 CPT-FNV921MP-EC   PIC 9(10) COMP-3   VALUE ZERO.                   
BF2       03 CPT-FNV921PR-EC   PIC 9(10) COMP-3   VALUE ZERO.                   
      *                                                                         
       01  PIC-EDIT            PIC Z(09)9.                                      
      *                                                                         
      *--- VARIABLES                                                            
      *                                                                         
       01  W-DATE-DU-JOUR                   PIC X(08) VALUE SPACES.             
BF2    01  W-NUM-7-2                        PIC S9(07)V99 COMP-3.               
      *                                                                         
      *--- STATISTIQUES DE TRAITEMENT                                           
      *                                                                         
       01 CR-STAT.                                                              
          03 CR-TIME.                                                           
             05 CR-HEURE         PIC 9(2).                                      
             05 CR-MINUTE        PIC 9(2).                                      
             05 CR-SECONDE       PIC 9(2).                                      
             05 FILLER           PIC 9(2).                                      
          03 CR-DUREE            PIC 9(8).                                      
          03 CR-DATE             PIC 9(6).                                      
          03 CR-JOUR-DEBUT       PIC 9(5).                                      
          03 CR-JOUR-FIN         PIC 9(5).                                      
      *                                                                         
      *--- FLAG                                                                 
      *                                                                         
       01 F-FNV921I          PIC 9     VALUE 0.                                 
          88 DEBUT-FNV921I             VALUE 0.                                 
          88 FIN-FNV921I               VALUE 1.                                 
      *                                                                         
       01 F-FNV388           PIC 9     VALUE 0.                                 
          88 DEBUT-FNV388              VALUE 0.                                 
          88 FIN-FNV388                VALUE 1.                                 
      *                                                                         
       01 F-FNV390           PIC 9     VALUE 0.                                 
          88 DEBUT-FNV390              VALUE 0.                                 
          88 FIN-FNV390                VALUE 1.                                 
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
      *01 FILLER  PIC X(16)   VALUE '*** Z-INOUT ****'.                         
      *01 Z-INOUT PIC X(4096) VALUE SPACE.                                      
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES DE GESTION DES ERREURS                                *          
      *--------------------------------------------------------------*          
      *    COPY SYBWERRO.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND / BETDATC / BFTNPER                            *          
      *--------------------------------------------------------------*          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND  PIC X(8) VALUE 'ABEND'.                                       
      *--                                                                       
       01  MW-ABEND  PIC X(8) VALUE 'ABEND'.                                    
      *}                                                                        
           COPY ABENDCOP.                                                       
      *                                                                         
      *01 W-SQL-MESSAGE.                                                        
      *   03 W-SQL-TABLE              PIC X(06)      VALUE SPACES.              
      *   03 FILLER                   PIC X(03)      VALUE ' : '.               
      *   03 W-SQL-FONCTION           PIC X(06)      VALUE SPACES.              
      *   03 FILLER                   PIC X(03)      VALUE ' : '.               
      *   03 W-SQL-CODE               PIC +ZZ9.                                 
      *   03 FILLER                   PIC X(03)      VALUE ' : '.               
      *   03 W-SQL-CLEF               PIC X(55)      VALUE SPACES.              
      *01 W-SQL-MESS.                                                           
      *   05 W-SQL-LRECL              PIC S9(8) COMP VALUE +72.                 
      *   05 W-SQL-MSGS.                                                        
      *      10  W-SQL-LGTH           PIC S9(4) COMP VALUE +288.                
      *      10  W-SQL-MSG            PIC X(72) OCCURS 4.                       
      *                                                                         
       01  BETDATC PIC X(10) VALUE 'BETDATC'.                                   
           COPY WORKDATC.                                                       
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BNV921 .                                                          
      *--------------                                                           
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT-FNV921I                                    
           PERFORM MODULE-TRAITEMENT-FNV388                                     
           PERFORM MODULE-TRAITEMENT-FNV390                                     
           PERFORM AFFICHAGE-COMPTEUR                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BNV921.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       MODULE-ENTREE .                                                          
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-          BIENVENUE DANS LE PROGRAMME          -'          
           DISPLAY '-                    BNV921                     -'          
           DISPLAY '-------------------------------------------------'          
      *                                                                         
           MOVE 'BNV921' TO  ABEND-PROG                                         
      *                                                                         
           ACCEPT CR-DATE FROM DATE                                             
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-JOUR-DEBUT FROM DAY                                        
      *                                                                         
           COMPUTE CR-DUREE  = CR-HEURE * 360                                   
                             + CR-MINUTE * 60 + CR-SECONDE                      
      *                                                                         
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-                                               -'          
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT   FDATE    FNV921I FNV388I FNV390I.                       
           OPEN OUTPUT  FNV921PX                                                
                        FNV921PM                                                
                        FNV921EC                                                
                        FNV921SR                                                
                        FNV921MP                                                
BF2                     FNV921PR                                                
      *                                                                         
      *----LECTURE FDATE                                                        
           READ FDATE INTO GFJJMMSSAA AT END                                    
                MOVE  'ERREUR LECTURE FDATE'  TO  ABEND-MESS                    
                PERFORM SORTIE-ANORMALE                                         
           END-READ                                                             
           CLOSE FDATE                                                          
      *                                                                         
      *----CONTROLE DATE                                                        
           MOVE '1' TO GFDATA                                                   
           CALL BETDATC USING WORK-BETDATC                                      
           IF GFVDAT NOT = '1'                                                  
              MOVE  'PB MODULE DE DATE BETDATC' TO ABEND-MESS                   
              PERFORM SORTIE-ANORMALE                                           
           END-IF                                                               
           DISPLAY '- **   TRAITEMENT AVEC LE FDATE ' GFJJMMSSAA                
                   '     ** -'                                                  
           DISPLAY '-                                               -'          
           .                                                                    
      *    MOVE  GFSAMJ-0  TO W-DATE-DU-JOUR.                                   
      *                                                                         
       FIN-MODULE-ENTREE.                 EXIT.                                 
      *                                                                         
      *****************************************************************         
      *             M O D U L E - T R A I T E M E N T                 *         
      *****************************************************************         
      *                                                                         
      *----------------------------------------                                 
       MODULE-TRAITEMENT-FNV921I .                                              
      *----------------------------------------                                 
      *                                                                         
           PERFORM LECTURE-FNV921                                               
           PERFORM UNTIL FIN-FNV921I                                            
      *                                                                         
              INITIALIZE  W-FNV921O-ENREGISTREMENT                              
              ADD  1  TO  CPT-FNV921-EC                                         
      *                                                                         
              MOVE  FNV921I-NCODIC     TO     W-NCODIC                          
              IF    W-BLANC = ' '                                               
                    MOVE W-SERVICE          TO W-FNV921O-NCODIC                 
              ELSE  MOVE FNV921I-NCODIC     TO W-FNV921O-NCODIC                 
              END-IF                                                            
              MOVE  FNV921I-NSOCIETE   TO     W-FNV921O-NSOCIETE                
              MOVE  FNV921I-NLIEU      TO     W-FNV921O-NLIEU                   
              MOVE  FNV921I-CTYPE      TO     W-FNV921O-CTYPE                   
              MOVE  FNV921I-PMONTANT   TO     W-FNV921O-PMONTANT                
              EVALUATE FNV921I-CTYPE                                            
                     WHEN    'PX'  WRITE FNV921PX-ENREGISTREMENT FROM           
                                         W-FNV921O-ENREGISTREMENT               
                                   ADD 1 TO CPT-FNV921PX-EC                     
                     WHEN    'PM'  WRITE FNV921PM-ENREGISTREMENT FROM           
                                         W-FNV921O-ENREGISTREMENT               
                                   ADD 1 TO CPT-FNV921PM-EC                     
      * MODIFICATION DU MONTANT POUR NE PAS L'AVOIR EN CLAIR EN BASE            
      * ==> DONNEE SENSIBLE !!                                                  
BF2                  WHEN    'PR'  COMPUTE W-NUM-7-2 =                          
BF2                                      (FNV921I-PMONTANT + 19) * 7            
BF2                                MOVE W-NUM-7-2 TO W-FNV921O-PMONTANT         
BF2                                WRITE FNV921PR-ENREGISTREMENT FROM           
BF2                                      W-FNV921O-ENREGISTREMENT               
BF2                                ADD 1 TO CPT-FNV921PR-EC                     
      *BF1           WHEN    'EC'  WRITE FNV921EC-ENREGISTREMENT FROM           
BF1                  WHEN    OTHER WRITE FNV921EC-ENREGISTREMENT FROM           
                                         W-FNV921O-ENREGISTREMENT               
                                   ADD 1 TO CPT-FNV921EC-EC                     
              END-EVALUATE                                                      
      *                                                                         
              PERFORM LECTURE-FNV921                                            
           END-PERFORM.                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT-FNV921I.     EXIT.                                 
       MODULE-TRAITEMENT-FNV388 .                                               
           PERFORM LECTURE-FNV388                                               
           PERFORM UNTIL FIN-FNV388                                             
      *                                                                         
              ADD  1  TO  CPT-FNV921-EC                                         
              INITIALIZE  W-FNV921O-ENREGISTREMENT                              
      *                                                                         
              MOVE  FNV388-NCODIC      TO     W-NCODIC                          
              IF    W-BLANC= ' '                                                
                    MOVE W-SERVICE          TO W-FNV921O-NCODIC                 
              ELSE  MOVE FNV388-NCODIC      TO W-FNV921O-NCODIC                 
              END-IF                                                            
              MOVE  FNV388-NSOCIETE    TO     W-FNV921O-NSOCIETE                
              MOVE  FNV388-NLIEU       TO     W-FNV921O-NLIEU                   
              MOVE  'MP'               TO     W-FNV921O-CTYPE                   
              MOVE  FNV388-SRP         TO     W-FNV921O-PMONTANT                
              WRITE FNV921MP-ENREGISTREMENT FROM                                
                    W-FNV921O-ENREGISTREMENT                                    
              ADD 1 TO CPT-FNV921MP-EC                                          
      *                                                                         
              PERFORM LECTURE-FNV388                                            
           END-PERFORM.                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT-FNV388. EXIT.                                      
       MODULE-TRAITEMENT-FNV390  .                                              
           PERFORM LECTURE-FNV390                                               
           PERFORM UNTIL FIN-FNV390                                             
      *                                                                         
              ADD  1  TO  CPT-FNV921-EC                                         
              INITIALIZE  W-FNV921O-ENREGISTREMENT                              
      *                                                                         
              MOVE  FNV390-NCODIC      TO     W-NCODIC                          
              IF    W-BLANC= ' '                                                
                    MOVE W-SERVICE          TO W-FNV921O-NCODIC                 
              ELSE  MOVE FNV390-NCODIC      TO W-FNV921O-NCODIC                 
              END-IF                                                            
              MOVE  FNV390-NSOCIETE    TO     W-FNV921O-NSOCIETE                
              MOVE  FNV390-NLIEU       TO     W-FNV921O-NLIEU                   
              MOVE  'SR'               TO     W-FNV921O-CTYPE                   
              MOVE  FNV390-SRP         TO     W-FNV921O-PMONTANT                
              WRITE FNV921SR-ENREGISTREMENT                                     
              FROM W-FNV921O-ENREGISTREMENT                                     
              ADD 1 TO CPT-FNV921SR-EC                                          
      *                                                                         
              PERFORM LECTURE-FNV390                                            
           END-PERFORM.                                                         
      *                                                                         
       FIN-MODULE-TRAITEMENT-FNV390. EXIT.                                      
      *----------------------------------------                                 
       LECTURE-FNV921 .                                                         
      *----------------------------------------                                 
      *                                                                         
           READ FNV921I                                                         
              AT END SET FIN-FNV921I TO TRUE                                    
           END-READ                                                             
      *                                                                         
           IF NOT FIN-FNV921I THEN                                              
              ADD 1 TO CPT-FNV921-LU                                            
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FNV921.               EXIT.                                  
      *----------------------------------------                                 
       LECTURE-FNV388  .                                                        
      *----------------------------------------                                 
      *                                                                         
           READ FNV388I                                                         
              AT END SET FIN-FNV388 TO TRUE                                     
           END-READ                                                             
      *                                                                         
           IF NOT FIN-FNV388 THEN                                               
              ADD 1 TO CPT-FNV388-LU                                            
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FNV388.               EXIT.                                  
      *----------------------------------------                                 
       LECTURE-FNV390  .                                                        
      *----------------------------------------                                 
      *                                                                         
           READ FNV390I                                                         
              AT END SET FIN-FNV390 TO TRUE                                     
           END-READ                                                             
      *                                                                         
           IF NOT FIN-FNV390 THEN                                               
              ADD 1 TO CPT-FNV390-LU                                            
           END-IF.                                                              
      *                                                                         
       FIN-LECTURE-FNV390.               EXIT.                                  
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       AFFICHAGE-COMPTEUR.                                                      
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY ' AFFICHAGE DES COMPTEURS :                     -'           
           MOVE  CPT-FNV921-LU   TO  PIC-EDIT                                   
           DISPLAY '- NBRE DE LIGNES LUES FNV921     ' PIC-EDIT '     -'        
           MOVE  CPT-FNV388-LU   TO  PIC-EDIT                                   
           DISPLAY '- NBRE DE LIGNES LUES FNV388     ' PIC-EDIT '     -'        
           MOVE  CPT-FNV390-LU   TO  PIC-EDIT                                   
           DISPLAY '- NBRE DE LIGNES LUES FNV390     ' PIC-EDIT '     -'        
           MOVE  CPT-FNV921PX-EC   TO  PIC-EDIT                                 
           DISPLAY '- NBRE DE LIGNES LUES (TYPE PX)  ' PIC-EDIT '     -'        
           MOVE  CPT-FNV921PM-EC   TO  PIC-EDIT                                 
           DISPLAY '- NBRE DE LIGNES LUES (TYPE PM)  ' PIC-EDIT '     -'        
           MOVE  CPT-FNV921EC-EC   TO  PIC-EDIT                                 
           DISPLAY '- NBRE DE LIGNES LUES (TYPE EC)  ' PIC-EDIT '     -'        
           MOVE  CPT-FNV921SR-EC   TO  PIC-EDIT                                 
           DISPLAY '- NBRE DE LIGNES LUES (TYPE SR)  ' PIC-EDIT '     -'        
           MOVE  CPT-FNV921MP-EC   TO  PIC-EDIT                                 
           DISPLAY '- NBRE DE LIGNES LUES (TYPE MP)  ' PIC-EDIT '     -'        
BF2        MOVE  CPT-FNV921PR-EC   TO  PIC-EDIT                                 
BF2        DISPLAY '- NBRE DE LIGNES LUES (TYPE PR)  ' PIC-EDIT '     -'        
           .                                                                    
      *                                                                         
       FIN-AFFICHAGE-COMPTEUR.            EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       MODULE-SORTIE .                                                          
      *----------------------------------------                                 
      *                                                                         
           CLOSE  FNV921I FNV388I FNV390I                                       
           CLOSE  FNV921PX                                                      
                  FNV921PM                                                      
                  FNV921EC                                                      
                  FNV921SR                                                      
                  FNV921MP                                                      
BF2               FNV921PR                                                      
      *                                                                         
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-DATE FROM DATE                                             
           DISPLAY '-                                               -'          
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           COMPUTE CR-DUREE  = CR-DUREE - CR-HEURE * 360                        
                             - CR-MINUTE * 60 - CR-SECONDE                      
           ACCEPT CR-JOUR-FIN FROM DAY                                          
           IF CR-JOUR-FIN > CR-JOUR-DEBUT                                       
              COMPUTE CR-DUREE  = CR-DUREE                                      
                                + (CR-JOUR-FIN - CR-JOUR-DEBUT) * 86400         
           END-IF                                                               
           DIVIDE CR-DUREE  BY 60 GIVING CR-HEURE  REMAINDER CR-MINUTE          
           DIVIDE CR-MINUTE BY 60 GIVING CR-MINUTE REMAINDER CR-SECONDE         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       DUREE TOTALE DU PROGRAMME '                         
                   CR-HEURE ':' CR-MINUTE ':' CR-SECONDE '      -'              
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       CETTE NUIT TOUT S''EST BIEN PASSE        -'         
           DISPLAY '-                   A DEMAIN                    -'          
           DISPLAY '-------------------------------------------------'          
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       SORTIE-ANORMALE .                                                        
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY '><                    ' ABEND-PROG(1:6)                     
                                               '                  ><'           
           DISPLAY '><     FIN ANORMALE PROVOQUEE PAR PROGRAMME   ><'           
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY ABEND-MESS                                                   
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.               EXIT.                                 
      *                                                                         
      *                                                                         
      ******************************************************************        
      ******************************************************************        
      **                            F I N                             **        
      ******************************************************************        
      ******************************************************************        
