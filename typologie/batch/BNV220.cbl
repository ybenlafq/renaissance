      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID.      BNV220.                                                 
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : DECISIONNEL - FICHIER ASSOC. FAM / MAG / DEPOT  *        
      *  PROGRAMME   : BNV220                                          *        
      *  CREATION    : 30/12/2012                                      *        
      *  FONCTION    : FORMATAGE DU FICHIER FNV219J QUI EST            *        
      *                ALIMENTE PAR LE BNV218                          *        
      *                                                                *        
      *  EN ENTREE   :                                                 *        
      *                FNV220J  : FICHIER DES ASSOCIATIONS J           *        
      *  EN SORTIE   :                                                 *        
      *                FNV220O  : FICHIER FORMATE DES ASSOCIATIONS     *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES.                                                           
           DECIMAL-POINT IS COMMA.                                              
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *                                                                         
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV220J   ASSIGN TO  FNV220J.                                 
      *--                                                                       
           SELECT FNV220J   ASSIGN TO  FNV220J                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *    SELECT FNV220O   ASSIGN TO  FNV220O.                                 
      *--                                                                       
           SELECT FNV220O   ASSIGN TO  FNV220O                                  
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *                                                                         
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
      *--- FICHIER EN ENTREE                                                    
      *                                                                         
      *------ FICHIER DES ASSOCIATIONS DU JOUR                                  
       FD  FNV220J                                                              
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FNV220J-ENREGISTREMENT PIC X(80).                                    
      *                                                                         
      *------ FICHIER DES ASSOCIATIONS DU LENDEMAIN                             
       FD  FNV220O                                                              
           RECORDING F                                                          
           BLOCK 0  RECORDS                                                     
           LABEL RECORD STANDARD.                                               
       01  FNV220O-ENREGISTREMENT  PIC X(80).                                   
      *                                                                         
      *                                                                         
       WORKING-STORAGE SECTION.                                                 
           COPY SYBWDIV0.                                                       
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES SPECIFIQUES PROGRAMME                                 *          
      *--------------------------------------------------------------*          
      *                                                                         
      *--- DSECT DU FICHIER EN ENTREE                                           
       01 W-FNV220J-ENREG.                                                      
          03 W-FNV220J-CFAM            PIC X(05) VALUE SPACES.                  
          03 W-FNV220J-TYPASSOC        PIC X(01) VALUE SPACES.                  
          03 W-FNV220J-LIEU            PIC X(06) VALUE SPACES.                  
          03 W-FNV220J-DEP             OCCURS 5.                                
             05 W-FNV220J-NSOCD        PIC X(03) VALUE SPACES.                  
             05 W-FNV220J-NLIEUD       PIC X(03) VALUE SPACES.                  
             05 W-FNV220J-PRIORITE     PIC X(01) VALUE SPACES.                  
          03 FILLER                    PIC X(33) VALUE SPACES.                  
       01 I-DEP                        PIC S9(03) COMP-3.                       
      *                                                                         
      *--- DSECT DU FICHIER EN SORTIE                                           
       01 W-FNV220O-ENREG.                                                      
          03 W-FNV220O-CFAM            PIC X(05) VALUE SPACES.                  
          03 FILLER                    PIC X(01) VALUE ';'.                     
          03 W-FNV220O-LIEU            PIC X(06) VALUE SPACES.                  
          03 FILLER                    PIC X(01) VALUE ';'.                     
          03 W-FNV220O-TYPASSOC        PIC X(01) VALUE SPACES.                  
          03 FILLER                    PIC X(01) VALUE ';'.                     
          03 W-FNV220O-NSOCD           PIC X(03) VALUE SPACES.                  
          03 W-FNV220O-NLIEUD          PIC X(03) VALUE SPACES.                  
          03 FILLER                    PIC X(01) VALUE ';'.                     
          03 W-FNV220O-PRIORITE        PIC X(01) VALUE SPACES.                  
          03 FILLER                    PIC X(57) VALUE SPACES.                  
       01 W-TITRE.                                                              
          03 FILLER                    PIC X(29)                                
             VALUE 'CFAM;CMAG;TYPE;DEPOT;PRIORITE'.                             
          03 FILLER                    PIC X(51) VALUE SPACES.                  
      *                                                                         
      *----COMPTEURS D'ENREGISTREMENTS                                          
      *                                                                         
       01 CPT-GENERAL.                                                          
          03 CPT-FNV220O-EC    PIC 9(08) COMP-3   VALUE ZERO.                   
          03 CPT-FNV220J-LU    PIC 9(08) COMP-3   VALUE ZERO.                   
      *                                                                         
       01  PIC-EDIT            PIC Z(07)9.                                      
      *                                                                         
      *--- STATISTIQUES DE TRAITEMENT                                           
      *                                                                         
       01 CR-STAT.                                                              
          03 CR-TIME.                                                           
             05 CR-HEURE         PIC 9(2).                                      
             05 CR-MINUTE        PIC 9(2).                                      
             05 CR-SECONDE       PIC 9(2).                                      
             05 FILLER           PIC 9(2).                                      
          03 CR-DUREE            PIC 9(8).                                      
          03 CR-DATE             PIC 9(6).                                      
          03 CR-JOUR-DEBUT       PIC 9(5).                                      
          03 CR-JOUR-FIN         PIC 9(5).                                      
      *                                                                         
      *--- FLAGS                                                                
      *                                                                         
       01 F-NV220J           PIC 9     VALUE 0.                                 
          88 DEBUT-FNV220J             VALUE 0.                                 
          88 FIN-FNV220J               VALUE 1.                                 
      *                                                                         
      *--------------------------------------------------------------*          
      *  ZONES BUFFER D'ENTREE/SORTIE                                *          
      *--------------------------------------------------------------*          
       01 FILLER  PIC X(16)   VALUE '*** Z-INOUT ****'.                         
       01 Z-INOUT PIC X(4096) VALUE SPACE.                                      
      *                                                                         
      *                                                                         
      *--------------------------------------------------------------*          
      *  MODULE ABEND / BETDATC / BFTNPER                            *          
      *--------------------------------------------------------------*          
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND  PIC X(8) VALUE 'ABEND'.                                       
      *--                                                                       
       01  MW-ABEND  PIC X(8) VALUE 'ABEND'.                                    
      *}                                                                        
           COPY ABENDCOP.                                                       
      *                                                                         
      *                                                                         
      *****************************************************************         
      *             P R O C E D U R E    D I V I S I O N              *         
      *****************************************************************         
       PROCEDURE DIVISION.                                                      
      *-------------------                                                      
       MODULE-BNV220.                                                           
      *--------------                                                           
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM TRAITEMENT                                                   
           PERFORM AFFICHAGE-COMPTEUR                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-BNV220.                 EXIT.                                 
      *                                                                         
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-          BIENVENUE DANS LE PROGRAMME          -'          
           DISPLAY '-                    BNV220                     -'          
           DISPLAY '-------------------------------------------------'          
      *                                                                         
           MOVE 'BNV220' TO  ABEND-PROG                                         
      *                                                                         
           ACCEPT CR-DATE FROM DATE                                             
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-JOUR-DEBUT FROM DAY                                        
      *                                                                         
           COMPUTE CR-DUREE  = CR-HEURE * 60                                    
                             + CR-MINUTE * 60 + CR-SECONDE                      
      *                                                                         
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-                                               -'          
      *                                                                         
      *----OUVERTURE DES FICHIERS                                               
           OPEN INPUT   FNV220J.                                                
           OPEN OUTPUT  FNV220O.                                                
      *                                                                         
       FIN-MODULE-ENTREE.                 EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       TRAITEMENT                      SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE CPT-GENERAL                                               
           SET DEBUT-FNV220J TO TRUE                                            
      *                                                                         
           PERFORM LECTURE-FNV220J                                              
           IF FIN-FNV220J                                                       
              MOVE  'FICHIER DES ASSOCIATIONS J VIDE !!!' TO ABEND-MESS         
              PERFORM SORTIE-ANORMALE                                           
           END-IF                                                               
           PERFORM ECRITURE-ENTETE                                              
      *                                                                         
           PERFORM UNTIL FIN-FNV220J                                            
              INITIALIZE  W-FNV220O-ENREG                                       
              MOVE W-FNV220J-CFAM       TO W-FNV220O-CFAM                       
              IF W-FNV220J-TYPASSOC = 'P' THEN                                  
                 MOVE 'D'                  TO W-FNV220O-TYPASSOC                
              ELSE                                                              
                 MOVE W-FNV220J-TYPASSOC   TO W-FNV220O-TYPASSOC                
              END-IF                                                            
              MOVE W-FNV220J-LIEU       TO W-FNV220O-LIEU                       
              PERFORM VARYING I-DEP FROM 1 BY 1                                 
                                UNTIL I-DEP > 5                                 
                                OR W-FNV220J-DEP (I-DEP) <= SPACES              
                 MOVE W-FNV220J-NSOCD    (I-DEP) TO W-FNV220O-NSOCD             
                 MOVE W-FNV220J-NLIEUD   (I-DEP) TO W-FNV220O-NLIEUD            
                 MOVE W-FNV220J-PRIORITE (I-DEP) TO W-FNV220O-PRIORITE          
                 PERFORM ECRITURE-FNV220O                                       
              END-PERFORM                                                       
              PERFORM LECTURE-FNV220J                                           
           END-PERFORM.                                                         
      *                                                                         
       FIN-TRAITEMENT.                    EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       AFFICHAGE-COMPTEUR              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-           AFFICHAGE DES COMPTEURS             -'          
           MOVE  CPT-FNV220J-LU   TO  PIC-EDIT                                  
           DISPLAY '-  NBRE D''ASSOCIATION J LUES        '                      
                   PIC-EDIT '    -'                                             
           MOVE  CPT-FNV220O-EC  TO  PIC-EDIT                                   
           DISPLAY '-  NBRE DE LIGNES ECRITE EN SORTIE  '                       
                   PIC-EDIT '    -'.                                            
      *                                                                         
       FIN-AFFICHAGE-COMPTEUR.            EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       LECTURE-FNV220J                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           READ FNV220J INTO W-FNV220J-ENREG                                    
             AT END                                                             
                SET FIN-FNV220J        TO TRUE                                  
             NOT END                                                            
                ADD 1                  TO CPT-FNV220J-LU                        
           END-READ.                                                            
      *                                                                         
       FIN-LECTURE-FNV220J.               EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       ECRITURE-ENTETE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE  FNV220O-ENREGISTREMENT                                   
           WRITE FNV220O-ENREGISTREMENT FROM W-TITRE.                           
      *                                                                         
       FIN-ECRITURE-ENTETE.               EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       ECRITURE-FNV220O                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           ADD 1 TO CPT-FNV220O-EC                                              
           INITIALIZE  FNV220O-ENREGISTREMENT                                   
           WRITE FNV220O-ENREGISTREMENT FROM W-FNV220O-ENREG.                   
      *                                                                         
       FIN-ECRITURE-FNV220O.              EXIT.                                 
      *                                                                         
      *                                                                         
      *----------------------------------------                                 
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           CLOSE FNV220J FNV220O.                                               
      *                                                                         
           ACCEPT CR-TIME FROM TIME                                             
           ACCEPT CR-DATE FROM DATE                                             
           DISPLAY '-                                               -'          
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-  NOUS SOMMES LE ' CR-DATE(5:2) '/' CR-DATE(3:2)           
                                       '/' CR-DATE(1:2) '  '                    
                   '-  IL EST ' CR-HEURE ':' CR-MINUTE ':' CR-SECONDE           
                   '  -'                                                        
           COMPUTE CR-DUREE  = CR-DUREE - CR-HEURE * 60                         
                             - CR-MINUTE * 60 - CR-SECONDE                      
           ACCEPT CR-JOUR-FIN FROM DAY                                          
           IF CR-JOUR-FIN > CR-JOUR-DEBUT                                       
              COMPUTE CR-DUREE  = CR-DUREE                                      
                                + (CR-JOUR-FIN - CR-JOUR-DEBUT) * 86400         
           END-IF                                                               
           DIVIDE CR-DUREE  BY 60 GIVING CR-HEURE  REMAINDER CR-MINUTE          
           DIVIDE CR-MINUTE BY 60 GIVING CR-MINUTE REMAINDER CR-SECONDE         
           DISPLAY '-------------------------------------------------'          
           DISPLAY '-       DUREE TOTALE DU PROGRAMME '                         
                   CR-HEURE ':' CR-MINUTE ':' CR-SECONDE '      -'              
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      *                                                                         
       FIN-MODULE-SORTIE.                 EXIT.                                 
      *                                                                         
      *----------------------------------------                                 
       SORTIE-ANORMALE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY '><                    ' ABEND-PROG(1:6)                     
                                               '                  ><'           
           DISPLAY '><     FIN ANORMALE PROVOQUEE PAR PROGRAMME   ><'           
           DISPLAY '><><><><><><><><><><><><><><><><><><><><><><><><'           
           DISPLAY ABEND-MESS                                                   
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
      *                                                                         
       FIN-SORTIE-ANORMALE.               EXIT.                                 
      *                                                                         
