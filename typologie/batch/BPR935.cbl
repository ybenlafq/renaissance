      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BPR935.                                                      
       AUTHOR. DSA035.                                                          
      * -------------------------------------------------------------           
      *                                                                         
      *  MISE EN FORME DU FICHIER DES OFFRES DE TELESURVEILLANCE                
      *                                                                         
      ***************************************************************           
      *  MODIFICATION : 15 SEPTEMBRE 2008                           *           
      *  AUTEUR       : QUADRA INFORMATIQUE                         *           
      *  INTERVENTION : LES ZONES NUMERIQUES DOIVENT ETRE ENTRE " " *           
      *  ---------------------------------------------------------- *           
      *  INTERVENTION : correctif, boucle sur le traitement d'une   *           
      *  zone num�rique qui n'existe pas...                         *           
      *  il faudrait reprendre le programmes il n'est pas vraiment  *           
      *  adapt�.                                                    *           
      * --------------------------------------------------------------          
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPR934  ASSIGN TO  FPR934.                                  
      *--                                                                       
            SELECT  FPR934  ASSIGN TO  FPR934                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPR935  ASSIGN TO  FPR935.                                  
      *--                                                                       
            SELECT  FPR935  ASSIGN TO  FPR935                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      ******************************************************************        
       DATA DIVISION.                                                           
      ******************************************************************        
       FILE SECTION.                                                            
      *                                                                         
       FD  FPR934 RECORDING F                                                   
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(400).                                         
      *                                                                         
       FD  FPR935 RECORDING V                                                   
           BLOCK 0 RECORDS                                                      
           RECORD VARYING FROM 10 TO 396 DEPENDING ON FPR935-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FPR935.                                                          
           02  REC-FPR935    PIC X(394).                                        
           02  0D-FPR935     PIC X.                                             
           02  0A-FPR935     PIC X.                                             
      ******************************************************************        
       WORKING-STORAGE SECTION.                                                 
      *                                                                         
      *                                                                         
      *01  FPR934-ENR   PIC X(400).                                             
           COPY FPR934.                                                         
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FPR934 VALUE 'F'.                                             
       01  CPT-FPR934  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  CPT-FPR935  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FPR935-ENR   PIC X(400).                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  FPR935-LENGTH PIC 9(4) COMP.                                         
      *--                                                                       
       01  FPR935-LENGTH PIC 9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MOV-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-MOV-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEB-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-DEB-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *01  0D          PIC X VALUE X'0D'.                                       
      *--                                                                       
       01  0D          PIC X VALUE X'0D'.                                       
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *01  0A          PIC X VALUE X'0A'.                                       
      *--                                                                       
       01  0A          PIC X VALUE X'8E'.                                       
      *}                                                                        
      ******************************************************************        
      * ABEND et BETDATC                                                        
      ******************************************************************        
           COPY ABENDCOP.                                                       
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
      ******************************************************************        
      * Proc: D�but, Milieu, Fin.                                               
      ******************************************************************        
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BPR935.                                               
           PERFORM  TRAIT-BPR935.                                               
           PERFORM  FIN-BPR935.                                                 
      ******************************************************************        
      * Ouverture de fichiers, param�tres: FDATE.                               
      ******************************************************************        
       DEBUT-BPR935 SECTION.                                                    
           DISPLAY 'BPR935: FICHIERS OFFRES TELESURVEILLANCE'                   
           OPEN INPUT FPR934                                                    
               OUTPUT FPR935.                                                   
      ******************************************************************        
      * Programme:                                                              
      * - Recopie du fichier                                                    
      ******************************************************************        
       TRAIT-BPR935 SECTION.                                                    
           PERFORM LECTURE-FPR934.                                              
           PERFORM UNTIL FIN-FPR934                                             
              PERFORM ECRITURE-FPR935                                           
              PERFORM LECTURE-FPR934                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * LECTURE FPR934                                                          
      ******************************************************************        
       LECTURE-FPR934 SECTION.                                                  
           READ FPR934 INTO FPR934-ENREG                                        
             AT END                                                             
                SET FIN-FPR934 TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FPR934                                             
                MOVE SPACE TO FPR934-NFICHIER                                   
           END-READ.                                                            
      ******************************************************************        
      * Ecriture sur FPR935                                                     
      ******************************************************************        
       ECRITURE-FPR935 SECTION.                                                 
           PERFORM CONDENSATION-FPR935.                                         
           MOVE 0D TO 0D-FPR935.                                                
           MOVE 0A TO 0A-FPR935.                                                
           WRITE ENR-FPR935 FROM FPR935-ENR.                                    
           ADD 1 TO CPT-FPR935.                                                 
      ******************************************************************        
      * CONDENSATION DE FPR934-ENR                                              
      ******************************************************************        
       CONDENSATION-FPR935 SECTION.                                             
           MOVE FPR934-ENREG TO FPR935-ENR.                                     
           MOVE 1 TO I-DEB-CHAMP.                                               
           PERFORM UNTIL FPR935-ENR(I-DEB-CHAMP:) = ' '                         
              IF FPR935-ENR(I-DEB-CHAMP:1) = ','                                
                 ADD 1 TO I-DEB-CHAMP                                           
              ELSE                                                              
                 IF FPR935-ENR(I-DEB-CHAMP:1) = '"'                             
                    PERFORM CONDENSATION-FPR935-ALPHA                           
                 ELSE                                                           
                    PERFORM CONDENSATION-FPR935-NUM                             
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE FPR935-LENGTH = I-DEB-CHAMP - 1.                             
       CONDENSATION-FPR935-ALPHA SECTION.                                       
     *** On vire les espaces � gauche...                                        
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FPR935-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR935-ENR   (I-MOV-CHAMP:)                                  
                TO FPR934-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR934-ENREG TO FPR935-ENR                                   
           END-PERFORM.                                                         
     *** ...on cherche la fin du champ...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FPR935-ENR(I-DEB-CHAMP:1) = '"'                              
           END-PERFORM.                                                         
     *** ...on cherche le dernier caract�re � droite...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FPR935-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
           ADD 1 TO I-DEB-CHAMP.                                                
     *** ...on vire les espaces � droite...                                     
           PERFORM UNTIL FPR935-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR935-ENR   (I-MOV-CHAMP:)                                  
                TO FPR934-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR934-ENREG TO FPR935-ENR                                   
           END-PERFORM.                                                         
     *** ...et on se positionne sur le prochain champ                           
           ADD 1 TO I-DEB-CHAMP.                                                
       CONDENSATION-FPR935-NUM SECTION.                                         
     *** On vire les espaces � gauche...                                        
           if fPR935-ENR(I-deb-CHAMP:1) not =  ' '                              
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR935-ENR   (I-MOV-CHAMP:)                                  
                TO FPR934-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR934-ENREG TO FPR935-ENR                                   
           end-if                                                               
     *** ...on cherche la fin du champ...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FPR935-ENR(I-DEB-CHAMP:1) = ','                              
                OR FPR935-ENR(I-DEB-CHAMP:) = ' '                               
                display 'ttt ' i-deb-champ                                      
           END-PERFORM.                                                         
     *** ...on cherche le dernier caract�re � droite...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FPR935-ENR(I-DEB-CHAMP:1) not = ' '                          
           END-PERFORM.                                                         
     *** ...et on vire les espaces � droite...                                  
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FPR935-ENR(I-DEB-CHAMP:1) > ' '                        
                      OR FPR935-ENR(I-DEB-CHAMP:) = ' '                         
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR935-ENR   (I-MOV-CHAMP:)                                  
                TO FPR934-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR934-ENREG  TO FPR935-ENR                                  
           END-PERFORM.                                                         
     *** (on est positionn� sur la virgule de fin de champ)                     
      ******************************************************************        
      * C'est la fin des Haricots.                                              
      ******************************************************************        
       FIN-BPR935 SECTION.                                                      
           CLOSE FPR934 FPR935.                                                 
           DISPLAY '* BPR935: FIN NORMALE DE PROGRAMME'.                        
           DISPLAY '*'.                                                         
           DISPLAY '*       NB. ENREGS LUS SUR FPR934: ' CPT-FPR934.            
           DISPLAY '*'.                                                         
           DISPLAY '*     NB. ENREGS ECRIS SUR FPR935: ' CPT-FPR935.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
      ******************************************************************        
      ******************************************************************        
      * BadaBoum                                                                
      ******************************************************************        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BPR935' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
