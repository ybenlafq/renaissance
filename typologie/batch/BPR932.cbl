      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       IDENTIFICATION DIVISION.                                                 
       PROGRAM-ID. BPR932.                                                      
       AUTHOR. DSA014.                                                          
      ******************************************************************        
      *                                                                         
      *  MISE EN FORME DU FICHIER � ENVOYER � AXA                               
      *                                                                         
      ******************************************************************        
       ENVIRONMENT DIVISION.                                                    
       CONFIGURATION SECTION.                                                   
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.                                   
       INPUT-OUTPUT SECTION.                                                    
       FILE-CONTROL.                                                            
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPR930  ASSIGN TO  FPR930.                                  
      *--                                                                       
            SELECT  FPR930  ASSIGN TO  FPR930                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT  FPR931  ASSIGN TO  FPR931.                                  
      *--                                                                       
            SELECT  FPR931  ASSIGN TO  FPR931                                   
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
       DATA DIVISION.                                                           
       FILE SECTION.                                                            
      *                                                                         
       FD  FPR930 RECORDING F                                                   
           BLOCK 0 RECORDS                                                      
           LABEL RECORD STANDARD.                                               
      *MW DAR-2                                                                 
       01  MW-FILLER        PIC X(359).                                         
      *                                                                         
       FD  FPR931 RECORDING V                                                   
           BLOCK 0 RECORDS                                                      
           RECORD VARYING FROM 10 TO 355 DEPENDING ON FPR931-LENGTH             
           LABEL RECORD STANDARD.                                               
       01  ENR-FPR931.                                                          
           02  REC-FPR931    PIC X(353).                                        
           02  0D-FPR931     PIC X.                                             
           02  0A-FPR931     PIC X.                                             
       WORKING-STORAGE SECTION.                                                 
       01  FPR931-ENTETE.                                                       
           05 FPR931-DATE                      PIC X(08).                       
           05 FPR930-IDFIC                     PIC X(04).                       
       01  FPR931-FIN.                                                          
           05  FPR931-NB-ENREG                 PIC 9(10)  VALUE 0.              
           COPY FPR930.                                                         
       01  FILLER PIC X VALUE SPACE.                                            
           88 FIN-FPR930 VALUE 'F'.                                             
       01  FILLER      PIC X(12)                VALUE '$CPT-FPR930$'.           
       01  CPT-FPR930  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER      PIC X(12)                VALUE '$CPT-FPR931$'.           
       01  CPT-FPR931  PIC S9(7) PACKED-DECIMAL VALUE 0.                        
       01  FILLER      PIC X(12)                VALUE '$FPR931-ENR$'.           
       01  FPR931-ENR   PIC X(359).                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  FPR931-LENGTH PIC 9(4) COMP.                                         
      *--                                                                       
       01  FPR931-LENGTH PIC 9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MOV-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-MOV-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEB-CHAMP PIC 9(4) COMP.                                           
      *--                                                                       
       01  I-DEB-CHAMP PIC 9(4) COMP-5.                                         
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *01  0D          PIC X VALUE X'0D'.                                       
      *--                                                                       
       01  0D          PIC X VALUE X'0D'.                                       
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *01  0A          PIC X VALUE X'0A'.                                       
      *--                                                                       
       01  0A          PIC X VALUE X'8E'.                                       
      *}                                                                        
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *01  ABEND   PIC X(8) VALUE 'ABEND  '.                                    
      *--                                                                       
       01  MW-ABEND   PIC X(8) VALUE 'ABEND  '.                                 
      *}                                                                        
           COPY ABENDCOP.                                                       
       PROCEDURE DIVISION.                                                      
           PERFORM  DEBUT-BPR932.                                               
           PERFORM  TRAIT-BPR932.                                               
           PERFORM  FIN-BPR932.                                                 
       DEBUT-BPR932 SECTION.                                                    
           OPEN INPUT FPR930                                                    
               OUTPUT FPR931.                                                   
       TRAIT-BPR932 SECTION.                                                    
           PERFORM LECTURE-FPR930.                                              
      *- ECRITURE DE L'ENTETE DE FICHIER                                        
           IF NOT FIN-FPR930                                                    
              MOVE FPR930-DFINPER  TO FPR931-DATE                               
              MOVE FPR930-DFINPER (3:4) TO FPR930-IDFIC                         
              MOVE 12                   TO FPR931-LENGTH                        
              WRITE ENR-FPR931 FROM FPR931-ENTETE                               
           END-IF.                                                              
           PERFORM UNTIL FIN-FPR930                                             
              PERFORM ECRITURE-FPR931                                           
              PERFORM LECTURE-FPR930                                            
           END-PERFORM.                                                         
       LECTURE-FPR930 SECTION.                                                  
           READ FPR930 INTO FPR930-ENREG                                        
             AT END                                                             
                SET FIN-FPR930 TO TRUE                                          
             NOT AT END                                                         
                ADD 1 TO CPT-FPR930                                             
      * - ELIMINATION DE CE QUI A SERVI � ECLATER LE FICHIER.                   
                MOVE SPACE TO FPR930-NFICHIER                                   
           END-READ.                                                            
       ECRITURE-FPR931 SECTION.                                                 
           PERFORM CONDENSATION-FPR931.                                         
           MOVE 0D TO 0D-FPR931.                                                
           MOVE 0A TO 0A-FPR931.                                                
           WRITE ENR-FPR931 FROM FPR931-ENR.                                    
           ADD 1 TO CPT-FPR931 FPR931-NB-ENREG.                                 
       CONDENSATION-FPR931 SECTION.                                             
           MOVE FPR930-ENREG TO FPR931-ENR.                                     
           MOVE 1 TO I-DEB-CHAMP.                                               
           PERFORM UNTIL FPR931-ENR(I-DEB-CHAMP:) = ' '                         
              IF FPR931-ENR(I-DEB-CHAMP:1) = ','                                
                 ADD 1 TO I-DEB-CHAMP                                           
              ELSE                                                              
                 IF FPR931-ENR(I-DEB-CHAMP:1) = '"'                             
                    PERFORM CONDENSATION-FPR931-ALPHA                           
                 ELSE                                                           
                    PERFORM CONDENSATION-FPR931-NUM                             
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           COMPUTE FPR931-LENGTH = I-DEB-CHAMP - 1.                             
       CONDENSATION-FPR931-ALPHA SECTION.                                       
     *** On vire les espaces � gauche...                                        
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FPR931-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR931-ENR   (I-MOV-CHAMP:)                                  
                TO FPR930-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR930-ENREG TO FPR931-ENR                                   
           END-PERFORM.                                                         
     *** ...on cherche la fin du champ...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FPR931-ENR(I-DEB-CHAMP:1) = '"'                              
           END-PERFORM.                                                         
     *** ...on cherche le dernier caract�re � droite...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FPR931-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
           ADD 1 TO I-DEB-CHAMP.                                                
     *** ...on vire les espaces � droite...                                     
           PERFORM UNTIL FPR931-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR931-ENR    (I-MOV-CHAMP:)                                 
                TO FPR930-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR930-ENREG TO FPR931-ENR                                   
           END-PERFORM.                                                         
     *** ...et on se positionne sur le prochain champ                           
           ADD 1 TO I-DEB-CHAMP.                                                
       CONDENSATION-FPR931-NUM SECTION.                                         
     *** On vire les espaces � gauche...                                        
           PERFORM UNTIL FPR931-ENR(I-DEB-CHAMP:1) > ' '                        
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR931-ENR (I-MOV-CHAMP:)                                    
              TO FPR930-ENREG (I-DEB-CHAMP:)                                    
              MOVE FPR930-ENREG TO FPR931-ENR                                   
           END-PERFORM.                                                         
     *** ...on cherche la fin du champ...                                       
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY 1                    
             UNTIL FPR931-ENR(I-DEB-CHAMP:1) = ','                              
                OR FPR931-ENR(I-DEB-CHAMP:) = ' '                               
           END-PERFORM.                                                         
     *** ...on cherche le dernier caract�re � droite...                         
           SUBTRACT 1 FROM I-DEB-CHAMP.                                         
           PERFORM VARYING I-DEB-CHAMP FROM I-DEB-CHAMP BY -1                   
             UNTIL FPR931-ENR(I-DEB-CHAMP:1) > ' '                              
           END-PERFORM.                                                         
     *** ...et on vire les espaces � droite...                                  
           ADD 1 TO I-DEB-CHAMP.                                                
           PERFORM UNTIL FPR931-ENR(I-DEB-CHAMP:1) > ' '                        
                      OR FPR931-ENR(I-DEB-CHAMP:) = ' '                         
              COMPUTE I-MOV-CHAMP = I-DEB-CHAMP + 1                             
              MOVE FPR931-ENR   (I-MOV-CHAMP:)                                  
                TO FPR930-ENREG (I-DEB-CHAMP:)                                  
              MOVE FPR930-ENREG TO FPR931-ENR                                   
           END-PERFORM.                                                         
       FIN-BPR932 SECTION.                                                      
      *- ECRITURE DU DERNIER ENREGISTREMENT                                     
           MOVE  10             TO FPR931-LENGTH                                
           WRITE ENR-FPR931   FROM FPR931-FIN.                                  
           CLOSE FPR930 FPR931.                                                 
           DISPLAY '* BPR932: FIN NORMALE DE PROGRAMME'.                        
           DISPLAY '*'.                                                         
           DISPLAY '*       NB. ENREGS LUS SUR FPR930: ' CPT-FPR930.            
           DISPLAY '*'.                                                         
           DISPLAY '*     NB. ENREGS ECRIS SUR FPR931: ' CPT-FPR931.            
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                            
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       ABEND-PROGRAMME SECTION.                                                 
           DISPLAY ABEND-MESS                                                   
           MOVE 'BPR932' TO ABEND-PROG.                                         
      *{ Tr-Change-Declared-Identifier 1.10                                     
      *    CALL ABEND USING ABEND-PROG ABEND-MESS.                              
      *--                                                                       
           CALL MW-ABEND USING ABEND-PROG ABEND-MESS.                           
      *}                                                                        
