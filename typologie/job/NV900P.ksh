#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV900P.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/07/25 AT 15.19.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV900P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#         QUIESCE DES TABLES EN MISE A JOUR DANS CETTE CHAINE                  
#  REPRISE: VERIFER BACKOUT-PLAN (RECOVER TO RBA , VERIFIER LE RBA ..)         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV900PA
       ;;
(NV900PA)
#
#NV900PDG
#NV900PDG Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#NV900PDG
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV900PAA
       ;;
(NV900PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
# ******** FICHIER CONTENANT LE NUMERO DE RBA                                  
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PNCGP/F07.NV0999AP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/NV900PAA
       m_ProgramExec IEFBR14 "RDAR,NV900P.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NV900PAD
       ;;
(NV900PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STOPAPLP.sysin
       m_UtilityExec
# ********************************************************************         
#   WAIT DE 1 MINUTE  POUR ETRE SUR QUE L APPLY EST BIEN ARRETE                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PAG PGM=HWAIT      ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV900PAG
       ;;
(NV900PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_ProgramExec HWAIT "060"
# ********************************************************************         
#   DELETE DE LA TABLE TTSL11 AVEC CLAUSE WHERE                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV900PAJ
       ;;
(NV900PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PAJ.sysin
       m_ExecSQL -f SYSIN
# *******************************************************                      
#   DELETE DE LA TABLE TTGS10                                                  
#   REPRISE: OUI                                                               
# *******************************************************                      
#                                                                              
# ***********************************                                          
# *   STEP NV900PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NV900PAM
       ;;
(NV900PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PAM.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NV900PAN
       ;;
(NV900PAN)
       m_CondExec 04,GE,NV900PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTNV11 +UNLOAD DE LA TABLE RTNV00                        
#  UNLOAD DE LA TABLE RTNV12 +UNLOAD DE LA TABLE TSSL11(DATA PROPAGATO         
#  UNLOAD DE LA TABLE TSGS10(DATA PROPAGATOR)                                  
# ********************************************************************         
#  CES TS DOIVENT ETRE  ABSOLUMENT DISPO AFIN DE FAIRE LA PURGE                
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NV900PAQ
       ;;
(NV900PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
#    RSBS01   : NAME=RSBS01,MODE=U - DYNAM=YES                                 
#    RSBS02   : NAME=RSBS02,MODE=U - DYNAM=YES                                 
#    RSBS03   : NAME=RSBS03,MODE=U - DYNAM=YES                                 
#    RSFL40   : NAME=RSFL40,MODE=U - DYNAM=YES                                 
#    RSFL50   : NAME=RSFL50,MODE=U - DYNAM=YES                                 
#    RSGA00   : NAME=RSGA00,MODE=U - DYNAM=YES                                 
#    RSGA01   : NAME=RSGA01,MODE=U - DYNAM=YES                                 
#    RSGA10   : NAME=RSGA10,MODE=U - DYNAM=YES                                 
#    RSGA14   : NAME=RSGA14,MODE=U - DYNAM=YES                                 
#    RSGA18   : NAME=RSGA18,MODE=U - DYNAM=YES                                 
#    RSGA22   : NAME=RSGA22,MODE=U - DYNAM=YES                                 
#    RSGA24   : NAME=RSGA24,MODE=U - DYNAM=YES                                 
#    RSGA27   : NAME=RSGA27,MODE=U - DYNAM=YES                                 
#    RSGA30   : NAME=RSGA30,MODE=U - DYNAM=YES                                 
#    RSGA31   : NAME=RSGA31,MODE=U - DYNAM=YES                                 
#    RSGA52   : NAME=RSGA52,MODE=U - DYNAM=YES                                 
#    RSGA53   : NAME=RSGA53,MODE=U - DYNAM=YES                                 
#    RSGA58   : NAME=RSGA58,MODE=U - DYNAM=YES                                 
#    RSGA59   : NAME=RSGA59,MODE=U - DYNAM=YES                                 
#    RSGA63   : NAME=RSGA63,MODE=U - DYNAM=YES                                 
#    RSGA64   : NAME=RSGA64,MODE=U - DYNAM=YES                                 
#    RSGA75   : NAME=RSGA75,MODE=U - DYNAM=YES                                 
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
#    RSGG20   : NAME=RSGG20,MODE=U - DYNAM=YES                                 
#    RSGG40   : NAME=RSGG40,MODE=U - DYNAM=YES                                 
#    RSGQ03   : NAME=RSGQ03,MODE=U - DYNAM=YES                                 
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
#    RSLI00   : NAME=RSLI00,MODE=U - DYNAM=YES                                 
#    RSLI01   : NAME=RSLI01,MODE=U - DYNAM=YES                                 
#    RSNV21   : NAME=RSNV21,MODE=U - DYNAM=YES                                 
#    RSPR00   : NAME=RSPR00,MODE=U - DYNAM=YES                                 
#    RSPR03   : NAME=RSPR03,MODE=U - DYNAM=YES                                 
#    RSPR06   : NAME=RSPR06,MODE=U - DYNAM=YES                                 
#    RSPR53   : NAME=RSPR53,MODE=U - DYNAM=YES                                 
#    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES                                 
#                                                                              
#    RSNV12   : NAME=RSNV12,MODE=U - DYNAM=YES                                 
#                                                                              
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
#                                                                              
#    RSNV02   : NAME=RSNV02,MODE=U - DYNAM=YES                                 
#                                                                              
#    TSSL11   : NAME=TSSL11,MODE=U - DYNAM=YES                                 
#                                                                              
#    TSGS10   : NAME=TSGS10,MODE=U - DYNAM=YES                                 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PTEM/NV900PAQ.NV11UNLP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC02 ${DATA}/PTEM/NV900PAQ.NV02UNLP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC03 ${DATA}/PTEM/NV900PAQ.NV12UNLP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC04 ${DATA}/PTEM/NV900PAQ.NV00UNLP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC05 ${DATA}/PTEM/NV900PAQ.SL11UNLP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC06 ${DATA}/PTEM/NV900PAQ.TGS10UNP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PAQ.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DU FICHIER UNLOAD DE RTNV11  .LRECL 139 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NV900PAT
       ;;
(NV900PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV900PAQ.NV11UNLP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/NV900PAQ.NV02UNLP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 139 -g +1 SORTOUT ${DATA}/PTEM/NV900PAT.NV11AAP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_61_12 61 CH 12
 /KEYS
   FLD_CH_61_12 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PAU
       ;;
(NV900PAU)
       m_CondExec 00,EQ,NV900PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE RTNV12  .LRECL 290 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NV900PAX
       ;;
(NV900PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NV900PAQ.NV12UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 290 -g +1 SORTOUT ${DATA}/PTEM/NV900PAX.NV12AAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 01 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PAY
       ;;
(NV900PAY)
       m_CondExec 00,EQ,NV900PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 334 DE LONG (AVEC OMIT )           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBA
       ;;
(NV900PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NV900PAQ.NV00UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 334 -g +1 SORTOUT ${DATA}/PTEM/NV900PBA.NV00AAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ENVOYE    "
 /FIELDS FLD_CH_287_10 287 CH 10
 /FIELDS FLD_CH_297_12 297 CH 12
 /CONDITION CND_1 FLD_CH_287_10 EQ CST_1_4 
 /KEYS
   FLD_CH_297_12 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBB
       ;;
(NV900PBB)
       m_CondExec 00,EQ,NV900PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 334 DE LONG (AVEC OMIT )           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBD
       ;;
(NV900PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/NV900PBA.NV00AAP
       m_FileAssign -d NEW,CATLG,DELETE -r 334 -g +1 SORTOUT ${DATA}/PTEM/NV900PBD.NV00FFP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "RAF       "
 /FIELDS FLD_CH_297_12 297 CH 12
 /FIELDS FLD_CH_287_10 287 CH 10
 /CONDITION CND_1 FLD_CH_287_10 EQ CST_1_4 
 /KEYS
   FLD_CH_297_12 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBE
       ;;
(NV900PBE)
       m_CondExec 00,EQ,NV900PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 334 DE LONG (AVEC OMIT )           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBG
       ;;
(NV900PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/NV900PBA.NV00AAP
       m_FileAssign -d NEW,CATLG,DELETE -r 334 -g +1 SORTOUT ${DATA}/PXX0/F07.V900LOGP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "RAF       "
 /FIELDS FLD_CH_287_10 287 CH 10
 /FIELDS FLD_CH_297_12 297 CH 12
 /CONDITION CND_1 FLD_CH_287_10 EQ CST_1_4 
 /KEYS
   FLD_CH_297_12 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBH
       ;;
(NV900PBH)
       m_CondExec 00,EQ,NV900PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 334 DE LONG  (INCLUDE RAF)         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBJ
       ;;
(NV900PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/NV900PAQ.NV00UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 334 -g +1 SORTOUT ${DATA}/PTEM/NV900PBJ.NV00BBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ENVOYE    "
 /FIELDS FLD_CH_297_12 297 CH 12
 /FIELDS FLD_CH_287_10 287 CH 10
 /CONDITION CND_1 FLD_CH_287_10 EQ CST_1_4 
 /KEYS
   FLD_CH_297_12 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBK
       ;;
(NV900PBK)
       m_CondExec 00,EQ,NV900PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE TTSL11  .LRECL 143 DE LONG (AVEC OMIT )           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBM
       ;;
(NV900PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/NV900PAQ.SL11UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 143 -g +1 SORTOUT ${DATA}/PTEM/NV900PBM.SL11AAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ENVOYE    "
 /FIELDS FLD_CH_122_10 122 CH 10
 /FIELDS FLD_CH_132_12 132 CH 12
 /CONDITION CND_1 FLD_CH_122_10 EQ CST_1_4 
 /KEYS
   FLD_CH_132_12 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBN
       ;;
(NV900PBN)
       m_CondExec 00,EQ,NV900PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE TTSL11  .LRECL 143 DE LONG (AVEC INCLUDE)         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBQ
       ;;
(NV900PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/NV900PAQ.SL11UNLP
       m_FileAssign -d NEW,CATLG,DELETE -r 143 -g +1 SORTOUT ${DATA}/PTEM/NV900PBQ.SL11BBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ENVOYE    "
 /FIELDS FLD_CH_122_10 122 CH 10
 /FIELDS FLD_CH_132_12 132 CH 12
 /CONDITION CND_1 FLD_CH_122_10 EQ CST_1_4 
 /KEYS
   FLD_CH_132_12 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBR
       ;;
(NV900PBR)
       m_CondExec 00,EQ,NV900PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE TTGS10  .LRECL 167 DE LONG (AVEC OMIT )           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBT
       ;;
(NV900PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/NV900PAQ.TGS10UNP
       m_FileAssign -d NEW,CATLG,DELETE -r 167 -g +1 SORTOUT ${DATA}/PTEM/NV900PBT.GS10AAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ENVOYE    "
 /FIELDS FLD_CH_146_10 146 CH 10
 /FIELDS FLD_CH_156_12 156 CH 12
 /CONDITION CND_1 FLD_CH_146_10 EQ CST_1_4 
 /KEYS
   FLD_CH_156_12 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBU
       ;;
(NV900PBU)
       m_CondExec 00,EQ,NV900PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE TTGS10  .LRECL 167 DE LONG (AVEC INCLUDE)         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=NV900PBX
       ;;
(NV900PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/NV900PAQ.TGS10UNP
       m_FileAssign -d NEW,CATLG,DELETE -r 167 -g +1 SORTOUT ${DATA}/PTEM/NV900PBX.GS10BBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ENVOYE    "
 /FIELDS FLD_CH_146_10 146 CH 10
 /FIELDS FLD_CH_156_12 156 CH 12
 /CONDITION CND_1 FLD_CH_146_10 EQ CST_1_4 
 /KEYS
   FLD_CH_156_12 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PBY
       ;;
(NV900PBY)
       m_CondExec 00,EQ,NV900PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV900                                                                      
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=NV900PCA
       ;;
(NV900PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
#    RSBS01   : NAME=RSBS01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSFL40   : NAME=RSFL40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL40 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA18   : NAME=RSGA18,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA18 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA24   : NAME=RSGA24,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA24 /dev/null
#    RSGA27   : NAME=RSGA27,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA27 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA31   : NAME=RSGA31,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
#    RSGA52   : NAME=RSGA52,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA52 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSGA58   : NAME=RSGA58,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA59   : NAME=RSGA59,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA63   : NAME=RSGA63,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA63 /dev/null
#    RSGA64   : NAME=RSGA64,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA64 /dev/null
#    RSGA75   : NAME=RSGA75,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA75 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGG20   : NAME=RSGG20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG20 /dev/null
#    RSGG40   : NAME=RSGG40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG40 /dev/null
#    RSGQ03   : NAME=RSGQ03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ03 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSLI01   : NAME=RSLI01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI01 /dev/null
#    RSNV21   : NAME=RSNV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV21 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR03   : NAME=RSPR03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR03 /dev/null
#    RSPR06   : NAME=RSPR06,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR06 /dev/null
#    RSPR53   : NAME=RSPR53,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR53 /dev/null
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV11 /dev/null
# ****** TABLE                                                                 
#    RSNV12   : NAME=RSNV12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV12 /dev/null
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
# ****** TABLE                                                                 
#    RSNV02   : NAME=RSNV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV02 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREES                                                    
#                                                                              
       m_FileAssign -d SHR -g ${G_A12} FNV110 ${DATA}/PTEM/NV900PAT.NV11AAP
#                                                                              
       m_FileAssign -d SHR -g ${G_A13} FNV120 ${DATA}/PTEM/NV900PAX.NV12AAP
#                                                                              
       m_FileAssign -d SHR -g ${G_A14} FNV000 ${DATA}/PTEM/NV900PBJ.NV00BBP
#                                                                              
       m_FileAssign -d SHR -g ${G_A15} FSL110 ${DATA}/PTEM/NV900PBQ.SL11BBP
#                                                                              
       m_FileAssign -d SHR -g ${G_A16} FGS100 ${DATA}/PTEM/NV900PBX.GS10BBP
#                                                                              
#                                                                              
# ****** FICHIER EN SORTIES                                                    
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 139 -g +1 FNV111 ${DATA}/PTEM/NV900PCA.NV11CCP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 290 -g +1 FNV121 ${DATA}/PTEM/NV900PCA.NV12CCP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 334 -g +1 FNV001 ${DATA}/PTEM/NV900PCA.NV00CCP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 143 -g +1 FSL111 ${DATA}/PTEM/NV900PCA.SL11CCP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 167 -g +1 FGS101 ${DATA}/PTEM/NV900PCA.GS10CCP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 139 -g +1 FNV021 ${DATA}/PTEM/NV900PCA.NV02AAP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV900 
       JUMP_LABEL=NV900PCB
       ;;
(NV900PCB)
       m_CondExec 04,GE,NV900PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES DEUX FICHIERS AFIN DE RELOADER LA TABLE RTNV00                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=NV900PCD
       ;;
(NV900PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/NV900PBD.NV00FFP
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/NV900PCA.NV00CCP
       m_FileAssign -d NEW,CATLG,DELETE -r 334 -g +1 SORTOUT ${DATA}/PTEM/NV900PCD.NV00DDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_1_10 1 PD 10
 /KEYS
   FLD_PD_1_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PCE
       ;;
(NV900PCE)
       m_CondExec 00,EQ,NV900PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES DEUX FICHIERS AFIN DE RELOADER LA TABLE TTSL11                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=NV900PCG
       ;;
(NV900PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/NV900PBM.SL11AAP
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/NV900PCA.SL11CCP
       m_FileAssign -d NEW,CATLG,DELETE -r 143 -g +1 SORTOUT ${DATA}/PTEM/NV900PCG.SL11DDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_112_10 112 PD 10
 /KEYS
   FLD_PD_112_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PCH
       ;;
(NV900PCH)
       m_CondExec 00,EQ,NV900PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES DEUX FICHIERS AFIN DE RELOADER LA TABLE TTGS10                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=NV900PCJ
       ;;
(NV900PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/NV900PBT.GS10AAP
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/NV900PCA.GS10CCP
       m_FileAssign -d NEW,CATLG,DELETE -r 167 -g +1 SORTOUT ${DATA}/PTEM/NV900PCJ.GS10DDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_136_10 136 PD 10
 /KEYS
   FLD_PD_136_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PCK
       ;;
(NV900PCK)
       m_CondExec 00,EQ,NV900PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD POSITIONNEL DE LA TABLE RTNV00 A PARTIR DES SEQUENTIELS                
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=NV900PCM
       ;;
(NV900PCM)
       m_CondExec ${EXADW},NE,YES 
# *************************************************************                
#  CES TS DOIVENT ETRE  ABSOLUMENT DISPO AFIN DE FAIRE LA PURGE                
# *************************************************************                
#    RSBS01   : NAME=RSBS01,MODE=U - DYNAM=YES                                 
#    RSBS02   : NAME=RSBS02,MODE=U - DYNAM=YES                                 
#    RSBS03   : NAME=RSBS03,MODE=U - DYNAM=YES                                 
#    RSFL40   : NAME=RSFL40,MODE=U - DYNAM=YES                                 
#    RSFL50   : NAME=RSFL50,MODE=U - DYNAM=YES                                 
#    RSGA00   : NAME=RSGA00,MODE=U - DYNAM=YES                                 
#    RSGA01   : NAME=RSGA01,MODE=U - DYNAM=YES                                 
#    RSGA10   : NAME=RSGA10,MODE=U - DYNAM=YES                                 
#    RSGA14   : NAME=RSGA14,MODE=U - DYNAM=YES                                 
#    RSGA18   : NAME=RSGA18,MODE=U - DYNAM=YES                                 
#    RSGA22   : NAME=RSGA22,MODE=U - DYNAM=YES                                 
#    RSGA24   : NAME=RSGA24,MODE=U - DYNAM=YES                                 
#    RSGA27   : NAME=RSGA27,MODE=U - DYNAM=YES                                 
#    RSGA30   : NAME=RSGA30,MODE=U - DYNAM=YES                                 
#    RSGA31   : NAME=RSGA31,MODE=U - DYNAM=YES                                 
#    RSGA52   : NAME=RSGA52,MODE=U - DYNAM=YES                                 
#    RSGA53   : NAME=RSGA53,MODE=U - DYNAM=YES                                 
#    RSGA58   : NAME=RSGA58,MODE=U - DYNAM=YES                                 
#    RSGA59   : NAME=RSGA59,MODE=U - DYNAM=YES                                 
#    RSGA63   : NAME=RSGA63,MODE=U - DYNAM=YES                                 
#    RSGA64   : NAME=RSGA64,MODE=U - DYNAM=YES                                 
#    RSGA75   : NAME=RSGA75,MODE=U - DYNAM=YES                                 
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
#    RSGG20   : NAME=RSGG20,MODE=U - DYNAM=YES                                 
#    RSGG40   : NAME=RSGG40,MODE=U - DYNAM=YES                                 
#    RSGQ03   : NAME=RSGQ03,MODE=U - DYNAM=YES                                 
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
#    RSLI00   : NAME=RSLI00,MODE=U - DYNAM=YES                                 
#    RSLI01   : NAME=RSLI01,MODE=U - DYNAM=YES                                 
#    RSNV21   : NAME=RSNV21,MODE=U - DYNAM=YES                                 
#    RSPR00   : NAME=RSPR00,MODE=U - DYNAM=YES                                 
#    RSPR03   : NAME=RSPR03,MODE=U - DYNAM=YES                                 
#    RSPR06   : NAME=RSPR06,MODE=U - DYNAM=YES                                 
#    RSPR53   : NAME=RSPR53,MODE=U - DYNAM=YES                                 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR -g ${G_A23} SYSREC00 ${DATA}/PTEM/NV900PCD.NV00DDP
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
# -X-NV900PR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PCM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NV900PCN
       ;;
(NV900PCN)
       m_CondExec 04,GE,NV900PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * RELOAD DE LA TABLE RTNV11 A PARTIR DU FICHIER ISSU DU PGM BNV900           
# * REPRISE .OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCQ PGM=PTLDRIVM   ** ID=AEB                                   
# ***********************************                                          
#{ Post-translation ADAPT_LOAD_REPLACE
#       JUMP_LABEL=NV900PCQ
#       ;;
#(NV900PCQ)
#       m_CondExec ${EXAEB},NE,YES 
#       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
#       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
#       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
#       m_OutputAssign -c "*" ST01MSG
#       m_OutputAssign -c "*" PTIMSG01
#       m_OutputAssign -c "*" ST02MSG
#       m_OutputAssign -c "*" PTIMSG03
#       m_OutputAssign -c "*" ST03MSG
#       m_OutputAssign -c "*" PTIMSG02
#       m_OutputAssign -c "*" PTIMSG
#       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
## ORTWK01 DD UNIT=(TEM350),SPACE=(CYL,(060,99)),DISP=(,DELETE)                 
##                                                                              
#       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
#       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
##                                                                              
##    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES                                 
#       m_FileAssign -d SHR RSNV11 /dev/null
##                                                                              
## ******* FIC DE LOAD DE RTNV11                                                
#       m_FileAssign -d SHR -g ${G_A24} SYSULD ${DATA}/PTEM/NV900PCA.NV11CCP
#       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PCQ.sysin
#       
#       #Untranslated utility: PTLDRIVM
     JUMP_LABEL=NV900PCQ_SORT 
       ;;
(NV900PCQ_SORT)       
     m_CondExec ${EXAEB},NE,YES
     m_OutputAssign -c "*" SYSOUT
     m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PTEM/NV900PCA.NV11CCP
     m_FileAssign -d NEW,CATLG,DELETE SORTOUT ${MT_TMP}/NV900P_NV900PCQ_RSNV11_${MT_JOB_NAME}_${MT_JOB_PID}
     m_FileAssign -i SYSIN
/PADBYTE x"20"  
/FIELDS FLD_CH_61_12 61 CH 12  
/KEYS    FLD_CH_61_12 ASCENDING
_end       
     m_FileSort -s SYSIN -i SORTIN -o SORTOUT
     JUMP_LABEL=NV900PCQ
        ;;
(NV900PCQ)       
    m_CondExec ${EXAEB},NE,YES      
    m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB       
    m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB       
    m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE       
    m_OutputAssign -c "*" ST01MSG       
    m_OutputAssign -c "*" PTIMSG01       
    m_OutputAssign -c "*" ST02MSG       
    m_OutputAssign -c "*" PTIMSG03       
    m_OutputAssign -c "*" ST03MSG
    m_OutputAssign -c "*" PTIMSG02       
    m_OutputAssign -c "*" PTIMSG       
    m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED       
    m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED       
    m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED       
    m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED       
    m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED       
    m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
    # ORTWK01 DD UNIT=(TEM350),SPACE=(CYL,(060,99)),DISP=(,DELETE)
    m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM       
    m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED       
    m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
    #
    #    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES       
    m_FileAssign -d SHR RSNV11 /dev/null
    #
    # ******* FIC DE LOAD DE RTNV11
     m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PCQ.sysin
     m_FileAssign -d SHR SYSULD ${MT_TMP}/NV900P_NV900PCQ_RSNV11_${MT_JOB_NAME}_${MT_JOB_PID}
     m_OutputAssign -c "*" SYSERR01          
     m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/NV900P_NV900PCQ_RSNV11.sysload
      m_UtilityExec
#} Post-translation
# ********************************************************************         
# * RELOAD DE LA TABLE RTNV12 A PARTIR DU FICHIER ISSU DU PGM BNV900           
# * REPRISE .OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCT PGM=PTLDRIVM   ** ID=AEG                                   
# ***********************************                                          
#{ Post-translation ADAPT_LOAD_REPLACE
#       JUMP_LABEL=NV900PCT
#       ;;
#(NV900PCT)
#       m_CondExec ${EXAEG},NE,YES 
#       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
#       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
#       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
#       m_OutputAssign -c "*" ST01MSG
#       m_OutputAssign -c "*" PTIMSG01
#       m_OutputAssign -c "*" ST02MSG
#       m_OutputAssign -c "*" PTIMSG03
#       m_OutputAssign -c "*" ST03MSG
#       m_OutputAssign -c "*" PTIMSG02
#       m_OutputAssign -c "*" PTIMSG
#       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
## ORTWK01 DD UNIT=(TEM350),SPACE=(CYL,(060,99)),DISP=(,DELETE)                 
##                                                                              
#       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
#       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
##                                                                              
##    RSNV12   : NAME=RSNV12,MODE=U - DYNAM=YES                                 
#       m_FileAssign -d SHR RSNV12 /dev/null
##                                                                              
## ******* FIC DE LOAD DE RTNV12                                                
#       m_FileAssign -d SHR -g ${G_A25} SYSULD ${DATA}/PTEM/NV900PCA.NV12CCP
#       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PCT.sysin
#       
#       #Untranslated utility: PTLDRIVM
     JUMP_LABEL=NV900PCT_SORT
       ;;
(NV900PCT_SORT)
     m_CondExec ${EXAEG},NE,YES 
     m_OutputAssign -c "*" SYSOUT
     m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/NV900PCA.NV12CCP
     m_FileAssign -d NEW,CATLG,DELETE SORTOUT ${MT_TMP}/NV900P_NV900PCT_RSNV12_${MT_JOB_NAME}_${MT_JOB_PID}
     m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS FLD_CH_1_12 1 CH 12
/FIELDS FLD_CH_13_20 13 CH 20 
/FIELDS FLD_CH_73_60 73 CH 60
/FIELDS FLD_CH_133_1 133 CH  1
/FIELDS FLD_CH_134_26 134 CH 26
/KEYS
   FLD_CH_1_12  ASCENDING,
   FLD_CH_13_20  ASCENDING,
   FLD_CH_73_60  ASCENDING,
   FLD_CH_133_1  ASCENDING,
   FLD_CH_134_26  ASCENDING
_end
     m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PCT
       ;;
(NV900PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ORTWK01 DD UNIT=(TEM350),SPACE=(CYL,(060,99)),DISP=(,DELETE)                 
#                                                                              
       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#                                                                              
#    RSNV12   : NAME=RSNV12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV12 /dev/null
#                                                                              
# ******* FIC DE LOAD DE RTNV12                                                
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PCT.sysin
       m_FileAssign -d SHR SYSULD ${MT_TMP}/NV900P_NV900PCT_RSNV12_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/NV900P_NV900PCT_RSNV12.sysload
       m_UtilityExec
#} Post-translation
# ********************************************************************         
#  LOAD POSITIONNEL DE LA TABLE TTSL11 A PARTIR DES SEQUENTIELS                
# ********************************************************************         
# * RELOAD DE LA TABLE TTSL11 A PARTIR DU FICHIER ISSU DU PGM BNV900           
# * ET DES DIFFERENTS TRIS                                                     
# * REPRISE .OUI                                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=NV900PCX
       ;;
(NV900PCX)
       m_CondExec ${EXAEL},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#    TSSL11   : NAME=TSSL11,MODE=U - DYNAM=YES                                 
# ******* FIC DE LOAD DE RTSL11                                                
       m_FileAssign -d SHR -g ${G_A26} SYSREC00 ${DATA}/PTEM/NV900PCG.SL11DDP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PCX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NV900PCY
       ;;
(NV900PCY)
       m_CondExec 04,GE,NV900PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD POSITIONNEL DE LA TABLE TTGS10                                         
# * RELOAD DE LA TABLE TTGS10 A PARTIR DU FICHIER ISSU DU PGM BNV900           
# * REPRISE .OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PDA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=NV900PDA
       ;;
(NV900PDA)
       m_CondExec ${EXAEQ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#    TSGS10   : NAME=TSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR -g ${G_A27} SYSREC00 ${DATA}/PTEM/NV900PCJ.GS10DDP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PDA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NV900PDB
       ;;
(NV900PDB)
       m_CondExec 04,GE,NV900PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * RELOAD DE LA TABLE RTNV02 A PARTIR DU FICHIER ISSU DU PGM BNV900           
# * REPRISE .OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PDD PGM=PTLDRIVM   ** ID=AEV                                   
# ***********************************                                          
#{ Post-translation ADAPT_LOAD_REPLACE
#       JUMP_LABEL=NV900PDD
#       ;;
#(NV900PDD)
#       m_CondExec ${EXAEV},NE,YES 
#       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
#       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
#       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
#       m_OutputAssign -c "*" ST01MSG
#       m_OutputAssign -c "*" PTIMSG01
#       m_OutputAssign -c "*" ST02MSG
#       m_OutputAssign -c "*" PTIMSG03
#       m_OutputAssign -c "*" ST03MSG
#       m_OutputAssign -c "*" PTIMSG02
#       m_OutputAssign -c "*" PTIMSG
#       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
#       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
##                                                                              
##    RSNV02   : NAME=RSNV02,MODE=U - DYNAM=YES                                 
#       m_FileAssign -d SHR RSNV02 /dev/null
##                                                                              
## ******* FIC DE LOAD DE RTNV02                                                
#       m_FileAssign -d SHR -g ${G_A28} SYSULD ${DATA}/PTEM/NV900PCA.NV02AAP
#       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PDD.sysin
#       
#       #Untranslated utility: PTLDRIVM
     JUMP_LABEL=NV900PDD_SORT
       ;;
(NV900PDD_SORT)
     m_CondExec ${EXAEV},NE,YES 
     m_OutputAssign -c "*" SYSOUT
     m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/NV900PCA.NV02AAP
     m_FileAssign -d NEW,CATLG,DELETE SORTOUT ${MT_TMP}/NV900P_NV900PDD_RSNV02_${MT_JOB_NAME}_${MT_JOB_PID}
     m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS FLD_CH_61_12 61 CH 12
/KEYS    FLD_CH_61_12 ASCENDING
_end
     m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV900PDD
       ;;
(NV900PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#                                                                              
#    RSNV02   : NAME=RSNV02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV02 /dev/null
#                                                                              
# ******* FIC DE LOAD DE RTNV02                                                
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PDD.sysin
       m_FileAssign -d SHR  SYSULD ${MT_TMP}/NV900P_NV900PDD_RSNV02_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/NV900P_NV900PDD_RSNV02.sysload
       m_UtilityExec
#} Post-translation
# ********************************************************************         
#   REPAIR NOCOPY DU PREMIER TABLESPACE                                        
#       (REMISE DU STATUS EN READ WRITE DU TABLESPACE)                         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV900PDG PGM=DSNUTILB   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=NV900PZA
       ;;
(NV900PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV900PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
