#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GI54EY.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGI54E -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/09 AT 11.49.49 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GI54EY                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   LE PASSAGE DE LA CHAINE DOIT SE FAIRE APRES QUE LE DEPOT EST FINI          
#                    SON INVENTAIRE                                            
# ********************************************************************         
#  BIE640 : CREATION DU FICHIER DES ECARTS D'INVENTAIRE ENTREPOT               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GI54EYA
       ;;
(GI54EYA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GI54EYAA
       ;;
(GI54EYAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  LIEUX                                                                
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  ETATS                                                                
#    RSGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  INVENTAIRE                                                           
#    RSIE05   : NAME=RSIE05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  INVENTAIRE                                                           
#    RSIE60   : NAME=RSIE60Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE60 /dev/null
# ******  AVANCEMENT D'INVENTAIRE                                              
#    RSIE10   : NAME=RSIE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  PRMP MYON                                                            
#    RSGG51   : NAME=RSGG51Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG51 /dev/null
# ******  TABLE LOGISTIQUE                                                     
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIE540 ${DATA}/PNCGY/F45.BIE540GE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE640 
       JUMP_LABEL=GI54EYAB
       ;;
(GI54EYAB)
       m_CondExec 04,GE,GI54EYAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETEE SUR:                                     
#  SOCIETE MAG SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GI54EYAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYAD
       ;;
(GI54EYAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PNCGY/F45.BIE540GE
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GI54EYAD.IE143AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GI54EYAE
       ;;
(GI54EYAE)
       m_CondExec 00,EQ,GI54EYAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV243  :  EDITION DES ECARTS SOCIETE PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GI54EYAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYAG
       ;;
(GI54EYAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A2} FIV143 ${DATA}/PTEM/GI54EYAD.IE143AY
# ******  EDITION DES ECARTS SOCIETE PAR LIEU                                  
#  IIV143   REPORT SYSOUT=(9,IIV143),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV143 ${DATA}/PXX0.FICHEML.GI54EY.GIE143GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV243 
       JUMP_LABEL=GI54EYAH
       ;;
(GI54EYAH)
       m_CondExec 04,GE,GI54EYAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS DEPOT SUR:                                        
#  SOCIETE SEQ RAYON LIB RAYON MAGASIN                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GI54EYAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYAJ
       ;;
(GI54EYAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PNCGY/F45.BIE540GE
# ******  FICHIERS DES ECARTS MAGASINS TRIE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GI54EYAJ.IE144AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_3 65 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GI54EYAK
       ;;
(GI54EYAK)
       m_CondExec 00,EQ,GI54EYAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV244  : EDITION DES ECARTS DEPOT PAR RAYON                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GI54EYAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYAM
       ;;
(GI54EYAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A4} FIV144 ${DATA}/PTEM/GI54EYAJ.IE144AY
# ******  EDITION DES ECARTS DEPOT PAR RAYON                                   
#  IIV144   REPORT SYSOUT=(9,IIV144),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV144 ${DATA}/PXX0.FICHEML.GI54EL.BIV144GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV244 
       JUMP_LABEL=GI54EYAN
       ;;
(GI54EYAN)
       m_CondExec 04,GE,GI54EYAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV143)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GI54EYAQ PGM=IEBGENER   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYAQ
       ;;
(GI54EYAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.GI54EY.GIE143GE
       m_OutputAssign -c 9 -w IIV143 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GI54EYAR
       ;;
(GI54EYAR)
       m_CondExec 00,EQ,GI54EYAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV144)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GI54EYAT PGM=IEBGENER   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYAT
       ;;
(GI54EYAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.GI54EL.BIV144GE
       m_OutputAssign -c 9 -w IIV144 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GI54EYAU
       ;;
(GI54EYAU)
       m_CondExec 00,EQ,GI54EYAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GI54EYZA
       ;;
(GI54EYZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GI54EYZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
