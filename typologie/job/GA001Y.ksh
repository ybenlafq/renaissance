#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA001Y.ksh                       --- VERSION DU 08/10/2016 12:47
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGA001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/03 AT 11.03.46 BY BURTECA                      
#    STANDARDS: P  JOBSET: GA001Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='LYON'                                                              
# ********************************************************************         
#     LOAD DES DONNEES ARTICLES     (RTGA07 : GROUPE-FOURNISSEURS)             
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA07                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA001YA
       ;;
(GA001YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/02/03 AT 11.03.46 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GA001Y                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'LOAD T.ARTICLES'                       
# *                           APPL...: IMPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA001YAA
       ;;
(GA001YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GROUPE FOURNISSEURS                                            
#    RSGA07Y  : NAME=RSGA07Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA07Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA07Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA07                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA07RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAA_RTGA07.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAB
       ;;
(GA001YAB)
       m_CondExec 04,GE,GA001YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA14 : FAMILLES)                        
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA14                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAD
       ;;
(GA001YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES                                                   
#    RSGA14Y  : NAME=RSGA14Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA14Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA14                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA14RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAD_RTGA14.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAE
       ;;
(GA001YAE)
       m_CondExec 04,GE,GA001YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA15 : FAMILLE/TYPE DECLARAT�)          
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA15                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAG
       ;;
(GA001YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR TYPE DE DECLARATIONS                          
#    RSGA15Y  : NAME=RSGA15Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA15Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA15Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA15                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA15RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAG_RTGA15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAH
       ;;
(GA001YAH)
       m_CondExec 04,GE,GA001YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA16 : FAMILLE/GARANTIE)                
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA16                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAJ
       ;;
(GA001YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR GARANTIE-COMPLEMENTAIRES                      
#    RSGA16Y  : NAME=RSGA16Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA16Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA16Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA16                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA16RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAJ_RTGA16.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAK
       ;;
(GA001YAK)
       m_CondExec 04,GE,GA001YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA18 : FAMILLE/CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA18                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAM
       ;;
(GA001YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR CODE-DESCRIPTIFS                              
#    RSGA18Y  : NAME=RSGA18Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA18Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA18Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA18                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA18RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAM_RTGA18.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAN
       ;;
(GA001YAN)
       m_CondExec 04,GE,GA001YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA20 : CARACTERISTIC SPECIFIC)          
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA20                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAQ
       ;;
(GA001YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CARACTERISTIQUES SPECIFIQUES                               
#    RSGA20Y  : NAME=RSGA20Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA20Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA20Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA20                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA20RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAQ_RTGA20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAR
       ;;
(GA001YAR)
       m_CondExec 04,GE,GA001YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA21 : FAMILLE/RAYON )                  
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA21                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAT
       ;;
(GA001YAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLE PAR RAYON                                          
#    RSGA21Y  : NAME=RSGA21Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA21Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA21Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA21                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA21RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAT_RTGA21.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAU
       ;;
(GA001YAU)
       m_CondExec 04,GE,GA001YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA22 : COMPOSANTS/RAYON)                
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA22                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GA001YAX
       ;;
(GA001YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES COMPOSANT/RAYON                                            
#    RSGA22Y  : NAME=RSGA22Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA22Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA22Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA22                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA22RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YAX_RTGA22.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YAY
       ;;
(GA001YAY)
       m_CondExec 04,GE,GA001YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA23 : CODE MARKETING )                 
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA23                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBA
       ;;
(GA001YBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES MARKETING                                            
#    RSGA23Y  : NAME=RSGA23Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA23Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA23Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA23                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA23RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBA_RTGA23.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBB
       ;;
(GA001YBB)
       m_CondExec 04,GE,GA001YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA24 : CODE DESCRIPTIFS)                
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA24                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBD
       ;;
(GA001YBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES DESCRIPTIFS                                          
#    RSGA24R  : NAME=RSGA24Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA24Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA24R /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA24                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA24RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBD_RTGA24.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBE
       ;;
(GA001YBE)
       m_CondExec 04,GE,GA001YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA25 : ASSO MARKETING DECRIPT)          
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA25                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBG
       ;;
(GA001YBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES ASSO MARKETING-DESCRIPTIFS                                 
#    RSGA25Y  : NAME=RSGA25Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA25Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA25Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA25                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA25RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBG_RTGA25.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBH
       ;;
(GA001YBH)
       m_CondExec 04,GE,GA001YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA26 : VALEURS CODE MAKETING)           
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA26                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBJ
       ;;
(GA001YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-MARKETING                                     
#    RSGA26Y  : NAME=RSGA26Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA26Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA26Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA26                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA26RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBJ_RTGA26.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBK
       ;;
(GA001YBK)
       m_CondExec 04,GE,GA001YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA27 : VALEURS CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA27                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBM
       ;;
(GA001YBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-DESCRIPTIFS                                   
#    RSGA27Y  : NAME=RSGA27Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA27Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA27Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA27                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA27RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBM_RTGA27.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBN
       ;;
(GA001YBN)
       m_CondExec 04,GE,GA001YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA71 : TABLE DES TABLES)                
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA71                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBQ
       ;;
(GA001YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES TABLES                                                     
#    RSGA71Y  : NAME=RSGA71Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA71Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA71Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA71                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA71RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBQ_RTGA71.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBR
       ;;
(GA001YBR)
       m_CondExec 04,GE,GA001YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA72 : TABLE DES BORNES)                
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA72                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBT
       ;;
(GA001YBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES BORNES                                                     
#    RSGA72Y  : NAME=RSGA72Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA72Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA72Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA72                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA72RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBT_RTGA72.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBU
       ;;
(GA001YBU)
       m_CondExec 04,GE,GA001YBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA99 : TABLE DES MESSAGES)              
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA99                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YBX PGM=DSNUTILB   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GA001YBX
       ;;
(GA001YBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA99Y  : NAME=RSGA99Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA99Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA99Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA99                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA99RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YBX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YBX_RTGA99.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YBY
       ;;
(GA001YBY)
       m_CondExec 04,GE,GA001YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA91 : TABLE DES RUBRIQUES)             
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA91                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCA
       ;;
(GA001YCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA91Y  : NAME=RSGA91Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA91Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA91Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA91                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA91RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YCA_RTGA91.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCB
       ;;
(GA001YCB)
       m_CondExec 04,GE,GA001YCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA92 : TABLE REGROUP DES RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA92                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCD PGM=DSNUTILB   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCD
       ;;
(GA001YCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA92Y  : NAME=RSGA92Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA92Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA92Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA92                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA92RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YCD_RTGA92.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCE
       ;;
(GA001YCE)
       m_CondExec 04,GE,GA001YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA93 : TABLE TOTALISA DE RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTGA93                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCG PGM=DSNUTILB   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCG
       ;;
(GA001YCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA93Y  : NAME=RSGA93Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA93Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA93Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTGA93                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA93RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YCG_RTGA93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCH
       ;;
(GA001YCH)
       m_CondExec 04,GE,GA001YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE LA FACTURATION INTERNE  RTFI05                                   
#     NON-MODIFIABLES PAR LES FILIALES  P945.RTFI05                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCJ PGM=DSNUTILB   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCJ
       ;;
(GA001YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSFI05Y  : NAME=RSFI05Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSFI05Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFI05Y /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P945.RTFI05                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PFI0/F07.UNLOAD.FI05UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YCJ_RTFI05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCK
       ;;
(GA001YCK)
       m_CondExec 04,GE,GA001YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE RTPM06 : TABLE DES MODE DE PAIEMENT NEM                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCM
       ;;
(GA001YCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES LOIS DE REAPPRO                                            
#    RSPM06Y  : NAME=RSPM06Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSPM06Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSPM06Y /dev/null
# ******* FIC DE LOAD VENANT DE GA001P                                         
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.PM06RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001Y_GA001YCM_RTPM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCN
       ;;
(GA001YCN)
       m_CondExec 04,GE,GA001YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES MODES DE D�LIVRANCE NATIONAUX SUR FILIALE NON PR�SENTS S         
#            DIF POUR CHAQUE FAMILLE                                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCQ
       ;;
(GA001YCQ)
       m_CondExec ${EXAEB},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13Y  : NAME=RSGA13Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01Y /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCQ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCR
       ;;
(GA001YCR)
       m_CondExec 04,GE,GA001YCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   INSERT DES MODES DE D�LIVRANCE NATIONAUX DE DIF NON PR�SENTS SUR           
#   FILIALE, POUR CHAQUE FAMILLE                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001YCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GA001YCT
       ;;
(GA001YCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001YCT.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001YCU
       ;;
(GA001YCU)
       m_CondExec 04,GE,GA001YCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
