#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA770P.ksh                       --- VERSION DU 08/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGA770 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/17 AT 16.43.31 BY BURTECN                      
#    STANDARDS: P  JOBSET: GA770P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='PARIS'                                                             
# ********************************************************************         
#  BGA770 : MAJ DE LA DATE DE RECEPTION DES CODIC GROUPES DE RTGA00            
#           SI TOUS LES CODICS ELEMENTS ONT ETE DEJA RECEPTIONNÚS              
#           ET SI LE CODIC GROUPE A 1 PRIX DANS TOUTES LES ZONES DE PR         
#  REPRISE: OUI SI FIN ANORMALE                                                
#           NON SI FIN NORMALE: FAIRE 1 RECOVER DE RSGA00 A PARTIR DES         
#           FULL IMAGE COPY (UTILISER LA CHAINE CORTEX DB2FIP)                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GA770PA
       ;;
(GA770PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GA770PAA
       ;;
(GA770PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE : SOUS TABLE ZPRIX                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES LIENS ENTRE ARTICLES                                       
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE DES PRIX PAR ARTICLE                                           
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE DES ARTICLES MUTES                                             
#    RSMU15   : NAME=RSMU15,MODE=U - DYNAM=YES                                 
# -X-RSMU15   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSMU15 /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE TRAITEE : 907                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* MAJ DE LA DATE DE RECEPTION (D1RECEPT) DE RTGA00                     
#    RSGA00   : NAME=RSGA00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA770 
       JUMP_LABEL=GA770PAB
       ;;
(GA770PAB)
       m_CondExec 04,GE,GA770PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
