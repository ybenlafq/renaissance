#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BGG70P.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBGG70 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/10/30 AT 17.28.23 BY PREPA2                       
#    STANDARDS: P  JOBSET: BGG70P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGG701                                                                     
#  EXTRACTION COEF D'AJUSTEMENT                                                
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BGG70PA
       ;;
(BGG70PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=BGG70PAA
       ;;
(BGG70PAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***********************************                                          
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA35   : NAME=RSGA35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA35 /dev/null
#    RSGJ10   : NAME=RSGJ10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGJ10 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSPT03   : NAME=RSPT03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT03 /dev/null
#                                                                              
# *****   FICHIER EXTRAC COEF                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 90 FGG701 ${DATA}/PXX0.F07.BGG701AP.COEF.DIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG701 
       JUMP_LABEL=BGG70PAB
       ;;
(BGG70PAB)
       m_CondExec 04,GE,BGG70PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
