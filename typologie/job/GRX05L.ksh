#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX05L.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGRX05 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/01 AT 14.32.05 BY BURTECN                      
#    STANDARDS: P  JOBSET: GRX05L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   PROG : BRX200 CHAINE MENSUELLE QUI PERMET A LA DIRECTION DES               
#         PRODUITS ET A LA DIRECTION DES VENTES DE SUIVRE LES ELEMENTS         
#         CONCERNANT LES ALIGNEMENTS PAR CONCURENT                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX05LA
       ;;
(GRX05LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRX05LAA
       ;;
(GRX05LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* FSEMAINE A DUMMY POUR LES MENSUELS                                   
       m_FileAssign -d SHR FSEMAINE /dev/null
# ******* FMOIS                                                                
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE PARAMETRES ASSOCIES AUX FAMILLES                               
#    RSGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE STOCKS                                                         
#    RSGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******* TABLE DES CONCURENTS (REMPLACE RTGA04)                               
#    RSRX00   : NAME=RSRX00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
# ******* TABLE DES RELEVES TRAITES                                            
#    RSRX22   : NAME=RSRX22L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22 /dev/null
# ******* TABLE DES PRIX DE RELEVES                                            
#    RSRX25   : NAME=RSRX25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IRX200 ${DATA}/PTEM/GRX05LAA.MRX200AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX200 
       JUMP_LABEL=GRX05LAB
       ;;
(GRX05LAB)
       m_CondExec 04,GE,GRX05LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (MRX200AL)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAD
       ;;
(GRX05LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GRX05LAA.MRX200AL
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX05LAD.MRX200BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_12_4 12 CH 4
 /FIELDS FLD_BI_95_5 95 CH 5
 /FIELDS FLD_BI_16_5 16 CH 05
 /FIELDS FLD_BI_7_3 07 CH 3
 /FIELDS FLD_BI_26_2 26 CH 2
 /FIELDS FLD_BI_10_2 10 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_4 ASCENDING,
   FLD_BI_16_5 DESCENDING,
   FLD_BI_95_5 ASCENDING,
   FLD_BI_26_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX05LAE
       ;;
(GRX05LAE)
       m_CondExec 00,EQ,GRX05LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS MRX200BL                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAG
       ;;
(GRX05LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GRX05LAD.MRX200BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GRX05LAG.MRX200CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX05LAH
       ;;
(GRX05LAH)
       m_CondExec 04,GE,GRX05LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAJ
       ;;
(GRX05LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GRX05LAG.MRX200CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX05LAJ.MRX200DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX05LAK
       ;;
(GRX05LAK)
       m_CondExec 00,EQ,GRX05LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IRX200                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAM
       ;;
(GRX05LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GRX05LAD.MRX200BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GRX05LAJ.MRX200DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRX200 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX05LAN
       ;;
(GRX05LAN)
       m_CondExec 04,GE,GRX05LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BRX210 CHAINE MENSUELLE QUI PERMET A LA DIRECTION DES               
#         PRODUITS ET A LA DIRECTION DES VENTES DE SUIVRE LES ELEMENTS         
#         CONCERNANT LES ALIGNEMENTS PAR MAGASIN                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAQ
       ;;
(GRX05LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* FSEMAINE A DUMMY POUR LES MENSUELS                                   
       m_FileAssign -d SHR FSEMAINE /dev/null
# ******* FMOIS                                                                
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE PARAMETRES ASSOCIES AUX FAMILLES                               
#    RSGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE STOCKS                                                         
#    RSGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******* TABLE DES CONCURENTS (REMPLACE RTGA04)                               
#    RSRX00   : NAME=RSRX00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
# ******* TABLE DES RELEVES TRAITES                                            
#    RSRX22   : NAME=RSRX22L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22 /dev/null
# ******* TABLE DES PRIX DE RELEVES                                            
#    RSRX25   : NAME=RSRX25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IRX210 ${DATA}/PTEM/GRX05LAQ.MRX210AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX210 
       JUMP_LABEL=GRX05LAR
       ;;
(GRX05LAR)
       m_CondExec 04,GE,GRX05LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (MRX210AL)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAT
       ;;
(GRX05LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GRX05LAQ.MRX210AL
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX05LAT.MRX210BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_25_4 25 CH 4
 /FIELDS FLD_BI_29_8 29 CH 8
 /FIELDS FLD_BI_37_6 37 CH 6
 /FIELDS FLD_BI_43_2 43 CH 2
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_BI_149_5 149 CH 5
 /FIELDS FLD_BI_7_3 07 CH 3
 /FIELDS FLD_BI_147_2 147 CH 2
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_147_2 ASCENDING,
   FLD_BI_149_5 ASCENDING,
   FLD_BI_25_4 ASCENDING,
   FLD_BI_29_8 ASCENDING,
   FLD_BI_37_6 ASCENDING,
   FLD_BI_43_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX05LAU
       ;;
(GRX05LAU)
       m_CondExec 00,EQ,GRX05LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS MRX210BL                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LAX
       ;;
(GRX05LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/GRX05LAT.MRX210BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GRX05LAX.MRX210CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX05LAY
       ;;
(GRX05LAY)
       m_CondExec 04,GE,GRX05LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBA
       ;;
(GRX05LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GRX05LAX.MRX210CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX05LBA.MRX210DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX05LBB
       ;;
(GRX05LBB)
       m_CondExec 00,EQ,GRX05LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IRX210                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBD
       ;;
(GRX05LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GRX05LAT.MRX210BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/GRX05LBA.MRX210DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRX210 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX05LBE
       ;;
(GRX05LBE)
       m_CondExec 04,GE,GRX05LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BRX220 CHAINE MENSUELLE QUI PERMET A LA DIRECTION DES               
#         PRODUITS ET A LA DIRECTION DES VENTES DE SUIVRE LES                  
#         ELEMENTS CONCERNANT LES ALIGNEMENTS PAR CHEF PRODUIT                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBG
       ;;
(GRX05LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* FSEMAINE A DUMMY POUR LES MENSUELS                                   
       m_FileAssign -d SHR FSEMAINE /dev/null
# ******* FMOIS                                                                
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE CHEF PRODUITS                                                  
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE PARAMETRES ASSOCIES AUX FAMILLES                               
#    RSGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE STOCKS                                                         
#    RSGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******* TABLE DES CONCURENTS (REMPLACE RTGA04)                               
#    RSRX00   : NAME=RSRX00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
# ******* TABLE DES RELEVES TRAITES                                            
#    RSRX22   : NAME=RSRX22L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22 /dev/null
# ******* TABLE DES PRIX DE RELEVES                                            
#    RSRX25   : NAME=RSRX25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IRX220 ${DATA}/PTEM/GRX05LBG.MRX220AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX220 
       JUMP_LABEL=GRX05LBH
       ;;
(GRX05LBH)
       m_CondExec 04,GE,GRX05LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (MRX220AL)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBJ
       ;;
(GRX05LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GRX05LBG.MRX220AL
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX05LBJ.MRX220BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_3 07 CH 3
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_27_2 27 CH 2
 /FIELDS FLD_BI_103_5 103 CH 5
 /FIELDS FLD_BI_17_5 17 CH 5
 /FIELDS FLD_BI_12_5 12 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_5 DESCENDING,
   FLD_BI_103_5 ASCENDING,
   FLD_BI_27_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX05LBK
       ;;
(GRX05LBK)
       m_CondExec 00,EQ,GRX05LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS MRX220BL                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBM
       ;;
(GRX05LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/GRX05LBJ.MRX220BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GRX05LBM.MRX220CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX05LBN
       ;;
(GRX05LBN)
       m_CondExec 04,GE,GRX05LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBQ
       ;;
(GRX05LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GRX05LBM.MRX220CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX05LBQ.MRX220DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX05LBR
       ;;
(GRX05LBR)
       m_CondExec 00,EQ,GRX05LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IRX220                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX05LBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LBT
       ;;
(GRX05LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/GRX05LBJ.MRX220BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PTEM/GRX05LBQ.MRX220DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRX220 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX05LBU
       ;;
(GRX05LBU)
       m_CondExec 04,GE,GRX05LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRX05LZA
       ;;
(GRX05LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX05LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
