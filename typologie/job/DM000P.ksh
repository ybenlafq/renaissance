#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  DM000P.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPDM000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/05 AT 11.13.18 BY BURTEC2                      
#    STANDARDS: P  JOBSET: DM000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BDM000                                                                
#  ------------                                                                
#  EXTRACTION DES FAMILLES DACEM DE LA RTGA14                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=DM000PA
       ;;
(DM000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2015/02/05 AT 11.13.18 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: DM000P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'GESTION ARTICLES'                      
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=DM000PAA
       ;;
(DM000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA14   : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA18   : NAME=RSGA18,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA18 /dev/null
# ------  FIC D'EXTRACTION POUR LOAD DE P996.RTGA14                            
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 FDM000 ${DATA}/PTEM/DM000PAA.BDM000AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM000 
       JUMP_LABEL=DM000PAB
       ;;
(DM000PAB)
       m_CondExec 04,GE,DM000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM000AP SUR INDEX CLUSTER DE LA RTGA14 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAD
       ;;
(DM000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PROG BDM000                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/DM000PAA.BDM000AP
# ------  FIC SERVANT A LOADER LA RTGA14 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA14RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PAE
       ;;
(DM000PAE)
       m_CondExec 00,EQ,DM000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDM005                                                                
#  ------------                                                                
#  EXTRACTION DES FAMILLES DACEM DE LA TABLE RTGA18 A PARTIR DU FICHIE         
#  BDM000AP                                                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAG
       ;;
(DM000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA18   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA18 /dev/null
# ------  FIC ISSU DU BDM000                                                   
       m_FileAssign -d SHR -g ${G_A2} FDM000 ${DATA}/PTEM/DM000PAA.BDM000AP
# ------  FIC D'EXTRACTION POUR LOAD DE P996.RTGA18                            
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 FDM005 ${DATA}/PTEM/DM000PAG.BDM005AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM005 
       JUMP_LABEL=DM000PAH
       ;;
(DM000PAH)
       m_CondExec 04,GE,DM000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM005AP SUR INDEX CLUSTER DE LA RTGA18 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAJ
       ;;
(DM000PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM005                                               
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/DM000PAG.BDM005AP
# ------  FIC SERVANT A LOADER LA RTGA14 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA18RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
 /* Record Type = F  Record Length = 13 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PAK
       ;;
(DM000PAK)
       m_CondExec 00,EQ,DM000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM005AP AVEC OUTREC POUR PGM BDM010                         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAM
       ;;
(DM000PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM005                                               
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/DM000PAG.BDM005AP
# ------  FIC SERVANT A LOADER LA RTGA14 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 5 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/DM000PAM.BDM005BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_6_5 6 CH 5
 /KEYS
   FLD_CH_6_5 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 5 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_6_5
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=DM000PAN
       ;;
(DM000PAN)
       m_CondExec 00,EQ,DM000PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDM010                                                                
#  ------------                                                                
#  EXTRACTION DES CODES DESCRIPTIFS DES TABLES RTGA24-25-27 A PARTIR D         
#  FICHIER BDM005BP ISSU DU TRI PRECEDENT                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAQ
       ;;
(DM000PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA24   : NAME=RSGA24,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA24 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA27   : NAME=RSGA27,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA27 /dev/null
# ------  FIC ISSU DU TRI PRECEDENT                                            
       m_FileAssign -d SHR -g ${G_A5} FDM005 ${DATA}/PTEM/DM000PAM.BDM005BP
# ------  FIC D'EXTRACTION POUR LOAD DES TABLES DACEM RTGA24-24-27             
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -t LSEQ -g +1 FDM010 ${DATA}/PTEM/DM000PAQ.BDM010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FDM011 ${DATA}/PTEM/DM000PAQ.BDM011AP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FDM012 ${DATA}/PTEM/DM000PAQ.BDM012AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM010 
       JUMP_LABEL=DM000PAR
       ;;
(DM000PAR)
       m_CondExec 04,GE,DM000PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM010AP SUR INDEX CLUSTER DE LA RTGA24 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAT
       ;;
(DM000PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM010                                               
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/DM000PAQ.BDM010AP
# ------  FIC SERVANT A LOADER LA RTGA24 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA24RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /* Record Type = F  Record Length = 25 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PAU
       ;;
(DM000PAU)
       m_CondExec 00,EQ,DM000PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM011AP SUR INDEX CLUSTER DE LA RTGA25 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=DM000PAX
       ;;
(DM000PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM010                                               
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/DM000PAQ.BDM011AP
# ------  FIC SERVANT A LOADER LA RTGA25 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA25RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_20 1 CH 20
 /KEYS
   FLD_CH_1_20 ASCENDING
 /* Record Type = F  Record Length = 20 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PAY
       ;;
(DM000PAY)
       m_CondExec 00,EQ,DM000PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM012AP SUR INDEX CLUSTER DE LA RTGA27 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBA
       ;;
(DM000PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM010                                               
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/DM000PAQ.BDM012AP
# ------  FIC SERVANT A LOADER LA RTGA27 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA27RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 30 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PBB
       ;;
(DM000PBB)
       m_CondExec 00,EQ,DM000PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM011AP AVEC OUTREC POUR PGM BDM015                         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBD
       ;;
(DM000PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM010                                               
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/DM000PAQ.BDM011AP
# ------  FIC SERVANT A LOADER LA RTGA14 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 5 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/DM000PBD.BDM011BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 5 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_5
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=DM000PBE
       ;;
(DM000PBE)
       m_CondExec 00,EQ,DM000PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDM015                                                                
#  ------------                                                                
#  EXTRACTION DES CODES MARKETINGS DES TABLES RTGA23-26 A PARTIR DU            
#  FICHIER BDM011BP ISSU DU TRI PRECEDENT                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBG
       ;;
(DM000PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA23   : NAME=RSGA23,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA23 /dev/null
#    RSGA26   : NAME=RSGA26,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA26 /dev/null
# ------  FIC ISSU DU TRI PRECEDENT                                            
       m_FileAssign -d SHR -g ${G_A10} FDM011 ${DATA}/PTEM/DM000PBD.BDM011BP
# ------  FIC D'EXTRACTION POUR LOAD DES TABLES DACEM RTGA24-24-27             
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 FDM015 ${DATA}/PTEM/DM000PBG.BDM015AP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FDM016 ${DATA}/PTEM/DM000PBG.BDM016AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM015 
       JUMP_LABEL=DM000PBH
       ;;
(DM000PBH)
       m_CondExec 04,GE,DM000PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM015AP SUR INDEX CLUSTER DE LA RTGA23 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBJ
       ;;
(DM000PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM015                                               
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/DM000PBG.BDM015AP
# ------  FIC SERVANT A LOADER LA RTGA23 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA23RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /* Record Type = F  Record Length = 25 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PBK
       ;;
(DM000PBK)
       m_CondExec 00,EQ,DM000PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM016AP SUR INDEX CLUSTER DE LA RTGA26 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBM
       ;;
(DM000PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM015                                               
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/DM000PBG.BDM016AP
# ------  FIC SERVANT A LOADER LA RTGA26 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA26RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 30 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PBN
       ;;
(DM000PBN)
       m_CondExec 00,EQ,DM000PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDM020                                                                
#  ------------                                                                
#  EXTRACTION DES RAYONS DE LA TABLES RTGA20 POUR NE PRENDRE QUE CEUX          
#  COMPOSES DE FAMILLES DACEM                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBQ
       ;;
(DM000PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA23   : NAME=RSGA20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA23 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA30 /dev/null
# ------  FIC D'EXTRACTION POUR LOAD DE LA TABLE RTGA20                        
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -t LSEQ -g +1 FDM020 ${DATA}/PTEM/DM000PBQ.BDM020AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM020 
       JUMP_LABEL=DM000PBR
       ;;
(DM000PBR)
       m_CondExec 04,GE,DM000PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM015AP SUR INDEX CLUSTER DE LA RTGA23 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBT
       ;;
(DM000PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM020                                               
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/DM000PBQ.BDM020AP
# ------  FIC SERVANT A LOADER LA RTGA20 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA20RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
 /* Record Type = F  Record Length = 26 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PBU
       ;;
(DM000PBU)
       m_CondExec 00,EQ,DM000PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDM025                                                                
#  ------------                                                                
#  EXTRACTION DES RAYONS DACEM A PARTIR DU FICHIER ISSU DU TRI PRECEDE         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=DM000PBX
       ;;
(DM000PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA21   : NAME=RSGA21,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA21 /dev/null
# ------  FIC ISSU DU TRI PRECEDENT                                            
       m_FileAssign -d SHR -g ${G_A14} FDM020 ${DATA}/PXX0/F96.RELOAD.GA20RB
# ------  FIC D'EXTRACTION POUR LOAD DE LA TABLE RTGA21                        
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 FDM025 ${DATA}/PTEM/DM000PBX.BDM025AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM025 
       JUMP_LABEL=DM000PBY
       ;;
(DM000PBY)
       m_CondExec 04,GE,DM000PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BDM025AP SUR INDEX CLUSTER DE LA RTGA21 POUR LE LOAD         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=DM000PCA
       ;;
(DM000PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FIC ISSU DU PGM BDM025                                               
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/DM000PBX.BDM025AP
# ------  FIC SERVANT A LOADER LA RTGA21 REPRIS DANS GA001B                    
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.RELOAD.GA21RB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_8_6 8 CH 6
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_8_6 ASCENDING
 /* Record Type = F  Record Length = 14 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DM000PCB
       ;;
(DM000PCB)
       m_CondExec 00,EQ,DM000PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BDM001                                                                
#  ------------                                                                
#  EXTRACTION DES SOUS-TABLES DE L'ENVIRONNEMENT DARTY                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DM000PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=DM000PCD
       ;;
(DM000PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01B,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  FIC D'EXTRACTION POUR LOAD DE P996.RTGA14                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FDM001 ${DATA}/PXX0/F96.FDM01AB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDM001 
       JUMP_LABEL=DM000PCE
       ;;
(DM000PCE)
       m_CondExec 04,GE,DM000PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=DM000PZA
       ;;
(DM000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DM000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
