#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BPR21Y.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYBPR21 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/11/17 AT 17.20.03 BY PREPA3                       
#    STANDARDS: P  JOBSET: BPR21Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#    PGM : BPR001                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BPR21YA
       ;;
(BPR21YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BPR21YAA
       ;;
(BPR21YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER CODE SOCIETE :945                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR001 ${DATA}/PXX0/BPR21YAA.BPR001AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR002 ${DATA}/PXX0/BPR21YAA.BPR002AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR001 
       JUMP_LABEL=BPR21YAB
       ;;
(BPR21YAB)
       m_CondExec 04,GE,BPR21YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC  (BPR001AM)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAD
       ;;
(BPR21YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/BPR21YAA.BPR001AY
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/BPR21YAD.BPR001BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_19 7 CH 19
 /KEYS
   FLD_BI_7_19 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BPR21YAE
       ;;
(BPR21YAE)
       m_CondExec 00,EQ,BPR21YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BPR000CM                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAG
       ;;
(BPR21YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/BPR21YAD.BPR001BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/BPR21YAG.BPR001CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=BPR21YAH
       ;;
(BPR21YAH)
       m_CondExec 04,GE,BPR21YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAJ
       ;;
(BPR21YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/BPR21YAG.BPR001CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/BPR21YAJ.FPR001AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BPR21YAK
       ;;
(BPR21YAK)
       m_CondExec 00,EQ,BPR21YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IPR001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAM
       ;;
(BPR21YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/BPR21YAD.BPR001BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PXX0/BPR21YAJ.FPR001AY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BPR001FY
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d NEW,CATLG,DELETE -r 222 -g +1 FEG198 ${DATA}/PXX0/F45.BPR001GY
# *********************************** FICHIER D'IMPRESSION                     
# ********ETAT NON UTILISE  COMMENT LE 171198 CGA                              
       m_OutputAssign -c "*" FEDITION
# FEDITION REPORT SYSOUT=(9,IPR001),RECFM=VA                                   
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=BPR21YAN
       ;;
(BPR21YAN)
       m_CondExec 04,GE,BPR21YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC  (BPR002AR)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAQ
       ;;
(BPR21YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/BPR21YAA.BPR002AY
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/BPR21YAQ.BPR002BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_19 7 CH 19
 /KEYS
   FLD_BI_7_19 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BPR21YAR
       ;;
(BPR21YAR)
       m_CondExec 00,EQ,BPR21YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BPR002CR                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAT
       ;;
(BPR21YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PXX0/BPR21YAQ.BPR002BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/BPR21YAT.BPR002CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=BPR21YAU
       ;;
(BPR21YAU)
       m_CondExec 04,GE,BPR21YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YAX
       ;;
(BPR21YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/BPR21YAT.BPR002CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/BPR21YAX.FPR002AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BPR21YAY
       ;;
(BPR21YAY)
       m_CondExec 00,EQ,BPR21YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IPR002                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BPR21YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YBA
       ;;
(BPR21YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PXX0/BPR21YAQ.BPR002BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PXX0/BPR21YAX.FPR002AY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/BPR21YBA.BPR002FY
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d NEW,CATLG,DELETE -r 222 -g +1 FEG198 ${DATA}/PXX0/F07.BPR002GY
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPR002 FEDITION
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=BPR21YBB
       ;;
(BPR21YBB)
       m_CondExec 04,GE,BPR21YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BPR21YZA
       ;;
(BPR21YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BPR21YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
