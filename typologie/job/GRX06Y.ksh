#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX06Y.ksh                       --- VERSION DU 19/10/2016 16:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGRX06 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/22 AT 14.22.31 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GRX06Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BRX002                                                                
#  -------------                                                               
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX06YA
       ;;
(GRX06YA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GRX06YAA
       ;;
(GRX06YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  PARAMETRE DATE                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAM SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE EN ECRITURE                                                    
#    RSRX20   : NAME=RSRX20Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRX20 /dev/null
#    RSRX22   : NAME=RSRX22Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22 /dev/null
#    RSRX25   : NAME=RSRX25Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
# FICHIER EN ENTREE                                                            
       m_FileAssign -d SHR -g +0 FRX002 ${DATA}/PXX0/F45.FGRX06AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX002 
       JUMP_LABEL=GRX06YAB
       ;;
(GRX06YAB)
       m_CondExec 04,GE,GRX06YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
