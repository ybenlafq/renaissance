#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV060P.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV060 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/26 AT 10.34.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV060P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DES DONNEES PARIS TABLES GV31                                        
#  REPRISE .OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV060PA
       ;;
(NV060PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV060PAA
       ;;
(NV060PAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV060PAA.NV060UPP
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/NV060PAA.NV060UDP
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/NV060PAA.NV060ULP
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC04 ${DATA}/PTEM/NV060PAA.NV060UMP
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC05 ${DATA}/PTEM/NV060PAA.NV060UOP
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC06 ${DATA}/PTEM/NV060PAA.NV060UYP
#                                                                              
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV060PAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DES FICHIER UNLOAD . SORTIE EN 24                                      
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV060PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV060PAD
       ;;
(NV060PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV060PAA.NV060UPP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/NV060PAA.NV060UDP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/NV060PAA.NV060ULP
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/NV060PAA.NV060UMP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/NV060PAA.NV060UOP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/NV060PAA.NV060UYP
       m_FileAssign -d NEW,CATLG,DELETE -r 24 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV060PAD.NV060AAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_9 1 CH 09
 /KEYS
   FLD_CH_1_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV060PAE
       ;;
(NV060PAE)
       m_CondExec 00,EQ,NV060PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV060 :TRAITEMENT DES PRIX  ARTICLES A + 1                                 
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV060PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV060PAG
       ;;
(NV060PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} FNV060J ${DATA}/PTEM/NV060PAD.NV060AAP
#                                                                              
       m_FileAssign -d SHR -g +0 FNV060V ${DATA}/PXX0/F07.NV060BBP
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE M.A.J                                                           
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
# -X-RSNV00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV060 
       JUMP_LABEL=NV060PAH
       ;;
(NV060PAH)
       m_CondExec 04,GE,NV060PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COPY DU  FICHIER J +1 SI  LA CHAINE EST OK                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV060PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV060PAJ
       ;;
(NV060PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DE GEN -1                                                        
       m_FileAssign -d SHR -g ${G_A8} IN1 ${DATA}/PTEM/NV060PAD.NV060AAP
# ******  FIC DE REPRISE + 1                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 24 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.NV060BBP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV060PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NV060PAK
       ;;
(NV060PAK)
       m_CondExec 16,NE,NV060PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV060PZA
       ;;
(NV060PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV060PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
