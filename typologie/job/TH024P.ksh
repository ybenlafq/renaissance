#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TH024P.ksh                       --- VERSION DU 17/10/2016 18:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTH024 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/15 AT 09.58.56 BY BURTEC2                      
#    STANDARDS: P  JOBSET: TH024P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BTH020      EXTRACTION ARTICLES                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TH024PA
       ;;
(TH024PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TH024PAA
       ;;
(TH024PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH00 /dev/null
#    RSTH01   : NAME=RSTH01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH01 /dev/null
#    RSTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH12 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 FTH020 ${DATA}/PXX0/F07.BTH020BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH020 
       JUMP_LABEL=TH024PAB
       ;;
(TH024PAB)
       m_CondExec 04,GE,TH024PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTH024      VENTES HEBDO                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAD
       ;;
(TH024PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSLV05   : NAME=RSLV05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLV05 /dev/null
#    RSLV15   : NAME=RSLV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLV15 /dev/null
#    RSLW05   : NAME=RSLW05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW05 /dev/null
#    RSLW15   : NAME=RSLW15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW15 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A1} FCODIC ${DATA}/PXX0/F07.BTH020BP
#         FICHIER SORTI                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 85 -t LSEQ -g +1 FIC077 ${DATA}/PXX0/F07.FTH024AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH024 
       JUMP_LABEL=TH024PAE
       ;;
(TH024PAE)
       m_CondExec 04,GE,TH024PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTH024AP pour envois sur re7                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAG
       ;;
(TH024PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/TH024PAG.FTTH024P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTH024AP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAJ
       ;;
(TH024PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH024PAG.FTTH024P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH030      NON SERVI CLIENT                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAM
       ;;
(TH024PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSRM60   : NAME=RSRM60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM60 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A3} FCODIC ${DATA}/PXX0/F07.BTH020BP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 FIC088 ${DATA}/PXX0/F07.FTH030AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 FSORTIE ${DATA}/PXX0/F07.FTH030BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH030 
       JUMP_LABEL=TH024PAN
       ;;
(TH024PAN)
       m_CondExec 04,GE,TH024PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAQ
       ;;
(TH024PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A4} SYSOUT ${DATA}/PTEM/TH024PAG.FTTH024P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH024P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAT
       ;;
(TH024PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH024PAG.FTTH024P(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  SORT DES FICHIERS FSORTIE DU STEP PRECEDENT                                 
#  REPRISEE NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=TH024PAX
       ;;
(TH024PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER                                                                  
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F07.FTH030BP
# *** SORTIE                                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TH024PAX.FTH030CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_13_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TH024PAY
       ;;
(TH024PAY)
       m_CondExec 00,EQ,TH024PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTRIEVE                                                                  
#  REPRISEE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=TH024PBA
       ;;
(TH024PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} FILEA ${DATA}/PXX0/F07.FTH030AP
       m_FileAssign -d SHR -g ${G_A8} FILEB ${DATA}/PTEM/TH024PAX.FTH030CP
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 FILEC ${DATA}/PXX0/F07.FTH030DP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/TH024PBA
       m_ProgramExec TH024PBA
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABT      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.FTH030DP.*,+                                            
# 088_gnx_nsk_nosrvcli_mai.TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FFTH030P,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABY      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=FTH030DP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=FTH030EP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=FFTH030P,MODE=I                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PBD PGM=JVMLDM76   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=TH024PBD
       ;;
(TH024PBD)
       m_CondExec ${EXABT},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A9} DD1 ${DATA}/PXX0/F07.FTH030DP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 27998 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.FTH030EP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PBD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FFTH30BP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PBG PGM=EZACFSM1   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=TH024PBG
       ;;
(TH024PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PBG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/TH024PBG.FFTH30BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FFTH30BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PBJ PGM=FTP        ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=TH024PBJ
       ;;
(TH024PBJ)
       m_CondExec ${EXACD},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PBJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH024PBG.FFTH30BP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH034  LIENS RR SUPPRIMES                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PBM PGM=BTH034     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=TH024PBM
       ;;
(TH024PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g -1 FTH034J1 ${DATA}/PXX0/F07.BTH034AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g +0 FTH034J ${DATA}/PXX0/F07.BTH034AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 FTH034O ${DATA}/PXX0/F07.BTH034BP
#                                                                              
       m_ProgramExec BPV212
       m_UtilityExec SYSTSIN
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH024PBQ PGM=FTP        ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=TH024PBQ
       ;;
(TH024PBQ)
       m_CondExec ${EXACN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PBQ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TH024PZA
       ;;
(TH024PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH024PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
