#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BS200P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBS200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/11/07 AT 15.25.51 BY BURTECA                      
#    STANDARDS: P  JOBSET: BS200P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BBS200 : EXTRACTION DES OFFRES � TRAITER                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BS200PA
       ;;
(BS200PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BS200PAA
       ;;
(BS200PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA65 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BS200PAA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FBS200 ${DATA}/PTEM/BS200PAA.BBS200AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS200 
       JUMP_LABEL=BS200PAB
       ;;
(BS200PAB)
       m_CondExec 04,GE,BS200PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER ISSU DE LG860P                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAD
       ;;
(BS200PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG865P
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 SORTOUT ${DATA}/PTEM/BS200PAD.BBS200RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_36 "000"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_149_3 149 PD 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_137_3 137 CH 3
 /FIELDS FLD_CH_149_3 149 CH 3
 /FIELDS FLD_CH_17_6 17 CH 6
 /FIELDS FLD_PD_125_3 125 PD 3
 /FIELDS FLD_CH_173_3 173 CH 3
 /FIELDS FLD_PD_77_3 77 PD 3
 /FIELDS FLD_CH_125_3 125 CH 3
 /FIELDS FLD_CH_113_3 113 CH 3
 /FIELDS FLD_CH_89_3 89 CH 3
 /FIELDS FLD_PD_61_3 61 PD 3
 /FIELDS FLD_CH_185_3 185 CH 3
 /FIELDS FLD_CH_77_3 77 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_PD_161_3 161 PD 3
 /FIELDS FLD_CH_23_2 23 CH 2
 /FIELDS FLD_PD_89_3 89 PD 3
 /FIELDS FLD_CH_101_3 101 CH 3
 /FIELDS FLD_CH_61_3 61 CH 3
 /FIELDS FLD_CH_40_3 40 CH 3
 /FIELDS FLD_PD_185_3 185 PD 3
 /FIELDS FLD_PD_40_3 40 PD 3
 /FIELDS FLD_PD_101_3 101 PD 3
 /FIELDS FLD_CH_161_3 161 CH 3
 /FIELDS FLD_PD_113_3 113 PD 3
 /FIELDS FLD_PD_173_3 173 PD 3
 /FIELDS FLD_CH_17_3 17 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_137_3 137 PD 3
 /CONDITION CND_3 FLD_CH_4_3 EQ CST_1_36 
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_23_2 ASCENDING,
   FLD_CH_17_3 ASCENDING,
   FLD_CH_20_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_3,
    TOTAL FLD_PD_61_3,
    TOTAL FLD_PD_77_3,
    TOTAL FLD_PD_89_3,
    TOTAL FLD_PD_101_3,
    TOTAL FLD_PD_113_3,
    TOTAL FLD_PD_125_3,
    TOTAL FLD_PD_137_3,
    TOTAL FLD_PD_149_3,
    TOTAL FLD_PD_161_3,
    TOTAL FLD_PD_173_3,
    TOTAL FLD_PD_185_3
 /OMIT CND_3
 /* Record Type = F  Record Length = 051 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_1_3,FLD_CH_23_2,FLD_CH_17_6,FLD_CH_40_3,FLD_CH_61_3,FLD_CH_77_3,FLD_CH_89_3,FLD_CH_101_3,FLD_CH_113_3,FLD_CH_125_3,FLD_CH_137_3,FLD_CH_149_3,FLD_CH_161_3,FLD_CH_173_3,FLD_CH_185_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=BS200PAE
       ;;
(BS200PAE)
       m_CondExec 00,EQ,BS200PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER ISSU DE LG860P                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAG
       ;;
(BS200PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FLG866P
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 SORTOUT ${DATA}/PTEM/BS200PAG.BBS200TP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_51_1 51 CH 1
 /FIELDS FLD_CH_103_20 103 CH 20
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_70_6 70 CH 6
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /* Record Type = F  Record Length = 040 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_13,FLD_CH_51_1,FLD_CH_70_6,FLD_CH_103_20
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=BS200PAH
       ;;
(BS200PAH)
       m_CondExec 00,EQ,BS200PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS201 : DECOMPOSITION DES OFFRES                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAJ
       ;;
(BS200PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR10   : NAME=RSPR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR10 /dev/null
#    RSPR21   : NAME=RSPR21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR21 /dev/null
#    RSPR22   : NAME=RSPR22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR22 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A1} FBS200 ${DATA}/PTEM/BS200PAA.BBS200AP
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FBS201 ${DATA}/PTEM/BS200PAJ.BBS201AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS201 
       JUMP_LABEL=BS200PAK
       ;;
(BS200PAK)
       m_CondExec 04,GE,BS200PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER                                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAM
       ;;
(BS200PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/BS200PAJ.BBS201AP
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 SORTOUT ${DATA}/PTEM/BS200PAM.BBS201BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_7 8 CH 7
 /KEYS
   FLD_CH_8_7 ASCENDING
 /* Record Type = F  Record Length = 140 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PAN
       ;;
(BS200PAN)
       m_CondExec 00,EQ,BS200PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS202 : ACCES AUX DONN�ES PAR ZONES DE PRIX                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAQ
       ;;
(BS200PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGA68   : NAME=RSGA68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA68 /dev/null
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN59 /dev/null
#    RSGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN68 /dev/null
#    RSGG20   : NAME=RSGG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A3} TRI865 ${DATA}/PTEM/BS200PAD.BBS200RP
       m_FileAssign -d SHR -g ${G_A4} TRI866 ${DATA}/PTEM/BS200PAG.BBS200TP
       m_FileAssign -d SHR -g ${G_A5} FBS201 ${DATA}/PTEM/BS200PAM.BBS201BP
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 270 -g +1 FBS202 ${DATA}/PTEM/BS200PAQ.BBS202AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS202 
       JUMP_LABEL=BS200PAR
       ;;
(BS200PAR)
       m_CondExec 04,GE,BS200PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER                                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAT
       ;;
(BS200PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/BS200PAQ.BBS202AP
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 270 -g +1 SORTOUT ${DATA}/PTEM/BS200PAT.BBS202BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_7 8 CH 7
 /FIELDS FLD_CH_172_2 172 CH 2
 /FIELDS FLD_CH_117_2 117 CH 2
 /FIELDS FLD_CH_150_3 150 CH 3
 /KEYS
   FLD_CH_8_7 ASCENDING,
   FLD_CH_150_3 ASCENDING,
   FLD_CH_172_2 ASCENDING,
   FLD_CH_117_2 ASCENDING
 /* Record Type = F  Record Length = 270 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PAU
       ;;
(BS200PAU)
       m_CondExec 00,EQ,BS200PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS203 : REPORT DES DONN�ES CONSTITUANT SUR LES OFFRES                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PAX PGM=BBS203     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=BS200PAX
       ;;
(BS200PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A7} FBS202 ${DATA}/PTEM/BS200PAT.BBS202BP
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 360 -g +1 FBS203 ${DATA}/PTEM/BS200PAX.BBS203AP
       m_ProgramExec BBS203 
# ********************************************************************         
#  BBS020 : PREPARATION DU FICHIER D'EDITION IBS020                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBA
       ;;
(BS200PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BS200PBA
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A8} FBS203 ${DATA}/PTEM/BS200PAX.BBS203AP
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBS020 ${DATA}/PTEM/BS200PBA.BBS020AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS020 
       JUMP_LABEL=BS200PBB
       ;;
(BS200PBB)
       m_CondExec 04,GE,BS200PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBD
       ;;
(BS200PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/BS200PBA.BBS020AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/BS200PBD.BBS020BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_20 7 CH 20
 /FIELDS FLD_BI_36_20 36 CH 20
 /FIELDS FLD_BI_34_2 34 CH 2
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_32_2 32 CH 2
 /KEYS
   FLD_BI_7_20 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_32_2 ASCENDING,
   FLD_BI_34_2 ASCENDING,
   FLD_BI_36_20 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PBE
       ;;
(BS200PBE)
       m_CondExec 00,EQ,BS200PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBG
       ;;
(BS200PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/BS200PBD.BBS020BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/BS200PBG.BBS020CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=BS200PBH
       ;;
(BS200PBH)
       m_CondExec 04,GE,BS200PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBJ
       ;;
(BS200PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/BS200PBG.BBS020CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/BS200PBJ.BBS020DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PBK
       ;;
(BS200PBK)
       m_CondExec 00,EQ,BS200PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBS030                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBM
       ;;
(BS200PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/BS200PBD.BBS020BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FCUMULS ${DATA}/PTEM/BS200PBJ.BBS020DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBS020 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=BS200PBN
       ;;
(BS200PBN)
       m_CondExec 04,GE,BS200PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER                                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBQ
       ;;
(BS200PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/BS200PAX.BBS203AP
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 360 -g +1 SORTOUT ${DATA}/PTEM/BS200PBQ.BBS203BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_150_3 150 CH 3
 /FIELDS FLD_BI_117_2 117 CH 2
 /FIELDS FLD_BI_172_2 172 CH 2
 /FIELDS FLD_BI_259_7 259 CH 7
 /FIELDS FLD_BI_8_7 8 CH 7
 /FIELDS FLD_BI_77_2 77 CH 2
 /KEYS
   FLD_BI_259_7 ASCENDING,
   FLD_BI_77_2 ASCENDING,
   FLD_BI_8_7 ASCENDING,
   FLD_BI_150_3 ASCENDING,
   FLD_BI_172_2 ASCENDING,
   FLD_BI_117_2 ASCENDING
 /* Record Type = F  Record Length = 360 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PBR
       ;;
(BS200PBR)
       m_CondExec 00,EQ,BS200PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGSM01 : EXTRACTION POUR L EDITION MIX GSM                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBT
       ;;
(BS200PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BS200PBT
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A15} FBS203 ${DATA}/PTEM/BS200PBQ.BBS203BP
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGSM01 ${DATA}/PTEM/BS200PBT.BGSM01AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGSM01 
       JUMP_LABEL=BS200PBU
       ;;
(BS200PBU)
       m_CondExec 04,GE,BS200PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=BS200PBX
       ;;
(BS200PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/BS200PBT.BGSM01AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/BS200PBX.BGSM01BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_17_5 17 CH 5
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_BI_12_5 12 CH 5
 /FIELDS FLD_BI_22_20 22 CH 20
 /FIELDS FLD_BI_42_7 42 CH 7
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_5 ASCENDING,
   FLD_BI_22_20 ASCENDING,
   FLD_BI_42_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PBY
       ;;
(BS200PBY)
       m_CondExec 00,EQ,BS200PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=BS200PCA
       ;;
(BS200PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/BS200PBX.BGSM01BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/BS200PCA.BGSM01CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=BS200PCB
       ;;
(BS200PCB)
       m_CondExec 04,GE,BS200PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=BS200PCD
       ;;
(BS200PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/BS200PCA.BGSM01CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/BS200PCD.BGSM01DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS200PCE
       ;;
(BS200PCE)
       m_CondExec 00,EQ,BS200PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBS030                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS200PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=BS200PCG
       ;;
(BS200PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/BS200PBX.BGSM01BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A20} FCUMULS ${DATA}/PTEM/BS200PCD.BGSM01DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGSM01 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=BS200PCH
       ;;
(BS200PCH)
       m_CondExec 04,GE,BS200PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BS200PZA
       ;;
(BS200PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS200PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
