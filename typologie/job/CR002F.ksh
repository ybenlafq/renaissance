#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CR002F.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFCR002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.27.33 BY BURTEC6                      
#    STANDARDS: P  JOBSET: CR002F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT DIF                            
#   REPRISE:OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CR002FA
       ;;
(CR002FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CR002FAA
       ;;
(CR002FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAA.BCR002AP
# ***                                                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAB
       ;;
(CR002FAB)
       m_CondExec 04,GE,CR002FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT DPM                            
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAD
       ;;
(CR002FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAD.BCR002AD
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAE
       ;;
(CR002FAE)
       m_CondExec 04,GE,CR002FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT DNN                            
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAG
       ;;
(CR002FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAG.BCR002AL
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAH
       ;;
(CR002FAH)
       m_CondExec 04,GE,CR002FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT DAL                            
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAJ
       ;;
(CR002FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAJ.BCR002AM
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAK
       ;;
(CR002FAK)
       m_CondExec 04,GE,CR002FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT MGIO                           
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAM
       ;;
(CR002FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAM.BCR002AO
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAN
       ;;
(CR002FAN)
       m_CondExec 04,GE,CR002FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT DLUX                           
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAQ
       ;;
(CR002FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAQ.BCR002AX
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAR
       ;;
(CR002FAR)
       m_CondExec 04,GE,CR002FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BCR002 : LISTE DES MODES DE PAIEMENT CREDIT DRA                            
#   REPRISE:OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAT
       ;;
(CR002FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES GENERALITES ARTICLE                               
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE                                                                
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE                                                                
#    RTGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE                                                                
#    RTGV14   : NAME=RSGV14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  TABLE                                                                
#    RTPR00   : NAME=RSPR00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER DES MODES DE PAIEMENT A ENVOYER VIA LA GATEWAY               
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FCR002 ${DATA}/PTEM/CR002FAT.BCR002AY
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCR002 
       JUMP_LABEL=CR002FAU
       ;;
(CR002FAU)
       m_CondExec 04,GE,CR002FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FUSION DES FICHIERS DES MODES DE PAIEMENT CREDIT TOUTES FILIALES            
#  AVANT TRANSFERT VIA LA GATEWAY                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=CR002FAX
       ;;
(CR002FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER DES MODES DE PAIEMENT CREDIT                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/CR002FAA.BCR002AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/CR002FAD.BCR002AD
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/CR002FAG.BCR002AL
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/CR002FAJ.BCR002AM
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/CR002FAM.BCR002AO
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/CR002FAQ.BCR002AX
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/CR002FAT.BCR002AY
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FCR002AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CR002FAY
       ;;
(CR002FAY)
       m_CondExec 00,EQ,CR002FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRANSFERT VERS (GATEWAY) XFBPRO FICHIER FCR002AP                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABO      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FCR002AP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCR002F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CR002FBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=CR002FBA
       ;;
(CR002FBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CR002FBA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP CR002FBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=CR002FBD
       ;;
(CR002FBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CR002FBD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                              
# SUCCESSEUR A VERIFIER MODIF $CR002P PAR $CR002F                              
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CR002FZA
       ;;
(CR002FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CR002FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
