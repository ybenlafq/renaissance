#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX55M.ksh                       --- VERSION DU 08/10/2016 17:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGRX55 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/07 AT 12.01.36 BY BURTECR                      
#    STANDARDS: P  JOBSET: GRX55M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   PROG : BRX055  MISE A JOUR DE LA TABLE RTRX55                              
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX55MA
       ;;
(GRX55MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAJ=${EXAAJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRX55MAA
       ;;
(GRX55MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******* FSEMAINE                                                             
       m_FileAssign -d SHR FSEMAINE ${DATA}/CORTEX4.P.MTXTFIX1/GRX55MAA
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX00   : NAME=RSRX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX20   : NAME=RSRX20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX20 /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX21   : NAME=RSRX21M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX21 /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX22   : NAME=RSRX22M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22 /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX25   : NAME=RSRX25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
# ******* UPDATE DE LA RTRX55 (TABLE RELEVE DE PRIX)                           
#    RSRX55   : NAME=RSRX55M,MODE=U - DYNAM=YES                                
# -X-RSRX55M  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSRX55 /dev/null
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -g +1 RTRX55 ${DATA}/PXX0/GRX55MAA.BRX055AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX055 
       JUMP_LABEL=GRX55MAB
       ;;
(GRX55MAB)
       m_CondExec 04,GE,GRX55MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SUR L'INDEX                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX55MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRX55MAD
       ;;
(GRX55MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GRX55MAA.BRX055AM
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -g +1 SORTOUT ${DATA}/PXX0/GRX55MAD.BRX055BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_4 8 CH 4
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_12_8 12 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_4 ASCENDING,
   FLD_CH_12_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX55MAE
       ;;
(GRX55MAE)
       m_CondExec 00,EQ,GRX55MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD/REPLACE DE LA TABLE RTRX55                                             
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX55MAG PGM=DSNUTILB   ** ID=AAJ                                   
# ***********************************                                          
       JUMP_LABEL=GRX55MAG
       ;;
(GRX55MAG)
       m_CondExec ${EXAAJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTRX55                                                
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/GRX55MAD.BRX055BM
#    RTRX55   : NAME=RSRX55M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTRX55 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX55MAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GRX55M_GRX55MAG_RTRX55.sysload
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GRX55MAH
       ;;
(GRX55MAH)
       m_CondExec 04,GE,GRX55MAG ${EXAAJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRX55MZA
       ;;
(GRX55MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX55MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
