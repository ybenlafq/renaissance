#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GG300P.ksh                       --- VERSION DU 09/10/2016 05:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGG300 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/27 AT 10.28.06 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GG300P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#                                                                              
#   RECUP DES PARAMETRES DE SAISIE PARAMETRE EDITION TP                        
#   REPRISE : OUI MAIS ATTENTION : MAJ DE LA MTXTFIX5 MEMBRE(BGG300)           
#                                                                              
# ********************************************************************         
# AAA      STEP  PGM=EX0100,LANG=ASM                                           
#                                                                              
# //TABLE    DD DSN=CORTEX4.P.MTXTFIX5(BGG300),DISP=SHR                        
# EX0100   FILE  NAME=EX0000AP,MODE=U                                          
# //PARAM    DD DSN=CORTEX4.P.MTXTFIX5,DISP=SHR                                
# SYSPRINT REPORT SYSOUT=X                                                     
# SNAPOUT  REPORT SYSOUT=X                                                     
# SYSIN    DATA  PARMS=EX0100P1,CLASS=VAR                                      
# ********************************************************************         
#  BGG300 : EXTRACTION DES DEMAMDES D'EDITION DES ECARTS PRMP PRA              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GG300PA
       ;;
(GG300PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GG300PAA
       ;;
(GG300PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE GENERALISEE : SOUS TABLE ECPRA (ECART DES PRA)                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  HISTORIQUES,TABLE DES MVTS DE RECEPTION                              
#    RSGG70   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG70 /dev/null
# ******  STOCKS ENTREPOT                                                      
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  STOCKS MAGASINS                                                      
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
# -X-RSAN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 907                                              
       m_FileAssign -i FNSOC
$DIFDEP_1_3
_end
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE DU JOUR D'EDITION : LE 25 DU MOIS                          
       m_FileAssign -d SHR FPARAM1 ${DATA}/CORTEX4.P.MTXTFIX1/GG300PAA
# ******  PARAMETRE DE TRAITEMENT : OUI OU NON DE EX0100                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/BGG300AP
#                                                                              
# ******  FICHIER D'EXTRACTION DES DEMANDES D'EDITION                          
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 FGG300 ${DATA}/PXX0/GG300PAA.BGG300AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG300 
       JUMP_LABEL=GG300PAB
       ;;
(GG300PAB)
       m_CondExec 04,GE,GG300PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION BGG300AP                                        
#           1,3,PD,A : N� DE SEQUENCE FAMILLE                                  
#           4,12,CH,A: MARQUE CODIC                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG300PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GG300PAD
       ;;
(GG300PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GG300PAA.BGG300AP
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PXX0/GG300PAD.BGG300BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_1_3 1 PD 3
 /FIELDS FLD_CH_4_12 4 CH 12
 /KEYS
   FLD_PD_1_3 ASCENDING,
   FLD_CH_4_12 ASCENDING
 /* Record Type = F  Record Length = 75 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GG300PAE
       ;;
(GG300PAE)
       m_CondExec 00,EQ,GG300PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGG310 : EDITION DES ECARTS ENTRE PRMP PRA PAR CODIC                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG300PAG PGM=BGG310     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GG300PAG
       ;;
(GG300PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION TRI�                                            
       m_FileAssign -d SHR -g ${G_A2} FGG300 ${DATA}/PXX0/GG300PAD.BGG300BP
#                                                                              
# ******  EDITION DES ECARTS ENTRE PRMP ET PRA PAR CODIC ET FAMILLE            
       m_OutputAssign -c 9 -w JGG310 JGG310
       m_ProgramExec BGG310 
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GG300PZA
       ;;
(GG300PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GG300PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
