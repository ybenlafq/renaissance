#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PSE02D.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPSE02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/08/27 AT 16.07.42 BY PREPA2                       
#    STANDARDS: P  JOBSET: PSE02D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPS030                                                                
#  ------------                                                                
#  VENTILATION DU CHIFFRE D'AFFAIRE                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PSE02DA
       ;;
(PSE02DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+2'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PSE02DAA
       ;;
(PSE02DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA41   : NAME=RSGA41D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
#    RSPS01   : NAME=RSPS01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS01 /dev/null
# ------  DATE,MOIS ET SOC                                                     
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------- FICHIER POUR GEN ETAT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS030 ${DATA}/PTEM/PSE02DAA.FIPS30AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS030 
       JUMP_LABEL=PSE02DAB
       ;;
(PSE02DAB)
       m_CondExec 04,GE,PSE02DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIPS030                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAD
       ;;
(PSE02DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PSE02DAA.FIPS30AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DAD.FIPS30BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_77_12 77 CH 12
 /FIELDS FLD_PD_50_6 50 PD 6
 /FIELDS FLD_BI_1_42 1 CH 42
 /FIELDS FLD_PD_62_7 62 PD 7
 /FIELDS FLD_BI_69_8 69 CH 8
 /FIELDS FLD_PD_56_6 56 PD 6
 /FIELDS FLD_PD_43_7 43 PD 7
 /KEYS
   FLD_BI_69_8 ASCENDING,
   FLD_BI_1_42 ASCENDING,
   FLD_BI_77_12 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_43_7,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_6,
    TOTAL FLD_PD_62_7
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DAE
       ;;
(PSE02DAE)
       m_CondExec 00,EQ,PSE02DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAG
       ;;
(PSE02DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/PSE02DAD.FIPS30BD
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PSE02DAG.FIPS30CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PSE02DAH
       ;;
(PSE02DAH)
       m_CondExec 04,GE,PSE02DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAJ
       ;;
(PSE02DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PSE02DAG.FIPS30CD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DAJ.FIPS30DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DAK
       ;;
(PSE02DAK)
       m_CondExec 00,EQ,PSE02DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION ETAT IPS030                                                        
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAM
       ;;
(PSE02DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/PSE02DAD.FIPS30BD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/PSE02DAJ.FIPS30DD
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT IPS030                                                          
       m_OutputAssign -c 9 -w IPS030 FEDITION
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PSE02DAN
       ;;
(PSE02DAN)
       m_CondExec 04,GE,PSE02DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPS600AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAQ
       ;;
(PSE02DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FPS600AD
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/PSE02DAQ.FPS600CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_112_8 112 PD 8
 /FIELDS FLD_PD_96_8 96 PD 8
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_PD_104_8 104 PD 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_96_8,
    TOTAL FLD_PD_104_8,
    TOTAL FLD_PD_112_8
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DAR
       ;;
(PSE02DAR)
       m_CondExec 00,EQ,PSE02DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPS040                                                                
#  ------------                                                                
#  CE PGM EXTRAIT A PARTIR DU FICHIER FPS600                                   
#  TRIE ET SOMME ET DE LA TABLE RTPS01 LE FIC ECHEANCIER A EDITER              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAT
       ;;
(PSE02DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE EN LECTURE                                                     
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPS01   : NAME=RSPS01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS01 /dev/null
# ------  DATE                                                                 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
# ------  FICHIER POUR GEN ETAT                                                
       m_FileAssign -d SHR -g ${G_A6} FPS600 ${DATA}/PTEM/PSE02DAQ.FPS600CD
# ------  FICHIER POUR GEN ETAT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS040 ${DATA}/PTEM/PSE02DAT.FIPS40AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS040 
       JUMP_LABEL=PSE02DAU
       ;;
(PSE02DAU)
       m_CondExec 04,GE,PSE02DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIPS040                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DAX
       ;;
(PSE02DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PSE02DAT.FIPS40AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DAX.FIPS40BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_69_8 69 CH 8
 /FIELDS FLD_BI_12_2 12 CH 2
 /FIELDS FLD_BI_7_5 7 CH 5
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_2 ASCENDING,
   FLD_CH_69_8 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DAY
       ;;
(PSE02DAY)
       m_CondExec 00,EQ,PSE02DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBA
       ;;
(PSE02DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/PSE02DAX.FIPS40BD
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PSE02DBA.FIPS40CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PSE02DBB
       ;;
(PSE02DBB)
       m_CondExec 04,GE,PSE02DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBD
       ;;
(PSE02DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/PSE02DBA.FIPS40CD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DBD.FIPS40DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DBE
       ;;
(PSE02DBE)
       m_CondExec 00,EQ,PSE02DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION ETAT IPS040                                                        
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBG
       ;;
(PSE02DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/PSE02DAX.FIPS40BD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/PSE02DBD.FIPS40DD
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT IPS040                                                          
       m_OutputAssign -c 9 -w IPS040 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PSE02DBH
       ;;
(PSE02DBH)
       m_CondExec 04,GE,PSE02DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPS600AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBJ
       ;;
(PSE02DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FPS600                                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FPS600AD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/PSE02DBJ.FPS600DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_9 1 CH 9
 /FIELDS FLD_PD_112_8 112 PD 8
 /FIELDS FLD_CH_28_6 28 CH 6
 /FIELDS FLD_PD_68_4 68 PD 4
 /FIELDS FLD_PD_52_8 52 PD 8
 /FIELDS FLD_CH_15_8 15 CH 8
 /FIELDS FLD_PD_72_8 72 PD 8
 /FIELDS FLD_PD_80_8 80 PD 8
 /FIELDS FLD_PD_96_8 96 PD 8
 /FIELDS FLD_PD_120_8 120 PD 8
 /FIELDS FLD_PD_60_8 60 PD 8
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_104_8 104 PD 8
 /FIELDS FLD_PD_44_8 44 PD 8
 /FIELDS FLD_PD_88_8 88 PD 8
 /KEYS
   FLD_CH_1_9 ASCENDING,
   FLD_CH_15_8 ASCENDING,
   FLD_CH_28_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_8,
    TOTAL FLD_PD_52_8,
    TOTAL FLD_PD_60_8,
    TOTAL FLD_PD_68_4,
    TOTAL FLD_PD_72_8,
    TOTAL FLD_PD_80_8,
    TOTAL FLD_PD_88_8,
    TOTAL FLD_PD_96_8,
    TOTAL FLD_PD_104_8,
    TOTAL FLD_PD_112_8,
    TOTAL FLD_PD_120_8
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DBK
       ;;
(PSE02DBK)
       m_CondExec 00,EQ,PSE02DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPS050                                                                
#  ------------                                                                
#  HISTORIQUE DES CONTRATS PAR SAV                                             
#  LE PGM EXTRAIT A PARTIR DU FICHIER FPS600 LE FICHIER HISTORIQUE A           
#  EDITER                                                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBM
       ;;
(PSE02DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ------  FIC FPS600 TRIE                                                      
       m_FileAssign -d SHR -g ${G_A12} FPS600 ${DATA}/PTEM/PSE02DBJ.FPS600DD
# ------  PARAMETRES                                                           
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FIC FIPS050                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS050 ${DATA}/PTEM/PSE02DBM.FIPS50AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS050 
       JUMP_LABEL=PSE02DBN
       ;;
(PSE02DBN)
       m_CondExec 04,GE,PSE02DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIPS050                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBQ
       ;;
(PSE02DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/PSE02DBM.FIPS50AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DBQ.FIPS50BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_66_2 66 CH 2
 /FIELDS FLD_CH_63_2 63 CH 2
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_12_5 12 CH 5
 /KEYS
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_63_2 ASCENDING,
   FLD_CH_66_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DBR
       ;;
(PSE02DBR)
       m_CondExec 00,EQ,PSE02DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBT
       ;;
(PSE02DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/PSE02DBQ.FIPS50BD
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PSE02DBT.FIPS50CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PSE02DBU
       ;;
(PSE02DBU)
       m_CondExec 04,GE,PSE02DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DBX
       ;;
(PSE02DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/PSE02DBT.FIPS50CD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DBX.FIPS50DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DBY
       ;;
(PSE02DBY)
       m_CondExec 00,EQ,PSE02DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION ETAT IPS050                                                        
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCA
       ;;
(PSE02DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/PSE02DBQ.FIPS50BD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/PSE02DBX.FIPS50DD
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT IPS050                                                          
       m_OutputAssign -c 9 -w IPS050 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PSE02DCB
       ;;
(PSE02DCB)
       m_CondExec 04,GE,PSE02DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPS600AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCD
       ;;
(PSE02DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FPS600AD
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/PSE02DCD.FPS600ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_96_8 96 PD 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_10_5 10 CH 5
 /FIELDS FLD_CH_15_5 15 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_15_5 ASCENDING,
   FLD_CH_10_5 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_96_8
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DCE
       ;;
(PSE02DCE)
       m_CondExec 00,EQ,PSE02DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FIPS90AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCG
       ;;
(PSE02DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FIPS90AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DCG.FIPS90ED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_80_5 80 PD 5
 /FIELDS FLD_CH_22_20 22 CH 20
 /FIELDS FLD_PD_70_5 70 PD 5
 /KEYS
   FLD_CH_7_5 ASCENDING,
   FLD_CH_22_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_70_5,
    TOTAL FLD_PD_80_5
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DCH
       ;;
(PSE02DCH)
       m_CondExec 00,EQ,PSE02DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPS060                                                                
#  ------------                                                                
#  SOLDE A COURS ET LONG TERME                                                 
#  LE PGM EXTRAIT A PARTIR DU FICHIER FPS600 LE FICHIER DES SOLDES             
#  DU LONG TERME ET DU COURT TERME _A EDITER                                    
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCJ
       ;;
(PSE02DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------- TABLE GENERALISEE VUE RVGA01NV                                       
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ------- TABLE PARAMETRES ASSOCIES AU CONTRAT                                 
#    RSGA41   : NAME=RSGA41D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ------- TABLE ECHEANCIER SAV 1                                               
#    RSPS01   : NAME=RSPS01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS01 /dev/null
# ------- FIC TRIE                                                             
       m_FileAssign -d SHR -g ${G_A18} FPS600 ${DATA}/PTEM/PSE02DCD.FPS600ED
# ------- FIC FIPS90 TRIE                                                      
       m_FileAssign -d SHR -g ${G_A19} FPS090 ${DATA}/PTEM/PSE02DCG.FIPS90ED
# ------  FMOIS                                                                
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER POUR GEN ETAT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS060 ${DATA}/PTEM/PSE02DCJ.FIPS60AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS060 
       JUMP_LABEL=PSE02DCK
       ;;
(PSE02DCK)
       m_CondExec 04,GE,PSE02DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIPS060                                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCM
       ;;
(PSE02DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/PSE02DCJ.FIPS60AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DCM.FIPS60BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_7_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DCN
       ;;
(PSE02DCN)
       m_CondExec 00,EQ,PSE02DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCQ
       ;;
(PSE02DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/PSE02DCM.FIPS60BD
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PSE02DCQ.FIPS60CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PSE02DCR
       ;;
(PSE02DCR)
       m_CondExec 04,GE,PSE02DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCT
       ;;
(PSE02DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/PSE02DCQ.FIPS60CD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DCT.FIPS60DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DCU
       ;;
(PSE02DCU)
       m_CondExec 00,EQ,PSE02DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION ETAT IPS060                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DCX
       ;;
(PSE02DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PTEM/PSE02DCM.FIPS60BD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A24} FCUMULS ${DATA}/PTEM/PSE02DCT.FIPS60DD
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT IPS060                                                          
       m_OutputAssign -c 9 -w IPS060 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PSE02DCY
       ;;
(PSE02DCY)
       m_CondExec 04,GE,PSE02DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPS600AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDA
       ;;
(PSE02DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/PSE02DBJ.FPS600DD
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g ${G_A26} SORTOUT ${DATA}/PTEM/PSE02DBJ.FPS600DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_96_8 96 PD 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_96_8
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DDB
       ;;
(PSE02DDB)
       m_CondExec 00,EQ,PSE02DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPS100                                                                
#  ------------                                                                
#  STATISTIQUE DE RACHAT DE GARANTIE                                           
#  LE PGM EFFECTUE L'EXTRACTION DU FIC DESTINE A L'ETAT DE STAT                
#  DE RACHAT DE GARANTIE COMPLEMENTAIRE                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDD
       ;;
(PSE02DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSPS00   : NAME=RSPS00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00 /dev/null
#    RSPS01   : NAME=RSPS01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS01 /dev/null
#    RSGA41   : NAME=RSGA41D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  FICHIER POUR GEN ETAT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS100 ${DATA}/PTEM/PSE02DDD.FIP100AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS100 
       JUMP_LABEL=PSE02DDE
       ;;
(PSE02DDE)
       m_CondExec 04,GE,PSE02DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIPS100                                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDG
       ;;
(PSE02DDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PTEM/PSE02DDD.FIP100AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DDG.FIP100BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_5 7 CH 5
 /KEYS
   FLD_BI_7_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DDH
       ;;
(PSE02DDH)
       m_CondExec 00,EQ,PSE02DDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDJ
       ;;
(PSE02DDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A28} FEXTRAC ${DATA}/PTEM/PSE02DDG.FIP100BD
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PSE02DDJ.FIP100CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PSE02DDK
       ;;
(PSE02DDK)
       m_CondExec 04,GE,PSE02DDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDM
       ;;
(PSE02DDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/PSE02DDJ.FIP100CD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DDM.FIP100DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DDN
       ;;
(PSE02DDN)
       m_CondExec 00,EQ,PSE02DDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION ETAT IPS100                                                        
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDQ
       ;;
(PSE02DDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A30} FEXTRAC ${DATA}/PTEM/PSE02DDG.FIP100BD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A31} FCUMULS ${DATA}/PTEM/PSE02DDM.FIP100DD
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT IPS100                                                          
       m_OutputAssign -c 9 -w IPS100 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PSE02DDR
       ;;
(PSE02DDR)
       m_CondExec 04,GE,PSE02DDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPS600AD                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDT
       ;;
(PSE02DDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FPS600AD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/PSE02DDT.FPS600GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_23_5 23 CH 5
 /FIELDS FLD_CH_7_13 7 CH 13
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_96_8 96 PD 8
 /KEYS
   FLD_CH_7_13 ASCENDING,
   FLD_CH_23_5 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_96_8
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DDU
       ;;
(PSE02DDU)
       m_CondExec 00,EQ,PSE02DDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROG : BPS110                                                               
#  -------------                                                               
#  CONSOLIDATION DES PARCS ET DES PRODUITS PAR SAV/AC                          
#  LE PGM EXTRAIT LE MONTANT DE LA FACTURATION PAR SAV CHAQUE MOIS             
#  DE L'EXERCICE EN COURS PAR TYPE DE SECTEUR AINSI QUE LE VOLUME              
#  DE GARANTIES AYANT EFFET DANS LA FACTURATION                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DDX
       ;;
(PSE02DDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# SCTLTBS FILE  DYNAM=YES,NAME=RSCTLTBS,MODE=I                                 
# ------  FIC FPS600 TRIE                                                      
       m_FileAssign -d SHR -g ${G_A32} FPS600 ${DATA}/PTEM/PSE02DDT.FPS600GD
# ------  PARAMETRE                                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER POUR GEN ETAT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS110 ${DATA}/PTEM/PSE02DDX.FIP110AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS110 
       JUMP_LABEL=PSE02DDY
       ;;
(PSE02DDY)
       m_CondExec 04,GE,PSE02DDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIPS110                                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DEA
       ;;
(PSE02DEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/PSE02DDX.FIP110AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DEA.FIP110BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_5 8 CH 5
 /FIELDS FLD_BI_13_5 13 CH 5
 /FIELDS FLD_BI_7_1 7 CH 1
 /FIELDS FLD_BI_18_2 18 CH 2
 /KEYS
   FLD_BI_7_1 ASCENDING,
   FLD_BI_8_5 ASCENDING,
   FLD_BI_13_5 ASCENDING,
   FLD_BI_18_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DEB
       ;;
(PSE02DEB)
       m_CondExec 00,EQ,PSE02DEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DED
       ;;
(PSE02DED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A34} FEXTRAC ${DATA}/PTEM/PSE02DEA.FIP110BD
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PSE02DED.FIP110CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PSE02DEE
       ;;
(PSE02DEE)
       m_CondExec 04,GE,PSE02DED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DEG
       ;;
(PSE02DEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/PSE02DED.FIP110CD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PSE02DEG.FIP110DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE02DEH
       ;;
(PSE02DEH)
       m_CondExec 00,EQ,PSE02DEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION ETAT IPS110                                                        
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE02DEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DEJ
       ;;
(PSE02DEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A36} FEXTRAC ${DATA}/PTEM/PSE02DEA.FIP110BD
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A37} FCUMULS ${DATA}/PTEM/PSE02DEG.FIP110DD
# ------  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ------  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ------  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER S/36 (LRECL 156) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG132 /dev/null
# ------  FICHIER S/36 (LRECL 222) EN DUMMY POUR DPM                           
       m_FileAssign -d SHR FEG198 /dev/null
# ------  ETAT IPS110                                                          
       m_OutputAssign -c 9 -w IPS110 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PSE02DEK
       ;;
(PSE02DEK)
       m_CondExec 04,GE,PSE02DEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PSE02DZA
       ;;
(PSE02DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE02DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
