#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV380P.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV380 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/01/23 AT 12.15.17 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV380P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  ATTENTION : CETTE CHAINE NE DOIT PAS TOURNE AVEC CICS                       
# ********************************************************************         
# ********************************************************************         
#  UNLOAD DES DONNEES PARIS TABLES GA69 GA00 GA01                              
#  REPRISE .OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV380PA
       ;;
(NV380PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'0'}
       RUN=${RUN}
       JUMP_LABEL=NV380PAA
       ;;
(NV380PAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV380PAA.NV380UDP
#                                                                              
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV380PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  SORT DU FICHIER UNLOAD .                                                    
#  ATTENTION LE FICHIER EN SORTIE SERA EN ENTREE DES PGM BNV380 + NV38         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAD
       ;;
(NV380PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV380PAA.NV380UDP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGP/F07.NV380DDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 07
 /FIELDS FLD_CH_8_3 08 CH 03
 /FIELDS FLD_CH_11_1 11 CH 01
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_11_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV380PAE
       ;;
(NV380PAE)
       m_CondExec 00,EQ,NV380PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV380 :TRAITEMENT DES PRIX  ARTICLES A + 1                                 
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAG
       ;;
(NV380PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FTGA69 ${DATA}/PNCGP/F07.NV380DDP
#                                                                              
# ****** FICHIER EN SORTIES                                                    
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 FNV380 ${DATA}/PTEM/NV380PAG.NV380AAP
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE M.A.J                                                           
#    RSNV00   : NAME=RSNV00,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV380 
       JUMP_LABEL=NV380PAH
       ;;
(NV380PAH)
       m_CondExec 04,GE,NV380PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV381 :TRAITEMENT DES PRIX  PRESTATIONS EN COURS                           
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAJ
       ;;
(NV380PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN SORTIE                                                     
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 FNV381 ${DATA}/PTEM/NV380PAJ.NV381BBP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 FNV381CC ${DATA}/PTEM/NV380PAJ.NV0381DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV381 
       JUMP_LABEL=NV380PAK
       ;;
(NV380PAK)
       m_CondExec 04,GE,NV380PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DES RTGA00  .LRECL 18 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAM
       ;;
(NV380PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NV380PAG.NV380AAP
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/NV380PAJ.NV381BBP
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV380PAM.NV381CCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV380PAN
       ;;
(NV380PAN)
       m_CondExec 00,EQ,NV380PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES PARIS ET FILIALES DE LA TABLE RTNV18                     
#  REPRISE .OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAQ
       ;;
(NV380PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
#    RSNV18   : NAME=RSNV18,MODE=I - DYNAM=YES                                 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV380PAQ.NV381UUP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV380PAQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BNV382 :COMPARAISON SITUATION NCG J +1 /SITUATION WCS A J                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAT
       ;;
(NV380PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# ****** TABLE                                                                 
#    RSNV18   : NAME=RSNV18,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV18 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
#                                                                              
       m_FileAssign -d SHR -g ${G_A5} FNV382N ${DATA}/PTEM/NV380PAM.NV381CCP
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FNV382W ${DATA}/PTEM/NV380PAQ.NV381UUP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV382 
       JUMP_LABEL=NV380PAU
       ;;
(NV380PAU)
       m_CondExec 04,GE,NV380PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER NV0381DP                                                    
#  REPRISE : NON EN DEBUT DE CHAINE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NV380PAX
       ;;
(NV380PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/NV380PAJ.NV0381DP
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.NV0381EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV380PAY
       ;;
(NV380PAY)
       m_CondExec 00,EQ,NV380PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV392 :COMPARAISON SITUATION NCG J +1 / SITUATION WCS _A J                  
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV380PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NV380PBA
       ;;
(NV380PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# ****** TABLE                                                                 
#    RSNV18   : NAME=RSNV18,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV18 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FNV392J ${DATA}/PXX0/F07.NV0381EP
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} FNV392J1 ${DATA}/PXX0/F07.NV0381EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV392 
       JUMP_LABEL=NV380PBB
       ;;
(NV380PBB)
       m_CondExec 04,GE,NV380PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV380PZA
       ;;
(NV380PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV380PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
