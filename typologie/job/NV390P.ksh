#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV390P.ksh                       --- VERSION DU 09/10/2016 05:47
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV390 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/10 AT 17.41.42 BY BURTEC2                      
#    STANDARDS: P  JOBSET: NV390P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BNV390 :DETERMINATION DU SRP EN COURS  A J +1                               
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV390PA
       ;;
(NV390PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV390PAA
       ;;
(NV390PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSSP15   : NAME=RSSP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP15 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN SORTIE LRECL 18                                            
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 FNV390 ${DATA}/PTEM/NV390PAA.NV390AAP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV390 
       JUMP_LABEL=NV390PAB
       ;;
(NV390PAB)
       m_CondExec 04,GE,NV390PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER ISSU DU PGM BNV390 (PRECEDENT)                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV390PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV390PAD
       ;;
(NV390PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV390PAA.NV390AAP
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV390PAD.NV390BBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 01 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV390PAE
       ;;
(NV390PAE)
       m_CondExec 00,EQ,NV390PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV391 :COMPARAISON SITUATION NCG J+1 / SITUATION WCS A J                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV390PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV390PAG
       ;;
(NV390PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLE EN ECRITURE                                                          
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A2} FNV391J ${DATA}/PTEM/NV390PAD.NV390BBP
       m_FileAssign -d SHR -g +0 FNV391J1 ${DATA}/PXX0/F07.NV390CCP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV391 
       JUMP_LABEL=NV390PAH
       ;;
(NV390PAH)
       m_CondExec 04,GE,NV390PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COPY DU  FICHIER J +1 SI  LA CHAINE EST OK                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV390PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV390PAJ
       ;;
(NV390PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DE GEN -1                                                        
       m_FileAssign -d SHR -g ${G_A3} IN1 ${DATA}/PTEM/NV390PAD.NV390BBP
# ******  FIC DE REPRISE REMIS A ZERO                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 18 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.NV390CCP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV390PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NV390PAK
       ;;
(NV390PAK)
       m_CondExec 16,NE,NV390PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV390PZA
       ;;
(NV390PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV390PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
