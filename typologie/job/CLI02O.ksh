#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CLI02O.ksh                       --- VERSION DU 08/10/2016 13:42
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POCLI02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/10/15 AT 15.33.33 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CLI02O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#   EXTRACTION  DES VENTES                                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CLI02OA
       ;;
(CLI02OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=CLI02OAA
       ;;
(CLI02OAA)
       m_CondExec ${EXAAA},NE,YES 
# ***                                                                          
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES DB2                                                                  
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV02   : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
#    RSBC31   : NAME=RSBC31O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC31 /dev/null
#    RSBC32   : NAME=RSBC32O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC32 /dev/null
#    RSBC33   : NAME=RSBC33O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC33 /dev/null
#    RSBC34   : NAME=RSBC34O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC34 /dev/null
#    RSBC35   : NAME=RSBC35O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC35 /dev/null
#    RSGV03   : NAME=RSGV03O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03 /dev/null
#  FICHIER DES ADRESSES                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FBC101 ${DATA}/PEX0/F16.BBC101AO
#  FICHIER DES ENTETES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -t LSEQ -g +1 FBC102 ${DATA}/PEX0/F16.BBC102AO
#  FICHIER DES LIGNES DE VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -t LSEQ -g +1 FBC103 ${DATA}/PEX0/F16.BBC103AO
#  FICHIER  DES REGLEMENTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -t LSEQ -g +1 FBC104 ${DATA}/PEX0/F16.BBC104AO
#  FICHIER DES ADRESSES                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 750 -t LSEQ -g +1 FBC105 ${DATA}/PEX0/F16.BBC105AO
#  FICHIER DES ENTETES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -t LSEQ -g +1 FBC106 ${DATA}/PEX0/F16.BBC106AO
#  FICHIER DES LIGNES DE VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -t LSEQ -g +1 FBC107 ${DATA}/PEX0/F16.BBC107AO
#  FICHIER  DES REGLEMENTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -t LSEQ -g +1 FBC108 ${DATA}/PEX0/F16.BBC108AO
#  PARAMETRE                                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC102 
       JUMP_LABEL=CLI02OAB
       ;;
(CLI02OAB)
       m_CondExec 04,GE,CLI02OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
