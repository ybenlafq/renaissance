#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TH014P.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTH014 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/11/21 AT 13.46.36 BY BURTEC2                      
#    STANDARDS: P  JOBSET: TH014P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTH014  : CALCUL DE COMPOSANTES                                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TH014PA
       ;;
(TH014PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TH014PAA
       ;;
(TH014PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  TABLES EN LECTURE                                                           
#                                                                              
#    RTGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA38 /dev/null
#    RTGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA67 /dev/null
#    RTTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH00 /dev/null
#    RTTH08   : NAME=RSTH08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH08 /dev/null
#    RTTH09   : NAME=RSTH09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH09 /dev/null
#    RTTH10   : NAME=RSTH10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH10 /dev/null
#    RTTH11   : NAME=RSTH11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH11 /dev/null
#    RTTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH12 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FTH012J ${DATA}/PTEM/TH014PAA.BTH014AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FTH014J ${DATA}/PTEM/TH014PAA.BTH014BP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FTH015J ${DATA}/PTEM/TH014PAA.BTH014CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH014 
       JUMP_LABEL=TH014PAB
       ;;
(TH014PAB)
       m_CondExec 04,GE,TH014PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FIC FTH012J                                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH014PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TH014PAD
       ;;
(TH014PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/TH014PAA.BTH014AP
# ****                                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TH014PAD.BTH014DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TH014PAE
       ;;
(TH014PAE)
       m_CondExec 00,EQ,TH014PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FIC FTH014J                                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH014PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TH014PAG
       ;;
(TH014PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/TH014PAA.BTH014BP
# ****                                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TH014PAG.BTH014EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TH014PAH
       ;;
(TH014PAH)
       m_CondExec 00,EQ,TH014PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FIC FTH015J                                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH014PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TH014PAJ
       ;;
(TH014PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/TH014PAA.BTH014CP
# ****                                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TH014PAJ.BTH014FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TH014PAK
       ;;
(TH014PAK)
       m_CondExec 00,EQ,TH014PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTH015  : CONSTITUTION DE LA SITUATION DU JOUR                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH014PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=TH014PAM
       ;;
(TH014PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  TABLES EN LECTURE                                                           
#    RTTH08   : NAME=RSTH08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH08 /dev/null
#    RTTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH12 /dev/null
#    RTTH14   : NAME=RSTH14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH14 /dev/null
#    RTTH15   : NAME=RSTH15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH15 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN LECTURE                                                          
       m_FileAssign -d SHR -g ${G_A4} FTH012J ${DATA}/PTEM/TH014PAD.BTH014DP
       m_FileAssign -d SHR -g ${G_A5} FTH014J ${DATA}/PTEM/TH014PAG.BTH014EP
       m_FileAssign -d SHR -g ${G_A6} FTH015J ${DATA}/PTEM/TH014PAJ.BTH014FP
#                                                                              
#  TABLES EN MAJ                                                               
#    RTTH12   : NAME=RSTH12,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH12 /dev/null
#    RTTH14   : NAME=RSTH14,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH14 /dev/null
#    RTTH15   : NAME=RSTH15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH15 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH015 
       JUMP_LABEL=TH014PAN
       ;;
(TH014PAN)
       m_CondExec 04,GE,TH014PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TH014PZA
       ;;
(TH014PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH014PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
