#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CB005M.ksh                       --- VERSION DU 08/10/2016 22:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMCB005 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.19.23 BY BURTEC6                      
#    STANDARDS: P  JOBSET: CB005M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   UNLOAD DES TABLES RX20 RS22 RX25                                           
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CB005MA
       ;;
(CB005MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/07/04 AT 18.19.23 BY BURTEC6                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CB005M                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'releve prix'                           
# *                           APPL...: REPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CB005MAA
       ;;
(CB005MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#    RSRX00   : NAME=RSRX20M,MODE=I - DYNAM=YES                                
#    RSRX20   : NAME=RSRX20M,MODE=I - DYNAM=YES                                
#    RSRX21   : NAME=RSRX21M,MODE=I - DYNAM=YES                                
#    RSRX22   : NAME=RSRX22M,MODE=I - DYNAM=YES                                
#    RSRX25   : NAME=RSRX25M,MODE=I - DYNAM=YES                                
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 270 -t LSEQ -g +1 SYSREC ${DATA}/PXX0/F89.CB0005AM
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005MAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY PUIS ENVOI LOTUS NOTES                           
# ********************************************************************         
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CB0005AM                                                            
#          DATAEND                                                             
# ********************************************************************         
#  CHANGEMENT PCL => CB005M <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCB005M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005MAD PGM=FTP        ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CB005MAD
       ;;
(CB005MAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${DATA}/CORTEX4.P.MTXTFIX1/CB005MAD
       m_UtilityExec
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP CB005MAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CB005MAG
       ;;
(CB005MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005MAG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
