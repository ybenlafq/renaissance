#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX06O.ksh                       --- VERSION DU 19/10/2016 16:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGRX06 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/22 AT 14.22.26 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GRX06O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BRX002                                                                
#  -------------                                                               
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX06OA
       ;;
(GRX06OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GRX06OAA
       ;;
(GRX06OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  PARAMETRE DATE                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAM SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE EN ECRITURE                                                    
#    RSRX20   : NAME=RSRX20O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRX20 /dev/null
#    RSRX22   : NAME=RSRX22O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22 /dev/null
#    RSRX25   : NAME=RSRX25O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
# FICHIER EN ENTREE                                                            
       m_FileAssign -d SHR -g +0 FRX002 ${DATA}/PXX0/F16.FGRX06AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX002 
       JUMP_LABEL=GRX06OAB
       ;;
(GRX06OAB)
       m_CondExec 04,GE,GRX06OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
