#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX10Y.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGRX10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/05/03 AT 15.14.47 BY BURTECO                      
#    STANDARDS: P  JOBSET: GRX10Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG : BGA069  ALIMENTATION DE LA TABLE RTGA69                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX10YA
       ;;
(GRX10YA)
       EXAAA=${EXAAA:-0}
       EXAAE=${EXAAE:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRX10YAA
       ;;
(GRX10YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* TABLE DES PRIX ET PRIMES                                             
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE                                                                
#    RSGA75   : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *******                                                                      
# ********************************************                                 
#                                                                              
# ***********************************                                          
# *   STEP GRX10YAD PGM=IKJEFT01   ** ID=AAE                                   
# ***********************************                                          

       m_ProgramExec -b BGA069 
       JUMP_LABEL=GRX10YAD
       ;;
(GRX10YAD)
       m_CondExec ${EXAAE},NE,YES 
# *******                                                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE                                                     
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA02Y  : NAME=RSGA02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA02Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN59 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN68 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA68Y  : NAME=RSGA68Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGG20Y  : NAME=RSGG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGG40Y  : NAME=RSGG40Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGI22Y  : NAME=RSGI22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGI22Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA79Y  : NAME=RSGA79Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA79Y /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGN75   : NAME=RSGN75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN75 /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA75Y  : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75Y /dev/null
# ******* TABLE EN M.A.J                                                       
#    RSHV15Y  : NAME=RSHV15Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15Y /dev/null
# *******                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM082 ${DATA}/PTEM/GRX10YAD.BGM082AY
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM082 
       JUMP_LABEL=GRX10YAE
       ;;
(GRX10YAE)
       m_CondExec 04,GE,GRX10YAD ${EXAAE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC1 (BGM082AY)                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX10YAG PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRX10YAG
       ;;
(GRX10YAG)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC1                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GRX10YAD.BGM082AY
# ********************************* FICHIER FEXTRAC1 TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX10YAG.BGM082BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_29_2 29 CH 2
 /FIELDS FLD_BI_12_5 12 CH 5
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_BI_22_7 22 CH 7
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_BI_17_5 17 CH 5
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_BI_12_5 ASCENDING,
   FLD_BI_17_5 ASCENDING,
   FLD_BI_22_7 ASCENDING,
   FLD_BI_29_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX10YAH
       ;;
(GRX10YAH)
       m_CondExec 00,EQ,GRX10YAG ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BGM082CY                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX10YAJ PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRX10YAJ
       ;;
(GRX10YAJ)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GRX10YAG.BGM082BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GRX10YAJ.BGM082CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX10YAK
       ;;
(GRX10YAK)
       m_CondExec 04,GE,GRX10YAJ ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX10YAM PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRX10YAM
       ;;
(GRX10YAM)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GRX10YAJ.BGM082CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX10YAM.BGM082DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX10YAN
       ;;
(GRX10YAN)
       m_CondExec 00,EQ,GRX10YAM ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : BFT060                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX10YAQ PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRX10YAQ
       ;;
(GRX10YAQ)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GRX10YAG.BGM082BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GRX10YAM.BGM082DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGM082 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX10YAR
       ;;
(GRX10YAR)
       m_CondExec 04,GE,GRX10YAQ ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRX10YZA
       ;;
(GRX10YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX10YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
