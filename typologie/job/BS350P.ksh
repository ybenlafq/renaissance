#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BS350P.ksh                       --- VERSION DU 08/10/2016 22:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBS350 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/07/08 AT 09.35.42 BY BURTEC7                      
#    STANDARDS: P  JOBSET: BS350P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BBS350 :    EXTRACT VENTES LIEES PAR CHEF PRODUIT                           
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BS350PA
       ;;
(BS350PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BS350PAA
       ;;
(BS350PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAM EDITION                                                        
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BS350PAA
#                                                                              
# ******  TABLE READ                                                           
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS05   : NAME=RSBS05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS05 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA20 /dev/null
#    RSGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA21 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA52   : NAME=RSGA52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA52 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSGA92   : NAME=RSGA92,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA92 /dev/null
#    RSGA93   : NAME=RSGA93,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA93 /dev/null
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN59 /dev/null
#    RSGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN68 /dev/null
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
#    RSST30   : NAME=RSST30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSST30 /dev/null
#                                                                              
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 2042 -g +1 FBS350 ${DATA}/PTEM/BS350PAA.BBS350AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS350 
       JUMP_LABEL=BS350PAB
       ;;
(BS350PAB)
       m_CondExec 04,GE,BS350PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FICHIER EXTRACT                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS350PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BS350PAD
       ;;
(BS350PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/BS350PAA.BBS350AP
       m_FileAssign -d NEW,CATLG,DELETE -r 2042 -g +1 SORTOUT ${DATA}/PTEM/BS350PAD.BBS350BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_PD_1_5 1 PD 5
 /FIELDS FLD_PD_6_4 6 PD 4
 /KEYS
   FLD_PD_1_5 ASCENDING,
   FLD_PD_6_4 DESCENDING,
   FLD_BI_10_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS350PAE
       ;;
(BS350PAE)
       m_CondExec 00,EQ,BS350PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS351 : EDITION DES VENTES LIEES PAR CHEF PRODUIT IBS350                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS350PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BS350PAG
       ;;
(BS350PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE READ                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d SHR -g ${G_A2} FBS350 ${DATA}/PTEM/BS350PAD.BBS350BP
#                                                                              
# ******* EDITION IBS350                                                       
       m_OutputAssign -c 9 -w IBS350 IBS350
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS351 
       JUMP_LABEL=BS350PAH
       ;;
(BS350PAH)
       m_CondExec 04,GE,BS350PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BS350PZA
       ;;
(BS350PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS350PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
