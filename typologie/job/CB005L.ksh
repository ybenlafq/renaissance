#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CB005L.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLCB005 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/10/29 AT 14.22.52 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CB005L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   UNLOAD DES TABLES RX20 RS22 RX25                                           
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CB005LA
       ;;
(CB005LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       G_A3=${G_A3:-'+2'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2013/10/29 AT 14.22.52 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CB005L                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'releve prix'                           
# *                           APPL...: REPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CB005LAA
       ;;
(CB005LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#    RSRX00   : NAME=RSRX20L,MODE=I - DYNAM=YES                                
#    RSRX20   : NAME=RSRX20L,MODE=I - DYNAM=YES                                
#    RSRX21   : NAME=RSRX21L,MODE=I - DYNAM=YES                                
#    RSRX22   : NAME=RSRX22L,MODE=I - DYNAM=YES                                
#    RSRX25   : NAME=RSRX25L,MODE=I - DYNAM=YES                                
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 270 -t LSEQ -g +1 SYSREC ${DATA}/PXX0/F61.CB0005AL
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005LAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY PUIS ENVOI LOTUS NOTES                           
# ********************************************************************         
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CB0005AL                                                            
#          DATAEND                                                             
# ********************************************************************         
#   UNLOAD DES TABLE                                                           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAD
       ;;
(CB005LAD)
       m_CondExec ${EXAAF},NE,YES 
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
#    RSGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES                                
#    RSGA75   : NAME=RSGA75L,MODE=I - DYNAM=YES                                
#    RSGA79   : NAME=RSGA79L,MODE=I - DYNAM=YES                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 270 -t LSEQ -g +1 SYSREC ${DATA}/PXX0/F61.CB0005BL
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005LAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC
# ********************************************************************         
#   UNLOAD DES TABLE                                                           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAG
       ;;
(CB005LAG)
       m_CondExec ${EXAAK},NE,YES 
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
#    RSGG20   : NAME=RSGG20L,MODE=I - DYNAM=YES                                
#    RSGG40   : NAME=RSGG40L,MODE=I - DYNAM=YES                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 270 -t LSEQ -g +1 SYSREC ${DATA}/PXX0/F61.CB0005CL
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005LAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC
# ********************************************************************         
#  CHANGEMENT PCL => CB005L <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCB005L                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAJ
       ;;
(CB005LAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${DATA}/CORTEX4.P.MTXTFIX1/CB005LAJ
       m_UtilityExec
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTCB005L                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAM
       ;;
(CB005LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005LAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/CB005LAM.FTCB005L
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCB005L                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAQ
       ;;
(CB005LAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${DATA}/CORTEX4.P.MTXTFIX1/CB005LAQ
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PTEM/CB005LAM.FTCB005L
       m_UtilityExec
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTCB005l                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAT
       ;;
(CB005LAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005LAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A2} SYSOUT ${DATA}/PTEM/CB005LAM.FTCB005L
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCB005l                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CB005LAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=CB005LAX
       ;;
(CB005LAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${DATA}/CORTEX4.P.MTXTFIX1/CB005LAX
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/CB005LAM.FTCB005L
       m_UtilityExec
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CB005LZA
       ;;
(CB005LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CB005LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
