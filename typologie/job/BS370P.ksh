#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BS370P.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBS370 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/07/06 AT 10.29.07 BY BURTEC6                      
#    STANDARDS: P  JOBSET: BS370P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BBS370 :    EXTRACT VENTES LIEES PAR CHEF PRODUIT                           
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BS370PA
       ;;
(BS370PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+2'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+2'}
       LASTJO=${LASTJO}
       RUN=${RUN}
       JUMP_LABEL=BS370PAA
       ;;
(BS370PAA)
       m_CondExec ${EXAAA},NE,YES 1,NE,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* MOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAM EDITION                                                        
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BS370PAA
# ******* PARAM EXTRACT                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BS370P1
#                                                                              
# ******  TABLE READ                                                           
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA20 /dev/null
#    RSGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA21 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSGA92   : NAME=RSGA92,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA92 /dev/null
#    RSGA93   : NAME=RSGA93,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA93 /dev/null
#    RSST30   : NAME=RSST30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSST30 /dev/null
#                                                                              
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FBS370 ${DATA}/PTEM/BS370PAA.BBS370AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS370 
       JUMP_LABEL=BS370PAB
       ;;
(BS370PAB)
       m_CondExec 04,GE,BS370PAA ${EXAAA},NE,YES 1,NE,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FICHIER EXTRACT                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAD
       ;;
(BS370PAD)
       m_CondExec ${EXAAF},NE,YES 1,NE,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/BS370PAA.BBS370AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/BS370PAD.BBS370BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_155_4 155 CH 4
 /FIELDS FLD_CH_149_6 149 CH 6
 /FIELDS FLD_CH_137_6 137 CH 6
 /FIELDS FLD_CH_143_6 143 CH 6
 /FIELDS FLD_CH_119_4 119 CH 4
 /FIELDS FLD_CH_131_6 131 CH 6
 /FIELDS FLD_CH_127_4 127 CH 4
 /FIELDS FLD_BI_1_3 01 CH 03
 /FIELDS FLD_CH_123_4 123 CH 4
 /KEYS
   FLD_BI_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_119_4,
    TOTAL FLD_CH_123_4,
    TOTAL FLD_CH_127_4,
    TOTAL FLD_CH_131_6,
    TOTAL FLD_CH_137_6,
    TOTAL FLD_CH_143_6,
    TOTAL FLD_CH_149_6,
    TOTAL FLD_CH_155_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS370PAE
       ;;
(BS370PAE)
       m_CondExec 00,EQ,BS370PAD ${EXAAF},NE,YES 1,NE,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS371 : EDITION DES VENTES LIEES PAR CHEF PRODUIT IBS370                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAG
       ;;
(BS370PAG)
       m_CondExec ${EXAAK},NE,YES 1,NE,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE READ                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d SHR -g ${G_A2} FBS370 ${DATA}/PTEM/BS370PAD.BBS370BP
#                                                                              
# ******* EDITION IBS370                                                       
       m_OutputAssign -c 9 -w IBS370 IBS371
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS371 
       JUMP_LABEL=BS370PAH
       ;;
(BS370PAH)
       m_CondExec 04,GE,BS370PAG ${EXAAK},NE,YES 1,NE,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FICHIER EXTRACT                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAJ
       ;;
(BS370PAJ)
       m_CondExec ${EXAAP},NE,YES 1,NE,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/BS370PAA.BBS370AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/BS370PAJ.BBS370CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_8 01 CH 08
 /FIELDS FLD_PD_9_4 9 PD 4
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_PD_9_4 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS370PAK
       ;;
(BS370PAK)
       m_CondExec 00,EQ,BS370PAJ ${EXAAP},NE,YES 1,NE,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS372 : EDITION DES VENTES LIEES PAR MAGASINS IBS372                       
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAM
       ;;
(BS370PAM)
       m_CondExec ${EXAAU},NE,YES 1,NE,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE READ                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d SHR -g ${G_A4} FBS370 ${DATA}/PTEM/BS370PAJ.BBS370CP
#                                                                              
# ******* EDITION IBS370                                                       
       m_OutputAssign -c 9 -w IBS372 IBS372
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS372 
       JUMP_LABEL=BS370PAN
       ;;
(BS370PAN)
       m_CondExec 04,GE,BS370PAM ${EXAAU},NE,YES 1,NE,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS370 :    EXTRACT VENTES LIEES PAR CHEF PRODUIT                           
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAQ
       ;;
(BS370PAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* MOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAM EDITION                                                        
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/BS370PAQ
# ******* PARAM EXTRACT                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BS370P2
#                                                                              
# ******  TABLE READ                                                           
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA20 /dev/null
#    RSGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA21 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSGA92   : NAME=RSGA92,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA92 /dev/null
#    RSGA93   : NAME=RSGA93,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA93 /dev/null
#    RSST30   : NAME=RSST30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSST30 /dev/null
#                                                                              
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A5} FBS370 ${DATA}/PTEM/BS370PAA.BBS370AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS370 
       JUMP_LABEL=BS370PAR
       ;;
(BS370PAR)
       m_CondExec 04,GE,BS370PAQ ${EXAAZ},NE,YES 1,EQ,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FICHIER EXTRACT                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAT
       ;;
(BS370PAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/BS370PAA.BBS370AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A7} SORTOUT ${DATA}/PTEM/BS370PAD.BBS370BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_137_6 137 CH 6
 /FIELDS FLD_CH_123_4 123 CH 4
 /FIELDS FLD_CH_155_4 155 CH 4
 /FIELDS FLD_CH_131_6 131 CH 6
 /FIELDS FLD_BI_1_3 01 CH 03
 /FIELDS FLD_CH_127_4 127 CH 4
 /FIELDS FLD_CH_119_4 119 CH 4
 /FIELDS FLD_CH_143_6 143 CH 6
 /FIELDS FLD_CH_149_6 149 CH 6
 /KEYS
   FLD_BI_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_119_4,
    TOTAL FLD_CH_123_4,
    TOTAL FLD_CH_127_4,
    TOTAL FLD_CH_131_6,
    TOTAL FLD_CH_137_6,
    TOTAL FLD_CH_143_6,
    TOTAL FLD_CH_149_6,
    TOTAL FLD_CH_155_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS370PAU
       ;;
(BS370PAU)
       m_CondExec 00,EQ,BS370PAT ${EXABE},NE,YES 1,EQ,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS371 : EDITION DES VENTES LIEES PAR CHEF PRODUIT IBS370                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=BS370PAX
       ;;
(BS370PAX)
       m_CondExec ${EXABJ},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE READ                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d SHR -g ${G_A8} FBS370 ${DATA}/PTEM/BS370PAD.BBS370BP
#                                                                              
# ******* EDITION IBS370                                                       
       m_OutputAssign -c 9 -w IBS370 IBS371
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS371 
       JUMP_LABEL=BS370PAY
       ;;
(BS370PAY)
       m_CondExec 04,GE,BS370PAX ${EXABJ},NE,YES 1,EQ,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FICHIER EXTRACT                                                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=BS370PBA
       ;;
(BS370PBA)
       m_CondExec ${EXABO},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/BS370PAA.BBS370AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A10} SORTOUT ${DATA}/PTEM/BS370PAJ.BBS370CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_9_4 9 PD 4
 /FIELDS FLD_CH_1_8 01 CH 08
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_PD_9_4 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS370PBB
       ;;
(BS370PBB)
       m_CondExec 00,EQ,BS370PBA ${EXABO},NE,YES 1,EQ,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS372 : EDITION DES VENTES LIEES PAR MAGASINS IBS372                       
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS370PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=BS370PBD
       ;;
(BS370PBD)
       m_CondExec ${EXABT},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE READ                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER D EXTRACT                                                    
       m_FileAssign -d SHR -g ${G_A11} FBS370 ${DATA}/PTEM/BS370PAJ.BBS370CP
#                                                                              
# ******* EDITION IBS370                                                       
       m_OutputAssign -c 9 -w IBS372 IBS372
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS372 
       JUMP_LABEL=BS370PBE
       ;;
(BS370PBE)
       m_CondExec 04,GE,BS370PBD ${EXABT},NE,YES 1,EQ,$[LASTJO] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BS370PZA
       ;;
(BS370PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS370PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
