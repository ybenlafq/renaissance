#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV000P.ksh                       --- VERSION DU 08/10/2016 12:44
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/12/05 AT 16.07.10 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   UNLOAD DE LA TABLE RTNV00                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTNV00 : PARIS                               
#  REPRISE .OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV000PA
       ;;
(NV000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV000PAA
       ;;
(NV000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
#    RSNV00   : NAME=RSNV00,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV000PAA.NV000UAP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV000PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 312 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV000PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV000PAD
       ;;
(NV000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV000PAA.NV000UAP
       m_FileAssign -d NEW,CATLG,DELETE -r 312 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV000PAD.NV000UBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_20 1 CH 20
 /FIELDS FLD_CH_22_255 22 CH 255
 /FIELDS FLD_PD_303_10 303 PD 10
 /KEYS
   FLD_CH_1_20 ASCENDING,
   FLD_PD_303_10 ASCENDING,
   FLD_CH_22_255 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV000PAE
       ;;
(NV000PAE)
       m_CondExec 00,EQ,NV000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV000 : ENVOI DES DONNEES NCG VERS LE CATALOGUE VIA MQ                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV000PAG
       ;;
(NV000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV10   : NAME=RSNV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV10 /dev/null
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREES                                                    
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FNV000 ${DATA}/PTEM/NV000PAD.NV000UBP
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
# ****** TABLE                                                                 
#    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV000 
       JUMP_LABEL=NV000PAH
       ;;
(NV000PAH)
       m_CondExec 04,GE,NV000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UPDATE SUR LA TABLE RTNV10                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV000PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV000PAJ
       ;;
(NV000PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV000PAJ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=NV000PAK
       ;;
(NV000PAK)
       m_CondExec 04,GE,NV000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV000PZA
       ;;
(NV000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
