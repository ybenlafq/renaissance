#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM070Y.ksh                       --- VERSION DU 09/10/2016 00:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGM070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/16 AT 06.06.14 BY BURTECA                      
#    STANDARDS: P  JOBSET: GM070Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA59 : PRIX                              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA75 : PRIME                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GM070YA
       ;;
(GM070YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       MERCRED=${MERCRED}
       RUN=${RUN}
       JUMP_LABEL=GM070YAA
       ;;
(GM070YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
#    RSGS30   : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
#    RSGA75   : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/GM070YAA.BGA59AY
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/GM070YAA.BGA75AY
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/GM070YAA.BGS30UY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070YAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DES FICHIERS D'UNLOAD DE LA GA59                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAD
       ;;
(GM070YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GM070YAA.BGA59AY
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YAD.BGA59ZY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_2 8 CH 2
 /FIELDS FLD_CH_15_8 15 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_15_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YAE
       ;;
(GM070YAE)
       m_CondExec 00,EQ,GM070YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES FICHIERS D'UNLOAD DE LA GA75                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAG
       ;;
(GM070YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GM070YAA.BGA75AY
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YAG.BGA75ZY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_CH_8_2 8 CH 2
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_10_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YAH
       ;;
(GM070YAH)
       m_CondExec 00,EQ,GM070YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC RTGS30                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAJ
       ;;
(GM070YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GM070YAA.BGS30UY
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/F07.BGS30FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_CH_21_3 21 CH 3
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_4_3,FLD_CH_18_3,FLD_CH_21_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070YAK
       ;;
(GM070YAK)
       m_CondExec 00,EQ,GM070YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM068                                                                      
#  EXTRACT DES VENTES SUR 28 JOURS GLISSANTS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAM
       ;;
(GM070YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSHV02   : NAME=RSHV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02 /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ****** FICHIER DES VENTES SUR 28 JOURS                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -t LSEQ -g +1 FGM068 ${DATA}/PTEM/GM070YAM.BGM068AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM068 
       JUMP_LABEL=GM070YAN
       ;;
(GM070YAN)
       m_CondExec 04,GE,GM070YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES SOCIETES                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAQ
       ;;
(GM070YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GM070YAM.BGM068AY
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YAQ.BGM068BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_8_4 08 PD 4
 /FIELDS FLD_CH_1_7 01 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_8_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YAR
       ;;
(GM070YAR)
       m_CondExec 00,EQ,GM070YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM069                                                                      
#  EXTRACT DES CODES DESCRIPTIFS , CODE MARKETING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAT
       ;;
(GM070YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#  FICHIER EN ENTRE ISSU DU GM010P                                             
       m_FileAssign -d SHR -g +0 FGA053 ${DATA}/PXX0/F07.BGA053BP
#  MODIF LE FGM069 PASSE DE 273 A 436                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -t LSEQ -g +1 FGM069 ${DATA}/PTEM/GM070YAT.BGM069AY
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FGM020 ${DATA}/PTEM/GM070YAT.BGM069BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM069 
       JUMP_LABEL=GM070YAU
       ;;
(GM070YAU)
       m_CondExec 04,GE,GM070YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070YAX
       ;;
(GM070YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GM070YAT.BGM069AY
#  CE FICHIER PASSE DE 273 A 436                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YAX.BGM069CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_145_5 145 CH 5
 /FIELDS FLD_CH_1_7 01 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_145_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YAY
       ;;
(GM070YAY)
       m_CondExec 00,EQ,GM070YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBA
       ;;
(GM070YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F45.BGM065MY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "945"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_5 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBB
       ;;
(GM070YBB)
       m_CondExec 00,EQ,GM070YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065Z                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBD
       ;;
(GM070YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F45.BGM065ZY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "945"
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBE
       ;;
(GM070YBE)
       m_CondExec 00,EQ,GM070YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM070                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBG
       ;;
(GM070YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA30   : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSGA66   : NAME=RSGA66Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66 /dev/null
#    RSGG20   : NAME=RSGG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20 /dev/null
#    RSGG40   : NAME=RSGG40Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSSP15   : NAME=RSSP15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSP15 /dev/null
#    RSGA68   : NAME=RSGA68Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
#    RSRX00   : NAME=RSRX00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
#    RSRX55   : NAME=RSRX55Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX55 /dev/null
#                                                                              
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
# **** DATE                                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ****** FICHIER LUS                                                           
       m_FileAssign -d SHR -g ${G_A6} FGM068 ${DATA}/PTEM/GM070YAQ.BGM068BY
#  CE FICHIER DOIT ETRE RELU EN 436                                            
       m_FileAssign -d SHR -g ${G_A7} FGM069 ${DATA}/PTEM/GM070YAX.BGM069CY
       m_FileAssign -d SHR -g ${G_A8} FGM020 ${DATA}/PTEM/GM070YAT.BGM069BY
       m_FileAssign -d SHR -g +0 RTGN59 ${DATA}/PXX0/F07.BGN59FF
       m_FileAssign -d SHR -g ${G_A9} RTGA59Z ${DATA}/PTEM/GM070YAD.BGA59ZY
       m_FileAssign -d SHR -g +0 RTGN75F ${DATA}/PXX0/F07.BGN75FF
       m_FileAssign -d SHR -g ${G_A10} RTGA75Z ${DATA}/PTEM/GM070YAG.BGA75ZY
       m_FileAssign -d SHR -g ${G_A11} FGM065M ${DATA}/PEX0/F45.BGM065MY
       m_FileAssign -d SHR -g ${G_A12} FGM065Z ${DATA}/PEX0/F45.BGM065ZY
       m_FileAssign -d SHR -g ${G_A13} RTGS30 ${DATA}/PTEM/F07.BGS30FY
       m_FileAssign -d SHR -g +0 RTGS10 ${DATA}/PXX0/F07.BGS10FF
# ****** FICHIER CREES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 RTGM01 ${DATA}/PTEM/GM070YBG.BGM070AY
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 RTGM06 ${DATA}/PTEM/GM070YBG.BGM070BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FGM075 ${DATA}/PTEM/GM070YBG.BGM070CY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FGM70 ${DATA}/PTEM/GM070YBG.BGM070DY
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 FGM01 ${DATA}/PTEM/GM070YBG.BGM070EY
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 FGM06 ${DATA}/PTEM/GM070YBG.BGM070FY
#  CREATION DE 2 NOUVEAUX FICHIERS LRECL 227                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 FSM025 ${DATA}/PTEM/GM070YBG.BGM070MY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 FSM026 ${DATA}/PTEM/GM070YBG.BGM070NY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM070 
       JUMP_LABEL=GM070YBH
       ;;
(GM070YBH)
       m_CondExec 04,GE,GM070YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBJ
       ;;
(GM070YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GM070YBG.BGM070CY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YBJ.BGM070GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBK
       ;;
(GM070YBK)
       m_CondExec 00,EQ,GM070YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBM
       ;;
(GM070YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GM070YBG.BGM070DY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YBM.BGM070HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBN
       ;;
(GM070YBN)
       m_CondExec 00,EQ,GM070YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBQ
       ;;
(GM070YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GM070YBG.BGM070AY
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YBQ.BGM070IY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_32_7 32 CH 7
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_24_1 24 CH 1
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_24_1 DESCENDING,
   FLD_CH_25_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBR
       ;;
(GM070YBR)
       m_CondExec 00,EQ,GM070YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBT
       ;;
(GM070YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GM070YBG.BGM070EY
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YBT.BGM070JY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_25_7 ASCENDING,
   FLD_CH_24_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBU
       ;;
(GM070YBU)
       m_CondExec 00,EQ,GM070YBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GM070YBX
       ;;
(GM070YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GM070YBG.BGM070BY
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YBX.BGM070KY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_8_7 8 CH 7
 /FIELDS FLD_CH_22_1 22 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_8_7 ASCENDING,
   FLD_CH_22_1 DESCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YBY
       ;;
(GM070YBY)
       m_CondExec 00,EQ,GM070YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCA
       ;;
(GM070YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GM070YBG.BGM070FY
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YCA.BGM070LY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_22_1 22 CH 1
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_22_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YCB
       ;;
(GM070YCB)
       m_CondExec 00,EQ,GM070YCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCD
       ;;
(GM070YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GM070YBG.BGM070MY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YCD.BGM070OY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_CH_104_7 104 CH 7
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_104_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YCE
       ;;
(GM070YCE)
       m_CondExec 00,EQ,GM070YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCG
       ;;
(GM070YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/GM070YBG.BGM070NY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YCG.BGM070PY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_CH_97_7 97 CH 7
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YCH
       ;;
(GM070YCH)
       m_CondExec 00,EQ,GM070YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM075                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
#  ACCES A LA SOUS TABLE GMPRI                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCJ
       ;;
(GM070YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  2 NOUVEAUX FICHIERS RELU EN 227                                             
       m_FileAssign -d SHR -g ${G_A22} FSM025 ${DATA}/PTEM/GM070YCD.BGM070OY
       m_FileAssign -d SHR -g ${G_A23} FSM026 ${DATA}/PTEM/GM070YCG.BGM070PY
       m_FileAssign -d SHR -g ${G_A24} FGM70 ${DATA}/PTEM/GM070YBM.BGM070HY
       m_FileAssign -d SHR -g ${G_A25} FGM075 ${DATA}/PTEM/GM070YBJ.BGM070GY
       m_FileAssign -d SHR -g ${G_A26} RTGM01 ${DATA}/PTEM/GM070YBQ.BGM070IY
       m_FileAssign -d SHR -g ${G_A27} RTGM06 ${DATA}/PTEM/GM070YBX.BGM070KY
       m_FileAssign -d SHR -g ${G_A28} FGM01 ${DATA}/PTEM/GM070YBT.BGM070JY
       m_FileAssign -d SHR -g ${G_A29} FGM06 ${DATA}/PTEM/GM070YCA.BGM070LY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGM075 ${DATA}/PTEM/GM070YCJ.BGM075AY
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 RTGM01S ${DATA}/PTEM/GM070YCJ.BGM075BY
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 RTGM06S ${DATA}/PTEM/GM070YCJ.BGM075CY
#  2 NOVEAUX FICHIERS DE SORTIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -t LSEQ -g +1 FSM025S ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 FGM080 ${DATA}/PTEM/GM070YCJ.BGM075EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM075 
       JUMP_LABEL=GM070YCK
       ;;
(GM070YCK)
       m_CondExec 04,GE,GM070YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM01                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCM
       ;;
(GM070YCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/GM070YCJ.BGM075BY
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -t LSEQ -g +1 SORTOUT ${DATA}/PGG945/F45.RELOAD.GM01RY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_14 25 CH 14
 /KEYS
   FLD_CH_25_14 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YCN
       ;;
(GM070YCN)
       m_CondExec 00,EQ,GM070YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM06                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCQ
       ;;
(GM070YCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GM070YCJ.BGM075CY
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SORTOUT ${DATA}/PGG945/F45.RELOAD.GM06RY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 01 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YCR
       ;;
(GM070YCR)
       m_CondExec 00,EQ,GM070YCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM70                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCT
       ;;
(GM070YCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/GM070YCJ.BGM075AY
       m_FileAssign -d NEW,CATLG,DELETE -r 402 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F45.RELOAD.GM70RY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_94_2 94 CH 2
 /FIELDS FLD_CH_20_74 20 CH 74
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_7_401 07 CH 401
 /FIELDS FLD_CH_7_10 07 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_74 ASCENDING,
   FLD_BI_94_2 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_401
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070YCU
       ;;
(GM070YCU)
       m_CondExec 00,EQ,GM070YCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM01                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GM070YCX
       ;;
(GM070YCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A33} SYSREC ${DATA}/PGG945/F45.RELOAD.GM01RY
#    RSGM01   : NAME=RSGM01Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM01 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070YCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070Y_GM070YCX_RTGM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070YCY
       ;;
(GM070YCY)
       m_CondExec 04,GE,GM070YCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM06                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDA
       ;;
(GM070YDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A34} SYSREC ${DATA}/PGG945/F45.RELOAD.GM06RY
#    RSGM06   : NAME=RSGM06Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM06 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070YDA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070Y_GM070YDA_RTGM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070YDB
       ;;
(GM070YDB)
       m_CondExec 04,GE,GM070YDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM70                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDD PGM=DSNUTILB   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDD
       ;;
(GM070YDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A35} SYSREC ${DATA}/PEX0/F45.RELOAD.GM70RY
#    RSGM70   : NAME=RSGM70Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM70 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070YDD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070Y_GM070YDD_RTGM70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070YDE
       ;;
(GM070YDE)
       m_CondExec 04,GE,GM070YDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDG
       ;;
(GM070YDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06   : NAME=RSGM06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A36} FGM020 ${DATA}/PTEM/GM070YAT.BGM069BY
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 945000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R945SOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070YDG
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020 IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070YDH
       ;;
(GM070YDH)
       m_CondExec 04,GE,GM070YDG ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI AVEC MARQUE)                             *         
#  REPRISE: OUI                                                                
# ********************************************************************         
# EVITE RELABEL                                                                
#                                                                              
# ***********************************                                          
# *   STEP GM070YDJ PGM=IEFBR14    ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDJ
       ;;
(GM070YDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_ProgramExec IEFBR14 
#                                                                              
# ********************************************************************         
#  BGM025    LE 1ER MERCREDI DU MOIS                                           
#  EDITION CAHIER-BLEU AVEC ARTICLES EPUISES ET EN CONTREMARQUE      *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDM
       ;;
(GM070YDM)
       m_CondExec ${EXAFK},NE,YES 1,EQ,$[MERCRED] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ******  TABLE CAHIER-BLEU PAR ZONES DE PRIX                                  
#    RSGM06   : NAME=RSGM06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
#                                                                              
# ******  CARTE DATE.                                                          
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM070YDM
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM025 IMPR
# ******  SOCIETE : 945000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R945SOC
#                                                                              
# ******  CARTE PARAMETRE.                                                     
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM025 
       JUMP_LABEL=GM070YDN
       ;;
(GM070YDN)
       m_CondExec 04,GE,GM070YDM ${EXAFK},NE,YES 1,EQ,$[MERCRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM030                                                                      
#  EDITION CAHIER-BLEU TOUTES ZONES MAIS IMPRESSION MONOZONE                   
#          SUIVANT LE PARAM SOUS-TABLE ZPRIX                                   
# ********************************************************************         
# AFP      STEP  PGM=IKJEFT01,LANG=UTIL                                        
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ********  TABLE GENERALISEE.                                                 
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01Y,MODE=(I,U)                             
# ********  TABLE DES LIEUX                                                    
# RSGA10   FILE  DYNAM=YES,NAME=RSGA10Y,MODE=(I,U)                             
# ********  TABLE DES CHEFS DE PRODUITS.                                       
# RSGA02   FILE  DYNAM=YES,NAME=RSGA02,MODE=(I,U)                              
# ********  TABLE DES FAMILLES.                                                
# RSGA14   FILE  DYNAM=YES,NAME=RSGA14Y,MODE=(I,U)                             
# ********  TABLE VALEURS CODES MARKETING PAR ARTICLE.                         
# RSGA26   FILE  DYNAM=YES,NAME=RSGA26Y,MODE=(I,U)                             
# ********  TABLE CAHIER-BLEU BATCH.                                           
# RSGM01   FILE  DYNAM=YES,NAME=RSGM01Y,MODE=I                                 
# ********  TABLE CAHIER-BLEU BATCH.                                           
# RSGM06   FILE  DYNAM=YES,NAME=RSGM06Y,MODE=I                                 
# *                                                                            
# *******  JJMMSSAA                                                            
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# * NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM          
# PARAM    DATA  *                                                             
# 0600000                                                                      
#          DATAEND                                                             
# *******  SOCIETE : 945000                                                    
# FSOCLIEU DATA  CLASS=FIX1,MBR=R945SOC                                        
# ********  FICHIERS D'EDITION.                                                
# //IMPR1  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR2  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR3  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR4  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR5  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR6  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR7  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR8  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR9  DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# //IMPR10 DD SYSOUT=(9,IGM030),SPIN=UNALLOC                                   
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGM030) PLAN(BGM030Y)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDQ
       ;;
(GM070YDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/GM070YCJ.BGM075AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YDQ.BGM080AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_20_67 20 CH 67
 /FIELDS FLD_CH_270_1 270 CH 1
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_67 ASCENDING,
   FLD_CH_270_1 ASCENDING,
   FLD_CH_87_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YDR
       ;;
(GM070YDR)
       m_CondExec 00,EQ,GM070YDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDT
       ;;
(GM070YDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GM070YCJ.BGM075EY
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YDT.BGM075FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_8_2 08 CH 2
 /FIELDS FLD_BI_1_7 01 CH 7
 /KEYS
   FLD_BI_8_2 ASCENDING,
   FLD_BI_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YDU
       ;;
(GM070YDU)
       m_CondExec 00,EQ,GM070YDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGM080                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070YDX
       ;;
(GM070YDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#  FICHIER ENTRE                                                               
       m_FileAssign -d SHR -g ${G_A39} FGM075 ${DATA}/PTEM/GM070YDQ.BGM080AY
       m_FileAssign -d SHR -g ${G_A40} FGM080 ${DATA}/PTEM/GM070YDT.BGM075FY
#  FICHIER SORTIE                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGM080 ${DATA}/PTEM/GM070YDX.BGM080BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGM085 ${DATA}/PTEM/GM070YDX.BGM085AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM080 
       JUMP_LABEL=GM070YDY
       ;;
(GM070YDY)
       m_CondExec 04,GE,GM070YDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM080                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GM070YEA
       ;;
(GM070YEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/GM070YDX.BGM080BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YEA.BGM080EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_PD_148_5 148 PD 5
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YEB
       ;;
(GM070YEB)
       m_CondExec 00,EQ,GM070YEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070YED
       ;;
(GM070YED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A42} FEXTRAC ${DATA}/PTEM/GM070YEA.BGM080EY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GM070YED.BGM080CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070YEE
       ;;
(GM070YEE)
       m_CondExec 04,GE,GM070YED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GM070YEG
       ;;
(GM070YEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PTEM/GM070YED.BGM080CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YEG.BGM080DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YEH
       ;;
(GM070YEH)
       m_CondExec 00,EQ,GM070YEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM080                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GM070YEJ
       ;;
(GM070YEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A44} FEXTRAC ${DATA}/PTEM/GM070YEA.BGM080EY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A45} FCUMULS ${DATA}/PTEM/GM070YEG.BGM080DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGM080 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070YEK
       ;;
(GM070YEK)
       m_CondExec 04,GE,GM070YEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM085                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GM070YEM
       ;;
(GM070YEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PTEM/GM070YDX.BGM085AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YEM.BGM085BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_CH_23_60 23 CH 60
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YEN
       ;;
(GM070YEN)
       m_CondExec 00,EQ,GM070YEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GM070YEQ
       ;;
(GM070YEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A47} FEXTRAC ${DATA}/PTEM/GM070YEM.BGM085BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GM070YEQ.BGM085CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070YER
       ;;
(GM070YER)
       m_CondExec 04,GE,GM070YEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GM070YET
       ;;
(GM070YET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/GM070YEQ.BGM085CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GM070YET.BGM085DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070YEU
       ;;
(GM070YEU)
       m_CondExec 00,EQ,GM070YET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM085                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070YEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GM070YEX
       ;;
(GM070YEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A49} FEXTRAC ${DATA}/PTEM/GM070YEM.BGM085BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FCUMULS ${DATA}/PTEM/GM070YET.BGM085DY
# ********************************** PARAMETRE DATE                            
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IGM085 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070YEY
       ;;
(GM070YEY)
       m_CondExec 04,GE,GM070YEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM070YZA
       ;;
(GM070YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
