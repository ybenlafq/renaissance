#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PTF30Y.ksh                       --- VERSION DU 08/10/2016 12:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYPTF30 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/02/02 AT 10.34.07 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PTF30Y                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  BTF032 : PURGE DES TABLES APPC CARTE-T RTTF30/35                            
#           CRITERE :  N�SEQ DE RTTF35 = NB TOTAL DE RTTF30                    
#                      ET DONT LA DATE CREATION = FDATE - DELAI(FPARAM         
#           DELAI   :  015 NB JOURS                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PTF30YA
       ;;
(PTF30YA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PTF30YAA
       ;;
(PTF30YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* APPC HOST <--> MICRO                                                 
#    RSTF30Y  : NAME=RSTF30Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTF30Y /dev/null
# ******* CONTROLE APPC HOST <--> MICRO                                        
#    RSTF35Y  : NAME=RSTF35Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTF35Y /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DELAI DE PURGE : FDATE - 015                                         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PTF30YAA
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF032 
       JUMP_LABEL=PTF30YAB
       ;;
(PTF30YAB)
       m_CondExec 04,GE,PTF30YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
