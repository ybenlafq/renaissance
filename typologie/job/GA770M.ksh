#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA770M.ksh                       --- VERSION DU 08/10/2016 13:21
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGA770 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/17 AT 16.42.20 BY BURTECN                      
#    STANDARDS: P  JOBSET: GA770M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGA770 : MAJ DE LA DATE DE RECEPTION DES CODIC GROUPES DE RTGA00            
#           SI TOUS LES CODICS ELEMENTS ONT ETE DEJA RECEPTIONNÚS              
#           ET SI LE CODIC GROUPE A 1 PRIX DANS TOUTES LES ZONES DE PR         
#  REPRISE: OUI SI FIN ANORMALE                                                
#           NON SI FIN NORMALE: FAIRE 1 RECOVER DE RSGA00 A PARTIR DES         
#           FULL IMAGE COPY (UTILISER LA CHAINE CORTEX DB2FIM)                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GA770MA
       ;;
(GA770MA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GA770MAA
       ;;
(GA770MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE : SOUS TABLE ZPRIX                                 
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******* TABLE DES LIENS ENTRE ARTICLES                                       
#    RSGA58M  : NAME=RSGA58M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58M /dev/null
# ******* TABLE DES PRIX PAR ARTICLE                                           
#    RSGA59M  : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59M /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE TRAITEE : 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# ******* MAJ DE LA DATE DE RECEPTION (D1RECEPT) DE RTGA00                     
#    RSGA00M  : NAME=RSGA00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******* MAJ DES ARTICLES MUTES                                               
#    RSMU15M  : NAME=RSMU15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU15M /dev/null
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA770 
       JUMP_LABEL=GA770MAB
       ;;
(GA770MAB)
       m_CondExec 04,GE,GA770MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
