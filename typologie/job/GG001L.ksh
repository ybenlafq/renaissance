#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GG001L.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGG001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/08/17 AT 12.43.53 BY BURTECB                      
#    STANDARDS: P  JOBSET: GG001L                                              
# --------------------------------------------------------------------         
# **--USER='CONTROLE'                                                          
# ********************************************************************         
#   EDITION DE LA BIBLE DES PRIX DE VENTE                                      
#   REPRISE : OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GG001LA
       ;;
(GG001LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GG001LAA
       ;;
(GG001LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES MAJ ARTICLES GA00 GA14      *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES ARTICLES                                                   
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  TABLE RELATION ARTICLE ZONE DE PRIX                                  
#    RSGA59L  : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59L /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ******  INPUT FROM CICS                                                      
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX5/BGG015AL
# ******  SORTIE                                                               
       m_OutputAssign -c 9 -w BGG015 IMPR1
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG015 
       JUMP_LABEL=GG001LAB
       ;;
(GG001LAB)
       m_CondExec 04,GE,GG001LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
