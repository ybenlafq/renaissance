#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BGA01G.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGBGA01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/05 AT 16.32.45 BY BURTEC2                      
#    STANDARDS: P  JOBSET: BGA01G                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='EXPLOIT'                                                           
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BGA01GA
       ;;
(BGA01GA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+2'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=BGA01GAA
       ;;
(BGA01GAA)
       m_CondExec ${EXAAA},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGG/F99.UNLOAD.RSFM01G
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SYSREC02 ${DATA}/PNCGG/F99.UNLOAD.RSFM02G
       m_FileAssign -d NEW,CATLG,DELETE -r 57 -t LSEQ -g +1 SYSREC03 ${DATA}/PNCGG/F99.UNLOAD.RSFM03G
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SYSREC04 ${DATA}/PNCGG/F99.UNLOAD.RSFM04G
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 SYSREC05 ${DATA}/PNCGG/F99.UNLOAD.RSFM05G
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SYSREC06 ${DATA}/PNCGG/F99.UNLOAD.RSFM06G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC07 ${DATA}/PNCGG/F99.UNLOAD.RSFM23G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC08 ${DATA}/PNCGG/F99.UNLOAD.RSFM54G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC09 ${DATA}/PNCGG/F99.UNLOAD.RSFM55G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC10 ${DATA}/PNCGG/F99.UNLOAD.RSFM56G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC11 ${DATA}/PNCGG/F99.UNLOAD.RSFM57G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC12 ${DATA}/PNCGG/F99.UNLOAD.RSFM58G
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 SYSREC13 ${DATA}/PNCGG/F99.UNLOAD.RSFM59G
       m_FileAssign -d NEW,CATLG,DELETE -r 78 -t LSEQ -g +1 SYSREC14 ${DATA}/PNCGG/F99.UNLOAD.RSFM64G
       m_FileAssign -d NEW,CATLG,DELETE -r 74 -t LSEQ -g +1 SYSREC15 ${DATA}/PNCGG/F99.UNLOAD.RSFM69G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC16 ${DATA}/PNCGG/F99.UNLOAD.RSFM73G
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGA01GAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  UNLOAD  DES TS RSFM   POUR INTERFACE / STANDARD .(LE 050799)                
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGA01GAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GAD
       ;;
(BGA01GAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGG/F99.UNLOAD.RSFM80G
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -t LSEQ -g +1 SYSREC02 ${DATA}/PNCGG/F99.UNLOAD.RSFM81G
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 SYSREC03 ${DATA}/PNCGG/F99.UNLOAD.RSFM82G
       m_FileAssign -d NEW,CATLG,DELETE -r 31 -t LSEQ -g +1 SYSREC04 ${DATA}/PNCGG/F99.UNLOAD.RSFM83G
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SYSREC05 ${DATA}/PNCGG/F99.UNLOAD.RSFM84G
       m_FileAssign -d NEW,CATLG,DELETE -r 67 -t LSEQ -g +1 SYSREC06 ${DATA}/PNCGG/F99.UNLOAD.RSFM87G
       m_FileAssign -d NEW,CATLG,DELETE -r 69 -t LSEQ -g +1 SYSREC07 ${DATA}/PNCGG/F99.UNLOAD.RSFM88G
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SYSREC08 ${DATA}/PNCGG/F99.UNLOAD.RSFM89G
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -t LSEQ -g +1 SYSREC09 ${DATA}/PNCGG/F99.UNLOAD.RSFM90G
       m_FileAssign -d NEW,CATLG,DELETE -r 55 -t LSEQ -g +1 SYSREC10 ${DATA}/PNCGG/F99.UNLOAD.RSFM92G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC11 ${DATA}/PNCGG/F99.UNLOAD.RSFM65G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC12 ${DATA}/PNCGG/F99.UNLOAD.RSFM85G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC13 ${DATA}/PNCGG/F99.UNLOAD.RSFM86G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC14 ${DATA}/PNCGG/F99.UNLOAD.RSFM91G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC15 ${DATA}/PNCGG/F99.UNLOAD.RSFM93G
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -t LSEQ -g +1 SYSREC16 ${DATA}/PNCGG/F99.UNLOAD.RSFM97G
       m_FileAssign -d NEW,CATLG,DELETE -r 74 -t LSEQ -g +1 SYSREC17 ${DATA}/PNCGG/F99.UNLOAD.RSFM98G
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC18 ${DATA}/PNCGG/F99.UNLOAD.RSFX00G
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SYSREC19 ${DATA}/PNCGG/F99.UNLOAD.RSFM94G
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGA01GAD.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#   BFTS00 : EXTRACTION TABLES FM POUR FORMATTER UN SEQUENTIEL                 
#   A PARTIR DES MAJ DE LA JOURNEE                                             
#                                                                              
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGA01GAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GAG
       ;;
(BGA01GAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******** TABLES EN LECTURE *************                                     
# *****   TABLE                                                                
#    RSFM13   : NAME=RSFM13G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM13 /dev/null
# *****   TABLE                                                                
#    RSFM16   : NAME=RSFM16G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM16 /dev/null
# *****   TABLE                                                                
#    RSFM19   : NAME=RSFM19G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM19 /dev/null
# *****   TABLE                                                                
#    RSFM20   : NAME=RSFM20G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM20 /dev/null
# *****   TABLE                                                                
#    RSFM25   : NAME=RSFM25G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM25 /dev/null
# *****   TABLE                                                                
#    RSFM30   : NAME=RSFM30G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM30 /dev/null
# *****   TABLE                                                                
#    RSFM60   : NAME=RSFM60G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM60 /dev/null
# *****   TABLE                                                                
#    RSFM61   : NAME=RSFM61G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM61 /dev/null
# *****   TABLE                                                                
#    RSFM62   : NAME=RSFM62G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM62 /dev/null
# *****   TABLE                                                                
#    RSFM64   : NAME=RSFM64G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM64 /dev/null
# *****   TABLE                                                                
#    RSFM69   : NAME=RSFM69G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM69 /dev/null
# *                                                                            
# ******  DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
# ******  FICHIER SAM EN SORTIE (LRECL 505)                                    
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 505 -t LSEQ -g +1 FFTS00 ${DATA}/PTEM/BGA01GAG.BFTS00AG
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTS00 
       JUMP_LABEL=BGA01GAH
       ;;
(BGA01GAH)
       m_CondExec 04,GE,BGA01GAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD  TABLE RTFX00 EN VUE DU LOAD DE LA RTFX90 FILIALE                    
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGA01GAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GAJ
       ;;
(BGA01GAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 SYSREC01 ${DATA}/PNCGG/F99.FX00M907
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGA01GAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI1 *  ON TRI LE SEQUENTIEL QUI PERMETTRA LE CHARGEMENT DES TABLES         
#          FT EN FILIALE ET PARIS (PCL GFM01?)                                 
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGA01GAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GAM
       ;;
(BGA01GAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC ISSU DU PGM BFTS00                                               
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/BGA01GAG.BFTS00AG
# ******* FIC TRIE POUR PGM BFTS05                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 505 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGG/F99.BFTS00BG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
 /* Record Type = F  Record Length = 505 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BGA01GAN
       ;;
(BGA01GAN)
       m_CondExec 00,EQ,BGA01GAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 *  ON TRI LE SEQUENTIEL QUI PERMETTRA LE CHARGEMENT DES TABLES         
#          FM EN FILIALE ET PARIS (PCL GFM01?)                                 
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGA01GAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GAQ
       ;;
(BGA01GAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC ISSU DE L'UNLOAD DE LA FM64                                      
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PNCGG/F99.UNLOAD.RSFM64G
# ******* FIC TRIE POUR LOAD DANS LES GFM01*                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 78 -t LSEQ -g ${G_A3} SORTOUT ${DATA}/PNCGG/F99.UNLOAD.RSFM64G
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
 /* Record Type = F  Record Length = 078 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BGA01GAR
       ;;
(BGA01GAR)
       m_CondExec 00,EQ,BGA01GAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI3 *  ON TRI LE SEQUENTIEL QUI PERMETTRA LE CHARGEMENT DES TABLES         
#          FM EN FILIALE ET PARIS (PCL GFM01?)                                 
# *******                                                                      
#  REPRISE : OUI EN CAS DE PLANTAGE                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGA01GAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GAT
       ;;
(BGA01GAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC ISSU DE L'UNLOAD DE LA FM64                                      
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PNCGG/F99.UNLOAD.RSFM97G
# ******* FIC TRIE POUR LOAD DANS LES GFM01*                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -t LSEQ -g ${G_A5} SORTOUT ${DATA}/PNCGG/F99.UNLOAD.RSFM97G
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_17 6 CH 17
 /FIELDS FLD_CH_54_8 54 CH 8
 /KEYS
   FLD_CH_6_17 ASCENDING,
   FLD_CH_54_8 ASCENDING,
   FLD_CH_1_5 ASCENDING
 /* Record Type = F  Record Length = 076 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BGA01GAU
       ;;
(BGA01GAU)
       m_CondExec 00,EQ,BGA01GAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BGA01GZA
       ;;
(BGA01GZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGA01GZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
