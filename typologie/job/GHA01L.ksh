#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHA01L.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGHA01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/29 AT 14.24.55 BY BURTECN                      
#    STANDARDS: P  JOBSET: GHA01L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CETTE CHAINE PEUT-ETRES REPORTEE APRES ACCORD D UN RESPONSABLE              
#  RISQUE DE PERTE D INFORMATION SUR LES TABLES ARTICLES                       
#  PAR RAPPORT AUX SAISIES DE PARIS                                            
# ********************************************************************         
# ********************************************************************         
#  BHA002 : CREATION DU FIC DE LOAD POUR LA TABLE P961.RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GHA01LA
       ;;
(GHA01LA)
#
#GHA01LBA
#GHA01LBA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01LBA
#
#
#GHA01LAJ
#GHA01LAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01LAJ
#
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GHA01LAA
       ;;
(GHA01LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE P961.RTGA01                                        
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FIC DES SS TABLES NON MODIFIABLES                                    
       m_FileAssign -d SHR -g +0 FHA001 ${DATA}/PXX0/F07.BHA001AP
#                                                                              
# ******* FIC DE LOAD POUR P961.RTGA01 (LILLE)                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FHA002 ${DATA}/PTEM/GHA01LAA.BHA002AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA002 
       JUMP_LABEL=GHA01LAB
       ;;
(GHA01LAB)
       m_CondExec 04,GE,GHA01LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P961.RTGA01                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LAD
       ;;
(GHA01LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P961.RTGA01                                  
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GHA01LAA.BHA002AL
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PGA961/F61.RELOAD.GA01RL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_15 6 CH 15
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01LAE
       ;;
(GHA01LAE)
       m_CondExec 00,EQ,GHA01LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#           LOAD DE LA TABLE GENERALISEES LILLE P961.RTGA01          *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LAG
       ;;
(GHA01LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GENERALISEE P961.RTGA01                                        
#    RSGA01   : NAME=RSGA01L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FIC DE LOAD POUR  P961.RTGA01                                        
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PGA961/F61.RELOAD.GA01RL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01LAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01L_GHA01LAG_RTGA01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01LAH
       ;;
(GHA01LAH)
       m_CondExec 04,GE,GHA01LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSGA01L DE LA D BASE PLDGA00     *         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LAM
       ;;
(GHA01LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ASSO ARTICLE/PROFIL                                            
#    RSGA73   : NAME=RSGA73L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA73 /dev/null
# ******* TABLE TEXT ETIQUETTE INFO                                            
#    RSGA74   : NAME=RSGA74L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA74 /dev/null
#                                                                              
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA73                     
       m_FileAssign -d SHR -g +0 FHA73 ${DATA}/PXX0/F07.BHA003AP
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA74                     
       m_FileAssign -d SHR -g +0 FHA74 ${DATA}/PXX0/F07.BHA003BP
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA004 
       JUMP_LABEL=GHA01LAN
       ;;
(GHA01LAN)
       m_CondExec 04,GE,GHA01LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA009 : MISE A JOUR DE LA TABLE P961.RTGA06                      *         
#           MISE A JOUR DE LA TABLE P961.RTFT10                      *         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LAQ
       ;;
(GHA01LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC EXTRACTS ENTITES DE COMMANDES PROVENANT DE NCP                   
       m_FileAssign -d SHR -g +0 FHA005 ${DATA}/PXX0/F07.BHA005AP
#                                                                              
# ******* TABLE GENERALISEE RTGA01                                             
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ENTITES DE COMMANDES RTGA06                                    
#    RSGA06   : NAME=RSGA06L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
# ******* TABLE DES REPERTOIRES FOURNISSEURS                                   
#    RSFT10   : NAME=RSFT10L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT10 /dev/null
# ******* TABLE FM                                                             
#    RSFM01   : NAME=RSFM01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA009 
       JUMP_LABEL=GHA01LAR
       ;;
(GHA01LAR)
       m_CondExec 04,GE,GHA01LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P961.RTHA30                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LAT
       ;;
(GHA01LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P961.RTHA30                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHA030CP
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 SORTOUT ${DATA}/PGA961/F61.RELOAD.HA30RL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01LAU
       ;;
(GHA01LAU)
       m_CondExec 00,EQ,GHA01LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE ARTICLES FILIALES  P961.RTHA30                   *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LAX
       ;;
(GHA01LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE ARTICLES FILIALES  P961.RTHA30                                 
#    RSHA30   : NAME=RSHA30L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHA30 /dev/null
# ******* FIC DE LOAD POUR  P961.RTHA30                                        
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PGA961/F61.RELOAD.HA30RL
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01LAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01L_GHA01LAX_RTHA30.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01LAY
       ;;
(GHA01LAY)
       m_CondExec 04,GE,GHA01LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSHA30R DE LA D BASE PLDGA00               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LBD
       ;;
(GHA01LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* N� DE SOCIETE A TRAITER                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA00 ${DATA}/PXX0/F07.BHA050AP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA03 ${DATA}/PXX0/F07.BHA050IP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA31 ${DATA}/PXX0/F07.BHA050JP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA33 ${DATA}/PXX0/F07.BHA050KP
# ******* FIC EXTRACTS RELATION ARTICLE/DECLARATION DE NCP                     
       m_FileAssign -d SHR -g +0 FHA51 ${DATA}/PXX0/F07.BHA050BP
# ******* FIC EXTRACTS RELATION ARTICLE/GARANTIE DE NCP                        
       m_FileAssign -d SHR -g +0 FHA52 ${DATA}/PXX0/F07.BHA050CP
# ******* FIC EXTRACTS ARTICLE/CODE DESCRIPTIF DE NCP                          
       m_FileAssign -d SHR -g +0 FHA53 ${DATA}/PXX0/F07.BHA050DP
# ******* FIC EXTRACTS ARTICLE/ENTITE DE COMMANDE DE NCP                       
       m_FileAssign -d SHR -g +0 FHA55 ${DATA}/PXX0/F07.BHA050EP
# ******* FIC EXTRACTS ARTICLE/ARTICLE DE NCP                                  
       m_FileAssign -d SHR -g +0 FHA58 ${DATA}/PXX0/F07.BHA050FP
# ******* FILE EXTRACTS ZONE DE TRI/PRIX STANDARD                              
       m_FileAssign -d SHR -g +0 FHA59 ${DATA}/PXX0/F07.BHA050GP
# ******* FILE EXTRACTS CARACTERISTIQUES SPECIFIQUES                           
       m_FileAssign -d SHR -g +0 FHA63 ${DATA}/PXX0/F07.BHA050HP
# ******* FIC  EXTRACTS DE GHA01P SUITE MEX PCM                                
       m_FileAssign -d SHR -g +0 FHA40 ${DATA}/PXX0/F07.BHA040AP
       m_FileAssign -d SHR -g +0 FHA41 ${DATA}/PXX0/F07.BHA041AP
       m_FileAssign -d SHR -g +0 FHA42 ${DATA}/PXX0/F07.BHA042AP
#                                                                              
# **TABLES EN MAJ                                                              
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA03   : NAME=RSGA03L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA03 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA31   : NAME=RSGA31L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA33   : NAME=RSGA33L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA33 /dev/null
# ******* TABLE RELATION ARTICLE/DECLARATION                                   
#    RSGA51   : NAME=RSGA51L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* TABLE RELATION ARTICLE/GARANTIE                                      
#    RSGA52   : NAME=RSGA52L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* TABLE RELATION ARTICLE/CODE DESCRIPTIF                               
#    RSGA53   : NAME=RSGA53L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* TABLE RELATION ARTICLE/ENTITE DE COMMANDE                            
#    RSGA55   : NAME=RSGA55L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA55 /dev/null
# ******* TABLE RELATION ARTICLE/ARTICLE                                       
#    RSGA58   : NAME=RSGA58L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLES : ZONE DE PRIX - PRIX STANDARDS                       
#    RSGA59   : NAME=RSGA59L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE ARTICLES : CARACTERISTIQUES SPECIFIQUES                        
#    RSGA63   : NAME=RSGA63L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA63 /dev/null
# ******* TABLE ARTICLES : MODE DE DELIVRANCE                                  
#    RSGA64   : NAME=RSGA64L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
#                                                                              
# **TABLES EN LECTURE                                                          
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE FAMILLE / MODE DE DELIVRANCE                                   
#    RSGA13   : NAME=RSGA13L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13 /dev/null
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FAMILLES : PARAMETRES ASSOCIES AUX FAMILLES                    
#    RSGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA051 
       JUMP_LABEL=GHA01LBE
       ;;
(GHA01LBE)
       m_CondExec 04,GE,GHA01LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA008 : MISE A JOUR DES CODES PARAM FAMILLE SUR LA RTGA30 A                
#           PARTIE DU FICHIER FGA30AF ISSU DE GHA01P                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LBG
       ;;
(GHA01LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FGA30 ${DATA}/PXX0/F99.FGA30AF
# ****** TABLE PARAM FAMILLE                                                   
#    RSGA30   : NAME=RSGA30L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA008 
       JUMP_LABEL=GHA01LBH
       ;;
(GHA01LBH)
       m_CondExec 04,GE,GHA01LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL001 :                                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LBJ
       ;;
(GHA01LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RTGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA19   : NAME=RSGA19L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTFL05   : NAME=RSFL05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL06   : NAME=RSFL06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL06 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTGQ05   : NAME=RSGQ05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGQ05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RTGA00   : NAME=RSGA00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA65   : NAME=RSGA65L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# -----   FICHIER REPRIS DANS LA CHA�NE FL003P                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FL001 ${DATA}/PXX0/F61.BFL001AL
# -----   FICHIER POUR EVITER DEPASSEMENT TABLEAU INTERNE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 159 -t LSEQ -g +1 FGA00 ${DATA}/PXX0/F61.BFL001CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL001 
       JUMP_LABEL=GHA01LBK
       ;;
(GHA01LBK)
       m_CondExec 04,GE,GHA01LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP00 A PARTIR DU TS RSSP00  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LBM
       ;;
(GHA01LBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP00   : NAME=RSSP00L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP00 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP00
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01LBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01L_GHA01LBM_RTSP00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01LBN
       ;;
(GHA01LBN)
       m_CondExec 04,GE,GHA01LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP05 A PARTIR DU TS RSSP05  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LBQ
       ;;
(GHA01LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP05   : NAME=RSSP05L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP05 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP05
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01LBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01L_GHA01LBQ_RTSP05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01LBR
       ;;
(GHA01LBR)
       m_CondExec 04,GE,GHA01LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP15 A PARTIR DU TS RSSP15  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01LBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LBT
       ;;
(GHA01LBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP15   : NAME=RSSP15L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP15 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP15
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01LBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01L_GHA01LBT_RTSP15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01LBU
       ;;
(GHA01LBU)
       m_CondExec 04,GE,GHA01LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GHA01LZA
       ;;
(GHA01LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
