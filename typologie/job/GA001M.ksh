#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA001M.ksh                       --- VERSION DU 08/10/2016 17:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGA001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/03 AT 11.01.50 BY BURTECA                      
#    STANDARDS: P  JOBSET: GA001M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#     LOAD DES DONNEES ARTICLES     (RTGA07 : GROUPE-FOURNISSEURS)             
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA07                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA001MA
       ;;
(GA001MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/02/03 AT 11.01.50 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GA001M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'LOAD T.ARTICLES'                       
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA001MAA
       ;;
(GA001MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GROUPE FOURNISSEURS                                            
#    RSGA07M  : NAME=RSGA07M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA07M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA07                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA07RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAA_RTGA07.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAB
       ;;
(GA001MAB)
       m_CondExec 04,GE,GA001MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA14 : FAMILLES)                        
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA14                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAD
       ;;
(GA001MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES                                                   
#    RSGA14M  : NAME=RSGA14M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA14                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA14RF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAD_RTGA14.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAE
       ;;
(GA001MAE)
       m_CondExec 04,GE,GA001MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA15 : FAMILLE/TYPE DECLARAT�)          
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA15                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAG
       ;;
(GA001MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR TYPE DE DECLARATIONS                          
#    RSGA15M  : NAME=RSGA15M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA15M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA15                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA15RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAG_RTGA15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAH
       ;;
(GA001MAH)
       m_CondExec 04,GE,GA001MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA16 : FAMILLE/GARANTIE)                
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA16                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAJ
       ;;
(GA001MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR GARANTIE-COMPLEMENTAIRES                      
#    RSGA16M  : NAME=RSGA16M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA16M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA16                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA16RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAJ_RTGA16.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAK
       ;;
(GA001MAK)
       m_CondExec 04,GE,GA001MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA18 : FAMILLE/CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA18                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAM
       ;;
(GA001MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR CODE-DESCRIPTIFS                              
#    RSGA18M  : NAME=RSGA18M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA18M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA18                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA18RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAM_RTGA18.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAN
       ;;
(GA001MAN)
       m_CondExec 04,GE,GA001MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA20 : CARACTERISTIC SPECIFIC)          
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA20                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAQ
       ;;
(GA001MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CARACTERISTIQUES SPECIFIQUES                               
#    RSGA20M  : NAME=RSGA20M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA20M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA20                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA20RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAQ_RTGA20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAR
       ;;
(GA001MAR)
       m_CondExec 04,GE,GA001MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA21 : FAMILLE/RAYON )                  
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA21                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAT
       ;;
(GA001MAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLE PAR RAYON                                          
#    RSGA21M  : NAME=RSGA21M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA21M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA21                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA21RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAT_RTGA21.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAU
       ;;
(GA001MAU)
       m_CondExec 04,GE,GA001MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA22 : COMPOSANTS/RAYON)                
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA22                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GA001MAX
       ;;
(GA001MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES COMPOSANT/RAYON                                            
#    RSGA22M  : NAME=RSGA22M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA22M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA22                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA22RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MAX_RTGA22.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MAY
       ;;
(GA001MAY)
       m_CondExec 04,GE,GA001MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA23 : CODE MARKETING )                 
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA23                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBA
       ;;
(GA001MBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES MARKETING                                            
#    RSGA23M  : NAME=RSGA23M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA23M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA23                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA23RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBA_RTGA23.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBB
       ;;
(GA001MBB)
       m_CondExec 04,GE,GA001MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA24 : CODE DESCRIPTIFS)                
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA24                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBD
       ;;
(GA001MBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES DESCRIPTIFS                                          
#    RSGA24R  : NAME=RSGA24M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA24R /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA24                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA24RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBD_RTGA24.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBE
       ;;
(GA001MBE)
       m_CondExec 04,GE,GA001MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA25 : ASSO MARKETING DECRIPT)          
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA25                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBG
       ;;
(GA001MBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES ASSO MARKETING-DESCRIPTIFS                                 
#    RSGA25M  : NAME=RSGA25M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA25M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA25                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA25RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBG_RTGA25.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBH
       ;;
(GA001MBH)
       m_CondExec 04,GE,GA001MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA26 : VALEURS CODE MAKETING)           
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA26                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBJ
       ;;
(GA001MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-MARKETING                                     
#    RSGA26M  : NAME=RSGA26M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA26                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA26RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBJ_RTGA26.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBK
       ;;
(GA001MBK)
       m_CondExec 04,GE,GA001MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA27 : VALEURS CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA27                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBM
       ;;
(GA001MBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-DESCRIPTIFS                                   
#    RSGA27M  : NAME=RSGA27M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA27M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA27                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA27RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBM_RTGA27.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBN
       ;;
(GA001MBN)
       m_CondExec 04,GE,GA001MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA71 : TABLE DES TABLES)                
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA71                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBQ
       ;;
(GA001MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES TABLES                                                     
#    RSGA71M  : NAME=RSGA71M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA71M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA71                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA71RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBQ_RTGA71.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBR
       ;;
(GA001MBR)
       m_CondExec 04,GE,GA001MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA72 : TABLE DES BORNES)                
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA72                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBT
       ;;
(GA001MBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES BORNES                                                     
#    RSGA72M  : NAME=RSGA72M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA72M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA72                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA72RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBT_RTGA72.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBU
       ;;
(GA001MBU)
       m_CondExec 04,GE,GA001MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA99 : TABLE DES MESSAGES)              
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA99                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MBX PGM=DSNUTILB   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GA001MBX
       ;;
(GA001MBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA99M  : NAME=RSGA99M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA99M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA99                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA99RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MBX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MBX_RTGA99.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MBY
       ;;
(GA001MBY)
       m_CondExec 04,GE,GA001MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA91 : TABLE DES RUBRIQUES)             
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA91                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCA
       ;;
(GA001MCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA91M  : NAME=RSGA91M,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA91M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA91M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA91                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA91RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MCA_RTGA91.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCB
       ;;
(GA001MCB)
       m_CondExec 04,GE,GA001MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA92 : TABLE REGROUP DES RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA92                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCD PGM=DSNUTILB   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCD
       ;;
(GA001MCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA92M  : NAME=RSGA92M,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA92M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA92M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA92                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA92RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MCD_RTGA92.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCE
       ;;
(GA001MCE)
       m_CondExec 04,GE,GA001MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA93 : TABLE TOTALISA DE RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTGA93                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCG PGM=DSNUTILB   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCG
       ;;
(GA001MCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA93M  : NAME=RSGA93M,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA93M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA93M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTGA93                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA93RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MCG_RTGA93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCH
       ;;
(GA001MCH)
       m_CondExec 04,GE,GA001MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE LA FACTURATION INTERNE  RTFI05                                   
#     NON-MODIFIABLES PAR LES FILIALES  P989.RTFI05                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCJ PGM=DSNUTILB   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCJ
       ;;
(GA001MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSFI05M  : NAME=RSFI05M,MODE=(U,N) - DYNAM=YES                            
# -X-RSFI05M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFI05M /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P989.RTFI05                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PFI0/F07.UNLOAD.FI05UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MCJ_RTFI05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCK
       ;;
(GA001MCK)
       m_CondExec 04,GE,GA001MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE RTPM06 : TABLE DES MODE DE PAIEMENT NEM                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCM
       ;;
(GA001MCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES LOIS DE REAPPRO                                            
#    RSPM06M  : NAME=RSPM06M,MODE=(U,N) - DYNAM=YES                            
# -X-RSPM06M  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSPM06M /dev/null
# ******* FIC DE LOAD VENANT DE GA001P                                         
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.PM06RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001M_GA001MCM_RTPM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCN
       ;;
(GA001MCN)
       m_CondExec 04,GE,GA001MCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES MODES DE D�LIVRANCE NATIONAUX SUR FILIALE NON PR�SENTS S         
#            DIF POUR CHAQUE FAMILLE                                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCQ
       ;;
(GA001MCQ)
       m_CondExec ${EXAEB},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13M  : NAME=RSGA13M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13M /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01M /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCQ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCR
       ;;
(GA001MCR)
       m_CondExec 04,GE,GA001MCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   INSERT DES MODES DE D�LIVRANCE NATIONAUX DE DIF NON PR�SENTS SUR           
#   FILIALE, POUR CHAQUE FAMILLE                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001MCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GA001MCT
       ;;
(GA001MCT)
       m_CondExec ${EXAEG},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13M  : NAME=RSGA13M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13M /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01M /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001MCT.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001MCU
       ;;
(GA001MCU)
       m_CondExec 04,GE,GA001MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
