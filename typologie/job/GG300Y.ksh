#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GG300Y.ksh                       --- VERSION DU 09/10/2016 00:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGG300 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/08 AT 12.03.11 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GG300Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#   RECUP DES PARAMETRES DE SAISIE PARAMETRE EDITION TP                        
#   REPRISE : OUI MAIS ATTENTION : MAJ DE LA MTXTFIX5 MEMBRE(BGG300Y)          
#                                                                              
# ********************************************************************         
# AAA      STEP  PGM=EX0100,LANG=ASM                                           
#                                                                              
# //TABLE    DD DSN=CORTEX4.P.MTXTFIX5(BGG300Y),DISP=SHR                       
# EX0100   FILE  NAME=EX0000AY,MODE=U                                          
# //PARAM    DD DSN=CORTEX4.P.MTXTFIX5,DISP=SHR                                
# SYSPRINT REPORT SYSOUT=X                                                     
# SNAPOUT  REPORT SYSOUT=X                                                     
# SYSIN    DATA  PARMS=EX0100P1,CLASS=VAR                                      
# ********************************************************************         
#  BGG300 : EXTRACTION DES DEMAMDES D'EDITION DES ECARTS PRMP PRA              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GG300YA
       ;;
(GG300YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GG300YAA
       ;;
(GG300YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE GENERALISEE : SOUS TABLE ECPRA (ECART DES PRA)                 
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  FAMILLES                                                             
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50Y  : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50Y /dev/null
# ******  HISTORIQUES,TABLE DES MVTS DE RECEPTION                              
#    RSGG70Y  : NAME=RSGG70Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG70Y /dev/null
# ******  STOCKS ENTREPOT                                                      
#    RSGS10Y  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10Y /dev/null
# ******  STOCKS MAGASINS                                                      
#    RSGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  ANOMALIES                                                            
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 945                                              
       m_FileAssign -i FNSOC
$DRADEP_1_3
_end
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE DU JOUR D'EDITION : LE 25 DU MOIS                          
       m_FileAssign -d SHR FPARAM1 ${DATA}/CORTEX4.P.MTXTFIX1/GG300YAA
# ******  PARAMETRE DE TRAITEMENT : OUI OU NON DE EX0100                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/BGG300AY
#                                                                              
# ******  FICHIER D'EXTRACTION DES DEMANDES D'EDITION                          
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 FGG300 ${DATA}/PXX0/GG300YAA.BGG300AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG300 
       JUMP_LABEL=GG300YAB
       ;;
(GG300YAB)
       m_CondExec 04,GE,GG300YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION BGG300AY                                        
#           1,3,PD,A : N� DE SEQUENCE FAMILLE                                  
#           4,12,CH,A: MARQUE CODIC                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG300YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GG300YAD
       ;;
(GG300YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GG300YAA.BGG300AY
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PXX0/GG300YAD.BGG300BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_12 4 CH 12
 /FIELDS FLD_PD_1_3 1 PD 3
 /KEYS
   FLD_PD_1_3 ASCENDING,
   FLD_CH_4_12 ASCENDING
 /* Record Type = F  Record Length = 75 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GG300YAE
       ;;
(GG300YAE)
       m_CondExec 00,EQ,GG300YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGG310 : EDITION DES ECARTS ENTRE PRMP PRA PAR CODIC                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG300YAG PGM=BGG310     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GG300YAG
       ;;
(GG300YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION TRI�                                            
       m_FileAssign -d SHR -g ${G_A2} FGG300 ${DATA}/PXX0/GG300YAD.BGG300BY
# ******  EDITION DES ECARTS ENTRE PRMP ET PRA PAR CODIC ET FAMILLE            
       m_OutputAssign -c 9 -w JGG310 JGG310
       m_ProgramExec BGG310 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GG300YZA
       ;;
(GG300YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GG300YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
