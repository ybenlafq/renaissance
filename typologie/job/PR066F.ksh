#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PR066F.ksh                       --- VERSION DU 09/10/2016 00:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFPR066 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/06/03 AT 09.23.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: PR066F                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   DELETE DES FICHIERS SéQUENTIELS FPR066                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PR066FA
       ;;
(PR066FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=PR066FAA
       ;;
(PR066FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
# **************************************                                       
       m_FileAssign -d SHR FPR066 ${DATA}/PXX0.F99.BPR066AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR066FAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PR066FAB
       ;;
(PR066FAB)
       m_CondExec 16,NE,PR066FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPR066  EXTRACTION DES VENTES INTERNET                                
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR066FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PR066FAD
       ;;
(PR066FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES DIF EN LECTURE                                                
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA06   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
# ******* TABLES DN EN LECTURE                                                 
#    RSGV02   : NAME=RSGV02R,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10R,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11R,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLES DRA EN LECTURE                                                
#    RSGV02   : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLES DAL EN LECTURE                                                
#    RSGV02   : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLES D.OUEST EN LECTURE                                            
#    RSGV02   : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLES DPM EN LECTURE                                                
#    RSGV02   : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLES DNPC EN LECTURE                                               
#    RSGV02   : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 205 FPR066 ${DATA}/PXX0.F99.BPR066AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR066 
       JUMP_LABEL=PR066FAE
       ;;
(PR066FAE)
       m_CondExec 04,GE,PR066FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPR069  EXTRACTION DES VENTES GSM INCORRECTES POUR CELLULE GS         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01                                                  
# SYSABOUT REPORT SYSOUT=*                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******* TABLES DIF EN LECTURE                                                
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01,MODE=I                                  
# RSGA06   FILE  DYNAM=YES,NAME=RSGA06,MODE=I                                  
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02,MODE=I                                  
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11,MODE=I                                  
# RSPR00   FILE  DYNAM=YES,NAME=RSPR00,MODE=I                                  
# ******* TABLES DN EN LECTURE                                                 
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02R,MODE=I                                 
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11R,MODE=I                                 
# ******* TABLES DRA EN LECTURE                                                
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02Y,MODE=I                                 
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11Y,MODE=I                                 
# ******* TABLES DAL EN LECTURE                                                
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02M,MODE=I                                 
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11M,MODE=I                                 
# ******* TABLES DOUEST EN LECTURE                                             
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02O,MODE=I                                 
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11O,MODE=I                                 
# ******* TABLES DPM EN LECTURE                                                
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02D,MODE=I                                 
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11D,MODE=I                                 
# ******* TABLES DNPC EN LECTURE                                               
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02L,MODE=I                                 
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11L,MODE=I                                 
# ******* FICHIER FDATE                                                        
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******* FICHIERS EN SORTIE                                                   
# FPR069   FILE  NAME=BPR069AP,MODE=O                                          
#                                                                              
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BPR069) PLAN(BPR069)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
