#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TH011P.ksh                       --- VERSION DU 08/10/2016 22:22
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTH011 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/11/21 AT 13.45.54 BY BURTEC2                      
#    STANDARDS: P  JOBSET: TH011P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTH011  : MISE � JOUR DE LA RVTH1100                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TH011PA
       ;;
(TH011PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=TH011PAA
       ;;
(TH011PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***********************                                                      
#   TABLES EN LECTURE   *                                                      
# ***********************                                                      
#                                                                              
#    RTGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA67 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTTH11   : NAME=RSTH11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH11 /dev/null
#                                                                              
# ***********************                                                      
#   TABLES EN ECRITURE  *                                                      
# ***********************                                                      
#                                                                              
#    RSTH11   : NAME=RSTH11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH11 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH011 
       JUMP_LABEL=TH011PAB
       ;;
(TH011PAB)
       m_CondExec 04,GE,TH011PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * BTH012  : MISE � JOUR DE LA TH12, TH14 , TH15                              
# * REPRISE : OUI                                                              
# ********************************************************************         
# AAF      STEP  PGM=IKJEFT01,LANG=UTIL                                        
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ************************                                                     
# *  TABLES EN LECTURE   *                                                     
# ************************                                                     
# *                                                                            
# RTGA00   FILE  DYNAM=YES,NAME=RSGA00,MODE=I                                  
# RTGA38   FILE  DYNAM=YES,NAME=RSGA38,MODE=I                                  
# RTGA82   FILE  DYNAM=YES,NAME=RSGA82,MODE=I                                  
# RTTH01   FILE  DYNAM=YES,NAME=RSTH01,MODE=I                                  
# RTTH10   FILE  DYNAM=YES,NAME=RSTH10,MODE=I                                  
# RTTH11   FILE  DYNAM=YES,NAME=RSTH11,MODE=I                                  
# *                                                                            
# ************************                                                     
# *  TABLES EN ECRITURE  *                                                     
# ************************                                                     
# *                                                                            
# RTTH12   FILE  DYNAM=YES,NAME=RSTH12,MODE=I                                  
# RTTH14   FILE  DYNAM=YES,NAME=RSTH14,MODE=I                                  
# *                                                                            
# RTTH12   FILE  DYNAM=YES,NAME=RSTH12,MODE=(U,N),REST=NO                      
# ******* CARTE DATE.                                                          
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# *                                                                            
# ******* FICHIER PARAM                                                        
# FPARAM2  FILE  NAME=FTH012AP,MODE=I                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BTH012) PLAN(BTH012)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
