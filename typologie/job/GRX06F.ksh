#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX06F.ksh                       --- VERSION DU 20/10/2016 12:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGRX06 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 19.13.12 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GRX06F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRANSFORMATION DU CSV EN FICHIER PLAT                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX06FA
       ;;
(GRX06FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRX06FAA
       ;;
(GRX06FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0/FTP.F99.FGRX06AF
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.FGRX06BF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY
/FIELDS
    FLD_01  1:  -  1: EN,
    FLD_02  2:  -  2: EN,
    FLD_03  3:  -  3: EN,
    FLD_04  4:  -  4: EN,
    FLD_05  5:  -  5: EN,
    FLD_06  6:  -  6: EN,
    FLD_07  7:  -  7: EN,
    FLD_08  8:  -  8:,
    FLD_09  9:  -  9:,
    FLD_10 10:  - 10:,
    FLD_11 11:  - 11: EN,
    FLD_12 12:  - 12: EN
    /DERIVEDFIELD extract_FLD_01 FLD_01  extract /[0-9]+/
    /DERIVEDFIELD build_FLD_01   extract_FLD_01   (9999999)
    /DERIVEDFIELD extract_FLD_02   FLD_02 extract /[0-9]+/
    /DERIVEDFIELD build_FLD_02   extract_FLD_02   (999)
    /DERIVEDFIELD extract_FLD_03   FLD_03 extract  /[0-9]+/
    /DERIVEDFIELD build_FLD_03   extract_FLD_03   (999)
    /DERIVEDFIELD extract_FLD_04   FLD_04  extract  /[0-9]+/
    /DERIVEDFIELD build_FLD_04  extract_FLD_04   (9999)
    /DERIVEDFIELD extract_FLD_05   FLD_05 extract /[0-9]+/
    /DERIVEDFIELD build_FLD_05   extract_FLD_05   (99)
    /DERIVEDFIELD extract_FLD_06   FLD_06  extract /[0-9]+/
    /DERIVEDFIELD build_FLD_06   extract_FLD_06  (99999999)
    /DERIVEDFIELD extract_FLD_07   FLD_07   extract /[0-9]+/
    /DERIVEDFIELD build_FLD_07   extract_FLD_07   (9999999)
    /DERIVEDFIELD build_FLD_08   FLD_08  20
    /DERIVEDFIELD build_FLD_09   FLD_09   5
    /DERIVEDFIELD build_FLD_10   FLD_10   5
    /DERIVEDFIELD build_FLD_11   FLD_11   (999999999)
    /DERIVEDFIELD build_FLD_12   FLD_12   (999999)
    /PADBYTE " "
/MT_INFILE_SORT  SORTIN  fieldseparator ";"
/REFORMAT
  build_FLD_01, build_FLD_02,
  build_FLD_03, build_FLD_04, build_FLD_05,
  build_FLD_06, build_FLD_07, build_FLD_08,
  build_FLD_09, build_FLD_10, build_FLD_11,
  build_FLD_12
 /* Record Type = F  Record Length = 79 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX06FAB
       ;;
(GRX06FAB)
       m_CondExec 00,EQ,GRX06FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ECLATEMENT DES FICHIERS                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX06FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRX06FAD
       ;;
(GRX06FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F99.FGRX06BF
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOF1 ${DATA}/PXX0/F91.FGRX06AD
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOF2 ${DATA}/PXX0/F61.FGRX06AL
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOF3 ${DATA}/PXX0/F89.FGRX06AM
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOF4 ${DATA}/PXX0/F16.FGRX06AO
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOF5 ${DATA}/PXX0/F07.FGRX06AP
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOF6 ${DATA}/PXX0/F45.FGRX06AY
# ****************************************************************             
#  SAUVEGARDE DU FICHIER FGRX06AF EN FSOV06AF (10 GENERATIONS)                 
#  LE LENDEMAIN                                                                
# ****************************************************************             
#  REPRISE : OUI                                                               
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GRX06FAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_20 "945"
 /DERIVEDFIELD CST_1_14 "916"
 /DERIVEDFIELD CST_1_5 "991"
 /DERIVEDFIELD CST_1_17 "907"
 /DERIVEDFIELD CST_1_11 "989"
 /DERIVEDFIELD CST_1_8 "961"
 /FIELDS FLD_CH_28_7 28 CH 7
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_5 FLD_CH_8_3 EQ CST_1_17 
 /CONDITION CND_4 FLD_CH_8_3 EQ CST_1_14 
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_5 
 /CONDITION CND_6 FLD_CH_8_3 EQ CST_1_20 
 /CONDITION CND_3 FLD_CH_8_3 EQ CST_1_11 
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_8 
 /KEYS
   FLD_CH_1_13 ASCENDING,
   FLD_CH_28_7 ASCENDING
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /INCLUDE CND_3
 /MT_OUTFILE_SUF 4
 /INCLUDE CND_4
 /MT_OUTFILE_SUF 5
 /INCLUDE CND_5
 /MT_OUTFILE_SUF 6
 /INCLUDE CND_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRX06FAG
       ;;
(GRX06FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/FTP.F99.FGRX06AF
       m_FileAssign -d NEW,CATLG,DELETE -r 91 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F99.FGRX06AF.SOV
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX06FAG.sysin
       m_UtilityExec
# ****************************************************************             
# *  DELETE DU FICHIER CUMUL                                   ***             
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GRX06FAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRX06FAJ
       ;;
(GRX06FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX06FAJ.sysin
       m_UtilityExec
# ****************************************************************             
#  CREATION FICHIER VIDE                                                       
# ****************************************************************             
#  REPRISE : OUI                                                               
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GRX06FAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRX06FAM
       ;;
(GRX06FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 91 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.FGRX06AF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX06FAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GRX06FAN
       ;;
(GRX06FAN)
       m_CondExec 16,NE,GRX06FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRX002                                                                
#  -------------                                                               
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX06FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRX06FAQ
       ;;
(GRX06FAQ)
       m_CondExec ${EXAAZ},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#  PARAMETRE DATE                                                              
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.PARAM.GRX06F.R$[RUN]/FDATE
# ******  PARAM SOCIETE                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE EN ECRITURE                                                    
#    RSRX20   : NAME=RSRX20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRX20 /dev/null
#    RSRX22   : NAME=RSRX22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRX22 /dev/null
#    RSRX25   : NAME=RSRX25,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRX25 /dev/null
#                                                                              
# FICHIER EN ENTREE                                                            
       m_FileAssign -d SHR -g ${G_A2} FRX002 ${DATA}/PXX0/F07.FGRX06AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX002 
       JUMP_LABEL=GRX06FAR
       ;;
(GRX06FAR)
       m_CondExec 04,GE,GRX06FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
