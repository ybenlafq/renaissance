#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BS001D.ksh                       --- VERSION DU 09/10/2016 00:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDBS001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/02 AT 10.19.37 BY BURTEC3                      
#    STANDARDS: P  JOBSET: BS001D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    DELETE DU FICHIER NON GDG AVANT  RECREATION PAR PGM                       
#    REPRISE : OUI                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BS001DA
       ;;
(BS001DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BS001DAA
       ;;
(BS001DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d OLD,KEEP,DELETE DD1 ${DATA}/PXX0.F91.BBS001BD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS001DAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BS001DAB
       ;;
(BS001DAB)
       m_CondExec 16,NE,BS001DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS001 : INTECFACE COMPTABLE DES COMMISSIONS _A RECEVOIR                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS001DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BS001DAD
       ;;
(BS001DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSPR00   : NAME=RSPR00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR21   : NAME=RSPR21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR21 /dev/null
#    RSPR22   : NAME=RSPR22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR22 /dev/null
#    RSPR24   : NAME=RSPR24,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR24 /dev/null
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSPT03   : NAME=RSPT03D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT03 /dev/null
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/BS001DAD
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 BBS001O ${DATA}/PTEM/BS001DAD.BBS001OD
# ******* FICHIER POUR LA DAFTEL VERS DXGSMP (FINANCEMENTS ABONNEMENTS         
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ BBS001P ${DATA}/PXX0.F91.BBS001BD
# ***                                                                          
       m_OutputAssign -c 9 -w IFX001 IFX001
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS001 
       JUMP_LABEL=BS001DAE
       ;;
(BS001DAE)
       m_CondExec 04,GE,BS001DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS002 : FICHIER _A ENVOYER VERS L ICS SI DFO OK                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS001DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BS001DAG
       ;;
(BS001DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ENTREE                                                     
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d SHR -g ${G_A1} BBS002I ${DATA}/PTEM/BS001DAD.BBS001OD
# ******* FICHIER POUR LA CHAINE FTICSD                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 BBS002O ${DATA}/PXX0/F91.BBS001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS002 
       JUMP_LABEL=BS001DAH
       ;;
(BS001DAH)
       m_CondExec 04,GE,BS001DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP BS001DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BS001DAJ
       ;;
(BS001DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  AJOUT PAR PHIL LE 20/09/2011                                                
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F91.BBS001AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.BBS002AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "00999"
 /FIELDS FLD_CH_29_5 29 CH 5
 /FIELDS FLD_CH_1_15 01 CH 15
 /CONDITION CND_1 FLD_CH_29_5 EQ CST_1_4 
 /KEYS
   FLD_CH_1_15 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BS001DAK
       ;;
(BS001DAK)
       m_CondExec 00,EQ,BS001DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BS001DZA
       ;;
(BS001DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS001DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
