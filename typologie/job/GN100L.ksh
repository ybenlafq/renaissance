#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GN100L.ksh                       --- VERSION DU 08/10/2016 12:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGN100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/04 AT 16.08.05 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GN100L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGN100  GESTION DES PRIX LOCAUX                                       
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GN100LA
       ;;
(GN100LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=GN100LAA
       ;;
(GN100LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  APRES PURGE DES TABLES DESTOCK.     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
# ******  TABLE SYST�ME POUR PERMETTRE AMELIORATION BGN100                     
#    QTPSEPX  : NAME=QTPSEPXL,MODE=U - DYNAM=YES                               
       m_FileAssign -d SHR QTPSEPX /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GN100LAA.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GN100LAB
       ;;
(GN100LAB)
       m_CondExec 04,GE,GN100LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGN100  GESTION DES PRIX LOCAUX                                       
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GN100LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GN100LAD
       ;;
(GN100LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************* TABLES EN LECTURE                                              
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA58   : NAME=RSGA58L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTGG05   : NAME=RSGG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG05 /dev/null
#    RTRX00   : NAME=RSRX00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTRX00 /dev/null
#    RTSP15   : NAME=RSSP15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTSP15 /dev/null
# ************ TABLES EN M.A.J                                                 
#    RTGA00   : NAME=RSGA00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA59   : NAME=RSGA59L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA68   : NAME=RSGA68L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA79   : NAME=RSGA79L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA79 /dev/null
#    RTGG20   : NAME=RSGG20L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGG20 /dev/null
#    RTHV15   : NAME=RSHV15L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTHV15 /dev/null
# ************ TABLES EN LECTURE (LOGISTIQUE)                                  
#    RTGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA67 /dev/null
#    RTGN15   : NAME=RSGN15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN15 /dev/null
#    RTGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN59 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ************ TABLES EN M.A.J (LOGISTIQUE)                                    
#    RTFL40   : NAME=RSFL40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *****                                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGN100 
       JUMP_LABEL=GN100LAE
       ;;
(GN100LAE)
       m_CondExec 04,GE,GN100LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGN200 DESCENTE DE LA PRIME DE REF NAT + COEF ZP + COEF FAMIL         
#        RECALCUL DES PRIMES DE REF ZONE DE PRIX SI TOPMAJ COEF FAMILL         
#        = 'O' MISE A ZERO DE LA PRIME POUR LES EPUISES                        
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GN100LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GN100LAG
       ;;
(GN100LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************* TABLES EN LECTURE                                              
#    RTGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA35   : NAME=RSGA35L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA35 /dev/null
#    RTGA58   : NAME=RSGA58L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA68   : NAME=RSGA68L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA79   : NAME=RSGA79L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA79 /dev/null
#    RTGA83   : NAME=RSGA83L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA83 /dev/null
#    RTGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGI12   : NAME=RSGI12L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGI12 /dev/null
#    RTGI14   : NAME=RSGI14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGI14 /dev/null
#    RTGJ10   : NAME=RSGJ10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGJ10 /dev/null
# ************ TABLES EN LECTURE (LOGISTIQUE)                                  
#    RTGI25   : NAME=RSGI25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGI25 /dev/null
#    RTGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN59 /dev/null
#    RTGN75   : NAME=RSGN75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN75 /dev/null
# ************ TABLES EN M.A.J                                                 
#    RTHV15   : NAME=RSHV15L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTHV15 /dev/null
#    RTGA75   : NAME=RSGA75L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *****                                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGN200 
       JUMP_LABEL=GN100LAH
       ;;
(GN100LAH)
       m_CondExec 04,GE,GN100LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
