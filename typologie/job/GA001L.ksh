#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA001L.ksh                       --- VERSION DU 09/10/2016 05:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGA001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/03 AT 11.01.11 BY BURTECA                      
#    STANDARDS: P  JOBSET: GA001L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='DNPC'                                                              
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
# ********************************************************************         
#     LOAD DES DONNEES ARTICLES     (RTGA07 : GROUPE-FOURNISSEURS)             
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA07                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA001LA
       ;;
(GA001LA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/02/03 AT 11.01.11 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GA001L                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'LOAD T.ARTICLES'                       
# *                           APPL...: IMPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA001LAA
       ;;
(GA001LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GROUPE FOURNISSEURS                                            
#    RSGA07L  : NAME=RSGA07L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA07L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA07                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA07RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAA_RTGA07.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAB
       ;;
(GA001LAB)
       m_CondExec 04,GE,GA001LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA14 : FAMILLES)                        
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA14                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAD
       ;;
(GA001LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES                                                   
#    RSGA14L  : NAME=RSGA14L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA14                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA14RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAD_RTGA14.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAE
       ;;
(GA001LAE)
       m_CondExec 04,GE,GA001LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA15 : FAMILLE/TYPE DECLARAT�)          
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA15                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAG
       ;;
(GA001LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR TYPE DE DECLARATIONS                          
#    RSGA15L  : NAME=RSGA15L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA15L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA15                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA15RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAG_RTGA15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAH
       ;;
(GA001LAH)
       m_CondExec 04,GE,GA001LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA16 : FAMILLE/GARANTIE)                
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA16                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAJ
       ;;
(GA001LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR GARANTIE-COMPLEMENTAIRES                      
#    RSGA16L  : NAME=RSGA16L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA16L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA16                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA16RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAJ_RTGA16.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAK
       ;;
(GA001LAK)
       m_CondExec 04,GE,GA001LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA18 : FAMILLE/CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA18                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAM
       ;;
(GA001LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR CODE-DESCRIPTIFS                              
#    RSGA18L  : NAME=RSGA18L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA18L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA18                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA18RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAM_RTGA18.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAN
       ;;
(GA001LAN)
       m_CondExec 04,GE,GA001LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA20 : CARACTERISTIC SPECIFIC)          
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA20                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAQ
       ;;
(GA001LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CARACTERISTIQUES SPECIFIQUES                               
#    RSGA20L  : NAME=RSGA20L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA20L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA20                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA20RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAQ_RTGA20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAR
       ;;
(GA001LAR)
       m_CondExec 04,GE,GA001LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA21 : FAMILLE/RAYON )                  
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA21                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAT
       ;;
(GA001LAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLE PAR RAYON                                          
#    RSGA21L  : NAME=RSGA21L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA21L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA21                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA21RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAT_RTGA21.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAU
       ;;
(GA001LAU)
       m_CondExec 04,GE,GA001LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA22 : COMPOSANTS/RAYON)                
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA22                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GA001LAX
       ;;
(GA001LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES COMPOSANT/RAYON                                            
#    RSGA22L  : NAME=RSGA22L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA22L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA22                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA22RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LAX_RTGA22.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LAY
       ;;
(GA001LAY)
       m_CondExec 04,GE,GA001LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA23 : CODE MARKETING )                 
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA23                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBA
       ;;
(GA001LBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES MARKETING                                            
#    RSGA23L  : NAME=RSGA23L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA23L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA23                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA23RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBA_RTGA23.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBB
       ;;
(GA001LBB)
       m_CondExec 04,GE,GA001LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA24 : CODE DESCRIPTIFS)                
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA24                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBD
       ;;
(GA001LBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES DESCRIPTIFS                                          
#    RSGA24R  : NAME=RSGA24L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA24R /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA24                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA24RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBD_RTGA24.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBE
       ;;
(GA001LBE)
       m_CondExec 04,GE,GA001LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA25 : ASSO MARKETING DECRIPT)          
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA25                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBG
       ;;
(GA001LBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES ASSO MARKETING-DESCRIPTIFS                                 
#    RSGA25L  : NAME=RSGA25L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA25L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA25                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA25RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBG_RTGA25.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBH
       ;;
(GA001LBH)
       m_CondExec 04,GE,GA001LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA26 : VALEURS CODE MAKETING)           
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA26                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBJ
       ;;
(GA001LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-MARKETING                                     
#    RSGA26L  : NAME=RSGA26L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA26                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA26RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBJ_RTGA26.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBK
       ;;
(GA001LBK)
       m_CondExec 04,GE,GA001LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA27 : VALEURS CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA27                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBM
       ;;
(GA001LBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-DESCRIPTIFS                                   
#    RSGA27L  : NAME=RSGA27L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA27L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA27                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA27RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBM_RTGA27.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBN
       ;;
(GA001LBN)
       m_CondExec 04,GE,GA001LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA71 : TABLE DES TABLES)                
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA71                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBQ
       ;;
(GA001LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES TABLES                                                     
#    RSGA71L  : NAME=RSGA71L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA71L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA71                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA71RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBQ_RTGA71.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBR
       ;;
(GA001LBR)
       m_CondExec 04,GE,GA001LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA72 : TABLE DES BORNES)                
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA72                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBT
       ;;
(GA001LBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES BORNES                                                     
#    RSGA72L  : NAME=RSGA72L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA72L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA72                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA72RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBT_RTGA72.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBU
       ;;
(GA001LBU)
       m_CondExec 04,GE,GA001LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA99 : TABLE DES MESSAGES)              
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA99                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LBX PGM=DSNUTILB   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GA001LBX
       ;;
(GA001LBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA99L  : NAME=RSGA99L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA99L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA99                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA99RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LBX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LBX_RTGA99.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LBY
       ;;
(GA001LBY)
       m_CondExec 04,GE,GA001LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA91 : TABLE DES RUBRIQUES)             
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA91                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCA
       ;;
(GA001LCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA91L  : NAME=RSGA91L,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA91L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA91L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA91                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA91RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LCA_RTGA91.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCB
       ;;
(GA001LCB)
       m_CondExec 04,GE,GA001LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA92 : TABLE REGROUP DES RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA92                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCD PGM=DSNUTILB   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCD
       ;;
(GA001LCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA92L  : NAME=RSGA92L,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA92L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA92L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA92                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA92RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LCD_RTGA92.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCE
       ;;
(GA001LCE)
       m_CondExec 04,GE,GA001LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA93 : TABLE TOTALISA DE RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTGA93                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCG PGM=DSNUTILB   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCG
       ;;
(GA001LCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA93L  : NAME=RSGA93L,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA93L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA93L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTGA93                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA93RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LCG_RTGA93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCH
       ;;
(GA001LCH)
       m_CondExec 04,GE,GA001LCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE LA FACTURATION INTERNE  RTFI05                                   
#     NON-MODIFIABLES PAR LES FILIALES  P961.RTFI05                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCJ PGM=DSNUTILB   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCJ
       ;;
(GA001LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSFI05L  : NAME=RSFI05L,MODE=(U,N) - DYNAM=YES                            
# -X-RSFI05L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFI05L /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P961.RTFI05                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PFI0/F07.UNLOAD.FI05UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LCJ_RTFI05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCK
       ;;
(GA001LCK)
       m_CondExec 04,GE,GA001LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE RTPM06 : TABLE DES MODE DE PAIEMENT NEM                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCM
       ;;
(GA001LCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES LOIS DE REAPPRO                                            
#    RSPM06L  : NAME=RSPM06L,MODE=(U,N) - DYNAM=YES                            
# -X-RSPM06L  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSPM06L /dev/null
# ******* FIC DE LOAD VENANT DE GA001P                                         
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.PM06RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001L_GA001LCM_RTPM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCN
       ;;
(GA001LCN)
       m_CondExec 04,GE,GA001LCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES MODES DE D�LIVRANCE NATIONAUX SUR FILIALE NON PR�SENTS S         
#            DIF POUR CHAQUE FAMILLE                                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCQ
       ;;
(GA001LCQ)
       m_CondExec ${EXAEB},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13L  : NAME=RSGA13L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13L /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01L /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCQ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCR
       ;;
(GA001LCR)
       m_CondExec 04,GE,GA001LCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   INSERT DES MODES DE D�LIVRANCE NATIONAUX DE DIF NON PR�SENTS SUR           
#   FILIALE, POUR CHAQUE FAMILLE                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001LCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GA001LCT
       ;;
(GA001LCT)
       m_CondExec ${EXAEG},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13L  : NAME=RSGA13L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13L /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01L /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001LCT.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001LCU
       ;;
(GA001LCU)
       m_CondExec 04,GE,GA001LCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
