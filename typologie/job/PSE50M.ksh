#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PSE50M.ksh                       --- VERSION DU 09/10/2016 05:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPSE50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/30 AT 09.57.15 BY BURTECA                      
#    STANDARDS: P  JOBSET: PSE50M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BPS500 :                                                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PSE50MA
       ;;
(PSE50MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PSE50MAA
       ;;
(PSE50MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLES                                                             
#    RSPS04   : NAME=RSPS04M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS04 /dev/null
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE MOIS                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 900 -t LSEQ -g +1 FPS501 ${DATA}/PXX0/F89.PSE050AM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 900 -t LSEQ -g +1 FPS502 ${DATA}/PXX0/F89.PSE050BM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI00 ${DATA}/PXX0/F89.PSE050DM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS500 
       JUMP_LABEL=PSE50MAB
       ;;
(PSE50MAB)
       m_CondExec 04,GE,PSE50MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FFTI00                                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE50MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PSE50MAD
       ;;
(PSE50MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F89.PSE050DM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.PSE050CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_64_8 64 PD 8
 /FIELDS FLD_BI_1_63 01 CH 63
 /FIELDS FLD_BI_72_329 72 CH 329
 /KEYS
   FLD_BI_1_63 ASCENDING,
   FLD_BI_72_329 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_64_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PSE50MAE
       ;;
(PSE50MAE)
       m_CondExec 00,EQ,PSE50MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
