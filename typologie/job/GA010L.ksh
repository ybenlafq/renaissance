#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA010L.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGA010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/15 AT 11.30.07 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GA010L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DNPC'                                                              
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
#  B G A 0 0 1                                                                 
# ********************************************************************         
#   CREATION DES RELATIONS EXPOSITION MAGASIN FAMILLE                          
#   MAJ TABLE RELATION FAMILLE EXPOSITION MAGASIN (RTGA19)                     
#   LECTURE DE LA TABLE RTGA14                                                 
#   REPRISE: OUI SI FIN ANORMALE NON SI REPRISE APRES FIN NORMALE              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA010LA
       ;;
(GA010LA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2001/02/15 AT 11.30.06 BY BURTEC5                
# *    JOBSET INFORMATION:    NAME...: GA010L                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXPO MAGASIN/FAM'                      
# *                           APPL...: REPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA010LAA
       ;;
(GA010LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    CREATION PARAMETRES               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FAMILLES                                                   
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ******* TABLE RELATION FAMILLE / EXPO MAGASIN                                
#    RSGA19L  : NAME=RSGA19,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19L /dev/null
# ******  TABLE RELATION ARTICLE / ASSORTIMENT MAGASIN                         
#    RSGA54L  : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54L /dev/null
       m_FileAssign -d SHR PARAMEX ${DATA}/CORTEX4.P.MTXTFIX5/BGA001L
       m_OutputAssign -c "*" IMPRIM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA001 
       JUMP_LABEL=GA010LAB
       ;;
(GA010LAB)
       m_CondExec 04,GE,GA010LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
