#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PTF30L.ksh                       --- VERSION DU 08/10/2016 13:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLPTF30 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/02/02 AT 10.27.38 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PTF30L                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  BTF032 : PURGE DES TABLES APPC CARTE-T RTTF30/35                            
#           CRITERE :  N�SEQ DE RTTF35 = NB TOTAL DE RTTF30                    
#                      ET DONT LA DATE CREATION = FDATE - DELAI(FPARAM         
#           DELAI   :  015 NB JOURS                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PTF30LA
       ;;
(PTF30LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PTF30LAA
       ;;
(PTF30LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* APPC HOST <--> MICRO                                                 
#    RSTF30L  : NAME=RSTF30L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTF30L /dev/null
# ******* CONTROLE APPC HOST <--> MICRO                                        
#    RSTF35L  : NAME=RSTF35L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTF35L /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DELAI DE PURGE : FDATE - 015                                         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PTF30LAA
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF032 
       JUMP_LABEL=PTF30LAB
       ;;
(PTF30LAB)
       m_CondExec 04,GE,PTF30LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
