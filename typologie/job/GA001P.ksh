#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA001P.ksh                       --- VERSION DU 09/10/2016 05:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGA001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/31 AT 11.21.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: GA001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='FILIALES'                                                          
# ********************************************************************         
#  BFL002                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA001PA
       ;;
(GA001PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2015/08/31 AT 11.21.36 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GA001P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'UNLOAD T.ARTICLES'                     
# *                           APPL...: IMPFILIA                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA001PAA
       ;;
(GA001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLES                                                             
#    RSFL50   : NAME=RSFL50,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSFL50 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL002 
       JUMP_LABEL=GA001PAB
       ;;
(GA001PAB)
       m_CondExec 04,GE,GA001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES ARTICLES NCP (RTFL14 : FAMILLES/ENTREPOTS)               
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA07 : GROUPE-FOURNISSEURS)              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA09 : SEG POUR ETAT IMU205)             
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA10 : LIEUX)                            
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA12 : CODES MARKETING IMU205)           
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA14 : FAMILLES)                         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA15 : FAMILLES/TYPE DECLAR�)            
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA16 : FAMILLES/GRANTIES)                
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA18 : FAMILLES/CODE DESCRIPT)           
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA20 : CARACTERISTIC SPECIFS)            
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA21 : FAMILLE/RAYON)                    
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA22 : COMPOSANT/RAYON )                 
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA23 : CODES MARKETING )                 
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA24 : CODES DESCRIPTIFS )               
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA25 : MARKET/DESCRIPTIFS )              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA26 : VALEURS CODE MARKETING)           
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA27 : VALEURS CODE DESCRIPTIF           
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA29 : AGREGAT MARKETING IMU205)         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA71 : TABLE DES TABLES)                 
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA72 : BORNES )                          
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA99 : MESSAGES )                        
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA91 : RUBRIQUES)                        
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA92 : REGROUPEMENT ET RUBRIQUES         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA93 : TOTALISATION DE RUBRIQUES         
#  UNLOAD DES DONNEES FACTURATIONS INTERNES . TABLE RTFI05 (PCF)               
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA06 : ENTITE DE COMMANDE)               
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA30 : PARAM ASSOCIES AUX FAMILL         
#  UNLOAD DES DONNEES ARTICLES NCP (RTTL03 : CODES DES LIVREURS)               
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAD
       ;;
(GA001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F99.RELOAD.FL14RF
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC02 ${DATA}/PXX0/F99.RELOAD.GA07RF
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SYSREC03 ${DATA}/PXX0/F99.RELOAD.GA09RF
       m_FileAssign -d NEW,CATLG,DELETE -r 306 -t LSEQ -g +1 SYSREC04 ${DATA}/PXX0/F99.RELOAD.GA10RF
       m_FileAssign -d NEW,CATLG,DELETE -r 24 -t LSEQ -g +1 SYSREC05 ${DATA}/PXX0/F99.RELOAD.GA12RF
       m_FileAssign -d NEW,CATLG,DELETE -r 59 -t LSEQ -g +1 SYSREC06 ${DATA}/PXX0/F99.RELOAD.GA14RF
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SYSREC07 ${DATA}/PXX0/F99.RELOAD.GA15RF
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -t LSEQ -g +1 SYSREC08 ${DATA}/PXX0/F99.RELOAD.FL14R3F
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAD.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAG
       ;;
(GA001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F99.RELOAD.GA16RF
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 SYSREC02 ${DATA}/PXX0/F99.RELOAD.GA18RF
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -t LSEQ -g +1 SYSREC03 ${DATA}/PXX0/F99.RELOAD.GA20RF
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 SYSREC04 ${DATA}/PXX0/F99.RELOAD.GA21RF
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -t LSEQ -g +1 SYSREC05 ${DATA}/PXX0/F99.RELOAD.GA22RF
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SYSREC06 ${DATA}/PXX0/F99.RELOAD.GA23RF
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SYSREC07 ${DATA}/PXX0/F99.RELOAD.GA24RF
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SYSREC08 ${DATA}/PXX0/F99.RELOAD.GA25RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAG.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAJ
       ;;
(GA001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F99.RELOAD.GA26RF
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC02 ${DATA}/PXX0/F99.RELOAD.GA27RF
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -t LSEQ -g +1 SYSREC03 ${DATA}/PXX0/F99.RELOAD.GA29RF
       m_FileAssign -d NEW,CATLG,DELETE -r 508 -t LSEQ -g +1 SYSREC04 ${DATA}/PXX0/F99.RELOAD.GA71RF
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -t LSEQ -g +1 SYSREC05 ${DATA}/PXX0/F99.RELOAD.GA72RF
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 SYSREC06 ${DATA}/PXX0/F99.RELOAD.GA99RF
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -t LSEQ -g +1 SYSREC07 ${DATA}/PXX0/F99.RELOAD.GA91RF
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -t LSEQ -g +1 SYSREC08 ${DATA}/PXX0/F99.RELOAD.GA92RF
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -t LSEQ -g +1 SYSREC09 ${DATA}/PXX0/F99.RELOAD.GA93RF
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -t LSEQ -g +1 SYSREC10 ${DATA}/PFI0/F07.UNLOAD.FI05UF
       m_FileAssign -d NEW,CATLG,DELETE -r 143 -t LSEQ -g +1 SYSREC11 ${DATA}/PGA0/F07.UNLOAD.GA06UF
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC12 ${DATA}/PXX0/F99.RELOAD.GA30RF
       m_FileAssign -d NEW,CATLG,DELETE -r 82 -t LSEQ -g +1 SYSREC13 ${DATA}/PXX0/F99.RELOAD.TL03RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAJ.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  UNLOAD RTPM06                                                               
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAM PGM=PTLDRIVM   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAM
       ;;
(GA001PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F99.RELOAD.PM06RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD POUR LOAD RTWS31 SUR WMS                                             
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAQ
       ;;
(GA001PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# ******* FICHIER D UNLOAD                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PWMP/SEM.GA31UNF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD RTWS00 ET RTWS05 POUR ENTREPOT NATIONAL SUD (LIMONEST) : ENS         
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAT PGM=PTLDRIVM   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAT
       ;;
(GA001PAT)
       m_CondExec ${EXABE},NE,YES 
# ******* FICHIER D UNLOAD 1                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 269 -t LSEQ -g +1 SYSREC01 ${DATA}/PWMY/SEM.FL50U3F
# ******* FICHIER D UNLOAD 2                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 268 -t LSEQ -g +1 SYSREC02 ${DATA}/PWMY/SEM.FL40U3F
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAT.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGB40                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001PAX PGM=PTLDRIVM   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GA001PAX
       ;;
(GA001PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 24 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F99.RELOAD.GB40RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001PAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
