#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA010P.ksh                       --- VERSION DU 08/10/2016 12:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGA010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/04/07 AT 17.29.23 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GA010P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='CONTROLE'                                                          
# ********************************************************************         
# ********************************************************************         
#  B G A 0 0 1                                                                 
# ********************************************************************         
#   CREATION DES RELATIONS EXPOSITION MAGASIN FAMILLE                          
#   MAJ TABLE RELATION FAMILLE EXPOSITION MAGASIN (RTGA19)                     
#   LECTURE DE LA TABLE RTGA14                                                 
#   REPRISE: OUI SI FIN ANORMALE NON SI REPRISE APRES FIN NORMALE              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA010PA
       ;;
(GA010PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  1999/04/07 AT 17.29.23 BY BURTEC5                
# *    JOBSET INFORMATION:    NAME...: GA010P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXPO MAGASIN/FAM'                      
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA010PAA
       ;;
(GA010PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES FAMILLES                                                          
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#  TABLE RELATION FAMILLE / EXPO MAGASIN                                       
#    RSGA19   : NAME=RSGA19,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
# ******  TABLE RELATION ARTICLE / ASSORTIMENT MAGASIN                         
#    RSGA54   : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
       m_FileAssign -d SHR PARAMEX ${DATA}/CORTEX4.P.MTXTFIX5/BGA001P
       m_OutputAssign -c "*" IMPRIM
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA001 
       JUMP_LABEL=GA010PAB
       ;;
(GA010PAB)
       m_CondExec 04,GE,GA010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
