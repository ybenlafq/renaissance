#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PR930F.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFPR930 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/14 AT 11.07.27 BY BURTECA                      
#    STANDARDS: P  JOBSET: PR930F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    DELETE DES FICHIERS NON GDG CR�ES DANS CETTE CHAINE.                      
#    REPRISE : OUI                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PR930FA
       ;;
(PR930FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+2'}
       G_A16=${G_A16:-'+2'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+2'}
       G_A21=${G_A21:-'+2'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PR930FAA
       ;;
(PR930FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d OLD,KEEP,DELETE DD1 ${DATA}/PXX0.F99.SPB000F
       m_FileAssign -d OLD,KEEP,DELETE DD2 ${DATA}/PXX0.F99.BPR931AP
       m_FileAssign -d OLD,KEEP,DELETE DD3 ${DATA}/PXX0.F99.BPR931BP
       m_FileAssign -d OLD,KEEP,DELETE DD4 ${DATA}/PXX0.F99.SPB000G
       m_FileAssign -d OLD,KEEP,DELETE DD5 ${DATA}/PXX0.F99.BPR932BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PR930FAB
       ;;
(PR930FAB)
       m_CondExec 16,NE,PR930FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR PARIS                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAD
       ;;
(PR930FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ------  DERNIER JOUR DU MAOIS                                                
       m_FileAssign -i FINMOIS
$FMOISJ
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930 ${DATA}/PTEM/PR930FAD.BPR930AP
# ******  FICHIER EN SORTIE  NEW                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930A ${DATA}/PTEM/PR930FAD.BPR932AP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR930 
       JUMP_LABEL=PR930FAE
       ;;
(PR930FAE)
       m_CondExec 04,GE,PR930FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR MGI OUEST                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAG
       ;;
(PR930FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV02   : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FINMOIS
$FMOISJ
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930 ${DATA}/PTEM/PR930FAG.BPR930AO
# ******  FICHIER EN SORTIE  NEW                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930A ${DATA}/PTEM/PR930FAG.BPR932AO
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR930 
       JUMP_LABEL=PR930FAH
       ;;
(PR930FAH)
       m_CondExec 04,GE,PR930FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR LYON                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAJ
       ;;
(PR930FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV02   : NAME=RSGV02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FINMOIS
$FMOISJ
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930 ${DATA}/PTEM/PR930FAJ.BPR930AY
# ******  FICHIER EN SORTIE  NEW                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930A ${DATA}/PTEM/PR930FAJ.BPR932AY
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR930 
       JUMP_LABEL=PR930FAK
       ;;
(PR930FAK)
       m_CondExec 04,GE,PR930FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR LILLE                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAM
       ;;
(PR930FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV02   : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FINMOIS
$FMOISJ
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930 ${DATA}/PTEM/PR930FAM.BPR930AL
# ******  FICHIER EN SORTIE  NEW                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930A ${DATA}/PTEM/PR930FAM.BPR932AL
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR930 
       JUMP_LABEL=PR930FAN
       ;;
(PR930FAN)
       m_CondExec 04,GE,PR930FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR METZ                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAQ
       ;;
(PR930FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV02   : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FINMOIS
$FMOISJ
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930 ${DATA}/PTEM/PR930FAQ.BPR930AM
# ******  FICHIER EN SORTIE  NEW                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930A ${DATA}/PTEM/PR930FAQ.BPR932AM
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR930 
       JUMP_LABEL=PR930FAR
       ;;
(PR930FAR)
       m_CondExec 04,GE,PR930FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR MARSEILLE                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAT
       ;;
(PR930FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV02   : NAME=RSGV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR00   : NAME=RSPR00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
# ------  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FINMOIS
$FMOISJ
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930 ${DATA}/PTEM/PR930FAT.BPR930AD
# ******  FICHIER EN SORTIE  NEW                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 FPR930A ${DATA}/PTEM/PR930FAT.BPR932AD
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR930 
       JUMP_LABEL=PR930FAU
       ;;
(PR930FAU)
       m_CondExec 04,GE,PR930FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PASSAGE DU BPR930 POUR ROUEN                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABJ      STEP  PGM=IKJEFT01                                                  
# SYSABOUT REPORT SYSOUT=*                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ******  TABLES EN LECTURE                                                    
# RSGV11   FILE  DYNAM=YES,NAME=RSGV11R,MODE=I                                 
# RSGV02   FILE  DYNAM=YES,NAME=RSGV02R,MODE=I                                 
# RSGA00   FILE  DYNAM=YES,NAME=RSGA00R,MODE=I                                 
# RSGA10   FILE  DYNAM=YES,NAME=RSGA10R,MODE=I                                 
# RSPR00   FILE  DYNAM=YES,NAME=RSPR00R,MODE=I                                 
# ------  FICHIER PARAMETRE                                                    
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDN                                          
# FINMOIS  DATA  CLASS=VAR,PARMS=PR930F1                                       
# ******  FICHIER EN SORTIE                                                    
# FPR930   FILE  NAME=BPR930AR,MODE=O                                          
# ******  FICHIER EN SORTIE  NEW                                               
# FPR930A  FILE  NAME=BPR932AR,MODE=O                                          
# ******                                                                       
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BPR930) PLAN(BPR930R)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI DES FICHIERS FPR930                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PR930FAX
       ;;
(PR930FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PR930FAD.BPR930AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/PR930FAG.BPR930AO
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/PR930FAJ.BPR930AY
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/PR930FAM.BPR930AL
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/PR930FAQ.BPR930AM
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/PR930FAT.BPR930AD
#         FILE  NAME=BPR930AR,MODE=I                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PR930FAX.BPR930BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_3_3 3 CH 3
 /KEYS
   FLD_CH_3_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_60_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR930FAY
       ;;
(PR930FAY)
       m_CondExec 00,EQ,PR930FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI INCLUDE DES CONTRATS                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBA
       ;;
(PR930FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PR930FAX.BPR930BP
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PR930FBA.BPR930CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "01"
 /FIELDS FLD_CH_358_2 358 CH 2
 /FIELDS FLD_CH_1_359 1 CH 359
 /CONDITION CND_1 FLD_CH_358_2 EQ CST_1_4 
 /KEYS
   FLD_CH_1_359 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR930FBB
       ;;
(PR930FBB)
       m_CondExec 00,EQ,PR930FBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS FPR931                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBD
       ;;
(PR930FBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/PR930FAD.BPR932AP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/PR930FAG.BPR932AO
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/PR930FAJ.BPR932AY
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/PR930FAM.BPR932AL
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/PR930FAQ.BPR932AM
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/PR930FAT.BPR932AD
#         FILE  NAME=BPR932AR,MODE=I                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ SORTOUT ${DATA}/PXX0.F99.BPR932BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_9 3 CH 9
 /KEYS
   FLD_CH_3_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR930FBE
       ;;
(PR930FBE)
       m_CondExec 00,EQ,PR930FBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI INCLUDE DES CONTRATS                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBG
       ;;
(PR930FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/PR930FAX.BPR930BP
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g ${G_A15} SORTOUT ${DATA}/PTEM/PR930FBA.BPR930CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "01"
 /FIELDS FLD_CH_358_2 358 CH 2
 /FIELDS FLD_CH_1_359 1 CH 359
 /CONDITION CND_1 FLD_CH_358_2 EQ CST_1_4 
 /KEYS
   FLD_CH_1_359 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR930FBH
       ;;
(PR930FBH)
       m_CondExec 00,EQ,PR930FBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR931 :                                                                    
#  REPRISE : OUI                                                               
#       CR�ATION DU FICHIER POUR ENVOI VIA GATEWAY                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBJ PGM=BPR931     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBJ
       ;;
(PR930FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A16} FPR930 ${DATA}/PTEM/PR930FBA.BPR930CP
# ******  FICHIER SPB VERS PKZIPP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1-359 -t LSEQ FPR931 ${DATA}/PXX0.F99.BPR931AP
       m_ProgramExec BPR931 
#                                                                              
# ********************************************************************         
#  TRI INCLUDE DES CONTRATS SPB                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBM
       ;;
(PR930FBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/PR930FAX.BPR930BP
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PR930FBM.BPR930DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "02"
 /FIELDS FLD_CH_1_359 1 CH 359
 /FIELDS FLD_CH_358_2 358 CH 2
 /CONDITION CND_1 FLD_CH_358_2 EQ CST_1_4 
 /KEYS
   FLD_CH_1_359 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR930FBN
       ;;
(PR930FBN)
       m_CondExec 00,EQ,PR930FBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR932 :                                                                    
#  REPRISE : OUI                                                               
#      CR�ATION DU FICHIER POUR ENVOI SPB VIA GATEWAY                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBQ PGM=BPR932     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBQ
       ;;
(PR930FBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A18} FPR930 ${DATA}/PTEM/PR930FBM.BPR930DP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 1-359 -t LSEQ FPR931 ${DATA}/PXX0.F99.BPR931BP
       m_ProgramExec BPR932 
#                                                                              
# ********************************************************************         
#   COMPRESS DU FICHIER BPR931BP SUR SPB000F   CONTRATS SPB                    
#   REPRISE : OUI                                                              
# ********************************************************************         
# ACX      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":SPB000F"")                                                       
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":BPR931BP"",SPB000F.TXT)                                       
#  ":BPR931BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBT PGM=EZACFSM1   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBT
       ;;
(PR930FBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FBT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/PR930FBT.FPR930AF
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FBX PGM=JVMLDM76   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PR930FBX
       ;;
(PR930FBX)
       m_CondExec ${EXACX},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR DD1 ${DATA}/PXX0.F99.BPR931BP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ FICZIP ${DATA}/PXX0.F99.SPB000F
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FBX.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#   COMPRESS DU FICHIER BPR932BP SUR SPB000G   CONTRATS SPB                    
#   FICHIER AVEC LE ; A LA PLACE DE LA ,                                       
#   REPRISE : OUI                                                              
# ********************************************************************         
# ADC      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":SPB000G"")                                                       
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":BPR932BP"",SPB000G.TXT)                                       
#  ":BPR932BP""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FCA PGM=EZACFSM1   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PR930FCA
       ;;
(PR930FCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FCA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A20} SYSOUT ${DATA}/PTEM/PR930FBT.FPR930AF
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FCD PGM=JVMLDM76   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PR930FCD
       ;;
(PR930FCD)
       m_CondExec ${EXADH},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR DD1 ${DATA}/PXX0.F99.BPR932BP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 359 -t LSEQ FICZIP ${DATA}/PXX0.F99.SPB000G
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FCD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI DU FICHIER VERS SPB VIA GATEWAY                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADH      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SPB000F,                                                            
#      FNAME=":SPB000F""                                                       
#          DATAEND                                                             
# **********************                                                       
#  ENVOI DU FICHIER VERS SPB VIA GATEWAY                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADM      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SPB000G,                                                            
#      FNAME=":SPB000G""                                                       
#          DATAEND                                                             
# **********************                                                       
#  DEPENDANCE POUR PLAN                                                        
#                                                                              
#  CHANGEMENT PCL => PR930F <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTPR930F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FCG PGM=FTP        ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=PR930FCG
       ;;
(PR930FCG)
       m_CondExec ${EXADM},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FCG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP PR930FCJ PGM=EZACFSM1   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=PR930FCJ
       ;;
(PR930FCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FCJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTPR930F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR930FCM PGM=FTP        ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=PR930FCM
       ;;
(PR930FCM)
       m_CondExec ${EXADW},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FCM.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP PR930FCQ PGM=EZACFSM1   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=PR930FCQ
       ;;
(PR930FCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FCQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PR930FZA
       ;;
(PR930FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR930FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
