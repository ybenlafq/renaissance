#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PSE01Y.ksh                       --- VERSION DU 08/10/2016 22:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYPSE01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 96/04/22 AT 15.26.24 BY BURTECA                      
#    STANDARDS: P  JOBSET: PSE01Y                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  PGM : BPS600                                                                
#  ------------                                                                
#  RECONSTITUTION DE L'ECHEANCIER.APRES MAJ DES MONTANTS DE CONTRATS           
#  VENDUS,ANNULES OU REMBOURSES DE LA RTPS01                                   
#  CE PGM BPS600 VA CONSTITUER UN FICHIER ECHEANCIER FPS600                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PSE01YA
       ;;
(PSE01YA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PSE01YAA
       ;;
(PSE01YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSPS01Y  : NAME=RSPS01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPS01Y /dev/null
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
#    RSGA41Y  : NAME=RSGA41Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41Y /dev/null
#    RSGA42Y  : NAME=RSGA42Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA42Y /dev/null
# ------  PARAMETRES FMOIS ET SOC                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 FPS600 ${DATA}/PXX0/F45.FPS600AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS600 
       JUMP_LABEL=PSE01YAB
       ;;
(PSE01YAB)
       m_CondExec 04,GE,PSE01YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
