#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX50O.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGRX50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/02/18 AT 14.16.40 BY PREPA2                       
#    STANDARDS: P  JOBSET: GRX50O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG : BRX510  SELECTION DE TOUS LES RELEVES NON PRIS EN                   
#         COMPTE DEPUIS LE DERNIER TRAITEMENT ET EFFECTUE LA SYNTHESE          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX50OA
       ;;
(GRX50OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRX50OAA
       ;;
(GRX50OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FSEMAINE                                                             
       m_FileAssign -d SHR FSEMAINE ${DATA}/CORTEX4.P.MTXTFIX1/GRX50OAA
# ******* TABLE ARTICLES                                                       
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE                                                                
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* TABLE EDITION DES ETATS TABLE MARKETING                              
#    RSGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******* TABLE MARQUES (GENERALITES)                                          
#    RSGA22O  : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22O /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30O /dev/null
# ******* TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE DES CODES VALEURS MARKETING                                    
#    RSGA83O  : NAME=RSGA83O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83O /dev/null
# ******* TABLE                                                                
#    RSGG20O  : NAME=RSGG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX00O  : NAME=RSRX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX20O  : NAME=RSRX20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX20O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX21O  : NAME=RSRX21O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX21O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX22O  : NAME=RSRX22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX25O  : NAME=RSRX25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX30O  : NAME=RSRX30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX30O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX55O  : NAME=RSRX55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX55O /dev/null
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IRX510 ${DATA}/PTEM/GRX50OAA.BRX510AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX510 
       JUMP_LABEL=GRX50OAB
       ;;
(GRX50OAB)
       m_CondExec 04,GE,GRX50OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRX510A)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAD
       ;;
(GRX50OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GRX50OAA.BRX510AO
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX50OAD.BRX510BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_103_2 103 CH 2
 /FIELDS FLD_BI_63_20 63 CH 20
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_339_11 339 CH 11
 /FIELDS FLD_BI_23_20 23 CH 20
 /FIELDS FLD_BI_110_2 110 CH 02
 /FIELDS FLD_BI_157_20 157 CH 20
 /FIELDS FLD_BI_7_3 07 CH 3
 /FIELDS FLD_BI_10_5 10 CH 5
 /FIELDS FLD_BI_43_20 43 CH 20
 /FIELDS FLD_PD_20_3 20 PD 03
 /FIELDS FLD_PD_105_5 105 PD 05
 /FIELDS FLD_BI_332_7 332 CH 7
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_BI_23_20 ASCENDING,
   FLD_BI_43_20 ASCENDING,
   FLD_BI_63_20 ASCENDING,
   FLD_BI_157_20 ASCENDING,
   FLD_BI_103_2 ASCENDING,
   FLD_BI_339_11 ASCENDING,
   FLD_PD_105_5 ASCENDING,
   FLD_BI_110_2 ASCENDING,
   FLD_BI_332_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX50OAE
       ;;
(GRX50OAE)
       m_CondExec 00,EQ,GRX50OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRX510BR                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAG
       ;;
(GRX50OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GRX50OAD.BRX510BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GRX50OAG.BRX510CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX50OAH
       ;;
(GRX50OAH)
       m_CondExec 04,GE,GRX50OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAJ
       ;;
(GRX50OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GRX50OAG.BRX510CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX50OAJ.BRX510DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX50OAK
       ;;
(GRX50OAK)
       m_CondExec 00,EQ,GRX50OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : JRX010                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAM
       ;;
(GRX50OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GRX50OAD.BRX510BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GRX50OAJ.BRX510DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
#  FEDITION REPORT SYSOUT=(9,IRX510),RECFM=VA                                  
       m_OutputAssign -c "*" FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX50OAN
       ;;
(GRX50OAN)
       m_CondExec 04,GE,GRX50OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BRX515  SELECTION DE TOUS LES RELEVES NON PRIS EN                   
#         COMPTE DEPUIS LE DERNIER TRAITEMENT ET EFFECTUE LA SYNTHESE          
#         POUR TOUS LES PRIX INFERIEURS AU PRIX DARTY                          
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAQ
       ;;
(GRX50OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FSEMAINE                                                             
       m_FileAssign -d SHR FSEMAINE ${DATA}/CORTEX4.P.MTXTFIX1/GRX50OAQ
# ******* TABLE ARTICLES                                                       
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE                                                                
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* TABLE EDITION DES ETATS TABLE MARKETING                              
#    RSGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******* TABLE MARQUES (GENERALITES)                                          
#    RSGA22O  : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22O /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30O /dev/null
# ******* TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE DES CODES VALEURS MARKETING                                    
#    RSGA83O  : NAME=RSGA83O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83O /dev/null
# ******* TABLE                                                                
#    RSGG20O  : NAME=RSGG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX00O  : NAME=RSRX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX20O  : NAME=RSRX20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX20O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX21O  : NAME=RSRX21O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX21O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX22O  : NAME=RSRX22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX22O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX25O  : NAME=RSRX25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX25O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX30O  : NAME=RSRX30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX30O /dev/null
# ******* TABLE RELEVE DE PRIX                                                 
#    RSRX55O  : NAME=RSRX55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX55O /dev/null
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IRX515 ${DATA}/PTEM/GRX50OAQ.BRX515AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX515 
       JUMP_LABEL=GRX50OAR
       ;;
(GRX50OAR)
       m_CondExec 04,GE,GRX50OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRX515AR)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAT
       ;;
(GRX50OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GRX50OAQ.BRX515AO
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX50OAT.BRX515BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_43_20 43 CH 20
 /FIELDS FLD_BI_332_7 332 CH 7
 /FIELDS FLD_BI_63_20 63 CH 20
 /FIELDS FLD_BI_110_2 110 CH 02
 /FIELDS FLD_BI_10_5 10 CH 5
 /FIELDS FLD_PD_105_5 105 PD 05
 /FIELDS FLD_BI_339_11 339 CH 11
 /FIELDS FLD_BI_103_2 103 CH 2
 /FIELDS FLD_BI_23_20 23 CH 20
 /FIELDS FLD_BI_7_3 07 CH 3
 /FIELDS FLD_PD_20_3 20 PD 03
 /FIELDS FLD_BI_157_20 157 CH 20
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 ASCENDING,
   FLD_BI_15_5 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_BI_23_20 ASCENDING,
   FLD_BI_43_20 ASCENDING,
   FLD_BI_63_20 ASCENDING,
   FLD_BI_157_20 ASCENDING,
   FLD_BI_103_2 ASCENDING,
   FLD_BI_339_11 ASCENDING,
   FLD_PD_105_5 ASCENDING,
   FLD_BI_110_2 ASCENDING,
   FLD_BI_332_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX50OAU
       ;;
(GRX50OAU)
       m_CondExec 00,EQ,GRX50OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRX515BR                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OAX
       ;;
(GRX50OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/GRX50OAT.BRX515BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GRX50OAX.BRX515CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX50OAY
       ;;
(GRX50OAY)
       m_CondExec 04,GE,GRX50OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OBA
       ;;
(GRX50OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GRX50OAX.BRX515CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GRX50OBA.BRX515DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX50OBB
       ;;
(GRX50OBB)
       m_CondExec 00,EQ,GRX50OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : JRX015                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX50OBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OBD
       ;;
(GRX50OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/GRX50OAT.BRX515BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/GRX50OBA.BRX515DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
#  FEDITION REPORT SYSOUT=(9,IRX515),RECFM=VA                                  
       m_OutputAssign -c "*" FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX50OBE
       ;;
(GRX50OBE)
       m_CondExec 04,GE,GRX50OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRX50OZA
       ;;
(GRX50OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX50OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
