#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PR500P.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPR500 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/08/31 AT 09.38.35 BY PREPA2                       
#    STANDARDS: P  JOBSET: PR500P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BPR501 :MAJ DE LA TABLE RTPR01                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PR500PA
       ;;
(PR500PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PR500PAA
       ;;
(PR500PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSPR01   : NAME=RSPR01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR01 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/PR500PAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR501 
       JUMP_LABEL=PR500PAB
       ;;
(PR500PAB)
       m_CondExec 04,GE,PR500PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR502 :MAJ DE LA TABLE RTPR02                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAD
       ;;
(PR500PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
#    RSPR01   : NAME=RSPR02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR01 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/PR500PAD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR502 
       JUMP_LABEL=PR500PAE
       ;;
(PR500PAE)
       m_CondExec 04,GE,PR500PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR506 :MAJ DE LA TABLE RTPR00                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAG
       ;;
(PR500PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSPR06   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR06 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/PR500PAG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR506 
       JUMP_LABEL=PR500PAH
       ;;
(PR500PAH)
       m_CondExec 04,GE,PR500PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR500 :EXTRAIT LES DONNEES COMMUNES A TOUTES LES FILIALES CREES OU         
#         :MISES A JOUR SUR PARIS DANS LA JOURNEE.                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAJ
       ;;
(PR500PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR01   : NAME=RSPR01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR01 /dev/null
#    RSPR02   : NAME=RSPR02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR02 /dev/null
#    RSPR03   : NAME=RSPR03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR03 /dev/null
#    RSPR04   : NAME=RSPR04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR04 /dev/null
#    RSPR05   : NAME=RSPR05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR05 /dev/null
#    RSPR06   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR06 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 FIPR00 ${DATA}/PTEM/PR500PAJ.BPR500AP
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -g +1 FIPR01 ${DATA}/PTEM/PR500PAJ.BPR500CP
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 FIPR03 ${DATA}/PTEM/PR500PAJ.BPR500EP
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -g +1 FIPR04 ${DATA}/PTEM/PR500PAJ.BPR500GP
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -g +1 FIPR05 ${DATA}/PTEM/PR500PAJ.BPR500IP
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -g +1 FIPR02 ${DATA}/PTEM/PR500PAJ.BPR500KP
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -g +1 FIPR06 ${DATA}/PTEM/PR500PAJ.BPR500MP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 FIPR10 ${DATA}/PTEM/PR500PAJ.BPR500TP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 FIPR11 ${DATA}/PTEM/PR500PAJ.BPR500UP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 FIPR12 ${DATA}/PTEM/PR500PAJ.BPR500VP
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR500 
       JUMP_LABEL=PR500PAK
       ;;
(PR500PAK)
       m_CondExec 04,GE,PR500PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR00                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAM
       ;;
(PR500PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500AP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_5 4 CH 5
 /KEYS
   FLD_CH_4_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PAN
       ;;
(PR500PAN)
       m_CondExec 00,EQ,PR500PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR01                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAQ
       ;;
(PR500PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500CP
       m_FileAssign -d NEW,CATLG,DELETE -r 12 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PAR
       ;;
(PR500PAR)
       m_CondExec 00,EQ,PR500PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR02                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAT
       ;;
(PR500PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500KP
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500LP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PAU
       ;;
(PR500PAU)
       m_CondExec 00,EQ,PR500PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR03                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PR500PAX
       ;;
(PR500PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500EP
       m_FileAssign -d NEW,CATLG,DELETE -r 10 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PAY
       ;;
(PR500PAY)
       m_CondExec 00,EQ,PR500PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR04                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBA
       ;;
(PR500PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500GP
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PBB
       ;;
(PR500PBB)
       m_CondExec 00,EQ,PR500PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR05                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBD
       ;;
(PR500PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500IP
       m_FileAssign -d NEW,CATLG,DELETE -r 26 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PBE
       ;;
(PR500PBE)
       m_CondExec 00,EQ,PR500PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR06                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBG
       ;;
(PR500PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500MP
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500NP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PBH
       ;;
(PR500PBH)
       m_CondExec 00,EQ,PR500PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR0X                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBJ
       ;;
(PR500PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500TP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PBK
       ;;
(PR500PBK)
       m_CondExec 00,EQ,PR500PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR0X                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBM
       ;;
(PR500PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500UP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500YP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PBN
       ;;
(PR500PBN)
       m_CondExec 00,EQ,PR500PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER MAJ TABLE RTPR0X                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBQ
       ;;
(PR500PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/PR500PAJ.BPR500VP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -g +1 SORTOUT ${DATA}/PXX0/F07.BPR500ZP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR500PBR
       ;;
(PR500PBR)
       m_CondExec 00,EQ,PR500PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BBS025 : MAJ DE LA TABLE RTBS25                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR500PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PR500PBT
       ;;
(PR500PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSBS25   : NAME=RSBS25P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBS25 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PR500PBT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS025 
       JUMP_LABEL=PR500PBU
       ;;
(PR500PBU)
       m_CondExec 04,GE,PR500PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PR500PZA
       ;;
(PR500PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR500PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
