#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA001O.ksh                       --- VERSION DU 08/10/2016 22:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGA001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/03 AT 11.02.32 BY BURTECA                      
#    STANDARDS: P  JOBSET: GA001O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#     LOAD DES DONNEES ARTICLES     (RTGA07 : GROUPE-FOURNISSEURS)             
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA07                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA001OA
       ;;
(GA001OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/02/03 AT 11.02.32 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GA001O                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'LOAD T.ARTICLES'                       
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA001OAA
       ;;
(GA001OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GROUPE FOURNISSEURS                                            
#    RSGA07O  : NAME=RSGA07O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA07O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA07                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA07RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAA_RTGA07.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAB
       ;;
(GA001OAB)
       m_CondExec 04,GE,GA001OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA14 : FAMILLES)                        
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA14                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAD
       ;;
(GA001OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES                                                   
#    RSGA14O  : NAME=RSGA14O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA14                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA14RF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAD_RTGA14.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAE
       ;;
(GA001OAE)
       m_CondExec 04,GE,GA001OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA15 : FAMILLE/TYPE DECLARAT�)          
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA15                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAG
       ;;
(GA001OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR TYPE DE DECLARATIONS                          
#    RSGA15O  : NAME=RSGA15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA15O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA15                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA15RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAG_RTGA15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAH
       ;;
(GA001OAH)
       m_CondExec 04,GE,GA001OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA16 : FAMILLE/GARANTIE)                
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA16                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAJ
       ;;
(GA001OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR GARANTIE-COMPLEMENTAIRES                      
#    RSGA16O  : NAME=RSGA16O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA16O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA16                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA16RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAJ_RTGA16.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAK
       ;;
(GA001OAK)
       m_CondExec 04,GE,GA001OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA18 : FAMILLE/CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA18                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAM
       ;;
(GA001OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLES PAR CODE-DESCRIPTIFS                              
#    RSGA18O  : NAME=RSGA18O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA18O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA18                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA18RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAM_RTGA18.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAN
       ;;
(GA001OAN)
       m_CondExec 04,GE,GA001OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA20 : CARACTERISTIC SPECIFIC)          
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA20                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAQ
       ;;
(GA001OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CARACTERISTIQUES SPECIFIQUES                               
#    RSGA20O  : NAME=RSGA20O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA20O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA20                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA20RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAQ_RTGA20.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAR
       ;;
(GA001OAR)
       m_CondExec 04,GE,GA001OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA21 : FAMILLE/RAYON )                  
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA21                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAT
       ;;
(GA001OAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES FAMILLE PAR RAYON                                          
#    RSGA21O  : NAME=RSGA21O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA21O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA21                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA21RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAT_RTGA21.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAU
       ;;
(GA001OAU)
       m_CondExec 04,GE,GA001OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA22 : COMPOSANTS/RAYON)                
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA22                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GA001OAX
       ;;
(GA001OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES COMPOSANT/RAYON                                            
#    RSGA22O  : NAME=RSGA22O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA22O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA22                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA22RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OAX_RTGA22.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OAY
       ;;
(GA001OAY)
       m_CondExec 04,GE,GA001OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA23 : CODE MARKETING )                 
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA23                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBA
       ;;
(GA001OBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES MARKETING                                            
#    RSGA23O  : NAME=RSGA23O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA23O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA23                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA23RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBA_RTGA23.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBB
       ;;
(GA001OBB)
       m_CondExec 04,GE,GA001OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA24 : CODE DESCRIPTIFS)                
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA24                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBD
       ;;
(GA001OBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES CODES DESCRIPTIFS                                          
#    RSGA24R  : NAME=RSGA24O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA24R /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA24                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA24RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBD_RTGA24.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBE
       ;;
(GA001OBE)
       m_CondExec 04,GE,GA001OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA25 : ASSO MARKETING DECRIPT)          
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA25                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBG
       ;;
(GA001OBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES ASSO MARKETING-DESCRIPTIFS                                 
#    RSGA25O  : NAME=RSGA25O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA25O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA25                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA25RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBG_RTGA25.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBH
       ;;
(GA001OBH)
       m_CondExec 04,GE,GA001OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA26 : VALEURS CODE MAKETING)           
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA26                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBJ
       ;;
(GA001OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-MARKETING                                     
#    RSGA26O  : NAME=RSGA26O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA26                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA26RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBJ_RTGA26.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBK
       ;;
(GA001OBK)
       m_CondExec 04,GE,GA001OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA27 : VALEURS CODE DESCRIPTIF)         
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA27                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBM
       ;;
(GA001OBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES VALEURS CODE-DESCRIPTIFS                                   
#    RSGA27O  : NAME=RSGA27O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA27O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA27                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA27RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBM_RTGA27.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBN
       ;;
(GA001OBN)
       m_CondExec 04,GE,GA001OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA71 : TABLE DES TABLES)                
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA71                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBQ
       ;;
(GA001OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES TABLES                                                     
#    RSGA71O  : NAME=RSGA71O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA71O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA71                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA71RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBQ_RTGA71.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBR
       ;;
(GA001OBR)
       m_CondExec 04,GE,GA001OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA72 : TABLE DES BORNES)                
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA72                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBT
       ;;
(GA001OBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES BORNES                                                     
#    RSGA72O  : NAME=RSGA72O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA72O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA72                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA72RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBT_RTGA72.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBU
       ;;
(GA001OBU)
       m_CondExec 04,GE,GA001OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA99 : TABLE DES MESSAGES)              
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA99                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OBX PGM=DSNUTILB   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GA001OBX
       ;;
(GA001OBX)
       m_CondExec ${EXACX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA99O  : NAME=RSGA99O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA99O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA99                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA99RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OBX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OBX_RTGA99.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OBY
       ;;
(GA001OBY)
       m_CondExec 04,GE,GA001OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES     (RTGA91 : TABLE DES RUBRIQUES)             
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA91                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCA
       ;;
(GA001OCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA91O  : NAME=RSGA91O,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA91O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA91O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA91                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA91RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OCA_RTGA91.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCB
       ;;
(GA001OCB)
       m_CondExec 04,GE,GA001OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA92 : TABLE REGROUP DES RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA92                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCD PGM=DSNUTILB   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCD
       ;;
(GA001OCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA92O  : NAME=RSGA92O,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA92O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA92O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA92                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA92RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OCD_RTGA92.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCE
       ;;
(GA001OCE)
       m_CondExec 04,GE,GA001OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DES DONNEES ARTICLES  (RTGA93 : TABLE TOTALISA DE RUBRIQUES         
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTGA93                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCG PGM=DSNUTILB   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCG
       ;;
(GA001OCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSGA93O  : NAME=RSGA93O,MODE=(U,N) - DYNAM=YES                            
# -X-RSGA93O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGA93O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTGA93                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.GA93RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OCG_RTGA93.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCH
       ;;
(GA001OCH)
       m_CondExec 04,GE,GA001OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE LA FACTURATION INTERNE  RTFI05                                   
#     NON-MODIFIABLES PAR LES FILIALES  P916.RTFI05                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCJ PGM=DSNUTILB   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCJ
       ;;
(GA001OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES MESSAGES                                                   
#    RSFI05O  : NAME=RSFI05O,MODE=(U,N) - DYNAM=YES                            
# -X-RSFI05O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSFI05O /dev/null
# ******* FIC DE LOAD DES DONNEES ARTICLES DE P916.RTFI05                      
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PFI0/F07.UNLOAD.FI05UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OCJ_RTFI05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCK
       ;;
(GA001OCK)
       m_CondExec 04,GE,GA001OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     LOAD DE RTPM06 : TABLE DES MODE DE PAIEMENT NEM                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCM PGM=DSNUTILB   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCM
       ;;
(GA001OCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE DES LOIS DE REAPPRO                                            
#    RSPM06O  : NAME=RSPM06O,MODE=(U,N) - DYNAM=YES                            
# -X-RSPM06O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSPM06O /dev/null
# ******* FIC DE LOAD VENANT DE GA001P                                         
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PXX0/F99.RELOAD.PM06RF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA001O_GA001OCM_RTPM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCN
       ;;
(GA001OCN)
       m_CondExec 04,GE,GA001OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES MODES DE D�LIVRANCE NATIONAUX SUR FILIALE NON PR�SENTS S         
#            DIF POUR CHAQUE FAMILLE                                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCQ
       ;;
(GA001OCQ)
       m_CondExec ${EXAEB},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13O  : NAME=RSGA13O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13O /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCQ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCR
       ;;
(GA001OCR)
       m_CondExec 04,GE,GA001OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   INSERT DES MODES DE D�LIVRANCE NATIONAUX DE DIF NON PR�SENTS SUR           
#   FILIALE, POUR CHAQUE FAMILLE                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA001OCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GA001OCT
       ;;
(GA001OCT)
       m_CondExec ${EXAEG},NE,YES 
# ******* TABLE EN ECRITURE                                                    
#    RSGA13O  : NAME=RSGA13O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA13O /dev/null
# ******* TABLE EN LECTURE                                                     
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA001OCT.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA001OCU
       ;;
(GA001OCU)
       m_CondExec 04,GE,GA001OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
