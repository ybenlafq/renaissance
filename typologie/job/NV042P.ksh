#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV042P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV042 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/20 AT 07.22.13 BY BURTECA                      
#    STANDARDS: P  JOBSET: NV042P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  ATTENTION : CETTE CHAINE NE DOIT PAS TOURNE AVEC CICS                       
# ********************************************************************         
#   UNLOAD DE LA TABLE RTGA00                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#  UNLOAD DES DONNEES ARTICLES NCP  RTGA00 : PARIS ET FILIALES                 
#  REPRISE .OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV042PA
       ;;
(NV042PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV042PAA
       ;;
(NV042PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
#    RSGA00   : NAME=RSGA00,MODE=(U,N) - DYNAM=YES                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=(U,N) - DYNAM=YES                            
#    RSGA00M  : NAME=RSGA00M,MODE=(U,N) - DYNAM=YES                            
#    RSGA00D  : NAME=RSGA00D,MODE=(U,N) - DYNAM=YES                            
#    RSGA00L  : NAME=RSGA00L,MODE=(U,N) - DYNAM=YES                            
#    RSGA00O  : NAME=RSGA00O,MODE=(U,N) - DYNAM=YES                            
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV042PAA.NV042UAP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV042PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES A PARTIR DE LA RTNV15                                    
#  REPRISE .OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAD
       ;;
(NV042PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
#    RSNV15   : NAME=RSNV15,MODE=I - DYNAM=YES                                 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV042PAD.NV042UBP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV042PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BCE610 : GENERATION D'ORDRE D'UNLOAD N�1                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAG PGM=BCE610     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAG
       ;;
(NV042PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FCE610O ${DATA}/PTEM/NV042PAG.BNV042P1
# ******  FICHIER SYSIN                                                        
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV042PAG.sysin
       m_ProgramExec BCE610 
# ********************************************************************         
#  UNLOAD N�1                                                                  
#  UNLOAD DES DONNEES A PARTIR DE LA SYSIN PRECEDENTE                          
#  REPRISE .OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAJ
       ;;
(NV042PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSNV15   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV15 /dev/null
#    RSNV15   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV15 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV042PAJ.NV042UCP
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/NV042PAG.BNV042P1
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  SORT DU FICHIER UNLOAD DES RTGA00  .LRECL 27 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAM
       ;;
(NV042PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NV042PAA.NV042UAP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV042PAM.NV042AAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV042PAN
       ;;
(NV042PAN)
       m_CondExec 00,EQ,NV042PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE LA RTNV15 .LRECL 38 DE LONG                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAQ
       ;;
(NV042PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/NV042PAD.NV042UBP
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV042PAQ.NV042BBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 " "
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_6_1 06 CH 01
 /CONDITION CND_1 FLD_CH_6_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_13 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV042PAR
       ;;
(NV042PAR)
       m_CondExec 00,EQ,NV042PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE LA RTNV15 .LRECL 38 DE LONG                       
#  NOUVEAU                                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAT
       ;;
(NV042PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/NV042PAD.NV042UBP
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV042PAT.NV042BCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 " "
 /FIELDS FLD_CH_6_1 06 CH 01
 /FIELDS FLD_CH_1_13 1 CH 13
 /CONDITION CND_1 FLD_CH_6_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_13 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV042PAU
       ;;
(NV042PAU)
       m_CondExec 00,EQ,NV042PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE LA RTPR00 A L AIDE DE LA SYSIN                    
#  NOUVEAU                                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=NV042PAX
       ;;
(NV042PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/NV042PAJ.NV042UCP
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV042PAX.NV042BDP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV042PAY
       ;;
(NV042PAY)
       m_CondExec 00,EQ,NV042PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV042                                                                      
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV042PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=NV042PBA
       ;;
(NV042PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV15   : NAME=RSNV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV15 /dev/null
# ****** TABLE                                                                 
# ****** TABLE                                                                 
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREES                                                    
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FRTGA00 ${DATA}/PTEM/NV042PAM.NV042AAP
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} FRTNV15 ${DATA}/PTEM/NV042PAQ.NV042BBP
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FRTNV15P ${DATA}/PTEM/NV042PAT.NV042BCP
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} FRTPR00 ${DATA}/PTEM/NV042PAX.NV042BDP
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
# ****** TABLE                                                                 
#    RSNV15   : NAME=RSNV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV15 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV042 
       JUMP_LABEL=NV042PBB
       ;;
(NV042PBB)
       m_CondExec 04,GE,NV042PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV042PZA
       ;;
(NV042PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV042PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
