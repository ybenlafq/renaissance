#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRX00O.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGRX00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/22 AT 14.19.46 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GRX00O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG : BRX100  EXTRACTION EN VUE DE LA CREATION DES ETATS                  
#                  DE SUPPORT DE RELEVE                                        
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRX00OA
       ;;
(GRX00OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRX00OAA
       ;;
(GRX00OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *******  FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* TABLE ARTICLES                                                       
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* TABLE GENERALISEE   VUE RVGA01TB ET TZ                               
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* TABLE EDITION DES ETATS TAB MARKETING                                
#    RSGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12O /dev/null
# ******* TABLE FAMILLES                                                       
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******* TABLE LIBELLES RAYON                                                 
#    RSGA20O  : NAME=RSGA20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20O /dev/null
# ******* TABLE FAMIL PARAMETRES ASSOCIES AUX FAMILLES                         
#    RSGA30O  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30O /dev/null
# ******* TABLE DES PRIX EN COURS                                              
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
# ******* TABLE DES CODES VALEURS MARKETING                                    
#    RSGA83O  : NAME=RSGA83O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA83O /dev/null
# ******* TABLE STOCKS SOUS LIEU ENTREPOT                                      
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******* TABLE STOCKS SOUS LIEU MAGASINS                                      
#    RSGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******* TABLE DES CONCURENTS (REMPLACE RTGA04)                               
#    RSRX00O  : NAME=RSRX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00O /dev/null
# ******* TABLE DES FREQUENCES DES RELEVES                                     
#    RSRX15O  : NAME=RSRX15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX15O /dev/null
# ******* TABLE DES RELEVES                                                    
#    RSRX20O  : NAME=RSRX20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX20O /dev/null
# ******* TABLE DES PROFILS PAR SOC/MAG/CODIC                                  
#    RSRX50O  : NAME=RSRX50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX50O /dev/null
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 JRX001 ${DATA}/PTEM/GRX00OAA.BRX001AO
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 461 -t LSEQ -g +1 FRX001 ${DATA}/PXX0/F16.BRX001EO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRX001 
       JUMP_LABEL=GRX00OAB
       ;;
(GRX00OAB)
       m_CondExec 04,GE,GRX00OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRX001AR)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX00OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GRX00OAD
       ;;
(GRX00OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GRX00OAA.BRX001AO
# ******  FEXTRAC TRIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRX00OAD.BRX001BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_16_7 16 CH 7
 /FIELDS FLD_BI_7_2 07 CH 2
 /FIELDS FLD_BI_9_3 09 CH 3
 /FIELDS FLD_BI_12_4 12 CH 4
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_BI_12_4 ASCENDING,
   FLD_BI_16_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX00OAE
       ;;
(GRX00OAE)
       m_CondExec 00,EQ,GRX00OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRX001CR                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX00OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRX00OAG
       ;;
(GRX00OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GRX00OAD.BRX001BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GRX00OAG.BRX001CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GRX00OAH
       ;;
(GRX00OAH)
       m_CondExec 04,GE,GRX00OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX00OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRX00OAJ
       ;;
(GRX00OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GRX00OAG.BRX001CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GRX00OAJ.BRX001DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRX00OAK
       ;;
(GRX00OAK)
       m_CondExec 00,EQ,GRX00OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : JRX001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX00OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRX00OAM
       ;;
(GRX00OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GRX00OAD.BRX001BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GRX00OAJ.BRX001DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEG132 ${DATA}/PXX0/F16.BRX060AO
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JRX001 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GRX00OAN
       ;;
(GRX00OAN)
       m_CondExec 04,GE,GRX00OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION JRX001 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRX00OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRX00OAQ
       ;;
(GRX00OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F16.BRX060AO
       m_OutputAssign -c 9 -w JRX001 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GRX00OAR
       ;;
(GRX00OAR)
       m_CondExec 00,EQ,GRX00OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GRX00OZA
       ;;
(GRX00OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRX00OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
