#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CLI02L.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLCLI02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/10/15 AT 15.33.16 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CLI02L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#   EXTRACTION  DES VENTES                                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CLI02LA
       ;;
(CLI02LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=CLI02LAA
       ;;
(CLI02LAA)
       m_CondExec ${EXAAA},NE,YES 
# ***                                                                          
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLES DB2                                                                  
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV02   : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
#    RSBC31   : NAME=RSBC31L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC31 /dev/null
#    RSBC32   : NAME=RSBC32L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC32 /dev/null
#    RSBC33   : NAME=RSBC33L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC33 /dev/null
#    RSBC34   : NAME=RSBC34L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC34 /dev/null
#    RSBC35   : NAME=RSBC35L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBC35 /dev/null
#    RSGV03   : NAME=RSGV03L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03 /dev/null
#  FICHIER DES ADRESSES                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FBC101 ${DATA}/PEX0/F61.BBC101AL
#  FICHIER DES ENTETES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -t LSEQ -g +1 FBC102 ${DATA}/PEX0/F61.BBC102AL
#  FICHIER DES LIGNES DE VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -t LSEQ -g +1 FBC103 ${DATA}/PEX0/F61.BBC103AL
#  FICHIER  DES REGLEMENTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -t LSEQ -g +1 FBC104 ${DATA}/PEX0/F61.BBC104AL
#  FICHIER DES ADRESSES                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 750 -t LSEQ -g +1 FBC105 ${DATA}/PEX0/F61.BBC105AL
#  FICHIER DES ENTETES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -t LSEQ -g +1 FBC106 ${DATA}/PEX0/F61.BBC106AL
#  FICHIER DES LIGNES DE VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 157 -t LSEQ -g +1 FBC107 ${DATA}/PEX0/F61.BBC107AL
#  FICHIER  DES REGLEMENTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -t LSEQ -g +1 FBC108 ${DATA}/PEX0/F61.BBC108AL
#  PARAMETRE                                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBC102 
       JUMP_LABEL=CLI02LAB
       ;;
(CLI02LAB)
       m_CondExec 04,GE,CLI02LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
