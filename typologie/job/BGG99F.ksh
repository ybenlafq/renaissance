#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BGG99F.ksh                       --- VERSION DU 08/10/2016 17:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFBGG99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/01 AT 16.26.35 BY BURTECA                      
#    STANDARDS: P  JOBSET: BGG99F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
#  DELETE DES FICHERS SORTANT DES CHAINES BGG71*                               
#  REPRISE OUI                                                                 
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BGG99FA
       ;;
(BGG99FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/04/01 AT 16.26.35 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: BGG99F                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'CUMULS SRP'                            
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BGG99FAA
       ;;
(BGG99FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***************************************                                      
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG99FAA.sysin
       m_UtilityExec
#                                                                              
# *********************************************************                    
#  ZIP DES FICHERS BGG702* DES FILIALES                                        
# *********************************************************                    
# AAF      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -NOSYSIN                                                                     
# -ACTION(ADD)                                                                 
# -TRAN(ASCII850)                                                              
# -ARCHIVE_SPACE_PRIMARY(00002000)                                             
# -ARCHIVE_SPACE_SECONDARY(00002000)                                           
# -ARCHUNIT(TEM350)                                                            
# -ARCHIVE(PXX0.F99.BGG702AF)                                                  
# -ZIPPED_DSN(PXX0.F91.BGG702AD.SRP.DPM,BGG702AD.SRP.DPM.CSV)                  
# -ZIPPED_DSN(PXX0.F61.BGG702AL.SRP.DNN,BGG702AL.SRP.DNN.CSV)                  
# -ZIPPED_DSN(PXX0.F89.BGG702AM.SRP.DAL,BGG702AM.SRP.DAL.CSV)                  
# -ZIPPED_DSN(PXX0.F16.BGG702AO.SRP.DOUEST,BGG702AO.SRP.DOUEST.CSV)            
# -ZIPPED_DSN(PXX0.F07.BGG702AP.SRP.DIF,BGG702AP.SRP.DIF.CSV)                  
# -ZIPPED_DSN(PXX0.F08.BGG702AX.SRP.LUXEMB,BGG702AX.SRP.LUXEMB.CSV)            
# -ZIPPED_DSN(PXX0.F45.BGG702AY.SRP.DRA,BGG702AY.SRP.DRA.CSV)                  
# PXX0.F91.BGG702AD.SRP.DPM                                                    
# PXX0.F61.BGG702AL.SRP.DNN                                                    
# PXX0.F89.BGG702AM.SRP.DAL                                                    
# PXX0.F16.BGG702AO.SRP.DOUEST                                                 
# PXX0.F07.BGG702AP.SRP.DIF                                                    
# PXX0.F08.BGG702AX.SRP.LUXEMB                                                 
# PXX0.F45.BGG702AY.SRP.DRA                                                    
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BGG99FAB
       ;;
(BGG99FAB)
       m_CondExec 16,NE,BGG99FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGG99FAD PGM=JVMLDM76   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BGG99FAD
       ;;
(BGG99FAD)
       m_CondExec ${EXAAF},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ FICZIP ${DATA}/PXX0.F99.BGG702AF
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG99FAD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  TRANSFERT VERS (GATEWAY) XFBPRO FICHIERS SRP BGG702A* FILIALES              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SRPXXX,                                                             
#      FNAME=":BGG702AF"",                                                     
#      NFNAME=SRPGSM.ZIP                                                       
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBGG99F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BGG99FAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BGG99FAG
       ;;
(BGG99FAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG99FAG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BGG99FAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BGG99FAJ
       ;;
(BGG99FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG99FAJ.sysin
       m_ProgramExec EZACFSM1
# *********************************************************                    
#  DELETE DES FICHERS SORTANT DES CHAINES BGG702*                              
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BGG99FAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BGG99FAM
       ;;
(BGG99FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BGG99FAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BGG99FAN
       ;;
(BGG99FAN)
       m_CondExec 16,NE,BGG99FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
