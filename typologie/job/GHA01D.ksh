#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHA01D.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGHA01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/29 AT 14.24.30 BY SYSTEM5                      
#    STANDARDS: P  JOBSET: GHA01D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CETTE CHAINE PEUT-ETRES REPORTEE APRES ACCORD D UN RESPONSABLE              
#  RISQUE DE PERTE D INFORMATION SUR LES TABLES ARTICLES                       
#  PAR RAPPORT AUX SAISIES DE PARIS                                            
# ********************************************************************         
#  BHA002 : CREATION DU FIC DE LOAD POUR LA TABLE P991.RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GHA01DA
       ;;
(GHA01DA)
#
#GHA01DBA
#GHA01DBA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01DBA
#
#
#GHA01DAJ
#GHA01DAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01DAJ
#
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GHA01DAA
       ;;
(GHA01DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE P991.RTGA01                                        
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FIC DES SS TABLES NON MODIFIABLES                                    
       m_FileAssign -d SHR -g +0 FHA001 ${DATA}/PXX0/F07.BHA001AP
#                                                                              
# ******* FIC DE LOAD POUR P991.RTGA01 (MARSEILLE)                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FHA002 ${DATA}/PTEM/GHA01DAA.BHA002AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA002 
       JUMP_LABEL=GHA01DAB
       ;;
(GHA01DAB)
       m_CondExec 04,GE,GHA01DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P991.RTGA01                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DAD
       ;;
(GHA01DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P991.RTGA01                                  
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GHA01DAA.BHA002AD
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PGA991/F91.RELOAD.GA01RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_6_15 6 CH 15
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01DAE
       ;;
(GHA01DAE)
       m_CondExec 00,EQ,GHA01DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#           LOAD DE LA TABLE GENERALISEES MARSEILLE P991.RTGA01      *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DAG
       ;;
(GHA01DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GENERALISEE P991.RTGA01                                        
#    RSGA01   : NAME=RSGA01D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FIC DE LOAD POUR  P991.RTGA01                                        
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PGA991/F91.RELOAD.GA01RD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01DAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01D_GHA01DAG_RTGA01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01DAH
       ;;
(GHA01DAH)
       m_CondExec 04,GE,GHA01DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSGA01D DE LA D BASE PDDGA00     *         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DAM
       ;;
(GHA01DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ASSO ARTICLE/PROFIL                                            
#    RSGA73   : NAME=RSGA73D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA73 /dev/null
# ******* TABLE TEXT ETIQUETTE INFO                                            
#    RSGA74   : NAME=RSGA74D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA74 /dev/null
#                                                                              
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA73                     
       m_FileAssign -d SHR -g +0 FHA73 ${DATA}/PXX0/F07.BHA003AP
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA74                     
       m_FileAssign -d SHR -g +0 FHA74 ${DATA}/PXX0/F07.BHA003BP
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA004 
       JUMP_LABEL=GHA01DAN
       ;;
(GHA01DAN)
       m_CondExec 04,GE,GHA01DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA009 : MISE A JOUR DE LA TABLE P991.RTGA06                      *         
#           MISE A JOUR DE LA TABLE P991.RTFT10                      *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DAQ
       ;;
(GHA01DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC EXTRACTS ENTITES DE COMMANDES PROVENANT DE NCP                   
       m_FileAssign -d SHR -g +0 FHA005 ${DATA}/PXX0/F07.BHA005AP
#                                                                              
# ******* TABLE GENERALISEE RTGA01                                             
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ENTITES DE COMMANDES RTGA06                                    
#    RSGA06   : NAME=RSGA06D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
# ******* TABLE DES REPERTOIRES FOURNISSEURS                                   
#    RSFT10   : NAME=RSFT10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT10 /dev/null
# ******* TABLE FM                                                             
#    RSFM01   : NAME=RSFM01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA009 
       JUMP_LABEL=GHA01DAR
       ;;
(GHA01DAR)
       m_CondExec 04,GE,GHA01DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P991.RTHA30                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DAT
       ;;
(GHA01DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P991.RTHA30                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHA030CP
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 SORTOUT ${DATA}/PGA991/F91.RELOAD.HA30RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01DAU
       ;;
(GHA01DAU)
       m_CondExec 00,EQ,GHA01DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE ARTICLES FILIALES  P991.RTHA30                   *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DAX
       ;;
(GHA01DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE ARTICLES FILIALES  P991.RTHA30                                 
#    RSHA30   : NAME=RSHA30D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHA30 /dev/null
# ******* FIC DE LOAD POUR  P991.RTHA30                                        
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PGA991/F91.RELOAD.HA30RD
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01DAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01D_GHA01DAX_RTHA30.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01DAY
       ;;
(GHA01DAY)
       m_CondExec 04,GE,GHA01DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSHA30R DE LA D BASE PDDGA00               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DBD
       ;;
(GHA01DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* N� DE SOCIETE A TRAITER                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA00 ${DATA}/PXX0/F07.BHA050AP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA03 ${DATA}/PXX0/F07.BHA050IP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA31 ${DATA}/PXX0/F07.BHA050JP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA33 ${DATA}/PXX0/F07.BHA050KP
# ******* FIC EXTRACTS RELATION ARTICLE/DECLARATION DE NCP                     
       m_FileAssign -d SHR -g +0 FHA51 ${DATA}/PXX0/F07.BHA050BP
# ******* FIC EXTRACTS RELATION ARTICLE/GARANTIE DE NCP                        
       m_FileAssign -d SHR -g +0 FHA52 ${DATA}/PXX0/F07.BHA050CP
# ******* FIC EXTRACTS ARTICLE/CODE DESCRIPTIF DE NCP                          
       m_FileAssign -d SHR -g +0 FHA53 ${DATA}/PXX0/F07.BHA050DP
# ******* FIC EXTRACTS ARTICLE/ENTITE DE COMMANDE DE NCP                       
       m_FileAssign -d SHR -g +0 FHA55 ${DATA}/PXX0/F07.BHA050EP
# ******* FIC EXTRACTS ARTICLE/ARTICLE DE NCP                                  
       m_FileAssign -d SHR -g +0 FHA58 ${DATA}/PXX0/F07.BHA050FP
# ******* FILE EXTRACTS ZONE DE TRI/PRIX STANDARD                              
       m_FileAssign -d SHR -g +0 FHA59 ${DATA}/PXX0/F07.BHA050GP
# ******* FILE EXTRACTS CARACTERISTIQUES SPECIFIQUES                           
       m_FileAssign -d SHR -g +0 FHA63 ${DATA}/PXX0/F07.BHA050HP
# ******* FIC  EXTRACTS DE GHA01P SUITE MEX PCM                                
       m_FileAssign -d SHR -g +0 FHA40 ${DATA}/PXX0/F07.BHA040AP
       m_FileAssign -d SHR -g +0 FHA41 ${DATA}/PXX0/F07.BHA041AP
       m_FileAssign -d SHR -g +0 FHA42 ${DATA}/PXX0/F07.BHA042AP
#                                                                              
# **TABLES EN MAJ                                                              
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA03   : NAME=RSGA03D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA03 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA31   : NAME=RSGA31D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA33   : NAME=RSGA33D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA33 /dev/null
# ******* TABLE RELATION ARTICLE/DECLARATION                                   
#    RSGA51   : NAME=RSGA51D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* TABLE RELATION ARTICLE/GARANTIE                                      
#    RSGA52   : NAME=RSGA52D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* TABLE RELATION ARTICLE/CODE DESCRIPTIF                               
#    RSGA53   : NAME=RSGA53D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* TABLE RELATION ARTICLE/ASSORTIMENT MAGASIN  LGT                      
#    RSGA54   : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
# ******* TABLE RELATION ARTICLE/ENTITE DE COMMANDE                            
#    RSGA55   : NAME=RSGA55D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA55 /dev/null
# ******* TABLE RELATION ARTICLE/ARTICLE                                       
#    RSGA58   : NAME=RSGA58D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLES : ZONE DE PRIX - PRIX STANDARDS                       
#    RSGA59   : NAME=RSGA59D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE ARTICLES : CARACTERISTIQUES SPECIFIQUES                        
#    RSGA63   : NAME=RSGA63D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA63 /dev/null
# ******* TABLE ARTICLES : MODE DE DELIVRANCE                                  
#    RSGA64   : NAME=RSGA64D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# ******* TABLE SPECIFIQUE ARTICLE GERES PAR ENTREPOT EXTERIEUR                
#                                                                              
# **TABLES EN LECTURE                                                          
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE FAMILLE / MODE DE DELIVRANCE                                   
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA13   : NAME=RSGA13D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13 /dev/null
# ******* TABLE FAMILLES : PARAMETRES ASSOCIES AUX FAMILLES                    
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA051 
       JUMP_LABEL=GHA01DBE
       ;;
(GHA01DBE)
       m_CondExec 04,GE,GHA01DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA008 : MISE A JOUR DES CODES PARAM FAMILLE SUR LA RTGA30 A                
#           PARTIE DU FICHIER FGA30AF ISSU DE GHA01P                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DBG
       ;;
(GHA01DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FGA30 ${DATA}/PXX0/F99.FGA30AF
# ****** TABLE PARAM FAMILLE                                                   
#    RSGA30   : NAME=RSGA30D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA008 
       JUMP_LABEL=GHA01DBH
       ;;
(GHA01DBH)
       m_CondExec 04,GE,GHA01DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL001 :                                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DBJ
       ;;
(GHA01DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA19   : NAME=RSGA19D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTFL05   : NAME=RSFL05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL06   : NAME=RSFL06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL06 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTGQ05   : NAME=RSGQ05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGQ05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RTGA00   : NAME=RSGA00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA65   : NAME=RSGA65D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# -----   FICHIER REPRIS DANS LA CHA�NE FL003P                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FL001 ${DATA}/PXX0/F91.BFL001AD
# -----   FICHIER POUR EVITER DEPASSEMENT TABLEAU INTERNE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 159 -t LSEQ -g +1 FGA00 ${DATA}/PXX0/F91.BFL001CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL001 
       JUMP_LABEL=GHA01DBK
       ;;
(GHA01DBK)
       m_CondExec 04,GE,GHA01DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP00 A PARTIR DU TS RSSP00  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DBM
       ;;
(GHA01DBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP00   : NAME=RSSP00D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP00 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP00
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01DBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01D_GHA01DBM_RTSP00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01DBN
       ;;
(GHA01DBN)
       m_CondExec 04,GE,GHA01DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP05 A PARTIR DU TS RSSP05  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DBQ
       ;;
(GHA01DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP05   : NAME=RSSP05D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP05 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP05
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01DBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01D_GHA01DBQ_RTSP05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01DBR
       ;;
(GHA01DBR)
       m_CondExec 04,GE,GHA01DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP15 A PARTIR DU TS RSSP15  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01DBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DBT
       ;;
(GHA01DBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP15   : NAME=RSSP15D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP15 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP15
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01DBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01D_GHA01DBT_RTSP15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01DBU
       ;;
(GHA01DBU)
       m_CondExec 04,GE,GHA01DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GHA01DZA
       ;;
(GHA01DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
