#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA010M.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGA010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/01 AT 09.41.52 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GA010M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGA001                                                                      
# ********************************************************************         
#   CREATION DES RELATIONS EXPOSITION MAGASIN FAMILLE                          
#   MAJ TABLE RELATION FAMILLE EXPOSITION MAGASIN (RTGA19)                     
#   LECTURE DE LA TABLE RTGA14                                                 
#   REPRISE: OUI SI FIN ANORMALE NON SI REPRISE APRES FIN NORMALE              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA010MA
       ;;
(GA010MA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2001/02/01 AT 09.41.52 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GA010M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXPO MAGASIN/FAM'                      
# *                           APPL...: REPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA010MAA
       ;;
(GA010MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES MAJ ARTICLES GA14           *                                       
#    APRES RECUP PARM CICS             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FAMILLES                                                   
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ******* TABLE RELATION FAMILLE / EXPO MAGASIN                                
#    RSGA19M  : NAME=RSGA19,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19M /dev/null
# ******  TABLE RELATION ARTICLE / ASSORTIMENT MAGASIN                         
#    RSGA54M  : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54M /dev/null
       m_FileAssign -d SHR PARAMEX ${DATA}/CORTEX4.P.MTXTFIX5/BGA001M
       m_OutputAssign -c "*" IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA001 
       JUMP_LABEL=GA010MAB
       ;;
(GA010MAB)
       m_CondExec 04,GE,GA010MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
