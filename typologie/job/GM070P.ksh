#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM070P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGM070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/11/04 AT 11.45.11 BY PREPA2                       
#    STANDARDS: P  JOBSET: GM070P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA59 : PRIX                              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA75 : PRIME                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GM070PA
       ;;
(GM070PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXA98=${EXA98:-0}
       FIRSTME=${FIRSTME}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       LUNDI=${LUNDI}
       MARDI=${MARDI}
       MERCRED=${MERCRED}
       RUN=${RUN}
       JUMP_LABEL=GM070PAA
       ;;
(GM070PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
#    RSGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PTEM/GM070PAA.BGA59ZP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC02 ${DATA}/PTEM/GM070PAA.BGA75ZP
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC03 ${DATA}/PTEM/GM070PAA.BGS30UP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070PAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DU FIC RTGS30                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAD
       ;;
(GM070PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GM070PAA.BGS30UP
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -g +1 SORTOUT ${DATA}/PTEM/F07.BGS30FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_18_3 18 CH 3
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_4_3,FLD_CH_18_3,FLD_CH_21_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070PAE
       ;;
(GM070PAE)
       m_CondExec 00,EQ,GM070PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM068                                                                      
#  EXTRACT DES VENTES SUR 28 JOURS GLISSANTS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAG
       ;;
(GM070PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSHV02   : NAME=RSHV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV02 /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** FICHIER DES VENTES SUR 28 JOURS                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 FGM068 ${DATA}/PTEM/GM070PAG.BGM068AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM068 
       JUMP_LABEL=GM070PAH
       ;;
(GM070PAH)
       m_CondExec 04,GE,GM070PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES SOCIETES                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAJ
       ;;
(GM070PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GM070PAG.BGM068AP
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/GM070PAJ.BGM068BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_PD_8_4 08 PD 4
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_8_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PAK
       ;;
(GM070PAK)
       m_CondExec 00,EQ,GM070PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM069                                                                      
#  EXTRACT DES CODES DESCRIPTIFS , CODE MARKETING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAM
       ;;
(GM070PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#  FICHIER EN ENTRE ISSU DE GM010P                                             
       m_FileAssign -d SHR -g +0 FGA053 ${DATA}/PXX0/F07.BGA053BP
#  MODIF LE FGM069 PASSE DE 273 A 436                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -g +1 FGM069 ${DATA}/PTEM/GM070PAM.BGM069AP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGM020 ${DATA}/PTEM/GM070PAM.BGM069BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM069 
       JUMP_LABEL=GM070PAN
       ;;
(GM070PAN)
       m_CondExec 04,GE,GM070PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAQ
       ;;
(GM070PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GM070PAM.BGM069AP
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -g +1 SORTOUT ${DATA}/PTEM/GM070PAQ.BGM069CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_145_5 145 CH 5
 /FIELDS FLD_CH_1_7 01 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_145_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PAR
       ;;
(GM070PAR)
       m_CondExec 00,EQ,GM070PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065M                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAT
       ;;
(GM070PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PEX0/F07.BGM065MP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "907"
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_5 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PAU
       ;;
(GM070PAU)
       m_CondExec 00,EQ,GM070PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065Z                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070PAX
       ;;
(GM070PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PEX0/F07.BGM065ZP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "907"
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PAY
       ;;
(GM070PAY)
       m_CondExec 00,EQ,GM070PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM070                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBA
       ;;
(GM070PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA66 /dev/null
#    RSGG20   : NAME=RSGG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG20 /dev/null
#    RSGG40   : NAME=RSGG40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG40 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSSP15   : NAME=RSSP15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSP15 /dev/null
#    RSGA68   : NAME=RSGA68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA68 /dev/null
#    RSRX00   : NAME=RSRX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRX00 /dev/null
#    RSRX55   : NAME=RSRX55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRX55 /dev/null
#                                                                              
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
# **** DATE                                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** FICHIER LUS                                                           
       m_FileAssign -d SHR -g ${G_A4} FGM068 ${DATA}/PTEM/GM070PAJ.BGM068BP
#  CE FICHIER DOIT ETRE RELU EN 436                                            
       m_FileAssign -d SHR -g ${G_A5} FGM069 ${DATA}/PTEM/GM070PAQ.BGM069CP
       m_FileAssign -d SHR -g ${G_A6} FGM020 ${DATA}/PTEM/GM070PAM.BGM069BP
       m_FileAssign -d SHR -g +0 RTGN59 ${DATA}/PXX0/F07.BGN59FF
       m_FileAssign -d SHR -g ${G_A7} RTGA59Z ${DATA}/PTEM/GM070PAA.BGA59ZP
       m_FileAssign -d SHR -g +0 RTGN75F ${DATA}/PXX0/F07.BGN75FF
       m_FileAssign -d SHR -g ${G_A8} RTGA75Z ${DATA}/PTEM/GM070PAA.BGA75ZP
       m_FileAssign -d SHR -g ${G_A9} FGM065M ${DATA}/PEX0/F07.BGM065MP
       m_FileAssign -d SHR -g ${G_A10} FGM065Z ${DATA}/PEX0/F07.BGM065ZP
       m_FileAssign -d SHR -g ${G_A11} RTGS30 ${DATA}/PTEM/F07.BGS30FP
       m_FileAssign -d SHR -g +0 RTGS10 ${DATA}/PXX0/F07.BGS10FF
# ****** FICHIER CREES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 RTGM01 ${DATA}/PTEM/GM070PBA.BGM070AP
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 RTGM06 ${DATA}/PTEM/GM070PBA.BGM070BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGM075 ${DATA}/PTEM/GM070PBA.BGM070CP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGM70 ${DATA}/PTEM/GM070PBA.BGM070DP
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 FGM01 ${DATA}/PTEM/GM070PBA.BGM070EP
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 FGM06 ${DATA}/PTEM/GM070PBA.BGM070FP
#  CREATION DE 2 NOUVEAUX FICHIERS LRECL 227                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM025 ${DATA}/PTEM/GM070PBA.BGM070MP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM026 ${DATA}/PTEM/GM070PBA.BGM070NP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM070 
       JUMP_LABEL=GM070PBB
       ;;
(GM070PBB)
       m_CondExec 04,GE,GM070PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBD
       ;;
(GM070PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GM070PBA.BGM070CP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070PBD.BGM070GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_268_1 268 CH 1
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_10_2 10 CH 2
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBE
       ;;
(GM070PBE)
       m_CondExec 00,EQ,GM070PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBG
       ;;
(GM070PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GM070PBA.BGM070DP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070PBG.BGM070HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBH
       ;;
(GM070PBH)
       m_CondExec 00,EQ,GM070PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBJ
       ;;
(GM070PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GM070PBA.BGM070AP
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PTEM/GM070PBJ.BGM070IP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_32_7 32 CH 7
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_24_1 24 CH 1
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_24_1 DESCENDING,
   FLD_CH_25_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBK
       ;;
(GM070PBK)
       m_CondExec 00,EQ,GM070PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBM
       ;;
(GM070PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GM070PBA.BGM070EP
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PTEM/GM070PBM.BGM070JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_229_2 229 CH 2
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_25_7 ASCENDING,
   FLD_CH_24_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBN
       ;;
(GM070PBN)
       m_CondExec 00,EQ,GM070PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBQ
       ;;
(GM070PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GM070PBA.BGM070BP
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PTEM/GM070PBQ.BGM070KP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_22_1 22 CH 1
 /FIELDS FLD_CH_8_7 8 CH 7
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_8_7 ASCENDING,
   FLD_CH_22_1 DESCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBR
       ;;
(GM070PBR)
       m_CondExec 00,EQ,GM070PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBT
       ;;
(GM070PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GM070PBA.BGM070FP
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PTEM/GM070PBT.BGM070LP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_22_1 22 CH 1
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_15_2 15 CH 2
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_22_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBU
       ;;
(GM070PBU)
       m_CondExec 00,EQ,GM070PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GM070PBX
       ;;
(GM070PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GM070PBA.BGM070MP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PTEM/GM070PBX.BGM070OP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_1_16 1 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_104_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PBY
       ;;
(GM070PBY)
       m_CondExec 00,EQ,GM070PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCA
       ;;
(GM070PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GM070PBA.BGM070NP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PTEM/GM070PCA.BGM070PP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_1_16 1 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PCB
       ;;
(GM070PCB)
       m_CondExec 00,EQ,GM070PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM075                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCD
       ;;
(GM070PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER PARAMETRE                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#  2 NOUVEAUX FICHIERS RELU EN 227                                             
       m_FileAssign -d SHR -g ${G_A20} FSM025 ${DATA}/PTEM/GM070PBX.BGM070OP
       m_FileAssign -d SHR -g ${G_A21} FSM026 ${DATA}/PTEM/GM070PCA.BGM070PP
       m_FileAssign -d SHR -g ${G_A22} FGM70 ${DATA}/PTEM/GM070PBG.BGM070HP
       m_FileAssign -d SHR -g ${G_A23} FGM075 ${DATA}/PTEM/GM070PBD.BGM070GP
       m_FileAssign -d SHR -g ${G_A24} RTGM01 ${DATA}/PTEM/GM070PBJ.BGM070IP
       m_FileAssign -d SHR -g ${G_A25} RTGM06 ${DATA}/PTEM/GM070PBQ.BGM070KP
       m_FileAssign -d SHR -g ${G_A26} FGM01 ${DATA}/PTEM/GM070PBM.BGM070JP
       m_FileAssign -d SHR -g ${G_A27} FGM06 ${DATA}/PTEM/GM070PBT.BGM070LP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM075 ${DATA}/PTEM/GM070PCD.BGM075AP
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 RTGM01S ${DATA}/PTEM/GM070PCD.BGM075BP
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 RTGM06S ${DATA}/PTEM/GM070PCD.BGM075CP
#  2 NOVEAUX FICHIERS DE SORTIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM025S ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 FGM080 ${DATA}/PTEM/GM070PCD.BGM075EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM075 
       JUMP_LABEL=GM070PCE
       ;;
(GM070PCE)
       m_CondExec 04,GE,GM070PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM01                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCG
       ;;
(GM070PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/GM070PCD.BGM075BP
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PGG0/F07.RELOAD.GM01RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_14 25 CH 14
 /KEYS
   FLD_CH_25_14 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PCH
       ;;
(GM070PCH)
       m_CondExec 00,EQ,GM070PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM06                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCJ
       ;;
(GM070PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/GM070PCD.BGM075CP
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PGG0/F07.RELOAD.GM06RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 01 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PCK
       ;;
(GM070PCK)
       m_CondExec 00,EQ,GM070PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM70                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCM
       ;;
(GM070PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/GM070PCD.BGM075AP
       m_FileAssign -d NEW,CATLG,DELETE -r 402 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.GM70RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_7_401 07 CH 401
 /FIELDS FLD_CH_20_74 20 CH 74
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_BI_94_2 94 CH 2
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_74 ASCENDING,
   FLD_BI_94_2 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_401
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070PCN
       ;;
(GM070PCN)
       m_CondExec 00,EQ,GM070PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM01                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCQ PGM=DSNUTILB   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCQ
       ;;
(GM070PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A31} SYSREC ${DATA}/PGG0/F07.RELOAD.GM01RP
#    RSGM01R  : NAME=RSGM01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGM01R /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070PCQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070P_GM070PCQ_RTGM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070PCR
       ;;
(GM070PCR)
       m_CondExec 04,GE,GM070PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM06                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCT PGM=DSNUTILB   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCT
       ;;
(GM070PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A32} SYSREC ${DATA}/PGG0/F07.RELOAD.GM06RP
#    RSGM06R  : NAME=RSGM06,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGM06R /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070PCT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070P_GM070PCT_RTGM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070PCU
       ;;
(GM070PCU)
       m_CondExec 04,GE,GM070PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM70                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GM070PCX
       ;;
(GM070PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A33} SYSREC ${DATA}/PXX0/F07.RELOAD.GM70RP
#    RSGM70R  : NAME=RSGM70,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGM70R /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070PCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070P_GM070PCX_RTGM70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070PCY
       ;;
(GM070PCY)
       m_CondExec 04,GE,GM070PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDA
       ;;
(GM070PDA)
       m_CondExec ${EXAEQ},NE,YES 1,EQ,$[MARDI] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01R  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01R /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02R  : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02R /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10R  : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA10R /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14R  : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA14R /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26R  : NAME=RSGA26,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA26R /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01R  : NAME=RSGM01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM01R /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06R  : NAME=RSGM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM06R /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A34} FGM020 ${DATA}/PTEM/GM070PAM.BGM069BP
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 907000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/RDARSOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070PDA
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020 IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070PDB
       ;;
(GM070PDB)
       m_CondExec 04,GE,GM070PDA ${EXAEQ},NE,YES 1,EQ,$[MARDI] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDD
       ;;
(GM070PDD)
       m_CondExec ${EXAEV},NE,YES 1,EQ,$[LUNDI] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01R  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01R /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02R  : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02R /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10R  : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA10R /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14R  : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA14R /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26R  : NAME=RSGA26,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA26R /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01R  : NAME=RSGM01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM01R /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06R  : NAME=RSGM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM06R /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A35} FGM020 ${DATA}/PTEM/GM070PAM.BGM069BP
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 907000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/RDARSOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070PDD
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020P IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070PDE
       ;;
(GM070PDE)
       m_CondExec 04,GE,GM070PDD ${EXAEV},NE,YES 1,EQ,$[LUNDI] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDG
       ;;
(GM070PDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01R  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01R /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02R  : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02R /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10R  : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA10R /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14R  : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA14R /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26R  : NAME=RSGA26,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA26R /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01R  : NAME=RSGM01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM01R /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06R  : NAME=RSGM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM06R /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A36} FGM020 ${DATA}/PTEM/GM070PAM.BGM069BP
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 907000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/RDARSOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070PDG
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020F IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070PDH
       ;;
(GM070PDH)
       m_CondExec 04,GE,GM070PDG ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM025                                                                      
#  EDITION CAHIER-BLEU AVEC ARTICLES EPUISES ET EN CONTREMARQUE                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDJ
       ;;
(GM070PDJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[FIRSTME] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE GENERALISEE.                                                    
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ****** TABLE DES CHEFS DE PRODUITS.                                          
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ****** TABLE DES LIEUX                                                       
#    RSGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA10 /dev/null
# ****** TABLE DES FAMILLES.                                                   
#    RSGA14   : NAME=RSGA14,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA14 /dev/null
# ****** TABLE VALEURS CODES MARKETING PAR ARTICLE.                            
#    RSGA26   : NAME=RSGA26,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA26 /dev/null
# ****** TABLE CAHIER-BLEU BATCH.                                              
#    RSGM01   : NAME=RSGM01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM01 /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06   : NAME=RSGM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGM06 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C, % ECART PRIX 2C,%ECART COM 2C         
#  SOIT 0600505                                                                
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM070PDJ
# ****** FICHIER D'EDITION.                                                    
       m_OutputAssign -c 9 -w BGM025 IMPR
# ****** SOCIETE : 907000                                                      
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/RDARSOC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM025 
       JUMP_LABEL=GM070PDK
       ;;
(GM070PDK)
       m_CondExec 04,GE,GM070PDJ ${EXAFF},NE,YES 1,EQ,$[FIRSTME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDM
       ;;
(GM070PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/GM070PCD.BGM075AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070PDM.BGM080AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_20_67 20 CH 67
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_CH_270_1 270 CH 1
 /FIELDS FLD_CH_87_7 87 CH 7
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_67 ASCENDING,
   FLD_CH_270_1 ASCENDING,
   FLD_CH_87_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PDN
       ;;
(GM070PDN)
       m_CondExec 00,EQ,GM070PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDQ
       ;;
(GM070PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GM070PCD.BGM075EP
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 SORTOUT ${DATA}/PTEM/GM070PDQ.BGM075FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 01 CH 7
 /FIELDS FLD_BI_8_2 08 CH 2
 /KEYS
   FLD_BI_8_2 ASCENDING,
   FLD_BI_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PDR
       ;;
(GM070PDR)
       m_CondExec 00,EQ,GM070PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGM080                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDT
       ;;
(GM070PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  FICHIER ENTRE                                                               
       m_FileAssign -d SHR -g ${G_A39} FGM075 ${DATA}/PTEM/GM070PDM.BGM080AP
       m_FileAssign -d SHR -g ${G_A40} FGM080 ${DATA}/PTEM/GM070PDQ.BGM075FP
#  FICHIER SORTIE                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM080 ${DATA}/PTEM/F07.BGM080BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM085 ${DATA}/PTEM/GM070PDT.BGM085AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM080 
       JUMP_LABEL=GM070PDU
       ;;
(GM070PDU)
       m_CondExec 04,GE,GM070PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM080                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070PDX
       ;;
(GM070PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/F07.BGM080BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070PDX.BGM080EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_CH_7_10 7 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PDY
       ;;
(GM070PDY)
       m_CondExec 00,EQ,GM070PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GM070PEA
       ;;
(GM070PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A42} FEXTRAC ${DATA}/PTEM/GM070PDX.BGM080EP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/F07.BGM080CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070PEB
       ;;
(GM070PEB)
       m_CondExec 04,GE,GM070PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070PED
       ;;
(GM070PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PTEM/F07.BGM080CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/F07.BGM080DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PEE
       ;;
(GM070PEE)
       m_CondExec 00,EQ,GM070PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM080                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GM070PEG
       ;;
(GM070PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A44} FEXTRAC ${DATA}/PTEM/GM070PDX.BGM080EP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A45} FCUMULS ${DATA}/PTEM/F07.BGM080DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c Z -w IGM080 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070PEH
       ;;
(GM070PEH)
       m_CondExec 04,GE,GM070PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM085                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GM070PEJ
       ;;
(GM070PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PTEM/GM070PDT.BGM085AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070PEJ.BGM085BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_CH_23_60 23 CH 60
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PEK
       ;;
(GM070PEK)
       m_CondExec 00,EQ,GM070PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GM070PEM
       ;;
(GM070PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A47} FEXTRAC ${DATA}/PTEM/GM070PEJ.BGM085BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GM070PEM.BGM085CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070PEN
       ;;
(GM070PEN)
       m_CondExec 04,GE,GM070PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PEQ PGM=SORT       ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GM070PEQ
       ;;
(GM070PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/GM070PEM.BGM085CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070PEQ.BGM085DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070PER
       ;;
(GM070PER)
       m_CondExec 00,EQ,GM070PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM085                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070PET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GM070PET
       ;;
(GM070PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ********************************** TABLE DES SOUS/TABLES                     
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A49} FEXTRAC ${DATA}/PTEM/GM070PEJ.BGM085BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FCUMULS ${DATA}/PTEM/GM070PEQ.BGM085DP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c Z -w IGM085 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070PEU
       ;;
(GM070PEU)
       m_CondExec 04,GE,GM070PET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM070PZA
       ;;
(GM070PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
