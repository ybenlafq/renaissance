#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA770O.ksh                       --- VERSION DU 08/10/2016 22:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGA770 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/01/17 AT 16.42.47 BY BURTECN                      
#    STANDARDS: P  JOBSET: GA770O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGA770 : MAJ DE LA DATE DE RECEPTION DES CODIC GROUPES DE RTGA00            
#           SI TOUS LES CODICS ELEMENTS ONT ETE DEJA RECEPTIONNÚS              
#           ET SI LE CODIC GROUPE A 1 PRIX DANS TOUTES LES ZONES DE PR         
#  REPRISE: OUI SI FIN ANORMALE                                                
#           NON SI FIN NORMALE: FAIRE 1 RECOVER DE RSGA00 A PARTIR DES         
#           FULL IMAGE COPY (UTILISER LA CHAINE CORTEX DB2FIO)                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GA770OA
       ;;
(GA770OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GA770OAA
       ;;
(GA770OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE : SOUS TABLE ZPRIX                                 
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE DES LIENS ENTRE ARTICLES                                       
#    RSGA58O  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58O /dev/null
# ******* TABLE DES PRIX PAR ARTICLE                                           
#    RSGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59O /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* SOCIETE TRAITEE : 916                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* MAJ DE LA DATE DE RECEPTION (D1RECEPT) DE RTGA00                     
#    RSGA00O  : NAME=RSGA00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* MAJ DES ARTICLES MUTES                                               
#    RSMU15   : NAME=RSMU15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU15 /dev/null
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA770 
       JUMP_LABEL=GA770OAB
       ;;
(GA770OAB)
       m_CondExec 04,GE,GA770OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
