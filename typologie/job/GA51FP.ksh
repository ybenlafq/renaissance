#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA51FP.ksh                       --- VERSION DU 20/10/2016 14:59
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGA51F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/08/19 AT 08.46.53 BY BURTECA                      
#    STANDARDS: P  JOBSET: GA51FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI SUR FICHIER FRTGA10 DE PARIS ET FILIALES PROVENANT DES JOBS GA0         
#  TRI SUR SOCIETE + LIEU + TYPE DE LIEU                                       
#  REPRISE: OUI.                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA51FPA
       ;;
(GA51FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2014/08/19 AT 08.46.53 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GA51FP                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'SOC LIEUX'                             
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA51FPAA
       ;;
(GA51FPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
# ********************************  ENTREE FIC LIEUX/VENTES(DPM)               
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F91.FRGA10AD
# ********************************  ENTREE FIC LIEUX/VENTES(LILLE)             
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F61.FRGA10AL
# ********************************  ENTREE FIC LIEUX/VENTES(METZ)              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F89.FRGA10AM
# ********************************  ENTREE FIC LIEUX/VENTES(PARIS)             
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F07.FRGA10AP
# ********************************  ENTREE FIC LIEUX/VENTES(LYON)              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F45.FRGA10AY
# ********************************  ENTREE FIC LIEUX/VENTES(DACEM)             
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F96.FRGA10AB
# ********************************  SORTIE FIC LIEUX/VENTES(MACK)              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F16.FRGA10AO
# ********************************  SORTIE FIC LIEUX/VENTES(LUX)               
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.FRGA10AX
# ********************************  SORTIE FIC LIEUX/VENTES(MACK)              
       m_FileAssign -d NEW,CATLG,DELETE -r 308 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.FRGA10AF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /* Record Type = F  Record Length = 308 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GA51FPAB
       ;;
(GA51FPAB)
       m_CondExec 00,EQ,GA51FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGA055                                                                
#  EXTRACTION DES SOCIETES LIEUX POUR PARIS SUR UN SEQUENTIEL REPRIS           
#  DANS GA51FP.                                                                
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA51FPAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA51FPAD
       ;;
(GA51FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE DES LIEUX                            
#    RTGA10   : NAME=RSGA10FP,MODE=U - DYNAM=YES                               
       m_FileAssign -d SHR RTGA10 /dev/null
# ********************************  FIC DU TRI PRECEDENT                       
       m_FileAssign -d SHR -g ${G_A1} FRTGA10 ${DATA}/PEX0/F07.FRGA10AF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA055 
       JUMP_LABEL=GA51FPAE
       ;;
(GA51FPAE)
       m_CondExec 04,GE,GA51FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
