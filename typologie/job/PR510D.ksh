#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PR510D.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPR510 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/27 AT 14.41.29 BY BURTECA                      
#    STANDARDS: P  JOBSET: PR510D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BPR510 :MISE � JOUR DES TABLES PRESTATIONS FILIALES � PARTIR DES            
#         :FICHIERS CR�ES PAR PR500P.                                          
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PR510DA
       ;;
(PR510DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PR510DAA
       ;;
(PR510DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSPR00   : NAME=RSPR00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR01   : NAME=RSPR01D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR01 /dev/null
#    RSPR03   : NAME=RSPR03D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR03 /dev/null
#    RSPR04   : NAME=RSPR04D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR04 /dev/null
#    RSPR05   : NAME=RSPR05D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR05 /dev/null
#    RSPR02   : NAME=RSPR02D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR02 /dev/null
#    RSPR06   : NAME=RSPR06D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06 /dev/null
       m_FileAssign -d SHR -g +0 FIPR00 ${DATA}/PXX0/F07.BPR500BP
       m_FileAssign -d SHR -g +0 FIPR01 ${DATA}/PXX0/F07.BPR500DP
       m_FileAssign -d SHR -g +0 FIPR03 ${DATA}/PXX0/F07.BPR500FP
       m_FileAssign -d SHR -g +0 FIPR04 ${DATA}/PXX0/F07.BPR500HP
       m_FileAssign -d SHR -g +0 FIPR05 ${DATA}/PXX0/F07.BPR500JP
       m_FileAssign -d SHR -g +0 FIPR02 ${DATA}/PXX0/F07.BPR500LP
       m_FileAssign -d SHR -g +0 FIPR06 ${DATA}/PXX0/F07.BPR500NP
       m_FileAssign -d SHR -g +0 FIPR10 ${DATA}/PXX0/F07.BPR500XP
       m_FileAssign -d SHR -g +0 FIPR11 ${DATA}/PXX0/F07.BPR500YP
       m_FileAssign -d SHR -g +0 FIPR12 ${DATA}/PXX0/F07.BPR500ZP
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR510 
       JUMP_LABEL=PR510DAB
       ;;
(PR510DAB)
       m_CondExec 04,GE,PR510DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR520 :EXTRACTION FICHIERS D'EDITION DESTINES A L'ETAT DES PREST-          
#         :ATIONS MISES A JOUR.                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR510DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PR510DAD
       ;;
(PR510DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSPR00   : NAME=RSPR00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
       m_FileAssign -d SHR -g +0 FIPR00 ${DATA}/PXX0/F07.BPR500BP
       m_FileAssign -d SHR -g +0 FIPR01 ${DATA}/PXX0/F07.BPR500DP
       m_FileAssign -d SHR -g +0 FIPR04 ${DATA}/PXX0/F07.BPR500HP
       m_FileAssign -d SHR -g +0 FIPR05 ${DATA}/PXX0/F07.BPR500JP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPR500 ${DATA}/PTEM/PR510DAD.BPR520AD
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR520 
       JUMP_LABEL=PR510DAE
       ;;
(PR510DAE)
       m_CondExec 04,GE,PR510DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BPR520AL                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR510DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PR510DAG
       ;;
(PR510DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PR510DAD.BPR520AD
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PR510DAG.BPR520BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_8 7 CH 8
 /KEYS
   FLD_CH_7_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR510DAH
       ;;
(PR510DAH)
       m_CondExec 00,EQ,PR510DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
#  ATTENTION : LE FICHIER FEXTRAC DOIT AVOIR UN LRECL=512 ET AINSI QUE         
#  LE FICHIER FCUMULS                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR510DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PR510DAJ
       ;;
(PR510DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/PR510DAG.BPR520BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PR510DAJ.BPR520CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PR510DAK
       ;;
(PR510DAK)
       m_CondExec 04,GE,PR510DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR510DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PR510DAM
       ;;
(PR510DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PR510DAJ.BPR520CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PR510DAM.BPR520DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PR510DAN
       ;;
(PR510DAN)
       m_CondExec 00,EQ,PR510DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR510DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PR510DAQ
       ;;
(PR510DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/PR510DAG.BPR520BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/PR510DAM.BPR520DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE FMOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* FICHIER D'IMPRESSION POUR LES SIEGES FILIALES ****                   
       m_OutputAssign -c 9 -w IPR500 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PR510DAR
       ;;
(PR510DAR)
       m_CondExec 04,GE,PR510DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPR506 :MAJ DE LA TABLE RTPR00                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PR510DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PR510DAT
       ;;
(PR510DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSPR00   : NAME=RSPR00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR06   : NAME=RSPR06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06 /dev/null
#    RSPR00   : NAME=RSPR00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/PR510DAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPR506 
       JUMP_LABEL=PR510DAU
       ;;
(PR510DAU)
       m_CondExec 04,GE,PR510DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PR510DZA
       ;;
(PR510DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PR510DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
