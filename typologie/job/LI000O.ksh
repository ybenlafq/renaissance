#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LI000O.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POLI000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/05/20 AT 16.46.24 BY BURTECM                      
#    STANDARDS: P  JOBSET: LI000O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BLI001 :  EXTRACTION DES LIVRAISONS                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LI000OA
       ;;
(LI000OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LI000OAA
       ;;
(LI000OAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  FAMILLES                                                             
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  VENTES                                                               
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  DETAILS TOURNEES                                                     
#    RSTL02O  : NAME=RSTL02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02O /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 43 -g +1 FLI001 ${DATA}/PTEM/LI000OAA.BLI001AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI001 
       JUMP_LABEL=LI000OAB
       ;;
(LI000OAB)
       m_CondExec 04,GE,LI000OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT FICHIER EXTRACTION                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LI000OAD
       ;;
(LI000OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/LI000OAA.BLI001AO
       m_FileAssign -d NEW,CATLG,DELETE -r 43 -g +1 SORTOUT ${DATA}/PTEM/LI000OAD.BLI001BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_ZD_32_6 32 ZD 6
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_ZD_38_6 38 ZD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_32_6,
    TOTAL FLD_ZD_38_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LI000OAE
       ;;
(LI000OAE)
       m_CondExec 00,EQ,LI000OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLI003  :  EDITION DES LIVRAISONS PAR PLATEFORME                            
#  REPRISE :  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LI000OAG
       ;;
(LI000OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LIEUX                                                                
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  DETAILS TOURNEES                                                     
#    RSTL02O  : NAME=RSTL02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02O /dev/null
# ******  RETOURS TOURNEES                                                     
#    RSTL04O  : NAME=RSTL04O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04O /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A2} FLI003 ${DATA}/PTEM/LI000OAD.BLI001BO
# *****   EDITION                                                              
       m_OutputAssign -c 9 -w ILI003 ILI003
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI003 
       JUMP_LABEL=LI000OAH
       ;;
(LI000OAH)
       m_CondExec 04,GE,LI000OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLI002  :  EXTRACTION DES LIVRAISONS                                        
#  REPRISE :  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LI000OAJ
       ;;
(LI000OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  SOUS TABLE TRETO                                                     
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  VENTES                                                               
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  RETOURS TOURNEES                                                     
#    RSTL04O  : NAME=RSTL04O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04O /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FLI002 ${DATA}/PTEM/LI000OAJ.BLI002AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI002 
       JUMP_LABEL=LI000OAK
       ;;
(LI000OAK)
       m_CondExec 04,GE,LI000OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT FICHIER EXTRACTION                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LI000OAM
       ;;
(LI000OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/LI000OAJ.BLI002AO
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 SORTOUT ${DATA}/PTEM/LI000OAM.BLI002BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_ZD_32_6 32 ZD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_32_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LI000OAN
       ;;
(LI000OAN)
       m_CondExec 00,EQ,LI000OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLI004  :  EDITION DES RETOURS DE LIVRAISONS AVEC CUMULS PAR CODES          
#  REPRISE :  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LI000OAQ
       ;;
(LI000OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LIEUX                                                                
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A4} FLI004 ${DATA}/PTEM/LI000OAM.BLI002BO
# *****   EDITION RETOURS LIVRAISONS                                           
       m_OutputAssign -c 9 -w ILI004 ILI004
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI004 
       JUMP_LABEL=LI000OAR
       ;;
(LI000OAR)
       m_CondExec 04,GE,LI000OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LI000OZA
       ;;
(LI000OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LI000OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
