#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TH013P.ksh                       --- VERSION DU 09/10/2016 05:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTH013 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/11/20 AT 10.57.02 BY BURTEC2                      
#    STANDARDS: P  JOBSET: TH013P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTH011  : MISE � JOUR DE LA RVTH1100                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TH013PA
       ;;
(TH013PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TH013PAA
       ;;
(TH013PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***********************                                                      
#   TABLES EN LECTURE   *                                                      
# ***********************                                                      
#                                                                              
#    RTTH15   : NAME=RSTH15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH15 /dev/null
#    RTTH21   : NAME=RSTH21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH21 /dev/null
#                                                                              
# ***********************                                                      
#   TABLES EN ECRITURE  *                                                      
# ***********************                                                      
#                                                                              
#    RSTH15   : NAME=RSTH15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH15 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FTH013 ${DATA}/PXX0/F07.BTH013AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH013 
       JUMP_LABEL=TH013PAB
       ;;
(TH013PAB)
       m_CondExec 04,GE,TH013PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FMD239AP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH013PAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TH013PAD
       ;;
(TH013PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH013PAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/TH013PAD.FTH013AP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTH013AP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH013PAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TH013PAG
       ;;
(TH013PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH013PAG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH013PAD.FTH013AP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TH013PZA
       ;;
(TH013PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH013PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
