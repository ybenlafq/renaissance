#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QINV0M.ksh                       --- VERSION DU 09/10/2016 05:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMQINV0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/09/17 AT 11.50.27 BY PREPA2                       
#    STANDARDS: P  JOBSET: QINV0M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGV600 : EXTRACTION DES VENTES DE LA RTGV11 NON TOPEE LIVREES               
#           DONT LA DATE DE LIVRAISON EST INFERIEURE AU SAMEDI PRECEDE         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QINV0MA
       ;;
(QINV0MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=QINV0MAA
       ;;
(QINV0MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$DATVTE
_end
# ******  VENTES NON TOPEE LIVREES DEPUIS SAMEDI                               
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FGV600 ${DATA}/PXX0/GV610MAA.BGV600AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV600 
       JUMP_LABEL=QINV0MAB
       ;;
(QINV0MAB)
       m_CondExec 04,GE,QINV0MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES VENTES NON TOPEE LIVREES DEPUIS LE SAMEDI PRECEDENT             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAD PGM=BGV610     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAD
       ;;
(QINV0MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  VENTES NON TOPEE LIVREES DEPUIS SAMEDI                               
       m_FileAssign -d SHR -g ${G_A1} FGV600 ${DATA}/PXX0/GV610MAA.BGV600AM
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_OutputAssign -c 9 -w BGV610 FIMP
       m_ProgramExec BGV610 
# ********************************************************************         
#  BIE015 : EXTRACTION SUR LA RTGS15                                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAG
       ;;
(QINV0MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES PRETS ENTREPOT                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
#    RSGS15M  : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15M /dev/null
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
#                                                                              
# ******  TABLE LIEUX                                                          
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
#                                                                              
# ******  FIC DE SORTIES                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 88 -g +1 FIE015 ${DATA}/PBI0/GIE15MAA.BIE015AM
#                                                                              
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE015 
       JUMP_LABEL=QINV0MAH
       ;;
(QINV0MAH)
       m_CondExec 04,GE,QINV0MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BIE015AM                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAJ
       ;;
(QINV0MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PBI0/GIE15MAA.BIE015AM
       m_FileAssign -d NEW,CATLG,DELETE -r 88 -g +1 SORTOUT ${DATA}/PBI0/GIE15MAD.BIE015BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_61_5 61 CH 5
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_86_3 86 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_14_1 ASCENDING,
   FLD_CH_86_3 ASCENDING,
   FLD_CH_61_5 ASCENDING,
   FLD_CH_7_7 ASCENDING
 /* Record Type = F  Record Length = 88 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=QINV0MAK
       ;;
(QINV0MAK)
       m_CondExec 00,EQ,QINV0MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE020  : LISTE DU DEPOT QUI DOIT BASCULER LE PRET RETOUR FOURN EN          
#            SOIT DE LA TABLE RTGS15 SSLIEU 'P' TYPE ' F' VERS RTGS60          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAM PGM=BIE020     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAM
       ;;
(QINV0MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  FIC DE SORTIES                                                       
       m_FileAssign -d SHR -g ${G_A3} FIE020 ${DATA}/PBI0/GIE15MAD.BIE015BM
# ******  FIC D IMPRESSION                                                     
       m_OutputAssign -c 9 -w IFI020 IFI020
       m_ProgramExec BIE020 
# ********************************************************************         
#  LISTE DU MIS DE COTE POUR L ENTREPOT                                        
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAQ
       ;;
(QINV0MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c 9 -w MISCOTE SYSPRINT
       m_OutputAssign -c "*" SYSABEND
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QINV0MAQ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=QINV0MAR
       ;;
(QINV0MAR)
       m_CondExec 04,GE,QINV0MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAT
       ;;
(QINV0MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QINV0M DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q003I (&&NSOC='$DALDEP_1_3' &&NLIEU='$DALDEP_4_3' FORM=ADMFIL.F003I
PRINT REPORT
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QINV0M1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QINV0MAU
       ;;
(QINV0MAU)
       m_CondExec 04,GE,QINV0MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH                                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MAX
       ;;
(QINV0MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QINV0M DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QINV0M2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QINV0M2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QINV0MAY
       ;;
(QINV0MAY)
       m_CondExec 04,GE,QINV0MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QEF00M                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QINV0MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MBA
       ;;
(QINV0MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QEF00M DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QMF00M33
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QMF00M33 -a QMFPARM DSQPRINT
       JUMP_LABEL=QINV0MBB
       ;;
(QINV0MBB)
       m_CondExec 04,GE,QINV0MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=QINV0MZA
       ;;
(QINV0MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/QINV0MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
