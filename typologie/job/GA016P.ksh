#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA016P.ksh                       --- VERSION DU 09/10/2016 00:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGA016 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/10/28 AT 12.02.41 BY PREPA3                       
#    STANDARDS: P  JOBSET: GA016P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DE LA TABLE RTHV70                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA016PA
       ;;
(GA016PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2010/10/28 AT 12.02.39 BY PREPA3                 
# *    JOBSET INFORMATION:    NAME...: GA016P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'CRE TABLE HISTO '                      
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA016PAA
       ;;
(GA016PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES TETE DE NUIT                *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -r 42 -g +1 SYSREC01 ${DATA}/PGV0/F07.UNLOAD.HV70UP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA016PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#    PGM=BHV070 PERMET DE RECONTRUIRE DE MANIERE MENSUELLE LA TOTALITE         
#    DE L'HISTORIQUE DES ACHATS DE DARTY PARIS A PARTIR DE TAB RTGG70          
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA016PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GA016PAD
       ;;
(GA016PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GAMMES (HISTOR DES MVTS DE RECEPTIONS)                                
#    RSGG70   : NAME=RSGG70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG70 /dev/null
#  TABLE COMMANDES FOURNISSEURS                                                
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#  TABLE GENERALISEE (S/TABLE EXACH)                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  SORTIE (FICHIER HISTOR PAR ACHATS PAR SOC MOIS ARTICLES)                    
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -g +1 FHV070 ${DATA}/PTEM/GA016PAD.BHV070BP
#  ENTREE (FICHIER DONNANT LE NUMERO DE SOCIETE A TRAITER)                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/GA016PAD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV070 
       JUMP_LABEL=GA016PAE
       ;;
(GA016PAE)
       m_CondExec 04,GE,GA016PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    PGM=SORT FUSION                                                           
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA016PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GA016PAG
       ;;
(GA016PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GA016PAD.BHV070BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BHV070AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BHV070AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F94.BHV070AR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BHV070AY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BHV070AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BHV070AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BHV070AX
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PGV0/F07.UNLOAD.HV70UP
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -g +1 SORTOUT ${DATA}/PGV0/F07.RELOAD.HV70RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_17_6 17 PD 6
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_PD_23_8 23 PD 8
 /FIELDS FLD_CH_38_5 38 CH 5
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_38_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_17_6,
    TOTAL FLD_PD_23_8
 /* Record Type = F  Record Length = 42 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GA016PAH
       ;;
(GA016PAH)
       m_CondExec 00,EQ,GA016PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV70                                                   
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GA016PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GA016PAJ
       ;;
(GA016PAJ)
       m_CondExec ${EXAAP},NE,YES 
#  FICHIERS DE TRAVAIL                                                         
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSHV70   : NAME=RSHV70,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV70 /dev/null
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PGV0/F07.RELOAD.HV70RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA016PAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GA016P_GA016PAJ_RTHV70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GA016PAK
       ;;
(GA016PAK)
       m_CondExec 04,GE,GA016PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GA016PZA
       ;;
(GA016PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GA016PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
