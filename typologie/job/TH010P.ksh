#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TH010P.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTH010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/02/19 AT 00.14.21 BY BURTEC2                      
#    STANDARDS: P  JOBSET: TH010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BXM100  : DETERMINATION CODICS/PRESTAS A TRAITER                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TH010PA
       ;;
(TH010PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=TH010PAA
       ;;
(TH010PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ***********************                                                      
#   TABLES EN LECTURE   *                                                      
# ***********************                                                      
#                                                                              
#    RSTH01   : NAME=RSTH01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH01 /dev/null
#                                                                              
# ***********************                                                      
#   TABLES EN ECRITURE  *                                                      
# ***********************                                                      
#                                                                              
#    RSTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH00 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH010 
       JUMP_LABEL=TH010PAB
       ;;
(TH010PAB)
       m_CondExec 04,GE,TH010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
