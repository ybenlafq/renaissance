#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TH020P.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTH020 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/19 AT 11.27.13 BY BURTEC2                      
#    STANDARDS: P  JOBSET: TH020P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BTH020      EXTRACTION ARTICLES                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TH020PA
       ;;
(TH020PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+4'}
       G_A11=${G_A11:-'+4'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+5'}
       G_A14=${G_A14:-'+5'}
       G_A15=${G_A15:-'+6'}
       G_A16=${G_A16:-'+6'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+7'}
       G_A19=${G_A19:-'+7'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+8'}
       G_A22=${G_A22:-'+8'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+9'}
       G_A26=${G_A26:-'+9'}
       G_A27=${G_A27:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+3'}
       G_A8=${G_A8:-'+3'}
       G_A9=${G_A9:-'+1'}
       RAAU=${RAAU:-TH020PAM}
       RUN=${RUN}
       JUMP_LABEL=TH020PAA
       ;;
(TH020PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH00 /dev/null
#    RSTH01   : NAME=RSTH01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH01 /dev/null
#    RSTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH12 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 13 -t LSEQ -g +1 FTH020 ${DATA}/PXX0/F07.BTH020AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH020 
       JUMP_LABEL=TH020PAB
       ;;
(TH020PAB)
       m_CondExec 04,GE,TH020PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTH021      EXTRACTION ARTICLES                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAD
       ;;
(TH020PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSTH00   : NAME=RSTH00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH00 /dev/null
#    RSTH01   : NAME=RSTH01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH01 /dev/null
#    RSTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH12 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A1} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER SORTI                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 FIC072 ${DATA}/PXX0/F07.BTH021AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH021 
       JUMP_LABEL=TH020PAE
       ;;
(TH020PAE)
       m_CondExec 04,GE,TH020PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAG
       ;;
(TH020PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAJ
       ;;
(TH020PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH022      EXTRACTION CODICS                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAM
       ;;
(TH020PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A3} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -t LSEQ -g +1 FIC074 ${DATA}/PXX0/F07.BTH022AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH022 
       JUMP_LABEL=TH020PAN
       ;;
(TH020PAN)
       m_CondExec 04,GE,TH020PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAQ
       ;;
(TH020PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A4} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAT
       ;;
(TH020PAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH023      VENTES SEMAINE EN COURS                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=TH020PAX
       ;;
(TH020PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSLV05   : NAME=RSLV05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLV05 /dev/null
#    RSLV15   : NAME=RSLV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLV15 /dev/null
#    RSLW05   : NAME=RSLW05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW05 /dev/null
#    RSLW15   : NAME=RSLW15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLW15 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A6} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 FIC076 ${DATA}/PXX0/F07.BTH023AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH023 
       JUMP_LABEL=TH020PAY
       ;;
(TH020PAY)
       m_CondExec 04,GE,TH020PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBA
       ;;
(TH020PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PBA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A7} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBD PGM=FTP        ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBD
       ;;
(TH020PBD)
       m_CondExec ${EXABT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PBD.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+3),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH026      STOCK                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBG
       ;;
(TH020PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A9} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 FIC078 ${DATA}/PXX0/F07.BTH026AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH026 
       JUMP_LABEL=TH020PBH
       ;;
(TH020PBH)
       m_CondExec 04,GE,TH020PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBJ PGM=EZACFSM1   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBJ
       ;;
(TH020PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PBJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A10} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBM
       ;;
(TH020PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PBM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+4),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH027      ORDRES ACHATS                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBQ
       ;;
(TH020PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGF10   : NAME=RSGF10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF10 /dev/null
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGF49   : NAME=RSGF49,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF49 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A12} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -t LSEQ -g +1 FIC079 ${DATA}/PXX0/F07.BTH027AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH027 
       JUMP_LABEL=TH020PBR
       ;;
(TH020PBR)
       m_CondExec 04,GE,TH020PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBT PGM=EZACFSM1   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBT
       ;;
(TH020PBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PBT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A13} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PBX PGM=FTP        ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=TH020PBX
       ;;
(TH020PBX)
       m_CondExec ${EXACX},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PBX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+5),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH028      FOURNOSSEUR PRINCIPAL                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCA
       ;;
(TH020PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA06   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSAD06   : NAME=RSAD06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAD06 /dev/null
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -t LSEQ -g +1 FIC085 ${DATA}/PXX0/F07.BTH028AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH028 
       JUMP_LABEL=TH020PCB
       ;;
(TH020PCB)
       m_CondExec 04,GE,TH020PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCD PGM=EZACFSM1   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCD
       ;;
(TH020PCD)
       m_CondExec ${EXADH},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PCD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A15} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCG PGM=FTP        ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCG
       ;;
(TH020PCG)
       m_CondExec ${EXADM},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PCG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+6),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH029      COMMANDES ACHATS EN RETARD                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCJ
       ;;
(TH020PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
#    RSGF49   : NAME=RSGF49,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF49 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A17} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 FIC086 ${DATA}/PXX0/F07.BTH029AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH029 
       JUMP_LABEL=TH020PCK
       ;;
(TH020PCK)
       m_CondExec 04,GE,TH020PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCM PGM=EZACFSM1   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCM
       ;;
(TH020PCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PCM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A18} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCQ PGM=FTP        ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCQ
       ;;
(TH020PCQ)
       m_CondExec ${EXAEB},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PCQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+7),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH031      EXTRACTION PRODUITS                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCT
       ;;
(TH020PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA03   : NAME=RSGA03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA03 /dev/null
#    RSGA55   : NAME=RSGA55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA55 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH12 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSRM60   : NAME=RSRM60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM60 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A20} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FIC119 ${DATA}/PXX0/F07.BTH031AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH031 
       JUMP_LABEL=TH020PCU
       ;;
(TH020PCU)
       m_CondExec 04,GE,TH020PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PCX PGM=EZACFSM1   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=TH020PCX
       ;;
(TH020PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PCX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A21} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PDA PGM=FTP        ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=TH020PDA
       ;;
(TH020PDA)
       m_CondExec ${EXAEQ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PDA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+8),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  PGM : BTH032      SOUS-FAMILLE                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=TH020PDD
       ;;
(TH020PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSRM60   : NAME=RSRM60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM60 /dev/null
#         FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A23} FCODIC ${DATA}/PXX0/F07.BTH020AP
#         FICHIER ENTRE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIC083 ${DATA}/PTEM/TH020PDD.BTH032AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTH032 
       JUMP_LABEL=TH020PDE
       ;;
(TH020PDE)
       m_CondExec 04,GE,TH020PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU REPRO                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=TH020PDG
       ;;
(TH020PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PTEM/TH020PDD.BTH032AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BTH032BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TH020PDH
       ;;
(TH020PDH)
       m_CondExec 00,EQ,TH020PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTTH020P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PDJ PGM=EZACFSM1   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=TH020PDJ
       ;;
(TH020PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PDJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A25} SYSOUT ${DATA}/PTEM/TH020PAG.FTTH020P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTH020P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PDM PGM=FTP        ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=TH020PDM
       ;;
(TH020PDM)
       m_CondExec ${EXAFK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PDM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.TH020PAG.FTTH020P(+9),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  REPRO DU FICHIER ABPAR SUR SEQUENTIEL                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TH020PDQ PGM=IDCAMS     ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=TH020PDQ
       ;;
(TH020PDQ)
       m_CondExec ${EXAFP},NE,YES 03,NE,$[RAAU] 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A27} INPUT ${DATA}/PXX0/F07.BTH022AP
       m_FileAssign -d NEW,CATLG,DELETE -r 22 -t LSEQ -g +1 OUTPUT ${DATA}/PXX0/F07.BTH034AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PDQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TH020PDR
       ;;
(TH020PDR)
       m_CondExec 16,NE,TH020PDQ ${EXAFP},NE,YES 03,NE,$[RAAU] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TH020PZA
       ;;
(TH020PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TH020PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
