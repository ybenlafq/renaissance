#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PSE01P.ksh                       --- VERSION DU 08/10/2016 13:57
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPSE01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 97/04/01 AT 16.58.23 BY BURTECA                      
#    STANDARDS: P  JOBSET: PSE01P                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  PGM : BPS600                                                                
#  ------------                                                                
#  RECONSTITUTION DE L'ECHEANCIER.APRES MAJ DES MONTANTS DE CONTRATS           
#  VENDUS,ANNULES OU REMBOURSES DE LA RTPS01                                   
#  CE PGM BPS600 VA CONSTITUER UN FICHIER ECHEANCIER FPS600                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PSE01PA
       ;;
(PSE01PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PSE01PAA
       ;;
(PSE01PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSPS01   : NAME=RSPS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPS01 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
#    RSGA42   : NAME=RSGA42,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA42 /dev/null
# ------  PARAMETRES FMOIS ET SOC                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 FPS600 ${DATA}/PXX0/F07.FPS600AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS600 
       JUMP_LABEL=PSE01PAB
       ;;
(PSE01PAB)
       m_CondExec 04,GE,PSE01PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
