#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM070M.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGM070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/11/07 AT 15.22.54 BY BURTECW                      
#    STANDARDS: P  JOBSET: GM070M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA59 : PRIX                              
#  UNLOAD DES DONNEES ARTICLES NCP (RTGA75 : PRIME                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GM070MA
       ;;
(GM070MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXA98=${EXA98:-0}
       FIRSTME=${FIRSTME}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       MERCRED=${MERCRED}
       RUN=${RUN}
       JUMP_LABEL=GM070MAA
       ;;
(GM070MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#    RSGS30   : NAME=RSGS30M,MODE=I - DYNAM=YES                                
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
#    RSGA75   : NAME=RSGA75M,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PTEM/GM070MAA.BGA59AM
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC02 ${DATA}/PTEM/GM070MAA.BGA75AM
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC03 ${DATA}/PTEM/GM070MAA.BGS30UM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070MAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  SORT DES FICHIERS D'UNLOAD DE LA GA59                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAD
       ;;
(GM070MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GM070MAA.BGA59AM
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SORTOUT ${DATA}/PTEM/GM070MAD.BGA59ZM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_8 15 CH 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_2 8 CH 2
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_15_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MAE
       ;;
(GM070MAE)
       m_CondExec 00,EQ,GM070MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DES FICHIERS D'UNLOAD DE LA GA75                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAG
       ;;
(GM070MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GM070MAA.BGA75AM
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SORTOUT ${DATA}/PTEM/GM070MAG.BGA75ZM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_2 8 CH 2
 /FIELDS FLD_CH_10_8 10 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_2 ASCENDING,
   FLD_CH_10_8 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MAH
       ;;
(GM070MAH)
       m_CondExec 00,EQ,GM070MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC RTGS30                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAJ
       ;;
(GM070MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GM070MAA.BGS30UM
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -g +1 SORTOUT ${DATA}/PTEM/F07.BGS30FM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_4_3,FLD_CH_18_3,FLD_CH_21_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070MAK
       ;;
(GM070MAK)
       m_CondExec 00,EQ,GM070MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM068                                                                      
#  EXTRACT DES VENTES SUR 28 JOURS GLISSANTS                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAM
       ;;
(GM070MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSHV02   : NAME=RSHV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02 /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ****** FICHIER DES VENTES SUR 28 JOURS                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 FGM068 ${DATA}/PTEM/GM070MAM.BGM068AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM068 
       JUMP_LABEL=GM070MAN
       ;;
(GM070MAN)
       m_CondExec 04,GE,GM070MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES SOCIETES                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAQ
       ;;
(GM070MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GM070MAM.BGM068AM
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/GM070MAQ.BGM068BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_PD_8_4 08 PD 4
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_8_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MAR
       ;;
(GM070MAR)
       m_CondExec 00,EQ,GM070MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM069                                                                      
#  EXTRACT DES CODES DESCRIPTIFS , CODE MARKETING                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAT
       ;;
(GM070MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA12   : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#  FICHIER EN ENTRE ISSU DU GM010P                                             
       m_FileAssign -d SHR -g +0 FGA053 ${DATA}/PXX0/F07.BGA053BP
#  MODIF LE FGM069 PASSE DE 273 A 436                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -g +1 FGM069 ${DATA}/PTEM/GM070MAT.BGM069AM
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGM020 ${DATA}/PTEM/GM070MAT.BGM069BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM069 
       JUMP_LABEL=GM070MAU
       ;;
(GM070MAU)
       m_CondExec 04,GE,GM070MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070MAX
       ;;
(GM070MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GM070MAT.BGM069AM
#  CE FICHIER PASSE DE 273 A 436                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 436 -g +1 SORTOUT ${DATA}/PTEM/GM070MAX.BGM069CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_145_5 145 CH 5
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_145_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MAY
       ;;
(GM070MAY)
       m_CondExec 00,EQ,GM070MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBA
       ;;
(GM070MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PEX0/F89.BGM065MM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "989"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_5 
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBB
       ;;
(GM070MBB)
       m_CondExec 00,EQ,GM070MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FGM065Z                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBD
       ;;
(GM070MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PEX0/F07.BGM065CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PEX0/F89.BGM065ZM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "989"
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_1 FLD_CH_8_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBE
       ;;
(GM070MBE)
       m_CondExec 00,EQ,GM070MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM070                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBG
       ;;
(GM070MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA58   : NAME=RSGA58M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSGA02   : NAME=RSGA02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA02 /dev/null
#    RSGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19 /dev/null
#    RSGA66   : NAME=RSGA66M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66 /dev/null
#    RSGG20   : NAME=RSGG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20 /dev/null
#    RSGG40   : NAME=RSGG40M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSMU06   : NAME=RSMU06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMU06 /dev/null
#    RSSP15   : NAME=RSSP15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSP15 /dev/null
#    RSGA68   : NAME=RSGA68M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
#    RSRX00   : NAME=RSRX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX00 /dev/null
#    RSRX55   : NAME=RSRX55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRX55 /dev/null
#                                                                              
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
# **** DATE                                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ****** FICHIER LUS                                                           
       m_FileAssign -d SHR -g ${G_A6} FGM068 ${DATA}/PTEM/GM070MAQ.BGM068BM
       m_FileAssign -d SHR -g ${G_A7} FGM069 ${DATA}/PTEM/GM070MAX.BGM069CM
       m_FileAssign -d SHR -g ${G_A8} FGM020 ${DATA}/PTEM/GM070MAT.BGM069BM
       m_FileAssign -d SHR -g +0 RTGN59 ${DATA}/PXX0/F07.BGN59FF
       m_FileAssign -d SHR -g ${G_A9} RTGA59Z ${DATA}/PTEM/GM070MAD.BGA59ZM
       m_FileAssign -d SHR -g +0 RTGN75F ${DATA}/PXX0/F07.BGN75FF
       m_FileAssign -d SHR -g ${G_A10} RTGA75Z ${DATA}/PTEM/GM070MAG.BGA75ZM
       m_FileAssign -d SHR -g ${G_A11} FGM065M ${DATA}/PEX0/F89.BGM065MM
       m_FileAssign -d SHR -g ${G_A12} FGM065Z ${DATA}/PEX0/F89.BGM065ZM
       m_FileAssign -d SHR -g ${G_A13} RTGS30 ${DATA}/PTEM/F07.BGS30FM
       m_FileAssign -d SHR -g +0 RTGS10 ${DATA}/PXX0/F07.BGS10FF
# ****** FICHIER CREES                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 RTGM01 ${DATA}/PTEM/GM070MBG.BGM070AM
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 RTGM06 ${DATA}/PTEM/GM070MBG.BGM070BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGM075 ${DATA}/PTEM/GM070MBG.BGM070CM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FGM70 ${DATA}/PTEM/GM070MBG.BGM070DM
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 FGM01 ${DATA}/PTEM/GM070MBG.BGM070EM
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 FGM06 ${DATA}/PTEM/GM070MBG.BGM070FM
#  CREATION DE 2 NOUVEAUX FICHIERS LRECL 227                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM025 ${DATA}/PTEM/GM070MBG.BGM070MM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM026 ${DATA}/PTEM/GM070MBG.BGM070NM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM070 
       JUMP_LABEL=GM070MBH
       ;;
(GM070MBH)
       m_CondExec 04,GE,GM070MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBJ
       ;;
(GM070MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GM070MBG.BGM070CM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070MBJ.BGM070GM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBK
       ;;
(GM070MBK)
       m_CondExec 00,EQ,GM070MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBM
       ;;
(GM070MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GM070MBG.BGM070DM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070MBM.BGM070HM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_10_2 10 CH 2
 /FIELDS FLD_CH_268_1 268 CH 1
 /KEYS
   FLD_CH_10_2 ASCENDING,
   FLD_CH_87_7 ASCENDING,
   FLD_CH_268_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBN
       ;;
(GM070MBN)
       m_CondExec 00,EQ,GM070MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBQ
       ;;
(GM070MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GM070MBG.BGM070AM
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PTEM/GM070MBQ.BGM070IM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_25_7 25 CH 7
 /FIELDS FLD_CH_32_7 32 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_24_1 DESCENDING,
   FLD_CH_25_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBR
       ;;
(GM070MBR)
       m_CondExec 00,EQ,GM070MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBT
       ;;
(GM070MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GM070MBG.BGM070EM
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PTEM/GM070MBT.BGM070JM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_229_2 229 CH 2
 /FIELDS FLD_CH_24_1 24 CH 1
 /FIELDS FLD_CH_25_7 25 CH 7
 /KEYS
   FLD_CH_229_2 ASCENDING,
   FLD_CH_25_7 ASCENDING,
   FLD_CH_24_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBU
       ;;
(GM070MBU)
       m_CondExec 00,EQ,GM070MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GM070MBX
       ;;
(GM070MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GM070MBG.BGM070BM
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PTEM/GM070MBX.BGM070KM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_22_1 22 CH 1
 /FIELDS FLD_CH_8_7 8 CH 7
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_8_7 ASCENDING,
   FLD_CH_22_1 DESCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MBY
       ;;
(GM070MBY)
       m_CondExec 00,EQ,GM070MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCA
       ;;
(GM070MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GM070MBG.BGM070FM
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PTEM/GM070MCA.BGM070LM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_15_2 15 CH 2
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_22_1 22 CH 1
 /KEYS
   FLD_CH_15_2 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_22_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MCB
       ;;
(GM070MCB)
       m_CondExec 00,EQ,GM070MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCD
       ;;
(GM070MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GM070MBG.BGM070MM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PTEM/GM070MCD.BGM070OM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_1_16 1 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_104_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MCE
       ;;
(GM070MCE)
       m_CondExec 00,EQ,GM070MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCG
       ;;
(GM070MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/GM070MBG.BGM070NM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PTEM/GM070MCG.BGM070PM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_111_1 111 CH 1
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_111_1 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MCH
       ;;
(GM070MCH)
       m_CondExec 00,EQ,GM070MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM075                                                                      
#  ALIMENTATION DU CAHIER BLEU                                                 
#  ACCES A LA SOUS TABLE GMPRI                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCJ
       ;;
(GM070MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  2 NOUVEAUX FICHIERS RELU EN 227                                             
       m_FileAssign -d SHR -g ${G_A22} FSM025 ${DATA}/PTEM/GM070MCD.BGM070OM
       m_FileAssign -d SHR -g ${G_A23} FSM026 ${DATA}/PTEM/GM070MCG.BGM070PM
       m_FileAssign -d SHR -g ${G_A24} FGM70 ${DATA}/PTEM/GM070MBM.BGM070HM
       m_FileAssign -d SHR -g ${G_A25} FGM075 ${DATA}/PTEM/GM070MBJ.BGM070GM
       m_FileAssign -d SHR -g ${G_A26} RTGM01 ${DATA}/PTEM/GM070MBQ.BGM070IM
       m_FileAssign -d SHR -g ${G_A27} RTGM06 ${DATA}/PTEM/GM070MBX.BGM070KM
       m_FileAssign -d SHR -g ${G_A28} FGM01 ${DATA}/PTEM/GM070MBT.BGM070JM
       m_FileAssign -d SHR -g ${G_A29} FGM06 ${DATA}/PTEM/GM070MCA.BGM070LM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM075 ${DATA}/PTEM/GM070MCJ.BGM075AM
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 RTGM01S ${DATA}/PTEM/GM070MCJ.BGM075BM
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 RTGM06S ${DATA}/PTEM/GM070MCJ.BGM075CM
#  2 NOVEAUX FICHIERS DE SORTIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 FSM025S ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 FGM080 ${DATA}/PTEM/GM070MCJ.BGM075EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM075 
       JUMP_LABEL=GM070MCK
       ;;
(GM070MCK)
       m_CondExec 04,GE,GM070MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM01                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCM
       ;;
(GM070MCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PTEM/GM070MCJ.BGM075BM
       m_FileAssign -d NEW,CATLG,DELETE -r 275 -g +1 SORTOUT ${DATA}/PGG989/F89.RELOAD.GM01RM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_25_14 25 CH 14
 /KEYS
   FLD_CH_25_14 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MCN
       ;;
(GM070MCN)
       m_CondExec 00,EQ,GM070MCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM06                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCQ
       ;;
(GM070MCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GM070MCJ.BGM075CM
       m_FileAssign -d NEW,CATLG,DELETE -r 53 -g +1 SORTOUT ${DATA}/PGG989/F89.RELOAD.GM06RM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 01 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MCR
       ;;
(GM070MCR)
       m_CondExec 00,EQ,GM070MCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI POUR LOAD RTGM70                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCT
       ;;
(GM070MCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/GM070MCJ.BGM075AM
       m_FileAssign -d NEW,CATLG,DELETE -r 402 -g +1 SORTOUT ${DATA}/PEX0/F89.RELOAD.GM70RM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_401 07 CH 401
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_CH_20_74 20 CH 74
 /FIELDS FLD_BI_94_2 94 CH 2
 /FIELDS FLD_PD_17_3 17 PD 3
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_74 ASCENDING,
   FLD_BI_94_2 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_401
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070MCU
       ;;
(GM070MCU)
       m_CondExec 00,EQ,GM070MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM01                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MCX PGM=DSNUTILB   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GM070MCX
       ;;
(GM070MCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A33} SYSREC ${DATA}/PGG989/F89.RELOAD.GM01RM
#    RSGM01   : NAME=RSGM01M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM01 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070MCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070M_GM070MCX_RTGM01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070MCY
       ;;
(GM070MCY)
       m_CondExec 04,GE,GM070MCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM06                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDA
       ;;
(GM070MDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A34} SYSREC ${DATA}/PGG989/F89.RELOAD.GM06RM
#    RSGM06   : NAME=RSGM06M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM06 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070MDA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070M_GM070MDA_RTGM06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070MDB
       ;;
(GM070MDB)
       m_CondExec 04,GE,GM070MDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTGM70                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDD PGM=DSNUTILB   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDD
       ;;
(GM070MDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ****** FIC DE LOAD                                                           
       m_FileAssign -d SHR -g ${G_A35} SYSREC ${DATA}/PEX0/F89.RELOAD.GM70RM
#    RSGM70   : NAME=RSGM70M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGM70 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070MDD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GM070M_GM070MDD_RTGM70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GM070MDE
       ;;
(GM070MDE)
       m_CondExec 04,GE,GM070MDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI SANS MARQUE)                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDG
       ;;
(GM070MDG)
       m_CondExec ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ****** TABLE CAHIER-BLEU PAR ZONES DE PRIX                                   
#    RSGM06   : NAME=RSGM06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
# ******  RELATION CODIC - SRP                                                 
       m_FileAssign -d SHR -g ${G_A36} FGM020 ${DATA}/PTEM/GM070MAT.BGM069BM
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOCIETE : 989000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R989SOC
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX3/GM070MDG
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM020 IMPR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM020 
       JUMP_LABEL=GM070MDH
       ;;
(GM070MDH)
       m_CondExec 04,GE,GM070MDG ${EXAFA},NE,YES 1,EQ,$[MERCRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM020                                                                      
#  EDITION CAHIER-BLEU (TRI AVEC MARQUE)                             *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#  EVITE RELABEL                                                               
# AFF      STEP  PGM=IEFBR14                                                   
#  EVITE RELABEL                                                               
# ********************************************************************         
#  BGM025    LE 1ER MERCREDI DU MOIS                                           
#  EDITION CAHIER-BLEU AVEC ARTICLES EPUISES ET EN CONTREMARQUE      *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDJ
       ;;
(GM070MDJ)
       m_CondExec ${EXAFF},NE,YES 1,EQ,$[FIRSTME] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE.                                                   
#    RSGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES CHEFS DE PRODUITS.                                         
#    RSGA02   : NAME=RSGA02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA02 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES.                                                  
#    RSGA14   : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE VALEURS CODES MARKETING PAR ARTICLE.                           
#    RSGA26   : NAME=RSGA26M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA26 /dev/null
# ******  TABLE CAHIER-BLEU BATCH.                                             
#    RSGM01   : NAME=RSGM01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM01 /dev/null
# ******  TABLE CAHIER-BLEU PAR ZONES DE PRIX                                  
#    RSGM06   : NAME=RSGM06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGM06 /dev/null
#                                                                              
# ******  CARTE DATE.                                                          
       m_FileAssign -i FDATE
$FDATE
_end
#  NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM 2         
       m_FileAssign -d SHR PARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM070MDJ
# ******  FICHIER D'EDITION.                                                   
       m_OutputAssign -c 9 -w BGM025 IMPR
# ******  SOCIETE : 989000                                                     
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R989SOC
#                                                                              
# ******  CARTE PARAMETRE.                                                     
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM025 
       JUMP_LABEL=GM070MDK
       ;;
(GM070MDK)
       m_CondExec 04,GE,GM070MDJ ${EXAFF},NE,YES 1,EQ,$[FIRSTME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGM030                                                                      
#  EDITION CAHIER-BLEU TOUTES ZONES MAIS IMPRESSION MONOZONE                   
#          SUIVANT LE PARAM SOUS-TABLE ZPRIX                                   
# ********************************************************************         
#  EVITE RELABEL                                                               
#                                                                              
# ***********************************                                          
# *   STEP GM070MDM PGM=IEFBR14    ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDM
       ;;
(GM070MDM)
       m_CondExec ${EXAFK},NE,YES 
       m_ProgramExec IEFBR14 
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# ********  TABLE GENERALISEE.                                                 
# RSGA01   FILE  DYNAM=YES,NAME=RSGA01M,MODE=(I,U)                             
# ********  TABLE DES LIEUX                                                    
# RSGA10   FILE  DYNAM=YES,NAME=RSGA10M,MODE=(I,U)                             
# ********  TABLE DES CHEFS DE PRODUITS.                                       
# RSGA02   FILE  DYNAM=YES,NAME=RSGA02,MODE=(I,U)                              
# ********  TABLE DES FAMILLES.                                                
# RSGA14   FILE  DYNAM=YES,NAME=RSGA14M,MODE=(I,U)                             
# ********  TABLE VALEURS CODES MARKETING PAR ARTICLE.                         
# RSGA26   FILE  DYNAM=YES,NAME=RSGA26M,MODE=(I,U)                             
# ********  TABLE CAHIER-BLEU BATCH.                                           
# RSGM01   FILE  DYNAM=YES,NAME=RSGM01M,MODE=I                                 
# ********  TABLE CAHIER-BLEU BATCH.                                           
# RSGM06   FILE  DYNAM=YES,NAME=RSGM06M,MODE=I                                 
# *                                                                            
# *******  JJMMSSAA                                                            
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# * NBRE DE JOURS DES EPUISES RECENTS 3C,% ECART PRIX 2C,% ECART COMM          
# PARAM    DATA  *                                                             
# 0600000                                                                      
#          DATAEND                                                             
# *******  SOCIETE : 989000                                                    
# FSOCLIEU DATA  CLASS=FIX1,MBR=R989SOC                                        
# ********  FICHIERS D'EDITION.                                                
# //IMPR1  DD SYSOUT=(9,IGM03001),SPIN=UNALLOC                                 
# //IMPR2  DD SYSOUT=(9,IGM03002),SPIN=UNALLOC                                 
# //IMPR3  DD SYSOUT=(9,IGM03003),SPIN=UNALLOC                                 
# //IMPR4  DD SYSOUT=(9,IGM03004),SPIN=UNALLOC                                 
# //IMPR5  DD SYSOUT=(9,IGM03005),SPIN=UNALLOC                                 
# //IMPR6  DD SYSOUT=(9,IGM03006),SPIN=UNALLOC                                 
# //IMPR7  DD SYSOUT=(9,IGM03007),SPIN=UNALLOC                                 
# //IMPR8  DD SYSOUT=(9,IGM03008),SPIN=UNALLOC                                 
# //IMPR9  DD SYSOUT=(9,IGM03009),SPIN=UNALLOC                                 
# //IMPR10 DD SYSOUT=(9,IGM03010),SPIN=UNALLOC                                 
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BGM030) PLAN(BGM030M)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDQ
       ;;
(GM070MDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/GM070MCJ.BGM075AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070MDQ.BGM080AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_7 87 CH 7
 /FIELDS FLD_CH_20_67 20 CH 67
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_7_10 07 CH 10
 /FIELDS FLD_CH_270_1 270 CH 1
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_67 ASCENDING,
   FLD_CH_270_1 ASCENDING,
   FLD_CH_87_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MDR
       ;;
(GM070MDR)
       m_CondExec 00,EQ,GM070MDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDT
       ;;
(GM070MDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GM070MCJ.BGM075EM
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -g +1 SORTOUT ${DATA}/PTEM/GM070MDT.BGM075FM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_7 01 CH 7
 /FIELDS FLD_BI_8_2 08 CH 2
 /KEYS
   FLD_BI_8_2 ASCENDING,
   FLD_BI_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MDU
       ;;
(GM070MDU)
       m_CondExec 00,EQ,GM070MDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGM080                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GM070MDX
       ;;
(GM070MDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTGA11   : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  FICHIER ENTRE                                                               
       m_FileAssign -d SHR -g ${G_A39} FGM075 ${DATA}/PTEM/GM070MDQ.BGM080AM
       m_FileAssign -d SHR -g ${G_A40} FGM080 ${DATA}/PTEM/GM070MDT.BGM075FM
#  FICHIER SORTIE                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM080 ${DATA}/PTEM/GM070MDX.BGM080BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGM085 ${DATA}/PTEM/GM070MDX.BGM085AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM080 
       JUMP_LABEL=GM070MDY
       ;;
(GM070MDY)
       m_CondExec 04,GE,GM070MDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM080                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GM070MEA
       ;;
(GM070MEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/GM070MDX.BGM080BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070MEA.BGM080EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_CH_7_10 7 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MEB
       ;;
(GM070MEB)
       m_CondExec 00,EQ,GM070MEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER IGM085                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GM070MED
       ;;
(GM070MED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A42} SORTIN ${DATA}/PTEM/GM070MDX.BGM085AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070MED.BGM085BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_23_60 23 CH 60
 /FIELDS FLD_PD_148_5 148 PD 5
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_CH_7_10 7 CH 10
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_20_3 ASCENDING,
   FLD_CH_23_60 ASCENDING,
   FLD_PD_148_5 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MEE
       ;;
(GM070MEE)
       m_CondExec 00,EQ,GM070MED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GM070MEG
       ;;
(GM070MEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A43} FEXTRAC ${DATA}/PTEM/GM070MED.BGM085BM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GM070MEG.BGM085CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GM070MEH
       ;;
(GM070MEH)
       m_CondExec 04,GE,GM070MEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GM070MEJ
       ;;
(GM070MEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A44} SORTIN ${DATA}/PTEM/GM070MEG.BGM085CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GM070MEJ.BGM085DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM070MEK
       ;;
(GM070MEK)
       m_CondExec 00,EQ,GM070MEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IGM085                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070MEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GM070MEM
       ;;
(GM070MEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A45} FEXTRAC ${DATA}/PTEM/GM070MED.BGM085BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A46} FCUMULS ${DATA}/PTEM/GM070MEJ.BGM085DM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c Z -w IGM085 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GM070MEN
       ;;
(GM070MEN)
       m_CondExec 04,GE,GM070MEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM070MZA
       ;;
(GM070MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
