#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  LI000M.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMLI000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/05/20 AT 16.45.46 BY BURTECM                      
#    STANDARDS: P  JOBSET: LI000M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BLI001 :  EXTRACTION DES LIVRAISONS                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=LI000MA
       ;;
(LI000MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=LI000MAA
       ;;
(LI000MAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  FAMILLES                                                             
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ******  VENTES                                                               
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  DETAILS TOURNEES                                                     
#    RSTL02M  : NAME=RSTL02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02M /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 43 -g +1 FLI001 ${DATA}/PTEM/LI000MAA.BLI001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI001 
       JUMP_LABEL=LI000MAB
       ;;
(LI000MAB)
       m_CondExec 04,GE,LI000MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT FICHIER EXTRACTION                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=LI000MAD
       ;;
(LI000MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/LI000MAA.BLI001AM
       m_FileAssign -d NEW,CATLG,DELETE -r 43 -g +1 SORTOUT ${DATA}/PTEM/LI000MAD.BLI001BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_ZD_38_6 38 ZD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_ZD_32_6 32 ZD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_32_6,
    TOTAL FLD_ZD_38_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LI000MAE
       ;;
(LI000MAE)
       m_CondExec 00,EQ,LI000MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLI003  :  EDITION DES LIVRAISONS PAR PLATEFORME                            
#  REPRISE :  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=LI000MAG
       ;;
(LI000MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LIEUX                                                                
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  DETAILS TOURNEES                                                     
#    RSTL02M  : NAME=RSTL02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL02M /dev/null
# ******  RETOURS TOURNEES                                                     
#    RSTL04M  : NAME=RSTL04M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04M /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A2} FLI003 ${DATA}/PTEM/LI000MAD.BLI001BM
# *****   EDITION                                                              
       m_OutputAssign -c 9 -w ILI003 ILI003
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI003 
       JUMP_LABEL=LI000MAH
       ;;
(LI000MAH)
       m_CondExec 04,GE,LI000MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLI002  :  EXTRACTION DES LIVRAISONS                                        
#  REPRISE :  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000MAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=LI000MAJ
       ;;
(LI000MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  SOUS TABLE TRETO                                                     
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  VENTES                                                               
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  RETOURS TOURNEES                                                     
#    RSTL04M  : NAME=RSTL04M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTL04M /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FLI002 ${DATA}/PTEM/LI000MAJ.BLI002AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI002 
       JUMP_LABEL=LI000MAK
       ;;
(LI000MAK)
       m_CondExec 04,GE,LI000MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT FICHIER EXTRACTION                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=LI000MAM
       ;;
(LI000MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/LI000MAJ.BLI002AM
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 SORTOUT ${DATA}/PTEM/LI000MAM.BLI002BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_ZD_32_6 32 ZD 6
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_32_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=LI000MAN
       ;;
(LI000MAN)
       m_CondExec 00,EQ,LI000MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BLI004  :  EDITION DES RETOURS DE LIVRAISONS AVEC CUMULS PAR CODES          
#  REPRISE :  OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP LI000MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=LI000MAQ
       ;;
(LI000MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  LIEUX                                                                
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
#                                                                              
# ******  SOCIETE                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EXTRACTION                                                   
       m_FileAssign -d SHR -g ${G_A4} FLI004 ${DATA}/PTEM/LI000MAM.BLI002BM
# *****   EDITION RETOURS LIVRAISONS                                           
       m_OutputAssign -c 9 -w ILI004 ILI004
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BLI004 
       JUMP_LABEL=LI000MAR
       ;;
(LI000MAR)
       m_CondExec 04,GE,LI000MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=LI000MZA
       ;;
(LI000MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/LI000MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
