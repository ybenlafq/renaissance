#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PSE00O.ksh                       --- VERSION DU 08/10/2016 13:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPSE00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/21 AT 12.14.43 BY BURTEC3                      
#    STANDARDS: P  JOBSET: PSE00O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSPS01 RSPS02 RSPS03                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PSE00OA
       ;;
(PSE00OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PSE00OAA
       ;;
(PSE00OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QPSE00O
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PSE00OAA
       m_ProgramExec IEFBR14 "RDAR,PSE00O.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PSE00OAD
       ;;
(PSE00OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QPSE00O
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PSE00OAE
       ;;
(PSE00OAE)
       m_CondExec 00,EQ,PSE00OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPS220                                                                
#  ------------                                                                
#  MISE A JOUR DE L'ECHEANCIER POUR LES REMBOURSEMENTS                         
#  CALL DU MBSV03 QUI NE FAIT PAS DE MAJ ET QUI LIT LES TABLES                 
#  RSGA00 RSGA01 RSGA43 RSGA45 RSGA46 RSSV01 RSGQ06                            
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE00OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PSE00OAG
       ;;
(PSE00OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA41   : NAME=RSGA41O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
#    RSGA42   : NAME=RSGA42O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA42 /dev/null
#    RSGA43   : NAME=RSGA42O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA43 /dev/null
#    RSGA45   : NAME=RSGA45O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA45 /dev/null
#    RSGA46   : NAME=RSGA46O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA46 /dev/null
#    RSSV01   : NAME=RSSV01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSV01 /dev/null
#    RSGQ06   : NAME=RSGQ06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ06 /dev/null
#    RSFX00   : NAME=RSFX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSPS01   : NAME=RSPS01O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSPS01 /dev/null
#    RSPS02   : NAME=RSPS02O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSPS02 /dev/null
#    RSPS03   : NAME=RSPS03O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSPS03 /dev/null
# ------  PARAMETRES                                                           
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ------  FICHIER POUR GEN ETAT                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FIPS090 ${DATA}/PXX0/F16.FIPS90AO
# ------  ETAT DES ANOMALIES                                                   
       m_OutputAssign -c 9 -w IPS220 IPS220
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS220 
       JUMP_LABEL=PSE00OAH
       ;;
(PSE00OAH)
       m_CondExec 04,GE,PSE00OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD TEMPORARIRE POUR PCA                                                 
#  UNLOAD DES MVTS DE LA RTPS03                                                
#  REPRISE: NON (BACKOUT=JOBSET)                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE00OAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PSE00OAJ
       ;;
(PSE00OAJ)
       m_CondExec ${EXAAP},NE,YES 
# ******* TABLE DES MOUVEMENTS                                                 
#    RSPS03   : NAME=RSPS03O,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SYSREC01 ${DATA}/PXX0/REC.PS03UNLO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE00OAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  PGM : BPS225                                                                
#  ------------                                                                
#  INTERFACE REMBOURSEMENT DESTINEE A ALIMENTER G.C.V                          
#  CALL DU MFV001                                                              
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE00OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PSE00OAM
       ;;
(PSE00OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSFX00   : NAME=RSFX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ------  TABLES EN MAJ                                                        
#    RSFT29   : NAME=RSFT29O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT29 /dev/null
#    RSPS03   : NAME=RSPS03O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS03 /dev/null
# ------  PARAMETRES                                                           
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ------  FICHIER DES ECS DESTINE A G.C.T                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PXX0/F16.PSE00O
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPS225 
       JUMP_LABEL=PSE00OAN
       ;;
(PSE00OAN)
       m_CondExec 04,GE,PSE00OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
