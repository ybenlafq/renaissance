#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM070F.ksh                       --- VERSION DU 08/10/2016 12:43
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGM070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/06 AT 10.41.05 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GM070F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='FILIALES'                                                          
# ********************************************************************         
#  UNLOAD DE LA TABLE RTGS10                                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GM070FA
       ;;
(GM070FA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2008/05/06 AT 10.41.04 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GM070F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'UNLOAD RTGS10'                         
# *                           APPL...: REPFILIA                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GM070FAA
       ;;
(GM070FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PTEM/GM070FAA.BGS10UF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070FAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  SORT DU FIC BGS10UF                                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM070FAD
       ;;
(GM070FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GM070FAA.BGS10UF
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 SORTOUT ${DATA}/PXX0/F07.BGS10FF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_1_6 01 CH 6
 /FIELDS FLD_CH_18_3 18 CH 3
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_7_7,FLD_CH_1_6,FLD_CH_18_3,FLD_CH_21_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GM070FAE
       ;;
(GM070FAE)
       m_CondExec 00,EQ,GM070FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES TABLE RVGA5977 ET RVGA7577                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM070FAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM070FAG
       ;;
(GM070FAG)
       m_CondExec ${EXAAK},NE,YES 
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
#    RSGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
#    RSGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC01 ${DATA}/PXX0/F07.BGA75UF
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC02 ${DATA}/PXX0/F07.BGN75FF
       m_FileAssign -d NEW,CATLG,DELETE -g +1 SYSREC03 ${DATA}/PXX0/F07.BGN59FF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070FAG.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM070FZA
       ;;
(GM070FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM070FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
