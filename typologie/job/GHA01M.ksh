#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GHA01M.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGHA01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/29 AT 14.25.16 BY BURTECN                      
#    STANDARDS: P  JOBSET: GHA01M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CETTE CHAINE PEUT-ETRES REPORTEE APRES ACCORD D UN RESPONSABLE              
#  RISQUE DE PERTE D INFORMATION SUR LES TABLES ARTICLES                       
#  PAR RAPPORT AUX SAISIES DE PARIS                                            
# ********************************************************************         
# ********************************************************************         
#  BHA002 : CREATION DU FIC DE LOAD POUR LA TABLE P989.RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GHA01MA
       ;;
(GHA01MA)
#
#GHA01MBA
#GHA01MBA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01MBA
#
#
#GHA01MAJ
#GHA01MAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GHA01MAJ
#
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GHA01MAA
       ;;
(GHA01MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE P989.RTGA01                                        
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FIC DES SS TABLES NON MODIFIABLES                                    
       m_FileAssign -d SHR -g +0 FHA001 ${DATA}/PXX0/F07.BHA001AP
#                                                                              
# ******* FIC DE LOAD POUR P989.RTGA01 (METZ)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FHA002 ${DATA}/PTEM/GHA01MAA.BHA002AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA002 
       JUMP_LABEL=GHA01MAB
       ;;
(GHA01MAB)
       m_CondExec 04,GE,GHA01MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P989.RTGA01                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MAD
       ;;
(GHA01MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P989.RTGA01                                  
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GHA01MAA.BHA002AM
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PGA989/F89.RELOAD.GA01RM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_6_15 6 CH 15
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01MAE
       ;;
(GHA01MAE)
       m_CondExec 00,EQ,GHA01MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#           LOAD DE LA TABLE GENERALISEES METZ P989.RTGA01          *          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MAG
       ;;
(GHA01MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE GENERALISEE P989.RTGA01                                        
#    RSGA01   : NAME=RSGA01M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FIC DE LOAD POUR  P989.RTGA01                                        
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PGA989/F89.RELOAD.GA01RM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01MAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01M_GHA01MAG_RTGA01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01MAH
       ;;
(GHA01MAH)
       m_CondExec 04,GE,GHA01MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSGA01M DE LA D BASE PMDGA00     *         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MAM
       ;;
(GHA01MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ASSO ARTICLE/PROFIL                                            
#    RSGA73   : NAME=RSGA73M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA73 /dev/null
# ******* TABLE TEXT ETIQUETTE INFO                                            
#    RSGA74   : NAME=RSGA74M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA74 /dev/null
#                                                                              
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA73                     
       m_FileAssign -d SHR -g +0 FHA73 ${DATA}/PXX0/F07.BHA003AP
# ******* FIC EXTRACT DES MODIFS DU JOUR SUR PDARTY.RTGA74                     
       m_FileAssign -d SHR -g +0 FHA74 ${DATA}/PXX0/F07.BHA003BP
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA004 
       JUMP_LABEL=GHA01MAN
       ;;
(GHA01MAN)
       m_CondExec 04,GE,GHA01MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA009 : MISE A JOUR DE LA TABLE P989.RTGA06                      *         
#           MISE A JOUR DE LA TABLE P989.RTFT10                      *         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MAQ
       ;;
(GHA01MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FIC EXTRACTS ENTITES DE COMMANDES PROVENANT DE NCP                   
       m_FileAssign -d SHR -g +0 FHA005 ${DATA}/PXX0/F07.BHA005AP
#                                                                              
# ******* TABLE GENERALISEE RTGA01                                             
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ENTITES DE COMMANDES RTGA06                                    
#    RSGA06   : NAME=RSGA06M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
# ******* TABLE DES REPERTOIRES FOURNISSEURS                                   
#    RSFT10   : NAME=RSFT10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT10 /dev/null
# ******* TABLE FM                                                             
#    RSFM01   : NAME=RSFM01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM01 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA009 
       JUMP_LABEL=GHA01MAR
       ;;
(GHA01MAR)
       m_CondExec 04,GE,GHA01MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FIC DONNEES ARTICLES AVANT LE LOAD DE P989.RTHA30                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MAT
       ;;
(GHA01MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC DONNEES ARTICLES DE P989.RTHA30                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHA030CP
# ******* FIC DONNEES ARTICLES TRI�                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 SORTOUT ${DATA}/PGA989/F89.RELOAD.HA30RM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GHA01MAU
       ;;
(GHA01MAU)
       m_CondExec 00,EQ,GHA01MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE ARTICLES FILIALES  P989.RTHA30                   *         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MAX
       ;;
(GHA01MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* TABLE ARTICLES FILIALES  P989.RTHA30                                 
#    RSHA30   : NAME=RSHA30M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHA30 /dev/null
# ******* FIC DE LOAD POUR  P989.RTHA30                                        
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PGA989/F89.RELOAD.HA30RM
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01MAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01M_GHA01MAX_RTHA30.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01MAY
       ;;
(GHA01MAY)
       m_CondExec 04,GE,GHA01MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPYPEND DU TABLESPACE RSHA30R DE LA D BASE PMDGA00               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MBD
       ;;
(GHA01MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* N� DE SOCIETE A TRAITER                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA00 ${DATA}/PXX0/F07.BHA050AP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA03 ${DATA}/PXX0/F07.BHA050IP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA31 ${DATA}/PXX0/F07.BHA050JP
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FHA33 ${DATA}/PXX0/F07.BHA050KP
# ******* FIC EXTRACTS RELATION ARTICLE/DECLARATION DE NCP                     
       m_FileAssign -d SHR -g +0 FHA51 ${DATA}/PXX0/F07.BHA050BP
# ******* FIC EXTRACTS RELATION ARTICLE/GARANTIE DE NCP                        
       m_FileAssign -d SHR -g +0 FHA52 ${DATA}/PXX0/F07.BHA050CP
# ******* FIC EXTRACTS ARTICLE/CODE DESCRIPTIF DE NCP                          
       m_FileAssign -d SHR -g +0 FHA53 ${DATA}/PXX0/F07.BHA050DP
# ******* FIC EXTRACTS ARTICLE/ENTITE DE COMMANDE DE NCP                       
       m_FileAssign -d SHR -g +0 FHA55 ${DATA}/PXX0/F07.BHA050EP
# ******* FIC EXTRACTS ARTICLE/ARTICLE DE NCP                                  
       m_FileAssign -d SHR -g +0 FHA58 ${DATA}/PXX0/F07.BHA050FP
# ******* FILE EXTRACTS ZONE DE TRI/PRIX STANDARD                              
       m_FileAssign -d SHR -g +0 FHA59 ${DATA}/PXX0/F07.BHA050GP
# ******* FILE EXTRACTS CARACTERISTIQUES SPECIFIQUES                           
       m_FileAssign -d SHR -g +0 FHA63 ${DATA}/PXX0/F07.BHA050HP
# ******* FIC  EXTRACTS DE GHA01P SUITE MEX PCM                                
       m_FileAssign -d SHR -g +0 FHA40 ${DATA}/PXX0/F07.BHA040AP
       m_FileAssign -d SHR -g +0 FHA41 ${DATA}/PXX0/F07.BHA041AP
       m_FileAssign -d SHR -g +0 FHA42 ${DATA}/PXX0/F07.BHA042AP
#                                                                              
# **TABLES EN MAJ                                                              
#                                                                              
# ****** TABLE ARTICLE                                                         
#    RSGA00   : NAME=RSGA00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA03   : NAME=RSGA03M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA03 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA31   : NAME=RSGA31M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ****** TABLE ARTICLE                                                         
#    RSGA33   : NAME=RSGA33M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA33 /dev/null
# ******* TABLE RELATION ARTICLE/DECLARATION                                   
#    RSGA51   : NAME=RSGA51M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* TABLE RELATION ARTICLE/GARANTIE                                      
#    RSGA52   : NAME=RSGA52M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* TABLE RELATION ARTICLE/CODE DESCRIPTIF                               
#    RSGA53   : NAME=RSGA53M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* TABLE RELATION ARTICLE/ENTITE DE COMMANDE                            
#    RSGA55   : NAME=RSGA55M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA55 /dev/null
# ******* TABLE RELATION ARTICLE/ARTICLE                                       
#    RSGA58   : NAME=RSGA58M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLES : ZONE DE PRIX - PRIX STANDARDS                       
#    RSGA59   : NAME=RSGA59M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE ARTICLES : CARACTERISTIQUES SPECIFIQUES                        
#    RSGA63   : NAME=RSGA63M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA63 /dev/null
# ******* TABLE ARTICLES : MODE DE DELIVRANCE                                  
#    RSGA64   : NAME=RSGA64M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# **TABLES EN LECTURE                                                          
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE FAMILLE / MODE DE DELIVRANCE                                   
#    RSGA13   : NAME=RSGA13M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA13 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE FAMILLES : PARAMETRES ASSOCIES AUX FAMILLES                    
#    RSGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA051 
       JUMP_LABEL=GHA01MBE
       ;;
(GHA01MBE)
       m_CondExec 04,GE,GHA01MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHA008 : MISE A JOUR DES CODES PARAM FAMILLE SUR LA RTGA30 A                
#           PARTIE DU FICHIER FGA30AF ISSU DE GHA01P                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MBG
       ;;
(GHA01MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EXTRACTS ARTICLES PROVENANT DE NCP                               
       m_FileAssign -d SHR -g +0 FGA30 ${DATA}/PXX0/F99.FGA30AF
# ****** TABLE PARAM FAMILLE                                                   
#    RSGA30   : NAME=RSGA30M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHA008 
       JUMP_LABEL=GHA01MBH
       ;;
(GHA01MBH)
       m_CondExec 04,GE,GHA01MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFL001 :                                                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MBJ
       ;;
(GHA01MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA19   : NAME=RSGA19M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTFL05   : NAME=RSFL05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL06   : NAME=RSFL06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFL06 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTGQ05   : NAME=RSGQ05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGQ05 /dev/null
# ------  TABLES EN MAJ                                                        
#    RTGA00   : NAME=RSGA00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA65   : NAME=RSGA65M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66 /dev/null
# ------  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# -----   FICHIER REPRIS DANS LA CHA�NE FL003P                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FL001 ${DATA}/PXX0/F89.BFL001AM
# -----   FICHIER POUR EVITER DEPASSEMENT TABLEAU INTERNE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 159 -t LSEQ -g +1 FGA00 ${DATA}/PXX0/F89.BFL001CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFL001 
       JUMP_LABEL=GHA01MBK
       ;;
(GHA01MBK)
       m_CondExec 04,GE,GHA01MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP00 A PARTIR DU TS RSSP00  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MBM
       ;;
(GHA01MBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP00   : NAME=RSSP00M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP00 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP00
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01MBM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01M_GHA01MBM_RTSP00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01MBN
       ;;
(GHA01MBN)
       m_CondExec 04,GE,GHA01MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP05 A PARTIR DU TS RSSP05  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MBQ PGM=DSNUTILB   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MBQ
       ;;
(GHA01MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP05   : NAME=RSSP05M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP05 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP05
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01MBQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01M_GHA01MBQ_RTSP05.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01MBR
       ;;
(GHA01MBR)
       m_CondExec 04,GE,GHA01MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTSP15 A PARTIR DU TS RSSP15  :JOB UNLOAD GHA01P         
#  SOUS RDAR                                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GHA01MBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MBT
       ;;
(GHA01MBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSSP15   : NAME=RSSP15M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSSP15 /dev/null
       m_FileAssign -d SHR -g +0 SYSREC ${DATA}/PNCGP/F07.UNLOAD.RSSP15
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01MBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GHA01M_GHA01MBT_RTSP15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GHA01MBU
       ;;
(GHA01MBU)
       m_CondExec 04,GE,GHA01MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GHA01MZA
       ;;
(GHA01MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GHA01MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
