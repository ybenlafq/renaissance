#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PSE50G.ksh                       --- VERSION DU 17/10/2016 18:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGPSE50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/05 AT 16.22.12 BY BURTECA                      
#    STANDARDS: P  JOBSET: PSE50G                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PSE50GA
       ;;
(PSE50GA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       G_A3=${G_A3:-'+2'}
       RAAA=${RAAA:-PSE50GAA}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/08/05 AT 16.22.12 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: PSE50G                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'EXTRACTION'                            
# *                           APPL...: REPFILIA                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PSE50GAA
       ;;
(PSE50GAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/F07.PSE050AP
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PXX0/F07.PSE050BP
       m_FileAssign -d SHR -g +0 IN3 ${DATA}/PXX0/F45.PSE050AY
       m_FileAssign -d SHR -g +0 IN4 ${DATA}/PXX0/F45.PSE050BY
       m_FileAssign -d SHR -g +0 IN5 ${DATA}/PXX0/F89.PSE050AM
       m_FileAssign -d SHR -g +0 IN6 ${DATA}/PXX0/F89.PSE050BM
       m_FileAssign -d SHR -g +0 IN7 ${DATA}/PXX0/F91.PSE050AD
       m_FileAssign -d SHR -g +0 IN8 ${DATA}/PXX0/F91.PSE050BD
       m_FileAssign -d SHR -g +0 IN9 ${DATA}/PXX0/F61.PSE050AL
       m_FileAssign -d SHR -g +0 IN10 ${DATA}/PXX0/F61.PSE050BL
       m_FileAssign -d SHR -g +0 IN11 ${DATA}/PXX0/F16.PSE050AO
       m_FileAssign -d SHR -g +0 IN12 ${DATA}/PXX0/F16.PSE050BO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE50GAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PSE50GAB
       ;;
(PSE50GAB)
       m_CondExec 16,NE,PSE50GAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORCAGE DU STATUT SOUS PLAN . MISE EN "JOB CANCELED" VOLONTAIRE             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE50GAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PSE50GAG
       ;;
(PSE50GAG)
       m_CondExec ${EXAAK},NE,YES 3,EQ,$[RAAA] 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE50GAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/PSE50GAG.FPSE050G
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP DU FICHIER                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE50GAJ PGM=JVMLDM76   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PSE50GAJ
       ;;
(PSE50GAJ)
       m_CondExec ${EXAAP},NE,YES 3,EQ,$[RAAA] 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g +0 DD1 ${DATA}/PXX0/F07.PSE050AP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F99.PSE50ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE50GAJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FPSE050G                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE50GAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PSE50GAM
       ;;
(PSE50GAM)
       m_CondExec ${EXAAU},NE,YES 3,EQ,$[RAAA] 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE50GAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A2} SYSOUT ${DATA}/PTEM/PSE50GAG.FPSE050G
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FPSE050G                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PSE50GAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PSE50GAQ
       ;;
(PSE50GAQ)
       m_CondExec ${EXAAZ},NE,YES 3,EQ,$[RAAA] 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE50GAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.PSE50GAG.FPSE050G(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PSE50GZA
       ;;
(PSE50GZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PSE50GZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
