#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GM065P.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGM065 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/16 AT 14.51.51 BY BURTECA                      
#    STANDARDS: P  JOBSET: GM065P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BGM065                                                                      
#  RECHERCHE STOCK ARTICLE                                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GM065PA
       ;;
(GM065PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GM065PAA
       ;;
(GM065PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE DES LIEUX                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ****** TABLE                                                                 
#    RSHV84   : NAME=RSHV84,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV84 /dev/null
# ****** TABLE                                                                 
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GM065PAA
# ****** FICHIER DES VENTES DES 4 SEMAINES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FGM065 ${DATA}/PTEM/GM065PAA.BGM065AP
# ****** FICHIER DES VENTES ISSUES DE RTHV04 ET RTHV12                         
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 FMU204 ${DATA}/PTEM/GM065PAA.BGM065DP
# ******  VENTES DES 8 DERNIERES SEMAINES                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FVENTES ${DATA}/PTEM/GM065PAA.FGM065AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGM065 
       JUMP_LABEL=GM065PAB
       ;;
(GM065PAB)
       m_CondExec 04,GE,GM065PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRM004                                                                      
#  RECHERCHE STOCK ARTICLE                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM065PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GM065PAD
       ;;
(GM065PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE DES LIEUX                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ****** TABLE                                                                 
#    RSHV84   : NAME=RSHV84,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV84 /dev/null
# ****** TABLE                                                                 
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
# ****** TABLE                                                                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER DES VENTES ISSUES DE RTHV04 ET RTHV12                         
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 FRM004 ${DATA}/PTEM/GM065PAD.BRM004AP
# ******  VENTES DES 8 DERNIERES SEMAINES                                      
       m_FileAssign -d SHR -g ${G_A1} FVENTES ${DATA}/PTEM/GM065PAA.FGM065AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRM004 
       JUMP_LABEL=GM065PAE
       ;;
(GM065PAE)
       m_CondExec 04,GE,GM065PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC FVENTES DESTINE AU REAPPRO MAGASIN                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM065PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GM065PAG
       ;;
(GM065PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GM065PAD.BRM004AP
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BGM065FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_45_4 45 PD 4
 /FIELDS FLD_PD_49_4 49 PD 4
 /FIELDS FLD_PD_37_4 37 PD 4
 /FIELDS FLD_CH_17_6 17 CH 6
 /FIELDS FLD_PD_33_4 33 PD 4
 /FIELDS FLD_PD_41_4 41 PD 4
 /FIELDS FLD_CH_31_2 31 CH 02
 /FIELDS FLD_CH_1_16 1 CH 16
 /FIELDS FLD_PD_53_4 53 PD 4
 /KEYS
   FLD_CH_1_16 ASCENDING,
   FLD_CH_17_6 DESCENDING,
   FLD_CH_31_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_33_4,
    TOTAL FLD_PD_37_4,
    TOTAL FLD_PD_41_4,
    TOTAL FLD_PD_45_4,
    TOTAL FLD_PD_49_4,
    TOTAL FLD_PD_53_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM065PAH
       ;;
(GM065PAH)
       m_CondExec 00,EQ,GM065PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC FVENTES DESTINE AU REAPPRO MAGASIN                               
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAF      SORT                                                                
# SORTIN   FILE  NAME=FGM065AP,MODE=I                                          
# SORTOUT  FILE  NAME=BGM065FP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,16,A,17,6,D,31,02,A),FORMAT=CH                               
#  SUM  FIELDS=(33,4,37,4,41,4,45,4,49,4,53,4),FORMAT=PD                       
#          DATAEND                                                             
# ********************************************************************         
#  CONSTITUTION DU FIC DES VENTES PAR MAGASIN                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM065PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GM065PAJ
       ;;
(GM065PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GM065PAA.BGM065AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.BGM065BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_20_7 20 CH 7
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_38_7 38 CH 7
 /FIELDS FLD_CH_34_4 34 CH 4
 /FIELDS FLD_CH_45_7 45 CH 7
 /FIELDS FLD_CH_16_4 16 CH 4
 /FIELDS FLD_CH_27_7 27 CH 7
 /FIELDS FLD_CH_14_2 14 CH 2
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_14_2 ASCENDING,
   FLD_CH_11_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_16_4,
    TOTAL FLD_CH_20_7,
    TOTAL FLD_CH_27_7,
    TOTAL FLD_CH_34_4,
    TOTAL FLD_CH_38_7,
    TOTAL FLD_CH_45_7,
    TOTAL FLD_CH_52_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM065PAK
       ;;
(GM065PAK)
       m_CondExec 00,EQ,GM065PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES PAR ZONE DE PRIX                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM065PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GM065PAM
       ;;
(GM065PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GM065PAA.BGM065AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.BGM065CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_38_7 38 CH 7
 /FIELDS FLD_CH_20_7 20 CH 7
 /FIELDS FLD_CH_52_7 52 CH 7
 /FIELDS FLD_CH_27_7 27 CH 7
 /FIELDS FLD_CH_34_4 34 CH 4
 /FIELDS FLD_CH_1_7 01 CH 7
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_16_4 16 CH 4
 /FIELDS FLD_CH_45_7 45 CH 7
 /FIELDS FLD_CH_14_2 14 CH 2
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_14_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_16_4,
    TOTAL FLD_CH_20_7,
    TOTAL FLD_CH_27_7,
    TOTAL FLD_CH_34_4,
    TOTAL FLD_CH_38_7,
    TOTAL FLD_CH_45_7,
    TOTAL FLD_CH_52_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM065PAN
       ;;
(GM065PAN)
       m_CondExec 00,EQ,GM065PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CONSTITUTION DU FIC DES VENTES DES 4 DERNIERES SEMAINES                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GM065PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GM065PAQ
       ;;
(GM065PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GM065PAA.BGM065DP
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.BGM065EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_24_4 24 PD 4
 /FIELDS FLD_PD_28_4 28 PD 4
 /FIELDS FLD_PD_16_4 16 PD 4
 /FIELDS FLD_PD_20_4 20 PD 4
 /FIELDS FLD_PD_32_4 32 PD 4
 /FIELDS FLD_CH_1_15 01 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_16_4,
    TOTAL FLD_PD_20_4,
    TOTAL FLD_PD_24_4,
    TOTAL FLD_PD_28_4,
    TOTAL FLD_PD_32_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GM065PAR
       ;;
(GM065PAR)
       m_CondExec 00,EQ,GM065PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GM065PZA
       ;;
(GM065PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GM065PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
