#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GG065L.ksh                       --- VERSION DU 08/10/2016 13:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGG065 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/11 AT 14.52.22 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GG065L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BGR017                                                               
# ********************************************************************         
#   EXTRACTION DES RISTOURNES POUR UNE JOURNEE DONNEE                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GG065LA
       ;;
(GG065LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GG065LAA
       ;;
(GG065LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES PRMP DU JOUR                                                      
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#  TABLE DES DEMANDES DE RECYCLAGES DE PRA                                     
#    RSGG60   : NAME=RSGG60L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG60 /dev/null
#  TABLE ARTICLES                                                              
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#  LISTE DES RECYCLAGE DE PRMP EFFECTUEES                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGB17 ${DATA}/PXX0/F61.BGB017AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR017 
       JUMP_LABEL=GG065LAB
       ;;
(GG065LAB)
       m_CondExec 04,GE,GG065LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM : BGR018                                                               
# ********************************************************************         
#   EDITION DES RISTOURNES POUR UNE JOURNEE DONNEE                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GG065LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GG065LAD
       ;;
(GG065LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES RECYCLAGE DE PRMP                                               
       m_FileAssign -d SHR -g ${G_A1} FGB17 ${DATA}/PXX0/F61.BGB017AL
#  LISTE DES RISTOURNES                                                        
       m_OutputAssign -c 9 -w IGB18 IGB18
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#    RSGA55   : NAME=RSGA55L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA55 /dev/null
#    RSGA06   : NAME=RSGA06L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGR018 
       JUMP_LABEL=GG065LAE
       ;;
(GG065LAE)
       m_CondExec 04,GE,GG065LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
