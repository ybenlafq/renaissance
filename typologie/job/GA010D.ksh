#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GA010D.ksh                       --- VERSION DU 08/10/2016 22:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGA010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/19 AT 09.54.29 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GA010D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DPM'                                                               
# ********************************************************************         
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
# ********************************************************************         
#  B G A 0 0 1                                                                 
# ********************************************************************         
#   CREATION DES RELATIONS EXPOSITION MAGASIN FAMILLE :                        
#   MAJ TABLE RELATION FAMILLE EXPOSITION MAGASIN (RTGA19)                     
#   LECTURE DE LA TABLE RTGA14                                                 
#   REPRISE: OUI SI FIN ANORMALE NON SI REPRISE APRES FIN NORMALE              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GA010DA
       ;;
(GA010DA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2001/02/19 AT 09.54.29 BY BURTEC5                
# *    JOBSET INFORMATION:    NAME...: GA010D                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXPO MAGASIN/FAM'                      
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GA010DAA
       ;;
(GA010DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    CREATION PARAMETRES               *                                       
# PRED1    LINK  NAME=$GA001D,MODE=I    *                                      
# PRED2    LINK  NAME=$GHA01D,MODE=I    *                                      
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FAMILLES                                                   
#    RSGA14D  : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14D /dev/null
# ******* TABLE RELATION FAMILLE / EXPO MAGASIN                                
#    RSGA19D  : NAME=RSGA19,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA19D /dev/null
# ******  TABLE RELATION ARTICLE / ASSORTIMENT MAGASIN                         
#    RSGA54D  : NAME=RSGA54,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54D /dev/null
       m_FileAssign -d SHR PARAMEX ${DATA}/CORTEX4.P.MTXTFIX5/BGA001D
       m_OutputAssign -c "*" IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGA001 
       JUMP_LABEL=GA010DAB
       ;;
(GA010DAB)
       m_CondExec 04,GE,GA010DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
