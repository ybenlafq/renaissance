#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BS1A1L.ksh                       --- VERSION DU 08/10/2016 22:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLBS1A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/09/20 AT 14.56.58 BY BURTEC7                      
#    STANDARDS: P  JOBSET: BS1A1L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   IDCAMS CREATION A VIDE DES FICHIERS :                                      
#   - FIC1 :                                                                   
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BS1A1LA
       ;;
(BS1A1LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2011/09/20 AT 14.56.58 BY BURTEC7                
# *    JOBSET INFORMATION:    NAME...: BS1A1L                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-BS001L'                            
# *                           APPL...: IMPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BS1A1LAA
       ;;
(BS1A1LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT1 ${DATA}/PXX0/F61.BBS002AL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS1A1LAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BS1A1LAB
       ;;
(BS1A1LAB)
       m_CondExec 16,NE,BS1A1LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    CREATION A VIDE D UNE GENERATION POUR DXGSMP                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BS1A1LAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BS1A1LAD
       ;;
(BS1A1LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 OUT1 ${DATA}/PXX0.F61.BBS001BL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BS1A1LAD.sysin
       m_UtilityExec
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=BS1A1LAE
       ;;
(BS1A1LAE)
       m_CondExec 16,NE,BS1A1LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
