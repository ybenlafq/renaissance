#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NV010P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPNV010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/18 AT 11.55.30 BY BURTEC2                      
#    STANDARDS: P  JOBSET: NV010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   UNLOAD DE LA TABLE RTNV00                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTNV00 : PARIS                               
#  REPRISE .OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=NV010PA
       ;;
(NV010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=NV010PAA
       ;;
(NV010PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
#                                                                              
#    RSNV00   : NAME=RSNV00,MODE=I - DYNAM=YES                                 
#                                                                              
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/NV010PAA.NV010UAP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV010PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 312 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV010PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NV010PAD
       ;;
(NV010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/NV010PAA.NV010UAP
       m_FileAssign -d NEW,CATLG,DELETE -r 312 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV010PAD.NV010UBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ADDITIONALDATA_ASSOC"
 /FIELDS FLD_CH_1_20 1 CH 20
 /FIELDS FLD_CH_22_255 22 CH 255
 /CONDITION CND_1 FLD_CH_1_20 EQ CST_1_4 
 /KEYS
   FLD_CH_22_255 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV010PAE
       ;;
(NV010PAE)
       m_CondExec 00,EQ,NV010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER UNLOAD DE RTNV00  .LRECL 312 DE LONG                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV010PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=NV010PAG
       ;;
(NV010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/NV010PAA.NV010UAP
       m_FileAssign -d NEW,CATLG,DELETE -r 312 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/NV010PAG.NV010UEP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ADDITIONALDATA_ASSOC"
 /FIELDS FLD_CH_22_255 22 CH 255
 /FIELDS FLD_CH_1_20 1 CH 20
 /CONDITION CND_1 FLD_CH_1_20 EQ CST_1_4 
 /KEYS
   FLD_CH_22_255 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV010PAH
       ;;
(NV010PAH)
       m_CondExec 00,EQ,NV010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT DU FICHIER VENANT DU PCL V205PZ:DCLF ZV205CCP                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV010PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=NV010PAJ
       ;;
(NV010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.NV050AAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BNV053AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BNV055AP
       m_FileAssign -d NEW,CATLG,DELETE -r 1255 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGP/F07.NV010UCP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_255 1 CH 255
 /KEYS
   FLD_CH_1_255 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=NV010PAK
       ;;
(NV010PAK)
       m_CondExec 00,EQ,NV010PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV010 : ENVOI DES DONNEES NCG VERS LE CATALOGUE VIA MQ                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV010PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=NV010PAM
       ;;
(NV010PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV10   : NAME=RSNV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV10 /dev/null
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREES                                                    
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FNV010 ${DATA}/PNCGP/F07.NV010UCP
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FNV000 ${DATA}/PTEM/NV010PAD.NV010UBP
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
# ****** TABLE                                                                 
#    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV010 
       JUMP_LABEL=NV010PAN
       ;;
(NV010PAN)
       m_CondExec 04,GE,NV010PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BNV010 : ENVOI DES DONNEES NCG VERS LE CATALOGUE VIA MQ                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NV010PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=NV010PAQ
       ;;
(NV010PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#   TABLES EN LECTURE                                                          
#                                                                              
# ****** TABLE                                                                 
#    RSNV10   : NAME=RSNV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV10 /dev/null
# ****** TABLE                                                                 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ****** CARTE DATE.                                                           
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREES                                                    
#                                                                              
       m_FileAssign -d SHR -g ${G_A5} FNV010 ${DATA}/PNCGP/F07.NV010UCP
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FNV000 ${DATA}/PTEM/NV010PAG.NV010UEP
#                                                                              
#   TABLES EN M.AJ                                                             
#                                                                              
# ****** TABLE                                                                 
#    RSNV00   : NAME=RSNV00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV00 /dev/null
# ****** TABLE                                                                 
#    RSNV11   : NAME=RSNV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BNV010 
       JUMP_LABEL=NV010PAR
       ;;
(NV010PAR)
       m_CondExec 04,GE,NV010PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=NV010PZA
       ;;
(NV010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NV010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
